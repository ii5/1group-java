package one.group.dao;

import java.util.List;

import one.group.core.enums.status.NotificationStatus;
import one.group.entities.jpa.NotificationTemplate;

public interface NotificationDAO
{
    public long fetchCountByStatus(String userId, NotificationStatus status);

    public List<NotificationTemplate> fetchNotificationTemplates();

}
