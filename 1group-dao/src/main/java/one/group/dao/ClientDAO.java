package one.group.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.helpers.ClientObject;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;

/**
 * The Interface ClientDAO.
 *
 * @author nyalfernandes
 */
public interface ClientDAO
{

    /**
     * To fetch list of clients of an account.
     *
     * @param accountId
     *            the account id
     * @return the list
     */
    public List<Client> fetchAllClientsOfAccount(String accountId);

    /**
     * Save client.
     *
     * @param client
     *            the client
     * @return the client
     */
    public void saveClient(Client client);

    /**
     * Fetch all push channels of clients of account.
     *
     * @param accountId
     *            the account id
     * @return the list
     */
    public List<String> fetchAllPushChannelsOfClientsOfAccount(String accountId);

    /**
     * Fetch client by id.
     *
     * @param clientId
     *            the client id
     * @return the client
     */
    public Client fetchClientById(String clientId);

    /**
     * Fetch by push channel.
     *
     * @param pushChannel
     *            the push channel
     * @return the client
     */
    public Client fetchByPushChannel(String pushChannel);

    /**
     * Gets the account from auth token.
     *
     * @param authToken
     *            the auth token
     * @param clientId
     *            the client id
     * @return the account from auth token
     * @throws AuthenticationException
     *             the authentication exception
     * @throws ClientProcessException
     *             the client process exception
     * @throws AccountNotFoundException
     *             the account not found exception
     */
    public Account getAccountFromAuthToken(String authToken, String clientId) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * Fetch by account and device platform.
     *
     * @param accountId
     *            the account id
     * @param devicePlatform
     *            the device platform
     * @return the list
     */
    public List<Client> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform);

    /**
     * Fetch client by account & cps Id
     * 
     * @param accountId
     * @param deviceToken
     * @return
     */
    public Client fetchByAccountIdAndDeviceToken(String accountId, String deviceToken);

    public List<Client> fetchByDeviceToken(String deviceToken);

    public List<Client> fetchAllActiveClientsOfAccount(String accountId);

    public List<Client> fetchAllActiveClients();

    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate);

    public void deactivateClient(List<Client> clientList);

    public void purgeClient(List<Client> clientList);

    public List<Client> getLastActivityTime(String accountId);

    /**
     * Fetch last activity of accounts
     * 
     * @param accountIds
     * @return
     */
    public List<ClientObject> getLastActivityOfAccounts(Set<String> accountIds);

    public String getAccountIdFromClientId(String clientId);
}