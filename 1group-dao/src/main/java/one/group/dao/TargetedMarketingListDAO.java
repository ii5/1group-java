package one.group.dao;

import java.util.List;

import one.group.entities.jpa.TargetedMarketingList;
/**
 * 
 * @author sanilshet
 *
 */
public interface TargetedMarketingListDAO 
{
	/**
	 * 
	 * @return
	 */
	public List<TargetedMarketingList> fetchAllTargetedList();
	
	/**
	 * 
	 * @param phoneNumber
	 */
	public void deleteTargetedListByPhoneNumber(String phoneNumber);
	
	
	public void deleteAllTargetedList();
	
}
