package one.group.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.socket.Message;
import one.group.exceptions.movein.Abstract1GroupException;

/**
 *
 * @author nyalfernandes
 *
 */
public interface MessageDAO
{
    public Set<Message> fetchMessagesOfAccount(String accountId);

    public Map<String, Integer> fetchMessageCountByGroupId(boolean isDuplicate, List<BpoMessageStatus> bpoMessageStatusList, List<MessageStatus> messageStatusList);

    /**
     * Save chat message
     *
     * @param message
     */
    public void saveMessage(Message message);

    /**
     * fetch last message of group
     *
     * @param groupId
     * @return
     */
    public List<Message> fetchLastMessageOfGroup(String groupId);

    public List<Message> fetchMessagesByStatusAndGroupIds(List<MessageStatus> messageStatusList, List<GroupStatus> groupStatusList, List<String> groupIdList,
            List<BpoMessageStatus> bpoMessageStatusList, String bpoMessageUpdateTime, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows, List<String> fields);

    public List<Message> fetchPhonecallTypeMessagesOfAccount(String accountId);

    public Message fetchMessageFromId(String messageId);

    public List<Message> fetchMessageByBroadcastIds(List<String> broadcastIds);

    public List<Message> fetchMessagesBySearchCriteria();

    public List<Message> fetchSharedMessagesByType(String accountId, String groupId);

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType);

    public List<Message> fetchMessagesByGroupIdAndSourceAndType(String groupId, MessageSourceType messageSourceType, List<MessageType> messageTypeList, int offset, int limit);

    public List<Message> fetchMessageByBroadcastIdsAndStatusAndSource(List<String> broadcastIdsList, List<MessageStatus> messageStatusList, List<MessageSourceType> messageSourceTypeList,
            MessageType messageType);

    public List<String> fetchMessageByBroadcastIdsAndSourceAndAccount(List<String> accountIdList, List<String> broadcastIdsList, MessageSourceType messageSourceType, MessageStatus messageStatus);

    public List<Message> fetchMessagesFromIds(List<String> messageIds, List<String> fields);
    
    public List<Message> fetchMessagesFromIdsWithException(List<String> messageIds, List<String> fields) throws Abstract1GroupException;

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType, int offset, int limit);

    public boolean updateMessageStatus(List<String> messageIds, MessageStatus messageStatus, BpoMessageStatus bpoMessageStatus, String updatedBy, String bpoUpdatedBy);

    public List<Message> fetchAllMessagesByAccountIdAndMessageSourceTypeList(String accountId, List<MessageType> messageTypeList, List<MessageSourceType> messageSourceTypeList, boolean latestToOldest);

    public long fetchUnreadMessageCount(String groupId);

    public long fetchTotalMessageCount(String groupId);

    public void updateMessageStatusBybroadcastId(String broadcastId, BroadcastStatus broadcastStatus, String accountId);

    public List<Message> fetchMessagesByBroadcastIdAndMessageStatus(String broadcastId, MessageStatus messageStatus);

    public void saveMessages(Collection<Message> messages);

    public List<String> fetchBroadcastIdsByMessageCreator(Set<String> mobileNumber);

    public Map<String, Map<String, Object>> fetchLatestMessageBasedOnGroupQuery(Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, boolean useGroup, SortField groupSortField,
            SortType groupSortType, String groupField, List<String> fields, int start, int rows);

    public long countAllMessagesByAccountIdForMeAndWhatsApp(String accountId);

    public List<Message> fetchMessagesByForMeAndWhatsappByAccountId(String accountId, int offset, int limit);

    public List<Message> fetchLastMessageOfGroupNotTypePhonecall(String groupId);

    public Map<String, Long> fetchBroadcastMessageCountByAccountId(List<String> accountIdList);

    public List<Message> fetchMessageByBroadcastIdAndSourceAndStatus(Collection<String> broadcastIdList, List<MessageSourceType> messageSourceList, List<MessageStatus> messageStatusList);
    
    public void update(Message message);
    
    public void update(List<Message> messageList);

	public void saveMessagesToDB(List<Message> messagesToUpdate);
    
}
