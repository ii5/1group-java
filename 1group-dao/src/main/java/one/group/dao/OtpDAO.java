package one.group.dao;

import one.group.entities.jpa.Otp;

public interface OtpDAO
{

    public Otp fetchOtpFromOtpAndPhoneNumber(String otp, String mobileNumber);

    public void saveOTP(Otp otp);

    public void deleteOtp(Otp otp);

}