package one.group.dao;

import java.util.List;

import one.group.entities.jpa.NearByLocation;

public interface NearByLocalityDAO
{
    public List<NearByLocation> fetchNearByLocalityByLocationId(String locationId);
}
