/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.LocationErrorTmp;

/**
 * @author ashishthorat
 *
 */
public interface LocationErrorTempDAO
{
    public List<LocationErrorTmp> fetchLocationErrorTmpByCityId(String cityId);

    public List<LocationErrorTmp> fetchLocationErrorTmpByParentId(String parentId);

    public List<LocationErrorTmp> fetchLocationErrortmpByLoginUserID(String loginUserId);

    public List<LocationErrorTmp> fetchLocationErrorTmpByStatus(String status);

    public void saveLocationErrorTmp(LocationErrorTmp locationErrorTmp);

    public LocationErrorTmp fetchLocationErrorTmpById(String locationErrorTmpId);
}
