/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.WaQrcodes;

/**
 * @author ashishthorat
 *
 */
public interface WaQrcodesDAO
{

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentServerId(String currentServerId);

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentClientId(String currentClientId);

    public List<WaQrcodes> fetchWaQrcodesDAOByStatus(String status);

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentCrmMobile(String currentCrmMobile);

    public List<WaQrcodes> fetchWaQrcodesDAOByWaGroupsApprovedStatus(String waGroupsApprovedStatus);
}
