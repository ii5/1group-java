package one.group.dao;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.Media;

public interface MediaDAO
{
    public Media addMedia(Media media);

    public void removeMedia(String entityType, Map<String, Object> parameters) throws IOException;

    public Media fetchByMediaId(String mediaId);
    
    public List<Media> fetchMediaByIds(Collection<String> mediaIdCollection);
}
