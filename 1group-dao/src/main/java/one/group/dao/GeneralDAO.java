package one.group.dao;

import java.util.Map;
import java.util.Set;

import one.group.core.enums.EntityFieldName;
import one.group.core.enums.EntityType;

public interface GeneralDAO
{

    public Set<String> saveAndFetchRecords(final String accountId, final EntityType type, final String queryString, final Map<EntityFieldName, Object> fieldVsValueMap, final String paginationKey,
            final int pageNo, final String refreshPaginationKey);

    public long fetchTotalRecordsCount(final String paginationKey);
}
