package one.group.dao;

import one.group.entities.jpa.BroadcastGroupRelation;

public interface BroadcastGroupRelationDAO
{
    public void saveBroadcastGroupRelation(BroadcastGroupRelation relation);
}
