package one.group.dao;

import java.util.List;

import one.group.entities.jpa.LocationMatch;

public interface LocationMatchDAO
{
    public List<LocationMatch> fetchExactLocationMatchByLocationId(String locationId, boolean isExact);

    public List<LocationMatch> fetchExactLocationMatchByLocationId(List<String> locationIdList, boolean isExact);

    public List<String> fetchExactLocationMatchIdListByLocationIdList(List<String> locationIdList, boolean isExact);

    public List<LocationMatch> fetchLocationMatchByLocationId(String locationId);

    public List<String> fetchLocationMatchByLocationIdList(List<String> locationIdList);

    public void saveLocationMatch(LocationMatch locationMatch);
}
