package one.group.dao;

import one.group.entities.jpa.OauthAuthorization;

import org.springframework.security.oauth2.provider.ClientRegistrationException;

public interface OAuthAuthorizationDAO
{
    public void saveClientDetail(OauthAuthorization oauthAuthorization);
    
    public OauthAuthorization loadClientByClientId(String clientId)
			throws ClientRegistrationException;
    
    public OauthAuthorization getClient(String clientId);

}