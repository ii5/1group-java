/**
 * 
 */
package one.group.dao;

import java.util.Collection;

import one.group.entities.jpa.AuthItemChild;

public interface AuthItemChildDAO
{

    public void saveAuthItemChild(AuthItemChild authItemChild);
    
    public Collection<AuthItemChild> fetchAll();
}
