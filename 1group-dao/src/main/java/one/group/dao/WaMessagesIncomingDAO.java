/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.WaMessagesIncoming;

/**
 * @author ashishthorat
 *
 */
public interface WaMessagesIncomingDAO
{
    /**
     * 
     * @param groupId
     * @return
     */
    public List<WaMessagesIncoming> fetchUserByGroupId(String groupId);

    /**
     * Save waMessagesArchive
     * 
     * @param waMessagesArchive
     * @return
     */
    public void saveWaMessagesIncoming(WaMessagesIncoming waMessagesIncoming);

    /**
     * 
     * @param waMessageId
     * @return
     */
    public WaMessagesIncoming fetchWaMessagesIncomingByWaMessagesIncomingId(String waMessageId);

    /**
     * 
     * @param waMobileNo
     * @return
     */
    public List<WaMessagesIncoming> fetchWaMessagesIncomingByWaMobileNo(String waMobileNo);

    /**
     * 
     * @param messageType
     * @return
     */
    public List<WaMessagesIncoming> fetchWaMessagesIncomingByMessageType(String messageType);

    /**
     * 
     * @param status
     * @return
     */
    public List<WaMessagesIncoming> fetchWaMessagesIncomingByStatus(String status);
}
