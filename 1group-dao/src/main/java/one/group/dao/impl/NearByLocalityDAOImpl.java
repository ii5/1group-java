package one.group.dao.impl;

import java.util.List;

import one.group.dao.NearByLocalityDAO;
import one.group.entities.jpa.NearByLocation;
import one.group.jpa.dao.NearByLocalityJpaDAO;

public class NearByLocalityDAOImpl implements NearByLocalityDAO
{

    NearByLocalityJpaDAO nearByLocalityJpaDAO;

    public NearByLocalityJpaDAO getNearByLocalityJpaDAO()
    {
        return nearByLocalityJpaDAO;
    }

    public void setNearByLocalityJpaDAO(NearByLocalityJpaDAO nearByLocalityJpaDAO)
    {
        this.nearByLocalityJpaDAO = nearByLocalityJpaDAO;
    }

    public List<NearByLocation> fetchNearByLocalityByLocationId(String locationId)
    {
        return nearByLocalityJpaDAO.fetchLocalityByLocationId(locationId);
    }

}
