/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.jboss.logging.annotations.Transform.TransformType;

import one.group.dao.AuthAssignmentDAO;
import one.group.entities.jpa.AuthAssignment;
import one.group.jpa.dao.AuthAssignmentJpaDAO;

public class AuthAssignmentDAOImpl implements AuthAssignmentDAO
{

    private AuthAssignmentJpaDAO authAssignmentJpaDAO;

    public AuthAssignmentJpaDAO getAuthAssignmentJpaDAO()
    {
        return authAssignmentJpaDAO;
    }

    public void setAuthAssignmentJpaDAO(AuthAssignmentJpaDAO authAssignmentJpaDAO)
    {
        this.authAssignmentJpaDAO = authAssignmentJpaDAO;
    }

    public void saveAuthAssignmentDAO(AuthAssignment authAssignment)
    {
        authAssignmentJpaDAO.saveAuthAssignment(authAssignment);
    }

    public List<AuthAssignment> fetchByUserId(String userId)
    {
        return authAssignmentJpaDAO.fetchByUserId(userId);
    }
    
    public List<AuthAssignment> fetchAuthAssignmentItemsByUserIdList(Collection<String> userIdList)
    {
        return authAssignmentJpaDAO.fetchAuthAssignmentItemsByUserIdList(userIdList);
    }
    
    public List<String> fetchAuthAssignmentItemNamesByUserId(String userId)
    {
        return authAssignmentJpaDAO.fetchAuthAssignmentItemNamesByUserId(userId);
    }
    
    public void clearAssignmentOfUser(Collection<String> userId)
    {
        authAssignmentJpaDAO.deleteAssignmentOfId(userId);
    }
    
    public List<String> fetchUserIdsByRoles(Collection<String> roles)
    {
    	return authAssignmentJpaDAO.fetchUserIdsByRoles(roles);
    }
}
