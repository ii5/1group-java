/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.cache.dao.AccountCacheDAO;
import one.group.dao.AccountRelationDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.jpa.dao.AccountRelationsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class AccountRelationDAOImpl implements AccountRelationDAO
{

    private AccountRelationsJpaDAO accountRelationsJpaDAO;

    private AccountCacheDAO accountCacheDAO;

    public AccountCacheDAO getAccountCacheDAO()
    {
        return accountCacheDAO;
    }

    public void setAccountCacheDAO(AccountCacheDAO accountCacheDAO)
    {
        this.accountCacheDAO = accountCacheDAO;
    }

    public AccountRelationsJpaDAO getAccountRelationsJpaDAO()
    {
        return accountRelationsJpaDAO;
    }

    public void setAccountRelationsJpaDAO(AccountRelationsJpaDAO accountRelationsJpaDAO)
    {
        this.accountRelationsJpaDAO = accountRelationsJpaDAO;
    }

    public void createAccountRelation(AccountRelations relations)
    {
        accountRelationsJpaDAO.createAccountRelation(relations);
    }

    public void updateAccountRelation(AccountRelations relations)
    {
        accountRelationsJpaDAO.updateAccountRelation(relations);
    }

    public List<AccountRelations> fetchByRequestByAccountAndToAccount(Account requestByAccount, Account otherAccount)
    {
        return accountRelationsJpaDAO.fetchByRequestByAccountAndToAccount(requestByAccount, otherAccount);
    }

    public List<AccountRelations> fetchByAccount(String accountId)
    {
        return accountRelationsJpaDAO.fetchAccountRelations(accountId);
    }

    public AccountRelations fetchByAccountAndOtherAccount(String requestByAccountId, String otherAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(requestByAccountId), "requestByAccount Id passed should not be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(otherAccountId), "Account Id passed should not be null or empty.");
        return accountRelationsJpaDAO.fetchByAccountAndOtherAccount(requestByAccountId, otherAccountId);
    }

    public boolean isAccountRelationExist(AccountRelations accountRelation)
    {
        return accountRelationsJpaDAO.isAccountRelationExist(accountRelation);
    }

    public void createAccountBlockByKey(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.createAccountBlockByKey(accountId, requestByAccountId);
    }

    public void removeAccountFromBlockedBy(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.removeAccountFromBlockedBy(accountId, requestByAccountId);
    }

    public List<AccountRelations> fetchAccountRelationBetweenAccounts(String requestByAccountId, String otherAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(requestByAccountId), "Request account id should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(otherAccountId), "Other account is should not be null or empty");

        return accountRelationsJpaDAO.fetchAccountRelationBetweenAccounts(requestByAccountId, otherAccountId);
    }

    public AccountRelations createUpdateAccountRelation(AccountRelations relations, boolean flush)
    {
        AccountRelations accountRelation = accountRelationsJpaDAO.merge(relations);
        if (flush)
        {
            accountRelationsJpaDAO.flush();
        }
        return accountRelation;
    }

    public void callFlush()
    {
        accountRelationsJpaDAO.flush();
    }

    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList, String query)
    {
        return accountRelationsJpaDAO.fetchAccountRelations(accountId, otherAccountIdList, query);
    }

    public void persistAllAccountRelation(List<AccountRelations> relations)
    {
        accountRelationsJpaDAO.persistAll(relations);
        accountRelationsJpaDAO.flush();
    }

    public void saveOrUpdateAllAccountRelation(List<AccountRelations> relations)
    {
        accountRelationsJpaDAO.saveOrUpdateAll(relations);
        accountRelationsJpaDAO.flush();
    }
    
    public List<String> fetchContactsByAccountId(String accountId)
    {
    	return accountRelationsJpaDAO.fetchContactsByAccountId(accountId);
    }

}
