package one.group.dao.impl;

import java.util.List;

import one.group.dao.PropertySearchRelationDAO;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.jpa.dao.PropertySearchRelationJpaDAO;

public class PropertySearchRelationDAOImpl implements PropertySearchRelationDAO
{
    private PropertySearchRelationJpaDAO propertySearchRelationJpaDAO;

    public PropertySearchRelationJpaDAO getPropertySearchRelationJpaDAO()
    {
        return propertySearchRelationJpaDAO;
    }

    public void setPropertySearchRelationJpaDAO(PropertySearchRelationJpaDAO propertySearchRelationJpaDAO)
    {
        this.propertySearchRelationJpaDAO = propertySearchRelationJpaDAO;
    }

    public List<PropertySearchRelation> getMatchingClients(String propertyListingId, String accountId)
    {
        return propertySearchRelationJpaDAO.getMatchingClients(propertyListingId, accountId);

    }

    public PropertySearchRelation getSearch(String id)
    {
        return propertySearchRelationJpaDAO.findById(id);
    }

    public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation)
    {
        propertySearchRelationJpaDAO.savePropertySearchRelation(propertySearchRelation);
    }
    
    public List<PropertySearchRelation> getPropertySearchRelationsOfAccount(String accountId)
    {
        return propertySearchRelationJpaDAO.getPropertySearchRelationsOfAccount(accountId);
    }

    public List<PropertySearchRelation> getAllMatchingClientsByIds(List<PropertyListing> propertyListingIds, String accountId)
    {
        // TODO Auto-generated method stub
        return propertySearchRelationJpaDAO.getAllMatchingClientsByIds(propertyListingIds, accountId);
    }	

}
