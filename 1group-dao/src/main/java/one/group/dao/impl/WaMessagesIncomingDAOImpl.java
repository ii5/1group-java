/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.WaMessagesIncomingDAO;
import one.group.entities.jpa.WaMessagesIncoming;
import one.group.jpa.dao.WaMessagesIncomingJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesIncomingDAOImpl implements WaMessagesIncomingDAO
{
    private WaMessagesIncomingJpaDAO waMessagesIncomingJpaDAO;

    public WaMessagesIncomingJpaDAO getWaMessagesIncomingJpaDAO()
    {
        return waMessagesIncomingJpaDAO;
    }

    public void setWaMessagesIncomingJpaDAO(WaMessagesIncomingJpaDAO waMessagesIncomingJpaDAO)
    {
        this.waMessagesIncomingJpaDAO = waMessagesIncomingJpaDAO;
    }

    public List<WaMessagesIncoming> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesIncoming(WaMessagesIncoming waMessagesIncoming)
    {
        waMessagesIncomingJpaDAO.saveWaMessagesIncoming(waMessagesIncoming);
    }

    public WaMessagesIncoming fetchWaMessagesIncomingByWaMessagesIncomingId(String waMessageId)
    {
        return waMessagesIncomingJpaDAO.fetchWaMessagesIncomingByWaMessagesIncomingId(waMessageId);
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByStatus(String status)
    {
        return null;
    }

}
