package one.group.dao.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import one.group.dao.MediaDAO;
import one.group.entities.jpa.Media;
import one.group.jpa.dao.MediaJpaDAO;

public class MediaDAOImpl implements MediaDAO
{
    private MediaJpaDAO mediaJpaDAO;

    public MediaJpaDAO getMediaJpaDAO()
    {
        return mediaJpaDAO;
    }

    public void setMediaJpaDAO(MediaJpaDAO mediaJpaDAO)
    {
        this.mediaJpaDAO = mediaJpaDAO;
    }

    public Media addMedia(Media media)
    {
        return mediaJpaDAO.updateMedia(media);
    }

    public void removeMedia(String entityType, Map<String, Object> parameters) throws IOException
    {
        mediaJpaDAO.removeMedia(entityType, parameters);
    }

    public Media fetchByMediaId(String mediaId)
    {
        return mediaJpaDAO.fetchByMediaId(mediaId);
    }
    
    public List<Media> fetchMediaByIds(Collection<String> mediaIdCollection) 
    {
    	return mediaJpaDAO.fetchMediaByIds(mediaIdCollection);
    }

}
