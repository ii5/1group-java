/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.WaQrcodesDAO;
import one.group.entities.jpa.WaQrcodes;

/**
 * @author ashishthorat
 *
 */
public class WaQrcodesDAOImpl implements WaQrcodesDAO
{

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentServerId(String currentServerId)
    {
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentClientId(String currentClientId)
    {
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByStatus(String status)
    {
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentCrmMobile(String currentCrmMobile)
    {
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByWaGroupsApprovedStatus(String waGroupsApprovedStatus)
    {
        return null;
    }

}
