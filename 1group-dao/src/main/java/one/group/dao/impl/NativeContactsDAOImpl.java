package one.group.dao.impl;

import java.util.List;
import java.util.Set;

import one.group.core.enums.status.Status;
import one.group.dao.NativeContactsDAO;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.jpa.NativeContacts;
import one.group.jpa.dao.NativeContactsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class NativeContactsDAOImpl implements NativeContactsDAO
{

    NativeContactsJpaDAO nativeContactsJpaDAO;

    public NativeContactsJpaDAO getNativeContactsJpaDAO()
    {
        return nativeContactsJpaDAO;
    }

    public void setNativeContactsJpaDAO(NativeContactsJpaDAO nativeContactsJpaDAO)
    {
        this.nativeContactsJpaDAO = nativeContactsJpaDAO;
    }

    public List<NativeContacts> fetchAllNativeContactsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return nativeContactsJpaDAO.fetchAllNativeContactsByAccountId(accountId);
    }

    public List<String> fetchNativeContactsByPhoneNumber(String phoneNumber, Status status)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(phoneNumber), "Phone Number should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(status.toString()), "Status should not be null or empty");

        return nativeContactsJpaDAO.fetchNativeContactsByPhoneNumber(phoneNumber, status);
    }

    public List<WSNativeContact> fetchAllNativeContactsByAccountIdFiltered(String accountId, Set<String> registeredPhoneNumbers, int offset, int limit)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return nativeContactsJpaDAO.fetchAllNativeContactsByAccountIdFiltered(accountId, registeredPhoneNumbers, offset, limit);
    }

    public int fetchAllNativeContactsByAccountIdCount(String accountId, Set<String> registeredPhoneNumbers)
    {
        return nativeContactsJpaDAO.fetchAllNativeContactsByAccountIdCount(accountId, registeredPhoneNumbers);
    }

}
