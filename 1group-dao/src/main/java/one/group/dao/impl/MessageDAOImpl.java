package one.group.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.request.MapSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.MessageDAO;
import one.group.entities.jpa.Messages;
import one.group.entities.socket.Message;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.jpa.dao.MessagesJpaDAO;
import one.group.solr.dao.MessageSolrDAO;
import one.group.solr.dao.impl.MessageSolrDAOImpl;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class MessageDAOImpl implements MessageDAO
{
    private MessageSolrDAO messageSolrDAO = new MessageSolrDAOImpl();
    private MessagesJpaDAO messageJpaDAO;
    private Logger logger = LoggerFactory.getLogger(MessageDAOImpl.class);
    
    public MessagesJpaDAO getMessageJpaDAO() 
    {
		return messageJpaDAO;
	}
    
    public void setMessageJpaDAO(MessagesJpaDAO messageJpaDAO) 
    {
		this.messageJpaDAO = messageJpaDAO;
	}

    public MessageSolrDAO getMessageSolrDAO()
    {
        return messageSolrDAO;
    }

    public void setMessageSolrDAO(MessageSolrDAO messageSolrDAO)
    {
        this.messageSolrDAO = messageSolrDAO;
    }

    public Set<Message> fetchMessagesOfAccount(String accountId)
    {
        Map<String, String> params = new HashMap<String, String>();
        // params.put("q", ")
        // SolrParams sParams = new MapSolrParams(params);

        return null;
    }

    public Map<String, Integer> fetchMessageCountByGroupId(boolean isDuplicate, List<BpoMessageStatus> bpoMessageStatusList, List<MessageStatus> messageStatusList)
    {

        Map<String, String> params = new HashMap<String, String>();
        Map<String, Integer> responseMap = new HashMap<String, Integer>();
        String q = "";
        if (bpoMessageStatusList != null && !bpoMessageStatusList.isEmpty())
        {
            String bpoMessageStatusListString = "bpoMessageStatus_s:(" + Utils.getCollectionAsString(bpoMessageStatusList, " ") + ")";
            q = q + " AND " + bpoMessageStatusListString;
        }

        if (messageStatusList != null && !messageStatusList.isEmpty())
        {
            String messageStatusListString = "messageStatus_s:(" + Utils.getCollectionAsString(messageStatusList, " ") + ")";
            q = q + " AND " + messageStatusListString + "  AND ";
        }

        q = q + "isDuplicate:" + isDuplicate;

        params.put("q", q);
        params.put("facet", "on");
        params.put("fl", "id");
        params.put("facet.field", "groupId");
        params.put("facet.limit", "" + Integer.MAX_VALUE);
        params.put("rows", "0");

        try
        {
            QueryResponse response = messageSolrDAO.getClient().query(new MapSolrParams(params));
            FacetField field = response.getFacetField("groupId");

            if (field != null)
            {
                for (Count c : field.getValues())
                {
                    responseMap.put(c.getName(), Long.valueOf(c.getCount()).intValue());
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return responseMap;
    }

    public List<Message> fetchMessagesByStatusAndGroupIds(List<MessageStatus> messageStatusList, List<GroupStatus> groupStatusList, List<String> groupIdList,
            List<BpoMessageStatus> bpoMessageStatusList, String bpoMessageUpdateTime, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows, List<String> fields)
    {
        return messageSolrDAO.fetchMessagesByStatusAndGroupIds(messageStatusList, groupStatusList, groupIdList, bpoMessageStatusList, bpoMessageUpdateTime, sort, filter, start, rows, fields);
    }

    public void saveMessage(Message message)
    {
        messageSolrDAO.saveMessage(message);
    }

    public List<Message> fetchLastMessageOfGroup(String groupId)
    {
        Validation.isTrue(!Utils.isNull(groupId), "Group id should not be null");
        return messageSolrDAO.fetchLastMessageOfGroup(groupId);
    }

    public List<Message> fetchPhonecallTypeMessagesOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account is should not be null");
        return messageSolrDAO.fetchPhonecallTypeMessagesOfAccount(accountId);
    }

    public Message fetchMessageFromId(String messageId)
    {
        Validation.isTrue(!Utils.isNull(messageId), "Message id should not be null");
        return messageSolrDAO.fetchMessageFromId(messageId);
    }

    public List<Message> fetchMessagesBySearchCriteria()
    {
        return messageSolrDAO.fetchMessagesBySearchCriteria();

    }

    public static void main(String[] args)
    {
        long start = System.currentTimeMillis();
        MessageDAOImpl dao = new MessageDAOImpl();
        dao.setMessageSolrDAO(new MessageSolrDAOImpl());
        System.out.println(dao.fetchMessageCountByGroupId(false, null, null));
        long end = System.currentTimeMillis();

        System.out.println(end - start);
    }

    public List<Message> fetchMessageByBroadcastIds(List<String> broadcastIds)
    {
        return messageSolrDAO.fetchMessageByBroadcastIds(broadcastIds);
    }

    public List<Message> fetchSharedMessagesByType(String accountId, String groupId)
    {
        return messageSolrDAO.fetchSharedMessagesByType(accountId, groupId);
    }

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType)
    {

        return messageSolrDAO.fetchMessagesByAccountIdAndMessageSourceType(accountId, messageSourceType);
    }

    public List<Message> fetchMessagesByGroupIdAndSourceAndType(String groupId, MessageSourceType messageSourceType, List<MessageType> messageTypeList, int offset, int limit)
    {
        Validation.isTrue(!Utils.isNull(groupId), "Group id should not be null");
        Validation.isTrue(!Utils.isNull(messageSourceType), "Message source type should not be null");
        return messageSolrDAO.fetchMessagesByGroupIdAndSourceAndType(groupId, messageSourceType, messageTypeList, offset, limit);
    }

    public List<Message> fetchMessageByBroadcastIdsAndStatusAndSource(List<String> broadcastIdsList, List<MessageStatus> messageStatusList, List<MessageSourceType> messageSourceTypeList,
            MessageType messageType)
    {
        Validation.isTrue(!Utils.isNull(broadcastIdsList), "Group id should not be null");
        Validation.isTrue(!Utils.isNull(messageStatusList), "Message status should not be null");
        Validation.isTrue(!Utils.isNull(messageSourceTypeList), "Message source type should not be null");
        return messageSolrDAO.fetchMessageByBroadcastIdsAndStatusAndSource(broadcastIdsList, messageStatusList, messageSourceTypeList, messageType);
    }

    public List<String> fetchMessageByBroadcastIdsAndSourceAndAccount(List<String> accountIdList, List<String> broadcastIdsList, MessageSourceType messageSourceType, MessageStatus messageStatus)
    {
        Validation.isTrue(!Utils.isNull(accountIdList), "Acount id list should not be null");
        Validation.isTrue(!Utils.isNull(broadcastIdsList), "Broadcast id should not be null");
        Validation.isTrue(!Utils.isNull(messageSourceType), "Message source type should not be null");
        Validation.isTrue(!Utils.isNull(messageStatus), "Message status should not be null");
        return messageSolrDAO.fetchMessageByBroadcastIdsAndSourceAndAccount(accountIdList, broadcastIdsList, messageSourceType, messageStatus);
    }

    public List<Message> fetchMessagesFromIds(List<String> messageIds, List<String> fields)
    {
        Validation.isTrue(!Utils.isNull(messageIds), "Message ids should not be null");
        return messageSolrDAO.fetchMessagesFromIds(messageIds, fields);
    }
    
    public List<Message> fetchMessagesFromIdsWithException(List<String> messageIds, List<String> fields) throws Abstract1GroupException
    {
        Validation.isTrue(!Utils.isNull(messageIds), "Message ids should not be null");
        return messageSolrDAO.fetchMessagesFromIdsWithException(messageIds, fields);
    }

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType, int offset, int limit)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Acount id should not be null");
        Validation.isTrue(!Utils.isNull(messageSourceType), "Message source type  should not be null");
        Validation.isTrue(!Utils.isNull(offset), "Offset source type should not be null");
        Validation.isTrue(!Utils.isNull(limit), "Limit should not be null");
        return messageSolrDAO.fetchMessagesByAccountIdAndMessageSourceType(accountId, messageSourceType, offset, limit);
    }

    public boolean updateMessageStatus(List<String> messageIds, MessageStatus messageStatus, BpoMessageStatus bpoMessageStatus, String updatedBy, String bpoUpdatedBy)
    {
        Validation.isTrue(!Utils.isNull(messageIds), "Message ids should not be null");
        Validation.isTrue(!Utils.isNull(updatedBy), "Update by user should not be null");
        return messageSolrDAO.updateMessageStatus(messageIds, messageStatus, bpoMessageStatus, updatedBy, bpoUpdatedBy);
    }

    public List<Message> fetchAllMessagesByAccountIdAndMessageSourceTypeList(String accountId, List<MessageType> messageTypeList, List<MessageSourceType> messageSourceTypeList, boolean latestToOldest)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Acount id should not be null");
        Validation.isTrue(!Utils.isNull(messageSourceTypeList), "Message source type list should not be null");
        Validation.isTrue(!Utils.isNull(messageTypeList), "Message type list should not be null");

        return messageSolrDAO.fetchAllMessagesByAccountIdAndMessageSourceTypeList(accountId, messageTypeList, messageSourceTypeList, latestToOldest);
    }

    public void updateMessageStatusBybroadcastId(String broadcastId, BroadcastStatus broadcastStatus, String accountId)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "Broadcast id should not be null");
        Validation.isTrue(!Utils.isNull(broadcastStatus), "BroadcastStatus should not be null");
        Validation.isTrue(!Utils.isNull(accountId), "accountId should not be null");
        messageSolrDAO.updateMessageStatusBybroadcastId(broadcastId, broadcastStatus, accountId);

    }

    public long fetchUnreadMessageCount(String groupId)
    {
        return messageSolrDAO.fetchUnreadMessageCount(groupId);
    }

    public long fetchTotalMessageCount(String groupId)
    {
        return messageSolrDAO.fetchTotalMessageCount(groupId);
    }

    public List<Message> fetchMessagesByBroadcastIdAndMessageStatus(String broadcastId, MessageStatus messageStatus)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "Broadcast id should not be null");
        Validation.isTrue(!Utils.isNull(messageStatus), "BroadcastStatus should not be null");
        return messageSolrDAO.fetchMessagesByBroadcastIdAndMessageStatus(broadcastId, messageStatus);

    }

    public void saveMessages(Collection<Message> messageList)
    {
        messageSolrDAO.saveMessages(messageList);
    }
    
    public void saveMessagesToDB(List<Message> messageList)
    {
    	Collection<Messages> messagesList = new ArrayList<Messages>();
    	if (messageList == null) return;
    	
    	for (Message m : messageList)
    	{
    		Messages ms = new Messages(m);
    		messagesList.add(ms);
    	}
    	
        try
        {
        	messageJpaDAO.saveMessages(messagesList);
        }
        catch (Exception e)
        {
        	logger.error("Exception while persisting wa sync messages in db.", e.getMessage());
        }
    }

    public List<String> fetchBroadcastIdsByMessageCreator(Set<String> mobileNumber)
    {
        return messageSolrDAO.fetchBroadcastIdsByMessageCreator(mobileNumber);
    }

    public Map<String, Map<String, Object>> fetchLatestMessageBasedOnGroupQuery(Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, boolean useGroup, SortField groupSortField,
            SortType groupSortType, String groupField, List<String> fields, int start, int rows)
    {

        return messageSolrDAO.fetchLatestMessageBasedOnGroupQuery(sort, filter, useGroup, groupSortField, groupSortType, groupField, fields, start, rows);
    }

    public long countAllMessagesByAccountIdForMeAndWhatsApp(String accountId)
    {
        return messageSolrDAO.countAllMessagesByAccountIdForMeAndWhatsApp(accountId);
    }

    public List<Message> fetchMessagesByForMeAndWhatsappByAccountId(String accountId, int offset, int limit)
    {
        return messageSolrDAO.fetchMessagesByForMeAndWhatsappByAccountId(accountId, offset, limit);
    }

    public List<Message> fetchLastMessageOfGroupNotTypePhonecall(String groupId)
    {
        Validation.isTrue(!Utils.isNull(groupId), "Group id should not be null");
        return messageSolrDAO.fetchLastMessageOfGroupNotTypePhonecall(groupId);
    }

    public Map<String, Long> fetchBroadcastMessageCountByAccountId(List<String> accountIdList)
    {
        return messageSolrDAO.fetchBroadcastMessageCountByAccountId(accountIdList);
    }

    public List<Message> fetchMessageByBroadcastIdAndSourceAndStatus(Collection<String> broadcastIdList, List<MessageSourceType> messageSourceList, List<MessageStatus> messageStatusList)
    {
        return messageSolrDAO.fetchMessageByBroadcastIdAndSourceAndStatus(broadcastIdList, messageSourceList, messageStatusList);
    }
    
    public void update(List<Message> messageList) 
    {
    	messageSolrDAO.update(messageList);
    }
    
    public void update(Message message) 
    {
    	messageSolrDAO.update(Arrays.asList(message));
    }

}
