package one.group.dao.impl;

import java.util.List;

import one.group.dao.SMSMarketingLogDAO;
import one.group.entities.jpa.SmsMarketingLog;
import one.group.jpa.dao.SMSMarketingLogJpaDAO;

public class SMSMarketingLogDAOImpl implements SMSMarketingLogDAO
{
    private SMSMarketingLogJpaDAO smsMarketingLogJpaDAO;

    public SMSMarketingLogJpaDAO getSmsMarketingLogJpaDAO()
    {
        return smsMarketingLogJpaDAO;
    }

    public void setSmsMarketingLogJpaDAO(SMSMarketingLogJpaDAO smsMarketingLogJpaDAO)
    {
        this.smsMarketingLogJpaDAO = smsMarketingLogJpaDAO;
    }

    public void saveSMSMarketingLog(SmsMarketingLog smsMarkingLog)
    {
        smsMarketingLogJpaDAO.saveSMSMarkingLog(smsMarkingLog);
    }

    public List<SmsMarketingLog> fetchAllLog()
    {
        return smsMarketingLogJpaDAO.fetchAllLog();
    }
}
