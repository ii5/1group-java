/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.LocationErrorTempDAO;
import one.group.entities.jpa.LocationErrorTmp;
import one.group.jpa.dao.LocationErrorTempJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class LocationErrorTempDAOImpl implements LocationErrorTempDAO
{
    private LocationErrorTempJpaDAO locationErrorTempJpaDAO;

    public LocationErrorTempJpaDAO getLocationErrorTempJpaDAO()
    {
        return locationErrorTempJpaDAO;
    }

    public void setLocationErrorTempJpaDAO(LocationErrorTempJpaDAO locationErrorTempJpaDAO)
    {
        this.locationErrorTempJpaDAO = locationErrorTempJpaDAO;
    }

    public List<LocationErrorTmp> fetchLocationErrorTmpByCityId(String cityId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrorTmpByParentId(String parentId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrortmpByLoginUserID(String loginUserId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrorTmpByStatus(String status)
    {
        return null;
    }

    public void saveLocationErrorTmp(LocationErrorTmp locationErrorTmp)
    {

    }

    public LocationErrorTmp fetchLocationErrorTmpById(String locationErrorTmpId)
    {
        return locationErrorTempJpaDAO.fetchLocationErrorTmpById(locationErrorTmpId);
    }

}
