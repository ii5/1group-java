package one.group.dao.impl;

import java.util.List;

import one.group.core.enums.status.NotificationStatus;
import one.group.dao.NotificationDAO;
import one.group.entities.jpa.NotificationTemplate;
import one.group.jpa.dao.NotificationJpaDAO;
import one.group.jpa.dao.NotificationTemplateJpaDAO;

public class NotificationDAOImpl implements NotificationDAO
{
    private NotificationJpaDAO notificationJpaDAO;

    private NotificationTemplateJpaDAO notificationTemplateJpaDAO;

    public NotificationJpaDAO getNotificationJpaDAO()
    {
        return notificationJpaDAO;
    }

    public void setNotificationJpaDAO(NotificationJpaDAO notificationJpaDAO)
    {
        this.notificationJpaDAO = notificationJpaDAO;
    }

    public NotificationTemplateJpaDAO getNotificationTemplateJpaDAO()
    {
        return notificationTemplateJpaDAO;
    }

    public void setNotificationTemplateJpaDAO(NotificationTemplateJpaDAO notificationTemplateJpaDAO)
    {
        this.notificationTemplateJpaDAO = notificationTemplateJpaDAO;
    }

    public long fetchCountByStatus(String userId, NotificationStatus status)
    {
        return notificationJpaDAO.fetchCountByStatus(userId, status);
    }

    public List<NotificationTemplate> fetchNotificationTemplates()
    {
        return notificationTemplateJpaDAO.fetchNotificationTemplates();
    }

}
