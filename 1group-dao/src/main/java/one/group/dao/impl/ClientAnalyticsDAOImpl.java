package one.group.dao.impl;

import java.util.List;

import one.group.dao.ClientAnalyticsDAO;
import one.group.entities.jpa.ClientAnalytics;
import one.group.jpa.dao.ClientAnalyticsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class ClientAnalyticsDAOImpl implements ClientAnalyticsDAO
{
    private ClientAnalyticsJpaDAO clientAnalyticsJpaDAO;

    public ClientAnalyticsJpaDAO getClientAnalyticsJpaDAO()
    {
        return clientAnalyticsJpaDAO;
    }

    public void setClientAnalyticsJpaDAO(ClientAnalyticsJpaDAO clientAnalyticsJpaDAO)
    {
        this.clientAnalyticsJpaDAO = clientAnalyticsJpaDAO;
    }

    public void saveclientAnalytics(ClientAnalytics clientAnalyticsObj)
    {
        clientAnalyticsJpaDAO.saveclientAnalytics(clientAnalyticsObj);
    }

    public List<ClientAnalytics> fetchTracesByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        return clientAnalyticsJpaDAO.fetchTracesByAccountId(accountId);
    }
}
