/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.StateDAO;
import one.group.entities.jpa.State;
import one.group.jpa.dao.StateJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class StateDAOImpl implements StateDAO
{
    private StateJpaDAO stateJpaDAO;

    public StateJpaDAO getStateJpaDAO()
    {
        return stateJpaDAO;
    }

    public void setStateJpaDAO(StateJpaDAO stateJpaDAO)
    {
        this.stateJpaDAO = stateJpaDAO;
    }

    public State getStateByCode(String code)
    {
        return null;
    }

    public State getStateByName(String name)
    {
        return null;
    }

    public State fetchStateById(String stateId)
    {
        return stateJpaDAO.fetchStateById(stateId);

    }

    public void saveState(State state)
    {
        stateJpaDAO.saveState(state);

    }

}
