package one.group.dao.impl;

import one.group.dao.OtpDAO;
import one.group.entities.jpa.Otp;
import one.group.jpa.dao.OtpJpaDAO;

public class OtpDAOImpl implements OtpDAO
{

    private OtpJpaDAO otpJpaDAO;

    public OtpJpaDAO getOtpJpaDAO()
    {
        return otpJpaDAO;
    }

    public void setOtpJpaDAO(OtpJpaDAO otpJpaDAO)
    {
        this.otpJpaDAO = otpJpaDAO;
    }

    public Otp fetchOtpFromOtpAndPhoneNumber(String otp, String mobileNumber)
    {
        return otpJpaDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);
    }

    public void saveOTP(Otp otp)
    {
        otpJpaDAO.persist(otp);
    }

    public void deleteOtp(Otp otp)
    {
        otpJpaDAO.deleteOtp(otp);

    }

}