package one.group.dao.impl;

import one.group.cache.dao.SyncGroupCacheDAO;
import one.group.core.Constant;
import one.group.dao.SyncGroupDAO;
import one.group.entities.jpa.Configuration;
import one.group.jpa.dao.ConfigurationJpaDAO;

public class SyncGroupDAOImpl implements SyncGroupDAO
{
    private SyncGroupCacheDAO syncGroupCacheDAO;

    private ConfigurationJpaDAO configurationJpaDAO;

    public SyncGroupCacheDAO getSyncGroupCacheDAO()
    {
        return syncGroupCacheDAO;
    }

    public void setSyncGroupCacheDAO(SyncGroupCacheDAO syncGroupCacheDAO)
    {
        this.syncGroupCacheDAO = syncGroupCacheDAO;
    }

    public void addCount(String count)
    {
        syncGroupCacheDAO.addCount(count);

    }

    public ConfigurationJpaDAO getConfigurationJpaDAO()
    {
        return configurationJpaDAO;
    }

    public void setConfigurationJpaDAO(ConfigurationJpaDAO configurationJpaDAO)
    {
        this.configurationJpaDAO = configurationJpaDAO;
    }

    public Integer getCount()
    {
        return null;
    }

    public String fetchGroupContent(String groupkey)
    {
        return syncGroupCacheDAO.fetchGroupContent(groupkey);
    }

    public boolean isFullRelease()
    {
        Configuration configuration = configurationJpaDAO.getConfiguationByKey(Constant.FULL_RELEASE);
        return Boolean.valueOf(configuration.getValue());
    }

}
