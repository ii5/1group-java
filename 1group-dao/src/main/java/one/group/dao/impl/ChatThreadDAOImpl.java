package one.group.dao.impl;

import java.util.Map;
import java.util.Set;

import one.group.cache.dao.ChatThreadCacheDAO;
import one.group.core.Constant;
import one.group.dao.ChatThreadDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class ChatThreadDAOImpl implements ChatThreadDAO
{
    private ChatThreadCacheDAO chatThreadCacheDAO;

    public ChatThreadCacheDAO getChatThreadCacheDAO()
    {
        return chatThreadCacheDAO;
    }

    public void setChatThreadCacheDAO(final ChatThreadCacheDAO chatThreadCacheDAO)
    {
        this.chatThreadCacheDAO = chatThreadCacheDAO;
    }

    public Set<String> fetchChatThreadListOfAccountByActivity(final String accountId, final int offset, final int limit)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNull(offset), "offset should not be null");
        Validation.isTrue(!Utils.isNull(limit), "limit should not be null");

        return chatThreadCacheDAO.fetchChatThreadListOfAccountByActivity(accountId, offset, limit);
    }

    public Set<String> fetchChatThreadListOfAccountByJoined(final String accountId, final int offset, final int limit)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNull(offset), "offset should not be null");
        Validation.isTrue(!Utils.isNull(limit), "limit should not be null");

        return chatThreadCacheDAO.fetchChatThreadListOfAccountByJoined(accountId, offset, limit);
    }

    public Double fetchChatThreadScore(final String chatThreadId, final String accountId, final String indexBy)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(indexBy), "Index by should not be null");

        return chatThreadCacheDAO.fetchChatThreadScore(chatThreadId, accountId, indexBy);
    }

    public void saveChatThreadByActivity(final String chatThreadId, final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        chatThreadCacheDAO.saveChatThreadByActivity(chatThreadId, accountId);
    }

    public void saveChatThreadByJoined(final String chatThreadId, final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        chatThreadCacheDAO.saveChatThreadByJoined(chatThreadId, accountId);
    }

    public void updateChatThreadScore(final String chatThreadId, final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        chatThreadCacheDAO.saveChatThreadByActivity(chatThreadId, accountId);
        chatThreadCacheDAO.saveChatThreadByJoined(chatThreadId, accountId);
    }

    public void updateChatThreadParticipants(final String chatThreadId, final String fromAccountId, final String toAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(fromAccountId), "From account id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(toAccountId), "To account id should not be null");

        chatThreadCacheDAO.updateChatThreadParticipants(chatThreadId, Constant.REDIS_HASHKEY_FROM_ACCOUNT_ID, fromAccountId);
        chatThreadCacheDAO.updateChatThreadParticipants(chatThreadId, Constant.REDIS_HASHKEY_TO_ACCOUNT_ID, toAccountId);
    }

    public Map<String, String> getChatThreadParticipants(final String chatThreadId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");

        return chatThreadCacheDAO.getChatThreadParticipants(chatThreadId);
    }

    public long getChatThreadIndexByActivity(final String accountId, final String chatThreadId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.getChatThreadIndexByActivity(accountId, chatThreadId);
    }

    public long getChatThreadIndexByJoined(final String accountId, final String chatThreadId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.getChatThreadIndexByJoined(accountId, chatThreadId);
    }

    public void updateChatThreadCursorReadIndex(String chatThreadId, String accountId, String index)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(index), "Index should not be null");

        chatThreadCacheDAO.updateChatThreadCursorReadIndex(chatThreadId, accountId, index);
    }

    public void updateChatThreadCursorReceivedIndex(String chatThreadId, String accountId, String index)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(index), "Index should not be null");

        chatThreadCacheDAO.updateChatThreadCursorReceivedIndex(chatThreadId, accountId, index);
    }

    public String fetchChatThreadCursorReadIndex(String chatThreadId, String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId);
    }

    public String fetchChatThreadCursorReceivedIndex(String chatThreadId, String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId);
    }

    public long fetchChatThreadCountByActivity(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.fetchChatThreadCountByActivity(accountId);
    }

    public long fetchChatThreadCountByJoined(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        return chatThreadCacheDAO.fetchChatThreadCountByJoined(accountId);
    }

    public void saveChatThreadForPropertyListing(String chatThreadId, String propertyListingId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(propertyListingId), "Property listing id should not be null");

        chatThreadCacheDAO.saveChatThreadForPropertyListing(chatThreadId, propertyListingId);
    }

    public Set<String> fetchChatThreadByProperty(String propertyListingId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(propertyListingId), "Property listing id should not be null");

        return chatThreadCacheDAO.fetchChatThreadByPropertyListing(propertyListingId);
    }

    public Object updateChatThreadCursor(String chatThreadId, String accountId, String receivedIndex, String readIndex)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Chat thread id should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(receivedIndex), "Received indexc should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(readIndex), "Read index should not be null or empty");
        Validation.isTrue(Integer.valueOf(receivedIndex) > -2, "Received index should be > -1");
        Validation.isTrue(Integer.valueOf(readIndex) > -2, "Read index should be > -1");
        Validation.isTrue(!(Integer.valueOf(readIndex) > Integer.valueOf(receivedIndex)), "Invalid value of receivedIndex. [" + readIndex + "]");
        return chatThreadCacheDAO.updateChatThreadCursor(chatThreadId, accountId, receivedIndex, readIndex);
    }

    public Set<String> fetchAllChatThreads(String pattern)
    {
        return chatThreadCacheDAO.fetchAllChatThreads(pattern);
    }

}
