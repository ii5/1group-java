package one.group.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.dao.SearchDAO;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Search;
import one.group.jpa.dao.SearchJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class SearchDAOImpl implements SearchDAO
{
    private SearchJpaDAO searchJpaDAO;

    public SearchJpaDAO getSearchJpaDAO()
    {
        return searchJpaDAO;
    }

    public void setSearchJpaDAO(SearchJpaDAO searchJpaDAO)
    {
        this.searchJpaDAO = searchJpaDAO;
    }

    public void saveSearch(Search search)
    {
        searchJpaDAO.saveSearch(search);
    }

    public List<Search> fetchSearchByLocationList(List<String> locationIdList)
    {
        Validation.notNull(locationIdList, "Location Id list passed should not be null.");

        return searchJpaDAO.fetchSearchByLocationList(locationIdList);
    }

    public Search fetchSearchByRequirementName(String requirementName, String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(requirementName), "Requirement name passed should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty");

        return searchJpaDAO.fetchSearchByRequirementName(requirementName, accountId);
    }

    public Search fetchSearchByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null");

        return searchJpaDAO.fetchSearchByAccountId(accountId);
    }

    public List<Search> fetchSearchByAccountIds(List<String> accountIds)
    {
        Validation.notNull(accountIds, "Account Ids passed should not be null.");

        return searchJpaDAO.fetchSearchByAccountIds(accountIds);
    }

    public List<Search> getMatchingSearchIds(String accountId, List<Location> locations, String transactionType, String propertyMarket, String rooms, List<String> amenities, int area, int rentPrice,
            int salePrice, String propertyType, String propertySubType)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty");
        Validation.isTrue(!Utils.isNull(locations), "Location passed should not be null");
        // Validation.isTrue(!Utils.isNullOrEmpty(transactionType),
        // "Transaction type passed should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(propertyMarket), "Property Market passed should not be null or empty");

        return searchJpaDAO.getMatchingSearchIds(accountId, locations, transactionType, propertyMarket, rooms, amenities, area, rentPrice, salePrice, propertyType, propertySubType);
    }

    public Search fetchSearchBySearchId(String searchId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(searchId), "Search Id passed should not be null");

        return searchJpaDAO.fetchSearchBySearchId(searchId);
    }

    public List<Search> fetchAllSearchBySearchIds(List<String> searchIds)
    {
        Validation.notNull(searchIds, "Search Id List passed should not be null.");

        return searchJpaDAO.fetchAllSearchBySearchIds(searchIds);
    }

    public void deleteSearch(Search search)
    {
        Validation.isTrue(!Utils.isNull(search), "Search passed should not be null");

        searchJpaDAO.deleteSearch(search);
    }

    public List<Search> fetchRequirementSearchByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id passed should not be null");

        return searchJpaDAO.fetchRequirementSearchByAccountId(accountId);
    }

    public List<Search> fetchAllExpiredRequirementSearch(Date expiryTime)
    {
        Validation.isTrue(!Utils.isNull(expiryTime), "Expiry time passed should not be null");

        return searchJpaDAO.fetchAllExpiredRequirementSearch(expiryTime);
    }

    public Search fetchLatestSearchByAccountId(String accountId) throws NoResultException
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account id passed should not be null");
        return searchJpaDAO.fetchLatestSearchByAccountId(accountId);

    }

    public List<Search> getMatchingSearches(String locationId, String cityId, long minPrice, long maxPrice, long minSize, long maxsize, long price, long size, PropertyType propertyType,
            PropertyTransactionType transactionType, Rooms rooms, BroadcastType broadcastType, PropertySubType propertySubType)
    {
        return searchJpaDAO.getMatchingSearches(locationId, cityId, minPrice, maxPrice, minSize, maxsize, price, size, propertyType, transactionType, rooms, broadcastType, propertySubType);
    }

}
