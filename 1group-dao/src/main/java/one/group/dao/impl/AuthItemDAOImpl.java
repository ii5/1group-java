/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.AuthItemDAO;
import one.group.entities.jpa.AuthItem;
import one.group.jpa.dao.AuthItemJpaDAO;

/**
 * @author ashishthorat
 *
 */

public class AuthItemDAOImpl implements AuthItemDAO
{

    private AuthItemJpaDAO authItemJpaDAO;

    public AuthItemJpaDAO getAuthItemJpaDAO()
    {
        return authItemJpaDAO;
    }

    public void setAuthItemJpaDAO(AuthItemJpaDAO authItemJpaDAO)
    {
        this.authItemJpaDAO = authItemJpaDAO;
    }

    public List<AuthItem> fetchAuthItemByType(String type)
    {
        return null;
    }

    public AuthItem fetchAuthItemByRuleName(String ruleName)
    {
        return null;
    }

    public AuthItem fetchAuthItemByGroupCode(String groupCode)
    {
        return null;
    }

    public void saveAuthItem(AuthItem authItem)
    {

    }

    public Collection<AuthItem> fetchAuthItemByNameList(Collection<String> nameList)
    {
        return authItemJpaDAO.fetchAuthItemByNameList(nameList);
    }
}
