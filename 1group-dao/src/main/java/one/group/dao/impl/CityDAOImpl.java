package one.group.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.Status;
import one.group.dao.CityDAO;
import one.group.entities.jpa.City;
import one.group.entities.jpa.helpers.BPOCityLocationData;
import one.group.jpa.dao.CityJpaDAO;

public class CityDAOImpl implements CityDAO
{
    private CityJpaDAO cityJpaDAO;

    /**
     * @return the cityJpaDAO
     */
    public CityJpaDAO getCityJpaDAO()
    {
        return cityJpaDAO;
    }

    /**
     * @param cityJpaDAO
     *            the cityJpaDAO to set
     */
    public void setCityJpaDAO(CityJpaDAO cityJpaDAO)
    {
        this.cityJpaDAO = cityJpaDAO;
    }

    public List<City> getAllCities()
    {
        return cityJpaDAO.getAllCities();
    }

    public City getCityById(String cityId)
    {
        return cityJpaDAO.findById(cityId);
    }

    public List<City> getAllCitiesByCityIds(Collection<String> cityIds)
    {
        if (cityIds.isEmpty())
        {
            return new ArrayList<City>();
        }
        return cityJpaDAO.getAllCitiesByCityIds(cityIds);
    }

    public List<BPOCityLocationData> getAllCityLocations(List<String> cityIds, List<Status> locationStatusList)
    {
        return cityJpaDAO.getAllCityLocations(cityIds, locationStatusList);
    }

    public List<City> getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField,SortType> sort, Map<FilterField,Set<String>> filter, int start, int rows)
    {
        return cityJpaDAO.getAllCities(cityIdList, isPublishedList, sort, filter, start, rows);
    }

    public List<City> getAllCities(List<Boolean> isPublishedList)
    {
        return cityJpaDAO.getAllCities(isPublishedList);
    }
}
