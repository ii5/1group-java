package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.core.enums.status.Status;
import one.group.dao.LocationDAO;
import one.group.entities.jpa.Location;
import one.group.jpa.dao.LocationJpaDAO;

public class LocationDAOImpl implements LocationDAO
{

    private LocationJpaDAO locationJpaDAO;

    public LocationJpaDAO getLocationJpaDAO()
    {
        return locationJpaDAO;
    }

    public void setLocationJpaDAO(LocationJpaDAO locationJpaDAO)
    {
        this.locationJpaDAO = locationJpaDAO;
    }

    public Location fetchById(String locationID)
    {
        return locationJpaDAO.findById(locationID);
    }

    public Collection<Location> getAllLocations()
    {
        return locationJpaDAO.getAllLocations();
    }

    public void updateLiveLocationTable(String cityId)
    {

        locationJpaDAO.updateLiveLocationTable(cityId);
    }

    public List<Location> fetchLocalitiesOfCityById(String cityId)
    {
        return locationJpaDAO.fetchLocalitiesOfCityById(cityId);
    }

    public List<Location> fetchAllLocations(Collection<String> locationIdList)
    {
        return locationJpaDAO.fetchAllLocations(locationIdList);
    }

    public List<Location> fetchAllLocationsByCityIds(List<String> cityIds)
    {
        return locationJpaDAO.fetchAllLocationsByCityIds(cityIds);
    }

    public List<Location> fetchAllLocationByStatus(List<Status> statusList)
    {
        return locationJpaDAO.fetchAllLocationByStatus(statusList);
    }
}
