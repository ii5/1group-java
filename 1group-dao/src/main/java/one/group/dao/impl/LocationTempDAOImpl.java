/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.LocationTempDAO;
import one.group.entities.jpa.LocationTmp;
import one.group.jpa.dao.LocationTempJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class LocationTempDAOImpl implements LocationTempDAO
{

    private LocationTempJpaDAO locationTempJpaDAO;

    public LocationTempJpaDAO getLocationTempJpaDAO()
    {
        return locationTempJpaDAO;
    }

    public void setLocationTempJpaDAO(LocationTempJpaDAO locationTempJpaDAO)
    {
        this.locationTempJpaDAO = locationTempJpaDAO;
    }

    public List<LocationTmp> fetchLocationTmpByCityId(String cityId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByParentId(String parentId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByLoginUserID(String loginUserId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByStatus(String status)
    {
        return null;
    }

    public void saveLocationTmp(LocationTmp locationTmp)
    {

    }

    public LocationTmp fetchLocationTmpByLocationTmpId(String locationTmpId)
    {

        return locationTempJpaDAO.fetchLocationTmpByLocationTmpId(locationTmpId);
    }

}
