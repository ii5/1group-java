/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.StarredBroadcastDAO;
import one.group.entities.jpa.StarredBroadcast;
import one.group.jpa.dao.StarredBroadcastJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class StarredBroadcastDAOImpl implements StarredBroadcastDAO
{
    private StarredBroadcastJpaDAO starredBroadcastJpaDAO;

    public StarredBroadcastJpaDAO getStarredBroadcastJpaDAO()
    {
        return starredBroadcastJpaDAO;
    }

    public void setStarredBroadcastJpaDAO(StarredBroadcastJpaDAO starredBroadcastJpaDAO)
    {
        this.starredBroadcastJpaDAO = starredBroadcastJpaDAO;
    }

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        return starredBroadcastJpaDAO.getStarredBroadcastByAccountId(accountId);
    }

    public void saveStarredBroadcast(StarredBroadcast starredBroadcast)
    {
        starredBroadcastJpaDAO.saveStarredBroadcast(starredBroadcast);
    }

    public List<StarredBroadcast> getStarredBroadByAccountId(String accountId)
    {
        return starredBroadcastJpaDAO.getStarredBroadcastByAccountId(accountId);
    }

    public StarredBroadcast getStarredBroadcastByAccountIdAndBroadcastId(String accountId, String broadcastId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(broadcastId), "Broadcast Id passed should not be null or empty.");
        return starredBroadcastJpaDAO.getStarredBroadcastByAccountIdAndBroadcastId(accountId, broadcastId);
    }

    public void unStarBroadcast(StarredBroadcast starredBroadcast)
    {
        Validation.isTrue(!Utils.isNull(starredBroadcast), "starredBroadcast Entity should not be null");
        starredBroadcastJpaDAO.unStarBroadcast(starredBroadcast);
    }

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId, int limit, int offset)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        Validation.isTrue(!Utils.isNull(limit), "Limit passed should not be null or empty.");
        Validation.isTrue(!Utils.isNull(offset), "Offset passed should not be null or empty.");
        return starredBroadcastJpaDAO.getStarredBroadcastByAccountId(accountId, limit, offset);
    }

    public List<StarredBroadcast> getStarredBroadcastIdsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        return starredBroadcastJpaDAO.getStarredBroadcastIdsByAccountId(accountId);
    }

    public boolean isBroadcastStarred(String broadcastId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(broadcastId), "BroadcastId Id passed should not be null or empty.");
        return starredBroadcastJpaDAO.isBroadcastStarred(broadcastId);
    }
}
