/**
 * 
 */
package one.group.dao.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import one.group.core.enums.FilterField;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.dao.GroupsDAO;
import one.group.entities.api.response.v2.WSAdminGroupMetaData;
import one.group.entities.jpa.BroadcastGroupRelation;
import one.group.entities.socket.Groups;
import one.group.jpa.dao.BroadcastGroupRelationJpaDAO;
import one.group.jpa.dao.GroupMembersJpaDAO;
import one.group.jpa.dao.GroupsJpaDAO;
import one.group.jpa.dao.impl.JPABaseDAOImpl;
import one.group.solr.dao.GroupsSolrDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;

/**
 * @author ashishthorat
 *
 */
public class GroupsDAOImpl extends JPABaseDAOImpl<String, Groups> implements GroupsDAO
{
    private GroupsJpaDAO groupsJpaDAO;

    private GroupsSolrDAO groupsSolrDAO;

    private BroadcastGroupRelationJpaDAO broadcastGroupRelationDAO;

    private GroupMembersJpaDAO groupMembersJpaDAO;

    public void setGroupMembersJpaDAO(GroupMembersJpaDAO groupMembersJpaDAO)
    {
        this.groupMembersJpaDAO = groupMembersJpaDAO;
    }

    public GroupMembersJpaDAO getGroupMembersJpaDAO()
    {
        return groupMembersJpaDAO;
    }

    public GroupsJpaDAO getGroupsJpaDAO()
    {
        return groupsJpaDAO;
    }

    public void setGroupsJpaDAO(GroupsJpaDAO groupsJpaDAO)
    {
        this.groupsJpaDAO = groupsJpaDAO;
    }

    public GroupsSolrDAO getGroupsSolrDAO()
    {
        return groupsSolrDAO;
    }

    public void setGroupsSolrDAO(GroupsSolrDAO groupsSolrDAO)
    {
        this.groupsSolrDAO = groupsSolrDAO;
    }

    public BroadcastGroupRelationJpaDAO getBroadcastGroupRelationDAO()
    {
        return broadcastGroupRelationDAO;
    }

    public void setBroadcastGroupRelationDAO(BroadcastGroupRelationJpaDAO broadcastGroupRelationDAO)
    {
        this.broadcastGroupRelationDAO = broadcastGroupRelationDAO;
    }

    public List<Groups> fetchGroupsByGroupStatus(GroupStatus status)
    {
        return null;
    }

    public Groups fetchByGroupId(String groupId)
    {
        return null;
    }

    public List<Groups> fetchGroupsByCityId(String cityId)
    {
        return null;
    }

    public List<Groups> fetchGroupsByLocationId(String cityId)
    {
        return null;
    }

    public List<Groups> fetchGroupsByCreatorMobileNo(String creatorMobileNo)
    {
        return null;
    }

    public Map<GroupStatus, Set<Groups>> fetchByStatusAndEverSync(List<GroupStatus> groupStatus, boolean everSync)
    {
        return groupsSolrDAO.fetchGroupsByGroupStatusAndEverSync(groupStatus, everSync);
    }

    public List<Groups> fetchGroupLabelAndIdByStatus(List<GroupStatus> groupStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        return groupsSolrDAO.fetchGroupLabelAndIdByStatus(groupStatusList, sort, filter);
    }

    public int fetchLatestMessageIndexOfGroup(String groupId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(groupId), "Group id should not be null or empty");
        return groupsSolrDAO.fetchLastMessageIndexOfGroup(groupId);
    }

    public String fetchGroupIdFromParticipants(String participantOne, String participantTwo)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(participantOne), "Participant one should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(participantTwo), "Participant two should not be null or empty");
        return UUID.randomUUID().toString();
    }

    public void saveBroadcastGroupRelation(BroadcastGroupRelation relation)
    {
        broadcastGroupRelationDAO.save(relation);
    }

    public List<Groups> fetchGroupsByGroupIds(List<String> groupIds)
    {
        return groupsSolrDAO.fetchGroupsByGroupIds(groupIds);
    }

    public void saveGroup(Groups groups)
    {
        Validation.isTrue(!Utils.isNull(groups), "Groups should not be null");
        groupsSolrDAO.saveGroups(groups);
    }

    public List<Groups> fetchGroupIdFromMembers(List<String> memberIds, GroupSource groupSource)
    {
        Validation.isTrue(!Utils.isNull(memberIds), "Members should not be null");
        Validation.isTrue(!Utils.isNull(groupSource), "Group source should not be null");
        return groupsSolrDAO.fetchGroupIdFromMembers(memberIds, groupSource);
    }

    public List<Groups> fetchAllGroupsByAccountId(String accountId, List<GroupSource> groupSourceList)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNull(groupSourceList), "group source List should not be null");
        return groupsSolrDAO.fetchAllGroupsByAccountId(accountId, groupSourceList);
    }

    public List<Groups> fetchUnassignedGroupsBasedOnCity(String cityId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        return groupsSolrDAO.fetchUnassignedGroupsBasedOnCityAndStatusList(Arrays.asList(GroupStatus.ACTIVE), (cityId == null) ? null : Arrays.asList(cityId), sort, filter);
    }

    public List<Groups> fetchDefaultGroupsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account id should not be null");
        return groupsSolrDAO.fetchDefaultGroupsByAccountId(accountId);
    }

    public List<Groups> fetchGroupsByInputs(List<GroupStatus> groupStatusList, List<String> groupIdList, int msgCountGreaterThan, int rows, int limit, String sortByField, String sortByType,
            Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        return groupsSolrDAO.fetchGroupsByInputs(groupStatusList, groupIdList, msgCountGreaterThan, rows, limit, sortByField, sortByType, sort, filter);
    }

    public void releaseAllGroups()
    {
        groupsSolrDAO.releaseAllGroups();
    }

    public List<Groups> fetchAllGroupsByAccountIdAndGroupId(String accountId, String groupId)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isNull(groupId), "Group id should not be null");

        return groupsSolrDAO.fetchAllGroupsByAccountIdAndGroupId(accountId, groupId);
    }

    public void saveGroupsBulk(List<Groups> groupsList)
    {
        groupsSolrDAO.saveGroupsBulk(groupsList);
    }

    public void updateGroup(SolrInputDocument groups)
    {
        groupsSolrDAO.update(groups);
    }

    public QueryResponse fetchGroupsToUpdateUnreadFrom(int start, int rows, boolean fullEntity, Collection<String> groupIdCollection)
    {
        return groupsSolrDAO.fetchGroupsToUpdateUnreadFrom(start, rows, fullEntity, groupIdCollection);
    }

    public Map<String, Groups> fetchGroups(int start, int rows)
    {
        return groupsSolrDAO.fetchGroups(start, rows);
    }

    public List<Groups> fetchGroupsByGroupIdsAndSource(List<String> groupIds, List<GroupSource> groupSourceList)
    {
        return groupsSolrDAO.fetchGroupsByGroupIdsAndSource(groupIds, groupSourceList);
    }

    public int fetchTotalGroupCount()
    {
        return groupsSolrDAO.fetchTotalGroupCount();
    }

    public WSAdminGroupMetaData fetchGroupMetaDataByGroupIdsAndEverSync(List<String> groupIdList, boolean everSync)
    {
        Validation.isTrue(!Utils.isNull(groupIdList), "GroupsIds should not be null");
        return groupsSolrDAO.fetchGroupMetaDataByGroupIdsAndEverSync(groupIdList, everSync);
    }
    
    public void update(List<Groups> groups)
    {
        groupsSolrDAO.update(groups);   
    }
}
