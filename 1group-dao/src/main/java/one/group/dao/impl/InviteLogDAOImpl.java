package one.group.dao.impl;

import java.util.List;

import one.group.dao.InviteLogDAO;
import one.group.entities.jpa.InviteLog;
import one.group.jpa.dao.InviteLogJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class InviteLogDAOImpl implements InviteLogDAO
{
    private InviteLogJpaDAO inviteLogJpaDAO;

    public InviteLogJpaDAO getInviteLogJpaDAO()
    {
        return inviteLogJpaDAO;
    }

    public void setInviteLogJpaDAO(InviteLogJpaDAO inviteLogJpaDAO)
    {
        this.inviteLogJpaDAO = inviteLogJpaDAO;
    }

    public void saveInviteLog(InviteLog inviteLog)
    {
        inviteLogJpaDAO.saveInviteLog(inviteLog);
    }

    public List<String> findAllInvitedByAccount(String accountId, long inviteDurationInMillies)
    {
        Validation.notNull(accountId, "Account id should not be null.");
        Validation.notNull(inviteDurationInMillies, "inviteDurationInMillies should not be null");

        return inviteLogJpaDAO.findAllInvitedByAccount(accountId, inviteDurationInMillies);
    }

    public InviteLog fetchInviteLogById(String inviteLogId)
    {
        return inviteLogJpaDAO.fetchInviteLogById(inviteLogId);
    }

}
