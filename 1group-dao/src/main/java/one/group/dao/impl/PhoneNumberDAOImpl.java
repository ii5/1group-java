package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.PhoneNumberDAO;
import one.group.entities.jpa.PhoneNumber;
import one.group.jpa.dao.PhoneNumberJpaDAO;

/**
 * 
 * @author nyalfernandes
 *
 */
public class PhoneNumberDAOImpl implements PhoneNumberDAO
{
    private PhoneNumberJpaDAO phoneNumberJpaDAO;

    public PhoneNumberJpaDAO getPhoneNumberJpaDAO()
    {
        return phoneNumberJpaDAO;
    }

    public void setPhoneNumberJpaDAO(PhoneNumberJpaDAO phoneNumberJpaDAO)
    {
        this.phoneNumberJpaDAO = phoneNumberJpaDAO;
    }

    public void savePhonenumber(PhoneNumber number)
    {
        phoneNumberJpaDAO.persist(number);
    }

    public PhoneNumber fetchPhoneNumber(String mobileNumber)
    {
        return phoneNumberJpaDAO.fetchPhoneNumber(mobileNumber);
    }
    
    public List<PhoneNumber> fetchPhoneNumbersByIds(Collection<String> phoneNumbers) 
    {
    	return phoneNumberJpaDAO.fetchPhoneNumbersByIds(phoneNumbers);
    }

}
