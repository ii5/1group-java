/**
 * 
 */
package one.group.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.AdminStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.dao.AdminDAO;
import one.group.entities.jpa.Admin;
import one.group.jpa.dao.AdminJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class AdminDAOImpl implements AdminDAO
{

    private AdminJpaDAO adminJpaDAO;

    public AdminJpaDAO getAdminJpaDAO()
    {
        return adminJpaDAO;
    }

    public void setAdminJpaDAO(AdminJpaDAO adminJpaDAO)
    {
        this.adminJpaDAO = adminJpaDAO;
    }

    public List<Admin> fetchAdminByRoleId(String roleId)
    {
        return null;
    }

    public List<Admin> fetchAdminByManagerId(String managerId)
    {
        return null;
    }

    public List<Admin> fetchAdminByStatus(String status)
    {
        return null;
    }

    public void saveAdmin(Admin admin)
    {
        adminJpaDAO.saveAdmin(admin);
    }

    public Admin fetchAdminById(String adminId)
    {
        return adminJpaDAO.fetchAdminById(adminId);
    }
    
    public List<Admin> fetchAdminUsersByRoleListAndStatusList(List<String> roleList, List<AdminStatus> adminStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        return adminJpaDAO.fetchAdminUsersByRoleListAndAdminStatusList(roleList, adminStatusList, sort, filter, start, rows);
    }

    public List<Admin> fetchAdminUsersByInput(String username, List<AdminStatus> adminStatusList, String email, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        return adminJpaDAO.fetchAdminUsersByInput(username, adminStatusList, email, sort, filter, start, rows);
    }
}
