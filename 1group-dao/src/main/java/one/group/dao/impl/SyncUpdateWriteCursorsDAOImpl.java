package one.group.dao.impl;

import java.util.Collection;

import one.group.dao.SyncUpdateWriteCursorsDAO;
import one.group.entities.jpa.SyncUpdateWriteCursors;
import one.group.jpa.dao.SyncUpdateWriteCursorsJpaDAO;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SyncUpdateWriteCursorsDAOImpl implements SyncUpdateWriteCursorsDAO
{
    public SyncUpdateWriteCursorsJpaDAO syncLogWriteCursorsJpaDAO;

    public void saveSyncLogWriteCursors(SyncUpdateWriteCursors writeCursor)
    {
        syncLogWriteCursorsJpaDAO.addSyncLogWriteCursors(writeCursor);
    }

    public SyncUpdateWriteCursors fetchByAccount(String accountId)
    {
        return syncLogWriteCursorsJpaDAO.fetchByAccount(accountId);
    }
    
    public Collection<SyncUpdateWriteCursors> findAllCursors()
    {
        return syncLogWriteCursorsJpaDAO.findAll();
    }

    public SyncUpdateWriteCursorsJpaDAO getSyncLogWriteCursorsJpaDAO()
    {
        return syncLogWriteCursorsJpaDAO;
    }

    public void setSyncLogWriteCursorsJpaDAO(SyncUpdateWriteCursorsJpaDAO syncLogWriteCursorsJpaDAO)
    {
        this.syncLogWriteCursorsJpaDAO = syncLogWriteCursorsJpaDAO;
    }
}
