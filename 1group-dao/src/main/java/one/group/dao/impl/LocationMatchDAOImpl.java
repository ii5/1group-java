package one.group.dao.impl;

import java.util.List;

import one.group.dao.LocationMatchDAO;
import one.group.entities.jpa.LocationMatch;
import one.group.jpa.dao.LocationMatchJpaDAO;

public class LocationMatchDAOImpl implements LocationMatchDAO
{

    LocationMatchJpaDAO locationMatchJpaDAO;

    public LocationMatchJpaDAO getLocationMatchJpaDAO()
    {
        return locationMatchJpaDAO;
    }

    public void setLocationMatchJpaDAO(LocationMatchJpaDAO locationMatchJpaDAO)
    {
        this.locationMatchJpaDAO = locationMatchJpaDAO;
    }

    public List<LocationMatch> fetchExactLocationMatchByLocationId(String locationId, boolean isExact)
    {
        return locationMatchJpaDAO.fetchExactLocationMatchByLocationId(locationId, isExact);
    }

    public List<LocationMatch> fetchLocationMatchByLocationId(String locationId)
    {
        return locationMatchJpaDAO.fetchLocationMatchByLocationId(locationId);
    }

    public List<LocationMatch> fetchExactLocationMatchByLocationId(List<String> locationIdList, boolean isExact)
    {
        return locationMatchJpaDAO.fetchExactLocationMatchByLocationIdList(locationIdList, isExact);
    }

    public List<String> fetchLocationMatchByLocationIdList(List<String> locationIdList)
    {
        return locationMatchJpaDAO.fetchLocationMatchByLocationIdList(locationIdList);
    }

    public List<String> fetchExactLocationMatchIdListByLocationIdList(List<String> locationIdList, boolean isExact)
    {
        return locationMatchJpaDAO.fetchExactLocationMatchIdListByLocationIdList(locationIdList, isExact);
    }

    public void saveLocationMatch(LocationMatch locationMatch)
    {
        locationMatchJpaDAO.saveLocationMatch(locationMatch);

    }

}
