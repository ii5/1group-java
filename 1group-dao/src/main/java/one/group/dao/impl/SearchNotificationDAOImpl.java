package one.group.dao.impl;

import java.util.List;

import one.group.dao.SearchNotificationDAO;
import one.group.entities.jpa.SearchNotification;
import one.group.jpa.dao.SearchNotificationJpaDAO;

public class SearchNotificationDAOImpl implements SearchNotificationDAO
{
    private SearchNotificationJpaDAO searchNotificationJpaDAO;

    public SearchNotificationJpaDAO getSearchNotificationJpaDAO()
    {
        return searchNotificationJpaDAO;
    }

    public void setSearchNotificationJpaDAO(SearchNotificationJpaDAO searchNotificationJpaDAO)
    {
        this.searchNotificationJpaDAO = searchNotificationJpaDAO;
    }

    public List<SearchNotification> fetchAllNotifications()
    {
        return searchNotificationJpaDAO.fetchAllNotifications();
    }

    public void saveSearchNotification(SearchNotification searchNotification)
    {
        searchNotificationJpaDAO.saveSearchNotification(searchNotification);
    }

    public void removeAllSearchNotification()
    {
        searchNotificationJpaDAO.removeAllSearchNotification();

    }

    public void removeSearchNotification(String id)
    {
        searchNotificationJpaDAO.removeById(id);

    }

    public long getSearchNotificationCountByAccountIdAndSentFlag(String accountId, boolean sendFlag)
    {
        return searchNotificationJpaDAO.getSearchNotificationCountByAccountIdAndSentFlag(accountId, sendFlag);

    }
}
