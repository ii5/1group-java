/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.dao.MessagesDAO;
import one.group.entities.jpa.Messages;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.jpa.dao.MessagesJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class MessagesDAOImpl implements MessagesDAO
{
    private MessagesJpaDAO messagesJpaDAO;

    public MessagesJpaDAO getMessagesJpaDAO()
    {
        return messagesJpaDAO;
    }

    public void setMessagesJpaDAO(MessagesJpaDAO messagesJpaDAO)
    {
        this.messagesJpaDAO = messagesJpaDAO;
    }

    public List<Messages> fetchMessagesByOneGroupStatus(MessageStatus status)
    {
        return null;
    }

    public void saveMessages(Messages messages)
    {
        messagesJpaDAO.saveMessages(messages);
    }

    public List<Messages> fetchMessagesByBpoMessageStatus(BpoMessageStatus status)
    {
        return null;
    }

    public List<Messages> fetchMessagesByMessageType(MessageType type)
    {
        return null;
    }

    public List<Messages> getMessagesByBroadcastId(String broadcastId) throws Abstract1GroupException
    {
        return null;
    }

    public boolean isMessageDuplicate(String messageId)
    {
        return false;
    }

    public List<Messages> getMessagesByGroupId(String groupId) throws Abstract1GroupException
    {
        return null;
    }

    public List<Messages> getMessagesByAccountId(String accountId) throws Abstract1GroupException
    {
        return null;
    }

    public Messages fetchByMessageByMessageId(String messageId)
    {
        return messagesJpaDAO.fetchByMessageByMessageId(messageId);
    }
    
    

}
