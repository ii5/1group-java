package one.group.dao.impl;

import java.util.List;

import one.group.dao.SyncUpdateDAO;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.SyncUpdate;
import one.group.jpa.dao.SyncUpdateJpaDAO;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SyncUpdateDAOImpl implements SyncUpdateDAO
{
    private SyncUpdateJpaDAO syncLogJpaDAO;

    public void saveAccountUpdate(SyncUpdate log)
    {
        syncLogJpaDAO.addAccountUpdate(log);
    }

    public void saveAccountUpdates(List<SyncUpdate> syncUpdateList)
    {
        syncLogJpaDAO.addAccountUpdates(syncUpdateList);
    }

    public List<SyncUpdate> fetchByAccount(String accountId)
    {
        return syncLogJpaDAO.fetchByAccount(accountId);
    }

    public SyncUpdate fetchByAccountAndUpdateIndex(String accountId, int updateIndex)
    {
        return syncLogJpaDAO.fetchByAccountAndUpdateIndex(accountId, updateIndex);
    }

    public List<SyncUpdate> fetchAllByAccount(String accountId, int fromUpdateIndex, int toUpdateIndex)
    {
        return syncLogJpaDAO.fetchByAccountFromAndTo(accountId, fromUpdateIndex, toUpdateIndex);
    }

    public SyncUpdateJpaDAO getSyncLogJpaDAO()
    {
        return syncLogJpaDAO;
    }

    public void setSyncLogJpaDAO(SyncUpdateJpaDAO syncLogJpaDAO)
    {
        this.syncLogJpaDAO = syncLogJpaDAO;
    }

    public int createClient(String clientId)
    {
        return syncLogJpaDAO.createClient(clientId);
    }

    public void appendUpdate(String accountId, WSSyncUpdate syncUpdate)
    {
        syncLogJpaDAO.appendUpdate(accountId, syncUpdate);
    }

    public int advanceReadCursor(String accountId, String clientId, int requestedCursorIndex)
    {
        return syncLogJpaDAO.advanceReadCursor(accountId, clientId, requestedCursorIndex);
    }

    public void deleteEarlierUpdatesOfAccount(int index, String accountId)
    {
        syncLogJpaDAO.deleteEarlierUpdatesOfAccount(index, accountId);
    }

}
