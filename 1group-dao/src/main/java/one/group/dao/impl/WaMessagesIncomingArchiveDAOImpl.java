/**
 * 
 */
package one.group.dao.impl;

import java.util.List;

import one.group.dao.WaMessagesIncomingArchiveDAO;
import one.group.entities.jpa.WaMessagesIncomingArchive;
import one.group.jpa.dao.WaMessagesIncomingArchiveJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesIncomingArchiveDAOImpl implements WaMessagesIncomingArchiveDAO
{
    private WaMessagesIncomingArchiveJpaDAO waMessagesIncomingArchiveJpaDAO;

    public WaMessagesIncomingArchiveJpaDAO getWaMessagesIncomingArchiveJpaDAO()
    {
        return waMessagesIncomingArchiveJpaDAO;
    }

    public void setWaMessagesIncomingArchiveJpaDAO(WaMessagesIncomingArchiveJpaDAO waMessagesIncomingArchiveJpaDAO)
    {
        this.waMessagesIncomingArchiveJpaDAO = waMessagesIncomingArchiveJpaDAO;
    }

    public List<WaMessagesIncomingArchive> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesIncomingArchive(WaMessagesIncomingArchive waMessagesIncomingArchive)
    {
        waMessagesIncomingArchiveJpaDAO.saveWaMessagesIncomingArchive(waMessagesIncomingArchive);
    }

    public WaMessagesIncomingArchive fetchWaMessagesIncomingArchiveById(String waMessageId)
    {
        return waMessagesIncomingArchiveJpaDAO.fetchWaMessagesIncomingArchiveById(waMessageId);
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByStatus(String status)
    {
        return null;
    }

}
