package one.group.dao.impl;

import one.group.dao.BroadcastGroupRelationDAO;
import one.group.entities.jpa.BroadcastGroupRelation;
import one.group.jpa.dao.BroadcastGroupRelationJpaDAO;

public class BroadcastGroupRelationDAOImpl implements BroadcastGroupRelationDAO
{

    private BroadcastGroupRelationJpaDAO broadcastGroupRelationJpaDAO;

    public BroadcastGroupRelationJpaDAO getBroadcastGroupRelationJpaDAO()
    {
        return broadcastGroupRelationJpaDAO;
    }

    public void setBroadcastGroupRelationJpaDAO(BroadcastGroupRelationJpaDAO broadcastGroupRelationJpaDAO)
    {
        this.broadcastGroupRelationJpaDAO = broadcastGroupRelationJpaDAO;
    }

    public void saveBroadcastGroupRelation(BroadcastGroupRelation relation)
    {
        broadcastGroupRelationJpaDAO.save(relation);
    }

}
