/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.ChatLogDAO;
import one.group.entities.jpa.ChatLog;
import one.group.jpa.dao.ChatLogJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class ChatLogDAOImpl implements ChatLogDAO
{
    private ChatLogJpaDAO chatLogJpaDAO;

    public ChatLogJpaDAO getChatLogJpaDAO()
    {
        return chatLogJpaDAO;
    }

    public void setChatLogJpaDAO(ChatLogJpaDAO chatLogJpaDAO)
    {
        this.chatLogJpaDAO = chatLogJpaDAO;
    }

    public void saveChatLog(ChatLog chatlog)
    {
        chatLogJpaDAO.saveChatLog(chatlog);
    }

    public ChatLog fetchChatLog(String chatThreadId, String index)
    {
        return chatLogJpaDAO.fetchChatLog(chatThreadId, index);
    }

}
