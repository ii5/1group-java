/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.AccountMessageCursorDAO;
import one.group.entities.jpa.AccountMessageCursors;
import one.group.jpa.dao.AccountMessageCursorJpaDAO;

public class AccountMessageCursorDAOImpl implements AccountMessageCursorDAO
{

    private AccountMessageCursorJpaDAO accountMessageCursorJpaDAO;

    public AccountMessageCursorJpaDAO getAccountMessageCursorJpaDAO()
    {
        return accountMessageCursorJpaDAO;
    }

    public void setAccountMessageCursorJpaDAO(AccountMessageCursorJpaDAO accountMessageCursorJpaDAO)
    {
        this.accountMessageCursorJpaDAO = accountMessageCursorJpaDAO;
    }

    public void saveAccountMessageCursor(AccountMessageCursors accountMessageCursor)
    {
        accountMessageCursorJpaDAO.saveAccountMessageCursor(accountMessageCursor);

    }

    public AccountMessageCursors fetchByAccountMessageCursorsId(String accountMessageCursorId)
    {
        return accountMessageCursorJpaDAO.fetchByAccountMessageCursorsId(accountMessageCursorId);
    }

}
