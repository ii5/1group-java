package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.ParentLocationsDAO;
import one.group.entities.jpa.ParentLocations;
import one.group.jpa.dao.ParentLocationJpaDAO;

/**
 * 
 * @author sanilshet
 *
 */
public class ParentLocationDAOImpl implements ParentLocationsDAO
{

    private ParentLocationJpaDAO parentLocationJpaDAO;

    public ParentLocationJpaDAO getParentLocationJpaDAO()
    {
        return parentLocationJpaDAO;
    }

    public void setParentLocationJpaDAO(ParentLocationJpaDAO parentLocationJpaDAO)
    {
        this.parentLocationJpaDAO = parentLocationJpaDAO;
    }

    /**
     * @param cityId
     */
    public List<ParentLocations> getAllLocationsByCityId(String cityId)
    {
        return parentLocationJpaDAO.getAllLocationsByCityId(cityId);
    }
    
	public List<ParentLocations> getAllParentLocations()
	{	
		return parentLocationJpaDAO.getAllParentLocations();
	}
	
	public Collection<ParentLocations> getAllParentLocationsByCity(List<String> cityIds) 
	{
		return parentLocationJpaDAO.getAllParentLocationsByCity(cityIds);
	}
}
