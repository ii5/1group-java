/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.MenuDAO;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.Menu;
import one.group.jpa.dao.MenuJpaDAO;

public class MenuDAOImpl implements MenuDAO
{
    private MenuJpaDAO menuJpaDAO;

    public MenuJpaDAO getMenuJpaDAO()
    {
        return menuJpaDAO;
    }

    public void setMenuJpaDAO(MenuJpaDAO menuJpaDAO)
    {
        this.menuJpaDAO = menuJpaDAO;
    }

    public void saveMenu(Admin admin)
    {

    }

    public void saveMenu(Menu menu)
    {
        menuJpaDAO.saveMenu(menu);

    }

    public Menu fetchByMenuId(String menuId)
    {
        return menuJpaDAO.fetchByMenuId(menuId);
    }

}
