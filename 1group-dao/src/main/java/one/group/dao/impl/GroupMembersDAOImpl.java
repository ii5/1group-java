/**
 * 
 */
package one.group.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import one.group.core.enums.GroupSource;
import one.group.dao.GroupMembersDAO;
import one.group.entities.api.request.bpo.WSGroupMembersMetadataRequest;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.helpers.GroupMembersObject;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.jpa.dao.GroupMembersJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class GroupMembersDAOImpl implements GroupMembersDAO
{
    private GroupMembersJpaDAO groupMembersJpaDAO;

    public GroupMembersJpaDAO getGroupMembersJpaDAO()
    {
        return groupMembersJpaDAO;
    }

    public void setGroupMembersJpaDAO(GroupMembersJpaDAO groupMembersJpaDAO)
    {
        this.groupMembersJpaDAO = groupMembersJpaDAO;
    }

    public List<GroupMembers> fetchGroupMembersByGroupId(String groupId)
    {
        return groupMembersJpaDAO.fetchGroupMembersByGroupId(groupId);
    }

    public List<GroupMembers> fetchGroupMembersByAccountId(String accountId)
    {
        return null;
    }

    public GroupMembers fetchGroupMembersById(String groupMemberId)
    {
        return null;
    }

    public List<GroupMembers> fetchGroupMembersByMobileNo(String mobilrNo)
    {
        return groupMembersJpaDAO.fetchGroupMembersByMobileNo(Arrays.asList(mobilrNo));
    }

    public boolean isAdmin(String groupMembersId)
    {
        return false;
    }

    public void saveGroupMembers(List<GroupMembers> groupMembers)
    {
        Validation.isTrue(!Utils.isNull(groupMembers), "Group members should not ne null");
        groupMembersJpaDAO.saveGroupMembers(groupMembers);
    }

    public int updateReadAndReceivedIndex(String groupId, String participantId, int receivedIndex, int readIndex, String receivedMessageId, String readMessageId)
    {
        Validation.isTrue(!Utils.isNull(groupId), "Chat thread id should not ne null");
        Validation.isTrue(!Utils.isNull(participantId), "Participant id should not ne null");
        Validation.isTrue(!Utils.isNull(readIndex), "Read index should not be null");
        Validation.isTrue(!Utils.isNull(receivedIndex), "Received index should not ne null");
        // Validation.isTrue(!Utils.isNull(receivedMessageId),
        // "Received message id ishould not be null");
        // Validation.isTrue(!Utils.isNull(readMessageId),
        // "Read message id should not be null");

        return groupMembersJpaDAO.updateReadAndReceivedIndex(groupId, participantId, receivedIndex, readIndex, receivedMessageId, readMessageId);
    }

    public List<GroupMembers> fetchMembersByParticipants(List<String> participantIds)
    {
        Validation.isTrue(!Utils.isNull(participantIds), "participantIds should not be null");
        return groupMembersJpaDAO.fetchGroupMemberByParticipantIds(participantIds);
    }

    public void saveGroupMembers(WSGroupMembersMetadataRequest groupMember, List<GroupMembers> existingGroupMembers) throws Abstract1GroupException
    {

        if (existingGroupMembers == null)
        {
            existingGroupMembers = new ArrayList<GroupMembers>();
        }

        GroupMembers gm = new GroupMembers();
        gm.setGroupId(groupMember.getGroupId());

        if (groupMember.getParticipantId() == null || groupMember.getParticipantId().trim().isEmpty())
        {
            if (groupMember.getMobileNumber() == null || groupMember.getMobileNumber().trim().isEmpty())
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
            }
        }
        else
        {
            gm.setParticipantId(groupMember.getParticipantId());
        }

        if (existingGroupMembers.contains(gm))
        {
            gm = existingGroupMembers.get(existingGroupMembers.indexOf(gm));
        }

        gm.setActiveMember(Utils.<Boolean> returnArg1OnNull(gm.isActiveMember(), groupMember.getActiveMember()));
        gm.setAdmin(Utils.<Boolean> returnArg1OnNull(gm.isAdmin(), groupMember.getIsAdmin()));
        gm.setCreatedTime(Utils.<Date> returnArg1OnNull(gm.getCreatedTime(), new Date()));
        gm.setCurrentSync(Utils.<Boolean> returnArg1OnNull(gm.isCurrentSync(), groupMember.getCurrentSync()));
        gm.setEverSync(Utils.<Boolean> returnArg1OnNull(gm.isEverSync(), groupMember.getEverSync()));
        gm.setMobileNumber(Utils.returnArg1OnNull(gm.getMobileNumber(), groupMember.getMobileNumber()));
        gm.setParticipantId(Utils.<String> returnArg1OnNull(gm.getParticipantId(), groupMember.getParticipantId()));
        gm.setReadIndex(Utils.<Integer> returnArg1OnNull(gm.getReadIndex(), groupMember.getReadIndex()));
        gm.setReceivedIndex(Utils.<Integer> returnArg1OnNull(gm.getReceivedIndex(), groupMember.getReceivedIndex()));
        gm.setUpdatedTime(new Date());

        groupMembersJpaDAO.saveGroupMembers(Arrays.asList(gm));
    }

    public GroupMembers fetchGroupMembersByAccountIdAndGroupId(String accountId, String groupId)
    {
        return groupMembersJpaDAO.fetchGroupMembersByAccountIdAndGroupId(accountId, groupId);
    }

    public List<GroupMembers> fetchGroupMembersByMobileNumbers(List<String> mobileNumbers)
    {
        return groupMembersJpaDAO.fetchGroupMembersByMobileNo(mobileNumbers);
    }

    public String fetchGroupIdFromParticipants(String participantsOne, String participantsTwo, GroupSource source)
    {
        return groupMembersJpaDAO.fetchGroupIdFromParticipants(participantsOne, participantsTwo, source);
    }

    public List<String> fetchGroupIdListByAccountId(String accountId)
    {
        return groupMembersJpaDAO.fetchGroupIdListByAccountId(accountId);
    }

    public List<String> fetchMemberIdListByGroupId(String groupId)
    {
        return groupMembersJpaDAO.fetchMemberIdListByGroupId(groupId);
    }

    public List<GroupMembers> fetchGroupMembersByAccountIdAndGroupId(String accountId, List<String> groupIdList)
    {
        return groupMembersJpaDAO.fetchGroupMembersByAccountIdAndGroupId(accountId, groupIdList);
    }

    public String fetchGroupIdFromParticipantIdAndSource(String accountId, GroupSource source)
    {
        return groupMembersJpaDAO.fetchGroupIdFromParticipantIdAndSource(accountId, source);
    }

    public List<GroupMembers> fetchGroupMembersByMobileNoAndEverSync(List<String> mobileNumbers, Boolean everSync)
    {
        return groupMembersJpaDAO.fetchGroupMembersByMobileNoAndEverSync(mobileNumbers, everSync);
    }

    public List<GroupMembersObject> fetchGroupMembersObjectByMobileNoAndEverSync(List<String> mobileNumbers, List<Boolean> everSync)
    {
        return groupMembersJpaDAO.fetchGroupMembersObjectByMobileNoAndEverSync(mobileNumbers, everSync);
    }

}
