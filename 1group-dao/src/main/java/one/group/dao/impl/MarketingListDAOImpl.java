package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.MarketingListDAO;
import one.group.entities.jpa.MarketingList;
import one.group.jpa.dao.MarketingListJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class MarketingListDAOImpl implements MarketingListDAO
{

    MarketingListJpaDAO marketingListJpaDAO;

    public MarketingListJpaDAO getMarketingListJpaDAO()
    {
        return marketingListJpaDAO;

    }

    public void setMarketingListJpaDAO(MarketingListJpaDAO marketingListJpaDAO)
    {
        this.marketingListJpaDAO = marketingListJpaDAO;
    }

    /**
     * 
     */
    public MarketingList checkPhoneNumberExists(String phoneNumber)
    {
        Validation.notNull(phoneNumber, "PhoneNumber should not be null.");
        return marketingListJpaDAO.checkPhoneNumberExists(phoneNumber);
    }

    public void saveAll(Collection<MarketingList> marketingListCollection)
    {
        Validation.notNull(marketingListCollection, "marketingListCollection should not be null or empty");
        marketingListJpaDAO.saveAll(marketingListCollection);
    }

    public void save(MarketingList marketingList)
    {
        Validation.notNull(marketingList, "marketingList should not be null or empty");
        marketingListJpaDAO.save(marketingList);
    }

    public Collection<MarketingList> fetchAll()
    {
        return marketingListJpaDAO.fetchAll();
    }

    public List<MarketingList> fetchAllMarketingListWhichAreNativeContacts(String accountId)
    {
        Validation.notNull(accountId, "AccountId should not be null.");
        return marketingListJpaDAO.fetchAllMarketingListWhichAreNativeContacts(accountId);
    }

    public List<MarketingList> fetchAllMarketingListByPhoneNumberList(List<String> phoneNumbers)
    {
        Validation.notNull(phoneNumbers, "Phone numbers should not be null");

        return marketingListJpaDAO.fetchMarketingListWherePhoneNumbersIn(phoneNumbers);
    }

    public List<MarketingList> fetchPhoneNumbersOrderByWhatsAppGroupCount(List<String> phoneNumbers)
    {
        Validation.notNull(phoneNumbers, "Phone numbers should not be null");

        return marketingListJpaDAO.fetchPhoneNumbersOrderByWhatsAppGroupCount(phoneNumbers);
    }

}
