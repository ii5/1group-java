package one.group.dao.impl;

import java.util.List;

import one.group.core.enums.StarType;
import one.group.dao.StarDAO;
import one.group.entities.jpa.Star;
import one.group.jpa.dao.StarJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class StarDAOImpl implements StarDAO
{
    private StarJpaDAO starJpaDAO;

    public StarJpaDAO getStarJpaDAO()
    {
        return starJpaDAO;
    }

    public void setStarJpaDAO(StarJpaDAO starJpaDAO)
    {
        this.starJpaDAO = starJpaDAO;
    }

    public Star doStar(String referenceId, String targetEntityId, StarType type)
    {
        return starJpaDAO.doStar(referenceId, targetEntityId, type);
    }

    public List<Star> getStarsByReference(String referenceId, StarType type)
    {
        return starJpaDAO.getAllStarsByReference(referenceId, type);
    }

    public Star getStarByReferenceAndTargetEntity(String referenceId, String targetEntityId)
    {
        Validation.isTrue(!Utils.isNull(referenceId), "referenceId should not be null");
        Validation.isTrue(!Utils.isNull(targetEntityId), "targetEntityId should not be null");
        return starJpaDAO.getStarByReferenceAndTargetEntity(referenceId, targetEntityId);
    }

    public List<Star> checkIsStarredForBroadcast(String accountId, List<String> broadcastIds)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void unStarBroadcast(Star star)
    {
        Validation.isTrue(!Utils.isNull(star), "Star Entity should not be null");
        starJpaDAO.unStarBroadcast(star);
    }

}
