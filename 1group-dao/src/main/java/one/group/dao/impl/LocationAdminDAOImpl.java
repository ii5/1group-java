package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.LocationAdminDAO;
import one.group.entities.jpa.LocationAdmin;
import one.group.jpa.dao.LocationAdminJpaDAO;

public class LocationAdminDAOImpl implements LocationAdminDAO
{

    private LocationAdminJpaDAO locationAdminJpaDAO;

    public LocationAdminJpaDAO getLocationAdminJpaDAO()
    {
        return locationAdminJpaDAO;
    }

    public void setLocationAdminJpaDAO(LocationAdminJpaDAO locationAdminJpaDAO)
    {
        this.locationAdminJpaDAO = locationAdminJpaDAO;
    }

    public Collection<LocationAdmin> getAllLocations()
    {
        return locationAdminJpaDAO.getAllLocations();
    }
    
    public Collection<LocationAdmin> getAllLocationsByCity(List<String> cityIds)
    {   
        return locationAdminJpaDAO.getAllLocationsByCity(cityIds);
    }
    
    public List<LocationAdmin> fetchLocalitiesOfCityById(String cityId)
    {
        return locationAdminJpaDAO.fetchLocalitiesOfCityById(cityId);
    }
}
