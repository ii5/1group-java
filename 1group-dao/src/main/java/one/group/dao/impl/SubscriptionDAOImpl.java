package one.group.dao.impl;

import java.util.Set;

import one.group.cache.dao.SubscriptionCacheDAO;
import one.group.core.enums.EntityType;
import one.group.dao.SubscriptionDAO;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SubscriptionDAOImpl implements SubscriptionDAO
{
    private SubscriptionCacheDAO subscriptionCacheDAO;

    public Set<String> fetchAllSubscriptions(String accountId)
    {
        return subscriptionCacheDAO.fetchAllSubscriptionsOf(accountId);
    }

    public Set<String> fetchSubscribersOf(String subscriptionId, EntityType subscriptionEntityType)
    {
        return subscriptionCacheDAO.fetchSubscribersOf(subscriptionId, subscriptionEntityType);
    }

    public Long fetchSubscriptionTime(String subscriptionId, EntityType subscriptionEntityType, String subscribingAccountId)
    {
        return subscriptionCacheDAO.getTTLForSubscription(subscribingAccountId, subscriptionId, subscriptionEntityType);
    }

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType, int forTime)
    {
        subscriptionCacheDAO.addSubscription(subscribingAccountId, subscriptionEntityId, subscriptionEntityType, forTime);
    }

    public void deleteSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType)
    {
        subscriptionCacheDAO.deleteSubscription(subscribingAccountId, subscriptionEntityId, subscriptionEntityType);
    }

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType)
    {
        subscriptionCacheDAO.addSubscription(subscribingAccountId, subscriptionEntityId, subscriptionEntityType);
    }

    public void setSubscriptionCacheDAO(SubscriptionCacheDAO subscriptionCacheDAO)
    {
        this.subscriptionCacheDAO = subscriptionCacheDAO;
    }

    public SubscriptionCacheDAO getSubscriptionCacheDAO()
    {
        return subscriptionCacheDAO;
    }

    public String isSubscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        return subscriptionCacheDAO.isSubscribe(subscribingAccountId, subscriptionEntityId, subscriptionType);
    }
    
    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType)
    {
        subscriptionCacheDAO.removeSubscription(subscribingAccountId, subscriptionType);
    }

    public void removeSubscriptionByKey(String key)
    {
        subscriptionCacheDAO.removeSubscriptionByKey(key);
    }
}
