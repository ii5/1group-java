/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.dao.WaUserCityDAO;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.jpa.WaUserCity;
import one.group.jpa.dao.WaUserCityJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaUserCityDAOImpl implements WaUserCityDAO
{
    private WaUserCityJpaDAO waUserCityJpaDAO;

    public WaUserCityJpaDAO getWaUserCityJpaDAO()
    {
        return waUserCityJpaDAO;
    }

    public void setWaUserCityJpaDAO(WaUserCityJpaDAO waUserCityJpaDAO)
    {
        this.waUserCityJpaDAO = waUserCityJpaDAO;
    }

    public void saveWaUserCityDAO(WaUserCity waUserCity)
    {
        waUserCityJpaDAO.saveWaUserCity(waUserCity);
    }

    public WaUserCity fetchByWaUserCityId(String waUserCityId)
    {
        return waUserCityJpaDAO.fetchByWaUserCityId(waUserCityId);
    }

    public List<WSUserCity> fetchByPriorityCityAndAdmin(Integer priority, String cityId, String adminId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        return waUserCityJpaDAO.fetchByPriorityCityAndAdmin(priority, cityId, adminId, sort, filter, start, rows);
    }

    public void deleteByUserIdAndCityList(String userId, Collection<String> cityIdList)
    {
        waUserCityJpaDAO.deleteByUserIdAndCityList(userId, cityIdList);
    }

    public void saveBulkByUserIdAndCityIdList(String userId, Map<String, Integer> cityIdMap)
    {
        waUserCityJpaDAO.saveBulkByUserIdAndCityIdList(userId, cityIdMap);
    }

    public void cleanAll()
    {
        waUserCityJpaDAO.cleanAll();
    }

    public void cleanByUserIdList(List<String> userIdList)
    {
        waUserCityJpaDAO.cleanByUserIdList(userIdList);
    }

    public List<WSUserCity> fetchUnassignedCityAdmin()
    {
        return waUserCityJpaDAO.fetchUnassignedCityAdmin();
    }

}
