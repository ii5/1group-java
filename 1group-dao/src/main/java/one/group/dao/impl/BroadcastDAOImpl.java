/**
 * 
 */
package one.group.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.dao.BroadcastCacheDAO;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.BroadcastDAO;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.helpers.BroadcastCountObject;
import one.group.entities.jpa.helpers.BroadcastTagCountObject;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.jpa.dao.BroadcastJpaDAO;
import one.group.jpa.dao.BroadcastTagJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class BroadcastDAOImpl implements BroadcastDAO
{
    private BroadcastJpaDAO broadcastJpaDAO;

    private BroadcastTagJpaDAO broadcastTagJpaDAO;

    private BroadcastCacheDAO broadcastCacheDAO;

    public BroadcastTagJpaDAO getBroadcastTagJpaDAO()
    {
        return broadcastTagJpaDAO;
    }

    public void setBroadcastTagJpaDAO(BroadcastTagJpaDAO broadcastTagJpaDAO)
    {
        this.broadcastTagJpaDAO = broadcastTagJpaDAO;
    }

    public BroadcastJpaDAO getBroadcastJpaDAO()
    {
        return broadcastJpaDAO;
    }

    public void setBroadcastJpaDAO(BroadcastJpaDAO broadcastJpaDAO)
    {
        this.broadcastJpaDAO = broadcastJpaDAO;
    }

    public BroadcastCacheDAO getBroadcastCacheDAO()
    {
        return broadcastCacheDAO;
    }

    public void setBroadcastCacheDAO(BroadcastCacheDAO broadcastCacheDAO)
    {
        this.broadcastCacheDAO = broadcastCacheDAO;
    }

    public Broadcast getBroadcastByBroadcastId(String broadcastId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException
    {
        return null;
    }

    public void closeBroadcast(Broadcast broadcast) throws Abstract1GroupException
    {

    }

    public Boolean doesBroadcastExist(String accountId)
    {
        return null;
    }

    public List<Broadcast> getBroadcasts(List<String> broadcastIds)
    {
        return broadcastJpaDAO.getBroadcasts(broadcastIds);
    }

    public Broadcast fetchBroadcast(String broadcastId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(broadcastId), "BroadcastId id should not be null");
        return broadcastJpaDAO.fetchBroadcast(broadcastId);
    }

    /**
     * @param shortReference
     *            - Short reference associated with property listing
     */
    public Broadcast fetchByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isNull(shortReference), "Broadcast short reference should not be null");
        return broadcastJpaDAO.fetchByShortReference(shortReference);
    }

    /**
     * @param Broadcast
     *            - Broadcast object
     */
    public void saveBroadcast(Broadcast Broadcast)
    {
        Validation.isTrue(!Utils.isNull(Broadcast), "Property listing should not be null");
        broadcastJpaDAO.saveBroadcast(Broadcast);
    }

    public List<Broadcast> getBroadcastByBroadcastIds(List<String> listbroadcastIds, BroadcastStatus status)
    {
        Validation.isTrue(!Utils.isNull(listbroadcastIds), "broadcast Id should not be null");
        Validation.isTrue(!Utils.isNull(status), "status should not be null");

        return broadcastJpaDAO.getBroadcastByBroadcastIds(listbroadcastIds, status);
    }

    public Broadcast getBroadcastByMessageId(String messageId)
    {
        Validation.isTrue(!Utils.isNull(messageId), "Message id should not be null");
        return broadcastJpaDAO.getBroadcastByMessageId(messageId);
    }

    public Broadcast getBroadcastByBroadcastId(String broadcastId)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "broadcast id should not be null");
        return broadcastJpaDAO.getBroadcastByBroadcastId(broadcastId);
    }

    public String fetchBroadcastIdByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isNull(shortReference), "Broadcast short reference should not be null");
        return broadcastJpaDAO.fetchBroadcastIdByShortReference(shortReference);
    }

    public String isBroadcastExist(String broadcastId)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "Broadcast id should not be null");
        return broadcastJpaDAO.isBroadcastExist(broadcastId);
    }

    public long fetchCount()
    {
        long count = broadcastJpaDAO.fetchCount();
        return count;
    }

    public List<BroadcastTagsConsolidated> fetchBroadcastsByCityAndLocation(List<String> cityIds, List<String> locationIds, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter,
            int start, int rows)
    {
        return broadcastTagJpaDAO.fetchBroadcastsByCityAndLocation(cityIds, locationIds, sort, filter, start, rows);
    }

    public List<BroadcastCountObject> fetchBroadcastCountByAccountIds(List<String> accountIds)
    {
        Validation.isTrue(!Utils.isNull(accountIds), "Account ids should not be null");
        return broadcastJpaDAO.fetchBroadcastCountByAccountIds(accountIds);

    }

    public List<Broadcast> fetchBroadcastByMessageCreatedBy(String createdBy, BroadcastStatus status)
    {
        return broadcastJpaDAO.fetchBroadcastByMessageCreatedBy(createdBy, status);

    }

    public List<Broadcast> fetchBroadcastByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Account id should not be null");
        return broadcastJpaDAO.fetchBroadcastByAccountId(accountId);
    }

    public void removeRelationOfBroadcastAndMessageUpdateStatus(String broadcastId, String messageId, BroadcastStatus broadcastStatus)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "Broadcast id should not be null");
        Validation.isTrue(!Utils.isNull(messageId), "Message id should not be null");
        broadcastJpaDAO.removeRelationOfBroadcastAndMessageUpdateStatus(broadcastId, messageId, broadcastStatus);
    }

    public void updateBroadcastViewCount(List<String> broadcastIdList)
    {
        Validation.isTrue(!Utils.isNull(broadcastIdList), "Broadcast id list should not be null");
        broadcastJpaDAO.updateBroadcastViewCount(broadcastIdList);
    }

    public Set<String> fetchBroadcast(String accountId, long start, long end)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Broadcast id list should not be null");
        Validation.isTrue(!Utils.isNull(start), "start should not be null");
        Validation.isTrue(!Utils.isNull(end), "End should not be null");
        return broadcastCacheDAO.fetchBroadcast(accountId, start, end);
    }

    public void saveBroadcastBulk(Set<PipelineCacheEntity> broadcastPipelineSet)
    {
        Validation.isTrue(!Utils.isNull(broadcastPipelineSet), "Broadcast id list should not be null");
        broadcastCacheDAO.saveBroadcastBulk(broadcastPipelineSet);
    }

    public void removeAllBroadcast(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");
        broadcastCacheDAO.removeAllBroadcast(accountId);

    }

    public List<BroadcastTagCountObject> fetchBroadcastTagCountByAccountIds(Set<String> accountIds, BroadcastTagStatus broadcastTagStatus, BroadcastStatus broadcastStatus)
    {
        Validation.isTrue(!Utils.isNull(accountIds), "Account ids should not be null");
        return broadcastJpaDAO.fetchBroadcastTagCountByAccountIds(accountIds, broadcastTagStatus, broadcastStatus);
    }

}
