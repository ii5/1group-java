package one.group.dao.impl;

import java.util.List;

import one.group.dao.TargetedMarketingListDAO;
import one.group.entities.jpa.TargetedMarketingList;
import one.group.jpa.dao.TargetedMarketingListJpaDAO;

/**
 * 
 * @author sanilshet
 *
 */
public class TargetedMarketingListDAOImpl implements TargetedMarketingListDAO
{
    TargetedMarketingListJpaDAO targetedMarketingListJpaDAO;

    public TargetedMarketingListJpaDAO getTargetedMarketingListJpaDAO()
    {
        return targetedMarketingListJpaDAO;
    }

    public void setTargetedMarketingListJpaDAO(TargetedMarketingListJpaDAO targetedMarketingListJpaDAO)
    {
        this.targetedMarketingListJpaDAO = targetedMarketingListJpaDAO;
    }

    public List<TargetedMarketingList> fetchAllTargetedList()
    {
        return targetedMarketingListJpaDAO.fetchAllTargetedList();
    }

    public void deleteTargetedListByPhoneNumber(String phoneNumber)
    {
        targetedMarketingListJpaDAO.deleteTargetedListByPhoneNumber(phoneNumber);
    }

    public void deleteAllTargetedList()
    {
        targetedMarketingListJpaDAO.deleteAllTargetedList();
    }

}
