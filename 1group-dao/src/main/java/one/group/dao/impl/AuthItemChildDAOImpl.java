/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;

import one.group.dao.AuthItemChildDAO;
import one.group.entities.jpa.AuthItemChild;
import one.group.jpa.dao.AuthItemChildJpaDAO;

public class AuthItemChildDAOImpl implements AuthItemChildDAO
{
    private AuthItemChildJpaDAO authItemChildJpaDAO;

    public AuthItemChildJpaDAO getAuthItemChildJpaDAO()
    {
        return authItemChildJpaDAO;
    }

    public void setAuthItemChildJpaDAO(AuthItemChildJpaDAO authItemChildJpaDAO)
    {
        this.authItemChildJpaDAO = authItemChildJpaDAO;
    }

    public void saveAuthItemChild(AuthItemChild authItemChild)
    {

    }
    
    public Collection<AuthItemChild> fetchAll()
    {
        return authItemChildJpaDAO.fetchAll();
    }
}
