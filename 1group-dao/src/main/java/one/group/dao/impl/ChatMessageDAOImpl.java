package one.group.dao.impl;

import java.util.Set;

import one.group.cache.dao.ChatMessageCacheDAO;
import one.group.dao.ChatMessageDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;
import redis.clients.jedis.Tuple;

public class ChatMessageDAOImpl implements ChatMessageDAO
{
    private ChatMessageCacheDAO chatMessageCacheDAO;

    public ChatMessageCacheDAO getChatMessageCacheDAO()
    {
        return chatMessageCacheDAO;
    }

    public void setChatMessageCacheDAO(final ChatMessageCacheDAO chatMessageCacheDAO)
    {
        this.chatMessageCacheDAO = chatMessageCacheDAO;
    }

    public Set<String> fetchChatMessagesOfChatThread(final String chatThreadId, final int offset, final int limit)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        Validation.isTrue(!Utils.isNull(offset), "Offset should not be null");
        Validation.isTrue(!Utils.isNull(limit), "Offset should not be null");
        return chatMessageCacheDAO.fetchChatMessagesOfChatThread(chatThreadId, offset, limit);
    }

    public Set<Tuple> fetchLastChatMessageOfChatThread(final String chatThreadId)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        return chatMessageCacheDAO.fetchLastChatMessageOfChatThread(chatThreadId);
    }

    public String saveChatMessage(final String chatThreadId, final String chatMessage)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        Validation.isTrue(!Utils.isEmpty(chatMessage), "Chat message is should not be null");
        return chatMessageCacheDAO.saveChatMessage(chatThreadId, chatMessage);
    }

    public Double fetchChatMessageIndex(final String chatThreadId, final String chatMessageKey)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        Validation.isTrue(!Utils.isEmpty(chatMessageKey), "Chat message key is should not be null");
        return chatMessageCacheDAO.fetchChatMessageIndex(chatThreadId, chatMessageKey);
    }

    public String fetchChatMessageContent(final String chatThreadId, final String chatMessageKey)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        Validation.isTrue(!Utils.isEmpty(chatMessageKey), "Chat message key is should not be null");
        return chatMessageCacheDAO.fetchChatMessageContent(chatThreadId, chatMessageKey);
    }

    public long fetchChatMessageCount(String chatThreadId)
    {
        Validation.isTrue(!Utils.isEmpty(chatThreadId), "Chat thread is should not be null");
        return chatMessageCacheDAO.fetchChatMessageCount(chatThreadId);
    }

    public Object saveChatMessage(String chatThreadId, String fromAccountId, String toAccountId, String chatMessageJson)
    {
        return chatMessageCacheDAO.saveChatMessageUsingLua(chatThreadId, fromAccountId, toAccountId, chatMessageJson);
    }
}
