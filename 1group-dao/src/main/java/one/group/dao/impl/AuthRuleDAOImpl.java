/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.AuthRuleDAO;
import one.group.entities.jpa.AuthRule;
import one.group.jpa.dao.AuthRuleJpaDAO;

public class AuthRuleDAOImpl implements AuthRuleDAO
{
    private AuthRuleJpaDAO authRuleJpaDAO;

    public AuthRuleJpaDAO getAuthRuleJpaDAO()
    {
        return authRuleJpaDAO;
    }

    public void setAuthRuleJpaDAO(AuthRuleJpaDAO authRuleJpaDAO)
    {
        this.authRuleJpaDAO = authRuleJpaDAO;
    }

    public void saveAuthRule(AuthRule authRule)
    {
        authRuleJpaDAO.saveAuthRule(authRule);
    }

}
