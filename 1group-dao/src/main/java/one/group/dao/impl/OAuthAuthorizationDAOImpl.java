package one.group.dao.impl;

import one.group.dao.OAuthAuthorizationDAO;
import one.group.entities.jpa.OauthAuthorization;
import one.group.jpa.dao.OAuthAuthorizationJpaDAO;

public class OAuthAuthorizationDAOImpl implements OAuthAuthorizationDAO
{

    private OAuthAuthorizationJpaDAO oAuthAuthorizationJpaDAO;

    public OAuthAuthorizationJpaDAO getoAuthAuthorizationJpaDAO()
    {
        return oAuthAuthorizationJpaDAO;
    }

    public void setoAuthAuthorizationJpaDAO(OAuthAuthorizationJpaDAO oAuthAuthorizationJpaDAO)
    {
        this.oAuthAuthorizationJpaDAO = oAuthAuthorizationJpaDAO;
    }

    public void saveClientDetail(OauthAuthorization oauthAuthorization)
    {
        oAuthAuthorizationJpaDAO.persist(oauthAuthorization);
    }

    public OauthAuthorization loadClientByClientId(String clientId)
    {
        return oAuthAuthorizationJpaDAO.getOauthAuthorizationByClientId(clientId);
    }

    public OauthAuthorization getClient(String clientId)
    {
        return oAuthAuthorizationJpaDAO.getOauthAuthorizationByClientId(clientId);
    }

}
