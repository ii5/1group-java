package one.group.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.dao.AccountCacheDAO;
import one.group.cache.dao.EntityCountCacheDAO;
import one.group.core.enums.EntityType;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.NativeContacts;
import one.group.entities.jpa.helpers.AccountObject;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.AccountRelationsJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.jpa.dao.MediaJpaDAO;
import one.group.jpa.dao.NativeContactsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.ScanResult;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class AccountDAOImpl implements AccountDAO
{
    private static final Logger logger = LoggerFactory.getLogger(AccountDAOImpl.class);

    private AccountCacheDAO accountCacheDAO;

    private AccountJpaDAO accountJpaDAO;

    private AccountRelationsJpaDAO accountRelationsJpaDAO;

    private NativeContactsJpaDAO nativeContactsJpaDAO;

    private ClientJpaDAO clientJpaDAO;

    private MediaJpaDAO mediaJpaDAO;

    private EntityCountCacheDAO entityCountCacheDAO;

    /**
     * @return the mediaJpaDAO
     */
    public MediaJpaDAO getMediaJpaDAO()
    {
        return mediaJpaDAO;
    }

    /**
     * @param mediaJpaDAO
     *            the mediaJpaDAO to set
     */
    public void setMediaJpaDAO(MediaJpaDAO mediaJpaDAO)
    {
        this.mediaJpaDAO = mediaJpaDAO;
    }

    public AccountRelationsJpaDAO getAccountRelationsJpaDAO()
    {
        return accountRelationsJpaDAO;
    }

    public void setAccountRelationsJpaDAO(AccountRelationsJpaDAO accountRelationsJpaDAO)
    {
        this.accountRelationsJpaDAO = accountRelationsJpaDAO;
    }

    public NativeContactsJpaDAO getNativeContactsJpaDAO()
    {
        return nativeContactsJpaDAO;
    }

    public void setNativeContactsJpaDAO(NativeContactsJpaDAO nativeContactsJpaDAO)
    {
        this.nativeContactsJpaDAO = nativeContactsJpaDAO;
    }

    public AccountCacheDAO getAccountCacheDAO()
    {
        return accountCacheDAO;
    }

    public AccountJpaDAO getAccountJpaDAO()
    {
        return accountJpaDAO;
    }

    public ClientJpaDAO getClientJpaDAO()
    {
        return clientJpaDAO;
    }

    public void setAccountCacheDAO(AccountCacheDAO accountCacheDAO)
    {
        this.accountCacheDAO = accountCacheDAO;
    }

    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
    {
        this.accountJpaDAO = accountJpaDAO;
    }

    public void setClientJpaDAO(ClientJpaDAO clientJpaDAO)
    {
        this.clientJpaDAO = clientJpaDAO;
    }

    public EntityCountCacheDAO getEntityCountCacheDAO()
    {
        return entityCountCacheDAO;
    }

    public void setEntityCountCacheDAO(EntityCountCacheDAO entityCountCacheDAO)
    {
        this.entityCountCacheDAO = entityCountCacheDAO;
    }

    public Set<String> fetchOnlineAccounts()
    {
        return accountCacheDAO.fetchOnlineAccounts();
    }

    public void deleteOnlineKey(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.deleteOnlineKey(accountId);
    }

    public void createOnlineKey(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.createOnlineKey(accountId);
    }

    public Long fetchOnlineAccountTime(String accountId)
    {
        return accountCacheDAO.fetchOnlineAccountTime(accountId);
    }

    public Account fetchAccountByPhoneNumber(String mobileNumber)
    {
        return accountJpaDAO.fetchAccountByPhoneNumber(mobileNumber);
    }

    public List<Account> getAllAccounts()
    {
        return accountJpaDAO.getAllAccounts();
    }

    public Account fetchAccountById(String accountId)
    {
        return accountJpaDAO.findById(accountId);
    }

    public Account updateAccount(String accountId, String firstName, String lastName, Status status, City city)
    {
        Account account = accountJpaDAO.findById(accountId);
        account.setStatus(status);
        account.setFullName(firstName);
        account.setCompanyName(lastName);
        account.setCity(city);
        // account.setType(AccountType.AGENT);

        accountJpaDAO.saveOrUpdate(account);

        return account;

    }

    public Account updateAccountMedia(String accountId, String mediaId)
    {
        // TODO Auto-generated method stub
        Account account = accountJpaDAO.findById(accountId);
        Media media = mediaJpaDAO.findById(mediaId);
        if (media.getDimension().equals(MediaDimensionType.FULL))
        {
            account.setFullPhoto(media);
        }
        else
        {
            account.setThumbnailPhoto(media);
        }

        accountJpaDAO.saveOrUpdate(account);

        return account;

    }

    public Account saveAccount(Account account)
    {
        return accountJpaDAO.saveOrUpdate(account);
    }

    public void persistAccount(Account account)
    {
        accountJpaDAO.persist(account);
    }

    public Account fetchAccountOfClient(String clientId)
    {
        return accountJpaDAO.getAccountFromClientId(clientId);
    }

    public Account fetchShortReferenceById(String accountId)
    {
        return accountJpaDAO.fetchShortReferenceById(accountId);

    }

    public Account fetchAccountByShortReference(String shortReference)
    {
        return accountJpaDAO.fetchAccountByShortReference(shortReference);
    }

    public void saveNativeContacts(Account account, String fullName, String companyName, String phoneNumber)
    {
        nativeContactsJpaDAO.saveNativeContacts(account, fullName, companyName, phoneNumber);

    }

    public void createAccountRelation(AccountRelations accountRelations)
    {
        accountRelationsJpaDAO.createAccountRelation(accountRelations);

    }

    public void updateAccountRelation(AccountRelations accountRelations)
    {
        accountRelationsJpaDAO.updateAccountRelation(accountRelations);

    }

    public Set<AccountRelations> fetchAccountRelations(Account account)
    {
        return accountRelationsJpaDAO.fetchAccountRelations(account);
    }

    public Set<Account> fetchAccountsLinkedWithPhonenumber(String phoneNumber)
    {
        return nativeContactsJpaDAO.fetchAccountsLinkedWithPhonenumber(phoneNumber);
    }

    public Set<String> fetchPhonenumbersLinkedWithAccount(Account account)
    {
        return nativeContactsJpaDAO.fetchPhonenumbersLinkedWithAccount(account);
    }

    public void createAccountBlockByKey(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.createAccountBlockByKey(accountId, requestByAccountId);

    }

    public void deleteAccountBlockByKey(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        accountCacheDAO.removeAccountFromBlockedBy(accountId, requestByAccountId);
    }

    public ScanResult<String> fetchAccountsToSendSmsNotification(String cursor, String pattern, int count)
    {
        return accountCacheDAO.fetchAccountsToSendSmsNotification(cursor, pattern, count);
    }

    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp)
    {
        accountCacheDAO.updateChatSmsNotificationTimestamp(accountId, timestampKey, timestamp);
    }

    public Map<String, String> fetchChatSmsNotificationTimestamp(String accountKey)
    {
        return accountCacheDAO.fetchChatSmsNotificationTimestamp(accountKey);
    }

    public NativeContacts updateNativeContacts(NativeContacts nativeContact)
    {
        return nativeContactsJpaDAO.merge(nativeContact);
    }

    public NativeContacts fetchNativeContacts(Account account, String phoneNumber)
    {
        return nativeContactsJpaDAO.fetchContactsWithPhonenumberAndAccount(phoneNumber, account);
    }

    public List<NativeContacts> fetchNativeContactsForMultiplePhoneNumbers(Account account, Set<String> phoneNumbers)
    {
        return nativeContactsJpaDAO.fetchContactsWithPhonenumbersAndAccount(phoneNumbers, account);
    }

    public void persistAllNativeContacts(List<NativeContacts> nativeContacts)
    {
        nativeContactsJpaDAO.persistAll(nativeContacts);
    }

    public Set<Account> fetchAccountsByPhoneNumbers(Set<String> mobileNumbers)
    {
        return accountJpaDAO.fetchAccountsByPhoneNumbers(mobileNumbers);

    }

    public String fetchCount()
    {
        String count = entityCountCacheDAO.getEntityCount(EntityType.ACCOUNT);
        return count;
    }

    public void updateCount(String count)
    {
        entityCountCacheDAO.updateEntityCount(count, EntityType.ACCOUNT);
    }

    public List<Account> fetchAllAccountsByStatus(Status status)
    {
        return accountJpaDAO.fetchAllAccountsByStatus(status);
    }

    public void updateAccount(Account account)
    {
        accountJpaDAO.saveOrUpdate(account);
    }

    public void saveAccountInviteCodeInCache(String accountId, String inviteCode, int expiryTime)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id should not be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(inviteCode), "Invite code should not be null or empty.");
        Validation.isTrue(!Utils.isNull(expiryTime), "Expiry time passed should not be null or empty.");

        accountCacheDAO.saveAccountInviteCode(accountId, inviteCode, expiryTime);
    }

    public String fetchAccountInviteCodeFromCache(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id should not be null or empty.");

        return accountCacheDAO.fetchInviteCodeByAccount(accountId);
    }

    public List<Account> fetchAllStatusOfNativeContactsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id should not be null or empty.");

        return accountJpaDAO.fetchAllStatusOfNativeContactsByAccountId(accountId);
    }

    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds)
    {
        return accountJpaDAO.fetchAllAccountIdsSubscribedToLocations(locationIds);
    }

    public List<AccountObject> fetchAllAccountIdsByStatus(Status status)
    {
        Validation.isTrue(!Utils.isNull(status), "Status should not be null");

        return accountJpaDAO.fetchAllAccountIdsByStatus(status);
    }

    public List<Account> fetchAllAccountByIds(Set<String> accountIds)
    {
        Validation.isTrue(!Utils.isNull(accountIds), "Account ids should not be null");

        return accountJpaDAO.fetchAllAccountByIds(accountIds);
    }

    public Account fetchAccountByUserId(String userId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Account fetchAccountBywaPhoneNumber(String waPhoneNumber)
    {
        return null;
    }

    public List<Account> fetchAccountByWaSyncStatus(String waSyncStatus)
    {
        return null;
    }

    public List<Account> fetchAccountByWaGroupSelectionStatus(String waGroupSelectionStatus)
    {
        return null;
    }

    public int getActiveGroupCountByUserId(long userId)
    {
        return 0;
    }

    public int getApprovedGroupCountByUserId(long userId)
    {
        return 0;
    }

    public Set<String> fetchAllPhoneNumbersOfNativeContactsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id should not be null or empty.");

        return accountJpaDAO.fetchAllPhoneNumbersOfNativeContactsByAccountId(accountId);
    }

    public Account fetchAccountByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id should not be null or empty.");
        return accountJpaDAO.fetchAccountByAccountId(accountId);
    }
    
    public List<Account> fetchSelectiveDataAccountsByIds(Set<String> accountIds)
    {
    	return accountJpaDAO.fetchSelectiveDataAccountsByIds(accountIds);
    }

}
