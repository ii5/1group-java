package one.group.dao.impl;

import one.group.dao.AmenityDAO;
import one.group.entities.jpa.Amenity;
import one.group.jpa.dao.AmenityJpaDAO;

public class AmenityDAOImpl implements AmenityDAO
{

    private AmenityJpaDAO amenityJpaDAO;

    public AmenityJpaDAO getAmenityJpaDAO()
    {
        return amenityJpaDAO;
    }

    public void setAmenityJpaDAO(AmenityJpaDAO amenityJpaDAO)
    {
        this.amenityJpaDAO = amenityJpaDAO;
    }

    public Amenity findAmenityById(String amenityId)
    {
        return amenityJpaDAO.findById(amenityId);
    }

    public Amenity findAmenityByName(String name)
    {
        return amenityJpaDAO.findByName(name);
    }

}
