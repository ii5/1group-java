/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.CityDAO;
import one.group.dao.UserDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.User;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.UserJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class UserDAOImpl implements UserDAO
{
    private AccountJpaDAO accountJpaDAO;
    private CityDAO cityDAO;
    private UserJpaDAO userJpaDAO;

    public UserJpaDAO getUserJpaDAO()
    {
        return userJpaDAO;
    }

    public void setUserJpaDAO(UserJpaDAO userJpaDAO)
    {
        this.userJpaDAO = userJpaDAO;
    }

    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public AccountJpaDAO getAccountJpaDAO()
    {
        return accountJpaDAO;
    }

    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
    {
        this.accountJpaDAO = accountJpaDAO;
    }

    public List<User> fetchUserByCityId(String cityId)
    {
        return null;
    }

    public User saveUser(User user)
    {
        return this.userJpaDAO.saveUser(user);
    }

    public User fetchUserByUserId(String userId)
    {
        return userJpaDAO.fetchUserByUserId(userId);
    }

    public User fetchUserByEmail(String email)
    {
        return null;
    }

    public User fetchUserByShortRef(String shortRef)
    {
        return null;
    }
    
    public Account fetchUserByNumber(String mobileNumber)
    {
        return accountJpaDAO.fetchUserByNumber(mobileNumber);
    }
    
    public void saveAll(Collection<User> userCollection)
    {
        userJpaDAO.saveAll(userCollection);
    }

}
