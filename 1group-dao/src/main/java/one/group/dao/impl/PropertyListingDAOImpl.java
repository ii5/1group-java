package one.group.dao.impl;

import java.util.List;
import java.util.Set;

import one.group.cache.dao.EntityCountCacheDAO;
import one.group.core.enums.EntityType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.PropertyListingDAO;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.helpers.PropertyListingCountObject;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class PropertyListingDAOImpl implements PropertyListingDAO
{

    private PropertyListingJpaDAO propertyListingJpaDAO;

    private EntityCountCacheDAO entityCountCacheDAO;

    public PropertyListingJpaDAO getPropertyListingJpaDAO()
    {
        return propertyListingJpaDAO;
    }

    public void setPropertyListingJpaDAO(PropertyListingJpaDAO propertyListingJpaDAO)
    {
        this.propertyListingJpaDAO = propertyListingJpaDAO;
    }

    public EntityCountCacheDAO getEntityCountCacheDAO()
    {
        return entityCountCacheDAO;
    }

    public void setEntityCountCacheDAO(EntityCountCacheDAO entityCountCacheDAO)
    {
        this.entityCountCacheDAO = entityCountCacheDAO;
    }

    /**
     * @param accountId
     * @param statusList
     */
    public List<PropertyListing> fetchPropertyListingbyAccountId(String accountId, List<BroadcastStatus> statusList)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        return propertyListingJpaDAO.fetchAllPropertyListingByAccountId(accountId, statusList);
    }

    /**
     * @param accountId
     * @param statusList
     */
    public long fetchPropertyListingCountByAccountId(String accountId, List<BroadcastStatus> statusList)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        return propertyListingJpaDAO.fetchPropertyListingCountByAccountId(accountId, statusList);
    }

    /**
     * @param shortReference
     */
    public PropertyListing fetchPropertyListingByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isEmpty(shortReference), "Short reference should not be null");
        return propertyListingJpaDAO.fetchPropertyListingByShortReference(shortReference);
    }

    /**
     * @param propertyListingId
     */
    public PropertyListing fetchPropertyListing(String propertyListingId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(propertyListingId), "Property listing id should not be null");
        return propertyListingJpaDAO.fetchPropertyListing(propertyListingId);
    }

    /**
     * @param propertyListing
     *            - Property listing object
     */
    public void renewPropertyListing(PropertyListing propertyListing)
    {
        Validation.isTrue(!Utils.isNull(propertyListing), "Property listing should not be null");
        propertyListingJpaDAO.renewPropertyListing(propertyListing);
    }

    /**
     * @param propertyListing
     *            - Property listing object
     */
    public void savePropertyListing(PropertyListing propertyListing)
    {
        Validation.isTrue(!Utils.isNull(propertyListing), "Property listing should not be null");
        propertyListingJpaDAO.savePropertyListing(propertyListing);
    }

    /**
     * @param shortReference
     *            - Short reference associated with property listing
     */
    public PropertyListing fetchByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isNull(shortReference), "Property listing short reference should not be null");
        return propertyListingJpaDAO.fetchByShortReference(shortReference);
    }

    /**
     * @param locationId
     *            - Location id associated with property listing
     * 
     */
    public List<String> fetchPropertiesByLocation(String locationId)
    {
        Validation.notNull(locationId, "Location ID passed should not be null.");
        return propertyListingJpaDAO.fetchPropertiesByLocation(locationId);
    }

    /**
     * Get active property listings
     */
    public List<PropertyListing> getActivePropertyListings()
    {
        return propertyListingJpaDAO.getActivePropertyListings();
    }

    /**
     * Get active and expired property listings
     */
    public List<PropertyListing> getActiveAndExpiredPropertyListings()
    {
        return propertyListingJpaDAO.getActiveAndExpiredPropertyListings();
    }

    /**
     * Search property listing by search criteria
     * 
     * @param accountId
     * @param locations
     * @param transactionType
     * @param markets
     * @param rooms
     * @param amenities
     * @param minArea
     * @param maxArea
     * @param minRentPrice
     * @param maxRentPrice
     * @param minSalePrice
     * @param maxSalePrice
     * @param propertyTypes
     * @param propertySubTypes
     * 
     */
    public List<PropertyListing> searchPropertyListings(String accountId, List<Location> locations, String transactionType, List<PropertyMarket> markets, List<String> rooms, List<Amenity> amenities,
            int minArea, int maxArea, long minRentPrice, long maxRentPrice, long minSalePrice, long maxSalePrice, List<PropertyType> propertyTypes, List<PropertySubType> propertySubTypes)
    {
        Validation.notNull(locations, "Locations passed should not be null");
        return propertyListingJpaDAO.searchPropertyListings(accountId, locations, transactionType, markets, rooms, amenities, minArea, maxArea, minRentPrice, maxRentPrice, minSalePrice, maxSalePrice,
                propertyTypes, propertySubTypes);
    }

    /**
     * @param accountId
     *            - Account Id of the requesting account
     */
    public List<PropertyListing> fetchPropertyListingbyAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        return propertyListingJpaDAO.fetchAllPropertyListingByAccountId(accountId);
    }

    public void markOtherPropertyListingsAsUnHot(String accountId, String propertyListingId)
    {
        propertyListingJpaDAO.markOtherPropertyListingsAsUnHot(accountId, propertyListingId);

    }

    public List<PropertyListingScoreObject> searchPropertyListing(List<Location> locationList, BroadcastStatus status)
    {
        return propertyListingJpaDAO.searchPropertyListing(locationList, status);
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationList, BroadcastStatus status, int maxResults)
    {
        return propertyListingJpaDAO.searchPropertyListingByLocationIds(locationList, status, maxResults);
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIds(List<String> accountIdList, List<Location> locationList, BroadcastStatus status)
    {
        return propertyListingJpaDAO.fetchAllPropertyListingsByAccountIds(accountIdList, locationList, status);
    }

    public void updatePropertyListingCount(String entityCount, EntityType entityType)
    {
        entityCountCacheDAO.updateEntityCount(entityCount, entityType);
    }

    public String getPropertyListingCountFromCache(EntityType entityType)
    {
        return entityCountCacheDAO.getEntityCount(entityType);
    }

    public Long fetchActivePropertyListingCount()
    {
        return propertyListingJpaDAO.fetchActivePropertyListingCount();
    }

    public List<PropertyListingScoreObject> searchPropertyListingByExactAndNearBy(String locationId, BroadcastStatus status)
    {
        return propertyListingJpaDAO.searchPropertyListingByExactAndNearBy(locationId, status);
    }

    public List<PropertyListing> fetchPropertyListingsByStatus(List<BroadcastStatus> propertyStatus)
    {
        return propertyListingJpaDAO.fetchPropertyListingsByStatus(propertyStatus);
    }

    public List<String> fetchPropertyListingIdsByStatus(List<BroadcastStatus> propertyStatus)
    {
        return propertyListingJpaDAO.fetchPropertyListingIdsByStatus(propertyStatus);
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByStatus(BroadcastStatus status)
    {
        return propertyListingJpaDAO.fetchAllPropertyListingsByStatus(status);
    }

    public long getPropertyListingCountByStatus(BroadcastStatus status)
    {
        return propertyListingJpaDAO.fetchPropertyListingCountByStatus(status);
    }

    public List<PropertyListing> fetchPropertyListingsByIdsAndStatus(List<String> propertyListingIds, List<BroadcastStatus> propertyStatus)
    {
        return propertyListingJpaDAO.fetchPropertyListingsByIdsAndStatus(propertyListingIds, propertyStatus);
    }

    public Long fetchAllPropertyListingCount()
    {
        return propertyListingJpaDAO.fetchAllPropertyListingCount();
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIdsAndLocationIds(List<String> accountIdList, List<String> locationIdList, BroadcastStatus status)
    {
        Validation.isTrue(!Utils.isNull(accountIdList), "Account id list should not be null");
        Validation.isTrue(!Utils.isNull(locationIdList), "Location id list should not be null");
        Validation.isTrue(!Utils.isNull(status), "Status should not be null");

        return propertyListingJpaDAO.fetchAllPropertyListingsByAccountIdsAndLocationIds(accountIdList, locationIdList, status);
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status)
    {
        Validation.isTrue(!Utils.isNull(locationIdList), "Location id list should not be null");
        Validation.isTrue(!Utils.isNull(status), "Status should not be null");
        return propertyListingJpaDAO.searchPropertyListingByLocationIds(locationIdList, status);
    }

    public List<PropertyListingCountObject> fetchPropertyListingCountOfAccountsByStatus(Set<String> accountIds, BroadcastStatus status)
    {
        Validation.isTrue(!Utils.isNull(accountIds), "AccountId set should not be null");
        Validation.isTrue(!Utils.isNull(status), "Status should not be null");

        return propertyListingJpaDAO.fetchPropertyListingCountOfAccountsByStatus(accountIds, status);
    }

    public List<PropertyListing> fetchPropertyListingsByPropertyListingds(List<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");
        return propertyListingJpaDAO.fetchPropertyListingsByPropertyListingds(propertyListingIds);

    }

    public List<String> fetchPropertyListingIdsByPropertyListingIdsAndStatus(List<String> propertyListingIds, BroadcastStatus status)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");
        Validation.isTrue(!Utils.isNull(status), "Status should not be null");

        return propertyListingJpaDAO.fetchPropertyListingIdsByPropertyListingIdsAndStatus(propertyListingIds, status);
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status, int maxResults, String accountId)
    {
        return propertyListingJpaDAO.searchPropertyListingByLocationIds(locationIdList, status, maxResults, accountId);
    }

    public List<PropertyListing> fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(Set<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");
        return propertyListingJpaDAO.fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(propertyListingIds);
    }

    public List<PropertyListingScoreObject> fetchAccountsByPropertyListingIds(List<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");
        return propertyListingJpaDAO.fetchAccountsByPropertyListingIds(propertyListingIds);
    }

    public List<PropertyListingScoreObject> isPropertyListingsExistOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "account Id should not be null");
        return propertyListingJpaDAO.isPropertyListingsExistOfAccount(accountId);
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsForOwnAccountId(String accountId, BroadcastStatus status)
    {
        return propertyListingJpaDAO.fetchAllPropertyListingsForAccountId(accountId, status);
    }

}
