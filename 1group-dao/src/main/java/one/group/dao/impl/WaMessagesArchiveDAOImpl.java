/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.dao.WaMessagesArchiveDAO;
import one.group.entities.jpa.WaMessagesArchive;
import one.group.jpa.dao.WaMessagesArchiveJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesArchiveDAOImpl implements WaMessagesArchiveDAO
{
    private WaMessagesArchiveJpaDAO waMessagesArchiveJpaDAO;

    public WaMessagesArchiveJpaDAO getWaMessagesArchiveJpaDAO()
    {
        return waMessagesArchiveJpaDAO;
    }

    public void setWaMessagesArchiveJpaDAO(WaMessagesArchiveJpaDAO waMessagesArchiveJpaDAO)
    {
        this.waMessagesArchiveJpaDAO = waMessagesArchiveJpaDAO;
    }

    public List<WaMessagesArchive> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesArchive(WaMessagesArchive waMessagesArchive)
    {
        waMessagesArchiveJpaDAO.saveWaMessagesArchive(waMessagesArchive);
    }

    public WaMessagesArchive fetchWaMessagesArchiveByWaMessageId(String waMessageId)
    {
        return waMessagesArchiveJpaDAO.fetchWaMessagesArchiveByWaMessageId(waMessageId);
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByStatus(String status)
    {
        return null;
    }

    public boolean isWaMessagesArchiveDuplicate(String waMessageId)
    {
        return false;
    }

    
    public void saveWAMessagesArchive(Collection<WaMessagesArchive> waMessagesArchiveList) 
    {
    	waMessagesArchiveJpaDAO.saveWaMessagesArchive(waMessagesArchiveList);
    }
}
