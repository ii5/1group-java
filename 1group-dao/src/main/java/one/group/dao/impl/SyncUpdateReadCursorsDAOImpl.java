package one.group.dao.impl;

import java.util.List;

import one.group.dao.SyncUpdateReadCursorsDAO;
import one.group.entities.jpa.SyncUpdateReadCursors;
import one.group.jpa.dao.SyncUpdateReadCursorsJpaDAO;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SyncUpdateReadCursorsDAOImpl implements SyncUpdateReadCursorsDAO
{
    private SyncUpdateReadCursorsJpaDAO syncLogReadCursorsJpaDAO;

    public void delete(SyncUpdateReadCursors readCursor)
    {
        syncLogReadCursorsJpaDAO.delete(readCursor);
    }

    public void saveSyncLogReadCursors(SyncUpdateReadCursors readCursor)
    {
        syncLogReadCursorsJpaDAO.createSyncLogReadCursors(readCursor);
    }

    public SyncUpdateReadCursors fetchByClient(String clientId)
    {
        return syncLogReadCursorsJpaDAO.fetchByClient(clientId);
    }

    public SyncUpdateReadCursorsJpaDAO getSyncLogReadCursorsJpaDAO()
    {
        return syncLogReadCursorsJpaDAO;
    }

    public void setSyncLogReadCursorsJpaDAO(SyncUpdateReadCursorsJpaDAO syncLogReadCursorsJpaDAO)
    {
        this.syncLogReadCursorsJpaDAO = syncLogReadCursorsJpaDAO;
    }

    public List<SyncUpdateReadCursors> getUpdatesByClientIds(List<String> clients)
    {
        return syncLogReadCursorsJpaDAO.getUpdatesByClientIds(clients);
    }
}
