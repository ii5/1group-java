package one.group.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import one.group.core.enums.status.DeviceStatus;
import one.group.dao.ClientDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.helpers.ClientObject;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.ClientExceptionCode;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ClientDAOImpl implements ClientDAO
{

    private ClientJpaDAO clientJpaDAO;

    /**
     * @return the clientJpaDAO
     */
    public ClientJpaDAO getClientJpaDAO()
    {
        return clientJpaDAO;
    }

    /**
     * @param clientJpaDAO
     *            the clientJpaDAO to set
     */
    public void setClientJpaDAO(ClientJpaDAO clientJpaDAO)
    {
        this.clientJpaDAO = clientJpaDAO;
    }

    public List<Client> fetchAllClientsOfAccount(String accountId)
    {
        return clientJpaDAO.getAllClientsOfAccount(accountId);
    }

    public String getActiveClient(String accountId)
    {
        return clientJpaDAO.getActiveClient(accountId).getIdAsString();
    }

    public void saveClient(Client client)
    {
        clientJpaDAO.persist(client);
    }

    public List<String> fetchAllPushChannelsOfClientsOfAccount(String accountId)
    {
        List<Client> clientList = clientJpaDAO.getAllClientsOfAccount(accountId);

        List<String> clientPushChannelList = new ArrayList<String>();

        for (Client client : clientList)
        {
            clientPushChannelList.add(client.getPushChannel());
        }

        return clientPushChannelList;
    }

    public Client fetchClientById(String clientId)
    {
        return clientJpaDAO.findById(clientId);
    }

    public Client fetchByPushChannel(String pushChannel)
    {
        return clientJpaDAO.fetchByPushChannel(pushChannel);

    }

    public Account getAccountFromAuthToken(String authToken, String clientId) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        Client client = clientJpaDAO.findById(clientId);
        if (client == null)
        {
            throw new ClientProcessException(ClientExceptionCode.CLIENT_NOT_FOUND, false, authToken);
        }

        client.setLastActivityTime(new Date());
        client = clientJpaDAO.saveOrUpdate(client);

        Account account = client.getAccount();

        if (account == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, client.getIdAsString());
        }
        return account;
    }

    public List<Client> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform)
    {
        return clientJpaDAO.fetchByAccountAndDevicePlatform(accountId, devicePlatform);
    }

    public Client fetchByAccountIdAndDeviceToken(String accountId, String deviceToken)
    {
        return clientJpaDAO.fetchByAccountIdAndDeviceToken(accountId, deviceToken);
    }

    public List<Client> fetchByDeviceToken(String deviceId)
    {
        return clientJpaDAO.fetchByDeviceToken(deviceId);
    }

    public List<Client> fetchAllActiveClientsOfAccount(String accountId)
    {
        return clientJpaDAO.getAllActiveClientsOfAccount(accountId);
    }

    public List<Client> fetchAllActiveClients()
    {
        return clientJpaDAO.getAllActiveClients();
    }

    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate)
    {
        return clientJpaDAO.findClientForSchedulers(deviceStatus, beforeTimeDate);
    }

    public void deactivateClient(List<Client> clientList)
    {
        clientJpaDAO.deactivateClient(clientList);
    }

    public void purgeClient(List<Client> clientList)
    {
        clientJpaDAO.purgeClient(clientList);
    }

    public List<Client> getLastActivityTime(String accountId)
    {
        return clientJpaDAO.getLastActivityTime(accountId);
    }

    public List<ClientObject> getLastActivityOfAccounts(Set<String> accountIds)
    {
        Validation.isTrue(!Utils.isNull(accountIds), "AccountIds should not be null");

        return clientJpaDAO.getLastActivityOfAccounts(accountIds);
    }

    public String getAccountIdFromClientId(String clientId)
    {
        Validation.isTrue(!Utils.isNull(clientId), "ClientId should not be null");
        return clientJpaDAO.getAccountIdFromClientId(clientId);
    }
}