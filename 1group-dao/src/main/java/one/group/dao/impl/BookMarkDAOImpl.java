package one.group.dao.impl;

import java.util.List;

import one.group.core.enums.BookMarkType;
import one.group.dao.BookMarkDAO;
import one.group.entities.jpa.BookMark;
import one.group.jpa.dao.BookMarkJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class BookMarkDAOImpl implements BookMarkDAO
{
    private BookMarkJpaDAO bookMarkJpaDAO;

    /**
     * @return the bookMarkJpaDAO
     */
    public BookMarkJpaDAO getBookMarkJpaDAO()
    {
        return bookMarkJpaDAO;
    }

    /**
     * @param bookMarkJpaDAO
     *            the bookMarkJpaDAO to set
     */
    public void setBookMarkJpaDAO(BookMarkJpaDAO bookMarkJpaDAO)
    {
        this.bookMarkJpaDAO = bookMarkJpaDAO;
    }

    /**
     * 
     */
    public BookMark bookmark(String referenceId, String targetEntityId, BookMarkType type, String action)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Validation.isTrue(!Utils.isEmpty(targetEntityId), "Target entity should not be null");
        return bookMarkJpaDAO.bookmark(referenceId, targetEntityId, type, action);
    }

    /**
     * 
     */
    public List<BookMark> getBookMarksByReference(String referenceId, BookMarkType type)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        return bookMarkJpaDAO.getAllBookMarksByReference(referenceId, type);
    }

    public BookMark getBookMarkByReferenceAndTargetEntity(String referenceId, String targetEntityId)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        return bookMarkJpaDAO.getBookMarkByReferenceAndTargetEntity(referenceId, targetEntityId);
    }
	
	public List<BookMark> checkIsBookMarkedForPropertyListings(String accountId, List<String> propertyListings) 
	{	
		Validation.isTrue(!Utils.isEmpty(accountId), "accountId should not be null");
		return bookMarkJpaDAO.checkIsBookMarkedForPropertyListings(accountId, propertyListings);
	}
}
