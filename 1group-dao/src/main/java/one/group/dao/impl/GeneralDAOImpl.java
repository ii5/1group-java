package one.group.dao.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.dao.PaginationCacheDAO;
import one.group.core.enums.EntityFieldName;
import one.group.core.enums.EntityType;
import one.group.dao.GeneralDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponseFactory;
import one.group.jpa.dao.GeneralJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneralDAOImpl implements GeneralDAO
{
    private static final Logger logger = LoggerFactory.getLogger(GeneralDAOImpl.class);

    private PaginationCacheDAO paginationCacheDAO;

    private GeneralJpaDAO generalJpaDAO;

    public PaginationCacheDAO getPaginationCacheDAO()
    {
        return paginationCacheDAO;
    }

    public void setPaginationCacheDAO(PaginationCacheDAO paginationCacheDAO)
    {
        this.paginationCacheDAO = paginationCacheDAO;
    }

    public GeneralJpaDAO getGeneralJpaDAO()
    {
        return generalJpaDAO;
    }

    public void setGeneralJpaDAO(GeneralJpaDAO generalJpaDAO)
    {
        this.generalJpaDAO = generalJpaDAO;
    }

    public Set<String> saveAndFetchRecords(final String accountId, final EntityType type, final String queryString, final Map<EntityFieldName, Object> fieldVsValueMap, final String paginationKey,
            final int pageNo, final String refreshPaginationKey)
    {
        Validation.isTrue(!Utils.isNull(type), "Type must not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(queryString), "Query string must not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(paginationKey), "Pagination key must not be empty or null");
        Validation.isTrue(!Utils.isNull(pageNo), "Page no must not be null");

        String cachePaginationKey = paginationKey;

        boolean isExist = paginationCacheDAO.exists(cachePaginationKey);

        if (!isExist)
        {
            List<Object> all = generalJpaDAO.executeAndFetchRecords(queryString, fieldVsValueMap);
            if (!all.isEmpty())
            {

                cachePaginationKey = refreshPaginationKey;

                int seconds = 120;
                Set<PipelineCacheEntity> records = new HashSet<PipelineCacheEntity>();
                int count = 0;
                for (Object id : all)
                {
                    WSResponseFactory wsRF = new WSResponseFactory();
                    ResponseEntity rsEntity = wsRF.getEntity(type, id);

                    String entityJson = Utils.getJsonString(rsEntity);

                    PipelineCacheEntity entity = new PipelineCacheEntity(cachePaginationKey);
                    entity.setScore(count);
                    entity.setValue(entityJson);
                    records.add(entity);
                    count++;
                }

                paginationCacheDAO.saveRecords(records);
                paginationCacheDAO.expire(cachePaginationKey, seconds);
            }
        }

        int logicalPageNo = 0;
        if (pageNo >= 1)
        {
            logicalPageNo = pageNo - 1;
        }

        int noOfRecordsPerPage = 5;
        int start = logicalPageNo * noOfRecordsPerPage;
        int end = start + noOfRecordsPerPage - 1;
        logger.info(start + "-" + end);
        logger.info(cachePaginationKey);
        return paginationCacheDAO.fetchRecords(cachePaginationKey, start, end);
    }

    public long fetchTotalRecordsCount(String paginationKey)
    {
        return paginationCacheDAO.fetchTotalRecordsCount(paginationKey);
    }

}
