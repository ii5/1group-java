/**
 * 
 */
package one.group.dao.impl;

import one.group.dao.WaAccountsDAO;
import one.group.entities.jpa.WaAccounts;
import one.group.jpa.dao.WaAccountsJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaAccountsDAOImpl implements WaAccountsDAO
{
    WaAccountsJpaDAO waAccountsJpaDAO;

    public WaAccountsJpaDAO getWaAccountsJpaDAO()
    {
        return waAccountsJpaDAO;
    }

    public void setWaAccountsJpaDAO(WaAccountsJpaDAO waAccountsJpaDAO)
    {
        this.waAccountsJpaDAO = waAccountsJpaDAO;
    }

    public void saveWaAccounts(WaAccounts waAccounts)
    {
        waAccountsJpaDAO.saveWaAccounts(waAccounts);
    }

}
