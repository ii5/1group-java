/**
 * 
 */
package one.group.dao.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.BroadcastTagDAO;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.helpers.BroadcastObject;
import one.group.jpa.dao.BroadcastTagJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class BroadcastTagDAOImpl implements BroadcastTagDAO
{
    private BroadcastTagJpaDAO broadcastTagJpaDAO;

    public BroadcastTagJpaDAO getBroadcastTagJpaDAO()
    {
        return broadcastTagJpaDAO;
    }

    public void setBroadcastTagJpaDAO(BroadcastTagJpaDAO broadcastTagJpaDAO)
    {
        this.broadcastTagJpaDAO = broadcastTagJpaDAO;
    }

    public List<BroadcastTag> fetchBroadcastTagByBroadcastId(String broadcastId, BroadcastStatus status)
    {
        return broadcastTagJpaDAO.fetchBroadcastTagByBroadcastId(broadcastId, status);
    }

    public List<BroadcastTag> fetchBroadcastTagByBroadcastIds(Set<String> broadcastIds)
    {
        return broadcastTagJpaDAO.fetchBroadcastTagByBroadcastIds(broadcastIds);
    }

    public List<BroadcastTag> fetchBroadcastTagByBroadcastType(BroadcastType type)
    {
        return null;
    }

    public List<BroadcastTag> fetchBroadcastTagByLocationId(String location_id)
    {
        return null;
    }

    public void saveBroadcastTag(BroadcastTag broadcastTag)
    {
        broadcastTagJpaDAO.saveBroadcastTag(broadcastTag);
    }

    public BroadcastTag fetchBroadcastTagbyBroadcastTagId(String broadcastTagId)
    {
        return broadcastTagJpaDAO.fetchBroadcastTagbyBroadcastTagId(broadcastTagId);
    }

    public List<BroadcastObject> searchBroadcastTypePropertyListing(PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer minArea, Integer maxArea, Long maxPrice, Long minPrice, List<Rooms> roomList, PropertySubType propertSubType, List<BroadcastStatus> broadcastStatusList,
            List<BroadcastTagStatus> broadcastTagStatusList, int limit, int offset)
    {
        // Validation.isTrue(!Utils.isNull(propertType),
        // "Property type should not be null");
        // Validation.isTrue(!Utils.isNull(broadcastType),
        // "Broadcast type should not be null");
        // Validation.isTrue(!Utils.isNull(transactionType),
        // "Transaction type should not be null");
        Validation.isTrue(!Utils.isNull(cityId), "City id should not be null");
        // Validation.isTrue(!Utils.isNull(location),
        // "Location should not be null");
        // Validation.isTrue(!Utils.isNull(minArea),
        // "Min area should not be null");
        // Validation.isTrue(!Utils.isNull(maxArea),
        // "Max area should not be null");
        // Validation.isTrue(!Utils.isNull(minPrice),
        // "Min price should not be null");
        // Validation.isTrue(!Utils.isNull(maxPrice),
        // "Max price should not be null");
        return broadcastTagJpaDAO.searchBroadcastTypePropertyListing(propertType, broadcastType, transactionType, cityId, location, minArea, maxArea, maxPrice, minPrice, roomList, propertSubType,
                broadcastStatusList, broadcastTagStatusList, limit, offset);
    }

    public List<BroadcastObject> searchBroadcastTypeRequirement(PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer area, Long price)
    {
        // Validation.isTrue(!Utils.isNull(propertType),
        // "Property type should not be null");
        Validation.isTrue(!Utils.isNull(broadcastType), "Broadcast type should not be null");
        // Validation.isTrue(!Utils.isNull(transactionType),
        // "Transaction type should not be null");
        Validation.isTrue(!Utils.isNull(cityId), "City id should not be null");
        // Validation.isTrue(!Utils.isNull(location),
        // "Location should not be null");
        // Validation.isTrue(!Utils.isNull(minArea),
        // "Min area should not be null");
        // Validation.isTrue(!Utils.isNull(maxArea),
        // "Max area should not be null");
        // Validation.isTrue(!Utils.isNull(minPrice),
        // "Min price should not be null");
        // Validation.isTrue(!Utils.isNull(maxPrice),
        // "Max price should not be null");
        return broadcastTagJpaDAO.searchBroadcastTypeRequirement(propertType, broadcastType, transactionType, cityId, location, area, price);
    }

    public List<String> getBroadcastTagIdsByBroadcastId(String broadcastId)
    {
        Validation.isTrue(!Utils.isNull(broadcastId), "broadcastId should not be null");
        return broadcastTagJpaDAO.getBroadcastTagIdsByBroadcastId(broadcastId);
    }

    public void deleteBroadcasTagsByIds(List<String> broadcastTagids)
    {
        if (!broadcastTagids.isEmpty())
        {
            broadcastTagJpaDAO.deleteBroadcasTagsByIds(broadcastTagids);
        }
    }

    public List<BroadcastObject> searchBroadcastByCityId(Integer cityId, int offset, int limit)
    {
        Validation.isTrue(!Utils.isNull(cityId), "City id should not be null");
        Validation.isTrue(!Utils.isNull(offset), "Offset should not be null");
        Validation.isTrue(!Utils.isNull(limit), "Limit should not be null");
        return broadcastTagJpaDAO.searchBroadcastByCityId(cityId, offset, limit);
    }

    public void saveBroadcastTags(Collection<BroadcastTag> tagCollection)
    {
        broadcastTagJpaDAO.saveBroadcastTags(tagCollection);
    }

    public List<BroadcastTag> fetchBroadcastTagByIds(Set<String> broadcastTagIds)
    {
        return broadcastTagJpaDAO.getBroadcastTagsByIds(broadcastTagIds);
    }

    public Long fetchBroadcastCountByCity(String cityId)
    {
        return broadcastTagJpaDAO.fetchBroadcastCountByCity(cityId);
    }
}
