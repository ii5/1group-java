package one.group.dao.impl;

import one.group.dao.ConfigurationDAO;
import one.group.entities.jpa.Configuration;
import one.group.jpa.dao.ConfigurationJpaDAO;

/**
 * 
 * @author sanilshet
 *
 */
public class ConfigurationDAOImpl implements ConfigurationDAO
{

    private ConfigurationJpaDAO configurationJpaDAO;

    public ConfigurationJpaDAO getConfigurationJpaDAO()
    {
        return configurationJpaDAO;
    }

    public void setConfigurationJpaDAO(ConfigurationJpaDAO configurationJpaDAO)
    {
        this.configurationJpaDAO = configurationJpaDAO;
    }

    /**
     * @param key
     */
    public Configuration getConfiguationByKey(String key)
    {
        return configurationJpaDAO.getConfiguationByKey(key);
    }

    /**
     * @param config
     */
    public Configuration saveConfiguration(Configuration config)
    {
        return configurationJpaDAO.saveOrUpdate(config);
    }

    /**
     * @param config
     */
    public void remove(Configuration config)
    {
        configurationJpaDAO.remove(config);
    }

}
