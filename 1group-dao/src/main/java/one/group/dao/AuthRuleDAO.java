/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.AuthRule;

public interface AuthRuleDAO
{

    public void saveAuthRule(AuthRule authRule);
}
