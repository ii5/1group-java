package one.group.dao;

import java.util.List;

import one.group.entities.jpa.SmsMarketingLog;

/**
 * 
 * @author miteshchavda
 *
 */

public interface SMSMarketingLogDAO
{
    public void saveSMSMarketingLog(SmsMarketingLog smsMarketingLog);

    public List<SmsMarketingLog> fetchAllLog();
}
