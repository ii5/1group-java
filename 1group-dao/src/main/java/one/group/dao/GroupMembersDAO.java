/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.core.enums.GroupSource;
import one.group.entities.api.request.bpo.WSGroupMembersMetadataRequest;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.helpers.GroupMembersObject;
import one.group.exceptions.movein.Abstract1GroupException;

/**
 * @author ashishthorat
 *
 */
public interface GroupMembersDAO
{
    public List<GroupMembers> fetchGroupMembersByGroupId(String groupId);

    public List<GroupMembers> fetchGroupMembersByAccountId(String accountId);

    public GroupMembers fetchGroupMembersById(String groupMemberId);

    public List<GroupMembers> fetchGroupMembersByMobileNo(String mobilrNo);

    public boolean isAdmin(String groupMembersId);

    public void saveGroupMembers(List<GroupMembers> groupMembers);

    public int updateReadAndReceivedIndex(String groupId, String participantId, int receivedIndex, int readIndex, String receivedMessageId, String readMessageId);

    public List<GroupMembers> fetchMembersByParticipants(List<String> memberIds);

    public void saveGroupMembers(WSGroupMembersMetadataRequest groupMember, List<GroupMembers> existingGroupMembers) throws Abstract1GroupException;

    public GroupMembers fetchGroupMembersByAccountIdAndGroupId(String accountId, String groupId);

    public List<GroupMembers> fetchGroupMembersByMobileNumbers(List<String> mobileNumbers);

    public String fetchGroupIdFromParticipants(String participantsOne, String participantsTwo, GroupSource source);

    public List<String> fetchGroupIdListByAccountId(String accountId);

    public List<String> fetchMemberIdListByGroupId(String groupId);

    public List<GroupMembers> fetchGroupMembersByAccountIdAndGroupId(String accountId, List<String> groupId);

    public String fetchGroupIdFromParticipantIdAndSource(String accountId, GroupSource source);

    public List<GroupMembers> fetchGroupMembersByMobileNoAndEverSync(List<String> mobileNumbers, Boolean everSync);

    public List<GroupMembersObject> fetchGroupMembersObjectByMobileNoAndEverSync(List<String> mobileNumbers, List<Boolean> everSync);
}
