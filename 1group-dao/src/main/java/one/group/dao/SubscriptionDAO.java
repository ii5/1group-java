package one.group.dao;

import java.util.Set;

import one.group.core.enums.EntityType;

/**
 * DAO that handles subscription
 * 
 * @author nyalfernandes
 *
 */
public interface SubscriptionDAO
{
    public Set<String> fetchAllSubscriptions(String accountId);

    public Set<String> fetchSubscribersOf(String subscriptionEntityId, EntityType subscriptionEntityType);

    public Long fetchSubscriptionTime(String subscriptionEntityId, EntityType subscriptionEntityType, String subscribingAccountId);

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType, int forTime);

    public void deleteSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType);

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType);

    public String isSubscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType);
    
    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType);
    
    public void removeSubscriptionByKey(String key);
}
