package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.PhoneNumber;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface PhoneNumberDAO
{
    public void savePhonenumber(PhoneNumber number);

    public PhoneNumber fetchPhoneNumber(String mobileNumber);
    
    public List<PhoneNumber> fetchPhoneNumbersByIds(Collection<String> phoneNumbers);
}
