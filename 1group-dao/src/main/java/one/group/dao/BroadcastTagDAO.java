/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.helpers.BroadcastObject;

/**
 * @author ashishthorat
 *
 */
public interface BroadcastTagDAO
{
    /**
     * Fetch broadcast_tag details by broadcast id
     * 
     * @param broadcastId
     * @return
     */
    public List<BroadcastTag> fetchBroadcastTagByBroadcastId(String broadcastId, BroadcastStatus status);

    /**
     * 
     * @param broadcastIds
     * @return
     */
    public List<BroadcastTag> fetchBroadcastTagByBroadcastIds(Set<String> broadcastIds);

    public List<BroadcastTag> fetchBroadcastTagByIds(Set<String> broadcastTagIds);

    /**
     * Fetch broadcast_tag details by broadcast Type
     * 
     * @param BroadcastType
     * @return
     */
    public List<BroadcastTag> fetchBroadcastTagByBroadcastType(BroadcastType type);

    /**
     * Fetch broadcast_tag details by location_id
     * 
     * @param broadcastId
     * @return
     */
    public List<BroadcastTag> fetchBroadcastTagByLocationId(String location_id);

    /**
     * Save broadcast_tag
     * 
     * @param broadcast_tag
     * @return
     */
    public void saveBroadcastTag(BroadcastTag Broadcast);

    public void saveBroadcastTags(Collection<BroadcastTag> tagCollection);

    public BroadcastTag fetchBroadcastTagbyBroadcastTagId(String broadcastTagId);

    public List<BroadcastObject> searchBroadcastTypePropertyListing(PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer minArea, Integer maxArea, Long maxPrice, Long minPrice, List<Rooms> roomList, PropertySubType propertSubType, List<BroadcastStatus> broadcastStatusList,
            List<BroadcastTagStatus> broadcastTagStatusList, int limit, int offset);

    public List<BroadcastObject> searchBroadcastTypeRequirement(PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer area, Long price);

    public List<String> getBroadcastTagIdsByBroadcastId(String broadcastId);

    public void deleteBroadcasTagsByIds(List<String> broadcastTagids);

    public List<BroadcastObject> searchBroadcastByCityId(Integer cityId, int offset, int limit);

    public Long fetchBroadcastCountByCity(String cityId);
}
