/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.State;

/**
 * @author ashishthorat
 *
 */
public interface StateDAO
{
    public State getStateByCode(String code);

    public State getStateByName(String name);

    public State fetchStateById(String stateId);

    public void saveState(State state);

}
