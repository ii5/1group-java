package one.group.dao;

import java.util.Set;

import redis.clients.jedis.Tuple;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface ChatMessageDAO
{
    /**
     * Fetch chat messages of chat thread
     * 
     * @param chatThreadId
     * @param sinceIndex
     * @param beforeIndex
     * @return
     */
    public Set<String> fetchChatMessagesOfChatThread(String chatThreadId, int sinceIndex, int beforeIndex);

    /**
     * Fetch last chat message of chat thread
     * 
     * @param chatThreadId
     * @return
     */
    public Set<Tuple> fetchLastChatMessageOfChatThread(String chatThreadId);

    /**
     * Save chat message
     * 
     * @param chatThreadId
     * @param chatMessage
     * @return
     */
    public String saveChatMessage(String chatThreadId, String chatMessage);

    /**
     * Fetch chat message index
     * 
     * @param chatThreadId
     * @param chatMessageKey
     * @return
     */
    public Double fetchChatMessageIndex(String chatThreadId, String chatMessageKey);

    /**
     * Fetch chat message content
     * 
     * @param chatThreadId
     * @param chatMessageKey
     * @return
     */
    public String fetchChatMessageContent(String chatThreadId, String chatMessageKey);

    /**
     * Fetch chat messages countr
     * 
     * @param chatThreadId
     * @return
     */
    public long fetchChatMessageCount(String chatThreadId);

    public Object saveChatMessage(String chatThreadId, String fromAccountId, String toAccountId, String chatMessageJson);
}
