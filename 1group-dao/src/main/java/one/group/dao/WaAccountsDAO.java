/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.WaAccounts;

/**
 * @author ashishthorat
 *
 */
public interface WaAccountsDAO
{
    public void saveWaAccounts(WaAccounts waAccounts);
}
