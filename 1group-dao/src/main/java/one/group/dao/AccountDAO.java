package one.group.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.City;
import one.group.entities.jpa.NativeContacts;
import one.group.entities.jpa.helpers.AccountObject;
import redis.clients.jedis.ScanResult;

/**
 * The Interface AccountDAO.
 *
 * @author nyalfernandes
 */
public interface AccountDAO
{

    /**
     * fetch all online accounts in the system.
     *
     * @return the sets the
     */
    public Set<String> fetchOnlineAccounts();

    /**
     * 
     * @param accountId
     * @return
     */
    public Long fetchOnlineAccountTime(String accountId);

    /**
     * funciton that marks an account online.
     *
     * @param accountId
     *            the account id
     */
    public void createOnlineKey(String accountId);

    /**
     * function that marks an account offline.
     *
     * @param accountId
     *            the account id
     */
    public void deleteOnlineKey(String accountId);

    /**
     * Fetch account by phone number.
     *
     * @param mobileNumber
     *            the mobile number
     * @return the account
     */
    public Account fetchAccountByPhoneNumber(String mobileNumber);

    /**
     * Gets the all accounts.
     *
     * @return the all accounts
     */
    public List<Account> getAllAccounts();

    /**
     * Update account.
     *
     * @param accountId
     *            the account id
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param status
     *            the status
     * @param localities
     *            the localities
     * @return the account
     */
    public Account updateAccount(String accountId, String firstName, String lastName, Status status, City city);

    /**
     * Update account media.
     *
     * @param accountId
     *            the account id
     * @param mediaId
     *            the media id
     * @return the account
     */
    public Account updateAccountMedia(String accountId, String mediaId);

    /**
     * Fetch account of client.
     *
     * @param clientId
     *            the client id
     * @return the account
     */
    public Account fetchAccountOfClient(String clientId);

    /**
     * Fetch account by id.
     *
     * @param accountId
     *            the account id
     * @return the account
     */
    public Account fetchAccountById(String accountId);

    /**
     * Save account.
     *
     * @param account
     *            the account
     * @return TODO
     */
    public Account saveAccount(Account account);

    /**
     * persists an account.
     * 
     * @param account
     * @return
     */
    public void persistAccount(Account account);

    /**
     * Fetch short reference by id.
     *
     * @param accountId
     *            the account id
     * @return the account
     */
    public Account fetchShortReferenceById(String accountId);

    /**
     * Fetch account by short reference.
     *
     * @param shortReference
     *            the short reference
     * @return the account
     */
    public Account fetchAccountByShortReference(String shortReference);

    /**
     * Save native contacts.
     *
     * @param account
     *            the account
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param phoneNumber
     *            the phone number
     */
    public NativeContacts updateNativeContacts(NativeContacts nativeContact);

    /**
     * Creates the account relation.
     *
     * @param accountRelations
     *            the account relations
     */
    public void createAccountRelation(AccountRelations accountRelations);

    /**
     * Update account relation.
     *
     * @param accountRelations
     *            the account relations
     */
    public void updateAccountRelation(AccountRelations accountRelations);

    /**
     * Fetch account relations.
     *
     * @param account
     *            the account
     * @return the sets the
     */
    public Set<AccountRelations> fetchAccountRelations(Account account);

    /**
     * Fetch accounts linked with phonenumber.
     *
     * @param phoneNumber
     *            the phone number
     * @return the sets the
     */
    public Set<Account> fetchAccountsLinkedWithPhonenumber(String phoneNumber);

    /**
     * function that store accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void createAccountBlockByKey(String accountId, String requestByAccountId);

    /**
     * function that delete accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void deleteAccountBlockByKey(String accountId, String requestByAccountId);

    public Set<String> fetchPhonenumbersLinkedWithAccount(Account account);

    public ScanResult<String> fetchAccountsToSendSmsNotification(String cursor, String pattern, int count);

    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp);

    public Map<String, String> fetchChatSmsNotificationTimestamp(String accountId);

    public NativeContacts fetchNativeContacts(Account account, String phoneNumber);

    public List<NativeContacts> fetchNativeContactsForMultiplePhoneNumbers(Account account, Set<String> phoneNumbers);

    public void persistAllNativeContacts(List<NativeContacts> nativeContacts);

    public Set<Account> fetchAccountsByPhoneNumbers(Set<String> mobileNumbers);

    public String fetchCount();

    public void updateCount(String count);

    /**
     * Fetch all accounts by status
     * 
     * @param status
     * @return
     */
    public List<Account> fetchAllAccountsByStatus(Status status);

    public void updateAccount(Account account);

    /**
     * Save invite code for an account for specific time duration
     * 
     */
    public void saveAccountInviteCodeInCache(String accountId, String inviteCode, int expiryTime);

    /**
     * fetch account invite code from cache
     * 
     * @param accountId
     * @return
     */
    public String fetchAccountInviteCodeFromCache(String accountId);

    public List<Account> fetchAllStatusOfNativeContactsByAccountId(String accountId);

    /**
     * 
     * @param locationIds
     * @return
     */
    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds);

    public List<AccountObject> fetchAllAccountIdsByStatus(Status status);

    /**
     * Fetch all accounts by ids
     * 
     * @param accountIds
     * @return
     */
    public List<Account> fetchAllAccountByIds(Set<String> accountIds);

    /**
     * Fetch account by id
     * 
     * @param userId
     * @return
     */
    public Account fetchAccountByUserId(String userId);

    /**
     * Fetch account by waPhoneNumber
     * 
     * @param userId
     * @return
     */
    public Account fetchAccountBywaPhoneNumber(String waPhoneNumber);

    /**
     * Fetch accounts by waSyncStatus
     * 
     * @param userId
     * @return
     */
    public List<Account> fetchAccountByWaSyncStatus(String waSyncStatus);

    /**
     * Fetch accounts by waGroupSelectionStatus
     * 
     * @param userId
     * @return
     */
    public List<Account> fetchAccountByWaGroupSelectionStatus(String waGroupSelectionStatus);

    public int getActiveGroupCountByUserId(long userId);

    public int getApprovedGroupCountByUserId(long userId);

    public Set<String> fetchAllPhoneNumbersOfNativeContactsByAccountId(String accountId);

    public Account fetchAccountByAccountId(String accountId);
    
    public List<Account> fetchSelectiveDataAccountsByIds(Set<String> accountIds);

}
