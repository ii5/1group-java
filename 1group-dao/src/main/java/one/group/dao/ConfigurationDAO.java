package one.group.dao;

import one.group.entities.jpa.Configuration;


/**
 * 
 * @author sanilshet
 *
 */
public interface ConfigurationDAO
{
    /**
     * 
     * @param key
     * @return
     */
    public Configuration getConfiguationByKey(String key);

    /**
     * 
     * @param config
     * @return
     */
    public Configuration saveConfiguration(Configuration config);

    /**
     * 
     * @param config
     */
    public void remove(Configuration config);
}
