package one.group.dao;

public interface SyncGroupDAO
{
    public void addCount(String count);

    public Integer getCount();

    public String fetchGroupContent(String groupkey);

    public boolean isFullRelease();
}
