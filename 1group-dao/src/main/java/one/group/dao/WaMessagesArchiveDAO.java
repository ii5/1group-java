/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.WaMessagesArchive;

/**
 * @author ashishthorat
 *
 */
public interface WaMessagesArchiveDAO
{

    /**
     * 
     * @param groupId
     * @return
     */
    public List<WaMessagesArchive> fetchUserByGroupId(String groupId);

    /**
     * Save waMessagesArchive
     * 
     * @param waMessagesArchive
     * @return
     */
    public void saveWaMessagesArchive(WaMessagesArchive waMessagesArchive);
    
    public void saveWAMessagesArchive(Collection<WaMessagesArchive> waMessagesArchiveList);

    /**
     * 
     * @param waMessageId
     * @return
     */
    public WaMessagesArchive fetchWaMessagesArchiveByWaMessageId(String waMessageId);

    /**
     * 
     * @param waMobileNo
     * @return
     */
    public List<WaMessagesArchive> fetchWaMessagesArchiveByWaMobileNo(String waMobileNo);

    /**
     * 
     * @param messageType
     * @return
     */
    public List<WaMessagesArchive> fetchWaMessagesArchiveByMessageType(String messageType);

    /**
     * 
     * @param status
     * @return
     */
    public List<WaMessagesArchive> fetchWaMessagesArchiveByStatus(String status);

    /**
     * 
     * @param waMessageId
     * @return
     */

    public boolean isWaMessagesArchiveDuplicate(String waMessageId);
}
