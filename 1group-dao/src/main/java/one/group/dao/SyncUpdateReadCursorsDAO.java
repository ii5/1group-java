package one.group.dao;

import java.util.List;

import one.group.entities.jpa.SyncUpdateReadCursors;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface SyncUpdateReadCursorsDAO
{
    public void delete(SyncUpdateReadCursors readCursor);

    public void saveSyncLogReadCursors(SyncUpdateReadCursors readCursor);

    public SyncUpdateReadCursors fetchByClient(String clientId);
    
    public List<SyncUpdateReadCursors> getUpdatesByClientIds(List<String> clients);
}
