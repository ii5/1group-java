/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.entities.jpa.Messages;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;

/**
 * @author ashishthorat
 *
 */
public interface MessagesDAO
{
    /**
     * 
     * @param status
     * @return
     */
    public List<Messages> fetchMessagesByOneGroupStatus(MessageStatus status);

    /**
     * Save messages
     * 
     * @param messages
     * @return
     */
    public void saveMessages(Messages messages);

    /**
     * 
     * @param status
     * @return
     */
    public List<Messages> fetchMessagesByBpoMessageStatus(BpoMessageStatus status);

    /**
     * 
     * @param status
     * @return
     */
    public List<Messages> fetchMessagesByMessageType(MessageType type);

    /**
     * 
     * @param broadcastId
     * @return
     * @throws General1GroupException
     */
    public List<Messages> getMessagesByBroadcastId(String broadcastId) throws Abstract1GroupException;

    /**
     * 
     * @param messageId
     * @return
     */
    public boolean isMessageDuplicate(String messageId);

    /**
     * 
     * @param groupId
     * @return
     * @throws General1GroupException
     */
    public List<Messages> getMessagesByGroupId(String groupId) throws Abstract1GroupException;

    /**
     * 
     * @param accountId
     * @return
     * @throws General1GroupException
     */
    public List<Messages> getMessagesByAccountId(String accountId) throws Abstract1GroupException;

    public Messages fetchByMessageByMessageId(String messageId);

}
