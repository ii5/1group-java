package one.group.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.City;
import one.group.entities.jpa.helpers.BPOCityLocationData;

public interface CityDAO
{
    /**
     * 
     * @return
     */
    public List<City> getAllCities();

    /**
     * 
     * @param cityId
     * @return
     */
    public City getCityById(String cityId);

    public List<City> getAllCitiesByCityIds(Collection<String> cityids);

    public List<City> getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField,SortType> sort, Map<FilterField,Set<String>> filter, int start, int rows);

    public List<BPOCityLocationData> getAllCityLocations(List<String> cityIds, List<Status> locationStatusList);

    public List<City> getAllCities(List<Boolean> isPublishedList);
}
