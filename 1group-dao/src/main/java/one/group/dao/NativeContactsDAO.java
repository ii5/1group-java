package one.group.dao;

import java.util.List;
import java.util.Set;

import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.jpa.NativeContacts;

public interface NativeContactsDAO
{
    public List<NativeContacts> fetchAllNativeContactsByAccountId(String accountId);

    public List<WSNativeContact> fetchAllNativeContactsByAccountIdFiltered(String accountId, Set<String> registeredPhoneNumbers, int offset, int limit);

    public List<String> fetchNativeContactsByPhoneNumber(String phoneNumber, Status status);

    public int fetchAllNativeContactsByAccountIdCount(String accountId, Set<String> registeredPhoneNumbers);

}
