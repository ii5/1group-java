package one.group.dao;

import java.util.Collection;

import one.group.entities.jpa.SyncUpdateWriteCursors;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface SyncUpdateWriteCursorsDAO
{
    public SyncUpdateWriteCursors fetchByAccount(String accountId);

    public void saveSyncLogWriteCursors(SyncUpdateWriteCursors writeCursor);
    
    public Collection<SyncUpdateWriteCursors> findAllCursors();
}
