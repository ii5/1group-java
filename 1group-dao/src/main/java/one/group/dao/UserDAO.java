/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.User;

/**
 * @author ashishthorat
 *
 */
public interface UserDAO
{
    /**
     * 
     * @param cityId
     * @return
     */
    public List<User> fetchUserByCityId(String cityId);

    /**
     * Save user
     * 
     * @param messages
     * @return TODO
     * @return
     */
    public User saveUser(User user);

    /**
     * 
     * @param userId
     * @return
     */
    public User fetchUserByUserId(String userId);

    /**
     * 
     * @param userId
     * @return
     */
    public User fetchUserByEmail(String email);

    /**
     * 
     * @param shortRef
     * @return
     */
    public User fetchUserByShortRef(String shortRef);
    
    public Account fetchUserByNumber(String mobileNumber);
    
    public void saveAll(Collection<User> userCollection);

}
