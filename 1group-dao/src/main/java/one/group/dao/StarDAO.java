package one.group.dao;

import java.util.List;

import one.group.core.enums.StarType;
import one.group.entities.jpa.Star;

public interface StarDAO
{
    /**
     * 
     * @param accountId
     * @param targetEntityId
     * @return
     */
    public Star doStar(String referenceId, String targetEntityId, StarType type);

    /**
     * 
     * @param referenceId
     * @param type
     * @return
     */
    public List<Star> getStarsByReference(String referenceId, StarType type);

    /**
     * 
     * @param referenceId
     * @param targetEntityId
     * @return
     */
    public Star getStarByReferenceAndTargetEntity(String referenceId, String targetEntityId);

    /**
     * 
     * @param accountId
     * @param broadcastIds
     * @return
     */
    public List<Star> checkIsStarredForBroadcast(String accountId, List<String> broadcastIds);

    /**
     * 
     * @param accountId
     * @param broadcastIds
     * @return
     */
    public void unStarBroadcast(Star star);
}
