/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;

/**
 * @author ashishthorat
 *
 */
public interface AccountRelationDAO
{

    public void createAccountRelation(AccountRelations relations);

    public AccountRelations createUpdateAccountRelation(AccountRelations relations, boolean flush);

    public void updateAccountRelation(AccountRelations relations);

    public List<AccountRelations> fetchByAccount(String accountId);

    public List<AccountRelations> fetchByRequestByAccountAndToAccount(Account requestByAccount, Account otherAccount);

    public AccountRelations fetchByAccountAndOtherAccount(String requestByAccount, String otherAccount);

    public boolean isAccountRelationExist(AccountRelations accountRelation);

    /**
     * function that store accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void createAccountBlockByKey(String accountId, String requestByAccountId);

    /**
     * function that delete accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void removeAccountFromBlockedBy(String accountId, String requestByAccountId);

    /**
     * To fetch account relation between two accounts
     * 
     * @param requestByAccountId
     * @param otherAccountId
     * @return
     */
    public List<AccountRelations> fetchAccountRelationBetweenAccounts(String requestByAccountId, String otherAccountId);

    /**
     * 
     * @param accountId
     * @param otherAccountIdList
     * @return
     */
    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList, String query);

    // TODO: Please think of an alternative post launch
    public void callFlush();

    public void persistAllAccountRelation(List<AccountRelations> relations);

    public void saveOrUpdateAllAccountRelation(List<AccountRelations> relations);
    
    public List<String> fetchContactsByAccountId(String accountId);

}
