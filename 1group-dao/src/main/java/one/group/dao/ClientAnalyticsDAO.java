package one.group.dao;

import java.util.List;

import one.group.entities.jpa.ClientAnalytics;

public interface ClientAnalyticsDAO
{
    void saveclientAnalytics(ClientAnalytics clientAnalyticsObj);

    public List<ClientAnalytics> fetchTracesByAccountId(String accountId);
}
