package one.group.dao;

import one.group.entities.jpa.Amenity;


public interface AmenityDAO
{

    /**
     * 
     * @param amenityId
     * @return
     */
    public Amenity findAmenityById(String amenityId);

    /**
     * 
     * @param name
     * @return
     */
    public Amenity findAmenityByName(String name);
}
