package one.group.dao;

import java.util.List;

import one.group.core.enums.BookMarkType;
import one.group.entities.jpa.BookMark;

public interface BookMarkDAO
{
    /**
     * 
     * @param accountId
     * @param propertyListingId
     * @return
     */
    public BookMark bookmark(String referenceId, String targetEntityId, BookMarkType type, String action);

    /**
     * 
     * @param referenceId
     * @param type
     * @return
     */
    public List<BookMark> getBookMarksByReference(String referenceId, BookMarkType type);

    /**
     * 
     * @param referenceId
     * @param targetEntityId
     * @return
     */
    public BookMark getBookMarkByReferenceAndTargetEntity(String referenceId, String targetEntityId);
    
    /**
     * 
     * @param accountId
     * @param propertyListings
     * @return
     */
    public List<BookMark>  checkIsBookMarkedForPropertyListings(String accountId, List<String> propertyListings);
}
