/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.ChatLog;

/**
 * @author ashishthorat
 *
 */
public interface ChatLogDAO
{
    public void saveChatLog(ChatLog chatlog);

    public ChatLog fetchChatLog(String chatThreadId, String index);
}
