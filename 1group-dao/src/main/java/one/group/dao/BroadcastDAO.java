/**
 * 
 */
package one.group.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.helpers.BroadcastCountObject;
import one.group.entities.jpa.helpers.BroadcastTagCountObject;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;

/**
 * @author ashishthorat
 *
 */
public interface BroadcastDAO
{

    /**
     * 
     * @param propertyListingId
     * @return
     * @throws General1GroupException
     */
    public Broadcast getBroadcastByBroadcastId(String broadcastId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException;

    /**
     * 
     * @param broadcast
     * @throws Abstract1GroupException
     */
    public void closeBroadcast(Broadcast broadcast) throws Abstract1GroupException;

    public Boolean doesBroadcastExist(String accountId);

    public List<Broadcast> getBroadcasts(List<String> broadcastIds);

    /**
     * Fetch broadcast details by broadcast id
     * 
     * @param broadcastId
     * @return
     */
    public Broadcast fetchBroadcast(String broadcastId);

    /**
     * 
     * @param shortReference
     * @return
     */
    public Broadcast fetchByShortReference(String shortReference);

    /**
     * Save broadcast
     * 
     * @param broadcast
     * @return
     */
    public void saveBroadcast(Broadcast Broadcast);

    /**
     * 
     * @param broadcastId
     * @return
     */
    public List<Broadcast> getBroadcastByBroadcastIds(List<String> listbroadcastIds, BroadcastStatus status);

    /**
     * 
     * @param messageId
     * @return
     * @throws General1GroupException
     */
    public Broadcast getBroadcastByMessageId(String messageId);

    /**
     * Fetch broadcast id by short reference
     * 
     * @param shortReference
     * @return
     */
    public String fetchBroadcastIdByShortReference(String shortReference);

    /**
     * Check broadcast exist or not
     * 
     * @param broadcastId
     * @return
     */
    public String isBroadcastExist(String broadcastId);

    public long fetchCount();

    public List<BroadcastTagsConsolidated> fetchBroadcastsByCityAndLocation(List<String> cityIds, List<String> locationIds, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter,
            int start, int rows);

    public List<BroadcastCountObject> fetchBroadcastCountByAccountIds(List<String> accountIds);

    public List<Broadcast> fetchBroadcastByAccountId(String accountId);

    public Broadcast getBroadcastByBroadcastId(String broadcastId);

    public List<Broadcast> fetchBroadcastByMessageCreatedBy(String createdBy, BroadcastStatus status);

    public void removeRelationOfBroadcastAndMessageUpdateStatus(String broadcastId, String messageId, BroadcastStatus broadcastStatus);

    public void updateBroadcastViewCount(List<String> broadcastIdList);

    public Set<String> fetchBroadcast(String accountId, long start, long end);

    public void saveBroadcastBulk(Set<PipelineCacheEntity> broadcastPipelineSet);

    public void removeAllBroadcast(String accountId);

    public List<BroadcastTagCountObject> fetchBroadcastTagCountByAccountIds(Set<String> accountIds, BroadcastTagStatus broadcastTagStatus, BroadcastStatus broadcastStatus);
}
