/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.Menu;

public interface MenuDAO
{

    public void saveMenu(Menu menu);

    public Menu fetchByMenuId(String menuId);
}
