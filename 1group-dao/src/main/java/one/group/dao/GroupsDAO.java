/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.v2.WSAdminGroupMetaData;
import one.group.entities.jpa.BroadcastGroupRelation;
import one.group.entities.socket.Groups;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;

/**
 * @author ashishthorat
 *
 */
public interface GroupsDAO
{
    public Map<GroupStatus, Set<Groups>> fetchByStatusAndEverSync(List<GroupStatus> groupStatus, boolean everSync);

    public List<Groups> fetchGroupLabelAndIdByStatus(List<GroupStatus> groupStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<Groups> fetchGroupsByInputs(List<GroupStatus> groupStatusList, List<String> groupIdList, int msgCountGreaterThan, int start, int rows, String sortByField, String sortByType,
            Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public int fetchLatestMessageIndexOfGroup(String groupId);

    public String fetchGroupIdFromParticipants(String participantOne, String participantTwo);

    public void saveBroadcastGroupRelation(BroadcastGroupRelation relation);

    public List<Groups> fetchGroupsByGroupIds(List<String> groupIds);

    public void saveGroup(Groups groups);

    public List<Groups> fetchGroupIdFromMembers(List<String> memberIds, GroupSource groupSource);

    public List<Groups> fetchAllGroupsByAccountId(String accountId, List<GroupSource> groupSourceList);

    public List<Groups> fetchAllGroupsByAccountIdAndGroupId(String accountId, String groupId);

    public List<Groups> fetchUnassignedGroupsBasedOnCity(String cityId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<Groups> fetchDefaultGroupsByAccountId(String accountId);

    public void releaseAllGroups();

    public void saveGroupsBulk(List<Groups> groupsList);

    public QueryResponse fetchGroupsToUpdateUnreadFrom(int start, int rows, boolean fullEntity, Collection<String> groupIdCollection);

    public Map<String, Groups> fetchGroups(int start, int rows);

    public void updateGroup(SolrInputDocument groups);

    public List<Groups> fetchGroupsByGroupIdsAndSource(List<String> groupIds, List<GroupSource> groupSourceList);

    public int fetchTotalGroupCount();

    public WSAdminGroupMetaData fetchGroupMetaDataByGroupIdsAndEverSync(List<String> groupIdList, boolean everSync);
    
    public void update(List<Groups> groups);

}
