package one.group.dao;

import java.util.List;

import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;

public interface PropertySearchRelationDAO
{
    public List<PropertySearchRelation> getMatchingClients(String propertyListingId, String accountId);
    
    public List<PropertySearchRelation> getPropertySearchRelationsOfAccount(String accountId);

    public PropertySearchRelation getSearch(String id);

    public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation);
    
    public List<PropertySearchRelation> getAllMatchingClientsByIds(List<PropertyListing> propertyListings, String accountId);
    
}
