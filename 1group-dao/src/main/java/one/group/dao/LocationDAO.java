package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.core.enums.status.Status;
import one.group.entities.jpa.Location;

public interface LocationDAO
{
    public List<Location> fetchLocalitiesOfCityById(String cityId);

    public Location fetchById(String locationId);

    public Collection<Location> getAllLocations();

    public void updateLiveLocationTable(String cityId);

    public List<Location> fetchAllLocations(Collection<String> locationIdList);

    public List<Location> fetchAllLocationsByCityIds(List<String> cityIds);

    public List<Location> fetchAllLocationByStatus(List<Status> statusList);
}
