package one.group.dao;

import java.util.Collection;

import one.group.entities.jpa.LocationMatchAdmin;

/**
 * 
 * @author sanilshet
 *
 */
public interface LocationMatchAdminDAO
{
    public Collection<LocationMatchAdmin> getAdminLocationMatches();
}
