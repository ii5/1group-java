package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.LocationAdmin;

public interface LocationAdminDAO
{

    public Collection<LocationAdmin> getAllLocations();
    
    public Collection<LocationAdmin> getAllLocationsByCity(List<String> cityIds);

    public List<LocationAdmin> fetchLocalitiesOfCityById(String cityId);

}
