/**
 * 
 */
package one.group.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.AuthAssignment;

public interface AuthAssignmentDAO
{

    public void saveAuthAssignmentDAO(AuthAssignment authAssignment);

    public List<AuthAssignment> fetchByUserId(String userId);
    
    public List<AuthAssignment> fetchAuthAssignmentItemsByUserIdList(Collection<String> userIdList);
    
    public List<String> fetchAuthAssignmentItemNamesByUserId(String userId);
    
    public List<String> fetchUserIdsByRoles(Collection<String> roles);
    
    public void clearAssignmentOfUser(Collection<String> userId);

}
