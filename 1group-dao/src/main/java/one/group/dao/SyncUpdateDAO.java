package one.group.dao;

import java.util.List;

import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.SyncUpdate;

public interface SyncUpdateDAO
{
    public void saveAccountUpdate(SyncUpdate log);

    public void saveAccountUpdates(List<SyncUpdate> syncUpdateList);

    public List<SyncUpdate> fetchByAccount(String accountId);

    public SyncUpdate fetchByAccountAndUpdateIndex(String accountId, int updateIndex);

    public List<SyncUpdate> fetchAllByAccount(String accountId, int fromUpdateIndex, int toUpdateIndex);

    public int createClient(String clientId);

    public void appendUpdate(String accountId, WSSyncUpdate syncUpdate);

    public int advanceReadCursor(String accountId, String clientId, int requestedCursorIndex);

    public void deleteEarlierUpdatesOfAccount(int index, String accountId);
}
