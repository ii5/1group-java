package one.group.dao;

import java.util.Map;
import java.util.Set;

public interface ChatThreadDAO
{
    /**
     * Fetch chat thread list of an account by Activity
     * 
     * @param accountId
     * @param sinceIndex
     * @param beforIndex
     * @return
     */
    public Set<String> fetchChatThreadListOfAccountByActivity(String accountId, int sinceIndex, int beforIndex);

    /**
     * Fetch chat thread list of an account by joined
     * 
     * @param accountId
     * @param sinceIndex
     * @param beforIndex
     * @return
     */
    public Set<String> fetchChatThreadListOfAccountByJoined(String accountId, int sinceIndex, int beforIndex);

    /**
     * Fetch score of chat thread
     * 
     * @param chatThreadId
     * @param accountId
     * @param indexBy
     * @return
     */
    public Double fetchChatThreadScore(String chatThreadId, String accountId, String indexBy);

    /**
     * Save chat thread by activity
     * 
     * @param chatThreadId
     * @param accountId
     */
    public void saveChatThreadByActivity(String chatThreadId, String accountId);

    /**
     * Save chat thread by joined
     * 
     * @param chatThreadId
     * @param accountId
     */
    public void saveChatThreadByJoined(String chatThreadId, String accountId);

    /**
     * Update chat thread score
     * 
     * @param chatThreadId
     * @param fromAccountId
     * @param toAaccountId
     */
    public void updateChatThreadParticipants(String chatThreadId, String fromAccountId, String toAccountId);

    /**
     * Fetch chat thread participants
     * 
     * @param chatThreadId
     * @return
     */
    public Map<String, String> getChatThreadParticipants(String chatThreadId);

    /**
     * Fetch chat thread index by activity
     * 
     * @param accountId
     * @param chatThreadId
     * @return
     */
    public long getChatThreadIndexByActivity(String accountId, String chatThreadId);

    /**
     * Fetch chat thread index by joined
     * 
     * @param accountId
     * @param chatThreadId
     * @return
     */
    public long getChatThreadIndexByJoined(String accountId, String chatThreadId);

    /**
     * Update chat thread score
     * 
     * @param chatThreadId
     * @param accountId
     */
    public void updateChatThreadScore(String chatThreadId, String accountId);

    /**
     * Update read chat message cursor of chat thread
     * 
     * @param chatThreadId
     * @param accountId
     * @param index
     */
    public void updateChatThreadCursorReadIndex(String chatThreadId, String accountId, String index);

    /**
     * Update received chat message cursor of chat thread
     * 
     * @param chatThreadId
     * @param accountId
     * @param index
     */
    public void updateChatThreadCursorReceivedIndex(String chatThreadId, String accountId, String index);

    /**
     * Fetch read cursor of chat thread of an account
     * 
     * @param chatThreadId
     * @param accountId
     * @return
     */
    public String fetchChatThreadCursorReadIndex(String chatThreadId, String accountId);

    /**
     * Fetch receive cursor of chat thread of an account
     * 
     * @param chatThreadId
     * @param accountId
     * @return
     */
    public String fetchChatThreadCursorReceivedIndex(String chatThreadId, String accountId);

    /**
     * Fetch chat threads count by activity
     * 
     * @param accountId
     * @return
     */
    public long fetchChatThreadCountByActivity(String accountId);

    /**
     * Fetch chat threads count by joined
     * 
     * @param accountId
     * @return
     */
    public long fetchChatThreadCountByJoined(String accountId);

    /**
     * Save chat thread for property listing
     * 
     * @param chatThreadId
     * @param propertyListingId
     */
    public void saveChatThreadForPropertyListing(String chatThreadId, String propertyListingId);

    /**
     * Fetch chat threads by property listing
     * 
     * @param propertyListingId
     * @return
     */
    public Set<String> fetchChatThreadByProperty(String propertyListingId);

    public Object updateChatThreadCursor(String chatThreadId, String accountId, String receivedIndex, String readIndex);

    public Set<String> fetchAllChatThreads(String pattern);
}