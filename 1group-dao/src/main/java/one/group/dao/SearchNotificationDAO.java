package one.group.dao;

import java.util.List;

import one.group.entities.jpa.SearchNotification;

public interface SearchNotificationDAO
{
    public List<SearchNotification> fetchAllNotifications();

    public void saveSearchNotification(SearchNotification searchNotification);

    public void removeAllSearchNotification();

    public void removeSearchNotification(String id);

    public long getSearchNotificationCountByAccountIdAndSentFlag(String accountId, boolean sendFlag);

}
