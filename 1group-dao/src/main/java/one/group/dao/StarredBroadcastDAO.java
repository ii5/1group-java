/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.StarredBroadcast;

/**
 * @author ashishthorat
 *
 */
public interface StarredBroadcastDAO
{
    /**
     * 
     * @param accountId
     * @return
     */
    public List<StarredBroadcast> getStarredBroadByAccountId(String accountId);

    public void saveStarredBroadcast(StarredBroadcast starredBroadcast);

    /**
     * 
     * @param accountId
     * @param broadcastId
     * @return
     */
    public StarredBroadcast getStarredBroadcastByAccountIdAndBroadcastId(String accountId, String broadcastId);

    public void unStarBroadcast(StarredBroadcast starredBroadcast);

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId);

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId, int offset, int limit);

    public List<StarredBroadcast> getStarredBroadcastIdsByAccountId(String accountId);

    public boolean isBroadcastStarred(String broadcastId);
}
