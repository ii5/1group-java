/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.WaMessagesIncomingArchive;

/**
 * @author ashishthorat
 *
 */
public interface WaMessagesIncomingArchiveDAO
{
    /**
     * 
     * @param groupId
     * @return
     */
    public List<WaMessagesIncomingArchive> fetchUserByGroupId(String groupId);

    /**
     * Save waMessagesArchive
     * 
     * @param waMessagesArchive
     * @return
     */
    public void saveWaMessagesIncomingArchive(WaMessagesIncomingArchive waMessagesIncomingArchive);

    /**
     * 
     * @param waMessageId
     * @return
     */
    public WaMessagesIncomingArchive fetchWaMessagesIncomingArchiveById(String waMessageId);

    /**
     * 
     * @param waMobileNo
     * @return
     */
    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByWaMobileNo(String waMobileNo);

    /**
     * 
     * @param messageType
     * @return
     */
    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByMessageType(String messageType);

    /**
     * 
     * @param status
     * @return
     */
    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByStatus(String status);

}
