/**
 * 
 */
package one.group.dao;

import one.group.entities.jpa.AccountMessageCursors;

public interface AccountMessageCursorDAO
{
    public void saveAccountMessageCursor(AccountMessageCursors accountMessageCursor);

    public AccountMessageCursors fetchByAccountMessageCursorsId(String accountMessageCursorId);
}
