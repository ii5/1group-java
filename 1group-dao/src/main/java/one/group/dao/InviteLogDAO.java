package one.group.dao;

import java.util.List;

import one.group.entities.jpa.InviteLog;

/**
 * 
 * @author sanilshet
 *
 */
public interface InviteLogDAO
{
    /**
     * 
     * @param inviteLog
     * @return
     */
    public void saveInviteLog(InviteLog inviteLog);

    public List<String> findAllInvitedByAccount(String accountId, long inviteDurationInMillies);

    public InviteLog fetchInviteLogById(String inviteLogId);
}
