/**
 * 
 */
package one.group.dao;

import java.util.List;

import one.group.entities.jpa.LocationTmp;

/**
 * @author ashishthorat
 *
 */
public interface LocationTempDAO
{
    public List<LocationTmp> fetchLocationTmpByCityId(String cityId);

    public List<LocationTmp> fetchLocationTmpByParentId(String parentId);

    public List<LocationTmp> fetchLocationTmpByLoginUserID(String loginUserId);

    public List<LocationTmp> fetchLocationTmpByStatus(String status);

    public void saveLocationTmp(LocationTmp locationTmp);

    public LocationTmp fetchLocationTmpByLocationTmpId(String LocationTmpId);
}
