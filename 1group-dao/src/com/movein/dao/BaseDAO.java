package one.group.dao;

import java.util.Collection;

public interface BaseDAO<K, E>
{

    /**
     * This method is used to persist an Entity from a repository.
     * 
     * @param entity
     */
    public void persist(E entity);

    /**
     * This method persists all entities passed as a parameter into the
     * repository.
     * 
     * @param entities
     */
    public void persistAll(Collection<E> entities);

    /**
     * This method deletes an entity from a repository.
     * 
     * @param entity
     */
    public void remove(E entity);

    /**
     * This method deletes all entries of the entities from the repository.
     */
    public void removeAll();

    /**
     * This method merges the passed entity into the repository and returns the
     * managed instance of the entity.
     * 
     * @param entity
     * @return Managed instance of the entity.
     */
    public E merge(E entity);

    /**
     * This method refreshes the entities in the repository
     * 
     * @param entity
     */
    public void refresh(E entity);

    /**
     * This method searches for an entity with the passed id.
     * 
     * @param id
     * @return Instance of the Entity found or null otherwise.
     */
    public E findById(K id);

    /**
     * This method returns all entries of a given entity.
     * 
     * @return
     */
    public Collection<E> findAll();

    /**
     * This method limit number of entries of a given entity
     * 
     */
    public Collection<E> findAllFromTo(E entity, int from, int to);
}
