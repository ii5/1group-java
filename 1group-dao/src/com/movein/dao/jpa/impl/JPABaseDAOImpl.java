package one.group.dao.jpa.impl;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import one.group.dao.jpa.JPABaseDAO;

public abstract class JPABaseDAOImpl<K, E> implements JPABaseDAO<K, E>
{
    private static final Logger logger = LoggerFactory
            .getLogger(JPABaseDAOImpl.class);
    private static final int BATCH_SIZE = 30;

    /**
     * The persistent Context
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * The entity type.
     */
    protected Class<E> entityClass;

    public JPABaseDAOImpl()
    {
        ParameterizedType genericSuperClass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperClass
                .getActualTypeArguments()[0];
    }

    public void persist(E entity)
    {
        logger.debug("Persisting entity " + entity);
        getEntityManager().persist(entity);
    }

    public void persistAll(Collection<E> entities)
    {
        Iterator<E> iterator = entities.iterator();
        int cursor = 0;

        while (iterator.hasNext())
        {
            E entity = (E) iterator.next();
            persist(entity);

            if (cursor / BATCH_SIZE == 0 || !iterator.hasNext())
            {
                logger.debug("Flushing after " + cursor + ".");
                getEntityManager().flush();
            }
            cursor++;
        }
    }

    public void remove(E entity)
    {
        logger.debug("Deleting entity " + entity + ".");
        getEntityManager().remove(entity);

    }

    public void removeAll()
    {
        logger.debug("Deleting bulk entities.");
        for (E entity : findAll())
        {
            remove(entity);
        }
    }

    public E merge(E entity)
    {
        logger.debug("Merging entity " + entity);
        return getEntityManager().merge(entity);
    }

    public void refresh(E entity)
    {
        logger.debug("Refreshing entity " + entity);
        getEntityManager().refresh(entity);
    }

    public E findById(K id)
    {
        logger.debug("Find entity " + entityClass.getSimpleName() + " by id "
                + id);
        return getEntityManager().find(entityClass, id);
    }

    public Collection<E> findAll()
    {
        logger.debug("Find all entities recorded of class "
                + entityClass.getSimpleName());
        return getEntityManager().createQuery(
                "SELECT h FROM " + entityClass.getName() + " h ORDER BY h.id")
                .getResultList();
    }

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager()
    {
        return this.entityManager;
    }

    public void flush(E entity)
    {
        logger.debug("Flushing entity " + entity + ".");
        getEntityManager().flush();
    }

    public E saveOrUpdate(E entity)
    {
        logger.debug("Saving or Updating entity " + entity + ".");
        entity = getEntityManager().merge(entity);
        flush(entity);
        return entity;
    }

    public Collection<E> findAllFromTo(E entity, int from, int to)
    {
        logger.debug("Find limited entities recorded of class "
                + entityClass.getSimpleName());
        return getEntityManager()
                .createQuery("SELECT h FROM " + entityClass.getName())
                .setFirstResult(from).setMaxResults(to).getResultList();
    }
}
