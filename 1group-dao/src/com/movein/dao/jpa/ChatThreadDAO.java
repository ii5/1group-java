package one.group.dao.jpa;

import java.math.BigInteger;
import java.util.Set;

import com.movein.entities.ChatThread;



public interface ChatThreadDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method returns chat history of an agent
     * 
     * @param fromContact
     * @return
     */
    public Set<ChatThread> getAllChatHistory(BigInteger fromContact);

    /**
     * This method returns without context chat history between two agents
     * 
     * @param fromContact
     * @param toContact
     * @return
     */
    public ChatThread getChatWitoutContext(BigInteger fromContact,
            BigInteger toContact);

    /**
     * This method returns contextual chat history between agents
     * 
     * @param fromContact
     * @param toContact
     * @param context
     * @return
     */
    public ChatThread getOneWithContext(BigInteger fromContact,
            BigInteger toContact, String context);

}
