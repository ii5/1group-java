package one.group.dao.jpa;

import java.math.BigInteger;

public interface AgentDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method returns online status of an agent
     * 
     * @param agent_id
     * @return
     */
    public boolean isOnline(BigInteger agent_id);

}
