package one.group.dao.jpa;

import java.util.Set;

import com.movein.entities.Property;

public interface PropertyDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method returns agents all properties
     * 
     * @param phoneNumber
     * @return
     */
    public Set<Property> getAgentAllProperty(String phoneNumber);

    /**
     * This method returns limited properties of an agent
     * 
     * @param phoneNumber
     * @param from
     * @param to
     * @return
     */
    public Set<Property> getAgentPropertyFromTo(String phoneNumber, int from,
            int to);
}
