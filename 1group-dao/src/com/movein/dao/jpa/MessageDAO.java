package one.group.dao.jpa;

import java.math.BigInteger;
import java.util.Set;

import com.movein.entities.Message;

public interface MessageDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method returns status of a message
     * 
     * @param messageId
     * @return
     */
    public String getMessageStatus(BigInteger messageId);

    /**
     * This method returns all chat messages
     * 
     * @param fromContact
     * @param toContact
     * @return
     */
    public Set<Message> getAllMessagesWithoutContext(BigInteger fromContact,
            BigInteger toContact);

    /**
     * This method returns all contextual messages
     * 
     * @param contextId
     * @return
     */
    public Set<Message> getAllMessagesWithContext(String contextId);

    /**
     * This method returns count of unread messages of an contextual chat
     * 
     * @param contexId
     * @return
     */
    public int countUnreadContexualMessages(String contexId);

}
