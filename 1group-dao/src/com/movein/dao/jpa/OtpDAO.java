package one.group.dao.jpa;

public interface OtpDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method generates new OTP
     * 
     * @param mobileNumber
     * @param deviceId
     * @return
     */
    public int getNewOtp(String phoneNumber, String deviceId);

    /**
     * This method verify OTP
     * 
     * @param otp
     * @param mobileNumber
     * @param deviceId
     * @return
     */
    public boolean checkOtp(String otp, String mobileNumber, String deviceId);

}
