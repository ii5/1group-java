package one.group.dao.jpa;

public interface PhoneDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method checks uniqueness of a phone number
     * 
     * @param phoneNumber
     * @return
     */
    public boolean validateUniquePhoneNumber(String phoneNumber);

    /**
     * This method validates phone number
     * 
     * @param phoneNumber
     * @return
     */
    public boolean validatePhoneNumber(String phoneNumber);
}
