package one.group.dao.jpa;

import java.math.BigInteger;
import java.util.Set;

import com.movein.jpa.Media;

public interface MediaDAO<K, E> extends JPABaseDAO<K, E>
{
    /**
     * This method returns media of a property
     * 
     * @param propertyId
     * @return
     */
    public Set<Media> getPropertyMedia(BigInteger propertyId);
}
