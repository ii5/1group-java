package one.group.dao.jpa;

import javax.persistence.EntityManager;

import one.group.dao.BaseDAO;

public interface JPABaseDAO<K, E> extends BaseDAO<K, E>
{
    /**
     * This method sets the entity manager for further transactions.
     * 
     * @param entityManager
     */
    public void setEntityManager(EntityManager entityManager);

    /**
     * This method synchronizes the entity with the underlying database.
     */
    public void flush(E entity);

    /**
     * This method first merges the entity with the context and then flushes the
     * entity.
     * 
     * @param entity
     * @return Managed Instance of the entity.
     */
    public E saveOrUpdate(E entity);
}
