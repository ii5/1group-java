package one.group.notification.scheduler;

import java.util.Date;

import one.group.services.SearchNotificationService;

public class SendNotificationTaskHelper
{
    private SearchNotificationService searchNotificationService;

    public SearchNotificationService getSearchNotificationService()
    {
        return searchNotificationService;
    }

    public void setSearchNotificationService(SearchNotificationService searchNotificationService)
    {
        this.searchNotificationService = searchNotificationService;
    }

    public void sendNotification()
    {
        System.out.println("sendNotification called..." + new Date());
        searchNotificationService.schedulerExecution();
    }

}
