package one.group.notification.scheduler;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TaskInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(TaskInitiator.class);

    public static void main(final String args[])
    {
        DOMConfigurator.configureAndWatch("log4j.xml");
        new ClassPathXmlApplicationContext("applicationContext-notificationScheduler.xml");
    }
}
