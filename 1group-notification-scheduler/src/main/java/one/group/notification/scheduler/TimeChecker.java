package one.group.notification.scheduler;

import java.util.Calendar;
import java.util.TimeZone;

public class TimeChecker
{

    public boolean checkRule1()
    {

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        cal2.set(Calendar.HOUR, 22);
        cal2.set(Calendar.MINUTE, 0);
        cal2.set(Calendar.SECOND, 0);

        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("IST"));

        return now.after(cal) && now.before(cal2);

    }

    public static void main(String args[])
    {
        TimeChecker timeChecker = new TimeChecker();
        System.out.println(timeChecker.checkRule1());

    }

}
