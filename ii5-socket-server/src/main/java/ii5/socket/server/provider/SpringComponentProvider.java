package ii5.socket.server.provider;

import org.glassfish.tyrus.core.ComponentProvider;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SpringComponentProvider extends ComponentProvider implements ApplicationContextAware
{
    @Autowired
    private static ApplicationContext applicationContext;

    public ApplicationContext getApplicationContext()
    {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    @Override
    public <T> Object create(Class<T> c)
    {
        return applicationContext.getBean(c);
    }

    @Override
    public boolean destroy(Object o)
    {
        return false;
    }

    @Override
    public boolean isApplicable(Class<?> c)
    {
        boolean isApplicable = (applicationContext.getBean(c) != null);
        
        if (!isApplicable)
        {
            throw new IllegalStateException("Bean of type " + c.getCanonicalName() + " is not configured in the spring context.");
        }
        
        return isApplicable;
    }

}
