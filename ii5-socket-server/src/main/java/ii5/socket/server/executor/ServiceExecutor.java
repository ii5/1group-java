package ii5.socket.server.executor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import one.group.entities.api.response.WSResponse;
import one.group.entities.socket.ResponseEntity;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class ServiceExecutor implements IExecutionHandler
{

    public SocketEntity execute(ExecutorObject executorObject, SocketEntity socketEntity) throws Abstract1GroupException
    {
        SocketEntity r = null;
        try
        {
            if (executorObject == null || executorObject.getExecutorInstance() == null || executorObject.getExecutorMethod() == null)
            {
                throw new General1GroupException(GeneralExceptionCode.PATH_NOT_FOUND, true);
            }
            Method m = executorObject.getExecutorMethod();
            Object o = executorObject.getExecutorInstance();
            String responseOutput = (String) m.invoke(o, socketEntity);
            WSResponse response = (WSResponse) Utils.getInstanceFromJson(responseOutput, WSResponse.class);
            socketEntity.setResponse(new ResponseEntity(response));
            Date date = new Date();
            System.out.println("[" + date + "]Execution done [" + socketEntity.getRequestHash() + "]" + "[" + socketEntity.getEndpoint() + "]");
        }
        // catch (Abstract1GroupException ame)
        // {
        // throw ame;
        // }
        catch (InvocationTargetException ite)
        {
            if (ite.getTargetException() instanceof Abstract1GroupException)
            {
                throw (Abstract1GroupException) ite.getTargetException();
            }
            else
            {
                throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, ite);
            }
        }
        catch (Abstract1GroupException abe)
        {
            throw abe;
        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, e);
        }

        return socketEntity;
    }
}
