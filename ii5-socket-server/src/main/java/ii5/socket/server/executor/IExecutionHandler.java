package ii5.socket.server.executor;

import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface IExecutionHandler
{   
    public SocketEntity execute(ExecutorObject executorObject, SocketEntity entity) throws Abstract1GroupException;

}
