package ii5.socket.server.executor;

import java.lang.reflect.Method;

/**
 * 
 * @author nyalfernandes
 *
 */
/**
 * @author nyalfernandes
 *
 */
public class ExecutorObject
{
    private Object executorInstance;
    
    private Method executorMethod;
    
    public ExecutorObject(Object executorInstance, Method executorMethod)
    {
        this.executorInstance = executorInstance;
        this.executorMethod = executorMethod;
    }

    public Object getExecutorInstance()
    {
        return executorInstance;
    }

    public void setExecutorInstance(Object executorInstance)
    {
        this.executorInstance = executorInstance;
    }

    public Method getExecutorMethod()
    {
        return executorMethod;
    }

    public void setExecutorMethod(Method executorMethod)
    {
        this.executorMethod = executorMethod;
    }

    @Override
    public String toString()
    {
        return "ExecutorObject [executorInstance=" + executorInstance.getClass().getSimpleName() + ", executorMethod=" + executorMethod.getName() + "]";
    }
    
    
}
