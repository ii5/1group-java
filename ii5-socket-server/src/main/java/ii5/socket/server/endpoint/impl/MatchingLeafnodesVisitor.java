package ii5.socket.server.endpoint.impl;

import java.util.ArrayList;
import java.util.List;

import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.IEndpointVisitor;

/**
 * 
 * @author nyalfernandes
 *
 */
public class MatchingLeafnodesVisitor implements IEndpointVisitor
{
    private IEndpointElement masterEndpoint;

    public IEndpointElement visit(IEndpointElement element)
    {
        if (element != null)
        {
            if (element.getParent() != null)
            {
                throw new IllegalArgumentException("Elements can only be matched from the root element.");
            }

            System.out.println(getMatch(element));

        }
        
        return element;
    }

    private List<IEndpointElement> filter(List<IEndpointElement> masterEndpointList, IEndpointElement endpointElementToMatch)
    {
        List<IEndpointElement> fiteredList = new ArrayList<IEndpointElement>();
        for (IEndpointElement masterEndpoint : masterEndpointList)
        {
            if (masterEndpoint.matches(endpointElementToMatch))
            {
                fiteredList.add(masterEndpoint);
                // System.out.println("match>>" + masterEndpoint + "--" +
                // endpointElementToMatch);
            }
        }

        return fiteredList;

    }

    private IEndpointElement getMatch(IEndpointElement element)
    {
        List<IEndpointElement> probableList = new ArrayList<IEndpointElement>(masterEndpoint.getLeafNodes().values());
        List<IEndpointElement> filteredList = null;
        IEndpointElement matchingEndpointElement = null;
        while (!element.getLeafNodes().isEmpty())
        {
            element = element.getLeafNodes().get(0);
            filteredList = filter(probableList, element);

            // System.out.println(filteredList);

            System.out.println(filteredList);

            List<IEndpointElement> nextMatchList = new ArrayList<IEndpointElement>();
            for (IEndpointElement filteredElement : filteredList)
            {
                if (filteredElement.getLeafNodes().size() > 0)
                    nextMatchList.add(filteredElement.getLeafNodes().get(0));
            }
            probableList = nextMatchList;

        }

        if (filteredList != null && !filteredList.isEmpty())
        {
            matchingEndpointElement = filteredList.get(0);

        }
        return matchingEndpointElement;

    }

    public void setMasterEndpoint(IEndpointElement element)
    {
        if (element.getParent() == null)
        {
            this.masterEndpoint = element;
        }
        else
        {
            throw new IllegalArgumentException("Master endpoint should be added from the root.");
        }
    }

    public IEndpointElement getMasterEndpoint()
    {
        return this.masterEndpoint;
    }
}
