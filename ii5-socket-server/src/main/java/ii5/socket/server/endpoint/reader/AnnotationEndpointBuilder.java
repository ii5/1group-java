package ii5.socket.server.endpoint.reader;

import ii5.socket.server.core.Constants;
import ii5.socket.server.endpoint.EndpointReader;
import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.impl.UrlEndpoint;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import one.group.core.enums.HTTPRequestMethodType;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class AnnotationEndpointBuilder implements EndpointReader, ApplicationContextAware
{
    private static final Logger logger = LoggerFactory.getLogger(AnnotationEndpointBuilder.class);
    private String packageName = "one.group.rest.services.socket";
    private String baseUrl = Constants.BASE_URL;
    private Class<Path> annotationClass = Path.class;
    private List<Class<? extends Annotation>> expectedHttpRequestMethodAnnotationList = Arrays.asList(GET.class, POST.class, PUT.class, DELETE.class, HEAD.class);
    private static ApplicationContext context;
    private IEndpointElement masterEndpointStructure;

    public IEndpointElement read()
    {
        IEndpointElement baseUrlEndpoint = new UrlEndpoint(baseUrl, null);
        try
        {
            populateEndpointMap(packageName, baseUrlEndpoint);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        print(baseUrlEndpoint);
        return baseUrlEndpoint;
    }

    public void setApplicationContext(ApplicationContext context) throws BeansException
    {
        this.context = context;
    }

    private void populateEndpointMap(String packageName, IEndpointElement urlEndpoint) throws ClassNotFoundException, IOException
    {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Path.class));
        for (BeanDefinition bd : scanner.findCandidateComponents("one.group.rest.services.socket"))
        {
            evalutateClass(Class.forName(bd.getBeanClassName()), annotationClass, urlEndpoint);
        }

    }

    private void evalutateClass(Class<?> clazz, Class<Path> annotationClass, IEndpointElement urlEndpoint)
    {
        if (clazz.isAnnotationPresent(annotationClass))
        {
            Object executorInstance = context.getBean(clazz);

            if (executorInstance == null)
            {
                throw new IllegalStateException("Executor instance not a spring bean.");
            }

            Path annotationImplemented = clazz.getAnnotation(annotationClass);
            String classAnnotationValue = annotationImplemented.value();
            IEndpointElement classEndpoint = null;

            classEndpoint = splitValueAndFormEndpoint(classAnnotationValue, urlEndpoint);

            for (Method m : clazz.getMethods())
            {
                if (m.isAnnotationPresent(annotationClass))
                {
                    Path methodAnnotationImplemented = m.getAnnotation(annotationClass);
                    IEndpointElement methodEndpoint = null;
                    String methodAnnotationValue = methodAnnotationImplemented.value();
                    if (methodAnnotationValue.contains("bpo"))
                    {
                        System.out.println();
                    }
                    System.out.println("Method value: " + methodAnnotationValue);
                    methodEndpoint = splitValueAndFormEndpoint(methodAnnotationValue, classEndpoint);
                    System.out.println("Result endpoint : " + methodEndpoint);
                    String httpMethodTypeValue = null;
                    for (Class<? extends Annotation> httpRequestAnnotation : expectedHttpRequestMethodAnnotationList)
                    {
                        if (m.isAnnotationPresent(httpRequestAnnotation))
                        {
                            httpMethodTypeValue = m.getAnnotation(httpRequestAnnotation).annotationType().getSimpleName();
                            HTTPRequestMethodType httpMethodRequestType = HTTPRequestMethodType.valueOf(httpMethodTypeValue);
                            methodEndpoint.addRequestSpecificExecutor(httpMethodRequestType, executorInstance, m);

                            break;
                        }
                    }
                }
            }
        }
    }

    private IEndpointElement splitValueAndFormEndpoint(String nodeName, IEndpointElement parentEndpoint)
    {
        String[] elements = nodeName.split("/");
        IEndpointElement urlNode = parentEndpoint;

        for (String e : elements)
        {
            if (!e.trim().isEmpty())
            {
                urlNode = formOrReturnEndpoint(e, urlNode);
            }
        }

        return urlNode;
    }

    private IEndpointElement formOrReturnEndpoint(String nodeName, IEndpointElement parentEndpoint)
    {
        if (parentEndpoint.hasLeafNode(nodeName))
        {
            return parentEndpoint.getLeafNode(nodeName);
        }
        else
        {
            return new UrlEndpoint(nodeName, parentEndpoint);
        }

    }

    public static void main(String[] args)
    {
        BasicConfigurator.configure();
        try
        {
            AnnotationEndpointBuilder e = new AnnotationEndpointBuilder();
            // MatchingLeafnodesVisitor visitor = new
            // MatchingLeafnodesVisitor();
            // IEndpointElement inputEndpoint = e.buildTestEndpoint();
            // IEndpointElement populatedEndpoint = e.read();
            // // e.print(populatedEndpoint);
            // visitor.setMasterEndpoint(populatedEndpoint);
            // // Thread.sleep(2000);
            // inputEndpoint.accept(visitor);
            e.print(e.read());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void print(IEndpointElement e)
    {
        if (e.getLeafNodes().isEmpty())
        {
            System.out.println(e);
        }
        for (IEndpointElement leafNode : e.getLeafNodes().values())
        {
            if (leafNode.getLeafNodes().isEmpty() || !leafNode.getRequestTypes().isEmpty())
            {
                System.out.println(leafNode);
            }
            else
            {
                print(leafNode);
            }
        }
    }
}
