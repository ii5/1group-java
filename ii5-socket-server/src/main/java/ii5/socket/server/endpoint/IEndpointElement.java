package ii5.socket.server.endpoint;


import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ii5.socket.server.executor.ExecutorObject;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpRequestType;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface IEndpointElement
{
    public void accept(IEndpointVisitor visitor);

    public IEndpointElement getParent();

    public String getNode();

    public void setLeafNodes(List<IEndpointElement> nodes);

    public void addLeafNode(IEndpointElement node);

    public boolean hasLeafNode(String nodeName);

    public IEndpointElement getLeafNode(String nodeName);

    public Map<String, IEndpointElement> getLeafNodes();

    public boolean matches(IEndpointElement obj);

    public void addRequestSpecificExecutor(HTTPRequestMethodType requestType, ExecutorObject executorInstance);
    
    public void addRequestSpecificExecutor(HTTPRequestMethodType requestType, Object executionInstance, Method executionMethod);

    public Set<HTTPRequestMethodType> getRequestTypes();
    
    public ExecutorObject getExecutorObject(HTTPRequestMethodType requestType);
    
    public Map<HTTPRequestMethodType, ExecutorObject> getRequestSpecificExecutors();

}
