package ii5.socket.server.endpoint;


/**
 * 
 * @author nyalfernandes
 *
 */
public interface EndpointReader
{
    public IEndpointElement read();
}
