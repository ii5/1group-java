package ii5.socket.server.endpoint.reader;

import ii5.socket.server.core.Constants;
import ii5.socket.server.endpoint.EndpointReader;
import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.impl.UrlEndpoint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HttpRequestType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PropertiesFileEndpointBuilder implements EndpointReader
{
    private static final Logger logger = LoggerFactory.getLogger(PropertiesFileEndpointBuilder.class);
    private String baseUrl = Constants.BASE_URL;
    private File inputFile;
    private List<IEndpointElement> urlEndpointList = new ArrayList<IEndpointElement>();

    public File getInputFile()
    {
        return this.inputFile;
    }

    public void setInputFile(File inputFile)
    {
        this.inputFile = inputFile;
    }

    public IEndpointElement read()
    {
        long start = System.currentTimeMillis();
        IEndpointElement baseUrlEndpoint = new UrlEndpoint(baseUrl, null);
        IEndpointElement versionEndpoint = new UrlEndpoint("{version}", baseUrlEndpoint);

        BufferedReader reader = null;

        try
        {
            reader = new BufferedReader(new FileReader(getInputFile()));
            String s = null;

            while ((s = reader.readLine()) != null)
            {
                if (isDesiredFormat(s))
                {
                    String[] splitLine = s.split(" +");
                    String httpRequestTypeName = splitLine[0].trim();
                    String endpoint = splitLine[1].trim();
                    String[] endpointNodeArray = endpoint.split("/");

                    HttpRequestType httpRequestType = HttpRequestType.valueOf(httpRequestTypeName);
                    IEndpointElement tempParentEndpoint = versionEndpoint;

                    for (String endpointNode : endpointNodeArray)
                    {
                        if (!endpointNode.trim().isEmpty())
                        {
                            UrlEndpoint node = null;
                            if (!tempParentEndpoint.hasLeafNode(endpointNode))
                            {
                                node = new UrlEndpoint(endpointNode, tempParentEndpoint);
                            }
                            else
                            {
                                node = (UrlEndpoint) tempParentEndpoint.getLeafNodes().get(0);
                            }

                            tempParentEndpoint = node;
                        }
                    }

                   // tempParentEndpoint.addRequestType(httpRequestType);

                    if (!urlEndpointList.contains(tempParentEndpoint))
                    {
                        urlEndpointList.add(tempParentEndpoint);
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Exception while reading endpoint properties file.");
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (Exception e)
                {
                }
            }
        }

        for (IEndpointElement endPoint : urlEndpointList)
        {
            System.out.println(endPoint);
        }

        long end = System.currentTimeMillis();

        System.out.println("Time: " + (end - start));
        return urlEndpointList.get(0);
    }

    private boolean isDesiredFormat(String s)
    {
        return true;
    }

    public static void main(String[] args)
    {
        UrlEndpoint baseUrlEndpoint = new UrlEndpoint(Constants.BASE_URL, null);
        UrlEndpoint versionEndpoint = new UrlEndpoint("{version}", baseUrlEndpoint);

        UrlEndpoint baseUrlEndpoint1 = new UrlEndpoint(Constants.BASE_URL, null);
        UrlEndpoint versionEndpoint1 = new UrlEndpoint("version", baseUrlEndpoint);

        System.out.println(versionEndpoint.matches(versionEndpoint1));

        PropertiesFileEndpointBuilder pfeb = new PropertiesFileEndpointBuilder();
        File endpointFile = new File("src/main/resources/endpoints.properties");

        System.out.println(endpointFile.getAbsolutePath() + " : " + endpointFile.exists());
        pfeb.setInputFile(endpointFile);

        pfeb.read();
    }

    public void populateEndpoint()
    {
        // TODO Auto-generated method stub

    }
}
