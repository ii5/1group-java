package ii5.socket.server.endpoint;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface IEndpointVisitor
{
    public IEndpointElement visit(IEndpointElement element);
}
