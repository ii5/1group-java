package ii5.socket.server.endpoint.impl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.IEndpointVisitor;
import ii5.socket.server.executor.ExecutorObject;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpRequestType;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class UrlEndpoint implements IEndpointElement
{
    private String node;
    private IEndpointElement parent;
    private Map<String, IEndpointElement> leafNodes = new HashMap<String, IEndpointElement>();
    private Map<HTTPRequestMethodType, ExecutorObject> requestSpecificExecutors = new HashMap<HTTPRequestMethodType, ExecutorObject>();


    protected UrlEndpoint(IEndpointElement endpointElement)
    {
        this.node = endpointElement.getNode();
        this.parent = endpointElement.getParent();
        this.leafNodes = endpointElement.getLeafNodes();
        this.requestSpecificExecutors = endpointElement.getRequestSpecificExecutors();
    }
    
    public UrlEndpoint(String node, IEndpointElement parent)
    {
        this.node = node.trim().replaceAll("^/+", "").replaceAll("/+$", "");

        // if (this.node.matches("^\\{.*\\}$"))
        // {
        // this.node = "{.*}";
        // }

        if (parent != null)
        {
            this.parent = parent;
            this.parent.addLeafNode(this);
        }
    }

    public String getNode()
    {
        return this.node;
    }

    public void setLeafNodes(List<IEndpointElement> nodes)
    {
        for (IEndpointElement node : nodes)
        {
            String nodeRef = node.getNode();
            if (node.getNode().matches("^\\{.*\\}$"))
            {
                nodeRef = ".*";
            }
            this.leafNodes.put(nodeRef, node);
        }
    }

    public void addLeafNode(IEndpointElement node)
    {
        String nodeRef = node.getNode();
        if (node.getNode().matches("^\\{.*\\}$"))
        {
            nodeRef = ".*";
        }

        this.leafNodes.put(nodeRef, node);
    }

    public Map<String, IEndpointElement> getLeafNodes()
    {
        return leafNodes;
    }

    public IEndpointElement getParent()
    {
        return this.parent;
    }

    public void accept(IEndpointVisitor visitor)
    {
        visitor.visit(this);
    }

    public void addRequestSpecificExecutor(HTTPRequestMethodType requestType, ExecutorObject executorInstance)
    {
        this.requestSpecificExecutors.put(requestType, executorInstance);
    }

    public void addRequestSpecificExecutor(HTTPRequestMethodType requestType, Object executionInstance,
            Method executionMethod)
    {
        ExecutorObject o = new ExecutorObject(executionInstance, executionMethod);
        addRequestSpecificExecutor(requestType, o);
    }

    public IEndpointElement getLeafNode(String nodeName)
    {
        String nodeRef = nodeName;
        if (nodeName.matches("^\\{.*\\}$"))
        {
            nodeRef = ".*";
        }

        return this.getLeafNodes().get(nodeRef);
    }

    public boolean hasLeafNode(String nodeName)
    {
        String nodeRef = nodeName;
        if (nodeName.matches("^\\{.*\\}$"))
        {
            nodeRef = ".*";
        }

        return this.getLeafNodes().containsKey(nodeRef);
    }

    public Set<HTTPRequestMethodType> getRequestTypes()
    {
        return this.requestSpecificExecutors.keySet();
    }

    public ExecutorObject getExecutorObject(HTTPRequestMethodType requestType)
    {
        return this.requestSpecificExecutors.get(requestType);
    }
    
    public Map<HTTPRequestMethodType, ExecutorObject> getRequestSpecificExecutors()
    {
        return requestSpecificExecutors;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((node == null) ? 0 : node.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UrlEndpoint other = (UrlEndpoint) obj;
        if (node == null)
        {
            if (other.node != null)
                return false;
        } else if (!node.equals(other.getNode()))
        {
            if (! (node.matches("^\\{.*\\}$") || other.getNode().matches("^\\{.*\\}$")) )
                return false;
        }

        if (parent == null)
        {
            if (other.parent != null)
            {
                return false;
            }
        } 
//        else if (!parent.equals(other.parent))
//        {
//            return false;
//        }

        return true;
    }

    public boolean matches(IEndpointElement obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        // UrlEndpoint other = (UrlEndpoint) obj;
        if (node == null)
        {
            if (obj.getNode() != null)
                return false;
        } else if (!node.equals(obj.getNode()))
        {
            if (!((node.startsWith("{") && node.endsWith("}"))
                    || (obj.getNode().startsWith("{") && obj.getNode().endsWith("}"))))
            {
                return false;
            }
        }

        if (parent == null)
        {
            if (obj.getParent() != null)
            {
                return false;
            }
        }
        // else if (!parent.equals(obj.getParent()))
        // {
        // return false;
        // }

        return true;
    }

    @Override
    public String toString()
    {
        return "UrlEndpoint [endpoint=" + printEndpoint() + ", requestTypeList=" + requestSpecificExecutors.keySet()
                + ", " + requestSpecificExecutors + "]";
    }

    // Helpers

    private String scanAndPrintParent(IEndpointElement element)
    {
        StringBuilder builder = new StringBuilder();

        IEndpointElement parent = element;
        while (parent != null)
        {
            String frontSlash = "/";
            String node = parent.getNode();
            if (node.endsWith(frontSlash))
            {
                frontSlash = "";
            }
            builder.insert(0, node + frontSlash);

            parent = parent.getParent();
        }

        return builder.toString();
    }

    private String printEndpoint()
    {
        return scanAndPrintParent(this);
    }

}
