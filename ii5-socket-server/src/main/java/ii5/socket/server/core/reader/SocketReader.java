package ii5.socket.server.core.reader;

import com.fasterxml.jackson.core.JsonParseException;

import ii5.socket.server.core.processor.Processor;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.General1GroupException;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SocketReader implements Processor<String, SocketEntity>
{
    public SocketEntity process(String input) throws Exception
    {
        try
        {
            SocketEntity socketEntity = (SocketEntity) Utils.getInstanceFromJson(input, SocketEntity.class);
            return socketEntity;
        } 
        catch (JsonParseException e)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }
    }
}
