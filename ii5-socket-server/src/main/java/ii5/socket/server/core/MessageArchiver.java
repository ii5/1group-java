package ii5.socket.server.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import one.group.core.enums.SolrCollectionType;
import one.group.entities.jpa.WaMessagesArchive;
import one.group.entities.socket.Message;
import one.group.services.WaArchiveMessageService;
import one.group.solr.SolrServerFactory;

public class MessageArchiver 
{

	public static void main(String[] args) throws Exception
	{
		archiveMessages();
	}
	
	private static void archiveMessages() throws Exception
    {
//		DOMConfigurator.configureAndWatch("log4j.xml");
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-socket-server-config.xml");
        WaArchiveMessageService archiveSerice = context.getBean("waArchiveMessageService", WaArchiveMessageService.class);
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
    	Map<String, String> params = new HashMap<String, String>();
    	List<String> messageIds = new ArrayList<String>();
    	List<WaMessagesArchive> messageArchive = new ArrayList<WaMessagesArchive>();
    	int start = 0;
    	int rows = 100;
    	do
    	{
    		messageIds.clear();
    		messageArchive.clear();
	    	List<SolrInputDocument> documentsToUpdate = new ArrayList<SolrInputDocument>();
	    	params.put("q", "entityType:message AND messageSource_s:WHATSAPP AND messageType_s:TEXT AND createdTime:[* TO NOW-30DAYS]");
//	    	params.put("fl", "id");
	    	params.put("start", start+"");
	    	params.put("rows", rows+"");
	    	System.out.println(params);
	    	QueryResponse response = c.query(new MapSolrParams(params));
	    	List<Message> messageList = response.getBeans(Message.class);
	    	
	    	
	    	for (Message m : messageList)
	    	{
	    		messageIds.add(m.getId());
	    		messageArchive.add(new WaMessagesArchive(m));
	    	}
	    	
	    	if (!messageArchive.isEmpty())
	    	{
	    		try
	    		{
	    			archiveSerice.saveMessagesToDB(messageArchive);
	    			c.deleteById(messageIds);
	    		}
	    		catch (Exception e)
	    		{
	    			e.printStackTrace();
	    		}
	    		
	    		c.commit();
	    	}
	    	
	    	System.out.println(messageIds);
//	    	start = start + rows;
    	} while (messageIds != null && !messageIds.isEmpty());
    	
    	c.close();
    }
}
