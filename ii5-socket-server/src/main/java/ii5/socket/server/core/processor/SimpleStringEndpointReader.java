package ii5.socket.server.core.processor;

import ii5.socket.server.core.Constants;
import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.impl.UrlEndpoint;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SimpleStringEndpointReader implements Processor<String, IEndpointElement>
{
    public IEndpointElement process(String endpoint) throws Exception
    {
        IEndpointElement baseUrlEndpoint = new UrlEndpoint(Constants.BASE_URL, null);
        IEndpointElement versionUrlEndpoint = new UrlEndpoint("v2", baseUrlEndpoint);
        
        IEndpointElement element = splitValueAndFormEndpoint(endpoint, versionUrlEndpoint);
        
        return baseUrlEndpoint;
    }

    private IEndpointElement splitValueAndFormEndpoint(String nodeName, IEndpointElement parentEndpoint)
    {
        String[] elements = nodeName.split("/");
        IEndpointElement urlNode = parentEndpoint;

        for (String e : elements)
        {
            if (!e.trim().isEmpty())
            {
                urlNode = formOrReturnEndpoint(e, urlNode);
            }
        }

        return urlNode;
    }

    private IEndpointElement formOrReturnEndpoint(String nodeName, IEndpointElement parentEndpoint)
    {
        if (parentEndpoint.hasLeafNode(nodeName))
        {
            return parentEndpoint.getLeafNode(nodeName);
        } else
        {
            return new UrlEndpoint(nodeName, parentEndpoint);
        }

    }
}
