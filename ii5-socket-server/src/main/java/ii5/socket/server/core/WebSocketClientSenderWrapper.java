package ii5.socket.server.core;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PathParamType;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.request.v2.WSPhoto;
import one.group.entities.api.request.v2.WSPhotoReference;
import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

import org.glassfish.tyrus.client.ClientManager;

public class WebSocketClientSenderWrapper
{
    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer 43d4b384-2749-4f97-aca4-39361d14223e"));
        }
    }

    public static void main(String args[]) throws IOException
    {
        runClient();
    }

    private static void runClient() throws IOException
    {
        final MyClientConfigurator clientConfig = new MyClientConfigurator();

        ClientManager client = ClientManager.createClient();
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {
            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://localhost:8025/websockets/base"));

            Scanner scanner = new Scanner(System.in);
            // while (true)
            {
                RequestEntity requestEntity = new RequestEntity();
                SocketEntity socketEntity = new SocketEntity();

                HashMap<HttpHeaderType, String> headers = new HashMap<HttpHeaderType, String>();
                headers.put(HttpHeaderType.AUTHORISATION, "bearer 43d4b384-2749-4f97-aca4-39361d14223e");
                socketEntity.setHeaders(headers);

                WSMessage wsMessage = new WSMessage();

                WSPhoto full = new WSPhoto();
                full.setHeight(168);
                full.setLocation("http://static.1group.com/api/images/accounts/1463466333275.jpg");
                full.setWidth(300);
                full.setSize(4126l);

                WSPhoto thumbnail = new WSPhoto();
                thumbnail.setHeight(160);
                thumbnail.setLocation("http://static.1group.com/api/images/accounts/thumbnail/1463466333275.jpg");
                thumbnail.setWidth(285);
                thumbnail.setSize(3685l);

                WSPhotoReference photo = new WSPhotoReference();
                photo.setFull(full);
                photo.setThumbnail(thumbnail);
                wsMessage.setPhoto(photo);

                wsMessage.setText("Hello - " + Utils.getSystemTime());

                wsMessage.setType(MessageType.BROADCAST.name());
                wsMessage.setCreatedById("6495927981331878");
                wsMessage.setToAccountId("9856444314230823");
                wsMessage.setClientSentTime(Utils.getSystemTime() + "");
                // wsMessage.setChatThreadId("chat-thread-id");
                wsMessage.setBroadcastId("4314965502307476");
                requestEntity.setMessage(wsMessage);

                // socketEntity.setEndpoint("/messages/phone_call");
                socketEntity.setEndpoint("/meta_data/chat_threads");
                socketEntity.setRequestType(HTTPRequestMethodType.GET);

                // socketEntity.setEndpoint("/messages");
                // socketEntity.setRequestType(HTTPRequestMethodType.POST);
                // socketEntity.setRequest(requestEntity);

                HashMap<PathParamType, String> pathParams = new HashMap<PathParamType, String>();
                pathParams.put(PathParamType.VERSION, "v2");
                socketEntity.setPathParams(pathParams);

                // String gg = chatWebService.sendChatMessage(socketEntity);

                session.getAsyncRemote().sendText(Utils.getJsonString(socketEntity));
                Thread.sleep(70000);

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
    }
}
