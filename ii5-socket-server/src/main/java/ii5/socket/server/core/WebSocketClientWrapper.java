package ii5.socket.server.core;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;

public class WebSocketClientWrapper
{
    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer 406b2763-7983-4e8f-a37e-0c77454f0604"));
        }
    }

    public static void main(String args[]) throws IOException
    {
        runClient();
    }

    private static void runClient() throws IOException
    {
        final MyClientConfigurator clientConfig = new MyClientConfigurator();

        ClientManager client = ClientManager.createClient();
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {
            // session = client.connectToServer(TyrusClientEndpoint.class, cec,
            // new URI("ws://api2.ii5.com:8025/websockets/base"));
            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://localhost:8025/websockets/base"));

            Scanner scanner = new Scanner(System.in);
            while (true)
            {

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
    }
}
