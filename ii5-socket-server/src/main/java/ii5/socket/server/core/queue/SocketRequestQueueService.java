package ii5.socket.server.core.queue;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import ii5.socket.server.endpoint.EndpointReader;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SocketRequestQueueService implements ApplicationContextAware, Serializable
{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SocketRequestQueueService.class);
    private BlockingDeque<RequestQueueObject> socketInputDeque = new LinkedBlockingDeque<RequestQueueObject>();
    private int poolSize = 300;
    private ExecutorService executorService;
    private EndpointReader endpointReader;
    private ApplicationContext applicationContext;
    private Set<MessageProcessorMetaData> processorMetaDataSet = new HashSet<MessageProcessorMetaData>();
    

    public SocketRequestQueueService()
    {

    }
    
   public Set<MessageProcessorMetaData> getProcessorMetaDataSet() 
   {
	   return processorMetaDataSet;
   }

    public SocketRequestQueueService(int poolSize)
    {
        this.poolSize = poolSize;
        executorService = Executors.newFixedThreadPool(poolSize);
    }
    
    public ApplicationContext getApplicationContext() 
    {
		return applicationContext;
	}
    
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException 
    {
    	this.applicationContext = applicationContext;
    }

    public EndpointReader getEndpointReader()
    {
        return endpointReader;
    }

    public void setEndpointReader(EndpointReader endpointReader)
    {
        this.endpointReader = endpointReader;
    }

    public void setPoolSize(int poolSize)
    {
        this.poolSize = poolSize;
    }

    public int getPoolSize()
    {
        return this.poolSize;
    }

    public void addRequestToQueue(String message, Session session)
    {
        try
        {
        	RequestQueueObject o = new RequestQueueObject(message, session, System.currentTimeMillis());
            System.out.println("socketInputDeque: size[" + this.socketInputDeque.size() + "] added");
            this.socketInputDeque.putLast(o);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public RequestQueueObject fetchRequestFromQueue()
    {
        try
        {
            System.out.println("socketInputDeque: size[" + this.socketInputDeque.size() + "] fetch");
            return this.socketInputDeque.takeFirst();
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public void initialise()
    {
        // IEndpointElement element = endpointReader.read();
        // socketMessageProcessorService.setMasterEndpointStructure(element);
        logger.info("poolSize[" + poolSize + "]");
        for (int i = 0; i < poolSize; i++)
        {
        	MessageProcessorMetaData metaData = new MessageProcessorMetaData(i, -1, -1);
        	SocketMessageProcessorService o = applicationContext.getBean("socketMessageProcessorService", SocketMessageProcessorService.class);
        	o.setRequestService(applicationContext.getBean("socketRequestQueueService", SocketRequestQueueService.class));
        	o.setMetaData(metaData);
        	executorService.submit(o);
        	this.processorMetaDataSet.add(metaData);
        }
    }
    
    public void forceReInitialiseExecutors()
    {
    	executorService.shutdownNow();
    	executorService = Executors.newFixedThreadPool(poolSize);
    	this.processorMetaDataSet.clear();
    	
    	for (int i = 0; i < poolSize; i++)
        {
    		MessageProcessorMetaData metaData = new MessageProcessorMetaData(i, -1, -1);
        	SocketMessageProcessorService o = applicationContext.getBean("socketMessageProcessorService", SocketMessageProcessorService.class);
        	o.setRequestService(applicationContext.getBean("socketRequestQueueService", SocketRequestQueueService.class));
        	o.setMetaData(metaData);
        	executorService.submit(o);
        	
        	this.processorMetaDataSet.add(metaData);
        }
    }
}
