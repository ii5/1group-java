package ii5.socket.server.core;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

import org.glassfish.tyrus.client.ClientManager;

public class TestUat2WebSocketClientWrapper
{
    public static long startTime;
    public static long endTime;

    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;

            headers.put("authorization", Arrays.asList("bearer 876a1c7a-828d-4366-923a-9cb0d2ca0f89"));
        }
    }

    public static void main(String args[]) throws IOException
    {
        runClient();
    }

    private static void runClient() throws IOException
    {
        final MyClientConfigurator clientConfig = new MyClientConfigurator();

        ClientManager client = ClientManager.createClient();
        client.getProperties().put("org.glassfish.tyrus.incomingBufferSize", Integer.MAX_VALUE);
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {

            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://localhost:8025/websockets/base"));

            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter json:");
            while (scanner.hasNext())
            {
                String input = scanner.nextLine();
                if (input.trim().isEmpty())
                {
                    continue;
                }
                if (!"exit".equals(input))
                {
                    for (int i = 0; i < 1; i++)
                    {
                        SocketEntity e = (SocketEntity) Utils.getInstanceFromJson(input, SocketEntity.class);
                        e.setClientRequestSentTime(System.currentTimeMillis());
                        session.getAsyncRemote().sendText(Utils.getJsonString(e));
                        System.out.println(i + " Sending json to server .....");
                        startTime = System.currentTimeMillis();
                    }
                }
                else
                {
                    break;
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
    }
}
