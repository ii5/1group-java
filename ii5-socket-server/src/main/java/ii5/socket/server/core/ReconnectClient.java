package ii5.socket.server.core;

import ii5.socket.server.core.WebSocketClientWrapperReceiver.MyClientConfigurator;

import java.net.URI;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.client.ClientProperties;

public class ReconnectClient
{

    public static void main(String[] args)
    {
        final CountDownLatch messageLatch = new CountDownLatch(1);

        ClientManager client = ClientManager.createClient();
        ;

        ClientManager.ReconnectHandler reconnectHandler = new ClientManager.ReconnectHandler()
        {

            private final AtomicInteger counter = new AtomicInteger(0);

            @Override
            public boolean onConnectFailure(Exception exception)
            {
                try
                {
                    Thread.sleep(1000);
                    System.out.println("Reconnecting after 5 seconds");
                    return true;
                }
                catch (InterruptedException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public long getDelay()
            {
                return 0;
            }
        };

        client.getProperties().put(ClientProperties.RECONNECT_HANDLER, reconnectHandler);

        try
        {
            final MyClientConfigurator clientConfig = new MyClientConfigurator();
            // ClientManager client = ClientManager.createClient();
            Session session = null;
            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();

            UUID ff = UUID.randomUUID();
            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://localhost:8025/websockets/base"));
            UUID ii = UUID.randomUUID();
            while (true)
            {
                // ystem.out.println(ii);
            }
            // client.connectToServer(TyrusClientEndpoint.class, cec, new
            // URI("ws://localhost:8025/websockets/base"));
            /*
             * client.connectToServer(new Endpoint() {
             * 
             * @Override public void onOpen(Session session, EndpointConfig
             * config) { try {
             * session.getBasicRemote().sendText("Do or do not, there is no try."
             * ); } catch (IOException e) { // do nothing. } } },
             * ClientEndpointConfig.Builder.create().build(),
             * URI.create("ws://localhost:8025/websockets/base"));
             */
        }
        catch (Exception e)
        {
            // ignore.
        }

    }
}
