package ii5.socket.server.core;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PathParamType;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

import org.glassfish.tyrus.client.ClientManager;

public class TestUat2WebSocketClient
{
    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;
        String authToken = null;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer " + authToken));

        }
    }

    public static void main(String args[]) throws IOException
    {
        String authToken = args[0];
        String fromAccountId = !Utils.isNullOrEmpty(args[1]) ? args[1] : null;
        String toAccountId = !Utils.isNullOrEmpty(args[2]) ? args[2] : null;
        String chatFlag = !Utils.isNullOrEmpty(args[3]) ? args[3] : "false";
        runClient(authToken, fromAccountId, toAccountId, chatFlag);
    }

    private static String getMessage(String authToken, String fromAccountId, String toAccountId)
    {
        RequestEntity requestEntity = new RequestEntity();
        SocketEntity socketEntity = new SocketEntity();

        HashMap<HttpHeaderType, String> headers = new HashMap<HttpHeaderType, String>();
        headers.put(HttpHeaderType.AUTHORISATION, "bearer " + authToken);
        socketEntity.setHeaders(headers);

        WSMessage wsMessage = new WSMessage();

        wsMessage.setText("Hello - " + new Date(Utils.getSystemTime()));

        wsMessage.setType(MessageType.TEXT.name());
        wsMessage.setCreatedById(fromAccountId);
        wsMessage.setToAccountId(toAccountId);
        wsMessage.setClientSentTime(Utils.getSystemTime() + "");
        requestEntity.setMessage(wsMessage);

        socketEntity.setEndpoint("/messages");
        socketEntity.setRequestType(HTTPRequestMethodType.POST);
        socketEntity.setRequest(requestEntity);

        HashMap<PathParamType, String> pathParams = new HashMap<PathParamType, String>();
        pathParams.put(PathParamType.VERSION, "v2");
        socketEntity.setPathParams(pathParams);

        return Utils.getJsonString(socketEntity);
    }

    private static void runClient(String authToken, String fromAccountId, String toAccountId, String chatFlag) throws IOException
    {

        final MyClientConfigurator clientConfig = new MyClientConfigurator();
        clientConfig.authToken = authToken;
        ClientManager client = ClientManager.createClient();
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {
            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://api2.ii5.com:8025/websockets/base"));

            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter json:");
            while (scanner.hasNext())
            {
                String input = scanner.nextLine();
                if (!"exit".equals(input))
                {
                    int i = 0;
                    while (true)
                    {
                        System.out.println(i + " Sending json to server .....");
                        session.getBasicRemote().sendText(input);

                        if (chatFlag.equals("true"))
                        {
                            String message = getMessage(authToken, fromAccountId, toAccountId);
                            System.out.println("Message[" + message + "]");
                            session.getBasicRemote().sendText(message);
                        }
                        i++;

                        Thread.sleep(10000);
                    }
                }
                else
                {
                    break;
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
    }
}
