package ii5.socket.server.core;

import java.util.List;
import java.util.Map;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

public class SocketServerConfig extends ServerEndpointConfig.Configurator
{

    @Override
    public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response)
    {
        Map<String, List<String>> ggg = request.getHeaders();
        System.out.println(ggg);

        List<String> headers = request.getHeaders().get("authorization");
        String token = null;
        if (headers != null && headers.size() > 0)
        {
            for (String authHeader : headers)
            {
                if ((authHeader.toLowerCase().startsWith("Bearer".toLowerCase())))
                {
                    token = authHeader.substring("Bearer".length()).trim();
                }
            }
        }
        System.out.println("Token in config:" + token);
        config.getUserProperties().put("token", token);
        super.modifyHandshake(config, request, response);
    }
}
