package ii5.socket.server.core.queue;

import java.io.Serializable;

/**
 * 
 * @author nyalf
 *
 */
public class MessageProcessorMetaData implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int id;
    private long lastMessageProcessingTime;
    private long currentMessageStartTime;
    private int processedCount = 0;
    private String currentMessageProcessing;
    private boolean isAlive = true;
    
    public MessageProcessorMetaData(int id, long lastMessageProcessingTime, long currentMessageProcessingStartTime)
    {
    	this.id = id;
    	this.lastMessageProcessingTime = lastMessageProcessingTime;
    	this.currentMessageStartTime = currentMessageProcessingStartTime;
    }

    public boolean isAlive() 
    {
		return isAlive;
	}
    
    public void setAlive(boolean isAlive) 
    {
		this.isAlive = isAlive;
	}
    
	public int getId() 
	{
		return id;
	}
	
	public long getLastMessageProcessingTime() 
	{
		return lastMessageProcessingTime;
	}

	public void setLastMessageProcessingTime(long lastMessageProcessingTime) 
	{
		this.lastMessageProcessingTime = lastMessageProcessingTime;
	}

	public long getCurrentMessageStartTime() 
	{
		return currentMessageStartTime;
	}

	public void setCurrentMessageStartTime(long currentMessageStartTime) 
	{
		this.currentMessageStartTime = currentMessageStartTime;
	}

	public int getProcessedCount() 
	{
		return processedCount;
	}

	public void incrementProcessedCount()
	{
		this.processedCount++;
	}
	
	public String getCurrentMessageProcessing() 
	{
		return currentMessageProcessing;
	}
	
	public void setCurrentMessageProcessing(String currentMessageProcessing) 
	{
		this.currentMessageProcessing = currentMessageProcessing;
	}

	@Override
	public String toString() {
		return "MessageProcessorMetaData [id=" + id + ", lastMessageProcessingTime=" + lastMessageProcessingTime
				+ ", currentMessageStartTime=" + currentMessageStartTime + ", processedCount=" + processedCount
				+ ", currentMessageProcessing=" + currentMessageProcessing + ", isAlive=" + isAlive + "]";
	}
    
    
}
