package ii5.socket.server.core;

import java.util.Date;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class TyrusClientEndpoint extends Endpoint
{
	private static int count = 0;
    @Override
    public void onOpen(final Session session, EndpointConfig config)
    {

        /*
         * try { session.getBasicRemote().sendText("Joining"); } catch
         * (IOException e) { e.printStackTrace(); }
         */

        session.addMessageHandler(new MessageHandler.Whole<String>()
        {
            public void onMessage(String message)
            {
            	try
            	{
	            	long receivedTime = System.currentTimeMillis();
	            	SocketEntity e = (SocketEntity)Utils.getInstanceFromJson(message, SocketEntity.class);
	            	e.setClientResponseReceivedTime(receivedTime);
	                System.out.println(++count + " [" + (System.currentTimeMillis() - TestUat2WebSocketClientWrapper.startTime) + ": " + Utils.getJsonString(e));
	                Thread.sleep(200);
	                System.out.println("Client Request Sent Time: " + new Date(e.getClientRequestSentTime()));
	                System.out.println("Server Request Received Time: " + new Date(e.getServerRequestReceivedTime()));
	                System.out.println("Server Request Processing Time: " + new Date(e.getServerRequestProcessingTime()));
	                System.out.println("Client Response Received Time: " + new Date(e.getClientResponseReceivedTime()));
                
            	}
            	catch (Exception e)
            	{
            		System.out.println("Exception while redering message: " + message);
            	}
            }

//            public void onMessage(PongMessage pongMessage)
//            {
//                System.out.println(pongMessage);
//            }
        });

//        new Thread(new Runnable()
//        {
//            public void run()
//            {
//                ByteBuffer buffer = ByteBuffer.allocate(1);
//                buffer.put((byte) 0xFF);
//                try
//                {
//                    session.getBasicRemote().sendPing(buffer);
//                    Thread.sleep(5000);
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
    }
}