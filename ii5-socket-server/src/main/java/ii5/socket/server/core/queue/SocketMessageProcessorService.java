package ii5.socket.server.core.queue;

import ii5.socket.server.core.SessionHolder;
import ii5.socket.server.core.processor.Processor;
import ii5.socket.server.endpoint.IEndpointElement;

import java.io.Serializable;
import java.util.Date;
import java.util.Properties;

import javax.websocket.Session;

import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.StoreKey;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.response.WSResult;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.socket.ResponseEntity;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AuthorizationExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.ClientService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SyncLogProducer;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SocketMessageProcessorService implements Runnable, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(SocketMessageProcessorService.class);
    private SocketRequestQueueService requestService;
    private IEndpointElement masterEndpointStructure;
    private Processor<String, SocketEntity> socketReader;
    private Processor<String, IEndpointElement> stringEndpointUrlBuilder;
    private Processor<SocketEntity, SocketEntity> socketWriter;
    private Processor<SocketEntity, SocketEntity> inputStringProcessor;
    private ClientService clientService;
    private Properties socketProperties;
    private SessionHolder sessionHolder;
    private MessageProcessorMetaData metaData;

    private SyncLogProducer kafkaProducer;
    private KafkaConfiguration kafkaConfiguration;

    public SocketMessageProcessorService()
    {
        logger.info("[" + metaData + "] " + this.getClass().getSimpleName() + " initiated.");
    }

    public MessageProcessorMetaData getMetaData()
    {
        return metaData;
    }

    public void setMetaData(MessageProcessorMetaData metaData)
    {
        this.metaData = metaData;
    }

    public SessionHolder getSessionHolder()
    {
        return sessionHolder;
    }

    public void setSessionHolder(SessionHolder sessionHolder)
    {
        this.sessionHolder = sessionHolder;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public Properties getSocketProperties()
    {
        return socketProperties;
    }

    public void setSocketProperties(Properties socketProperties)
    {
        this.socketProperties = socketProperties;
    }

    public SocketMessageProcessorService(SocketRequestQueueService requestService)
    {
        this.requestService = requestService;
    }

    public void setRequestService(SocketRequestQueueService requestService)
    {
        this.requestService = requestService;
    }

    public Processor<String, IEndpointElement> getStringEndpointUrlBuilder()
    {
        return stringEndpointUrlBuilder;
    }

    public void setStringEndpointUrlBuilder(Processor<String, IEndpointElement> stringEndpointUrlBuilder)
    {
        this.stringEndpointUrlBuilder = stringEndpointUrlBuilder;
    }

    public IEndpointElement getMasterEndpointStructure()
    {
        return masterEndpointStructure;
    }

    public void setMasterEndpointStructure(IEndpointElement masterEndpointStructure)
    {
        this.masterEndpointStructure = masterEndpointStructure;
    }

    public Processor<String, SocketEntity> getSocketReader()
    {
        return socketReader;
    }

    public void setSocketReader(Processor<String, SocketEntity> socketReader)
    {
        this.socketReader = socketReader;
    }

    public Processor<SocketEntity, SocketEntity> getSocketWriter()
    {
        return socketWriter;
    }

    public void setSocketWriter(Processor<SocketEntity, SocketEntity> socketWriter)
    {
        this.socketWriter = socketWriter;
    }

    public Processor<SocketEntity, SocketEntity> getInputStringProcessor()
    {
        return inputStringProcessor;
    }

    public void setInputStringProcessor(Processor<SocketEntity, SocketEntity> inputStringProcessor)
    {
        this.inputStringProcessor = inputStringProcessor;
    }

    public SyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public void run()
    {
        while (true)
        {
            RequestQueueObject requestInput = requestService.fetchRequestFromQueue();

            if (requestInput == null)
                continue;

            Session session = requestInput.getSession();
            SocketEntity output = new SocketEntity();

            metaData.setCurrentMessageStartTime(System.currentTimeMillis());
            metaData.setCurrentMessageProcessing(requestInput.getMessage());

            SocketEntity writtenObject = output;
            try
            {

                output = socketReader.process(requestInput.getMessage());
                output.setServerRequestReceivedTime(requestInput.getReceivedTime());
                output = readInput(requestInput, output);

                // String input = requestService.fetchRequestFromQueue();
                // logger.info("Processing " + input);
                // SocketEntity output = socketReader.process(input);

                // SocketEntity output = requestService.fetchRequestFromQueue();
                Date date = new Date();
                logger.info("[" + date + "] Fetch from request queue [" + output.getRequestHash() + "]" + "[" + output.getEndpoint() + "]");
                IEndpointElement url = stringEndpointUrlBuilder.process(output.getEndpoint());

                IEndpointElement matchingRootElement = matches(url, masterEndpointStructure);

                if (matchingRootElement == null)
                {
                    throw new IllegalStateException("URL Match not found!");
                }

                output.addToStore(StoreKey.URL_ENDPOINT, matchingRootElement);
                // output.addPathParam(PathParamType.VERSION, "v2");

                SocketEntity processedOutput = inputStringProcessor.process(output);
                date = new Date();
                logger.info("[" + date + "] Pushing to socketWriter [" + output.getRequestHash() + "]" + "[" + output.getEndpoint() + "]");
                writtenObject = socketWriter.process(processedOutput);

                logger.info("[" + new Date() + "] Finshed processing [" + output.getRequestHash() + "]" + "[" + output.getEndpoint() + "]");

            }
            catch (Abstract1GroupException ame)
            {
                try
                {
                    logger.error("Exception while processing message.[" + output + "]", ame);

                    ResponseEntity responseEntity = new ResponseEntity();
                    WSResult result = new WSResult();
                    result.setCode(ame.getCode());
                    result.setIsFatal(false);
                    result.setMessage(ame.getMessage());
                    responseEntity.setResult(result);
                    output.setResponse(responseEntity);
                    // logger.info(Utils.getJsonString(output));
                    Date date = new Date();
                    socketWriter.process(output);

                    if (ame.getHttpCode() == 401)
                    {
                        logger.info("[" + date + "]session closed" + "[" + session.getId() + "]for unauthorized client");
                        session.close();
                    }
                }
                catch (Exception in)
                {
                    logger.error("Exception while reporting error for : " + requestInput);
                }
                ;
                // session.close();
            }
            catch (Exception e)
            {
                try
                {
                    logger.error("Exception while processing message.", e);
                    ResponseEntity responseEntity = new ResponseEntity();
                    WSResult result = new WSResult();
                    result.setCode("16006");
                    result.setIsFatal(true);
                    result.setMessage(e.getMessage());
                    responseEntity.setResult(result);
                    output.setResponse(responseEntity);

                    Date date = new Date();
                    socketWriter.process(output);
                }
                catch (Exception in)
                {
                    logger.error("Exception while reporting error for : " + requestInput);
                }
                ;
            }
            finally
            {
                try
                {
                    writtenObject.getStore().remove(StoreKey.URL_ENDPOINT);
                    if (writtenObject.getResponse() != null)
                    {
                        writtenObject.getResponse().setData(null);
                    }
                    String key = "0";
                    if (!Utils.isNull(writtenObject.getStore().get(StoreKey.CURRENT_ACCOUNT_ID)))
                    {
                        key = writtenObject.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
                    }

                    kafkaProducer.write(writtenObject, kafkaConfiguration.getTopic(TopicType.LOG).getName(), key);
                }
                catch (SyncLogProcessException e)
                {
                    logger.error("Exception while writing into LOG + ", e);
                }
                catch (Exception e)
                {
                    logger.error("Exception while saving into log", e);
                }
            }

            metaData.setLastMessageProcessingTime(System.currentTimeMillis() - metaData.getCurrentMessageStartTime());
            metaData.incrementProcessedCount();
            metaData.setCurrentMessageStartTime(-1);
            metaData.setCurrentMessageProcessing(null);
        }
    }

    private IEndpointElement matches(IEndpointElement urlToMatch, IEndpointElement rootElement)
    {
        IEndpointElement leaf = null;
        if (urlToMatch.equals(rootElement))
        {
            if (logger.isDebugEnabled()) 
            {
            	logger.info("Matches[" + urlToMatch + ", " + rootElement);
            }
            
            if (urlToMatch.getLeafNodes().isEmpty())
            {
                return rootElement;
            }

            for (IEndpointElement e : urlToMatch.getLeafNodes().values())
            {
                for (IEndpointElement rootLeafElement : rootElement.getLeafNodes().values())
                {
                    leaf = matches(e, rootLeafElement);
                    if (leaf != null)
                    {
                        return leaf;
                    }
                }
            }
        }
        else
        {
            // logger.info("Does not Match[" + urlToMatch + ", " +
            // rootElement);
        }
        return leaf;
    }

    private SocketEntity readInput(RequestQueueObject queueInput, SocketEntity input) throws Exception
    {
        String message = queueInput.getMessage();
        Session session = queueInput.getSession();
        Date date = new Date();

        try
        {
            String reqToken = Utils.getAuthTokenFromHeaderString(input.getHeader(HttpHeaderType.AUTHORISATION));

            if (reqToken == null || reqToken.isEmpty())
            {
                throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
            }

            // String accountId =
            // clientService.getAccountIdByAuthToken(reqToken);
            WSAccount account = sessionHolder.getAccount(reqToken);
            WSClient client = sessionHolder.getClient(reqToken);

            Long sessionRegisteredAccount = sessionHolder.getAccountId(session);

            if (account == null)
            {
                logger.error("Account not found in session holder.");
                if (sessionRegisteredAccount != null)
                {
                    input.addToStore(StoreKey.CURRENT_ACCOUNT_ID, sessionRegisteredAccount);
                    input.addToStore(StoreKey.CURRENT_SESSION_ID, session.getId());
                }
                throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
            }

            if (client == null)
            {
                logger.error("Client not associated for account " + account.getId() + " with token " + reqToken);
                throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
            }

            String accountId = account.getId();

            if (!accountId.equals(sessionRegisteredAccount + ""))
            {
                logger.error("Session Opened and requests made from different auth tokens.");
                input.addToStore(StoreKey.CURRENT_ACCOUNT_ID, sessionRegisteredAccount);
                input.addToStore(StoreKey.CURRENT_SESSION_ID, session.getId());
                throw new AuthenticationException(AuthorizationExceptionCode.USER_ACCESS_DENIED, true);
            }

            input.addToStore(StoreKey.CURRENT_ACCOUNT_ID, Long.valueOf(accountId));
            input.addToStore(StoreKey.CURRENT_SESSION_ID, session.getId());
            input.addToStore(StoreKey.MBEAN_SERVICE_URL, socketProperties.getProperty("serviceUrl"));
            input.addToStore(StoreKey.MBEAN_OBJECT_NAME, socketProperties.getProperty("objectName"));
            input.addToStore(StoreKey.CURRENT_WS_ACCOUNT, sessionHolder.getAccount(reqToken));
            input.addToStore(StoreKey.CURRENT_WS_CLIENT, sessionHolder.getClient(reqToken));

            if (!(account.getStatus().equals(Status.ACTIVE) || account.getStatus().equals(Status.ADMIN)))
            {
                logger.error("Account " + accountId + " not ACTIVE.");
                throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
            }

            if (Status.ADMIN.equals(account.getStatus()))
            {
                // Used for json response

                // COMMENTING FOR NOW SINCE THE TOOL TAKES A LOT OF TIME TO LOAD
                // input.addToStore(StoreKey.FROM_BPO, "true");
            }

            String requestHash = input.getRequestHash();

            if (requestHash == null || requestHash.trim().isEmpty())
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
            }

            logger.info("[" + date + "]Token in onMessage[" + reqToken + "] from request header. accountId[" + accountId + "]");
            input.getHeaders().put(HttpHeaderType.AUTHORISATION, reqToken);
            // if (!token.equals(reqToken))
            // {
            // throw new
            // AuthenticationException(GeneralExceptionCode.UNAUTHORIZED, true);
            // }

        }
        catch (Abstract1GroupException abe)
        {
            throw abe;
        }

        return input;
    }

}
