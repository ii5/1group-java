
package ii5.socket.server.core;

import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.apache.log4j.xml.DOMConfigurator;
import org.glassfish.tyrus.client.ClientManager;

import one.group.core.enums.FilterField;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.ResponseEntity;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class TyrusClient
{

    // UAT ADMIN TOKEN : 6961cf2e-f048-4ab3-811c-de92d50dcd25

    private static CountDownLatch messageLatch;

    private static long start;
    private static long end;

    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer 26618758-673a-4ada-af77-da100d3dc80b"));
        }
    }

    public static void main(String[] args) throws Exception
    {
    	DOMConfigurator.configure("log4j.xml");
        int runCount = 1;
        while (runCount != 0)
        {
        	HashMap<HttpHeaderType, String> headers = new HashMap<HttpHeaderType, String>();
	        HashMap<QueryParamType, String> queryParamMap = new HashMap<QueryParamType, String>();
	        HashMap<PathParamType, String> pathParamMap = new HashMap<PathParamType, String>();
	        final SocketEntity se = new SocketEntity();
	        RequestEntity request = new RequestEntity();
	        ResponseEntity response = new ResponseEntity();
	        HashMap<SortField, SortType> sort = new HashMap<SortField, SortType>();
	        HashMap<FilterField, Set<String>> filter = new HashMap<FilterField, Set<String>>();
	        
	        headers.put(HttpHeaderType.AUTHORISATION, "bearer 26618758-673a-4ada-af77-da100d3dc80b");
	        
	        pathParamMap.put(PathParamType.VERSION, "v2");
	        
	        queryParamMap.put(QueryParamType.GROUP_IDS, "BPO Operator");
	        
	        se.setEndpoint("/bpo/messages");
	        se.setRequestType(HTTPRequestMethodType.POST);
	        
	        queryParamMap.put(QueryParamType.LIMIT, "10");
	        
	        Map<String, Set<WSMessage>> messagesToPost = new HashMap<String, Set<WSMessage>>();
	        Set<WSMessage> messageSet = new HashSet<WSMessage>();
	        messagesToPost.put("TEST-GROUP", messageSet);
	        
	        for (int i = 0; i < 200; i++)
	        {
	        	WSMessage message = new WSMessage();
	        	message.setId("TEST" + Utils.randomUUID());
	        	message.setText("TEST-TEXT-" + runCount + "-" + i + "-" + System.currentTimeMillis());
	        	Calendar c = Calendar.getInstance();
	        	c.add(Calendar.DATE, -10);
	        	message.setClientSentTime(c.getTime().getTime()+"");
	        	message.setCreatedByMobileNumber("+919833143042");
	        	message.setType(MessageType.TEXT.toString());
	        	
	        	messageSet.add(message);
	        }
	        
	        request.setPostMessages(messagesToPost);
	        
	        se.setRequest(request);
	        se.setHeaders(headers);
	        se.setPathParams(pathParamMap);
	        se.setQueryParams(queryParamMap);
	        se.setSort(sort);
	        se.setFilter(filter);
	        se.setRequestHash("1");
	
	        System.out.println(runCount + " : " + Utils.getJsonString(se));
	        
	        try
	        {
	            messageLatch = new CountDownLatch(1);
	
	            final MyClientConfigurator clientConfig = new MyClientConfigurator();
	
	            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
	            
	
	            ClientManager client = ClientManager.createClient();
	            client.getProperties().put("org.glassfish.tyrus.incomingBufferSize", Integer.MAX_VALUE);
	            client.connectToServer(new Endpoint()
	            {
	                @Override
	                public void onOpen(Session session, EndpointConfig config)
	                {
	                    try
	                    {
//	                        session.addMessageHandler(new MessageHandler.Whole<String>()
//	                        {
//	                            public void onMessage(String message)
//	                            {
//	                                end = System.currentTimeMillis() - start;
//	                                System.out.println("Received message: (" + end + ") " + message);
//	                                messageLatch.countDown();
//	                            }
//	                        });
	
	                       
	                        session.getAsyncRemote().sendText(Utils.getJsonString(se));
	                        messageLatch.countDown();
	                        start = System.currentTimeMillis();
	                        // Thread.sleep(2000);
	
	                    }
	                    catch (Exception e)
	                    {
	                        e.printStackTrace();
	                    }
	                }
	                
	                @Override
	                public void onClose(Session session, CloseReason closeReason) 
	                {
	                	System.out.println("Session " + session.getId() + " closed. Reason : " + closeReason);
	                	super.onClose(session, closeReason);
	//                	try {session.close();} catch (Exception e) {}
	                	System.exit(1);
	                }
	
	            }, cec, new URI("ws://localhost:8025/websockets/base"));
	            messageLatch.await(500, TimeUnit.SECONDS);
	
//	             Thread.sleep(200000);
	
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	        finally
	        {
	
	        }
	        
	        runCount--;
        }
        
        Thread.sleep(2000000);

    }
    
    
}
