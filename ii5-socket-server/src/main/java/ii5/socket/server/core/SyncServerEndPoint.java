package ii5.socket.server.core;

import java.io.IOException;
import java.util.Set;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ii5.socket.server.core.processor.Processor;
import ii5.socket.server.core.queue.SocketAuthenticationProcessService;
import ii5.socket.server.core.queue.SocketRequestQueueService;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.AuthenticationException;
import one.group.services.ClientService;
import one.group.services.helpers.PushServiceObject;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */

@ServerEndpoint(value = "/sync", configurator = SocketServerConfig.class)
public class SyncServerEndPoint
{
    private SocketAuthenticationProcessService authProcessService;
    private SocketRequestQueueService requestQueueService;
    private ClientService clientService;
    private Processor<String, SocketEntity> socketReader;
    private Logger logger = LoggerFactory.getLogger(SyncServerEndPoint.class);
    private SessionHolder sessionHolder;
    
    public SessionHolder getSessionHolder() 
    {
		return sessionHolder;
	}
    
    public void setSessionHolder(SessionHolder sessionHolder) 
    {
		this.sessionHolder = sessionHolder;
	}

    public Processor<String, SocketEntity> getSocketReader()
    {
        return socketReader;
    }

    public void setSocketReader(Processor<String, SocketEntity> socketReader)
    {
        this.socketReader = socketReader;
    }

    public SocketAuthenticationProcessService getAuthProcessService()
    {
        return authProcessService;
    }

    public void setAuthProcessService(SocketAuthenticationProcessService authProcessService)
    {
        this.authProcessService = authProcessService;
    }

    public SocketRequestQueueService getRequestQueueService()
    {
        return requestQueueService;
    }

    public void setRequestQueueService(SocketRequestQueueService requestQueueService)
    {
        this.requestQueueService = requestQueueService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    private void processSync(String message)
    {
        try
        {
            PushServiceObject pushServiceObject = (PushServiceObject) Utils.getInstanceFromJson(message, PushServiceObject.class);
            String dataToPush = Utils.getJsonString(pushServiceObject.getData());
            System.out.println("pushServiceObject - " + Utils.getJsonString(pushServiceObject));
            String toAccountIdStr = pushServiceObject.getAccountId();

            long toAccountId = Long.parseLong(toAccountIdStr);

            Set<Session> sessions = sessionHolder.getSessions(toAccountId);

            if (sessions != null)
            {
                System.out.println("Total clients of account[" + toAccountId + "] session[" + sessions.size() + "]");
                for (Session session : sessions)
                {
                    session.getAsyncRemote().sendText(dataToPush);
                }
            }
            else
            {
                System.out.println("No clients of account[" + toAccountId + "] ");
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @OnMessage
    public void onMessage(String message, Session session, EndpointConfig configuration) throws Exception
    {
        processSync(message);
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig configuration) throws IOException, AuthenticationException
    {
    	logger.info("OPEN : " + session);
        /*
         * String token = (String)
         * configuration.getUserProperties().get("token"); if (token == null ||
         * token.isEmpty()) { throw new
         * AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED,
         * true); }
         * 
         * boolean isAuthorized = false; try { isAuthorized =
         * authProcessService.isAuthorized(token); } catch (Exception e) {
         * e.printStackTrace(); } if (!isAuthorized) {
         * 
         * throw new
         * AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED,
         * true); }
         * 
         * String accountId = clientService.getAccountIdByAuthToken(token);
         * Set<Session> accountsSessionSet =
         * Holder.ACCOUNT_SESSION_SET_MAP.get(accountId);
         * 
         * if (accountsSessionSet == null) { accountsSessionSet = new
         * HashSet<Session>(); }
         * 
         * accountsSessionSet.add(session);
         * Holder.ACCOUNT_SESSION_SET_MAP.put(Long.parseLong(accountId),
         * accountsSessionSet);
         * Holder.SESSION_ACCOUNT_ID_MAP.put(session.getId(),
         * Long.valueOf(accountId));
         */
    }

    @OnClose
    public void onClose(Session session, EndpointConfig configuration) throws IOException
    {
    	logger.info("CLOSE: " + session);
        /*
         * Long accountId = Holder.SESSION_ACCOUNT_ID_MAP.get(session.getId());
         * Holder.ACCOUNT_SESSION_SET_MAP.get(accountId).remove(session);
         * Holder.SESSION_ACCOUNT_ID_MAP.remove(session.getId());
         */
    }

    @OnError
    public void onError(Session session, Throwable t) throws IOException
    {
    	logger.info("ERROR :" + session);
        t.printStackTrace();
    }
}
