package ii5.socket.server.core;

import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import one.group.entities.socket.SocketEntity;

public class Lookup 
{
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		Map environment = new HashMap();
		String url = "service:jmx:jmxmp://localhost:9875";
		JMXServiceURL jmxUrl = new JMXServiceURL("service:jmx:jmxmp://localhost:9876");
		JMXConnector  jmxc = JMXConnectorFactory.connect(jmxUrl, environment);
		MBeanServerConnection  mbsc = jmxc.getMBeanServerConnection();
		
		mbsc.invoke(new ObjectName("bean:name=socketWriter"), "process",new Object[] {new SocketEntity()}, new String[]{SocketEntity.class.getName()});
		
	}
}
