package ii5.socket.server.core.writer;

import java.util.Date;
import java.util.Set;

import javax.websocket.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ii5.socket.server.core.SessionHolder;
import ii5.socket.server.core.processor.Processor;
import one.group.core.Constant;
import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.enums.StoreKey;
import one.group.entities.api.request.WSClient;
import one.group.entities.socket.SocketEntity;
import one.group.services.helpers.EnvironmentConfiguration;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SocketWriter implements Processor<SocketEntity, SocketEntity>
{
	private static final Logger logger = LoggerFactory.getLogger(SocketWriter.class);
	private SessionHolder sessionHolder;
	private EnvironmentConfiguration environmentConfiguration;
	
	public EnvironmentConfiguration getEnvironmentConfiguration() 
	{
		return environmentConfiguration;
	}
	
	public void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration) 
	{
		this.environmentConfiguration = environmentConfiguration;
	}
	
	public SessionHolder getSessionHolder() 
	{
		return sessionHolder;
	}
	
	public void setSessionHolder(SessionHolder sessionHolder) 
	{
		this.sessionHolder = sessionHolder;
	}

    public SocketEntity process(SocketEntity input) throws Exception
    {
        Date date = new Date();
        logger.info("[" + date + "]Pushing to clients [" + input.getRequestHash() + "]" + "[" + input.getEndpoint() + "]");
//        logger.info(input.getResponse() + "");
        
        // Since it is a last moment change, adding this is a try catch block. The response should not fail because of this.
        // Commented this for v2.2 because of a bug on client.
        /*
        try
        {
        	String softUpdateOn = environmentConfiguration.getSoftUpdateOn();
	       
	        if (Constant.STRING_TRUE.equals(softUpdateOn))
		    {
	        	input.addPathParam(PathParamType.VERSION, APIVersion.currentVersion().toString());
		    }	        
        } 
        catch (Exception e)
        {
        	logger.error("Exception while setting path param version in response.", e);
        }
        */
        
        Long accountId = (Long) input.getFromStore(StoreKey.CURRENT_ACCOUNT_ID);
        Set<Session> sessionSet = sessionHolder.getSessions(accountId);
        
        input.setServerRequestProcessingTime(System.currentTimeMillis());
        SocketEntity toSend = new SocketEntity();
        toSend.setRelevantResponseData(input);
        

        if (sessionSet != null)
        {
        	logger.info("Potential target count to receive data: " + sessionSet.size());
            for (Session s : sessionSet)
            {

                // Break loop after sending response to client which has sent
                // request.
            	String currentSessionId = input.getStore().get(StoreKey.CURRENT_SESSION_ID) == null ? null : input.getStore().get(StoreKey.CURRENT_SESSION_ID).toString();
            	if (currentSessionId != null)
            	{
	                
	                if (currentSessionId.equals(s.getId()))
	                {
	                    Object fromBpo = input.getFromStore(StoreKey.FROM_BPO);
	
	                    long start = System.currentTimeMillis();
	                    if (fromBpo != null)
	                    {
	                        s.getAsyncRemote().sendText(Utils.getJsonStringForBPO(toSend));
	                    }
	                    else
	                    {
	                        s.getAsyncRemote().sendText(Utils.getJsonString(toSend));
	                    }
	                    long end = System.currentTimeMillis();
	                    logger.info("[" + date + "] Sent to [" + s.getId() + "] [" + accountId + "] in " + (end - start));
	                    break;
	                }
            	}
            	else
            	{
            		Object fromBpo = input.getFromStore(StoreKey.FROM_BPO);

                    long start = System.currentTimeMillis();
                    if (fromBpo != null)
                    {
                        s.getAsyncRemote().sendText(Utils.getJsonStringForBPO(toSend));
                    }
                    else
                    {
                        s.getAsyncRemote().sendText(Utils.getJsonString(toSend));
                    }
                    long end = System.currentTimeMillis();
                    logger.info("[" + date + "] Sent to [" + s.getId() + "] [" + accountId + "] in " + (end - start));
            	}
            }
            date = new Date();
//            logger.info("[" + date + "]Responded to: (" + toSend.getHeader(HttpHeaderType.AUTHORISATION) + ") " + toSend.getEndpoint() + " : " + toSend.getRequestHash());
//            logger.info("for " + Utils.getJsonString(toSend.getRequest()));
        }
        else
        {
        	logger.warn("No sessions of [" + accountId + "] alive to send date " + Utils.getJsonString(toSend));
        }
        return input;
    }
}
