package ii5.socket.server.core.processor;

import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.endpoint.impl.MatchingLeafnodesVisitor;
import ii5.socket.server.endpoint.impl.UrlEndpoint;
import ii5.socket.server.endpoint.reader.AnnotationEndpointBuilder;

import java.io.IOException;

import one.group.entities.socket.SocketEntity;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SimpleSocketProcessor implements Processor<SocketEntity, SocketEntity>
{
    public SocketEntity process(SocketEntity input) throws JsonMappingException, JsonGenerationException, IOException
    {
        String endPoint = input.getEndpoint();

        AnnotationEndpointBuilder e = new AnnotationEndpointBuilder();
        MatchingLeafnodesVisitor visitor = new MatchingLeafnodesVisitor();

        IEndpointElement populatedEndpoint = e.read();
        System.out.println(populatedEndpoint);

        String baseUrl = "http://10.50.249.94:8080/1group/";
        IEndpointElement urlEndpoint = new UrlEndpoint(baseUrl, null);
        IEndpointElement versionEndpoint = new UrlEndpoint("{version}", urlEndpoint);
        IEndpointElement accounts = new UrlEndpoint("accounts", versionEndpoint);
        IEndpointElement accountsId = new UrlEndpoint("9699962336797799", accounts);
        IEndpointElement broadcasts = new UrlEndpoint("broadcasts", accountsId);
        // broadcasts.addRequestType(HttpRequestType.GET);

        visitor.setMasterEndpoint(populatedEndpoint);
        urlEndpoint.accept(visitor);
        return null;
    }

    public static void main(String[] args)
    {
        AnnotationEndpointBuilder e = new AnnotationEndpointBuilder();
        MatchingLeafnodesVisitor visitor = new MatchingLeafnodesVisitor();

        IEndpointElement populatedEndpoint = e.read();

        String baseUrl = "http://10.50.249.94:8080/1group/";
        IEndpointElement urlEndpoint = new UrlEndpoint(baseUrl, null);
        IEndpointElement versionEndpoint = new UrlEndpoint("v2", urlEndpoint);
        IEndpointElement accounts = new UrlEndpoint("property_listings", versionEndpoint);
        IEndpointElement accountsId = new UrlEndpoint("9699962336797799", accounts);
        // IEndpointElement broadcasts = new UrlEndpoint("broadcasts",
        // accountsId);
        // accountsId.addRequestType(HttpRequestType.GET);

        visitor.setMasterEndpoint(populatedEndpoint);
        urlEndpoint.accept(visitor);
    }
}
