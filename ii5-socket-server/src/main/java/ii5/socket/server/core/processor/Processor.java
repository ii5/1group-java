package ii5.socket.server.core.processor;

/**
 * 
 * @author nyalfernandes
 *
 * @param <I>
 * @param <O>
 */
public interface Processor<I, O>
{
    public O process(I input) throws Exception;
}
