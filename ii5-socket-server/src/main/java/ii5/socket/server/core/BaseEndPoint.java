package ii5.socket.server.core;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ii5.socket.server.core.processor.Processor;
import ii5.socket.server.core.queue.SocketAuthenticationProcessService;
import ii5.socket.server.core.queue.SocketRequestQueueService;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.response.WSResult;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.jpa.Account;
import one.group.entities.socket.ResponseEntity;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AuthorizationExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.AccountService;
import one.group.services.ClientService;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */

@ServerEndpoint(value = "/base", configurator = SocketServerConfig.class)
public class BaseEndPoint
{
    private static final Logger logger = LoggerFactory.getLogger(BaseEndPoint.class);
    private SocketAuthenticationProcessService authProcessService;
    private SocketRequestQueueService requestQueueService;
    private ClientService clientService;
    private Processor<String, SocketEntity> socketReader;
    private SessionHolder sessionHolder;
    private Properties socketProperties;
    private AccountService accountService;
    
    public AccountService getAccountService() 
    {
		return accountService;
	}
    
    public void setAccountService(AccountService accountService) 
    {
		this.accountService = accountService;
	}

    public Properties getSocketProperties()
    {
        return socketProperties;
    }

    public void setSocketProperties(Properties socketProperties)
    {
        this.socketProperties = socketProperties;
        System.out.println(socketProperties);
    }

    public SessionHolder getSessionHolder()
    {
        return sessionHolder;
    }

    public void setSessionHolder(SessionHolder sessionHolder)
    {
        this.sessionHolder = sessionHolder;
    }

    public Processor<String, SocketEntity> getSocketReader()
    {
        return socketReader;
    }

    public void setSocketReader(Processor<String, SocketEntity> socketReader)
    {
        this.socketReader = socketReader;
    }

    public SocketAuthenticationProcessService getAuthProcessService()
    {
        return authProcessService;
    }

    public void setAuthProcessService(SocketAuthenticationProcessService authProcessService)
    {
        this.authProcessService = authProcessService;
    }

    public SocketRequestQueueService getRequestQueueService()
    {
        return requestQueueService;
    }

    public void setRequestQueueService(SocketRequestQueueService requestQueueService)
    {
        this.requestQueueService = requestQueueService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    @OnMessage
    public void onMessage(String message, Session session, EndpointConfig configuration) throws Exception
    {

        String token = (String) configuration.getUserProperties().get("token");
        // logger.info("Token in onMessage[" + token +
        // "] from socket header[" + session.getId() + "]");
        
        if (logger.isDebugEnabled())
        {
        	logger.info(message);
        }
        
        Date date = new Date();
        try
        {
            requestQueueService.addRequestToQueue(message, session);
            date = new Date();
        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, e);
        }

    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig configuration) throws IOException, AuthenticationException
    {
        String token = (String) configuration.getUserProperties().get("token");
        // logger.info("Token in onOpen[" + token +
        // "] from socket header" + "[" + session.getId() + "]");
        if (token == null || token.isEmpty())
        {
            throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
        }

        boolean isAuthorized = false;
        try
        {
            isAuthorized = authProcessService.isAuthorized(token);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (!isAuthorized)
        {

            throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
        }

        try
        {
//        	Account account = clientService.getAccountFromAuthToken(token);
        	
        	WSClient wsClient = clientService.getClientByAuthToken(token);
        	WSAccount wsAccount = accountService.fetchAccountFromAuthToken(token);
        	
        	sessionHolder.addAuthToken(token, wsAccount);
        	sessionHolder.addAuthToken(token, wsClient);
	        sessionHolder.addSessionToAccount(Long.valueOf(wsAccount.getId()), session);
	        
	        Date date = new Date();
	        logger.info("[" + date + "]Token in onOpen[" + token + "] from socket header" + "[" + session.getId() + "][" + sessionHolder.getSessions(Long.valueOf(wsAccount.getId())).size() + "]");
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	throw new AuthenticationException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true);
        }

    }

    @OnClose
    public void onClose(Session session, EndpointConfig configuration) throws IOException
    {
        Date date = new Date();
        Long accountId = sessionHolder.removeSession(session);
        logger.info("[" + date + "]session closed" + "[" + session.getId() + "][" + accountId + "]");
    }

    @OnError
    public void onError(Session session, Throwable t) throws IOException
    {
        if (t instanceof Abstract1GroupException)
        {
            Abstract1GroupException ae = (Abstract1GroupException) t;

            SocketEntity socketEntity = new SocketEntity();

            ResponseEntity responseEntity = new ResponseEntity();
            WSResult result = new WSResult();
            result.setCode("16006");
            result.setIsFatal(true);
            result.setMessage(ae.getMessage());
            responseEntity.setResult(result);
            socketEntity.setResponse(responseEntity);
            logger.info(Utils.getJsonString(socketEntity));
            session.getBasicRemote().sendText(Utils.getJsonString(socketEntity));

            if (ae.getHttpCode() == 401)
            {
                Date date = new Date();
                logger.info("[" + date + "]session closed" + "[" + session.getId() + "]for unauthorized client");
                session.close();
            }

        }
        Date date = new Date();
        logger.info("[" + date + "] Error while processing request" + t.getMessage());
        t.printStackTrace();
    }
}
