package ii5.socket.server.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.StoreKey;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class SocketEntityReader 
{
	public static void main(String args[]) throws IOException
	{
		
		FileReader fr = null;
		BufferedReader br = null;
		FileWriter fw = null;
		FileWriter registrationFW = null;
		try
		{
			
			fw = new FileWriter("output.txt");
			fr = new FileReader("logs.txt");
			br = new BufferedReader(fr);
			registrationFW = new FileWriter("registrations.csv");
			String line = null;
			while ( (line = br.readLine()) != null )
			{
				SocketEntity e = (SocketEntity) Utils.getInstanceFromJson(line, SocketEntity.class);
				
				String endpoint = e.getEndpoint();
				String currentAccountId = e.getFromStore(StoreKey.CURRENT_ACCOUNT_ID) == null ? null : e.getFromStore(StoreKey.CURRENT_ACCOUNT_ID).toString();
				Long serverTime = e.getServerRequestReceivedTime() == null ? -1 : e.getServerRequestReceivedTime();
				HTTPRequestMethodType requestType = e.getRequestType();
				
				if (endpoint != null && endpoint.contains("search") && HTTPRequestMethodType.POST.equals(requestType))
				{
					WSBroadcastSearchQueryRequest search = e.getRequest().getSearch();
					if (search != null)
					{
						StringBuilder sb = new StringBuilder();
						sb.append(search.getAccountId()).append(",");
						sb.append(e.getClientRequestSentTime()).append(",");
						sb.append(search.getSearchTime()).append(",");
						sb.append(search.getCityId()).append(",");
						sb.append(search.getLocationId()).append(",");
						sb.append(search.getMinArea()).append(",");
						sb.append(search.getMaxArea()).append(",");
						sb.append(search.getMinPrice()).append(",");
						sb.append(search.getMaxPrice()).append(",");
						sb.append(search.getRooms()).append(",");
						sb.append(search.getTransactionType()).append(",");
						sb.append(search.getPropertyType()).append(",");
						sb.append(search.getPropertySubType()).append(",");
						sb.append(search.getBroadcastType());
						sb.append("\n");
						fw.append(sb.toString());
						
					}
				}
				
				
				
				fw.flush();
				
			}
		
		}
		catch (Exception e)
		{
			
		}
		finally
		{
			try
			{
				if (fr != null) fr.close();
				if (br != null) br.close();
				if (registrationFW != null) registrationFW.close();
			}
			catch (Exception e) {}
		}
		
	}
}
