package ii5.socket.server.core.queue;

import javax.transaction.Transactional;

import one.group.authenticate.impl.MoveInTokenServices;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

public class SocketAuthenticationProcessService
{
    private MoveInTokenServices tokenServices;

    public MoveInTokenServices getTokenServices()
    {
        return tokenServices;
    }

    public void setTokenServices(MoveInTokenServices tokenServices)
    {
        this.tokenServices = tokenServices;
    }

    @Transactional
    public boolean isAuthorized(String token)
    {
        OAuth2AccessToken accessToken = tokenServices.readAccessToken(token);
        if (accessToken == null)
        {
            return false;
        }
        return true;
    }
}
