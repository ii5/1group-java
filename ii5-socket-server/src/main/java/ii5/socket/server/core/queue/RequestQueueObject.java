package ii5.socket.server.core.queue;

import javax.websocket.Session;

/**
 * 
 * @author nyalf
 *
 */
public class RequestQueueObject 
{
	private String message;
	private Session session;
	private long receivedTime;
	
	public RequestQueueObject(String message, Session session, long receivedTime)
	{
		this.message = message;
		this.session = session;
		this.receivedTime = receivedTime;
	}
	
	public long getReceivedTime() 
	{
		return receivedTime;
	}
	
	public String getMessage() 
	{
		return message;
	}
	
	public Session getSession() 
	{
		return session;
	}

	@Override
	public String toString() 
	{
		return "RequestQueueObject [message=" + message + ", session=" + session + "]";
	}
	
	
}
