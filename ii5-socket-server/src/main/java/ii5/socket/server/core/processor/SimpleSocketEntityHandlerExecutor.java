package ii5.socket.server.core.processor;

import ii5.socket.server.endpoint.IEndpointElement;
import ii5.socket.server.executor.IExecutionHandler;

import java.util.Date;

import one.group.core.enums.StoreKey;
import one.group.entities.socket.SocketEntity;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SimpleSocketEntityHandlerExecutor implements Processor<SocketEntity, SocketEntity>
{

    private IExecutionHandler handler;

    public IExecutionHandler getHandler()
    {
        return handler;
    }

    public void setHandler(IExecutionHandler handler)
    {
        this.handler = handler;
    }

    public SocketEntity process(SocketEntity input) throws Exception
    {

        System.out.println("[" + new Date() + "]Processing request queue [" + input.getRequestHash() + "]" + "[" + input.getEndpoint() + "]");
        IEndpointElement element = (IEndpointElement) input.getFromStore(StoreKey.URL_ENDPOINT);
        return handler.execute(element.getExecutorObject(input.getRequestType()), input);
    }
}
