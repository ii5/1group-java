package ii5.socket.server.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;
import javax.websocket.Session;

import one.group.entities.api.request.WSClient;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.jpa.Account;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SessionHolder
{
    private Map<Long, Set<Session>> accountVsSessionMap = new HashMap<Long, Set<Session>>();
    private Map<String, Long> sessionVsAccountMap = new HashMap<String, Long>();
    private Map<String, WSAccount> authTokenVsAccountMap = new HashMap<String, WSAccount>();
    private Map<String, WSClient> authTokenVsClientMap = new HashMap<String, WSClient>();
    
    public Map<Long, Set<Session>> getAccountVsSessionMap()
    {
    	return this.accountVsSessionMap;
    }
    
    public void setAccountVsSessionMap(Map<Long, Set<Session>> accountVsSessionMap)
    {
    	this.accountVsSessionMap = accountVsSessionMap;
    }
    
    public Map<String, Long> getSessionVsAccountMap() 
    {
		return sessionVsAccountMap;
	}
    
    public void setSessionVsAccountMap(Map<String, Long> sessionVsAccountMap) 
    {
		this.sessionVsAccountMap = sessionVsAccountMap;
	}
    
    public void addSessionToAccount(Long accountId, Session s)
    {
    	Set<Session> sessions = this.accountVsSessionMap.get(accountId);
    	if (sessions == null)
    	{
    		sessions = new HashSet<Session>();
    		accountVsSessionMap.put(accountId, sessions);
    	}
    	
    	sessions.add(s);
    	
    	this.sessionVsAccountMap.put(s.getId(), accountId);
    }
    
    public Set<Session> getSessions(Long accountId)
    {
    	return this.accountVsSessionMap.get(accountId);
    }
    
    public Long removeSession(Session s)
    {
    	Long accountId = sessionVsAccountMap.remove(s.getId());
    	Set<Session> sessionSet = accountVsSessionMap.get(accountId);
    	
    	if (sessionSet != null)
    	{
    		sessionSet.remove(s);
    	}
    	
    	return accountId;
    }
    
    public Long getAccountId(Session s)
    {
    	return sessionVsAccountMap.get(s.getId());
    }
    
    public WSAccount addAuthToken(String authToken, Account account)
    {
    	WSAccount wsAccount = new WSAccount(account);
    	this.authTokenVsAccountMap.put(authToken, wsAccount);
    	return wsAccount;
    }
    
    public WSAccount addAuthToken(String authToken, WSAccount account)
    {
    	this.authTokenVsAccountMap.put(authToken, account);
    	return account;
    }
    
    public WSAccount getAccount(String authToken)
    {
    	return this.authTokenVsAccountMap.get(authToken);
    }
    
    public WSClient addAuthToken(String authToken, WSClient client)
    {
    	this.authTokenVsClientMap.put(authToken, client);
    	return client;
    }
    
    public WSClient getClient(String authToken)
    {
    	return this.authTokenVsClientMap.get(authToken);
    }
    
}
