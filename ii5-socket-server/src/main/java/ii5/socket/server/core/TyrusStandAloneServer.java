package ii5.socket.server.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.xml.DOMConfigurator;
import org.glassfish.tyrus.server.Server;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author nyalfernandes
 *
 */
public class TyrusStandAloneServer
{
    public static void main(String[] args) throws Exception
    {
    	DOMConfigurator.configureAndWatch("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-socket-server-config.xml");
        runServer(context);
    }

    private static void runServer(ApplicationContext context) throws Exception
    {
        Server server = context.getBean("server", Server.class);
//        Server syncSocketServer = context.getBean("syncSocketServer", Server.class);

        try
        {
            server.start();
//            syncSocketServer.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please press any key to halt the server.");
            reader.readLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            // server.stop();
        }
    }
}
