package ii5.socket.server.core;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.SocketEntity;

import org.glassfish.tyrus.client.ClientManager;

public class WebSocketClientWrapperReceiver
{
    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer 0cbe6b02-cfb0-401f-b1e8-7299edd7fd22"));
        }
    }

    public static void main(String args[]) throws IOException
    {
        runClient();
    }

    private static void runClient() throws IOException
    {
        final MyClientConfigurator clientConfig = new MyClientConfigurator();

        ClientManager client = ClientManager.createClient();
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {
            session = client.connectToServer(TyrusClientEndpoint.class, cec, new URI("ws://localhost:8025/websockets/base"));

            Scanner scanner = new Scanner(System.in);
            while (true)
            {
                RequestEntity request = new RequestEntity();
                SocketEntity se = new SocketEntity();

                // sync
                /*
                 * WSSyncRequest synRequest = new WSSyncRequest();
                 * synRequest.setAckSyncIndex(5); synRequest.setMaxResults(4);
                 * request.setSync(synRequest);
                 * 
                 * se.setEndpoint("/sync");
                 * se.setRequestType(HttpRequestType.POST);
                 * se.setRequest(request); // se.setHeaders(headers);
                 * session.getAsyncRemote().sendText(Utils.getJsonString(se));
                 * 
                 * synRequest.setAckSyncIndex(5); synRequest.setMaxResults(4);
                 * Thread.sleep(7000);
                 */

                // Get ChatThreads

                // se.setEndpoint("/chat_threads");
                // se.setRequestType(HttpRequestType.GET);
                // se.setRequest(request);
                // session.getAsyncRemote().sendText(Utils.getJsonString(se));

                // get ChatMessage
                // Map<PathParamType, String> pathParams = new
                // HashMap<PathParamType, String>();
                // se.setEndpoint("/chat_threads/{chat_thread_id}");
                // se.setRequestType(HttpRequestType.GET);
                // pathParams.pu t(PathParamType.CHAT_THREAD_IDS,
                // "chat-thread-5666263985569411-5947948258647571");
                // se.setPathParams(pathParams);
                // session.getBasicRemote().sendText(Utils.getJsonString(se));

                // Delete ChatThread
                // Map<PathParamType, String> pathParams = new
                // HashMap<PathParamType, String>();
                // se.setEndpoint("/chat_threads/{chat_thread_id}");
                // se.setRequestType(HttpRequestType.DELETE);
                // pathParams.put(PathParamType.CHAT_THREAD_IDS,
                // "chat-thread-5666263985569411-5947948258647571");
                // se.setPathParams(pathParams);
                // session.getBasicRemote().sendText(Utils.getJsonString(se));

                // get Starred ChatMessage
                // Map<PathParamType, String> pathParam1 = new
                // HashMap<PathParamType, String>();
                // se.setEndpoint("/chat_threads/starred/{chat_thread_id}");
                // se.setRequestType(HttpRequestType.GET);
                // pathParam1.put(PathParamType.CHAT_THREAD_IDS,
                // "chat-thread-5666263985569411-5947948258647571");
                // se.setPathParams(pathParam1);
                // session.getBasicRemote().sendText(Utils.getJsonString(se));
                //
                // Thread.sleep(7000);

                /*
                 * Map<HttpHeaderType, String> headers = new
                 * HashMap<HttpHeaderType, String>(); WSSendChatMessage
                 * sendMessage = new WSSendChatMessage();
                 * sendMessage.setType(MessageType.TEXT);
                 * sendMessage.setText("ggggdsdgxxdfffr");
                 * sendMessage.setToAccountId("1285083943253583");
                 * sendMessage.setClientSentTime(Long.valueOf("1447902956726"));
                 * headers.put(HttpHeaderType.AUTHORISATION,
                 * "bearer e2b34b79-273d-4b0d-a3c1-6c77da2775f9");
                 * request.setChatMessage(sendMessage);
                 * se.setEndpoint("/chat_messages");
                 * se.setRequestType(HttpRequestType.POST);
                 * se.setRequest(request); se.setHeaders(headers);
                 */
                // session.getAsyncRemote().sendText("hi");
                Thread.sleep(7000);

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
    }
}
