--
-- PostgreSQL database dump
--


--
-- TOC entry 250 (class 1259 OID 60220)
-- Name: account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account (
    type character varying(31) NOT NULL,
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    address character varying(10000),
    email character varying(255),
    establishmentname character varying(10000),
    firstname character varying(255),
    initial_contacts_sync_complete boolean NOT NULL,
    lastname character varying(255),
    short_reference character varying(255),
    status character varying(255),
    workingsince date,
    name character varying(255),
    fullphoto_id character varying(255),
    primaryphonenumber_id character varying(255) NOT NULL,
    thumbnailphoto_id character varying(255)
);


ALTER TABLE account OWNER TO moveuser;

--
-- TOC entry 251 (class 1259 OID 60226)
-- Name: account_account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_account (
    account_id character varying(255) NOT NULL,
    contacts_id character varying(255) NOT NULL
);


ALTER TABLE account_account OWNER TO moveuser;

--
-- TOC entry 252 (class 1259 OID 60232)
-- Name: account_city; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_city (
    account_id character varying(255) NOT NULL,
    cities_id character varying(255) NOT NULL
);


ALTER TABLE account_city OWNER TO moveuser;

--
-- TOC entry 253 (class 1259 OID 60238)
-- Name: account_location; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_location (
    account_id character varying(255) NOT NULL,
    localities_id character varying(255) NOT NULL
);


ALTER TABLE account_location OWNER TO moveuser;

--
-- TOC entry 254 (class 1259 OID 60244)
-- Name: account_phonenumber; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_phonenumber (
    account_id character varying(255) NOT NULL,
    phonenumbers_id character varying(255) NOT NULL
);


ALTER TABLE account_phonenumber OWNER TO moveuser;

--
-- TOC entry 255 (class 1259 OID 60250)
-- Name: account_relations; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_relations (
    otheraccount_id character varying(255) NOT NULL,
    created_on timestamp without time zone,
    is_blocked boolean,
    is_blocked_by boolean,
    is_contact boolean,
    is_contact_of boolean,
    score integer,
    scored_as integer,
    account_id character varying(255) NOT NULL
);


ALTER TABLE account_relations OWNER TO moveuser;

--
-- TOC entry 246 (class 1259 OID 59842)
-- Name: accountrelations; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE accountrelations (
    otheraccount_id character varying(255) NOT NULL,
    created_on timestamp without time zone,
    is_blocked boolean,
    is_blocked_by boolean,
    is_contact boolean,
    is_contact_of boolean,
    score integer,
    scored_as integer,
    account_id character varying(255) NOT NULL
);


ALTER TABLE accountrelations OWNER TO moveuser;

--
-- TOC entry 256 (class 1259 OID 60256)
-- Name: amenity; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE amenity (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE amenity OWNER TO moveuser;

--
-- TOC entry 257 (class 1259 OID 60262)
-- Name: association; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE association (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    approvalstatus integer,
    name character varying(255) NOT NULL,
    status character varying(255),
    phonenumber_id character varying(255)
);


ALTER TABLE association OWNER TO moveuser;

--
-- TOC entry 258 (class 1259 OID 60268)
-- Name: association_account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE association_account (
    associations_id character varying(255) NOT NULL,
    agents_id character varying(255) NOT NULL
);


ALTER TABLE association_account OWNER TO moveuser;

--
-- TOC entry 259 (class 1259 OID 60274)
-- Name: bookmark; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE bookmark (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    reference_id character varying(255),
    target_entity character varying(255),
    type character varying(255)
);


ALTER TABLE bookmark OWNER TO moveuser;

--
-- TOC entry 260 (class 1259 OID 60280)
-- Name: city; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE city (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE city OWNER TO moveuser;

--
-- TOC entry 261 (class 1259 OID 60286)
-- Name: client; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE client (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    appname character varying(255),
    appversion character varying(255),
    development character varying(255),
    devicemodel character varying(255),
    deviceplatform character varying(255),
    devicetoken character varying(255),
    deviceversion character varying(255),
    isonline boolean,
    pushalert character varying(255),
    pushbadge character varying(255),
    pushchannel character varying(255),
    pushsound character varying(255),
    status character varying(255),
    account_id character varying(255),
    lastactivitytime timestamp without time zone
);


ALTER TABLE client OWNER TO moveuser;

--
-- TOC entry 262 (class 1259 OID 60292)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: moveuser
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO moveuser;

--
-- TOC entry 263 (class 1259 OID 60294)
-- Name: invitation; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE invitation (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    fromaccount bytea NOT NULL,
    status character varying(255),
    type character varying(255),
    toaccount_id character varying(255)
);


ALTER TABLE invitation OWNER TO moveuser;

--
-- TOC entry 244 (class 1259 OID 59046)
-- Name: location; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location (
    id character varying(255) NOT NULL,
    geopoint geometry,
    name character varying(255) NOT NULL,
    parent_id character varying(255),
    status integer,
    type character varying(255),
    zip_code character varying(255),
    city_id character varying(255)
);


ALTER TABLE location OWNER TO moveuser;

--
-- TOC entry 264 (class 1259 OID 60300)
-- Name: location_match; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location_match (
    location_id character varying(255) NOT NULL,
    match_id character varying(255) NOT NULL,
    is_exact boolean DEFAULT false NOT NULL
);


ALTER TABLE location_match OWNER TO moveuser;

--
-- TOC entry 265 (class 1259 OID 60307)
-- Name: media; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE media (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    dimension character varying(255),
    height integer,
    name character varying(255),
    size integer,
    type character varying(255),
    url character varying(255),
    width integer
);


ALTER TABLE media OWNER TO moveuser;

--
-- TOC entry 266 (class 1259 OID 60313)
-- Name: native_contacts; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE native_contacts (
    phone_number character varying(255) NOT NULL,
    created_on timestamp without time zone,
    first_name character varying(4000),
    last_name character varying(4000),
    account_id character varying(255) NOT NULL
);


ALTER TABLE native_contacts OWNER TO moveuser;

--
-- TOC entry 247 (class 1259 OID 59904)
-- Name: nativecontacts; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE nativecontacts (
    phone_number character varying(255) NOT NULL,
    created_on timestamp without time zone,
    first_name character varying(255),
    last_name character varying(255),
    account_id character varying(255) NOT NULL
);


ALTER TABLE nativecontacts OWNER TO moveuser;

--
-- TOC entry 245 (class 1259 OID 59126)
-- Name: nearby_localities; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE nearby_localities (
    id character varying(255) NOT NULL,
    status character varying(255),
    location_id character varying(255),
    nearby_location character varying(255)
);


ALTER TABLE nearby_localities OWNER TO moveuser;

--
-- TOC entry 267 (class 1259 OID 60319)
-- Name: oauth_access_token; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE oauth_access_token (
    token_id character varying(255) NOT NULL,
    authentication bytea,
    authentication_id character varying(255),
    refresh_token character varying(255),
    token bytea,
    user_name character varying(255),
    client_id character varying(255) NOT NULL
);


ALTER TABLE oauth_access_token OWNER TO moveuser;

--
-- TOC entry 268 (class 1259 OID 60325)
-- Name: oauth_authorization; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE oauth_authorization (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    access_token_validity integer,
    additional_information character varying(255),
    authorities character varying(255),
    authorized_grant_types character varying(255),
    client_secret character varying(255),
    resource_ids character varying(255),
    scope character varying(255),
    client_id character varying(255) NOT NULL
);


ALTER TABLE oauth_authorization OWNER TO moveuser;

--
-- TOC entry 269 (class 1259 OID 60331)
-- Name: otp; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE otp (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    expirytime timestamp without time zone,
    otp character varying(255) NOT NULL,
    client_id character varying(255),
    phonenumber_id character varying(255) NOT NULL
);


ALTER TABLE otp OWNER TO moveuser;

--
-- TOC entry 270 (class 1259 OID 60337)
-- Name: person; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE person (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE person OWNER TO moveuser;

--
-- TOC entry 271 (class 1259 OID 60343)
-- Name: phonenumber; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE phonenumber (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    number character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE phonenumber OWNER TO moveuser;

--
-- TOC entry 272 (class 1259 OID 60349)
-- Name: project; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE project (
    id character varying(255) NOT NULL,
    is_user_suggested boolean,
    name character varying(255)
);


ALTER TABLE project OWNER TO moveuser;

--
-- TOC entry 273 (class 1259 OID 60355)
-- Name: property_listing; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE property_listing (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    area integer,
    commission_type character varying(255),
    custom_sublocation character varying(255),
    description character varying(4000),
    external_source character varying(255),
    is_hot boolean,
    last_renewed_time timestamp without time zone,
    property_market character varying(255),
    rent_price bigint NOT NULL,
    rooms character varying(255),
    sale_price bigint NOT NULL,
    short_reference character varying(24),
    status character varying(255),
    sub_type character varying(255),
    type character varying(255),
    account_id character varying(255),
    location_id character varying(255)
);


ALTER TABLE property_listing OWNER TO moveuser;

--
-- TOC entry 274 (class 1259 OID 60361)
-- Name: property_listing_amenity; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE property_listing_amenity (
    property_listing_id character varying(255) NOT NULL,
    amenities_id character varying(255) NOT NULL
);


ALTER TABLE property_listing_amenity OWNER TO moveuser;

--
-- TOC entry 275 (class 1259 OID 60367)
-- Name: property_search_relation; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE property_search_relation (
    search_id character varying(255) NOT NULL,
    property_listing_id character varying(255) NOT NULL,
    account_id character varying(255) NOT NULL
);


ALTER TABLE property_search_relation OWNER TO moveuser;

--
-- TOC entry 248 (class 1259 OID 59952)
-- Name: propertylisting; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE propertylisting (
    id character varying(255) NOT NULL,
    createdby character varying(255) NOT NULL,
    createdtime timestamp without time zone NOT NULL,
    updatedby character varying(255),
    updatedtime timestamp without time zone,
    area integer,
    commission_type character varying(255),
    custom_sublocation character varying(255),
    description character varying(4000),
    external_source character varying(255),
    is_hot boolean,
    last_renewed_time timestamp without time zone,
    property_market character varying(255),
    rent_price bigint NOT NULL,
    rooms character varying(255),
    sell_price bigint NOT NULL,
    short_reference character varying(24),
    status character varying(255),
    sub_type character varying(255),
    type character varying(255),
    account_id character varying(255),
    location_id character varying(255)
);


ALTER TABLE propertylisting OWNER TO moveuser;

--
-- TOC entry 249 (class 1259 OID 59958)
-- Name: propertylisting_amenity; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE propertylisting_amenity (
    propertylisting_id character varying(255) NOT NULL,
    amenities_id character varying(255) NOT NULL
);


ALTER TABLE propertylisting_amenity OWNER TO moveuser;

--
-- TOC entry 276 (class 1259 OID 60373)
-- Name: recent_search; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE recent_search (
    id character varying(255) NOT NULL,
    created_by character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    updated_by character varying(255),
    updated_time timestamp without time zone,
    amenities character varying(255),
    location_id character varying(255) NOT NULL,
    max_area integer,
    max_rent_price bigint,
    max_sell_price bigint,
    min_area integer,
    min_rent_price bigint,
    min_sell_price bigint,
    property_market character varying(255) NOT NULL,
    property_sub_type character varying(255),
    property_type character varying(255) NOT NULL,
    rooms character varying(255),
    search_time bigint NOT NULL,
    transaction_type character varying(255) NOT NULL,
    account_id character varying(255)
);


ALTER TABLE recent_search OWNER TO moveuser;

--
-- TOC entry 277 (class 1259 OID 60379)
-- Name: search; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE search (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    amenities character varying(255),
    client_name character varying(255),
    location_id character varying(255) NOT NULL,
    max_area integer,
    max_rent_price bigint,
    max_sale_price bigint,
    min_area integer,
    min_rent_price bigint,
    min_sale_price bigint,
    property_market character varying(255) NOT NULL,
    property_sub_type character varying(255),
    property_type character varying(255) NOT NULL,
    rooms character varying(255),
    search_time bigint NOT NULL,
    transaction_type character varying(255) NOT NULL,
    account_id character varying(255)
);


ALTER TABLE search OWNER TO moveuser;

--
-- TOC entry 278 (class 1259 OID 60385)
-- Name: sync_update; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update (
    update_index integer NOT NULL,
    account_id character varying(255) NOT NULL,
    update character varying(10000) NOT NULL
);


ALTER TABLE sync_update OWNER TO moveuser;

--
-- TOC entry 279 (class 1259 OID 60391)
-- Name: sync_update_read_cursors; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update_read_cursors (
    client_id character varying(255) NOT NULL,
    max_acked_update integer NOT NULL
);


ALTER TABLE sync_update_read_cursors OWNER TO moveuser;

--
-- TOC entry 280 (class 1259 OID 60394)
-- Name: sync_update_write_cursors; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update_write_cursors (
    account_id character varying(255) NOT NULL,
    total_updates integer NOT NULL
);


ALTER TABLE sync_update_write_cursors OWNER TO moveuser;

--
-- TOC entry 281 (class 1259 OID 60397)
-- Name: trustscore; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE trustscore (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    fromaccount bytea NOT NULL,
    message character varying(255),
    toaccount_id character varying(255)
);


ALTER TABLE trustscore OWNER TO moveuser;

--
-- TOC entry 3759 (class 2606 OID 60404)
-- Name: account_account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_account
    ADD CONSTRAINT account_account_pkey PRIMARY KEY (account_id, contacts_id);


--
-- TOC entry 3761 (class 2606 OID 60406)
-- Name: account_city_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_city
    ADD CONSTRAINT account_city_pkey PRIMARY KEY (account_id, cities_id);


--
-- TOC entry 3763 (class 2606 OID 60408)
-- Name: account_location_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_location
    ADD CONSTRAINT account_location_pkey PRIMARY KEY (account_id, localities_id);


--
-- TOC entry 3765 (class 2606 OID 60410)
-- Name: account_phonenumber_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_phonenumber
    ADD CONSTRAINT account_phonenumber_pkey PRIMARY KEY (account_id, phonenumbers_id);


--
-- TOC entry 3757 (class 2606 OID 60412)
-- Name: account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- TOC entry 3767 (class 2606 OID 60414)
-- Name: account_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_relations
    ADD CONSTRAINT account_relations_pkey PRIMARY KEY (otheraccount_id, account_id);


--
-- TOC entry 3749 (class 2606 OID 59997)
-- Name: accountrelations_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY accountrelations
    ADD CONSTRAINT accountrelations_pkey PRIMARY KEY (otheraccount_id, account_id);


--
-- TOC entry 3769 (class 2606 OID 60416)
-- Name: amenity_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY amenity
    ADD CONSTRAINT amenity_pkey PRIMARY KEY (id);


--
-- TOC entry 3773 (class 2606 OID 60418)
-- Name: association_account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY association_account
    ADD CONSTRAINT association_account_pkey PRIMARY KEY (associations_id, agents_id);


--
-- TOC entry 3771 (class 2606 OID 60420)
-- Name: association_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY association
    ADD CONSTRAINT association_pkey PRIMARY KEY (id);


--
-- TOC entry 3775 (class 2606 OID 60422)
-- Name: bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY bookmark
    ADD CONSTRAINT bookmark_pkey PRIMARY KEY (id);


--
-- TOC entry 3777 (class 2606 OID 60424)
-- Name: city_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- TOC entry 3779 (class 2606 OID 60426)
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 3781 (class 2606 OID 60428)
-- Name: invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY invitation
    ADD CONSTRAINT invitation_pkey PRIMARY KEY (id);


--
-- TOC entry 3783 (class 2606 OID 60430)
-- Name: location_match_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY location_match
    ADD CONSTRAINT location_match_pkey PRIMARY KEY (location_id, match_id);


--
-- TOC entry 3745 (class 2606 OID 59053)
-- Name: location_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- TOC entry 3785 (class 2606 OID 60432)
-- Name: media_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- TOC entry 3787 (class 2606 OID 60434)
-- Name: native_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY native_contacts
    ADD CONSTRAINT native_contacts_pkey PRIMARY KEY (phone_number, account_id);


--
-- TOC entry 3751 (class 2606 OID 60017)
-- Name: nativecontacts_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY nativecontacts
    ADD CONSTRAINT nativecontacts_pkey PRIMARY KEY (phone_number, account_id);


--
-- TOC entry 3747 (class 2606 OID 59133)
-- Name: nearby_localities_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT nearby_localities_pkey PRIMARY KEY (id);


--
-- TOC entry 3789 (class 2606 OID 60436)
-- Name: oauth_access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (token_id, client_id);


--
-- TOC entry 3755 (class 2606 OID 60033)
-- Name: propertylisting_amenity_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY propertylisting_amenity
    ADD CONSTRAINT propertylisting_amenity_pkey PRIMARY KEY (propertylisting_id, amenities_id);


--
-- TOC entry 3753 (class 2606 OID 60035)
-- Name: propertylisting_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY propertylisting
    ADD CONSTRAINT propertylisting_pkey PRIMARY KEY (id);


--
-- TOC entry 3790 (class 2606 OID 59336)
-- Name: fk_blu34gxuh8y1aou2c4h3etcdk; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT fk_blu34gxuh8y1aou2c4h3etcdk FOREIGN KEY (location_id) REFERENCES location(id);


--
-- TOC entry 3791 (class 2606 OID 59341)
-- Name: fk_lrhw517q5u4gprn0y0t0jm2dh; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT fk_lrhw517q5u4gprn0y0t0jm2dh FOREIGN KEY (nearby_location) REFERENCES location(id);


--
-- TOC entry 3792 (class 2606 OID 60212)
-- Name: fk_uncqdshqwuovmsvyla9om2q5; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY propertylisting_amenity
    ADD CONSTRAINT fk_uncqdshqwuovmsvyla9om2q5 FOREIGN KEY (propertylisting_id) REFERENCES propertylisting(id);


--
-- TOC entry 3915 (class 0 OID 0)
-- Dependencies: 9
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-08-17 11:43:48

--
-- PostgreSQL database dump complete
--

