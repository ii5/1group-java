--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: amenity; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO amenity VALUES ('1', '2015-02-06 10:12:15', 'garden', 'garden');
INSERT INTO amenity VALUES ('2', '2015-02-06 10:12:15', 'parking', 'parking');
INSERT INTO amenity VALUES ('3', '2015-02-06 10:12:15', 'pool', 'pool');
INSERT INTO amenity VALUES ('4', '2015-02-06 10:12:15', 'intercom', 'intercom');
INSERT INTO amenity VALUES ('5', '2015-02-06 10:12:15', 'security', 'security');
INSERT INTO amenity VALUES ('6', '2015-02-06 10:12:15', 'clubhouse', 'clubhouse');
INSERT INTO amenity VALUES ('7', '2015-02-06 10:12:15', 'gym', 'gym');
INSERT INTO amenity VALUES ('8', '2015-02-06 10:12:15', 'lift', 'lift');
INSERT INTO amenity VALUES ('9', '2015-02-06 10:12:15', 'play_area', 'play_area');
INSERT INTO amenity VALUES ('10', '2015-02-06 10:12:15', 'maintenance_staff', 'maintenance_staff');
INSERT INTO amenity VALUES ('11', '2015-02-06 10:12:15', 'rain_harvesting', 'rain_harvesting');


--
-- PostgreSQL database dump complete
--

