CREATE INDEX property_listing_short_reference_index ON property_listing USING btree (short_reference);
CREATE INDEX property_listing_status_index ON property_listing USING btree (status);

-- November 3 2015 : Nyal : Changes in Search table to store meta data required for report generation.
ALTER TABLE Search ADD COLUMN em_count integer;
ALTER TABLE Search ADD COLUMN em_max_relevance_score numeric;
ALTER TABLE Search ADD COLUMN em_mean_score numeric;

ALTER TABLE Search ADD COLUMN nm_count integer;
ALTER TABLE Search ADD COLUMN nm_max_relevance_score numeric;
ALTER TABLE Search ADD COLUMN nm_mean_score numeric;

--November 10 2015: Saurabh : Changes to native_contacts table to accomodate full_name and company name as large as 4000 chars

ALTER TABLE native_contacts ALTER COLUMN full_name TYPE character varying(4000); 
ALTER TABLE native_contacts ALTER COLUMN company_name TYPE character varying(4000); 

--November 17 2015: Sanil : Adding marketing list table

CREATE TABLE marketing_list (
    phone_number character varying(255) NOT NULL,
    data_source character varying(255) NOT NULL
);

ALTER TABLE ONLY marketing_list
    ADD CONSTRAINT marketing_list_pkey PRIMARY KEY (phone_number);

-- November 24, 2015: Mitesh Chavda: Added sms marketing log 

CREATE TABLE sms_marketing_log (
   id character varying(255) NOT NULL,
   phone_number character varying(255),
   message character varying(1000),
   campaign character varying(255),
   sent_time timestamp without time zone
);

ALTER TABLE ONLY sms_marketing_log    ADD CONSTRAINT id PRIMARY KEY (id);

-- November 24, 2015: Ashish Thorat: chat log

CREATE TABLE chat_log (
   chat_thread_id character varying(255) NOT NULL,
   index bigint NOT NULL,
   from_account_id character varying(255) NOT NULL,
   to_account_id character varying(255) NOT NULL,
   client_sent_time timestamp without time zone NOT NULL,
   server_time timestamp without time zone NOT NULL,
   message_type character varying(255) NOT NULL,
   text character varying(255),
   property_listing_id character varying(255),
   photo_full_location character varying(255)
);

ALTER TABLE ONLY chat_log
   ADD CONSTRAINT chat_log_pkey PRIMARY KEY (chat_thread_id, index);

-- December 09, 2015: Mitesh Chavda

ALTER TABLE marketing_list ADD COLUMN wa_group_count integer default '0';

-- December 10, 2015: Sanil Shet

CREATE TABLE invite_log (
   id character varying(255) NOT NULL,
   from_account_id character varying(255) NOT NULL,
   to_phone_number character varying(255) NOT NULL,
   sent_time timestamp without time zone NOT NULL
);

ALTER TABLE ONLY invite_log
   ADD CONSTRAINT invite_log_pkey PRIMARY KEY (id);

--December 15, 2015 : Ashish Thorat 
ALTER TABLE chat_log ALTER COLUMN text TYPE character varying(4000);

-- January 11 2016 : API 76 
ALTER TABLE property_listing ADD COLUMN description_length integer NOT NULL DEFAULT '0';

UPDATE property_Listing set description_length = 0 where description_length IS NULL;
UPDATE property_listing SET description_length = LENGTH(description) where description is not null;

-- January 11 2016: API-69
ALTER TABLE account ADD COLUMN registration_time timestamp without time zone DEFAULT NULL;

-- January 12 2016: API 76
CREATE INDEX "index_account_idx" ON native_contacts USING btree (account_id);

--May-20 2016  API-126
alter table account_location rename column localities_id to locations_id


-- November-25-2016 V2-710
CREATE TABLE `search_notification` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `is_send` tinyint(1) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `search_notification`
ADD PRIMARY KEY (`id`)

ALTER TABLE `search_notification`
ADD PRIMARY KEY (`user_id`)

ALTER TABLE `search_notification`
ADD PRIMARY KEY (`is_send`)
​
