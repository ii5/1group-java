#!/bin/sh
psqluser="moveuser"   # Database username
psqlpass="movethrough"  # Database password
psqldb="movein"   # Database name
host="10.50.249.80:5432"


if [ -z $2 ];
	then
	echo "Data path dierctory not set";
	exit;
fi;

if [ "$1" = "newsetup" ]
then
   echo "Running postgis extension"
   psql -U $psqluser -d $psqldb -f $2"/postgis-extensions.sql" -h $host
   echo "Potsgis extensions created"

   echo "Running Store procedures and custom functions"
   psql -U $psqluser -d $psqldb -f $2"/functions.sql" -h $host
   echo "Done- Store procedures and custom functions"

   echo "Creating movein schema"
   psql -U $psqluser -d $psqldb -f $2"/movein-structure.sql" -h $host
   echo "Done  creating schema"

  echo "importing location admin table"
  psql -U $psqluser -d $psqldb -f $2"/location_admin.sql" -h $host
  echo "Done importing location admin"

  echo "importing location match admin table"
  psql -U $psqluser -d $psqldb -f $2"/location_match_admin.sql" -h $host
  echo "Done importing location match admin"

  echo "importing nearby location  admin table"
  psql -U $psqluser -d $psqldb -f $2"/nearby_localities_admin.sql" -h $host
  echo "Done importing location nearby"  
  
  echo "Importing parent location table"
  psql -U $psqluser -d $psqldb -f $2"/parent_location_admin.sql" -h $host
  echo "Done importing parent location admin"

  echo "importing cities"
  psql -U $psqluser -d $psqldb -f $2"/city.sql" -h $host
  echo "Done cities import"

  echo "importing amenities"
  psql -U $psqluser -d $psqldb -f $2"/amenitydata.sql" -h $host
  echo "Done amenities import"
  exit;
else
 echo "Alter statements started"
 psql -U $psqluser -d $psqldb -f $2"/alters.sql" -h $host
 echo "Alter statements ended"
fi;



