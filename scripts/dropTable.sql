--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-08-17 11:44:54

ALTER TABLE ONLY public.propertylisting_amenity DROP CONSTRAINT fk_uncqdshqwuovmsvyla9om2q5;
ALTER TABLE ONLY public.nearby_localities DROP CONSTRAINT fk_lrhw517q5u4gprn0y0t0jm2dh;
ALTER TABLE ONLY public.nearby_localities DROP CONSTRAINT fk_blu34gxuh8y1aou2c4h3etcdk;
ALTER TABLE ONLY public.propertylisting DROP CONSTRAINT propertylisting_pkey;
ALTER TABLE ONLY public.propertylisting_amenity DROP CONSTRAINT propertylisting_amenity_pkey;
ALTER TABLE ONLY public.oauth_access_token DROP CONSTRAINT oauth_access_token_pkey;
ALTER TABLE ONLY public.nearby_localities DROP CONSTRAINT nearby_localities_pkey;
ALTER TABLE ONLY public.nativecontacts DROP CONSTRAINT nativecontacts_pkey;
ALTER TABLE ONLY public.native_contacts DROP CONSTRAINT native_contacts_pkey;
ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
ALTER TABLE ONLY public.location DROP CONSTRAINT location_pkey;
ALTER TABLE ONLY public.location_match DROP CONSTRAINT location_match_pkey;
ALTER TABLE ONLY public.invitation DROP CONSTRAINT invitation_pkey;
ALTER TABLE ONLY public.client DROP CONSTRAINT client_pkey;
ALTER TABLE ONLY public.city DROP CONSTRAINT city_pkey;
ALTER TABLE ONLY public.bookmark DROP CONSTRAINT bookmark_pkey;
ALTER TABLE ONLY public.association DROP CONSTRAINT association_pkey;
ALTER TABLE ONLY public.association_account DROP CONSTRAINT association_account_pkey;
ALTER TABLE ONLY public.amenity DROP CONSTRAINT amenity_pkey;
ALTER TABLE ONLY public.accountrelations DROP CONSTRAINT accountrelations_pkey;
ALTER TABLE ONLY public.account_relations DROP CONSTRAINT account_relations_pkey;
ALTER TABLE ONLY public.account DROP CONSTRAINT account_pkey;
ALTER TABLE ONLY public.account_phonenumber DROP CONSTRAINT account_phonenumber_pkey;
ALTER TABLE ONLY public.account_location DROP CONSTRAINT account_location_pkey;
ALTER TABLE ONLY public.account_city DROP CONSTRAINT account_city_pkey;
ALTER TABLE ONLY public.account_account DROP CONSTRAINT account_account_pkey;
DROP TABLE public.trustscore;
DROP TABLE public.sync_update_write_cursors;
DROP TABLE public.sync_update_read_cursors;
DROP TABLE public.sync_update;
DROP TABLE public.search;
DROP TABLE public.recent_search;
DROP TABLE public.propertylisting_amenity;
DROP TABLE public.propertylisting;
DROP TABLE public.property_search_relation;
DROP TABLE public.property_listing_amenity;
DROP TABLE public.property_listing;
DROP TABLE public.project;
DROP TABLE public.phonenumber;
DROP TABLE public.person;
DROP TABLE public.otp;
DROP TABLE public.oauth_authorization;
DROP TABLE public.oauth_access_token;
DROP TABLE public.nearby_localities;
DROP TABLE public.nativecontacts;
DROP TABLE public.native_contacts;
DROP TABLE public.media;
DROP TABLE public.location_match;
DROP TABLE public.location;
DROP TABLE public.invitation;
DROP SEQUENCE public.hibernate_sequence;
DROP TABLE public.client;
DROP TABLE public.city;
DROP TABLE public.bookmark;
DROP TABLE public.association_account;
DROP TABLE public.association;
DROP TABLE public.amenity;
DROP TABLE public.accountrelations;
DROP TABLE public.account_relations;
DROP TABLE public.account_phonenumber;
DROP TABLE public.account_location;
DROP TABLE public.account_city;
DROP TABLE public.account_account;
DROP TABLE public.account;