CREATE TABLE APPLICATION_LOCK 
(
    lock_type character varying(50) NOT NULL 
);

ALTER TABLE APPLICATION_LOCK ADD UNIQUE(lock_type);


INSERT INTO application_lock (lock_type) values('SYNC_UPDATE');
INSERT INTO application_lock (lock_type) values('SYNC_UPDATE_READ_CURSORS');
INSERT INTO application_lock (lock_type) values('SYNC_UPDATE_WRITE_CURSORS');


CREATE OR REPLACE FUNCTION create_client(clientid text)
  RETURNS integer AS
$BODY$
DECLARE
accountId  character varying(255);
totalupdates integer;
checkExistingClient character varying(255);
BEGIN
	checkExistingClient:= (SELECT readcursor.client_id FROM sync_update_read_cursors readcursor where readcursor.client_id = clientId);
	accountId:= (SELECT c.account_id FROM client c where c.id = clientId);

	IF (accountId is NULL) then
		RETURN -3;
	END IF;

	IF (checkExistingClient is NOT NULL) then
		RETURN -2;
	END IF;

	totalupdates:= (SELECT s.total_updates FROM sync_update_write_cursors s where s.account_id = accountId FOR UPDATE);

	if(totalUpdates is null) then
		totalupdates = -1;        
	END IF;

	INSERT INTO sync_update_read_cursors (client_id,max_acked_update) VALUES (clientId,totalupdates);
	RETURN totalupdates;

END;
$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION append_update(
    accountid text,
    syncupdate text)
  RETURNS integer AS
$BODY$
DECLARE
totalUpdates INTEGER;
syncUpdateJson json;
lock_type text;
BEGIN
	lock_type:= (SELECT a.lock_type FROM application_lock a WHERE a.lock_type = 'SYNC_UPDATE_WRITE_CURSORS' FOR UPDATE);
	
	totalUpdates:= (SELECT s.total_updates FROM sync_update_write_cursors s WHERE s.account_id = accountid FOR UPDATE);

	IF(totalUpdates IS NOT NULL) THEN
	    totalUpdates:= totalUpdates + 1;
	    UPDATE sync_update_write_cursors SET total_updates = totalUpdates WHERE account_id = accountid;
	ELSE
	    totalUpdates:= 0;
	    INSERT INTO sync_update_write_cursors (account_id,total_updates) VALUES (accountid,totalUpdates);
	END IF;
	
	syncUpdate:= (SELECT json_object_set_key(syncUpdate::json,'index',totalUpdates));
	INSERT INTO sync_update (update_index,account_id, update) VALUES (totalUpdates,accountid,syncUpdate);

	RETURN totalUpdates;

END;
$BODY$
  LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION advance_read_cursor(
    clientid text,
    accountid text,
    requestedcursorindex integer)
  RETURNS integer AS
$BODY$
DECLARE
	maxAckedUpdateOfClient INTEGER;
	totalUpdatesToAccount INTEGER;
BEGIN
	maxAckedUpdateOfClient:= (SELECT max_acked_update FROM sync_update_read_cursors WHERE client_id = clientId FOR UPDATE);
	totalUpdatesToAccount:= (SELECT total_updates FROM sync_update_write_cursors WHERE account_id = accountId FOR UPDATE);
	IF (maxAckedUpdateOfClient IS NULL OR totalUpdatesToAccount IS NULL) then
		RETURN -2;
	END IF;
	
	maxAckedUpdateOfClient:= LEAST(GREATEST(maxAckedUpdateOfClient,requestedCursorIndex),totalUpdatesToAccount);

	-- IF(maxAckedUpdateOfClient <> requestedCursorIndex) then
		UPDATE sync_update_read_cursors SET max_acked_update = maxAckedUpdateOfClient WHERE client_id = clientId;
	-- END IF;
	
	RETURN maxAckedUpdateOfClient;
END;
$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION json_object_set_key(
    json json,
    key_to_set text,
    value_to_set anyelement)
  RETURNS json AS
$BODY$
SELECT concat('{', string_agg(to_json("key") || ':' || "value", ','), '}')::json
 FROM (SELECT *
         FROM json_each("json")
        WHERE "key" <> "key_to_set"
        UNION ALL
       SELECT "key_to_set", to_json("value_to_set")) AS "fields"
$BODY$
  LANGUAGE sql;


CREATE OR replace function update_nearby_locations() RETURNS void as $$
BEGIN
SET session_replication_role = replica;
ALTER TABLE nearby_localities DISABLE TRIGGER USER;
delete from nearby_localities;
INSERT INTO nearby_localities(id,status,location_id,nearby_location) ( select id,case status when '1' then 'ACTIVE' else 'INACTIVE' end as status , location_id,nearby_location from nearby_localities_admin);
ALTER TABLE nearby_localities ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql;


CREATE OR replace function update_location_match() RETURNS void as $$
BEGIN
SET session_replication_role = replica;
ALTER TABLE location_match DISABLE TRIGGER USER;
delete from location_match;
INSERT INTO location_match(is_exact,match_id,location_id) ( select is_exact,match_id,location_id from location_match_admin);
ALTER TABLE location_match ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql;


CREATE OR replace function update_locations() RETURNS void as $$
BEGIN;
SET session_replication_role = replica;
ALTER TABLE location DISABLE TRIGGER USER;
delete from location;
INSERT INTO location (id,geopoint,name,parent_id,status,type,zip_code,city_id,location_display_name) ( select id,geopoint,name,parent_id,status,case type when 'locality' then 'LOCALITY' when 'sublocality' then 'SUB_LOCALITY' when 'project' then 'PROJECT' when 'road' then 'ROAD' end,zip_code,city_id,location_display_name from location_admin);
ALTER TABLE location ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql; 

--function to update live location table city wise
CREATE OR replace function update_city_wise_locations(ids TEXT) RETURNS void as $$
DECLARE
       ids integer[];
BEGIN
ids = string_to_array($1,',');
SET session_replication_role = replica;
ALTER TABLE location DISABLE TRIGGER USER;
delete from location where city_id::int = ANY(ids);
INSERT INTO location (id,geopoint,name,parent_id,status,type,zip_code,city_id,location_display_name) 
( select id,geopoint,name,parent_id,status,case type when 'locality' then 'LOCALITY' when 'sublocality' then 'SUB_LOCALITY' when 'project' then 'PROJECT' when 'road' then 'ROAD' end,zip_code,city_id,location_display_name from location_admin where city_id::int = ANY(ids));
ALTER TABLE location ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql; 


CREATE OR replace function update_location_match_city_wise(ids TEXT) RETURNS void as $$
DECLARE
       ids integer[];
BEGIN
ids = string_to_array($1,',');
SET session_replication_role = replica;
ALTER TABLE location_match DISABLE TRIGGER USER;
delete from location_match where city_id::int = ANY(ids);
INSERT INTO location_match(is_exact,match_id,location_id) ( select is_exact,match_id,location_id from location_match_admin  where city_id::int = ANY(ids));
ALTER TABLE location_match ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql;

CREATE OR replace function update_nearby_locations_city_wise(ids TEXT) RETURNS void as $$
DECLARE
       ids integer[];
BEGIN
ids = string_to_array($1,',');
SET session_replication_role = replica;
ALTER TABLE nearby_localities DISABLE TRIGGER USER;
delete from nearby_localities where city_id::int = ANY(ids);
INSERT INTO nearby_localities(id,status,location_id,nearby_location, city_id) ( select id,case status when '1' then 'ACTIVE' else 'INACTIVE' end as status , location_id,nearby_location,city_id from nearby_localities_admin  where city_id::int = ANY(ids));
ALTER TABLE nearby_localities ENABLE TRIGGER USER;
SET session_replication_role = DEFAULT;
END;
$$ LANGUAGE plpgsql;
