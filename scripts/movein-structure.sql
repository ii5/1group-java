

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account (
    type character varying(31) NOT NULL,
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    address character varying(10000),
    email character varying(255),
    establishmentname character varying(10000),
    fullname character varying(255),
    initial_contacts_sync_complete boolean NOT NULL,
    companyname character varying(255),
    short_reference character varying(255),
    status character varying(255),
    workingsince date,
    name character varying(255),
    fullphoto_id character varying(255),
    primaryphonenumber_id character varying(255) NOT NULL,
    thumbnailphoto_id character varying(255)
);


--
-- Name: account_account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_account (
    account_id character varying(255) NOT NULL,
    contacts_id character varying(255) NOT NULL
);

--
-- Name: account_city; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_city (
    account_id character varying(255) NOT NULL,
    cities_id character varying(255) NOT NULL
);




--
-- Name: account_location; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_location (
    account_id character varying(255) NOT NULL,
    localities_id character varying(255) NOT NULL
);

--
-- Name: account_phonenumber; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_phonenumber (
    account_id character varying(255) NOT NULL,
    phonenumbers_id character varying(255) NOT NULL
);

--
-- Name: account_relations; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE account_relations (
    otheraccount_id character varying(255) NOT NULL,
    created_on timestamp without time zone,
    is_blocked boolean,
    is_blocked_by boolean,
    is_contact boolean,
    is_contact_of boolean,
    score integer,
    scored_as integer,
    account_id character varying(255) NOT NULL
);

--
-- Name: amenity; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE amenity (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);

--
-- Name: application_lock; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE application_lock (
    lock_type character varying(50) NOT NULL
);



--
-- Name: association; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE association (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    approvalstatus integer,
    name character varying(255) NOT NULL,
    status character varying(255),
    phonenumber_id character varying(255)
);




--
-- Name: association_account; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE association_account (
    associations_id character varying(255) NOT NULL,
    agents_id character varying(255) NOT NULL
);


--
-- Name: bookmark; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE bookmark (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    reference_id character varying(255),
    target_entity character varying(255),
    type character varying(255)
);

--
-- Name: COLUMN bookmark.reference_id; Type: COMMENT; Schema: public; Owner: moveuser
--

COMMENT ON COLUMN bookmark.reference_id IS 'This is used to store account_id';


--
-- Name: COLUMN bookmark.target_entity; Type: COMMENT; Schema: public; Owner: moveuser
--

COMMENT ON COLUMN bookmark.target_entity IS 'property listing id is stored here';


--
-- Name: city; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE city (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);

--
-- Name: client; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE client (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    appname character varying(255),
    appversion character varying(255),
    development character varying(255),
    devicemodel character varying(255),
    deviceplatform character varying(255),
    devicetoken character varying(255),
    deviceversion character varying(255),
    isonline boolean,
    lastactivitytime timestamp without time zone,
    pushalert character varying(255),
    pushbadge character varying(255),
    pushchannel character varying(255),
    pushsound character varying(255),
    status character varying(255),
    account_id character varying(255),
	cps_id character varying(500)
);

--
-- Name: client_analytics; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE client_analytics (
    id integer NOT NULL,
    account_id character varying(255),
    action character varying(255),
    client_id character varying(255),
    client_event_time timestamp without time zone,
    context character varying(255),
    created_time timestamp without time zone NOT NULL,
    entity_id character varying(255),
    entity_type character varying(255)
);

--
-- Name: configuration; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE configuration (
    id character varying NOT NULL,
    key character varying(255) NOT NULL,
    value text NOT NULL,
    created_time timestamp without time zone NOT NULL
);

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: moveuser
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- Name: invitation; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE invitation (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    fromaccount bytea NOT NULL,
    status character varying(255),
    type character varying(255),
    toaccount_id character varying(255)
);

--
-- Name: location; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location (
    id character varying(255) NOT NULL,
    geopoint geometry,
    name character varying(255) NOT NULL,
    parent_id character varying(255),
    status integer,
    type character varying(255),
    zip_code character varying(255),
    city_id character varying(255),
    location_display_name character varying(255)
);

--
-- Name: location_admin; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location_admin (
    id integer NOT NULL,
    geopoint geometry,
    name character varying(255) NOT NULL,
    parent_id integer,
    status integer,
    type character varying(255),
    zip_code character varying(255),
    city_id character varying(255),
    location_display_name character varying(255)
);

--
-- Name: location_match; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location_match (
    is_exact boolean,
    match_id character varying(255) NOT NULL,
    location_id character varying(255) NOT NULL
);

--
-- Name: location_match_admin; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE location_match_admin (
    location_id integer NOT NULL,
    match_id integer NOT NULL,
    is_exact boolean DEFAULT false NOT NULL
);


--
-- Name: media; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE media (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    dimension character varying(255),
    height integer,
    name character varying(255),
    size integer,
    type character varying(255),
    url character varying(255),
    width integer
);


--
-- Name: native_contacts; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE native_contacts (
    phone_number character varying(255) NOT NULL,
    created_on timestamp without time zone,
    full_name character varying(255),
    company_name character varying(255),
    account_id character varying(255) NOT NULL
);


--
-- Name: nearby_localities; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE nearby_localities (
    id character varying(255) NOT NULL,
    status character varying(255),
    location_id character varying(255),
    nearby_location character varying(255)
);


--
-- Name: nearby_localities_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: moveuser
--

CREATE SEQUENCE nearby_localities_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nearby_localities_admin; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE nearby_localities_admin (
    id integer DEFAULT nextval('nearby_localities_admin_id_seq'::regclass) NOT NULL,
    status integer,
    location_id integer,
    nearby_location integer
);


--
-- Name: oauth_access_token; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE oauth_access_token (
    token_id character varying(255) NOT NULL,
    authentication bytea,
    authentication_id character varying(255),
    refresh_token character varying(255),
    token bytea,
    user_name character varying(255),
    client_id character varying(255) NOT NULL
);

--
-- Name: oauth_authorization; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE oauth_authorization (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    access_token_validity integer,
    additional_information character varying(255),
    authorities character varying(255),
    authorized_grant_types character varying(255),
    client_secret character varying(255),
    resource_ids character varying(255),
    scope character varying(255),
    client_id character varying(255) NOT NULL
);


--
-- Name: otp; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE otp (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    expirytime timestamp without time zone,
    otp character varying(255) NOT NULL,
    phonenumber_id character varying(255) NOT NULL
);


--
-- Name: parent_location_admin; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE parent_location_admin (
    id character varying(255) NOT NULL,
    locality_id character varying(255),
    name character varying(255),
    city_id character varying(255)
);


--
-- Name: person; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE person (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);

--
-- Name: phonenumber; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE phonenumber (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    number character varying(255) NOT NULL,
    type character varying(255)
);


--
-- Name: project; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE project (
    id character varying(255) NOT NULL,
    is_user_suggested boolean,
    name character varying(255)
);

--
-- Name: property_listing; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE property_listing (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    area integer,
    commission_type character varying(255),
    custom_sublocation character varying(255),
    description character varying(4000),
    external_source character varying(255),
    is_hot boolean,
    last_renewed_time timestamp without time zone,
    property_market character varying(255),
    rent_price bigint NOT NULL,
    rooms character varying(255),
    sale_price bigint NOT NULL,
    short_reference character varying(24),
    status character varying(255),
    sub_type character varying(255),
    type character varying(255),
    account_id character varying(255),
    location_id character varying(255),
    amenities character varying(1000) DEFAULT NULL::character varying
);

--
-- Name: property_search_relation; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE property_search_relation (
    search_id character varying(255) NOT NULL,
    property_listing_id character varying(255) NOT NULL,
    account_id character varying(255) NOT NULL
);


--
-- Name: search; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE search (
    id character varying(255) NOT NULL,
    amenities character varying(255),
    location_id character varying(255) NOT NULL,
    max_area integer,
    max_rent_price bigint,
    max_sale_price bigint,
    min_area integer,
    min_rent_price bigint,
    min_sale_price bigint,
    property_market character varying(255) NOT NULL,
    property_sub_type character varying(255),
    property_type character varying(255) NOT NULL,
    requirement_name character varying(255),
    rooms character varying(255),
    search_time timestamp without time zone NOT NULL,
    transaction_type character varying(255) NOT NULL,
    account_id character varying(255)
);


SET default_with_oids = true;

--
-- Name: short_reference; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE short_reference (
    reference character varying(255)
);


SET default_with_oids = false;

--
-- Name: sync_update; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update (
    update_index integer NOT NULL,
    account_id character varying(255) NOT NULL,
    update character varying(10000) NOT NULL
);




--
-- Name: sync_update_read_cursors; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update_read_cursors (
    client_id character varying(255) NOT NULL,
    max_acked_update integer NOT NULL
);




--
-- Name: sync_update_write_cursors; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE sync_update_write_cursors (
    account_id character varying(255) NOT NULL,
    total_updates integer NOT NULL
);




--
-- Name: trustscore; Type: TABLE; Schema: public; Owner: moveuser; Tablespace: 
--

CREATE TABLE trustscore (
    id character varying(255) NOT NULL,
    created_time timestamp without time zone NOT NULL,
    fromaccount bytea NOT NULL,
    message character varying(255),
    toaccount_id character varying(255)
);




--
-- Name: account_account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_account
    ADD CONSTRAINT account_account_pkey PRIMARY KEY (account_id, contacts_id);


--
-- Name: account_city_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_city
    ADD CONSTRAINT account_city_pkey PRIMARY KEY (account_id, cities_id);


--
-- Name: account_location_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_location
    ADD CONSTRAINT account_location_pkey PRIMARY KEY (account_id, localities_id);


--
-- Name: account_phonenumber_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_phonenumber
    ADD CONSTRAINT account_phonenumber_pkey PRIMARY KEY (account_id, phonenumbers_id);


--
-- Name: account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- Name: account_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_relations
    ADD CONSTRAINT account_relations_pkey PRIMARY KEY (otheraccount_id, account_id);


--
-- Name: amenity_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY amenity
    ADD CONSTRAINT amenity_pkey PRIMARY KEY (id);


--
-- Name: application_lock_lock_type_key; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY application_lock
    ADD CONSTRAINT application_lock_lock_type_key UNIQUE (lock_type);


--
-- Name: association_account_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY association_account
    ADD CONSTRAINT association_account_pkey PRIMARY KEY (associations_id, agents_id);


--
-- Name: association_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY association
    ADD CONSTRAINT association_pkey PRIMARY KEY (id);


--
-- Name: bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY bookmark
    ADD CONSTRAINT bookmark_pkey PRIMARY KEY (id);


--
-- Name: city_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- Name: client_analytics_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY client_analytics
    ADD CONSTRAINT client_analytics_pkey PRIMARY KEY (id);


--
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: configuration_primary_key; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_primary_key PRIMARY KEY (id);


--
-- Name: invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY invitation
    ADD CONSTRAINT invitation_pkey PRIMARY KEY (id);


--
-- Name: location_admin_primary_key; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY location_admin
    ADD CONSTRAINT location_admin_primary_key PRIMARY KEY (id);


--
-- Name: location_match_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY location_match
    ADD CONSTRAINT location_match_pkey PRIMARY KEY (match_id, location_id);


--
-- Name: location_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- Name: media_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: native_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY native_contacts
    ADD CONSTRAINT native_contacts_pkey PRIMARY KEY (phone_number, account_id);


--
-- Name: nearby_localities_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT nearby_localities_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (token_id, client_id);


--
-- Name: oauth_authorization_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY oauth_authorization
    ADD CONSTRAINT oauth_authorization_pkey PRIMARY KEY (id);


--
-- Name: otp_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY otp
    ADD CONSTRAINT otp_pkey PRIMARY KEY (id);


--
-- Name: person_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: phonenumber_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY phonenumber
    ADD CONSTRAINT phonenumber_pkey PRIMARY KEY (id);


--
-- Name: project_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- Name: property_listing_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY property_listing
    ADD CONSTRAINT property_listing_pkey PRIMARY KEY (id);


--
-- Name: property_search_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY property_search_relation
    ADD CONSTRAINT property_search_relation_pkey PRIMARY KEY (search_id, property_listing_id, account_id);


--
-- Name: search_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY search
    ADD CONSTRAINT search_pkey PRIMARY KEY (id);


--
-- Name: sync_update_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY sync_update
    ADD CONSTRAINT sync_update_pkey PRIMARY KEY (update_index, account_id);


--
-- Name: sync_update_read_cursors_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY sync_update_read_cursors
    ADD CONSTRAINT sync_update_read_cursors_pkey PRIMARY KEY (client_id);


--
-- Name: sync_update_write_cursors_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY sync_update_write_cursors
    ADD CONSTRAINT sync_update_write_cursors_pkey PRIMARY KEY (account_id);


--
-- Name: trustscore_pkey; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY trustscore
    ADD CONSTRAINT trustscore_pkey PRIMARY KEY (id);


--
-- Name: uk_1u14xqkuoljaam6ohfsq0v7nc; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_phonenumber
    ADD CONSTRAINT uk_1u14xqkuoljaam6ohfsq0v7nc UNIQUE (phonenumbers_id);


--
-- Name: uk_5w9xbbtqwvj0atju5m392bal5; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY amenity
    ADD CONSTRAINT uk_5w9xbbtqwvj0atju5m392bal5 UNIQUE (name);


--
-- Name: uk_b6tpfd3sd1ch530wfapd41nyq; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY city
    ADD CONSTRAINT uk_b6tpfd3sd1ch530wfapd41nyq UNIQUE (name);

--
-- Name: uk_en01cddg8mfxmr0k6mpbkdegq; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY oauth_authorization
    ADD CONSTRAINT uk_en01cddg8mfxmr0k6mpbkdegq UNIQUE (client_id);


--
-- Name: uk_hd1df1p56qg4noqjpwb4gp8ss; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account_city
    ADD CONSTRAINT uk_hd1df1p56qg4noqjpwb4gp8ss UNIQUE (cities_id);


--
-- Name: uk_iaav1gdoakrqyo42d3b6qgbsr; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY phonenumber
    ADD CONSTRAINT uk_iaav1gdoakrqyo42d3b6qgbsr UNIQUE (number);


--
-- Name: uk_jnxat7s0r31ow77f2vprdti31; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY association
    ADD CONSTRAINT uk_jnxat7s0r31ow77f2vprdti31 UNIQUE (name);


--
-- Name: uk_oxdk48wtjc5s524k8ds347giv; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT uk_oxdk48wtjc5s524k8ds347giv UNIQUE (primaryphonenumber_id);


--
-- Name: uk_sp8o76qtiwlt0ywbep1xaa3ss; Type: CONSTRAINT; Schema: public; Owner: moveuser; Tablespace: 
--

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT uk_sp8o76qtiwlt0ywbep1xaa3ss UNIQUE (client_id);


--
-- Name: fk_1u14xqkuoljaam6ohfsq0v7nc; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_phonenumber
    ADD CONSTRAINT fk_1u14xqkuoljaam6ohfsq0v7nc FOREIGN KEY (phonenumbers_id) REFERENCES phonenumber(id);


--
-- Name: fk_1xlv3y6x6cfmt21r5s4od3d67; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY location_match
    ADD CONSTRAINT fk_1xlv3y6x6cfmt21r5s4od3d67 FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: fk_2h5hu3mroy51r1hvdi0e5ekvw; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY location
    ADD CONSTRAINT fk_2h5hu3mroy51r1hvdi0e5ekvw FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: fk_41uvwjaue2rwnd3kvjxqt8nxe; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_search_relation
    ADD CONSTRAINT fk_41uvwjaue2rwnd3kvjxqt8nxe FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_4id4jd6ihqn59251w0if57bcf; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY search
    ADD CONSTRAINT fk_4id4jd6ihqn59251w0if57bcf FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_4pdhhr40slsqsy5bgi58vnpo6; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_listing_amenity
    ADD CONSTRAINT fk_4pdhhr40slsqsy5bgi58vnpo6 FOREIGN KEY (property_listing_id) REFERENCES property_listing(id);


--
-- Name: fk_7kn46upix6as9pmjxbyhx31u3; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY client
    ADD CONSTRAINT fk_7kn46upix6as9pmjxbyhx31u3 FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_8bopt1se7yr5spa647n6x3txm; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_phonenumber
    ADD CONSTRAINT fk_8bopt1se7yr5spa647n6x3txm FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_aij5otjns8qn2y96graa9qs73; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_relations
    ADD CONSTRAINT fk_aij5otjns8qn2y96graa9qs73 FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_best3ew2nov5yd0cor3hua0dq; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_city
    ADD CONSTRAINT fk_best3ew2nov5yd0cor3hua0dq FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_bfur0amyt55jvcsm2lby9ava1; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_search_relation
    ADD CONSTRAINT fk_bfur0amyt55jvcsm2lby9ava1 FOREIGN KEY (search_id) REFERENCES search(id);


--
-- Name: fk_blu34gxuh8y1aou2c4h3etcdk; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT fk_blu34gxuh8y1aou2c4h3etcdk FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: fk_d7tf2youw8w221g21gjfgjoxc; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_location
    ADD CONSTRAINT fk_d7tf2youw8w221g21gjfgjoxc FOREIGN KEY (localities_id) REFERENCES location(id);


--
-- Name: fk_d85mrptbcgyeukfhir02u3px8; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_location
    ADD CONSTRAINT fk_d85mrptbcgyeukfhir02u3px8 FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_dgp0e18bxswh2kuuvw8pai8q4; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account
    ADD CONSTRAINT fk_dgp0e18bxswh2kuuvw8pai8q4 FOREIGN KEY (fullphoto_id) REFERENCES media(id);


--
-- Name: fk_en01cddg8mfxmr0k6mpbkdegq; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY oauth_authorization
    ADD CONSTRAINT fk_en01cddg8mfxmr0k6mpbkdegq FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: fk_f30yemigpvtmt1nqokvnc2sex; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_account
    ADD CONSTRAINT fk_f30yemigpvtmt1nqokvnc2sex FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_fdt248sffav0v6l6putw4wr4o; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY location_match
    ADD CONSTRAINT fk_fdt248sffav0v6l6putw4wr4o FOREIGN KEY (match_id) REFERENCES location(id);


--
-- Name: fk_fpoqowvy4isxgfs5slnx5n2bp; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY association
    ADD CONSTRAINT fk_fpoqowvy4isxgfs5slnx5n2bp FOREIGN KEY (phonenumber_id) REFERENCES phonenumber(id);


--
-- Name: fk_ggwcyqynpspksk493dpinyydv; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY otp
    ADD CONSTRAINT fk_ggwcyqynpspksk493dpinyydv FOREIGN KEY (phonenumber_id) REFERENCES phonenumber(id);


--
-- Name: fk_hd1df1p56qg4noqjpwb4gp8ss; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_city
    ADD CONSTRAINT fk_hd1df1p56qg4noqjpwb4gp8ss FOREIGN KEY (cities_id) REFERENCES city(id);


--
-- Name: fk_hdagi65cgk7trqffh0ibhp2yp; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY native_contacts
    ADD CONSTRAINT fk_hdagi65cgk7trqffh0ibhp2yp FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_ivttmfgvpw9b9p1x1y9rdit11; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_listing
    ADD CONSTRAINT fk_ivttmfgvpw9b9p1x1y9rdit11 FOREIGN KEY (account_id) REFERENCES account(id);


--
-- Name: fk_j7vqm9k13ye7updadenx73f40; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_listing_amenity
    ADD CONSTRAINT fk_j7vqm9k13ye7updadenx73f40 FOREIGN KEY (amenities_id) REFERENCES amenity(id);


--
-- Name: fk_jik2cjyoxl5e84vv4ycxm7h6p; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY association_account
    ADD CONSTRAINT fk_jik2cjyoxl5e84vv4ycxm7h6p FOREIGN KEY (associations_id) REFERENCES association(id);


--
-- Name: fk_ju31kmob2r7pfo99rtoj38kcj; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY invitation
    ADD CONSTRAINT fk_ju31kmob2r7pfo99rtoj38kcj FOREIGN KEY (toaccount_id) REFERENCES account(id);


--
-- Name: fk_k499tb1m1tdkrhltpqpiaogwt; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account_account
    ADD CONSTRAINT fk_k499tb1m1tdkrhltpqpiaogwt FOREIGN KEY (contacts_id) REFERENCES account(id);


--
-- Name: fk_knilk72jijn87g82vo8tj26be; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_search_relation
    ADD CONSTRAINT fk_knilk72jijn87g82vo8tj26be FOREIGN KEY (property_listing_id) REFERENCES property_listing(id);


--
-- Name: fk_lrhw517q5u4gprn0y0t0jm2dh; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY nearby_localities
    ADD CONSTRAINT fk_lrhw517q5u4gprn0y0t0jm2dh FOREIGN KEY (nearby_location) REFERENCES location(id);


--
-- Name: fk_oxdk48wtjc5s524k8ds347giv; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account
    ADD CONSTRAINT fk_oxdk48wtjc5s524k8ds347giv FOREIGN KEY (primaryphonenumber_id) REFERENCES phonenumber(id);


--
-- Name: fk_p9k9m604esoycixvbrr8ickji; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY account
    ADD CONSTRAINT fk_p9k9m604esoycixvbrr8ickji FOREIGN KEY (thumbnailphoto_id) REFERENCES media(id);


--
-- Name: fk_q9a9u1kxfh4yuukejnrh2tgin; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY property_listing
    ADD CONSTRAINT fk_q9a9u1kxfh4yuukejnrh2tgin FOREIGN KEY (location_id) REFERENCES location(id);


--
-- Name: fk_qgw7q4ekate91amwvp5mn3qi; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY association_account
    ADD CONSTRAINT fk_qgw7q4ekate91amwvp5mn3qi FOREIGN KEY (agents_id) REFERENCES account(id);


--
-- Name: fk_ro2pxrr059tb2wkn3t0k9wrx6; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY trustscore
    ADD CONSTRAINT fk_ro2pxrr059tb2wkn3t0k9wrx6 FOREIGN KEY (toaccount_id) REFERENCES account(id);


--
-- Name: fk_sp8o76qtiwlt0ywbep1xaa3ss; Type: FK CONSTRAINT; Schema: public; Owner: moveuser
--

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT fk_sp8o76qtiwlt0ywbep1xaa3ss FOREIGN KEY (client_id) REFERENCES client(id);




--
-- PostgreSQL database dump complete
--

