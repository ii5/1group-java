package one.group.authenticate;

import one.group.core.request.RawRequest;

/**
 * The <code>Authenticator</code> class declares the basic functions of an
 * authenticator.
 * 
 * 
 * @author nyalfernandes
 * 
 */
public interface Authenticator
{
    /**
     * Authenticate the request.
     * 
     * @return
     */
    public boolean authenticate(RawRequest request);

    // /**
    // * Is the authentication mechanism valid based on the inputs?
    // *
    // * @return
    // */
    // public boolean isValid();

}
