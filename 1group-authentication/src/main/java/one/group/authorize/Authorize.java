package one.group.authorize;

import one.group.core.request.RawRequest;

/**
 * Interface <code>Authorize</code> validates permission for each request. For
 * each request to be authorized it has to be authenticated first.
 * 
 * 
 * @author nyalfernandes
 * 
 */
public interface Authorize
{
    public boolean authorize(RawRequest request);
}
