package one.group.authorize.impl;

import one.group.authorize.Authorize;
import one.group.core.request.RawRequest;

/**
 * <class>ServiceAccessAuthorization</code> is a concrete implementation of
 * {@link Authorize}. This authorization type provides checks to allow certain
 * roles to access certain services.
 * 
 * @author nyalfernandes
 * 
 */
public class ServiceAccessAuthorization implements Authorize
{

    public boolean authorize(final RawRequest request)
    {
        // TODO Auto-generated method stub
        return false;
    }
}
