package one.group.core.respone;

import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPResponseType;
import one.group.core.request.RawRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.codes.OneGroupExceptionCode;
import one.group.exceptions.movein.OneGroupException;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ResponseBuilder
{
    public static String buildOkResponse(final Object data, String message)
    {
        String jsonString = "";
        if (Utils.isNullOrEmpty(message))
        {
            message = "Processed!";
        }

        jsonString = buildResponseString(false, message, "200", null, data, false);

        return jsonString;

    }

    public static String buildOkResponse(final Object data, String message, boolean forBpo)
    {
        String jsonString = "";
        if (Utils.isNullOrEmpty(message))
        {
            message = "Processed!";
        }

        jsonString = buildResponseString(false, message, "200", null, data, forBpo);

        return jsonString;

    }

    public static String buildOkResponse(final Object data, GeneralSuccessCode successCode)
    {
        String message = successCode.getSuccessMessage();
        // String code = successCode.getCode();
        String code = successCode.getResponseType().getCode() + "";
        String jsonString = "";
        if (Utils.isNullOrEmpty(message))
        {
            message = "Processed!";
        }

        jsonString = buildResponseString(false, message, code, null, data, false);

        return jsonString;

    }

    public static String buildResponse(final Map<String, Object> content, final HTTPResponseType responseType, final boolean isFatal, final String message, final String code, final Throwable th)
    {
        Validation.notNull(th, "Request passed should not be null.");
        Validation.notNull(responseType, "Response Type passed should not be null.");
        Validation.notNull(message, "The message should not be null.");
        Validation.notNull(code, "Code passed should not be null.");

        String jsonOutput = null;

        if (responseType.isException())
        {
            jsonOutput = buildResponseString(isFatal, th.getMessage(), code, null, content, false);
            throw new WebApplicationException(Response.status(responseType.getCode()).entity(jsonOutput).build());
        }

        jsonOutput = buildResponseString(isFatal, message, code, null, content, false);

        return jsonOutput;
    }

    public static String buildResponse(final RawRequest request, final Map<String, Object> content, final HTTPResponseType responseType, final boolean isFatal, final String message, final String code)
    {
        Validation.notNull(request, "Request passed should not be null.");
        Validation.notNull(responseType, "Response Type passed should not be null.");
        Validation.notNull(message, "The message should not be null.");
        Validation.notNull(code, "Code passed should not be null.");

        String jsonOutput = buildResponseString(isFatal, message, code, request, content, false);

        if (responseType.isException())
        {
            throw new WebApplicationException(Response.status(responseType.getCode()).entity(jsonOutput).build());
        }

        return jsonOutput;
    }

    public static String buildResponseString(final boolean isFatal, final String message, final String code, final RawRequest request, final Object content, boolean forBpo)
    {
        WSResult result = new WSResult(code, message, isFatal);
        WSResponse response = new WSResponse(result);
        response.setData(content);

        String jsonOutput = null;
        if (forBpo)
        {
            jsonOutput = Utils.getJsonStringForBPO(response);
        }
        else
        {
            jsonOutput = Utils.getJsonString(response);
        }

        return jsonOutput;

    }

    public static void shootException(final OneGroupException exception)
    {
        Validation.notNull(exception, "Exception passed should not be null.");
        String json = buildResponseString(exception.isFatal(), exception.getMessage(), exception.getCode(), null, null, false);
        // String json = buildResponseString(exception.isFatal(),
        // exception.getMessage(), exception.getHttpCode() + "", null, null);

        // throw new
        // WebApplicationException(Response.status(exception.getHttpCode()).entity(json).build());
        throw new WebApplicationException(Response.status(Integer.parseInt(exception.getCode())).entity(json).build());
    }

    private static void shootException(final Throwable th, final boolean isFatal, final String message, final OneGroupExceptionCode code, final int httpCode, final RawRequest request,
            final Object content)
    {
        if (th instanceof OneGroupException)
        {
            shootException((OneGroupException) th);
        }
        Validation.notNull(th, "The exception passed should not be null.");
        String json = buildResponseString(isFatal, message, String.valueOf(code.getCode()), request, content, false);
        // String json = buildResponseString(isFatal, message,
        // String.valueOf(httpCode + ""), request, content);

        // throw new
        // WebApplicationException(Response.status(httpCode).entity(json).build());

        throw new WebApplicationException(Response.status(code.getCode()).entity(json).build());
    }

    public static void shootFatalException(final Throwable th, final OneGroupExceptionCode exceptionCode, final HTTPResponseType responseType, String message)
    {
        message = message == null ? th.getMessage() : message;
        shootException(th, true, message, exceptionCode, responseType.getCode(), null, null);
    }
}
