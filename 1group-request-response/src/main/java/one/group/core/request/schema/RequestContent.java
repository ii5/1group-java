package one.group.core.request.schema;

import java.util.HashMap;
import java.util.Map;

/**
 * <code>RawRequestContent</code> is the base class of all different request
 * contents we can expect in JSON.
 * 
 * @author nyalfernandes
 * 
 */
public abstract class RequestContent
{
    private Map<String, String> data = new HashMap<String, String>();

    public Map<String, String> getData()
    {
        return data;
    }

    public abstract String getJSONString();

    public void setData(final Map<String, String> content)
    {
        this.data = content;
    }

}
