package one.group.core.request;

import javax.ws.rs.core.MultivaluedMap;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.status.RequestStatus;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class RequestBuilder
{
    public static RawRequest build(final MultivaluedMap<String, String> headers, final MultivaluedMap<String, String> pathParameters, final MultivaluedMap<String, String> queryParameters,
            final HTTPRequestMethodType httpMethodType)
    {
        RawRequest request = new RawRequest();
        request.setHeaders(headers);
        request.setStatus(RequestStatus.GHOST);
        request.setpathParameters(pathParameters);
        request.setQueryParameters(queryParameters);
        request.setRequestMethodType(httpMethodType);

        return request;
    }
}
