package one.group.core.request.schema;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The chat message schema expected when requesting the server to send a chat
 * message to another client.
 * 
 * @author nyalfernandes
 * 
 */
@XmlRootElement
public class ChatMessage extends RequestContent
{
    @XmlElement(name = "from_client_id")
    private String fromClientId;

    @XmlElement(name = "from_user_id")
    private String fromUserId;

    @XmlElement(name = "to_user_id")
    private String toUserId;

    @XmlElement(name = "chat_thread_id")
    private String chatThreadId;

    @XmlElement(name = "property_id")
    private String propertyId;

    @XmlElement(name = "attempted_sent_time")
    private Date attemptedSenTTime;

    @XmlElement(name = "token")
    private String messageToken;

    public ChatMessage()
    {
    }

    @Override
    public String getJSONString()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
