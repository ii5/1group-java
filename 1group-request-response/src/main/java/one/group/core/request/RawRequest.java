package one.group.core.request;

import javax.ws.rs.core.MultivaluedMap;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.status.RequestStatus;
import one.group.entities.api.response.ResponseEntity;

/**
 * <code>Request</code> is a simple wrapper to the request through HTTP. A
 * holder map object contains key values of all information that is requested.
 * 
 * 
 * 
 * @author nyalfernandes
 * 
 */
public class RawRequest
{

    private HTTPRequestMethodType requestMethodType = HTTPRequestMethodType.GET;
    private MultivaluedMap<String, String> headers;
    private MultivaluedMap<String, String> pathParameters;
    private MultivaluedMap<String, String> queryParameters;
    private ResponseEntity content;
    private RequestStatus status = RequestStatus.GHOST;

    public ResponseEntity getContent()
    {
        return content;
    }

    public MultivaluedMap<String, String> getHeaders()
    {
        return headers;
    }

    public MultivaluedMap<String, String> getPathParameters()
    {
        return pathParameters;
    }

    public MultivaluedMap<String, String> getQueryParameters()
    {
        return queryParameters;
    }

    public HTTPRequestMethodType getRequestMethodType()
    {
        return requestMethodType;
    }

    public RequestStatus getStatus()
    {
        return status;
    }

    public void setContent(final ResponseEntity content)
    {
        this.content = content;
    }

    public void setHeaders(final MultivaluedMap<String, String> headers)
    {
        this.headers = headers;
    }

    public void setpathParameters(final MultivaluedMap<String, String> uRLparameters)
    {
        pathParameters = uRLparameters;
    }

    public void setQueryParameters(final MultivaluedMap<String, String> queryParameters)
    {
        this.queryParameters = queryParameters;
    }

    public void setRequestMethodType(final HTTPRequestMethodType requestMethodType)
    {
        this.requestMethodType = requestMethodType;
    }

    public void setStatus(final RequestStatus status)
    {
        this.status = status;
    }

}
