package one.group.standalone.consumer;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.PushServerType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.services.helpers.PushServiceObject;
import one.group.standalone.AbstractSyncHandler;
import one.group.standalone.handler.task.SyncTaskHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles consumed records of type {@link SyncDataType#INVALIDATION}.
 * 
 * @author nyalfernandes
 * 
 */
public class HintSyncHandler extends AbstractSyncHandler<WSEntityReference>
{
    private static final Logger logger = LoggerFactory.getLogger(HintSyncHandler.class);

    private ClientService clientService;

    private PushService pushService;

    private SubscriptionService subscriptionService;

    private SyncDBService syncDBService;

    private SyncTaskHandler syncTaskHandler;

    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {

        if (syncLogEntry.getType().equals(SyncDataType.HINT))
        {
            Map<String, Set<ResponseEntity>> accountVsSyncUpdate = syncTaskHandler.handle(syncLogEntry);

            for (String accountId : accountVsSyncUpdate.keySet())
            {
                List<String> pushChannelIds = clientService.fetchAllPushChannelsOfClientsOfAccount(accountId);

                for (ResponseEntity syncUpdate : accountVsSyncUpdate.get(accountId))
                {
                    WSSyncUpdate update = (WSSyncUpdate) syncUpdate;
                    
                    syncDBService.appendUpdate(accountId, update);
                    
                    PushServiceObject config = new PushServiceObject();
                    config.addService(PushServerType.PUSHER);
                    config.setPusherChannelList(pushChannelIds);
                    config.setPusherEventName("");
                    config.setData(update);

                    pushService.forwardRequest(config);
                }
            }
        }
        else
        {
            getSuccessor().handle(syncLogEntry);
        }
        return null;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public SyncTaskHandler getSyncTaskHandler()
    {
        return syncTaskHandler;
    }

    public void setSyncTaskHandler(SyncTaskHandler syncTaskHandler)
    {
        this.syncTaskHandler = syncTaskHandler;
    }
}
