package one.group.standalone.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.ClientType;
import one.group.core.enums.PushServerType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.jpa.Client;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.services.helpers.PushServiceObject;
import one.group.standalone.AbstractSyncHandler;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles consumed records of type {@link SyncDataType#MESSAGE}.
 * 
 * @author nyalfernandes
 * 
 */
public class ChatMessageSyncHandler extends AbstractSyncHandler<WSChatMessage>
{
    private static final Logger logger = LoggerFactory.getLogger(ChatMessageSyncHandler.class);

    private ClientService clientService;

    private PushService pushService;

    private SubscriptionService subscriptionService;

    private SyncDBService syncDBService;

    @Override
    public Map<String, Set<ResponseEntity>> process(final SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {
        Validation.notNull(syncLogEntry, "Record should not be null.");
        if (syncLogEntry.getType().equals(SyncDataType.MESSAGE))
        {

            List<String> associatedAccountIds = subscriptionService.retreiveAllSubscribersOf(syncLogEntry.getAssociatedEntityId(), syncLogEntry.getAssociatedEntityType());
            WSChatMessage wsChatMessage = syncLogEntry.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSChatMessage.class);
            for (String accountId : associatedAccountIds)
            {
                List<Client> allClientsOfAccount = clientService.fetchAllActiveClientsOfAccount(accountId);
                Map<PushServerType, List<String>> channelTypeVsNames = getAllChannelsOfClients(allClientsOfAccount);
                List<String> pushChannelIds = channelTypeVsNames.get(PushServerType.PUSHER);
                List<String> androidDeviceIds = channelTypeVsNames.get(PushServerType.GCM);
                List<String> iosDeviceIds = channelTypeVsNames.get(PushServerType.APNS);

                WSSyncUpdate update = new WSSyncUpdate(Constant.INDEX_NOT_SET, syncLogEntry.getType());
                // update.setChatMessage(wsChatMessage);

                syncDBService.appendUpdate(accountId, update);

                PushServiceObject config = new PushServiceObject();
                config.addService(PushServerType.PUSHER);

                // Dont send GCM notification to sender of chat message
                if (!wsChatMessage.getFromAccountId().equals(accountId))
                {
                    config.addService(PushServerType.GCM);
                    config.addService(PushServerType.APNS);
                    config.setGcmDeviceList(androidDeviceIds);
                    config.setApnsDeviceList(iosDeviceIds);
                }
                config.setPusherChannelList(pushChannelIds);
                config.setData(update);
                config.setPusherEventName("");

                pushService.forwardRequest(config);
            }
        }
        else
        {
            logger.debug("Passing to successor: " + getSuccessor());
            getSuccessor().handle(syncLogEntry);
        }

        return null;

    }

    private Map<PushServerType, List<String>> getAllChannelsOfClients(List<Client> clientList)
    {
        List<String> pushChannelList = new ArrayList<String>();
        List<String> iosDeviceIds = new ArrayList<String>();
        List<String> androidDeviceIds = new ArrayList<String>();
        Map<PushServerType, List<String>> channelTypeVsNames = new HashMap<PushServerType, List<String>>();

        for (Client c : clientList)
        {
            if (c.getPushChannel() != null && !pushChannelList.contains(c.getPushChannel()))
            {
                pushChannelList.add(c.getPushChannel());
            }

            if (c.getDevicePlatform().equals(ClientType.IOS.toString()) && c.getCpsId() != null && !iosDeviceIds.contains(c.getCpsId()))
            {
                iosDeviceIds.add(c.getDeviceToken());
            }
            else if (c.getDevicePlatform().equals(ClientType.ANDROID.name()) && c.getCpsId() != null && !androidDeviceIds.contains(c.getCpsId()))
            {
                androidDeviceIds.add(c.getCpsId());
            }
        }

        channelTypeVsNames.put(PushServerType.APNS, iosDeviceIds);
        channelTypeVsNames.put(PushServerType.GCM, androidDeviceIds);
        channelTypeVsNames.put(PushServerType.PUSHER, pushChannelList);

        return channelTypeVsNames;
    }

    private List<String> getAlliOSDevicesOfClients(List<Client> clientList)
    {
        List<String> iosDeviceIds = new ArrayList<String>();
        for (Client c : clientList)
        {
            if (c.getDevicePlatform().equals(ClientType.IOS.getPlatform()) && c.getDeviceToken() != null && !iosDeviceIds.contains(c.getDeviceToken()))
            {
                iosDeviceIds.add(c.getPushChannel());
            }
        }

        return iosDeviceIds;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }
}
