package one.group.standalone.consumer;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import one.group.entities.sync.SyncLogEntry;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.standalone.SyncEntityHandler;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Meant to be run in a thread.
 * 
 * @author nyalfernandes
 * 
 * @param <T>
 */
public class SimpleSyncLogConsumer implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(SimpleSyncLogConsumer.class);

    private KafkaStream<byte[], byte[]> kafkaStream;
    
    private SyncEntityHandler syncHandlerChain;
    
    private SubscriptionService subscriptionService;
    
    private PushService pushService;
    
    private ClientService clientService;
    
    public ClientService getClientService()
    {
        return clientService;
    }
    
    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public PushService getPushService()
    {
        return pushService;
    }
    
    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }
    
    public KafkaStream<byte[], byte[]> getKafkaStream()
    {
        return kafkaStream;
    }
    
    public void setKafkaStream(KafkaStream<byte[], byte[]> kafkaStream)
    {
        this.kafkaStream = kafkaStream;
    }
    
    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }
    
    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }
    
    public SyncEntityHandler getSyncHandlerChain()
    {
        return syncHandlerChain;
    }
    
    public void setSyncHandlerChain(SyncEntityHandler syncHandlerChain)
    {
        this.syncHandlerChain = syncHandlerChain;
    }

    // TODO: Implement cache invalidation.
    public void run()
    {
        SyncLogEntry syncLogEntry = null;
        ConsumerIterator<byte[], byte[]> it = kafkaStream.iterator();
        while (it.hasNext())
        {
            logger.info(it.clientId());
            try
            {
                MessageAndMetadata<byte[], byte[]> messageAndData = it.next();
                String rawData = new String(messageAndData.message());
                System.out.println(messageAndData.offset());
                rawData = rawData.trim();
                syncLogEntry = (SyncLogEntry) Utils.getInstanceFromJson(rawData, SyncLogEntry.class);
                logger.info("Got data: " + syncLogEntry);
                syncHandlerChain.handle(syncLogEntry);
                
            }
            catch (Exception e)
            {
                logger.error("Error while consuming message.", e);
            }
            finally
            {
                
            }
        }
        
        logger.error("Closing connection for Consumer for stream " + kafkaStream);

    }

    public static void main(String[] args) throws UnsupportedEncodingException
    {
        String topic = "page_visits";
        int threads = 5;
        Properties props = new Properties();
        props.setProperty("zookeeper.connect", "localhost:2181");
        props.setProperty("group.id", "GROUP-1");
        props.setProperty("zookeeper.seesion.timeout.ms", "400");
        props.setProperty("zookeeper.sync.time.ms", "200");
        props.setProperty("auto.commit.interval.ms", "1000");

        ConsumerConfig config = new ConsumerConfig(props);
        ConsumerConnector consumer = Consumer.createJavaConsumerConnector(config);

        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, 1);

        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
        KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        while (it.hasNext())
        {
            System.out.println("D : " + new String(it.next().message()));
        }

    }
}
