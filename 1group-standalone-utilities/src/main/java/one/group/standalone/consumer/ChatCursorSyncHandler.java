package one.group.standalone.consumer;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.PushServerType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSChatCursor;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.services.helpers.PushServiceObject;
import one.group.standalone.AbstractSyncHandler;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ChatCursorSyncHandler extends AbstractSyncHandler<WSChatCursor>
{
    private static final Logger logger = LoggerFactory.getLogger(ChatCursorSyncHandler.class);

    private ClientService clientService;

    private PushService pushService;

    private SubscriptionService subscriptionService;

    private SyncDBService syncDBService;

    @Override
    public Map<String, Set<ResponseEntity>> process(final SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {
        Validation.notNull(syncLogEntry, "Record should not be null.");
        if (syncLogEntry.getType().equals(SyncDataType.CHAT_CURSOR))
        {
            List<String> associatedAccountIds = subscriptionService.retreiveAllSubscribersOf(syncLogEntry.getAssociatedEntityId(), syncLogEntry.getAssociatedEntityType());
            for (String accountId : associatedAccountIds)
            {
                List<String> pushChannelIds = clientService.fetchAllPushChannelsOfClientsOfAccount(accountId);

                WSSyncUpdate update = new WSSyncUpdate(-1, syncLogEntry.getType());
                update.setChatCursor(syncLogEntry.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSChatCursor.class));

                syncDBService.appendUpdate(accountId, update);

                PushServiceObject config = new PushServiceObject();
                config.addService(PushServerType.PUSHER);
                config.setPusherChannelList(pushChannelIds);
                config.setData(update);
                config.setPusherEventName("");

                pushService.forwardRequest(config);
            }
        }
        else
        {
            logger.debug("Passing to successor: " + getSuccessor());
            getSuccessor().handle(syncLogEntry);
        }

        return null;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }
}
