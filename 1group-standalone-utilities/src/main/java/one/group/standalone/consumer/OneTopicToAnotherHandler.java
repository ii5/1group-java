package one.group.standalone.consumer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.I0Itec.zkclient.ZkClient;
import org.apache.log4j.Logger;

import kafka.admin.AdminUtils;

import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.standalone.AbstractSyncHandler;
import one.group.standalone.ZookeeperClientFactory;
import one.group.sync.producer.SyncLogProducer;

public class OneTopicToAnotherHandler extends AbstractSyncHandler<SyncLogEntry>
{
    private static final Logger logger = Logger.getLogger(OneTopicToAnotherHandler.class);
    private SyncLogProducer kafkaProducer;
    
    private Properties standAloneProperties;
    
    private ZookeeperClientFactory zkClientFactory;
    
    public ZookeeperClientFactory getZkClientFactory()
    {
        return zkClientFactory;
    }
    
    public void setZkClientFactory(ZookeeperClientFactory zkClientFactory)
    {
        this.zkClientFactory = zkClientFactory;
    }
    
    public Properties getStandAloneProperties()
    {
        return standAloneProperties;
    }
    
    public void setStandAloneProperties(Properties standAloneProperties)
    {
        this.standAloneProperties = standAloneProperties;
    }
    
    public SyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }
    
    public void setKafkaProducer(SyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }
    
    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> responseMap = new HashMap<String, Set<ResponseEntity>>();
        
        SyncDataType logType = record.getType();
        String logTopic = standAloneProperties.getProperty("log.topic");
        String appTopic = standAloneProperties.getProperty("app.topic");
        ZkClient zkClient = zkClientFactory.getClient();
        
        boolean logTopicExists = AdminUtils.topicExists(zkClient, logTopic);
        boolean appTopicExists = AdminUtils.topicExists(zkClient, appTopic);
        
        if (!logTopicExists || !appTopicExists)
        {
            throw new IllegalStateException("Needed topic does not exits: LOG_TOPIC[" + logTopic + "=" + logTopicExists +"], APP_TOPIC[" + appTopic + "=" + appTopicExists + "]");
        }
        
        if (logType.equals(SyncDataType.LOG))
        {
            String key = null;
            try { key = record.getOriginatingAccount().getId();} catch (Exception e) {};
            logger.info(logTopic + ":" + record);
            kafkaProducer.write(record, logTopic, key);
        }
        else
        {
            logger.info(appTopic + ":" + record);
            kafkaProducer.write(record, appTopic);
        }
        
        return responseMap;
    }
    
}
