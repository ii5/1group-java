package one.group.standalone;

import java.util.Properties;

import kafka.utils.ZKStringSerializer$;

import org.I0Itec.zkclient.ZkClient;

/**
 * Zookeeper factory.
 * 
 * @author nyalfernandes
 * 
 */
public class ZookeeperClientFactory
{
    private Properties configProperties = new Properties();
    private ZkClient client;

    public ZkClient getClient()
    {
        if (client == null)
        {
            final String zkConnect = configProperties.getProperty(ZookeeperConfig.HOST) + ":" + configProperties.getProperty(ZookeeperConfig.PORT) + "/kafka";
            client = new ZkClient(zkConnect, Integer.valueOf(configProperties.getProperty(ZookeeperConfig.SESSION_TIMEOUT)), Integer.valueOf(configProperties
                    .getProperty(ZookeeperConfig.CONNECTION_TIMEOUT)), ZKStringSerializer$.MODULE$);
        }

        return client;
    }

    class ZookeeperConfig
    {
        public static final String HOST = "zookeeper.host";
        public static final String PORT = "zookeeper.port";
        public static final String SESSION_TIMEOUT = "zookeeper.session.timeout";
        public static final String CONNECTION_TIMEOUT = "zookeeper.connection.timeout";
        // public static final String SERIALIZER = "zookeeper.serializer";
    }
    
    public void setConfigProperties(Properties configProperties)
    {
        this.configProperties = configProperties;
    }
    
    public Properties getConfigProperties()
    {
        return configProperties;
    }
}
