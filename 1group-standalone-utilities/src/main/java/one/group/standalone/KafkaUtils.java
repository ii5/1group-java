package one.group.standalone;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kafka.admin.AdminUtils;
import kafka.utils.ZkUtils;
import one.group.core.enums.TopicType;
import one.group.standalone.consumer.KafkaConsumerFactory;
import one.group.standalone.consumer.OneGroupConsumer;
import one.group.sync.KafkaConfiguration;
import one.group.sync.SimplePartitioner;
import one.group.sync.producer.KafkaProducerFactory;
import one.group.utils.validation.Validation;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import scala.collection.Map;
import scala.collection.Seq;

/**
 * Set up topics and partitions on Kafka
 * 
 * @author nyalfernandes
 * 
 */
public class KafkaUtils implements ApplicationContextAware
{
    private static final Logger logger = LoggerFactory.getLogger(KafkaUtils.class);

    private ZookeeperClientFactory zkClientFactory;

    private KafkaConsumerFactory consumerFactory;

    private KafkaProducerFactory<String, String> producerFactory;

    private SyncEntityHandler syncHandler;

    private ApplicationContext applicationContext;

    private KafkaConfiguration kafkaConfiguration;

    private static ExecutorService executor;

    public static final String SYNC_SERVER_OFFSET_FILE_TEMPLATE = "[topic]-offsets.log";

    public SyncEntityHandler getSyncHandler()
    {
        return syncHandler;
    }

    public void setSyncHandler(SyncEntityHandler syncHandler)
    {
        this.syncHandler = syncHandler;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public KafkaConsumerFactory getConsumerFactory()
    {
        return consumerFactory;
    }

    public void setConsumerFactory(KafkaConsumerFactory consumerFactory)
    {
        this.consumerFactory = consumerFactory;
    }

    public ZookeeperClientFactory getZkClientFactory()
    {
        return zkClientFactory;
    }

    public void setZkClientFactory(ZookeeperClientFactory zkClientFactory)
    {
        this.zkClientFactory = zkClientFactory;
    }

    public KafkaProducerFactory<String, String> getProducerFactory()
    {
        return producerFactory;
    }

    public void setProducerFactory(KafkaProducerFactory<String, String> producerFactory)
    {
        this.producerFactory = producerFactory;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    public void initialize()
    {
        try
        {
            for (TopicType type : TopicType.values())
            {
                logger.info("Initializing all for topic type " + type);
                String appTopic = kafkaConfiguration.getTopic(type).getName();
                createTopicIfNotExists(appTopic, type);
                Thread.sleep(2000);
                createConsumers(appTopic, type);
            }
            
            // Thread.sleep(2000);
            // createProducers();
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    public void createTopicIfNotExists(String topicName, TopicType type)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        ZkClient client = zkClientFactory.getClient();
        if (!AdminUtils.topicExists(client, topicName))
        {
            logger.info("Creating topic " + topicName);
            AdminUtils.createTopic(client, topicName, kafkaConfiguration.getTopic(type).getPartitionCount(), kafkaConfiguration.getTopic(type).getReplicationFactor(), new Properties());
        }
        else
        {
            logger.warn("Topic " + topicName + " already exists.");
        }
    }

    public void deleteTopic(String topicName)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        ZkClient client = zkClientFactory.getClient();
        if (AdminUtils.topicExists(client, topicName))
        {
            logger.debug("Deleting topic " + topicName);
            AdminUtils.deleteTopic(client, topicName);
        }
        else
        {
            logger.warn("Topic " + topicName + " already deleted.");
        }
    }

    public void partitionTopicFurther(String topicName, int addOn)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        Validation.isFalse(addOn > 0, "Additional partitions should always be greater that 0.");

        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> partitionsOfTopic = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();
        int currentPartitionSize = partitionsOfTopic.size();
        AdminUtils.addPartitions(client, topicName, currentPartitionSize + addOn, "", true, new Properties());
    }

    public void createConsumers(String topicName, TopicType type) throws Exception
    {
        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> topicPartitioMap = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();

        if (topicPartitioMap == null)
        {
            throw new IllegalStateException("No topic '" + topicName + "' present.");
        }

        List<Integer> toHandlePartitionList = getPartitionsOfTopic(topicPartitioMap, type);

        if (!toHandlePartitionList.isEmpty())
        {
            executor = Executors.newFixedThreadPool(toHandlePartitionList.size());
            Properties serverProperties = applicationContext.getBean("sync.server.properties", Properties.class);
            String syncOffsetFileName = (serverProperties == null) ? SYNC_SERVER_OFFSET_FILE_TEMPLATE : serverProperties.getProperty("syncOffsetLocation", SYNC_SERVER_OFFSET_FILE_TEMPLATE);

            logger.info("Loading offsets for topic " + topicName);
            Properties syncOffsetProperties = new Properties();
            syncOffsetFileName = syncOffsetFileName.replace("[topic]", topicName);
            File syncOffsetFile = new File(syncOffsetFileName);
            if (syncOffsetFile.exists())
            {
                syncOffsetProperties.load(new FileInputStream(syncOffsetFileName));
            }
            else
            {
                if (syncOffsetFile.getParentFile() != null)
                {
                    syncOffsetFile.getParentFile().mkdirs();
                }
                syncOffsetFile.canExecute();
                syncOffsetFile.canWrite();
                syncOffsetFile.canRead();
                syncOffsetFile.createNewFile();
            }

            for (int partition : toHandlePartitionList)
            {
                Thread.sleep(100);
                OneGroupConsumer consumer = new OneGroupConsumer(partition, syncOffsetProperties, consumerFactory.getConfigProperties(), syncOffsetFile, topicName, type);
                consumer.setSyncHandler(syncHandler);
                logger.debug(""+consumer);
                executor.submit(consumer);
            }
        }
    }

    public void createProducers()
    {
        producerFactory.initialize();
    }

    public int partitionFor(String key, TopicType type)
    {
        return new SimplePartitioner().partition(key, kafkaConfiguration.getTopic(type).getPartitionCount());
    }

    private List<Integer> getPartitionsOfTopic(Map<Object, Seq<Object>> topicPartitioMap, TopicType type)
    {
        List<Integer> partitionList = new ArrayList<Integer>();

        if (kafkaConfiguration.getTopic(type).isAllPartitions())
        {
            int size = topicPartitioMap.size();
            for (int i = 0; i < size; i++)
            {
                partitionList.add(i);
            }
        }
        else
        {
            partitionList.addAll(kafkaConfiguration.getTopic(type).getPartitionList());
        }

        System.out.println(partitionList);
        return partitionList;
    }

}
