package one.group.standalone;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class StandAloneUtilsInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(StandAloneUtilsInitiator.class);

    public static void main(final String[] args) throws Exception
    {
        DOMConfigurator.configureAndWatch("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-standalone.xml", "classpath*:**applicationContext-services.xml",
                "classpath*:**applicationContext-DAO.xml", "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml",
                "classpath*:**applicationContext-configuration-PushService.xml");
        logger.info("Sync Server initiated");

        // Old Code - DO NOT TOUCH FOR NOW.
        /*
         * SyncLogDAO syncLogDAO = context.getBean("syncLogDAO",
         * SyncLogDAO.class); SyncUpdateWriteCursorsDAO syncLogWriteCursorsDAO =
         * context.getBean("syncLogWriteCursorsDAO",
         * SyncUpdateWriteCursorsDAO.class); SyncLogProducer syncProducer =
         * context.getBean("syncLogProducer", SimpleSyncLogProducer.class);
         * SyncServer server = new SyncServer();
         * server.initialize(context.getBean("sync.server.properties",
         * Properties.class)); while (true) { // Thread.sleep(1000);
         * logger.debug("Initiating SyncServer"); server.readAndPush(syncLogDAO,
         * syncProducer, syncLogWriteCursorsDAO.findAllCursors()); }
         */
    }

}
