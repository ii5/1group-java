package one.group.standalone;



import java.util.Map;
import java.util.Set;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface SyncHandler
{
    public Map<String,Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException;
}
