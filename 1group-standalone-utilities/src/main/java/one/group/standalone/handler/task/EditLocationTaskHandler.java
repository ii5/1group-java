package one.group.standalone.handler.task;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class EditLocationTaskHandler// implements SyncTaskHandler
{
//    public static final String KEY = formKey();
//
//    private static final Logger logger = LoggerFactory.getLogger(EditLocationTaskHandler.class);
//
//    private AccountDAO accountDAO;
//
//    private LocationService locationService;
//
//    private PropertyListingService propertyListingService;
//
//    private ScoreService scoreService;
//
//    private NewsFeedService newsFeedService;
//
//    private SearchService searchService;
//
//    private AccountRelationService accountRelationService;
//
//    private CacheAction cacheAction;
//
//    private PropertySearchRelationDAO propertySearchRelationDAO;
//
//    public PropertySearchRelationDAO getPropertySearchRelationDAO()
//    {
//        return propertySearchRelationDAO;
//    }
//
//    public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO)
//    {
//        this.propertySearchRelationDAO = propertySearchRelationDAO;
//    }
//
//    public AccountDAO getAccountDAO()
//    {
//        return accountDAO;
//    }
//
//    public void setAccountDAO(AccountDAO accountDAO)
//    {
//        this.accountDAO = accountDAO;
//    }
//
//    public LocationService getLocationService()
//    {
//        return locationService;
//    }
//
//    public void setLocationService(LocationService locationService)
//    {
//        this.locationService = locationService;
//    }
//
//    public PropertyListingService getPropertyListingService()
//    {
//        return propertyListingService;
//    }
//
//    public void setPropertyListingService(PropertyListingService propertyListingService)
//    {
//        this.propertyListingService = propertyListingService;
//    }
//
//    public ScoreService getScoreService()
//    {
//        return scoreService;
//    }
//
//    public void setScoreService(ScoreService scoreService)
//    {
//        this.scoreService = scoreService;
//    }
//
//    public NewsFeedService getNewsFeedService()
//    {
//        return newsFeedService;
//    }
//
//    public void setNewsFeedService(NewsFeedService newsFeedService)
//    {
//        this.newsFeedService = newsFeedService;
//    }
//
//    public SearchService getSearchService()
//    {
//        return searchService;
//    }
//
//    public void setSearchService(SearchService searchService)
//    {
//        this.searchService = searchService;
//    }
//
//    public AccountRelationService getAccountRelationService()
//    {
//        return accountRelationService;
//    }
//
//    public void setAccountRelationService(AccountRelationService accountRelationService)
//    {
//        this.accountRelationService = accountRelationService;
//    }
//
//    public CacheAction getCacheAction()
//    {
//        return cacheAction;
//    }
//
//    public void setCacheAction(CacheAction cacheAction)
//    {
//        this.cacheAction = cacheAction;
//    }
//
//    @Transactional
//    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
//    {
//        String entityId = record.getAssociatedEntityId();
//        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
//
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("account_id", entityId);
//
//        WSSyncUpdate update = new WSSyncUpdate();
//        update.setEntity(record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class));
//        update.setType(SyncDataType.INVALIDATION);
//
//        addToMap(response, entityId, update);
//
//        WSEntityReference ref = new WSEntityReference(HintType.DELETE, EntityType.NEWS_FEED, null);
//        update = new WSSyncUpdate(SyncDataType.INVALIDATION);
//        update.setEntity(ref);
//
//        addToMap(response, entityId, update);
//
//        Account account = accountDAO.fetchAccountById(entityId);
//        List<Location> matchingLocations = new ArrayList<Location>();
//        List<Location> exactMatchingLocations = new ArrayList<Location>();
//        List<Search> searchList = searchService.fetchSearchByAccount(account.getId());
//        List<String> searchLocations = new ArrayList<String>();
//
//        if (account.getLocations() != null)
//        {
//            matchingLocations.addAll(account.getLocations());
//            for (Location location : account.getLocations())
//            {
//                matchingLocations.addAll(locationService.getMatchingLocation(location, true));
//            }
//        }
//
//        exactMatchingLocations.addAll(matchingLocations);
//
//        for (Search search : searchList)
//        {
//            if (!searchLocations.contains(search.getLocationId()))
//            {
//                searchLocations.add(search.getLocationId());
//            }
//        }
//
//        matchingLocations.addAll(locationService.getMatchingAndNearByLocation(searchLocations, true));
//
//        cacheAction.invalidate(NewsFeedKey.SORTED_SET_NEWS_FEED.getFormedKey(values));
//
//        List<PropertyListingScoreObject> propertyScoreObjectList = fetchPropertyListingsBasedOnLocation(matchingLocations);
//
//        processNewsFeed(account, response, searchService.fetchPropertyAgainstSearchMapForAccount(account.getId()), exactMatchingLocations, propertyScoreObjectList, true);
//
//        return response;
//    }
//
//    private List<PropertyListingScoreObject> fetchPropertyListingsBasedOnLocation(List<Location> locationList)
//    {
//        List<PropertyListingScoreObject> propertyListingList = propertyListingService.searchPropertyListing(locationList, BroadcastStatus.ACTIVE);
//        return propertyListingList;
//    }
//
//    private Map<String, Set<ResponseEntity>> processNewsFeed(Account account, Map<String, Set<ResponseEntity>> response, Map<PropertyListingScoreObject, List<Search>> propertyVsSearchMap,
//            List<Location> exactMatchingLocations, List<PropertyListingScoreObject> propertyScoreObjectList, boolean skipPushUpdate) throws General1GroupException
//    {
//        List<PropertyListingScoreObject> propertyListingsOfContactList = new ArrayList<PropertyListingScoreObject>();
//        Map<String, WSAccountRelations> otherAccountIdVsAccountRelationMap = new HashMap<String, WSAccountRelations>();
//
//        propertyListingsOfContactList.addAll(propertyScoreObjectList);
//        // Add property listings matching search of an account.
//        propertyListingsOfContactList.addAll(propertyVsSearchMap.keySet());
//
//        // List<Search> searchList =
//        // searchService.fetchSearchByAccount(accountId);
//        List<String> matchingSearchIdList = new ArrayList<String>();
//        int recentSearchCount = 0;
//        Calendar c = Calendar.getInstance();
//        c.set(1984, Calendar.NOVEMBER, 21);
//        Date mostRecentSearch = c.getTime();
//        Date latestMatchingSavedSearchTime = c.getTime();
//
//        otherAccountIdVsAccountRelationMap.putAll(accountRelationService.fetchAccountRelationsAsMap(account.getId()));
//
//        if (!otherAccountIdVsAccountRelationMap.isEmpty())
//        {
//            propertyListingsOfContactList.addAll(propertyListingService.fetchAllPropertyListingsByAccountIds(new ArrayList<String>(otherAccountIdVsAccountRelationMap.keySet()),
//                    exactMatchingLocations, BroadcastStatus.ACTIVE));
//        }
//
//        for (PropertyListingScoreObject propertyListing : propertyListingsOfContactList)
//        {
//            List<Search> searchList = propertyVsSearchMap.get(propertyListing);
//            mostRecentSearch = c.getTime();
//            latestMatchingSavedSearchTime = c.getTime();
//            recentSearchCount = 0;
//            matchingSearchIdList.clear();
//            if (searchList != null)
//            {
//                for (Search search : searchList)
//                {
//                    if (!Utils.isEmpty(search.getRequirementName()))
//                    {
//                        matchingSearchIdList.add(search.getId());
//                        latestMatchingSavedSearchTime = search.getSearchTime().after(latestMatchingSavedSearchTime) ? search.getSearchTime() : latestMatchingSavedSearchTime;
//                    }
//                    else
//                    {
//                        recentSearchCount++;
//                        mostRecentSearch = search.getSearchTime().after(mostRecentSearch) ? search.getSearchTime() : mostRecentSearch;
//                    }
//                }
//            }
//
//            WSAccountRelations accountRelation = otherAccountIdVsAccountRelationMap.get(propertyListing.getAccountId());
//            RelevantDataObject accountRelevantData = new RelevantDataObject();
//            accountRelevantData.setRelevantAccountId(propertyListing.getAccountId());
//
//            accountRelevantData.setLatestMatchingSavedSearchTime(latestMatchingSavedSearchTime.getTime());
//            accountRelevantData.setLatestRecentSearchTime(mostRecentSearch.getTime());
//            accountRelevantData.setMatchingRecentSearchCount(recentSearchCount);
//            accountRelevantData.setMatchingSavedSearchList(matchingSearchIdList);
//
//            for (Location l : account.getLocations())
//            {
//                if (l.getId().equals(propertyListing.getLocationId()))
//                {
//                    accountRelevantData.setPropertyInSameLocation(true);
//                    break;
//                }
//            }
//
//            if (propertyListing.getAccountId().equals(account.getId()))
//            {
//                accountRelevantData.setPostingAccount(true);
//            }
//
//            if (accountRelation != null)
//            {
//                accountRelevantData.setIsBlocked(accountRelation.getIsBlockedBy());
//                accountRelevantData.setIsBlockedBy(accountRelation.getIsBlocked());
//                accountRelevantData.setIsContact(accountRelation.getIsContactOf());
//                accountRelevantData.setIsContactOf(accountRelation.getIsContactOf());
//                accountRelevantData.setScore(accountRelation.getScoredAs());
//                accountRelevantData.setScoredAs(accountRelation.getScore());
//            }
//
////            float score = scoreService.getNewsFeedScore(accountRelevantData, propertyListing);
////            accountRelevantData.setRelevanceScore(score);
////
////            if (score >= ScoreConstants.MINIMUM_NEWS_FEED_INCLUSION_SCORE)
////            {
////                newsFeedService.saveNewsFeed(account.getId(), propertyListing.getId(), propertyListing.getAccountId(), score, "" + propertyListing.getCreationTime().getTime());
////                if (!skipPushUpdate)
////                {
////                    WSNewsFeed wsNewsFeed = new WSNewsFeed(propertyListing.getId(), propertyListing.getAccountId(), score, "" + propertyListing.getCreationTime().getTime());
////                    WSSyncUpdate update = new WSSyncUpdate();
////                    update.setNewsFeed(wsNewsFeed);
////                    update.setType(SyncDataType.NEWS);
////                    addToMap(response, account.getId(), update);
////                }
////            }
//
//        }
//
//        return response;
//    }
//
//    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
//    {
//        if (syncUpdateMap.containsKey(accountId))
//        {
//            syncUpdateMap.get(accountId).add(update);
//        }
//        else
//        {
//            // To preserve insertion order
//            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
//            updateSet.add(update);
//            syncUpdateMap.put(accountId, updateSet);
//        }
//    }
//
//    private static String formKey()
//    {
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("action", SyncActionType.EDIT_LOCATION.toString());
//        values.put("entityType", EntityType.ACCOUNT.toString());
//        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
//        return key;
//    }
}
