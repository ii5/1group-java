package one.group.standalone.handler.task;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class NewsFeedTaskHandler implements SyncTaskHandler
{
//    public static final String KEY = formKey();
//
//    private static final Logger logger = LoggerFactory.getLogger(NewsFeedTaskHandler.class);
//
//    private NewsFeedService newsFeedService;
//
//    public NewsFeedService getNewsFeedService()
//    {
//        return newsFeedService;
//    }
//
//    public void setNewsFeedService(NewsFeedService newsFeedService)
//    {
//        this.newsFeedService = newsFeedService;
//    }

    public Map<String,Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
        
        return response;
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.FETCH.toString());
        values.put("entityType", EntityType.NEWS_FEED.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
