package one.group.standalone.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class CloseExpirePropertyListingTaskHandler implements SyncTaskHandler
{
    public static final String AUTO_CLOSE_KEY = autoCloseFormKey();

    public static final String AUTO_EXPIRE_KEY = autoExpireFormKey();

    public static final String MANUAL_CLOSE_KEY = manualCloseFormKey();

    public static final String RENEW_KEY = renewFormKey();

    public static final String EDIT_INPLACE_KEY = editInplaceFormKey();

    private SubscriptionService subscriptionService;

    private SyncDBService syncDBService;

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    /**
     * Handler to return map of subscribed account ids with Sync Update
     * 
     * @return {@link Map}
     */
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Validation.notNull(record, "Record passed cannot be null");
        Map<String, Set<ResponseEntity>> map = new HashMap<String, Set<ResponseEntity>>();
        EntityType type = record.getAssociatedEntityType();
        // get all subscribed accounts
        List<String> asscociatedAccountIds = subscriptionService.retreiveAllSubscribersOf(record.getAssociatedEntityId(), type);
        Set<ResponseEntity> responseEntity = new HashSet<ResponseEntity>();
        // prepare a map for each account and respective sync update
        for (String accountId : asscociatedAccountIds)
        {
            WSEntityReference wsEntityReference = new WSEntityReference(HintType.FETCH, type, record.getAssociatedEntityId());
            WSSyncUpdate syncUpdate = new WSSyncUpdate();
            syncUpdate.setEntity(wsEntityReference);
            syncUpdate.setType(record.getType());
            responseEntity.add(syncUpdate);
            map.put(accountId, responseEntity);
        }
        return map;
    }

    /**
     * Generate auto close key for sync handler
     * 
     * @return
     */
    private static String autoCloseFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.AUTO_CLOSE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate auto expire key for sync handler
     * 
     * @return
     */
    private static String autoExpireFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.AUTO_EXPIRE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate manual close key for sync handler
     * 
     * @return
     */
    private static String manualCloseFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.MANUAL_CLOSE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate renew action key for sync handler
     * 
     * @return
     */
    private static String renewFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.RENEW.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate Edit in place key for sync handler
     * 
     * @return
     */
    private static String editInplaceFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT_INPLACE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
