package one.group.standalone.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class GeneralSyncTaskHandler implements SyncTaskHandler
{
    private SubscriptionService subscriptionService;

    public static final String EDIT_ACCOUNTS_KEY = formEditAccountsKey();

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Validation.notNull(record, "The SyncLogEntry should not be null.");
        Map<String, Set<ResponseEntity>> accountVsUpdateMap = new HashMap<String, Set<ResponseEntity>>();

        List<String> subscribedAccountIds = subscriptionService.retreiveAllSubscribersOf(record.getAssociatedEntityId(), record.getAssociatedEntityType());
        WSEntityReference entityReference = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class);
        WSSyncUpdate update = new WSSyncUpdate();
        update.setEntity(entityReference);
        update.setType(record.getType());

        WSSyncUpdate updateOwn = new WSSyncUpdate(SyncDataType.INVALIDATION);
        updateOwn.setEntity(new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT, record.getAssociatedEntityId()));
        addToMap(accountVsUpdateMap, record.getAssociatedEntityId(), updateOwn);

        for (String subscribedAccountId : subscribedAccountIds)
        {
            addToMap(accountVsUpdateMap, subscribedAccountId, update);
        }

        return accountVsUpdateMap;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new HashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formEditAccountsKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT.toString());
        values.put("entityType", EntityType.ACCOUNT.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
