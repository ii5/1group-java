package one.group.standalone.handler.task;

public class SyncAccountRelationTaskHandler //implements SyncTaskHandler
{
//    public static final String SYNC_ACCOUNT_KEY = syncAccountFormKey();
//
//    public static final String SYNC_ACCOUNT_RELATIONS_KEY = syncAccountRelationsFormKey();
//
//    public static final String REGISTER_ACCOUNT_KEY = registerAccountFormKey();
//
//    public static final String REGISTER_ACCOUNT_RELATIONS_KEY = registerAccountRelationsFormKey();
//
//    private static final Logger logger = LoggerFactory.getLogger(SyncAccountRelationTaskHandler.class);
//
//    private AccountDAO accountDAO;
//
//    private SyncDBService syncDBService;
//
//    private SubscriptionService subscriptionService;
//
//    private NewsFeedService newsFeedService;
//
//    private LocationMatchDAO locationMatchDAO;
//
//    private PropertyListingDAO propertyListingDAO;
//
//    private CacheAction cacheAction;
//
//    private AccountRelationService accountRelationService;
//
//    private PropertyListingService propertyListingService;
//
//    private ScoreService scoreService;
//
//    private LocationService locationService;
//
//    private SearchService searchService;
//
//    public SearchService getSearchService()
//    {
//        return searchService;
//    }
//
//    public void setSearchService(SearchService searchService)
//    {
//        this.searchService = searchService;
//    }
//
//    public LocationService getLocationService()
//    {
//        return locationService;
//    }
//
//    public void setLocationService(LocationService locationService)
//    {
//        this.locationService = locationService;
//    }
//
//    public ScoreService getScoreService()
//    {
//        return scoreService;
//    }
//
//    public void setScoreService(ScoreService scoreService)
//    {
//        this.scoreService = scoreService;
//    }
//
//    public PropertyListingService getPropertyListingService()
//    {
//        return propertyListingService;
//    }
//
//    public void setPropertyListingService(PropertyListingService propertyListingService)
//    {
//        this.propertyListingService = propertyListingService;
//    }
//
//    public PropertyListingDAO getPropertyListingDAO()
//    {
//        return propertyListingDAO;
//    }
//
//    public void setPropertyListingDAO(PropertyListingDAO propertyListingDAO)
//    {
//        this.propertyListingDAO = propertyListingDAO;
//    }
//
//    public LocationMatchDAO getLocationMatchDAO()
//    {
//        return locationMatchDAO;
//    }
//
//    public void setLocationMatchDAO(LocationMatchDAO locationMatchDAO)
//    {
//        this.locationMatchDAO = locationMatchDAO;
//    }
//
//    public AccountDAO getAccountDAO()
//    {
//        return accountDAO;
//    }
//
//    public void setAccountDAO(AccountDAO accountDAO)
//    {
//        this.accountDAO = accountDAO;
//    }
//
//    public SyncDBService getSyncDBService()
//    {
//        return syncDBService;
//    }
//
//    public void setSyncDBService(SyncDBService syncDBService)
//    {
//        this.syncDBService = syncDBService;
//    }
//
//    public NewsFeedService getNewsFeedService()
//    {
//        return newsFeedService;
//    }
//
//    public void setNewsFeedService(NewsFeedService newsFeedService)
//    {
//        this.newsFeedService = newsFeedService;
//    }
//
//    public CacheAction getCacheAction()
//    {
//        return cacheAction;
//    }
//
//    public void setCacheAction(CacheAction cacheAction)
//    {
//        this.cacheAction = cacheAction;
//    }
//
//    public SubscriptionService getSubscriptionService()
//    {
//        return subscriptionService;
//    }
//
//    public void setSubscriptionService(SubscriptionService subscriptionService)
//    {
//        this.subscriptionService = subscriptionService;
//    }
//
//    public AccountRelationService getAccountRelationService()
//    {
//        return accountRelationService;
//    }
//
//    public void setAccountRelationService(AccountRelationService accountRelationService)
//    {
//        this.accountRelationService = accountRelationService;
//    }
//
//    @Transactional
//    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
//    {
//        // To preserve insertion order
//        Set<ResponseEntity> responseEntityList = new LinkedHashSet<ResponseEntity>();
//        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
//
//        String accountId = record.getAssociatedEntityId();
//        Account account = accountDAO.fetchAccountById(accountId);
//
//        List<Location> matchingLocations = new ArrayList<Location>();
//
//        if (account.getLocations() != null)
//        {
//            for (Location location : account.getLocations())
//            {
//                matchingLocations.add(location);
//                matchingLocations.addAll(locationService.getMatchingLocation(location, true));
//            }
//        }
//
//        if (record.getAssociatedEntityType().equals(EntityType.ACCOUNT_RELATION))
//        {
//            WSContactsInfo contactsInfo = record.getAdditionalDataAs(SyncEntryKey.CONTACTS, WSContactsInfo.class);
//            if (contactsInfo != null)
//            {
//                Boolean isInitialContactsSyncFlagFlipped = contactsInfo.isContactsSyncFlagFlipped();
//                Set<String> otherDeltaAccountIds = contactsInfo.getDeltaContacts();
//                if (isInitialContactsSyncFlagFlipped)
//                {// Only happens once for an account,
//                    List<String> otherAccountIds = new ArrayList<String>();
//                    Map<String, String> values = new HashMap<String, String>();
//                    values.put("account_id", accountId);
//                    cacheAction.invalidate(NewsFeedKey.SORTED_SET_NEWS_FEED.getFormedKey(values));
//                    Set<WSAccountRelations> relations = accountRelationService.fetchContactsOfAccount(accountId);
//
//                    WSEntityReference ref = new WSEntityReference(HintType.DELETE, EntityType.NEWS_FEED, null);
//                    WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
//                    update.setEntity(ref);
//
//                    addToMap(response, accountId, update);
//
//                    for (WSAccountRelations relation : relations)
//                    {
//                        String otherAccountId = relation.getContactId();
//                        if (accountId != null && otherAccountId != null && !accountId.equals(otherAccountId))
//                        {
//                            otherAccountIds.add(otherAccountId);
//                        }
//                    }
//                    response = processNewsFeed(account, otherAccountIds, response, responseEntityList, matchingLocations, false, true);
//                }
//                else if (account.isInitialContactsSyncComplete())
//                {
//                    response = processNewsFeed(account, otherDeltaAccountIds, response, responseEntityList, matchingLocations, false, false);
//                }
//
//                for (String otherAccountId : otherDeltaAccountIds)
//                {
//                    if (accountId != null && otherAccountId != null && !accountId.equals(otherAccountId))
//                    {
//                        WSAccountRelations relation = accountRelationService.fetchAccountRelation(accountId, otherAccountId);
//                        response = generateSyncUpdatesForContactRelations(relation, response, responseEntityList, accountId, otherAccountId);
//                        if (relation.getIsContact())
//                            subscriptionService.subscribe(accountId, otherAccountId, EntityType.ACCOUNT);
//                        else if (relation.getIsContactOf())
//                            subscriptionService.subscribe(otherAccountId, accountId, EntityType.ACCOUNT);
//                    }
//                }
//            }
//        }
//        return response;
//    }
//
//    private Map<String, Set<ResponseEntity>> generateSyncUpdatesForContactRelations(WSAccountRelations relation, Map<String, Set<ResponseEntity>> response, Set<ResponseEntity> responseEntityList,
//            String accountId, String otherAccountId)
//    {
//        if (relation != null)
//        {// UniDirectional A-->C
//            if (relation.getIsContact())
//            {
//                WSSyncUpdate update = new WSSyncUpdate();
//                WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT_RELATION, otherAccountId);
//                update.setEntity(entityReference);
//                update.setType(SyncDataType.INVALIDATION);
//                addToMap(response, accountId, update);
//            }
//            else if (relation.getIsContactOf())
//            {
//                WSSyncUpdate update = new WSSyncUpdate();
//                WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT_RELATION, accountId);
//                update.setEntity(entityReference);
//                update.setType(SyncDataType.INVALIDATION);
//                addToMap(response, otherAccountId, update);
//            }
//        }
//        return response;
//    }
//
//    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
//    {
//        if (syncUpdateMap.containsKey(accountId))
//        {
//            syncUpdateMap.get(accountId).add(update);
//        }
//        else
//        {
//            // To preserve insertion order
//            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
//            updateSet.add(update);
//            syncUpdateMap.put(accountId, updateSet);
//        }
//    }
//
//    private Map<String, Set<ResponseEntity>> processNewsFeed(Account account, Collection<String> otherAccountIds, Map<String, Set<ResponseEntity>> response, Set<ResponseEntity> responseEntityList,
//            List<Location> matchingLocations, boolean skipNewsFeed, boolean firstContactSyncComplete) throws General1GroupException
//    {
//        List<PropertyListingScoreObject> propertyListingsList = new ArrayList<PropertyListingScoreObject>();
//        Map<String, WSAccountRelations> otherAccountIdVsAccountRelationMap = new HashMap<String, WSAccountRelations>();
//        Map<PropertyListingScoreObject, List<Search>> propertyAgainstSearchMap = searchService.fetchPropertyAgainstSearchMapForAccount(account.getId());
//        List<String> matchingSearchIdList = new ArrayList<String>();
//        List<String> matchingLocationIds = new ArrayList<String>();
//        int recentSearchCount = 0;
//        Calendar c = Calendar.getInstance();
//        c.set(1984, Calendar.NOVEMBER, 21);
//        Date mostRecentSearch = c.getTime();
//        Date latestMatchingSavedSearchTime = c.getTime();
//
//        for (Location l : matchingLocations)
//        {
//            matchingLocationIds.add(l.getId());
//        }
//
//        if (firstContactSyncComplete)
//        {
//            List<PropertyListingScoreObject> propertListingOfMatchingLocations = propertyListingService.searchPropertyListing(matchingLocations, BroadcastStatus.ACTIVE);
//            propertyListingsList.addAll(propertListingOfMatchingLocations);
//        }
//
//        if (otherAccountIds != null && !otherAccountIds.isEmpty())
//        {
//            // Add properties of Contacts
//            propertyListingsList.addAll(propertyListingService.fetchAllPropertyListingsByAccountIds(new ArrayList<String>(otherAccountIds), matchingLocations, BroadcastStatus.ACTIVE));
//            otherAccountIdVsAccountRelationMap.putAll(accountRelationService.fetchAccountRelationsAsMap(account.getId(), new ArrayList<String>(otherAccountIds)));
//        }
//
//        // Adding properties that matches search for an account.
//        propertyListingsList.addAll(propertyAgainstSearchMap.keySet());
//
//        for (PropertyListingScoreObject propertyListing : propertyListingsList)
//        {
//            List<Search> searchList = propertyAgainstSearchMap.get(propertyListing);
//            mostRecentSearch = c.getTime();
//            latestMatchingSavedSearchTime = c.getTime();
//            recentSearchCount = 0;
//            matchingSearchIdList.clear();
//
//            if (searchList != null)
//            {
//                for (Search search : searchList)
//                {
//                    if (!Utils.isEmpty(search.getRequirementName()))
//                    {
//                        matchingSearchIdList.add(search.getId());
//                        latestMatchingSavedSearchTime = search.getSearchTime().after(latestMatchingSavedSearchTime) ? search.getSearchTime() : latestMatchingSavedSearchTime;
//                    }
//                    else
//                    {
//                        recentSearchCount++;
//                        mostRecentSearch = search.getSearchTime().after(mostRecentSearch) ? search.getSearchTime() : mostRecentSearch;
//                    }
//                }
//            }
//
//            WSAccountRelations accountRelation = otherAccountIdVsAccountRelationMap.get(propertyListing.getAccountId());
//            RelevantDataObject accountRelevantData = new RelevantDataObject();
//            accountRelevantData.setRelevantAccountId(propertyListing.getAccountId());
//
//            accountRelevantData.setLatestRecentSearchTime(mostRecentSearch.getTime());
//            accountRelevantData.setMatchingRecentSearchCount(recentSearchCount);
//            accountRelevantData.setMatchingSavedSearchList(matchingSearchIdList);
//            accountRelevantData.setLatestMatchingSavedSearchTime(latestMatchingSavedSearchTime.getTime());
//
//            if (propertyListing.getAccountId().equals(account.getId()))
//            {
//                accountRelevantData.setPostingAccount(true);
//            }
//
//            if (matchingLocationIds.contains(propertyListing.getLocationId()))
//            {
//                accountRelevantData.setPropertyInSameLocation(true);
//            }
//
//            if (accountRelation != null)
//            {
//                accountRelevantData.setIsBlocked(accountRelation.getIsBlockedBy());
//                accountRelevantData.setIsBlockedBy(accountRelation.getIsBlocked());
//                accountRelevantData.setIsContact(accountRelation.getIsContactOf());
//                accountRelevantData.setIsContactOf(accountRelation.getIsContactOf());
//                accountRelevantData.setScore(accountRelation.getScoredAs());
//                accountRelevantData.setScoredAs(accountRelation.getScore());
//            }
//
//            float score = scoreService.getNewsFeedScore(accountRelevantData, propertyListing);
//            accountRelevantData.setRelevanceScore(score);
//
//            logger.debug("Complete: " + firstContactSyncComplete + ", RD:" + accountRelevantData + ", PL:" + propertyListing.getId());
//            if (score >= ScoreConstants.MINIMUM_NEWS_FEED_INCLUSION_SCORE)
//            {
//                newsFeedService.saveNewsFeed(account.getId(), propertyListing.getId(), propertyListing.getAccountId(), score, "" + propertyListing.getCreationTime().getTime());
//                if (!skipNewsFeed)
//                {
//                    WSNewsFeed wsNewsFeed = new WSNewsFeed(propertyListing.getId(), propertyListing.getAccountId(), score, "" + propertyListing.getCreationTime().getTime());
//                    WSSyncUpdate update = new WSSyncUpdate();
//                    update.setNewsFeed(wsNewsFeed);
//                    update.setType(SyncDataType.NEWS);
//                    addToMap(response, account.getId(), update);
//                }
//            }
//
//        }
//
//        return response;
//    }
//
//    /**
//     * Generate sync account key for sync handler
//     * 
//     * @return
//     */
//    private static String syncAccountFormKey()
//    {
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("action", SyncActionType.SYNC_CONTACTS.toString());
//        values.put("entityType", EntityType.ACCOUNT.toString());
//        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
//        return key;
//    }
//
//    /**
//     * Generate sync account relations key for sync handler
//     * 
//     * @return
//     */
//    private static String syncAccountRelationsFormKey()
//    {
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("action", SyncActionType.SYNC_CONTACTS.toString());
//        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
//        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
//        return key;
//    }
//
//    /**
//     * Generate register account key for sync handler
//     * 
//     * @return
//     */
//    private static String registerAccountFormKey()
//    {
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("action", SyncActionType.REGISTER.toString());
//        values.put("entityType", EntityType.ACCOUNT.toString());
//        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
//        return key;
//    }
//
//    /**
//     * Generate register account relations key for sync handler
//     * 
//     * @return
//     */
//    private static String registerAccountRelationsFormKey()
//    {
//        Map<String, String> values = new HashMap<String, String>();
//        values.put("action", SyncActionType.REGISTER.toString());
//        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
//        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
//        return key;
//    }

}
