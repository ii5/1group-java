package one.group.standalone.handler.task;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class PropertyListingSearchTaskHandler implements SyncTaskHandler
{
    private SubscriptionService subscriptionService;

    public static final String ADD_SEARCH_KEY = formAddSearchKey();
    public static final String DELETE_SEARCH_KEY = formDeleteSearchKey();
    public static final String EDIT_SEARCH_KEY = formEditSearchKey();

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Validation.notNull(record, "The SyncLogEntry should not be null.");
        Map<String, Set<ResponseEntity>> accountVsUpdateMap = new HashMap<String, Set<ResponseEntity>>();

        WSRequest wsRequest = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSRequest.class);

//        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING_SEARCH, wsRequest.getSearch().getSearchId());
//        if (record.getAction().equals(SyncActionType.DELETE))
//        {
//            entityReference.setType(HintType.DELETE);
//        }
//        WSSyncUpdate update = new WSSyncUpdate();
//        update.setType(SyncDataType.PROPERTY_LISTING_SEARCH);
//        update.setEntity(entityReference);
//        update.setType(record.getType());
//
//        Set<ResponseEntity> responseEntity = new HashSet<ResponseEntity>();
//        responseEntity.add(update);

//        String accountId = wsRequest.getSearch().getAccountId();
//        accountVsUpdateMap.put(accountId, responseEntity);

        return accountVsUpdateMap;
    }

    private static String formAddSearchKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.ADD.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING_SEARCH.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    private static String formDeleteSearchKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.DELETE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING_SEARCH.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    private static String formEditSearchKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING_SEARCH.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
