package one.group.utils.validation;

import com.movein.core.Constant;
import one.group.utils.validation.exception.ValidationException;

/**
 * 
 * 
 * @author nyalfernandes
 * 
 * TODO : Define mobile number validations.
 *
 */
public class Validation 
{
	public static boolean validateMobileNumber(String number)
	{
		return false;
	}
	
	public static void notNull(Object o, String message)
	{
	    if (o == null)
	    {
	        String msg;
	        msg = message == null ? Constant.MSG_NULL_OBJ : message;
	        throw new ValidationException(msg, new NullPointerException(msg));
	    }
	}
	
	public static void isTrue(boolean isTrue, String message)
	{
	    if (!isTrue)
	    {
	        String msg;
            msg = message == null ? Constant.MSG_NOT_TRUE : message;
            throw new ValidationException(msg, new IllegalStateException(msg));
	    }
	}
	
	public static void isFalse(boolean isFalse, String message)
    {
        if (!isFalse)
        {
            String msg;
            msg = message == null ? Constant.MSG_NOT_FALSE : message;
            throw new ValidationException(msg, new IllegalStateException(msg));
        }
    }
}
