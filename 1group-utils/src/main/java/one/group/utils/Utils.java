package one.group.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import one.group.core.ClientVersion;
import one.group.core.Constant;
import one.group.core.enums.ClientType;
import one.group.core.enums.ClientUpdateType;
import one.group.core.enums.EntityType;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.SyncDataType;
import one.group.utils.validation.Validation;
import one.group.utils.validation.exception.ValidationException;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Utility methods for the project.
 * 
 * TODO: Document precisely.
 * 
 * @author nyalfernandes
 * 
 */
public class Utils
{

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    public static final String BEARER_TYPE = "Bearer";

    /**
     * Replaces placeholders present in the format <code>[replaceString]</code>
     * in key with respective values in <code>values</code>.
     * 
     * @param key
     * @param values
     * @return
     */
    public static String populateKey(String key, Map<String, String> values)
    {
        Validation.notNull(key, "Key passed should not be null.");
        Validation.notNull(values, "Value map passed should not be null.");
        for (Entry<String, String> entry : values.entrySet())
        {
            if (entry.getValue() != null)
            {
                key = key.replace("[" + entry.getKey() + "]", entry.getValue());
            }
        }

        return key;
    }

    /**
     * Method checks if there are unreplaced place holders in the key. Searches
     * for pattern [*].
     * 
     * @param key
     * @return true if there pattern [*] is found in <code>key</code>
     */
    public static boolean unreplacedPlaceHoldersInKey(String key)
    {
        return Pattern.matches(Constant.REGEX_PLACEHOLDER, key);
    }

    /**
     * Checks if the Object passed is null.
     * 
     * @param o
     * @return
     */
    public static boolean isNull(Object o)
    {
        return o == null;
    }

    /**
     * Checks if the String passed is null or empty.
     * 
     * @param s
     * @return
     */
    public static boolean isNullOrEmpty(String s)
    {
        return isNull(s) || isEmpty(s);
    }

    /**
     * Checks if the string passed is null or empty.
     * 
     * @param s
     * @return
     */
    public static boolean isEmpty(String s)
    {
        return isNull(s) || s.trim().isEmpty();
    }

    /**
     * 
     * @param o
     * @return
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws IOException
     */
    public static String getJsonString(Object o)
    {
        String jsonString = "";
        try
        {
            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
            mapper.setSerializationInclusion(Include.NON_NULL);
            mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
            mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
            mapper.enable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
            mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

            jsonString = mapper.writeValueAsString(o);
        }
        catch (Exception e)
        {
            logger.error("Something went wrong while serializing into JSon.", e);
            jsonString = "Something went wrong while serializing into JSon.";
        }

        return jsonString;
    }

    public static String getJsonStringForBPO(Object o)
    {
        String jsonString = "";
        try
        {
            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
            // mapper.setSerializationInclusion(Include.NON_NULL);
            mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
            mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
            mapper.enable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
            mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);

            jsonString = mapper.writeValueAsString(o);
        }
        catch (Exception e)
        {
            logger.error("Something went wrong while serializing into JSon.", e);
            jsonString = "Something went wrong while serializing into JSon.";
        }

        return jsonString;
    }

    /**
     * 
     * @param jsonString
     * @param c
     * @return
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws IOException
     */
    public static Object getInstanceFromJson(String jsonString, Class c) throws JsonMappingException, JsonGenerationException, IOException
    {
        mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, false);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return mapper.readValue(jsonString, c);
    }

    public static Object getInstanceFromJson(String jsonString, TypeReference type) throws JsonMappingException, JsonGenerationException, IOException
    {
        mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        return mapper.readValue(jsonString, type);
    }

    /**
     * 
     * @return
     */
    public static String randomUUID()
    {
        return UUID.randomUUID().toString();
    }

    /**
     * Generates a random string using secure random depending upon character
     * sets.
     * 
     * Thanks to http://ideone.com/xvIZcd
     * 
     * Concerns:
     * http://www.cigital.com/justice-league-blog/2014/01/06/issues-when
     * -using-java-securerandom/
     * 
     * @param charset
     * @param length
     * @param excludeInitialZero
     *            TODO
     * @return
     */
    public static String randomString(char[] charset, int length, boolean excludeInitialZero)
    {
        Random random = new SecureRandom();
        char[] result = new char[length];

        for (int i = 0; i < result.length; i++)
        {
            int randomIndex = random.nextInt(charset.length);

            if (i == 0 && excludeInitialZero && charset[randomIndex] == '0')
            {
                char[] trimmedChar = Constant.CHARSET_NUMERIC_WITHOUT_ZERO;
                randomIndex = random.nextInt(trimmedChar.length);
                result[i] = trimmedChar[randomIndex];
            }
            else
            {
                result[i] = charset[randomIndex];
            }
        }

        return new String(result);
    }

    /**
     * Returns the total Topic partitions required.
     * 
     * @return
     */
    public static int kafkaTopicPartitionCount()
    {
        int count = 0;
        count = count + SyncDataType.values().length;

        return count;
    }

    /**
     * Returns the auth token from the header if present, null otherwise.
     * 
     * @param httpHeaders
     * @return
     */
    public static String getAuthTokenFromHeader(HttpHeaders httpHeaders)
    {
        if (httpHeaders == null)
        {
            return "";
        }

        List<String> headers = httpHeaders.getRequestHeader("authorization");
        String token = null;
        for (String authHeader : headers)
        {
            if ((authHeader.toLowerCase().startsWith(BEARER_TYPE.toLowerCase())))
            {
                token = authHeader.substring(BEARER_TYPE.length()).trim();
            }
        }
        return token;
    }

    public static String getAuthTokenFromHeaderString(String headerValue)
    {
        if (headerValue == null)
        {
            return null;
        }

        if ((headerValue.toLowerCase().startsWith(BEARER_TYPE.toLowerCase())))
        {
            headerValue = headerValue.substring(BEARER_TYPE.length()).trim();
        }

        return headerValue;
    }

    /**
     * Method generates chat thread id based on accountIdOne & accountIdTwo.
     * 
     * where accountIdOne < accountIdTwo
     * 
     * ex: chat-thread-accountIdOne-accontIdTwo
     * 
     * @param accountIdOne
     * @param accountTwo
     * @param chatKey
     * @return
     */
    public static String generateChatThreadId(String accountIdOne, String accountIdTwo, String chatKey)
    {
        Map<String, String> accountIds = new HashMap<String, String>();
        accountIds.put("account_id_one", accountIdOne);
        accountIds.put("account_id_two", accountIdTwo);
        int compare = accountIdOne.compareTo(accountIdTwo);
        if (compare > 0)
        {
            accountIds.put("account_id_one", accountIdTwo);
            accountIds.put("account_id_two", accountIdOne);
        }

        return Utils.populateKey(chatKey, accountIds);
    }

    /**
     * Method generates system time in milliseconds
     * 
     * @return
     */
    public static long getSystemTime()
    {
        return System.currentTimeMillis();
    }

    /**
     * 
     * @param offset
     * @param limit
     * @param totalRecords
     * @return
     */
    public static Map<String, Integer> validateOffsetLimit(int offset, int limit, int totalRecords)
    {
        Map<String, Integer> map = new HashMap<String, Integer>();

        int start = totalRecords - Constant.PAGE_DEFAULT_LIMIT;
        int end = totalRecords;

        if (isNull(limit))
            limit = -Constant.PAGE_DEFAULT_LIMIT;

        if (isNull(offset))
            offset = totalRecords;

        if (offset < 0 || offset > Integer.MAX_VALUE || offset > totalRecords)
        {
            throw new ValidationException("Invalid offset:" + offset);
        }
        else if (limit <= -Constant.PAGE_DEFAULT_MAX_LIMIT || limit >= Constant.PAGE_DEFAULT_MAX_LIMIT || limit > totalRecords)
        {
            throw new ValidationException("Invalid limit: " + limit);
        }

        // Fetch records in positive direction
        if (limit > 0)
        {
            start = offset;
            end = limit;
        }

        // Fetch records in negative direction
        if (limit < 0)
        {
            start = limit + offset + 1;
            end = offset;
        }

        if (start < 0)
            start = 0;

        if (end >= totalRecords)
            end = totalRecords;

        logger.info("limit: " + limit + ", offset: " + offset + ", totalRecords:" + totalRecords);
        logger.info("start: " + start + ", end: " + end);

        map.put("start", start);
        map.put("end", end);
        return map;
    }

    /**
     * 
     * @param offsetStr
     * @param limitStr
     * @param totalRecords
     * @return
     */
    public static Map<String, Integer> getOffsetAndLimit1(String offsetStr, String limitStr, int totalRecords)
    {
        Map<String, Integer> map = new HashMap<String, Integer>();

        int start;
        int end;
        int offset;
        int limit;

        if (isNull(offsetStr) == true)
        {
            start = totalRecords - Constant.REDIS_MAX_RESULTS + 1;
            end = totalRecords;
            if (!isNull(limitStr))
            {
                limit = Integer.valueOf(limitStr);
                if (limit > 0)
                {
                    start = totalRecords;
                    end = totalRecords + limit;
                }
                else
                {
                    start = totalRecords + limit + 1;
                    end = totalRecords;
                }
            }
        }
        else
        {

            offset = Integer.valueOf(offsetStr);
            if (offset > totalRecords)
            {
                offset = totalRecords;
            }
            start = offset - Constant.REDIS_MAX_RESULTS + 1;
            end = offset;
            if (!isNull(limitStr))
            {
                limit = Integer.valueOf(limitStr);
                if (limit > 0)
                {
                    start = offset + 1;
                    end = offset + limit - 1;
                }
                else
                {
                    start = offset + limit + 1;
                    end = Math.abs(offset);
                }
            }
        }

        if (start < 0)
        {
            start = 0;
        }

        if (end > totalRecords)
        {
            end = totalRecords;
        }

        logger.info("M. offset:" + offsetStr + ", limit:" + limitStr + ",  total: " + totalRecords + ", start:" + start + ", end:" + end);

        map.put("offset", start);
        map.put("limit", end);

        return map;
    }

    public static Map<String, Integer> getOffsetAndLimit(String offsetStr, String limitStr, int maxIndex)
    {
        Map<String, Integer> map = new HashMap<String, Integer>();
        int start;
        int end;
        int offset;
        int limit;
        int positiveLimit = -1;

        limit = Utils.isNullOrEmpty(limitStr) ? -20 : Integer.parseInt(limitStr);
        offset = Utils.isNullOrEmpty(offsetStr) ? maxIndex : Integer.parseInt(offsetStr);

        if (offset > maxIndex)
        {

            offset = maxIndex;
            if (limit > 0)
                limit = 0;
        }
        if (limit > 0)
        {
            start = offset;
            end = offset + limit - 1;
            positiveLimit = limit;
        }
        else if (limit < 0)
        {
            start = offset + limit + 1;
            end = offset;
            if (offset + limit >= 0)
            {
                positiveLimit = limit * -1;
            }
            else
            {
                positiveLimit = offset + 1;
            }

        }
        else
        {
            start = end = positiveLimit = 0;
        }

        if (start < 0)
        {
            start = 0;
        }
        logger.info("maxIndex: " + maxIndex + ", offset:" + start + ", limit:" + positiveLimit);
        map.put("offset", start);
        map.put("limit", positiveLimit);
        return map;
    }

    /**
     * 
     * @param formData
     * @param key
     * @return
     */
    public static String fetchValueFromFormData(FormDataMultiPart formData, String key)
    {
        if (formData != null && key != null && formData.getField(key) != null)
        {
            if (formData.getField(key).getMediaType().equals(MediaType.TEXT_PLAIN_TYPE))
            {
                return formData.getField(key).getEntityAs(String.class).trim();
            }
        }

        return null;
    }

    /**
     * 
     * @param uriInfo
     * @param key
     * @return
     */
    public static String fetchValueFromPathData(UriInfo uriInfo, String key)
    {
        if (uriInfo != null && key != null)
        {
            MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
            if (pathParams.containsKey(key))
            {
                return pathParams.getFirst(key).toString().trim();
            }
        }
        return null;
    }

    /**
     * 
     * @param len
     * @return
     */
    public static String generateAccountReference()
    {
        char[] chars = "ABCDEFGHJKMNPQRSTUVWXYZ".toCharArray();
        char[] chars1 = "23456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        SecureRandom random = new SecureRandom();
        char c = 0;
        char c1 = 0;

        for (int i = 0; i < 5; i++)
        {
            if (i < 3)
            {
                c = chars[random.nextInt(chars.length)];
                sb.append(c);

            }
            if (i < 2)
            {
                c1 = chars1[random.nextInt(chars1.length)];
                sb.append(c1);
            }

        }
        return sb.toString();
    }

    /**
     * 
     * 
     * 
     * @param fileName
     * 
     * @return
     */

    public static String getResource(String fileName)

    {
        String script = null;
        InputStream inputStream = null;
        try
        {
            inputStream = Utils.class.getResourceAsStream("/" + fileName);
            StringBuilder out = new StringBuilder();

            int character;

            while ((character = inputStream.read()) != -1)
            {

                out.append((char) character);

            }
            script = out.toString();
        }
        catch (Exception e)
        {
            logger.error("Error while reading [" + "/" + fileName + "]", e);
        }
        finally
        {
            Utils.close(inputStream);
        }
        return script;
    }

    /**
     * Close a closeable without an exception.
     * 
     * @param c
     */
    public static void close(Closeable c)
    {
        try
        {
            if (c != null)
            {
                c.close();
            }
        }
        catch (Exception e)
        {
            logger.error("Error while closing " + c, e);
        }
    }

    /**
     * 
     * @param toTest
     * @param inSet
     * @return
     */
    public static boolean oneOf(String toTest, String[] inSet)
    {
        for (String s : inSet)
        {
            if (toTest.equals(inSet))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param uriInfo
     * @param key
     * @return
     */
    public static String fetchValueFromQueryParam(UriInfo uriInfo, String key)
    {
        if (uriInfo != null && key != null)
        {
            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
            if (queryParams.containsKey(key))
            {
                return queryParams.getFirst(key).toString().trim();
            }
        }
        return null;
    }

    public static Integer getTimestampInInteger(Long timestamp)
    {
        return (int) (timestamp / 1000);
    }

    public static Long getTimestampInLong(Integer timestamp)
    {
        return (long) timestamp * 1000L;
    }

    public static boolean equalLists(List<String> one, List<String> two)
    {
        if (one == null && two == null)
        {
            return true;
        }

        if ((one == null && two != null) || one != null && two == null || one.size() != two.size())
        {
            return false;
        }

        // to avoid messing the order of the lists we will use a copy
        one = new ArrayList<String>(one);
        two = new ArrayList<String>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }

    /**
     * 
     * @param phoneNumber
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String generateInviteCode(String phoneNumber)
    {
        String val = String.valueOf((Math.abs(phoneNumber.hashCode()) % 10000));
        StringBuffer inviteCode = new StringBuffer(val);
        for (int i = 0; i < 4 - val.length(); i++)
        {
            inviteCode.insert(0, 0);
        }
        return inviteCode.toString();
    }

    public static boolean checkVersionAndFunctionality(Class<?> clazz, Object o)
    {
        if (clazz == null || o == null)
            return false;

        if (clazz.isAssignableFrom(o.getClass()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static ClientUpdateType getClientUpdateType(ClientType requestedClientType, String requestedClientVersion)
    {
        if (ClientVersion.isCurrentVersion(requestedClientType, requestedClientVersion))
        {
            return ClientUpdateType.NO_UPDATE;
        }
        else if (ClientVersion.isSupportedVersion(requestedClientType, requestedClientVersion))
        {
            return ClientUpdateType.SOFT_UPDATE;
        }
        else if (!ClientVersion.isSupportedVersion(requestedClientType, requestedClientVersion))
        {
            return ClientUpdateType.HARD_UPDATE;
        }
        return null;
    }

    public static String generatePaginationKey(String token, EntityType entityType) throws NoSuchAlgorithmException
    {
        String paginationKey = token + "-" + Utils.getSystemTime() + "-" + entityType;
        paginationKey = getMD5(paginationKey);

        return paginationKey;
    }

    public static String getMD5(String string) throws NoSuchAlgorithmException
    {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(string.getBytes(), 0, string.length());
        return new BigInteger(1, m.digest()).toString(16);
    }

    public static String getCollectionAsString(Collection<?> input, String delimiter)
    {
        StringBuilder s = new StringBuilder();
        for (Object o : input)
        {
            if (o != null)
            {
                String output = o.toString();

                if (!output.matches("[a-zA-Z0-9]*"))
                {
                    output = "\"" + output + "\"";
                }
                s.append(output + delimiter);
            }
        }

        s.setLength(s.toString().length() - delimiter.length());

        return s.toString();
    }

    public static String getCollectionAsStringForEnum(Collection<? extends Enum> input, String delimiter)
    {
        StringBuilder s = new StringBuilder();
        for (Enum o : input)
        {
            s.append(o.name() + delimiter);
        }

        s.setLength(s.toString().length() - delimiter.length());

        return s.toString();
    }

    public static String getCollectionAsStringForEnum(Collection<String> input, String delimiter, Class<? extends Enum> enumClass)
    {
        StringBuilder s = new StringBuilder();
        for (String o : input)
        {
            s.append(Enum.valueOf(enumClass, o) + delimiter);
        }

        s.setLength(s.toString().length() - delimiter.length());

        return s.toString();
    }

    public static <T> T returnArg1OnNull(T arg1, T arg2)
    {
        return arg2 == null ? arg1 : arg2;
    }

    public static List<String> getCollectionFromString(String listValues, String delimiter)
    {
        List<String> collection = new ArrayList<String>();
        if (listValues != null && delimiter != null)
        {
            String[] values = listValues.split(delimiter);
            for (String value : values)
            {
                collection.add(value);
            }
        }
        return collection;
    }

    public static String sortMapToSolrString(Map<SortField, SortType> sort)
    {
        StringBuilder builder;
        if (sort != null && !sort.isEmpty())
        {
            builder = new StringBuilder();

            for (Entry<SortField, SortType> entry : sort.entrySet())
            {
                SortField field = entry.getKey();
                SortType type = entry.getValue();

                builder.append(field.solrField()).append(" ").append(type.toString()).append(",");
            }
        }
        else
        {
            return "";
        }

        return builder.substring(0, builder.length() - 1);
    }

    public static String sortMapToSolrString(Map<SortField, SortType> sort, SortField... fieldsToConsider)
    {
        StringBuilder builder;
        if (sort != null && !sort.isEmpty())
        {
            builder = new StringBuilder();

            for (SortField field : fieldsToConsider)
            {
                SortType type = sort.get(field);

                if (type != null)
                {
                    builder.append(field.solrField()).append(" ").append(type.toString()).append(",");
                }
            }
        }
        else
        {
            return "";
        }

        return builder.substring(0, builder.length() - 1);
    }

    public static String filterMapToSolrString(Map<FilterField, Set<String>> filter)
    {
        StringBuilder builder;

        if (filter != null && !filter.isEmpty())
        {
            builder = new StringBuilder();

            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                List<String> valueList = new ArrayList<String>(entry.getValue());

                if (valueList != null && !valueList.isEmpty())
                {
                    builder.append(field.solrField() + ":(");
                    for (int i = 0; i < valueList.size(); i++)
                    {
                        String value = valueList.get(i);
                        Object parsedValue = field.castIntoTypeSingle(value);

                        if (parsedValue instanceof Enum)
                        {
                            value = ((Enum) parsedValue).name();
                        }

                        if (!value.startsWith("["))
                        {
                            builder.append("\"" + value + "\"");
                        }
                        else
                        {
                            builder.append(value);
                        }

                        if (i != (valueList.size() - 1))
                        {
                            builder.append(" OR ");
                        }
                    }
                    builder.append(")");
                    builder.append(" AND ");
                }

            }
        }
        else
        {
            return "";
        }

        // System.out.println(builder.toString());

        if (builder.length() >= 4)
        {
            return builder.substring(0, builder.length() - 4);
        }
        else
        {
            return builder.toString();
        }
    }

    public static String filterMapToSolrString(Map<FilterField, Set<String>> filter, FilterField... fieldsToConsider)
    {
        StringBuilder builder;

        if (filter != null && !filter.isEmpty())
        {
            builder = new StringBuilder();

            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                List<String> valueList = new ArrayList<String>(entry.getValue());

                if (valueList != null && !valueList.isEmpty())
                {
                    builder.append(field.solrField() + ":(");
                    for (int i = 0; i < valueList.size(); i++)
                    {
                        String value = valueList.get(i);
                        builder.append(value);

                        if (i != (valueList.size() - 1))
                        {
                            builder.append(" OR ");
                        }
                    }
                    builder.append(")");
                    builder.append(" AND ");
                }

            }
        }
        else
        {
            return "";
        }

        if (builder.length() >= 4)
        {
            return builder.substring(0, builder.length() - 4);
        }
        else
        {
            return builder.toString();
        }
    }

    public static Map<SortField, SortType> subSetOfSort(Map<SortField, SortType> sort, SortField... fields)
    {
        Map<SortField, SortType> filteredMap = new HashMap<SortField, SortType>();

        if (fields != null && fields.length != 0)
        {
            for (SortField field : fields)
            {
                SortType t = sort.get(field);
                if (t != null)
                {
                    filteredMap.put(field, t);
                }
            }
        }

        return filteredMap;
    }

    public static Map<FilterField, Set<String>> subSetOfFilter(Map<FilterField, Set<String>> filter, FilterField... fields)
    {
        Map<FilterField, Set<String>> filteredMap = new HashMap<FilterField, Set<String>>();

        if (fields != null && fields.length != 0)
        {
            for (FilterField field : fields)
            {
                Set<String> valueSet = filter.get(field);
                if (valueSet != null)
                {
                    filteredMap.put(field, valueSet);
                }
            }
        }

        return filteredMap;
    }

    public static String generateIdForMessage()
    {
        return "M-" + System.currentTimeMillis() + "-" + UUID.randomUUID().toString();
    }

    public static String generateIdForGroup(String accountIdOne, String accountIdTwo, String chatThreadKey, String source)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("group_source", source);
        params.put("account_id_one", accountIdOne);
        params.put("account_id_two", accountIdTwo);
        int compare = accountIdOne.compareTo(accountIdTwo);
        if (compare > 0)
        {
            params.put("account_id_one", accountIdTwo);
            params.put("account_id_two", accountIdOne);
        }

        return Utils.populateKey(chatThreadKey, params);
    }

    public static boolean checkBetweenTimeFrame(int beginHourFrame, int beginMinuteFrame, int beginSecondFrame, int endHourFrame, int endMinuteFrame, int endSecondFrame, String timeZone)
    {
        Calendar timeNow = Calendar.getInstance(TimeZone.getTimeZone(timeZone));

        Calendar timeBeginFrame = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        timeBeginFrame.set(Calendar.HOUR_OF_DAY, beginHourFrame);
        timeBeginFrame.set(Calendar.MINUTE, beginMinuteFrame);
        timeBeginFrame.set(Calendar.SECOND, beginSecondFrame);

        Calendar timeEndFrame = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        timeEndFrame.set(Calendar.HOUR_OF_DAY, endHourFrame);
        timeEndFrame.set(Calendar.MINUTE, endMinuteFrame);
        timeEndFrame.set(Calendar.SECOND, endSecondFrame);
        return (timeNow.after(timeBeginFrame) && timeNow.before(timeEndFrame));

    }

}
