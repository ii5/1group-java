package one.group.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import one.group.core.Constant;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaExtensionType;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author bhumikabhatt
 * 
 */
public class ManagePhotos
{
    private static final Logger logger = LoggerFactory.getLogger(ManagePhotos.class);

    /**
     * 
     * Function will save photo and create it's thumbnail
     * 
     * @param photoStream
     *            InputStream file
     * @param extension
     *            File extension
     * @param fullSizeFilePath
     *            File path for saving full size image
     * @param thumbnailSizeFilePath
     *            File path for saving thumbnail image
     * @param fullPhotoUrl
     *            Photo URL of full size image
     * @param thumbnailPhotoUrl
     *            Photo URL of thumbnail image
     * @param createThumbnail
     *            Flag to create thumbnail
     * @return
     * @throws IOException
     */
    public static Map<MediaDimensionType, Map<String, Object>> uploadMedia(InputStream photoStream, String extension, String fullSizeFilePath, String thumbnailSizeFilePath, String fullPhotoUrl,
            String thumbnailPhotoUrl, int thumbnailWidth, int thumbnailHeight, boolean createThumbnail) throws IOException
    {
        // Upload full photo file
        OutputStream out = new FileOutputStream(new File(fullSizeFilePath));

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = photoStream.read(bytes)) != -1)
        {
            out.write(bytes, 0, read);
        }
        out.flush();
        out.close();

        File fullPhotoFile = new File(fullSizeFilePath);
        BufferedImage fullPhoto = ImageIO.read(fullPhotoFile);

        Map<String, Object> fullPhotoMap = new HashMap<String, Object>();
        fullPhotoMap.put("height", fullPhoto.getHeight());
        fullPhotoMap.put("width", fullPhoto.getWidth());
        fullPhotoMap.put("size", fullPhotoFile.length());
        fullPhotoMap.put("location", fullPhotoUrl);

        Map<String, Object> thumbnailPhotoMap = new HashMap<String, Object>();
        // Create thumbnail photo
        if (createThumbnail == true)
        {
            int fullPhotoWidth = fullPhoto.getWidth();
            int fullPhotoheight = fullPhoto.getHeight();

            if (fullPhotoWidth / fullPhotoheight < thumbnailWidth / thumbnailHeight)
            {
                thumbnailHeight = (int) Math.floor((double) fullPhotoheight * (double) thumbnailWidth / (double) fullPhotoWidth);
            }
            else
            {
                thumbnailWidth = (int) Math.floor((double) fullPhotoWidth * (double) thumbnailHeight / (double) fullPhotoheight);
            }
            Image scaledImg = fullPhoto.getScaledInstance(thumbnailWidth, thumbnailHeight, Image.SCALE_SMOOTH);
            BufferedImage thumbnail = new BufferedImage(thumbnailWidth, thumbnailHeight, BufferedImage.TYPE_INT_RGB);
            thumbnail.createGraphics().drawImage(scaledImg, 0, 0, null);
            File thumbnailFile = new File(thumbnailSizeFilePath);
            ImageIO.write(thumbnail, extension, thumbnailFile);

            thumbnailPhotoMap.put("height", thumbnail.getHeight());
            thumbnailPhotoMap.put("width", thumbnail.getWidth());
            thumbnailPhotoMap.put("size", thumbnailFile.length());
            thumbnailPhotoMap.put("location", thumbnailPhotoUrl);
        }

        Map<MediaDimensionType, Map<String, Object>> photoMap = new HashMap<MediaDimensionType, Map<String, Object>>();
        photoMap.put(MediaDimensionType.FULL, fullPhotoMap);
        photoMap.put(MediaDimensionType.THUMBNAIL, thumbnailPhotoMap);

        return photoMap;
    }

    public static String getPhotoName(FormDataBodyPart photoBodyPart)
    {
        String extension = getExtension(photoBodyPart.getMediaType().toString());
        return Utils.getSystemTime() + "." + extension;
    }

    public static String getExtension(String mediaType)
    {
        String[] fileType = mediaType.split("/");

        MediaExtensionType mediaExtensionType = MediaExtensionType.find(fileType[1]);

        if (Utils.isNull(mediaExtensionType))
        {
            throw new IllegalArgumentException();
        }

        return (fileType.length == 2) ? mediaExtensionType.toString() : Constant.DEFAULT_PHOTO_EXTENSION;
    }

    public static Map<String, Object> saveFullPhoto(String photoName, InputStream photoStream, FormDataContentDisposition photoDetails, String relativePath, String absolutePath) throws IOException
    {

        String fullSizeFilePath = relativePath + photoName;
        String fullPhotoUrl = absolutePath + photoName;

        File file = new File(fullSizeFilePath);
        OutputStream out = new FileOutputStream(file);
        int read = 0;
        byte[] bytes = new byte[1024];

        int counter = 1;
        try
        {
            while ((read = photoStream.read(bytes)) != -1)
            {
                if ((counter * read) > Constant.MAX_PHOTO_SIZE)
                {
                    file.delete();
                    out.close();

                    throw new IllegalStateException("File uploaded is too large: Max size is: " + Constant.MAX_PHOTO_SIZE_STR);
                }
                counter++;
                out.write(bytes, 0, read);
            }
            out.flush();
        }
        catch (Exception e)
        {
            logger.error("Error uploading image: " + e.getMessage(), e);
            throw new IOException(e);
        }
        finally
        {
            Utils.close(out);
            Utils.close(photoStream);
        }

        File fullPhotoFile = new File(fullSizeFilePath);
        BufferedImage fullPhoto = ImageIO.read(fullPhotoFile);
        if (fullPhotoFile.length() > Constant.MAX_PHOTO_SIZE)
        {
            throw new IllegalStateException("File uploaded is too large: Max size is: " + Constant.MAX_PHOTO_SIZE_STR);
        }
        Map<String, Object> photo = new HashMap<String, Object>();
        photo.put("width", fullPhoto.getWidth());
        photo.put("height", fullPhoto.getHeight());
        photo.put("size", fullPhotoFile.length());
        photo.put("location", fullPhotoUrl);
        photo.put("path", fullSizeFilePath);

        return photo;

    }

    public static Map<String, Object> saveThumbnailPhoto(String photoName, String sourceFilePath, FormDataBodyPart photoBodyPart, String relativePath, String absolutePath) throws IOException
    {

        String extension = getExtension(photoBodyPart.getMediaType().toString());

        // To set thumbnail photo path
        String thumbnailPath = relativePath + photoName;
        String thumbnailLocation = absolutePath + photoName;

        File fullPhotoFile = new File(sourceFilePath);
        BufferedImage fullPhoto = ImageIO.read(fullPhotoFile);
        int fullPhotoWidth = fullPhoto.getWidth();
        int fullPhotoheight = fullPhoto.getHeight();

        // Create thumbnail photo
        int thumbnailWidth = Constant.MIN_PHOTO_THUMBNAIL_WIDTH;
        int thumbnailHeight = Constant.MIN_PHOTO_THUMBNAIL_HEIGHT;
        if (fullPhotoWidth / fullPhotoheight < thumbnailWidth / thumbnailHeight)
        {
            thumbnailHeight = (int) Math.floor((double) fullPhotoheight * (double) thumbnailWidth / (double) fullPhotoWidth);
        }
        else
        {
            thumbnailWidth = (int) Math.floor((double) fullPhotoWidth * (double) thumbnailHeight / (double) fullPhotoheight);
        }
        Image scaledImg = fullPhoto.getScaledInstance(thumbnailWidth, thumbnailHeight, Image.SCALE_SMOOTH);
        BufferedImage thumbnail = new BufferedImage(thumbnailWidth, thumbnailHeight, BufferedImage.TYPE_INT_RGB);
        thumbnail.createGraphics().drawImage(scaledImg, 0, 0, null);

        File thumbnailFile = new File(thumbnailPath);
        ImageIO.write(thumbnail, extension, thumbnailFile);

        if (thumbnailFile.length() > Constant.MAX_PHOTO_SIZE)
        {
            throw new IllegalStateException("File uploaded is too large: Max size is: " + Constant.MAX_PHOTO_SIZE_STR);
        }

        Map<String, Object> photo = new HashMap<String, Object>();
        photo.put("width", thumbnail.getWidth());
        photo.put("height", thumbnail.getHeight());
        photo.put("size", thumbnailFile.length());
        photo.put("location", thumbnailLocation);
        photo.put("path", thumbnailPath);

        return photo;

    }
}
