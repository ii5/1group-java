package one.group.utils.validation.exception;

public class ValidationException extends RuntimeException
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ValidationException(final String message)
    {
        super(message);
    }

    public ValidationException(final String message, final Throwable th)
    {
        super(message, th);
    }
}
