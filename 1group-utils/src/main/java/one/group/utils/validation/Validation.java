package one.group.utils.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import one.group.core.Constant;
import one.group.utils.Utils;
import one.group.utils.validation.exception.ValidationException;

/**
 * 
 * 
 * @author nyalfernandes
 * 
 *         TODO : Define mobile number validations.
 *         <<<<<<< HEAD
 * 
 */
public class Validation
{
    public static List<String> allParametersSet(final Map<String, Object> paramsToCheck, final String... requiredParams)
    {
        List<String> paramsNotSet = new ArrayList<String>();
        for (String requiredParam : requiredParams)
        {
            if (!paramsToCheck.containsKey(requiredParam) && Utils.isNull(paramsToCheck.get(requiredParam)))
            {
                paramsNotSet.add(requiredParam);
            }
        }

        return paramsNotSet;
    }

    public static void isFalse(final boolean isFalse, final String message)
    {
        if (!isFalse)
        {
            String msg;
            msg = message == null ? Constant.MSG_NOT_FALSE : message;
            throw new ValidationException(msg, new IllegalStateException(msg));
        }
    }

    public static void isTrue(final boolean isTrue, final String message)
    {
        if (!isTrue)
        {
            String msg;
            msg = message == null ? Constant.MSG_NOT_TRUE : message;
            throw new ValidationException(msg, new IllegalStateException(msg));
        }
    }

    public static void notNull(final Object o, final String message)
    {
        if (o == null)
        {
            String msg;
            msg = message == null ? Constant.MSG_NULL_OBJ : message;
            throw new ValidationException(msg, new NullPointerException(msg));
        }
    }

    public static List<String> parametersExists(final Map<String, String> paramsToCheck, final String... requiredParams)
    {
        List<String> nonExistentParams = new ArrayList<String>();

        for (String requiredParam : requiredParams)
        {
            if (!paramsToCheck.containsKey(requiredParam))
            {
                nonExistentParams.add(requiredParam);
            }
        }
        return nonExistentParams;
    }

    public static boolean validateMobileNumber(final String number)
    {
        return Pattern.matches("\\s*\\+\\d{12}\\s*", number);
    }

    /**
     * 
     * @param string
     * @param field
     */
    public static void checkLength(String string, String field)
    {
        if (string.length() > Constant.MAX_TEXT_LENGTH)
        {
            throw new ValidationException(field + Constant.LENGTH_EXCEEDS, new IllegalStateException(field + Constant.LENGTH_EXCEEDS));
        }
    }

    public static void checkFieldExists(String field, String fieldValue)
    {
        if (Utils.isNullOrEmpty(fieldValue))
        {
            throw new ValidationException(Constant.NO_PARAM_PASSED + " : " + field, new IllegalStateException(Constant.NO_PARAM_PASSED + " : " + field));
        }
    }

    public static void checkFieldNotEmpty(String field, String value)
    {

        if (value.trim().length() == 0)
        {
            // throw new RuntimeException(field + Constant.MSG_NULL_OBJ);
            throw new ValidationException(field + " " + Constant.MSG_NULL_OBJ, new IllegalStateException(field + Constant.MSG_NULL_OBJ));
        }
    }

    public static void checkFieldLength(String field, String value, Integer length)
    {
        if (value.trim().length() != length)
        {
            throw new ValidationException(field + " " + Constant.FIELD_LENGTH_NOT_VALID, new IllegalStateException(field + Constant.FIELD_LENGTH_NOT_VALID));
        }
    }

    public static boolean isNumeric(String field, String value, int limitLength, boolean customException)
    {

        String regex = null;
        if (limitLength > 0)
        {
            regex = "[0-9]{" + limitLength + "}";
        }
        else
        {
            regex = "[0-9]";
        }

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(value);

        if (!m.matches())
        {
            if (customException == true)
            {
                return false;
            }
            else
            {
                String msg = Constant.INVALID_FIELD_VALUE + " " + field + " :" + value;
                throw new ValidationException(msg, new IllegalStateException(msg));
            }
        }
        return true;
    }

    public static void checkAlphaNumeric(String field, String value, boolean allowSpace)
    {

        String regex = null;

        if (allowSpace == true)
        {
            regex = "([A-Z 0-9 a-z])\\w+";
        }
        else
        {
            regex = "([A-Z0-9a-z])\\w+";
        }

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(value);

        if (!m.matches())
        {
            String msg = Constant.INVALID_FIELD_VALUE + " " + field + " :" + value;
            throw new ValidationException(msg, new IllegalStateException(msg));
        }
        return;
    }

    public static void checkNames(String field, String value, boolean allowSpace, boolean allowNumbers)
    {
        String regex = null;

        if (allowSpace == true)
        {
            if (allowNumbers == true)
            {
                regex = "([a-zA-Z]+)[a-zA-Z0-9 ]*";
            }
            else
            {
                regex = "([a-zA-Z]+)[a-zA-Z ]*";
            }
        }
        else
        {
            if (allowNumbers == true)
            {
                regex = "([a-zA-Z]+)[a-zA-Z0-9]*";
            }
            else
            {
                regex = "([a-zA-Z]+)[a-zA-Z]*";
            }
        }

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(value);

        if (!m.matches())
        {
            String msg = Constant.INVALID_FIELD_VALUE + " " + field + " :" + value;
            throw new ValidationException(msg, new IllegalStateException(msg));
        }
        return;
    }

    /**
     * 
     * Method used to validate fields and check if value is not null for
     * mandatory fields
     * 
     * @param fieldList
     */
    public static void validateFields(HashMap<String, String> fieldList)
    {
        for (String key : fieldList.keySet())
        {
            String value = fieldList.get(key);
            checkFieldExists(key, value);
            checkFieldNotEmpty(key, value);
        }
        return;
    }

    public static void main(String[] args)
    {
        System.out.println("+919833143042".matches("\\s*\\+\\d{12}\\s*"));
    }
}
