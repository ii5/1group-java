package one.group.chat.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import one.group.core.enums.GroupSource;
import one.group.entities.cache.ChatKey;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class generateDefaultGroupMembersCSV
{

    private static final Logger logger = LoggerFactory.getLogger(generateDefaultGroupMembersCSV.class);

    private static void generateCsv(String outputFileName, List<String[]> result) throws IOException
    {
        CSVWriter writer = null;
        String baseDir = "";
        String csvFilePath = "";
        try
        {
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getPath();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }
            writer = new CSVWriter(new FileWriter(baseDir + "/" + System.currentTimeMillis() + "-" + outputFileName));
            writer.writeAll(result);
            writer.flush();

        }
        catch (FileNotFoundException e)
        {
            System.err.println(e.getMessage());
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            if (writer != null)
                writer.close();
        }
    }

    public static void main(String[] args) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {

        String[] groupMembersHeader = { "group_id", "participant_id", "read_index", "received_index", "received_message_id", "read_message_id", "created_by", "updated_by", "created_time",
                "updated_time", "mobile_number", "active_member", "is_admin", "ever_sync", "current_sync", "source" };

        String defaultGroupMembersCSV = "defultGroupMembers.csv";
        System.out.println("Start: Saving group members in CSV - " + defaultGroupMembersCSV);
        List<String[]> defaultMembersResult = new ArrayList<String[]>();
        defaultMembersResult.add(groupMembersHeader);

        String csvFilePath = "user.csv";
        CSVReader reader = null;
        try
        {
            String baseDir = "";
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getParent();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }

            reader = new CSVReader(new FileReader(csvFilePath));
            String[] row;
            String chatKey = ChatKey.GROUP_ID.getKey();
            while ((row = reader.readNext()) != null)
            {
                String accountId = row[0];
                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                String meGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ME.toString());
                String[] groupMembersMe = { meGroupId, accountId, "-1", "-1", null, null, accountId, accountId, ft.format(date) + "", ft.format(date) + "", null, 0 + "", 0 + "", 1 + "", 0 + "",
                        GroupSource.ME.name() };

                String starredGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.STARRED.toString());
                String[] groupMembersStarred = { starredGroupId, accountId, "-1", "-1", null, null, accountId, accountId, ft.format(date) + "", ft.format(date) + "", null, 0 + "", 0 + "", 0 + "",
                        0 + "", GroupSource.STARRED.name() };

                String oneGroupGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ONEGROUP.toString());
                String[] groupMembersOneGroup = { oneGroupGroupId, accountId, "-1", "-1", null, null, accountId, accountId, ft.format(date) + "", ft.format(date) + "", null, 0 + "", 0 + "", 0 + "",
                        0 + "", GroupSource.ONEGROUP.name() };

                defaultMembersResult.add(groupMembersMe);
                defaultMembersResult.add(groupMembersStarred);
                defaultMembersResult.add(groupMembersOneGroup);

            }
        }
        catch (FileNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
        finally
        {
            if (reader != null)
                reader.close();
        }

        generateCsv(defaultGroupMembersCSV, defaultMembersResult);

    }
}
