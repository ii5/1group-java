package one.group.chat.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.GroupSource;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.status.Status;
import one.group.dao.ChatThreadDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.MessageDAO;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.GroupsService;
import one.group.utils.Utils;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVWriter;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class MigrationInitiator
{

    private static final Logger logger = LoggerFactory.getLogger(MigrationInitiator.class);

    private static void generateCsv(String outputFileName, List<String[]> result) throws IOException
    {
        CSVWriter writer = null;
        String baseDir = "";
        String csvFilePath = "";
        try
        {
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getPath();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }
            writer = new CSVWriter(new FileWriter(baseDir + "/" + System.currentTimeMillis() + "-" + outputFileName));
            writer.writeAll(result);
            writer.flush();

        }
        catch (FileNotFoundException e)
        {
            System.err.println(e.getMessage());
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            if (writer != null)
                writer.close();
        }
    }

    public static void main(String[] args) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {
        DOMConfigurator.configure("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-chat-migration.xml",
                "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-services.xml");

        logger.info("Chat migration process initiated");

        ChatThreadDAO chatThreadDAO = context.getBean("chatThreadDAO", ChatThreadDAO.class);
        GroupsService groupsService = context.getBean("groupsService", GroupsService.class);
        ChatMessageService chatMessageService = context.getBean("chatMessageService", ChatMessageService.class);
        MessageDAO messageDAO = context.getBean("messageDAO", MessageDAO.class);
        GroupsDAO groupsDAO = context.getBean("groupsDAO", GroupsDAO.class);
        AccountService accountService = context.getBean("accountService", AccountService.class);

        List<String[]> result = new ArrayList<String[]>();
        List<String[]> membersResult = new ArrayList<String[]>();
        String[] rowListHeader = { "Chat Thread id", "Total message count", "Participant One", "Received Index", "Read Index", "Received Message Id", "Read Message Id", "Participant Two",
                "Received Index", "Read Index", "Received Message Id", "Read Message Id" };

        String[] groupMembersHeader = { "group_id", "participant_id", "read_index", "received_index", "received_message_id", "read_message_id", "created_by", "updated_by", "created_time",
                "updated_time", "mobile_number", "active_member", "is_admin", "ever_sync", "current_sync", "source" };

        result.add(rowListHeader);
        membersResult.add(groupMembersHeader);
        String output = "chatThread.csv";
        String groupMembersCSV = "groupMembers.csv";
        String defaultGroupMembersCSV = "defaultGroupMembers.csv";

        String pattern = "chat_threads:*:participants";
        Set<String> chatThreads = chatThreadDAO.fetchAllChatThreads(pattern);
        int count = 0;
        System.out.println("Total chat threads [" + chatThreads.size() + "]");
        List<Groups> groupsList = new ArrayList<Groups>();
        List<Message> messageList = new ArrayList<Message>();

        for (String chatThread : chatThreads)
        {
            String[] chatThreadArray = chatThread.split(":");
            String chatThreadId = chatThreadArray[1];

            System.out.println(count + " [" + chatThreadId + "]");

            // Fetch participants from chat thread id
            Map<String, String> participants = chatThreadDAO.getChatThreadParticipants(chatThreadId);

            // Create DIRECT group in solr & create group members in mysql
            String createdById = participants.get("from_account_id");
            String toAccountId = participants.get("to_account_id");
            List<String> participantIds = new ArrayList<String>();
            participantIds.add(createdById);
            participantIds.add(toAccountId);
            Groups group = groupsService.formGroupObject(chatThreadId, createdById, participantIds, GroupSource.DIRECT);

            // Fetch chat message of chat thread
            List<WSChatMessage> wsChatMessageList = chatMessageService.fetchChatMessagesOfChatThread(chatThreadId, 0, -1);
            Map<String, String> indexVsMessageId = new HashMap<String, String>();
            Date groupCreatedTime = new Date();
            Date groupUpdatedTime = new Date();
            int messageCounter = 0;
            for (WSChatMessage wsChatMessage : wsChatMessageList)
            {

                Message message = new Message();
                String messageId = Utils.generateIdForMessage();
                message.setId(messageId);
                message.setGroupId(chatThreadId);
                message.setMessageStatus(MessageStatus.UNDEFINED);
                message.setClientSentTime(new Date(Long.valueOf(wsChatMessage.getClientSentTime())));
                message.setCreatedBy(wsChatMessage.getFromAccountId());

                if (createdById.equals(wsChatMessage.getFromAccountId()))
                {
                    message.setToAccountId(toAccountId);
                }
                else
                {
                    message.setToAccountId(wsChatMessage.getFromAccountId());
                }

                if (wsChatMessage.getIndex() == 0)
                {
                    groupCreatedTime = new Date(Long.valueOf(wsChatMessage.getServerTime()));
                }

                if (messageList.size() - 1 == wsChatMessage.getIndex())
                {
                    groupUpdatedTime = new Date(Long.valueOf(wsChatMessage.getServerTime()));
                }
                message.setIndex(wsChatMessage.getIndex());
                message.setMessageSource(MessageSourceType.DIRECT);

                message.setCreatedBy(wsChatMessage.getFromAccountId());
                message.setCreatedTime(new Date(Long.valueOf(wsChatMessage.getServerTime())));
                message.setUpdatedBy(wsChatMessage.getFromAccountId());
                message.setUpdatedTime(new Date(Long.valueOf(wsChatMessage.getServerTime())));

                if (wsChatMessage.getType().equals(MessageType.TEXT))
                {
                    message.setMessageType(MessageType.TEXT);
                    message.setText(wsChatMessage.getText());
                }
                else if (wsChatMessage.getType().equals(MessageType.PROPERTY_LISTING))
                {
                    message.setMessageType(MessageType.BROADCAST);
                    message.setBroadcastId(wsChatMessage.getPropertyListingId());
                }
                else if (wsChatMessage.getType().equals(MessageType.PHOTO))
                {
                    message.setMessageType(MessageType.PHOTO);
                    message.setText(Utils.getJsonString(wsChatMessage.getPhoto()));
                }
                else if (wsChatMessage.getType().equals(MessageType.PHONE_CALL))
                {
                    message.setMessageType(MessageType.PHONE_CALL);
                }

                messageList.add(message);

                indexVsMessageId.put(wsChatMessage.getIndex() + "", messageId);
                messageCounter++;
            }
            if (!messageList.isEmpty())
            {
                try
                {

                    group.setCreatedTime(groupCreatedTime);
                    group.setUpdatedTime(groupUpdatedTime);
                    group.setTotalmsgCount(messageCounter);
                    groupsList.add(group);
                }
                catch (Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }

            // Update message cursor in mysql
            List<String> rowList = new ArrayList<String>();
            rowList.add(chatThreadId);
            rowList.add(messageCounter + "");
            for (String participantId : participantIds)
            {
                String readIndex = chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, participantId);
                String receivedIndex = chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, participantId);

                String receivedMessageId = indexVsMessageId.get(receivedIndex);
                String readMessageId = indexVsMessageId.get(readIndex);

                rowList.add(participantId);
                rowList.add(receivedIndex);
                rowList.add(readIndex);
                rowList.add(receivedMessageId);
                rowList.add(readMessageId);

                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String[] groupMembersA = { chatThreadId, participantId, readIndex, receivedIndex, receivedMessageId, readMessageId, participantId, participantId, ft.format(groupCreatedTime) + "",
                        ft.format(groupUpdatedTime) + "", null, 0 + "", 0 + "", 0 + "", 0 + "", GroupSource.DIRECT.name() };
                membersResult.add(groupMembersA);
            }

            String[] resultRow = rowList.toArray(new String[0]);

            result.add(resultRow);

            count++;
        }

        System.out.println("Total Message[" + messageList.size() + "]");
        messageDAO.saveMessages(messageList);

        System.out.println("Total Group[" + groupsList.size() + "]");
        groupsDAO.saveGroupsBulk(groupsList);

        Set<String> accountIdSet = accountService.fetchIdsOfAllActiveAccount();
        System.out.println("Start: Generate default groups for accounts[" + accountIdSet.size() + "]");
        Set<String> accountIdBulkSet = new HashSet<String>();
        int i = 0;
        Iterator<String> iterator = accountIdSet.iterator();
        List<GroupMembers> allGroupsMembersList = new ArrayList<GroupMembers>();
        List<Groups> allGroupsList = new ArrayList<Groups>();

        List<Groups> defaultGroups = new ArrayList<Groups>();
        Map<List<Groups>, List<GroupMembers>> groupVsMembersMapAll = new HashMap<List<Groups>, List<GroupMembers>>();
        while (iterator.hasNext())
        {
            String accountId = iterator.next();
            if (accountIdBulkSet.size() == 100 || !iterator.hasNext())
            {
                Map<String, WSAccount> idVsWSAccountMap = accountService.fetchAccountDetailsInBulk(accountIdBulkSet);
                for (String aId : accountIdBulkSet)
                {
                    WSAccount wsAccount = idVsWSAccountMap.get(aId);
                    if (wsAccount != null && wsAccount.getStatus().equals(Status.ACTIVE))
                    {
                        System.out.println(i + " - Account found[" + aId + "]");
                        String cityId = wsAccount.getCity().getId();
                        String fullName = wsAccount.getFullName();
                        String mobileNumber = wsAccount.getPrimaryMobile();
                        Map<List<Groups>, List<GroupMembers>> groupVsMembersMap = groupsService.createDefaultGroupsForAccountAndReturnMembers(aId, cityId, fullName, mobileNumber);

                        for (List<Groups> groupsFormedList : groupVsMembersMap.keySet())
                        {
                            allGroupsList.addAll(groupsFormedList);
                        }

                        for (List<GroupMembers> groupMFormedList : groupVsMembersMap.values())
                        {
                            allGroupsMembersList.addAll(groupMFormedList);
                        }
                        // groupVsMembersMapAll.putAll(groupVsMembersMap);
                    }
                    else if (wsAccount != null)
                    {
                        System.out.println(i + " - Account found[" + aId + "] but status[" + wsAccount.getStatus() + "]");
                    }
                    else
                    {
                        System.out.println(i + " - Account not found[" + aId + "]");
                    }
                    i++;
                }
                accountIdBulkSet = new HashSet<String>();
            }
            else
            {
                accountIdBulkSet.add(accountId);
            }
        }

        System.out.println("End: Generate default groups");

        System.out.println("Generating group member csv - " + groupMembersCSV);
        generateCsv(groupMembersCSV, membersResult);

        System.out.println("Generating group csv - " + output);
        generateCsv(output, result);

        System.out.println("Start: Saving group bulk");
        groupsDAO.saveGroupsBulk(allGroupsList);
        System.out.println("End: Saving group bulk");

        System.out.println("Start: Saving group members in CSV - " + defaultGroupMembersCSV);
        List<String[]> defaultMembersResult = new ArrayList<String[]>();
        defaultMembersResult.add(groupMembersHeader);
        for (GroupMembers gm : allGroupsMembersList)
        {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String[] groupMembersA = { gm.getGroupId(), gm.getParticipantId(), "-1", "-1", null, null, gm.getParticipantId(), gm.getParticipantId(), ft.format(date) + "", ft.format(date) + "", "1",
                    0 + "", 0 + "", 0 + "", 0 + "", gm.getSource().name() };
            defaultMembersResult.add(groupMembersA);
        }
        generateCsv(defaultGroupMembersCSV, defaultMembersResult);

        // groupsService.saveGroupMembersBulk(allGroupsMembersList);
        System.out.println("End: Saving group members CSV");

        System.out.println("ends");
        context.close();
    }
}
