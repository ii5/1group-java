package one.group.notification;

import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSWhatsAppConnectRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSWhatsAppConnect;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountService;
import one.group.services.MarketingListService;
import one.group.services.PropertyListingService;
import one.group.services.helpers.NotificationConfiguration;
import one.group.utils.Utils;

public class WhatsAppConnect
{
    private static final Logger logger = LoggerFactory.getLogger(WhatsAppConnect.class);

    private NotificationConfiguration notificationConfiguration;
//    private NewsFeedService newsFeedService;
    private AccountService accountService;
    private MarketingListService marketingListService;
    private PropertyListingService propertyListingService;

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public MarketingListService getMarketingListService()
    {
        return marketingListService;
    }

    public void setMarketingListService(MarketingListService marketingListService)
    {
        this.marketingListService = marketingListService;
    }

    public NotificationConfiguration getNotificationConfiguration()
    {
        return notificationConfiguration;
    }

    public void setNotificationConfiguration(NotificationConfiguration notificationConfiguration)
    {
        this.notificationConfiguration = notificationConfiguration;
    }

//    public NewsFeedService getNewsFeedService()
//    {
//        return newsFeedService;
//    }
//
//    public void setNewsFeedService(NewsFeedService newsFeedService)
//    {
//        this.newsFeedService = newsFeedService;
//    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public String getInviteCodeForAnAccount(String phoneNumber, String accountId) throws JsonMappingException, JsonGenerationException, IOException, RequestValidationException
    {
        // check invite code in cache if exist then return it else make bpo syn
        // api call then generate invite code and save it into redis
        String inviteCode = notificationConfiguration.getDefaultInvitationCode();

        String cachedInviteCode = accountService.fetchAccountInviteCode(accountId);
        if (!Utils.isNullOrEmpty(cachedInviteCode))
        {
            inviteCode = cachedInviteCode;
        }
        else
        {
//            WSWhatsAppConnect connectResult = getWhatsAppInfoFromPhoneNumber(phoneNumber);
//            Set<WSNewsFeed> newsFeed = newsFeedService.fetchNewsFeed(accountId);
//            int newsFeedCount = 0;
//            if (newsFeed != null && newsFeed.size() > 0)
//            {
//                newsFeedCount = newsFeed.size();
//            }
//
//            validate("totalConnectedGroups", connectResult.getTotalConnectedGroups(), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
//            validate("everSync", String.valueOf(connectResult.isEverSync()), NOT_NULL(), NOT_EMPTY());
//            validate("inviteStatus", connectResult.getInviteStatus(), NOT_NULL(), NOT_EMPTY());
//
//            int groupCount = Integer.valueOf(connectResult.getTotalConnectedGroups());
//            boolean everSync = connectResult.isEverSync();
//            String inviteStatus = connectResult.getInviteStatus();
//            // check requesting phone number exists in marketing list table
//            MarketingList isExistsPhoneNumber = marketingListService.checkPhoneNumberExists(phoneNumber);
//            boolean inMarketingList = isExistsPhoneNumber != null ? true : false;
//            // check if requesting phone number has 1 or more properties created
//            int getPropertyListingOfAccount = propertyListingService.getPropertyListingCountOfAccount(accountId, accountId, true);
//            boolean hasPropertyListing = getPropertyListingOfAccount > 0 ? true : false;
//            boolean isEligible = isInvited(inviteStatus, groupCount, newsFeedCount, everSync, inMarketingList, hasPropertyListing);
//            
//            if (isEligible)
//            {
//                inviteCode = Utils.generateInviteCode(phoneNumber);
//                accountService.saveInviteCodeForAccount(accountId, inviteCode);
//            }
        }
        logger.debug("Fetching invite code for phone number [" + phoneNumber + "], account id [" + accountId + "], invite code [" + inviteCode + "]");
        return inviteCode;
    }

    public boolean isInvited(String inviteStatus, int groupCount, int newsFeedCount, boolean everSync, boolean inMarketingList, boolean hasPropertyListing) throws JsonMappingException,
            JsonGenerationException, IOException, RequestValidationException
    {

        validate("groupCountThreshold", String.valueOf(notificationConfiguration.getGroupCountThreshold()), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        validate("newsFeedCountThreshold", String.valueOf(notificationConfiguration.getNewsFeedCountThreshold()), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        int groupCountThreshold = Integer.valueOf(notificationConfiguration.getGroupCountThreshold());
        int newsFeedThreshold = Integer.valueOf(notificationConfiguration.getNewsFeedCountThreshold());
        if (inviteStatus.equalsIgnoreCase("blacklist"))
        {
            return false;
        }
        else if (inviteStatus.equalsIgnoreCase("whitelist"))
        {
            return true;
        }        
        else if (inMarketingList || hasPropertyListing) 
        {
        	return true; 
        }
        
        else
        {
            if (groupCount >= groupCountThreshold && newsFeedCount >= newsFeedThreshold)
            {
                return true;
            }
            else if (everSync)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public WSWhatsAppConnect getWhatsAppInfoFromPhoneNumber(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        String baseUrl = notificationConfiguration.getWhatsAppSyncUrl();

        WSWhatsAppConnectRequest request = new WSWhatsAppConnectRequest();
        request.setPhoneNumber(phoneNumber);

        Response response = callWebEndPoint(baseUrl, null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);

        WSResponse wsResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSWhatsAppConnect result = (WSWhatsAppConnect) Utils.getInstanceFromJson(Utils.getJsonString(wsResponse.getData()), WSWhatsAppConnect.class);

        return result;
    }

    private Response callWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {

        ClientConfig config = new ClientConfig();
        // config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }
}
