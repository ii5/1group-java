package one.group.notification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import one.group.entities.api.response.GCMData;
import one.group.entities.api.response.GCMNotification;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSGCMRequest;
import one.group.entities.jpa.Client;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.services.AccountService;
import one.group.services.ClientService;
import one.group.services.SMSMarketingLogService;
import one.group.services.SMSService;
import one.group.services.helpers.GCMConnectionFactory;
import one.group.services.helpers.NotificationConfiguration;
import one.group.utils.Utils;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVReader;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class NotificationInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(NotificationInitiator.class);

    public static void main(final String[] args) throws JsonMappingException, JsonGenerationException, IOException, AccountNotFoundException
    {
        DOMConfigurator.configure("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-notification.xml",
                "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-services.xml");

        logger.info("Notification process initiated");

        if (args.length == 0)
        {
            logger.error("========================================================");
            logger.error("Invalid type of notification or type has not been passed");
            logger.error("========================================================");

            context.close();
            return;
        }

        String notificationType = args[0];
        NotificationInitiator initiator = (NotificationInitiator) context.getBean("notificationInitiator");

        NotificationConfiguration notificationConfig = (NotificationConfiguration) context.getBean("notificationConfiguration");
        String notificationMode = notificationConfig.getNotificationMode();

        if (notificationMode.equals("true"))
        {
            logger.info("Notification [" + notificationType + "] is initiated");

            if (notificationType.equals("special_update"))
            {
                initiator.sendSpecialUpdate(context);
            }
            else if (notificationType.equalsIgnoreCase("app_update"))
            {
                initiator.sendAppUpdatesToAllActiveClients(context);
            }
            else if (notificationType.equalsIgnoreCase("send_invitation"))
            {
                initiator.sendInvitationNotification(context);
            }
            else if (notificationType.equalsIgnoreCase("sms_notification"))
            {
                if (args.length <= 1)
                {
                    logger.error("====================================================");
                    logger.error("CSV path is not provided");
                    logger.error("====================================================");
                }
                else
                {
                    String csvFilePath = args[1];
                    initiator.sendSMSNotification(context, csvFilePath);
                }
            }
            else
            {
                logger.info("========================================================");
                logger.info("Invalid type of notification or type has not been passed");
                logger.info("========================================================");
            }
        }
        else
        {
            logger.info("========================================================");
            logger.info("Notification is disabled");
            logger.info("========================================================");
        }
        context.close();
    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendSpecialUpdate(ClassPathXmlApplicationContext context) throws JsonMappingException, JsonGenerationException, IOException, AccountNotFoundException
    {

        AccountService accountService = (AccountService) context.getBean("accountService");
        List<WSAccount> accounts = new ArrayList<WSAccount>();
        // accounts = accountService.fetchAllActiveAccounts();

//        WSAccount account1 = accountService.fetchAccountByMobileNumber("+919967388337");
//        WSAccount account2 = accountService.fetchAccountByMobileNumber("+917506497079");
//        WSAccount account3 = accountService.fetchAccountByMobileNumber("+919833143042");
//        WSAccount account4 = accountService.fetchAccountByMobileNumber("+918976718081");
//        accounts.add(account1);
//        accounts.add(account2);
//        accounts.add(account3);
//        accounts.add(account4);
//
//        if (accounts != null && accounts.size() > 0)
//        {
//            ClientService clientService = (ClientService) context.getBean("clientService");
//            GCMConnectionFactory connectionFactory = (GCMConnectionFactory) context.getBean("gcmConnectionFactory");
//            NotificationConfiguration notificationConfig = (NotificationConfiguration) context.getBean("notificationConfiguration");
//            for (WSAccount account : accounts)
//            {
//                try
//                {
//                    if (account == null)
//                    {
//                        continue;
//                    }
//                    String accountId = account.getId();
//
//                    // fetch all active clients of an account
//                    List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);
//
//                    List<String> cpsIdListOfActiveClient = new ArrayList<String>();
//                    if (activeClients != null && activeClients.size() > 0)
//                    {
//                        for (Client client : activeClients)
//                        {
//                            cpsIdListOfActiveClient.add(client.getCpsId());
//                        }
//                    }
//
//                    // Send GCM notification to Active client
//                    WSGCMRequest gcmRequestData = new WSGCMRequest();
//
//                    GCMData gcmData = new GCMData();
//                    gcmData.setId(notificationConfig.getSpecialUpdateDataId());
//                    gcmData.setType(notificationConfig.getSpecialUpdateDataType());
//                    gcmRequestData.setData(gcmData);
//
//                    GCMNotification gcmNotification = new GCMNotification();
//                    gcmNotification.setBody(notificationConfig.getSpecialUpdateNotificationBody());
//                    gcmNotification.setTitle(notificationConfig.getSpecialUpdateNotificationTitle());
//                    gcmRequestData.setNotification(gcmNotification);
//
//                    gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);
//
//                    connectionFactory.postData(gcmRequestData);
//                    System.out.println("Pushing Special Updates to Account [" + accountId + "]" + Utils.getJsonString(gcmRequestData));
//
//                }
//                catch (Exception e)
//                {
//                    logger.error("Error while sending Special Updates", e);
//                }
//            }
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//            logger.info("Special updates has been successfully sent to all client!!!");
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//        }
//        else
//        {
//            logger.info("========================================================");
//            logger.info("No Clients found");
//            logger.info("========================================================");
//
//        }

    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendAppUpdatesToAllActiveClients(ClassPathXmlApplicationContext context) throws JsonMappingException, JsonGenerationException, IOException, AccountNotFoundException
    {
        AccountService accountService = (AccountService) context.getBean("accountService");
        List<WSAccount> accounts = new ArrayList<WSAccount>();
        // accounts = accountService.fetchAllActiveAccounts();

//        WSAccount account1 = accountService.fetchAccountByMobileNumber("+919967388337");
//        WSAccount account2 = accountService.fetchAccountByMobileNumber("+917506497079");
//        WSAccount account3 = accountService.fetchAccountByMobileNumber("+919833143042");
//        WSAccount account4 = accountService.fetchAccountByMobileNumber("+918976718081");
//        accounts.add(account1);
//        accounts.add(account2);
//        accounts.add(account3);
//        accounts.add(account4);
//
//        if (accounts != null && accounts.size() > 0)
//        {
//            ClientService clientService = (ClientService) context.getBean("clientService");
//            GCMConnectionFactory connectionFactory = (GCMConnectionFactory) context.getBean("gcmConnectionFactory");
//            NotificationConfiguration notificationConfig = (NotificationConfiguration) context.getBean("notificationConfiguration");
//
//            for (WSAccount account : accounts)
//            {
//                try
//                {
//                    if (account == null)
//                    {
//                        continue;
//                    }
//                    String accountId = account.getId();
//
//                    // fetch all active clients of an account
//                    List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);
//
//                    List<String> cpsIdListOfActiveClient = new ArrayList<String>();
//                    if (activeClients != null && activeClients.size() > 0)
//                    {
//                        for (Client client : activeClients)
//                        {
//                            cpsIdListOfActiveClient.add(client.getCpsId());
//                        }
//                    }
//
//                    // Send GCM notification to Active client
//                    WSGCMRequest gcmRequestData = new WSGCMRequest();
//
//                    GCMData gcmData = new GCMData();
//                    gcmData.setId(notificationConfig.getAppUpdateDataId());
//                    gcmData.setType(notificationConfig.getAppUpdateDataType());
//                    gcmRequestData.setData(gcmData);
//
//                    GCMNotification gcmNotification = new GCMNotification();
//                    gcmNotification.setBody(notificationConfig.getAppUpdateNotificationBody());
//                    gcmNotification.setTitle(notificationConfig.getAppUpdateNotificationTitle());
//                    gcmRequestData.setNotification(gcmNotification);
//
//                    gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);
//
//                    connectionFactory.postData(gcmRequestData);
//                    System.out.println("Pushing App Updates to Account [" + accountId + "]" + Utils.getJsonString(gcmRequestData));
//
//                }
//                catch (Exception e)
//                {
//                    logger.error("Error while sending App Updates", e);
//                }
//            }
//
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//            logger.info("App updates has been successfully sent to all client!!!");
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//        }
//        else
//        {
//            logger.info("========================================================");
//            logger.info("No Clients found");
//            logger.info("========================================================");
//        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendInvitationNotification(ClassPathXmlApplicationContext context) throws JsonMappingException, JsonGenerationException, IOException, AccountNotFoundException
    {
        AccountService accountService = (AccountService) context.getBean("accountService");
        List<WSAccount> accounts = new ArrayList<WSAccount>();
//        accounts = accountService.fetchAllActiveAccounts();
//
//        /*
//         * WSAccount account1 =
//         * accountService.fetchAccountByMobileNumber("+919967388337"); WSAccount
//         * account2 =
//         * accountService.fetchAccountByMobileNumber("+917506497079"); WSAccount
//         * account3 =
//         * accountService.fetchAccountByMobileNumber("+919833143042"); WSAccount
//         * account4 =
//         * accountService.fetchAccountByMobileNumber("+918976718081");
//         * accounts.add(account1); accounts.add(account2);
//         * accounts.add(account3); accounts.add(account4);
//         */
//
//        if (accounts != null && accounts.size() > 0)
//        {
//            WhatsAppConnect whatsAppConnect = (WhatsAppConnect) context.getBean("whatsAppConnect");
//            ClientService clientService = (ClientService) context.getBean("clientService");
//            GCMConnectionFactory connectionFactory = (GCMConnectionFactory) context.getBean("gcmConnectionFactory");
//            SMSService smsCountryService = (SMSService) context.getBean("smsCountryService");
//            SMSService smsNetcoreService = (SMSService) context.getBean("smsNetcoreService");
//            NotificationConfiguration notificationConfig = (NotificationConfiguration) context.getBean("notificationConfiguration");
//
//            logger.info("Total active accounts [" + accounts.size() + "]");
//            int count = 0;
//            int processCounter = 0;
//            for (WSAccount account : accounts)
//            {
//                try
//                {
//                    if (account == null)
//                    {
//                        continue;
//                    }
//                    String accountId = account.getId();
//                    String phoneNumber = account.getPrimaryMobile();
//
//                    // fetch all active clients of an account
//                    List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);
//
//                    List<String> cpsIdListOfActiveClient = new ArrayList<String>();
//                    if (activeClients != null && activeClients.size() > 0)
//                    {
//                        for (Client client : activeClients)
//                        {
//                            cpsIdListOfActiveClient.add(client.getCpsId());
//                        }
//                    }
//
//                    // Get invite code for an account
//                    String inviteCode = whatsAppConnect.getInviteCodeForAnAccount(phoneNumber, accountId);
//
//                    // Send GCM notification to Active client
//                    WSGCMRequest gcmRequestData = new WSGCMRequest();
//                    GCMNotification gcmNotification = new GCMNotification();
//
//                    String smsContent = null;
//                    if (!inviteCode.equals(notificationConfig.getDefaultInvitationCode()))
//                    {
//                        smsContent = notificationConfig.getInvitationCodeMessage();
//                        smsContent = smsContent.replace("##INVITECODE##", inviteCode);
//
//                        gcmNotification.setBody(smsContent);
//                        gcmNotification.setTitle(notificationConfig.getAppUpdateNotificationTitle());
//
//                        GCMData gcmData = new GCMData();
//                        gcmData.setId(notificationConfig.getAppUpdateDataId());
//                        gcmData.setType(notificationConfig.getAppUpdateDataType());
//
//                        gcmRequestData.setData(gcmData);
//                        gcmRequestData.setNotification(gcmNotification);
//                        gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);
//
//                        connectionFactory.postData(gcmRequestData);
//
//                        // Send SMS to phoneNumber
//                        // smsCountryService.sendSMS(phoneNumber, smsContent);
//                        // smsNetcoreService.sendSMS(phoneNumber, smsContent);
//
//                        count++;
//                        logger.info(processCounter + "[" + phoneNumber + "] invitation code [" + inviteCode + "] sent");
//                    }
//                    else
//                    {
//                        logger.info(processCounter + "[" + phoneNumber + "] not eligible for invitation code." + inviteCode);
//                    }
//                }
//                catch (Exception e)
//                {
//                    logger.error("Error while sending invitation code", e);
//                }
//                processCounter++;
//            }
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//            logger.info("Invitation code has been successfully sent to total [" + count + "] accounts!!!");
//            logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//        }
//        else
//        {
//            logger.info("========================================================");
//            logger.info("No Clients found");
//            logger.info("========================================================");
//        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendSMSNotification(ClassPathXmlApplicationContext context, String csvFilePath) throws JsonMappingException, JsonGenerationException, IOException, AccountNotFoundException
    {
        CSVReader reader = null;
        SMSService smsCountryService = (SMSService) context.getBean("smsCountryService");
        SMSService smsNetcoreService = (SMSService) context.getBean("smsNetcoreService");
        SMSMarketingLogService smsMarketingLogService = (SMSMarketingLogService) context.getBean("smsMarketingLogService");

        try
        {
            String baseDir = "";
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getParent();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }

            reader = new CSVReader(new FileReader(csvFilePath));
            String[] row;

            while ((row = reader.readNext()) != null)
            {

                if (row.length == 3)
                {
                    String mobileNumber = row[0];
                    String message = row[1];
                    String campaign = row[2];

                    smsMarketingLogService.saveSMSMarketingLog(mobileNumber, message, campaign);
                    // smsCountryService.sendSMS(mobileNumber, message);
                    smsNetcoreService.sendSMS(mobileNumber, message);
                }
                else
                {
                    logger.error("Invalid entry [" + Arrays.toString(row) + "]");
                }

            }
        }
        catch (FileNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
        finally
        {
            if (reader != null)
                reader.close();
        }
    }
}
