package one.group.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kafka.utils.ZkUtils;
import one.group.core.Constant;
import one.group.core.enums.TopicType;
import one.group.report.consumer.OneGroupReportConsumer;
import one.group.sync.KafkaConfiguration;
import one.group.sync.SyncEntityHandler;
import one.group.sync.ZookeeperClientFactory;
import one.group.sync.consumer.KafkaConsumerFactory;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import scala.collection.Map;
import scala.collection.Seq;

public class KafkaReportUtils implements ApplicationContextAware
{

    private static final Logger logger = LoggerFactory.getLogger(KafkaReportUtils.class);

    private ZookeeperClientFactory zkClientFactory;

    private KafkaConsumerFactory consumerFactory;

    private SyncEntityHandler syncHandler;

    private SyncEntityHandler apiLogSyncHandler;

    private ApplicationContext applicationContext;

    private KafkaConfiguration kafkaConfiguration;

    private static ExecutorService executor;

    public ZookeeperClientFactory getZkClientFactory()
    {
        return zkClientFactory;
    }

    public void setZkClientFactory(ZookeeperClientFactory zkClientFactory)
    {
        this.zkClientFactory = zkClientFactory;
    }

    public KafkaConsumerFactory getConsumerFactory()
    {
        return consumerFactory;
    }

    public void setConsumerFactory(KafkaConsumerFactory consumerFactory)
    {
        this.consumerFactory = consumerFactory;
    }

    public SyncEntityHandler getSyncHandler()
    {
        return syncHandler;
    }

    public void setSyncHandler(SyncEntityHandler syncHandler)
    {
        this.syncHandler = syncHandler;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    public SyncEntityHandler getApiLogSyncHandler()
    {
        return apiLogSyncHandler;
    }

    public void setApiLogSyncHandler(SyncEntityHandler apiLogSyncHandler)
    {
        this.apiLogSyncHandler = apiLogSyncHandler;
    }

    public void initialize()
    {
        try
        {
            logger.info("Initializing all for topic type " + TopicType.APP.toString());
            String appTopic = kafkaConfiguration.getTopic(TopicType.APP).getName();
            Thread.sleep(2000);
            createConsumers(appTopic, TopicType.APP);

            String logTopic = kafkaConfiguration.getTopic(TopicType.LOG).getName();
            logger.info("Initializing all for topic type " + TopicType.LOG.toString());
            Thread.sleep(2000);
            createConsumersForLog(logTopic, TopicType.LOG);
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    public void createConsumers(String topicName, TopicType type) throws Exception
    {
        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> topicPartitioMap = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();

        if (topicPartitioMap == null)
        {
            throw new IllegalStateException("No topic '" + topicName + "' present.");
        }

        List<Integer> toHandlePartitionList = getPartitionsOfTopic(topicPartitioMap, type);

        if (!toHandlePartitionList.isEmpty())
        {
            executor = Executors.newFixedThreadPool(toHandlePartitionList.size());
            Properties serverProperties = applicationContext.getBean("sync.server.properties", Properties.class);

            logger.info("Loading offsets for topic " + topicName);
            Properties syncOffsetProperties = new Properties();
            String syncOffsetFileName = getFileFromServerProperties("syncOffsetLocation", Constant.SYNC_SERVER_OFFSET_FILE_TEMPLATE, topicName, serverProperties);
            File syncOffsetFile = createFile(syncOffsetFileName);
            syncOffsetProperties.load(new FileInputStream(syncOffsetFile));

            logger.info("Loading error offsets for topic " + topicName);
            Properties chatLogErrorOffsetProperties = new Properties();
            String chatLogErrorOffsetFileName = getFileFromServerProperties("chatLogErrorOffset", Constant.ERROR_OFFSET_FILE_TEMPLATE, topicName, serverProperties);
            File chatLogErrorOffsetFile = createFile(chatLogErrorOffsetFileName);
            chatLogErrorOffsetProperties.load(new FileInputStream(chatLogErrorOffsetFile));

            for (int partition : toHandlePartitionList)
            {
                Thread.sleep(100);
                OneGroupReportConsumer consumer = new OneGroupReportConsumer(partition, syncOffsetProperties, consumerFactory.getConfigProperties(), syncOffsetFile, topicName, type,
                        chatLogErrorOffsetFile, chatLogErrorOffsetProperties);
                consumer.setSyncHandler(syncHandler);
                logger.info("" + consumer);
                executor.submit(consumer);
            }
        }
    }

    public void createConsumersForLog(String topicName, TopicType type) throws Exception
    {
        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> topicPartitioMap = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();

        if (topicPartitioMap == null)
        {
            throw new IllegalStateException("No topic '" + topicName + "' present.");
        }

        List<Integer> toHandlePartitionList = getPartitionsOfTopic(topicPartitioMap, type);

        if (!toHandlePartitionList.isEmpty())
        {
            executor = Executors.newFixedThreadPool(toHandlePartitionList.size());
            Properties serverProperties = applicationContext.getBean("sync.server.properties", Properties.class);
            String syncOffsetFileName = (serverProperties == null) ? Constant.SYNC_SERVER_OFFSET_FILE_TEMPLATE : serverProperties.getProperty("syncOffsetLocation",
                    Constant.SYNC_SERVER_OFFSET_FILE_TEMPLATE);

            logger.info("Loading offsets for topic " + topicName);
            Properties syncOffsetProperties = new Properties();
            syncOffsetFileName = syncOffsetFileName.replace("[topic]", topicName);
            File syncOffsetFile = new File(syncOffsetFileName);
            if (syncOffsetFile.exists())
            {
                syncOffsetProperties.load(new FileInputStream(syncOffsetFileName));
            }
            else
            {
                if (syncOffsetFile.getParentFile() != null)
                {
                    syncOffsetFile.getParentFile().mkdirs();
                }
                syncOffsetFile.canExecute();
                syncOffsetFile.canWrite();
                syncOffsetFile.canRead();
                syncOffsetFile.createNewFile();
            }

            for (int partition : toHandlePartitionList)
            {
                Thread.sleep(100);

                OneGroupReportConsumer consumer = new OneGroupReportConsumer(partition, syncOffsetProperties, consumerFactory.getConfigProperties(), syncOffsetFile, topicName, type);
                consumer.setSyncHandler(apiLogSyncHandler);
                logger.info("" + consumer);
                executor.submit(consumer);
            }
        }
    }

    private List<Integer> getPartitionsOfTopic(Map<Object, Seq<Object>> topicPartitioMap, TopicType type)
    {
        List<Integer> partitionList = new ArrayList<Integer>();

        if (kafkaConfiguration.getTopic(type).isAllPartitions())
        {
            int size = topicPartitioMap.size();
            for (int i = 0; i < size; i++)
            {
                partitionList.add(i);
            }
        }
        else
        {
            partitionList.addAll(kafkaConfiguration.getTopic(type).getPartitionList());
        }

        System.out.println(partitionList);
        return partitionList;
    }

    private File createFile(String OffsetFileName) throws IOException
    {
        File OffsetFile = new File(OffsetFileName);

        if (OffsetFile.getParentFile() != null)
        {
            OffsetFile.getParentFile().mkdirs();
        }
        OffsetFile.canExecute();
        OffsetFile.canWrite();
        OffsetFile.canRead();
        OffsetFile.createNewFile();

        return OffsetFile;
    }

    private String getFileFromServerProperties(String key, String defaultTemplate, String topicName, Properties serverProperties)
    {
        String fileName = (serverProperties == null) ? defaultTemplate : serverProperties.getProperty(key, defaultTemplate);
        fileName = fileName.replace("[topic]", topicName);

        return fileName;
    }

}
