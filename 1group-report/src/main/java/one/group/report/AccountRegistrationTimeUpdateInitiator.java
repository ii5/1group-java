package one.group.report;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AccountRegistrationTimeUpdateInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(AccountRegistrationTimeUpdateInitiator.class);

    public static void main(String[] args)
    {
        DOMConfigurator.configureAndWatch("log4j.xml");

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-registration-time.xml");

        logger.info("Update registration initiated");
    }

}
