package one.group.report;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ChatLogServerInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(ChatLogServerInitiator.class);

    public static void main(String[] args)
    {
        DOMConfigurator.configureAndWatch("log4j.xml");

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-report.xml");

        logger.info("Report initiated");
    }

}
