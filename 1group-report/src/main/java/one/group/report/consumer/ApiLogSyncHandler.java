package one.group.report.consumer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import one.group.entities.api.response.APIStats;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.APIDetails;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.sync.AbstractSyncHandler;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

public class ApiLogSyncHandler extends AbstractSyncHandler
{
    private static final Logger logger = LoggerFactory.getLogger(ApiLogSyncHandler.class);
    private static Properties configProperties = new Properties();
    private static final String fileToFTPKey = "fileToFTP";
    private static final String localDirectory = "localDirectory";
    private static final String[] apiLogHeaders = new String[] { "API endpoint name", "Sum of times called", "Sum of time spent processing requests(ms)", "Sum of client errors (4XX)",
            "Sum of server errors (5XX)", "Max time spent processing an individual request (ms)", "Details of request" };

    private static Map<String, APIStats> apiStats = new HashMap<String, APIStats>();

    public Properties getConfigProperties()
    {
        return configProperties;
    }

    public void setConfigProperties(Properties configProperties)
    {
        this.configProperties = configProperties;
    }

    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {
        File file = new File(configProperties.getProperty(localDirectory) + configProperties.getProperty(fileToFTPKey));
        if (!file.exists())
            apiStats.clear();

        APIDetails apiDetails = syncLogEntry.getApiDetails();
        String endpointURL = apiDetails.getUrl();

        endpointURL = getProcessedEndpoint(endpointURL);

        APIStats stats = apiStats.get(endpointURL);
        if (stats == null)
        {
            stats = new APIStats();
            apiStats.put(endpointURL, stats);
        }
        stats.setTotalTimesCalled(stats.getTotalTimesCalled() + 1);
        long requestProcessingTime = apiDetails.getResponseTime().getTime() - apiDetails.getRequestTime().getTime();
        long maxRequestProcessingTime = requestProcessingTime;
        stats.setTotalTimeSpentProcessing(stats.getTotalTimeSpentProcessing() + requestProcessingTime);
        String statusCode = apiDetails.getResponse().getResult().getCode();
        if (statusCode.startsWith("4"))
        { // Client Error
            stats.setTotalClientErrors(stats.getTotalClientErrors() + 1);

        }
        else if (statusCode.startsWith("5"))
        {// Server Error
            stats.setTotalServerErrors(stats.getTotalServerErrors() + 1);
        }
        if (stats.getMaxTimeSpentProcessing() < maxRequestProcessingTime)
        {
            stats.setMaxTimeSpentProcessing(maxRequestProcessingTime);
            stats.setAdditionalData(apiDetails.getAdditionalRequestData());
        }

        Validation.notNull(syncLogEntry, "Record should not be null.");
        writeToCSVAndFlush(file, apiDetails);
        return null;
    }

    // private String getProcessedEndpoint(String endpoint)
    // {
    // int lastSlashPosition = endpoint.lastIndexOf("/");
    // String endpointPart = endpoint.substring(lastSlashPosition + 1);
    //
    // String processedEndpoint = "";
    // String patternStr = "(([0-9]+,?)+)";
    // Pattern pattern = Pattern.compile(patternStr);
    // Matcher matcher = pattern.matcher(endpointPart);
    // if (matcher.find())
    // {
    // processedEndpoint = endpoint.replaceAll(endpointPart, "");
    // }
    // else
    // {
    // processedEndpoint = endpoint;
    // }
    //
    // return processedEndpoint;
    // }

    private boolean isNumeric(String s)
    {
        return java.util.regex.Pattern.matches("\\d+", s);
    }

    private String getProcessedEndpoint(String endpoint)
    {
        String[] endpointParts = endpoint.split("/");
        StringBuilder builder = new StringBuilder();
        for (String endpointPart : endpointParts)
        {
            String[] endpointPartAry = endpointPart.split(",");
            for (String endpointPartStr : endpointPartAry)
            {
                if (!isNumeric(endpointPartStr))
                {
                    builder.append(endpointPartStr);
                    builder.append("/");
                }
            }
        }
        endpoint = builder.toString();
        if (endpoint.contains("/cursors"))
        {
            int endIndx = endpoint.indexOf("/chat_threads/") + "/chat_threads/".length();
            endpoint = endpoint.substring(0, endIndx);
            endpoint = endpoint + "cursors";
        }

        if (endpoint.contains("/chat-thread-"))
        {
            int endIndx = endpoint.indexOf("/chat_threads/") + "/chat_threads/".length();
            endpoint = endpoint.substring(0, endIndx);

        }
        return endpoint;

    }

    public static void writeToCSVAndFlush(File file, APIDetails apiDetails)
    {
        Set<String> endpointUrlKeys = apiStats.keySet();
        List<String[]> reportData = new ArrayList<String[]>();
        reportData.add(apiLogHeaders);
        for (String endpointUrl : endpointUrlKeys)
        {
            APIStats rowStats = apiStats.get(endpointUrl);
            String[] rowData = new String[7];
            rowData[0] = endpointUrl;
            rowData[1] = String.valueOf(rowStats.getTotalTimesCalled());
            rowData[2] = String.valueOf(rowStats.getTotalTimeSpentProcessing());
            rowData[3] = String.valueOf(rowStats.getTotalClientErrors());
            rowData[4] = String.valueOf(rowStats.getTotalServerErrors());
            rowData[5] = String.valueOf(rowStats.getMaxTimeSpentProcessing());
            rowData[6] = String.valueOf(rowStats.getAdditionalData());
            reportData.add(rowData);
        }
        try
        {
            CSVWriter writer = new CSVWriter(new FileWriter(file));
            writer.writeAll(reportData);
            writer.flush();
            writer.close();

        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {

        String x = "1234";
        String[] splitted = x.split(",");
        System.out.println(splitted[0]);
        ApiLogSyncHandler alsh = new ApiLogSyncHandler();
        String ep = "/1group-rest/v1.4/accounts/3428558644679987,1234/property_listings";
        String pe = alsh.getProcessedEndpoint(ep);
        System.out.println(pe);

    }

}
