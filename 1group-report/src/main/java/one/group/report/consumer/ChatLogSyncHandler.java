/**
 * 
 */
package one.group.report.consumer;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ChatLogService;
import one.group.services.SubscriptionService;
import one.group.sync.AbstractSyncHandler;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class ChatLogSyncHandler extends AbstractSyncHandler<WSChatMessage>
{
    private static final Logger logger = LoggerFactory.getLogger(ChatLogSyncHandler.class);

    private ChatLogService chatLogService;

    private SubscriptionService subscriptionService;

    public ChatLogService getChatLogService()
    {
        return chatLogService;
    }

    public void setChatLogService(ChatLogService chatLogService)
    {
        this.chatLogService = chatLogService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {
        Validation.notNull(syncLogEntry, "Record should not be null.");
        if (syncLogEntry.getType().equals(SyncDataType.MESSAGE))
        {

            List<String> associatedAccountIds = subscriptionService.retreiveAllSubscribersOf(syncLogEntry.getAssociatedEntityId(), syncLogEntry.getAssociatedEntityType());

            if (associatedAccountIds.size() != 2)
            {
                throw new IllegalStateException("Expected subscribed accounts count is two, got " + associatedAccountIds.size());
            }
            WSChatMessage wsChatMessage = syncLogEntry.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSChatMessage.class);

            logger.debug("ChatThreadId Passing to ChatLog:" + wsChatMessage.getChatThreadId());
            logger.debug("ClientSentTime Passing to ChatLog:" + wsChatMessage.getClientSentTime());
            logger.debug("FromAccountId Passing to ChatLog:" + wsChatMessage.getFromAccountId());
            // logger.debug("PropertyListingId Passing to ChatLog:" +
            // wsChatMessage.getPropertyListingId());
            logger.debug("ServerTime Passing to ChatLog:" + wsChatMessage.getServerTime());
            // logger.debug("Index Passing to ChatLog:" +
            // wsChatMessage.getIndex());
            logger.debug("ToAccountID Passing to ChatLog:" + associatedAccountIds.get(0));

            associatedAccountIds.remove(wsChatMessage.getFromAccountId());
            chatLogService.saveChatLog(wsChatMessage, associatedAccountIds.get(0));
        }
        return null;
    }

}
