package one.group.report.consumer;

import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import kafka.api.FetchRequest;
import kafka.api.FetchRequestBuilder;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.common.ErrorMapping;
import kafka.common.TopicAndPartition;
import kafka.javaapi.FetchResponse;
import kafka.javaapi.OffsetRequest;
import kafka.javaapi.OffsetResponse;
import kafka.javaapi.PartitionMetadata;
import kafka.javaapi.TopicMetadata;
import kafka.javaapi.TopicMetadataRequest;
import kafka.javaapi.TopicMetadataResponse;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.message.MessageAndOffset;
import one.group.core.Constant;
import one.group.core.enums.TopicType;
import one.group.entities.sync.SyncLogEntry;
import one.group.services.AccountService;
import one.group.sync.SyncHandler;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateAccountRegisterTimeConsumer implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(UpdateAccountRegisterTimeConsumer.class);

    private SyncHandler syncHandler;

    private Properties offsetProperties;

    private int partition;

    private String clientPreFix;

    private String groupId;

    private List<Broker> brokerList = new ArrayList<Broker>();;

    private int timeout;

    private int bufferSize;

    private String topic;

    private TopicType topicType;

    private String clientId;

    private File syncOffsetFile;

    private Properties errorOffsetProperties;

    private AccountService accountService;

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public UpdateAccountRegisterTimeConsumer()
    {

    }

    public UpdateAccountRegisterTimeConsumer(int partition, Properties offsetProperties, Properties consumerProperties, File syncOffsetFile, String topicName, TopicType topicType,
            File errorOffsetFile, Properties errorOffsetProperties)
    {
        Validation.notNull(offsetProperties, "OffsetProperties file should not be null.");
        Validation.notNull(consumerProperties, "Consumer Properties file should not be null.");
        Validation.isTrue(!consumerProperties.isEmpty(), "Consumer Properties should not be empty.");
        Validation.isTrue(!(partition < 0), "Partition number should not be less than 0.");
        Validation.notNull(syncOffsetFile, "Sync Offset File should not be null.");
        Validation.notNull(errorOffsetFile, "Error Offset File should not be null.");
        Validation.notNull(errorOffsetProperties, "ErrorOffsetProperties file should not be null.");

        validateConsumerProperties(consumerProperties, topicType);
        this.offsetProperties = offsetProperties;
        this.partition = partition;
        this.syncOffsetFile = syncOffsetFile;
        this.topic = topicName;
        this.topicType = topicType;
        this.clientId = clientPreFix + "-" + topic + "-" + partition;
        this.errorOffsetProperties = errorOffsetProperties;
    }

    public UpdateAccountRegisterTimeConsumer(int partition, Properties offsetProperties, Properties consumerProperties, File syncOffsetFile, String topicName, TopicType topicType)
    {
        Validation.notNull(offsetProperties, "OffsetProperties file should not be null.");
        Validation.notNull(consumerProperties, "Consumer Properties file should not be null.");
        Validation.isTrue(!consumerProperties.isEmpty(), "Consumer Properties should not be empty.");
        Validation.isTrue(!(partition < 0), "Partition number should not be less than 0.");
        Validation.notNull(syncOffsetFile, "Sync Offset File should not be null.");

        validateConsumerProperties(consumerProperties, topicType);
        this.offsetProperties = offsetProperties;
        this.partition = partition;
        this.syncOffsetFile = syncOffsetFile;
        this.topic = topicName;
        this.topicType = topicType;
        this.clientId = clientPreFix + "-" + topic + "-" + partition;
    }

    public void run()
    {
        readData();
    }

    public void readData()
    {
        SimpleConsumer consumer = null;
        try
        {
            PartitionMetadata metaData = findLeader(brokerList, topic, partition);
            if (metaData == null)
            {
                logger.error("Cannot find metadata for Topic and Parition. Exiting!");
                return;
            }

            if (metaData.leader() == null)
            {
                logger.error("Cannot find leader for Topic or Partition. Exiting!");
                return;
            }

            String leader = InetAddress.getByName(metaData.leader().host()).getHostAddress();
            int leaderPort = metaData.leader().port();

            consumer = new SimpleConsumer(leader, leaderPort, timeout, bufferSize, clientId);

            long whichTime = System.currentTimeMillis();
            // long readOffset = getLastOffset(consumer, topic, partition,
            // whichTime, clientId);
            long readOffset = Long.parseLong(offsetProperties.getProperty("" + partition, "0"));
            int numErrors = 0;

            logger.info(clientId + " initiated.");
            String regex = "(\\/1group-rest)\\/(v.*)\\/(registration.*)$";
            List<String> processedAccounts = new ArrayList<String>();
            while (true)
            {
                if (consumer == null)
                {
                    consumer = new SimpleConsumer(leader, leaderPort, timeout, bufferSize, clientId);
                }

                FetchRequest request = new FetchRequestBuilder().clientId(clientId).addFetch(topic, partition, readOffset, bufferSize).maxWait(2000).build();
                FetchResponse response = consumer.fetch(request);

                if (response.hasError())
                {
                    numErrors++;
                    int code = response.errorCode(topic, partition);
                    logger.error("Error(" + numErrors + ") in consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + "]");
                    if (numErrors > 5)
                    {
                        break;
                    }

                    if (code == ErrorMapping.OffsetOutOfRangeCode())
                    {
                        readOffset = getLastOffset(consumer, topic, partition, whichTime, clientId);
                    }
                    consumer.close();
                    consumer = null;

                    leader = findNewLeader(brokerList, leader, topic, partition);
                    continue;
                }
                numErrors = 0;

                for (MessageAndOffset messageAndOffset : response.messageSet(topic, partition))
                {
                    long currentOffset = messageAndOffset.offset();
                    if (currentOffset < readOffset)
                    {
                        logger.warn("[" + topic + ":" + partition + "] Current offset < Read Offset, [CurrOffset: " + currentOffset + ", ReadOffset:" + readOffset + "]");
                        continue;
                    }

                    readOffset = messageAndOffset.nextOffset();
                    ByteBuffer payload = messageAndOffset.message().payload();

                    byte[] bytes = new byte[payload.limit()];
                    payload.get(bytes);

                    String rawData = new String(bytes, Constant.UTF8_ENCODING);
                    // logger.info(clientId + " [" + currentOffset + ": " +
                    // rawData + "]");
                    SyncLogEntry syncLogEntry = null;
                    try
                    {
                        syncLogEntry = (SyncLogEntry) Utils.getInstanceFromJson(rawData, SyncLogEntry.class);
                        String apiUrl = syncLogEntry.getApiDetails().getUrl();
                        if (apiUrl.matches(regex) && apiUrl.contains("registration"))
                        {
                            logger.info("raw data" + rawData);
                            String accountId = syncLogEntry.getOriginatingAccount().getId();
                            if (!processedAccounts.contains(accountId))
                            {
                                Date registrationTime = syncLogEntry.getApiDetails().getRequestTime();

                                boolean isProcessed = accountService.ifNullOnlyUpdateRegistrationTimeOfAccount(accountId, registrationTime);
                                if (isProcessed)
                                {
                                    processedAccounts.add(accountId);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.error("Error while parsing data into SyncLogEntry. Data: " + rawData, e);
                    }

                    offsetProperties.setProperty("" + partition, "" + readOffset);
                    commitOffsets(offsetProperties, syncOffsetFile);

                    // OffsetCommitResponse commitResponse =
                    // consumer.commitOffsets(offSetCommitRequest(clientId,
                    // groupId, currentOffset, topic, partition));
                    // if (commitResponse.hasError())
                    // {
                    // logger.info(commitResponse.toString());
                    // }
                }
            }
            logger.info("Processed accounts [" + processedAccounts + "]");
            logger.info("====================== " + clientId);

        }
        catch (Exception e)
        {
            logger.error("Shutting down consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + "]", e);
        }
        finally
        {
            if (consumer != null)
            {
                consumer.close();
            }

            logger.error("Shutting down consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + "]");
        }
    }

    public long getLastOffset(SimpleConsumer consumer, String topic, int partition, long whichTime, String clientId)
    {
        TopicAndPartition topicAndPartition = new TopicAndPartition(topic, partition);
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo = new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(whichTime, 1));

        OffsetRequest request = new OffsetRequest(requestInfo, kafka.api.OffsetRequest.CurrentVersion(), clientId);
        OffsetResponse response = consumer.getOffsetsBefore(request);

        if (response.hasError())
        {
            logger.error("Error fetching data offset. " + ErrorMapping.exceptionFor(response.errorCode(topic, partition)));
            return 0;
        }

        long[] offsets = response.offsets(topic, partition);
        return offsets[0];

    }

    private String findNewLeader(List<Broker> brokers, String oldLeader, String topic, int partition) throws Exception
    {
        for (int i = 0; i < 3; i++)
        {
            boolean goToSleep = false;
            PartitionMetadata metadata = findLeader(brokers, topic, partition);
            if (metadata == null)
            {
                goToSleep = true;
            }
            else if (metadata.leader() == null)
            {
                goToSleep = true;
            }
            else if (oldLeader.equalsIgnoreCase(metadata.leader().host()) && i == 0)
            {
                // first time through if the leader hasn't changed give
                // ZooKeeper a second to recover
                // second time, assume the broker did recover before failover,
                // or it was a non-Broker issue
                //
                goToSleep = true;
            }
            else
            {
                return metadata.leader().host();
            }
            if (goToSleep)
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ie)
                {
                }
            }
        }
        logger.error("Unable to find new leader after Broker failure. Exiting");
        throw new Exception("Unable to find new leader after Broker failure. Exiting");
    }

    private PartitionMetadata findLeader(List<Broker> brokers, String topic, int partition)
    {
        PartitionMetadata metadata = null;

        loop:
        for (Broker broker : brokers)
        {
            SimpleConsumer consumer = null;

            try
            {
                consumer = new SimpleConsumer(broker.getHost(), Integer.parseInt(broker.getPort()), timeout, bufferSize, "leaderLookup");
                List<String> topics = Collections.singletonList(topic);
                TopicMetadataRequest topicMetadataRequest = new TopicMetadataRequest(topics);
                TopicMetadataResponse topicMetadataResponse = consumer.send(topicMetadataRequest);

                List<TopicMetadata> topicMetaDataList = topicMetadataResponse.topicsMetadata();

                for (TopicMetadata topicMetadata : topicMetaDataList)
                {
                    for (PartitionMetadata partitionMetadata : topicMetadata.partitionsMetadata())
                    {
                        if (partitionMetadata.partitionId() == partition)
                        {
                            metadata = partitionMetadata;
                            break loop;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.error("Error communicating with broker " + broker + " of [" + brokers + "].", e);
            }
            finally
            {
                if (consumer != null)
                {
                    consumer.close();
                }
            }
        }

        return metadata;
    }

    private boolean validateConsumerProperties(Properties p, TopicType type)
    {
        Validation.notNull(p, "Consumer properties should not be null.");
        StringBuilder errorMessage = new StringBuilder();
        String template = "Required property '%s' not set.";

        if (Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "brokers")))
        {
            errorMessage.append(String.format(template, "brokers")).append("\n");
        }
        this.brokerList = getBrokers(p.getProperty(type.toString() + "." + "brokers"));

        if (Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "groupId")))
        {
            errorMessage.append(String.format(template, "groupId")).append("\n");
        }
        this.groupId = p.getProperty(type.toString() + "." + "groupId");

        setDefaults();
        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "timeout")))
        {
            this.timeout = Integer.parseInt(p.getProperty(type.toString() + "." + "timeout"));
        }

        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "bufferSize")))
        {
            this.bufferSize = Integer.parseInt(p.getProperty(type.toString() + "." + "bufferSize"));
        }

        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "clientIdPrefix")))
        {
            this.clientPreFix = p.getProperty(type.toString() + "." + "clientIdPrefix");
        }

        return true;
    }

    private void setDefaults()
    {
        this.timeout = Constant.CONSUMER_CLIENT_TIMEOUT;
        this.bufferSize = Constant.CONSUMER_BUFFER_SIZE;
        this.clientPreFix = Constant.CONSUMER_CLIENT_PREFIX;
    }

    private List<Broker> getBrokers(String unParsed)
    {
        Set<Broker> brokerList = new HashSet<Broker>();

        String[] brokersRaw = unParsed.split(",");

        if (brokersRaw.length == 0)
        {
            throw new IllegalArgumentException("Required parameter 'brokers' not set correctly. Format: 'localhost:2181,10.40.23.21:2998'");
        }

        for (String broker : brokersRaw)
        {
            String[] brokerDetails = broker.split(":");

            if (brokerDetails.length < 2 || brokerDetails.length > 2)
            {
                throw new IllegalArgumentException("Required parameter 'brokers' not set correctly. Format: 'localhost:2181,10.40.23.21:2998'");
            }

            brokerList.add(new Broker(brokerDetails[0], brokerDetails[1]));
        }

        return new ArrayList<Broker>(brokerList);
    }

    private void commitOffsets(Properties offsetProperties, File offsetFile) throws Exception
    {
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(offsetFile);
            offsetProperties.store(fos, new Date().toString());
        }
        catch (Exception e)
        {
            logger.error("Error while persisting offsets [" + offsetProperties + "]: " + e.getMessage());
        }
        finally
        {
            Utils.close(fos);
        }
    }

    public long getBufferSize()
    {
        return bufferSize;
    }

    public String getClientId()
    {
        return clientId;
    }

    public String getClientPreFix()
    {
        return clientPreFix;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public Properties getOffsetProperties()
    {
        return offsetProperties;
    }

    public int getPartition()
    {
        return partition;
    }

    public long getTimeout()
    {
        return timeout;
    }

    public String getTopic()
    {
        return topic;
    }

    public List<Broker> getBrokerList()
    {
        return this.brokerList;
    }

    public SyncHandler getSyncHandler()
    {
        return syncHandler;
    }

    public void setSyncHandler(SyncHandler syncHandler)
    {
        this.syncHandler = syncHandler;
    }

    public Properties getErrorOffsetProperties()
    {
        return errorOffsetProperties;
    }

    public void setErrorOffsetProperties(Properties errorOffsetProperties)
    {
        this.errorOffsetProperties = errorOffsetProperties;
    }

    class Broker
    {
        private String host;

        private String port;

        public Broker(String host, String port)
        {
            Validation.isTrue(!Utils.isNullOrEmpty(host), "broker host should not be null or empty.");
            this.host = host;
            this.port = port;
        }

        public String getHost()
        {
            return host;
        }

        public String getPort()
        {
            return port;
        }

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((host == null) ? 0 : host.hashCode());
            result = prime * result + ((port == null) ? 0 : port.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Broker other = (Broker) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (host == null)
            {
                if (other.host != null)
                    return false;
            }
            else if (!host.equals(other.host))
                return false;
            if (port == null)
            {
                if (other.port != null)
                    return false;
            }
            else if (!port.equals(other.port))
                return false;
            return true;
        }

        private UpdateAccountRegisterTimeConsumer getOuterType()
        {
            return UpdateAccountRegisterTimeConsumer.this;
        }

    }

    @Override
    public String toString()
    {
        return "OneGroupConsumer [syncHandler=" + syncHandler + ", offsetProperties=" + offsetProperties + ", partition=" + partition + ", clientPreFix=" + clientPreFix + ", groupId=" + groupId
                + ", brokerList=" + brokerList + ", timeout=" + timeout + ", bufferSize=" + bufferSize + ", topic=" + topic + ", topicType=" + topicType + ", clientId=" + clientId
                + ", syncOffsetFile=" + syncOffsetFile + "]";
    }

}
