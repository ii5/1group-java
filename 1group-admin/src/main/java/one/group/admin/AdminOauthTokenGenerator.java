package one.group.admin;

import one.group.services.AdminUtilityService;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AdminOauthTokenGenerator
{
    private static final Logger logger = LoggerFactory.getLogger(AdminOauthTokenGenerator.class);

    /**
     * This method is used for generating oauth Token for admin user. role.
     */
    public static void main(String[] args)
    {
        DOMConfigurator.configure("log4j.xml");
        BasicConfigurator.configure();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml");
        logger.info("AdminOauthTokenGenerator started");
        AdminUtilityService admin = (AdminUtilityService) context.getBean("adminUtilityService");
        String oAuthToken = admin.generateAdminOauthToken();
        if (oAuthToken != null)
        {
            System.out.println("Admin token generated successfulyy !!!");
            System.out.println("ADMIN TOKEN : " + oAuthToken);
        }
        else
        {
            System.out.println("FAILED - Admin token could not be generated !!!");
        }
        System.exit(0);

    }

}
