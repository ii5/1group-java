public class Test
{
    public static void main(final String[] args)
    {
        String s1 = "Hello!";
        String s2 = "Hello!";
        String s3 = new String("Hello!");
        String s4 = s2;

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));

        s3 = s3.intern();

        System.out.println(s1 == s4);
        System.out.println(s1.equals(s4));

        System.out.println(s4 == s3);
        System.out.println(s4.equals(s3));
    }
}
