package one.group.validate.operations;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface ValidationOperator
{
    public boolean operate(String op1);
}
