package one.group.validate.operations;

import one.group.exceptions.codes.RequestValidationExceptionCode;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SimpleValidationOperator implements ValidationOperator
{
    private RequestValidationExceptionCode exceptionCode;

    public SimpleValidationOperator(RequestValidationExceptionCode exceptionCode)
    {
        this.exceptionCode = exceptionCode;
    }

    // must be overriden
    public boolean operate(String op1)
    {
        return true;
    }

    public RequestValidationExceptionCode getCode()
    {
        return exceptionCode;
    }

    public boolean advance(String op1)
    {
        return true;
    }
}
