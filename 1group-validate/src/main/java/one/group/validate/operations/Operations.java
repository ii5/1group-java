package one.group.validate.operations;

import java.io.UnsupportedEncodingException;

import jersey.repackaged.com.google.common.collect.Lists;
import one.group.core.Constant;
import one.group.exceptions.codes.OneGroupExceptionCode;
import one.group.exceptions.movein.RequestValidationException;
import one.group.utils.Utils;

import org.apache.commons.lang.math.NumberUtils;

import static one.group.exceptions.codes.RequestValidationExceptionCode.BOTH_SET;
import static one.group.exceptions.codes.RequestValidationExceptionCode.COUNT_EXCEEDED;
import static one.group.exceptions.codes.RequestValidationExceptionCode.INVALID_FORMAT;
import static one.group.exceptions.codes.RequestValidationExceptionCode.IS_EMPTY;
import static one.group.exceptions.codes.RequestValidationExceptionCode.IS_NULL;
import static one.group.exceptions.codes.RequestValidationExceptionCode.LENGTH_EXCEEDED;
import static one.group.exceptions.codes.RequestValidationExceptionCode.NOT_NUMERIC;
import static one.group.exceptions.codes.RequestValidationExceptionCode.NOT_VALID;
import static one.group.exceptions.codes.RequestValidationExceptionCode.OUT_OF_RANGE;
import static one.group.exceptions.codes.RequestValidationExceptionCode.SIZE_EXCEEDED;

/**
 * 
 * @author nyalfernandes
 * 
 */
public final class Operations
{
    public static final ValidationOperator IS_EQUAL(final String op2)
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && op1.equals(op2);
            }
        };
    }

    public static final ValidationOperator IS_NUMERIC()
    {
        return new SimpleValidationOperator(NOT_NUMERIC)
        {
            public boolean operate(String op1)
            {
                if (op1.contains("+"))
                {
                    op1 = op1.replaceFirst("\\+", "");
                }
                else if (op1.contains("-"))
                {
                    op1 = op1.replaceFirst("\\-", "");
                }
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && NumberUtils.isDigits(op1);
            }
        };
    }

    public static final ValidationOperator IS_INTEGER()
    {
        return new SimpleValidationOperator(NOT_NUMERIC)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && NumberUtils.isDigits(op1);
            }
        };
    }

    public static final ValidationOperator NOT_NULL()
    {
        return new SimpleValidationOperator(IS_NULL)
        {
            public boolean operate(String op1)
            {
                return !Utils.isNull(op1);
            }
        };
    }

    public static final ValidationOperator NOT_EMPTY()
    {
        return new SimpleValidationOperator(IS_EMPTY)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && !Utils.isNullOrEmpty(op1);
            }
        };
    }

    public static final ValidationOperator UNICODE()
    {
        return new SimpleValidationOperator(INVALID_FORMAT)
        {
            public boolean operate(String op1)
            {
                try
                {
                    op1.getBytes(Constant.UTF8_ENCODING);
                }
                catch (UnsupportedEncodingException uee)
                {
                    return false;
                }

                return true;
            }
        };
    }

    public static final ValidationOperator IS_GREATER_THAN(final double op2)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && IS_NUMERIC().operate(op1) && Double.valueOf(op1) > op2;
            }
        };
    }

    public static final ValidationOperator IS_GREATER_OR_EQUAL(final String op2)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && IS_NUMERIC().operate(op1) && NOT_NULL().operate(op2) && NOT_EMPTY().operate(op2) && IS_NUMERIC().operate(op2)
                        && Double.valueOf(op1) >= Double.valueOf(op2);
            }
        };
    }

    public static final ValidationOperator IS_LESS_THAN(final double op2)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && IS_NUMERIC().operate(op1) && Double.valueOf(op1) < op2;
            }
        };
    }

    public static final ValidationOperator IS_LESS_OR_EQUAL(final String op2)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && IS_NUMERIC().operate(op1) && NOT_NULL().operate(op2) && NOT_EMPTY().operate(op2) && IS_NUMERIC().operate(op2)
                        && Double.valueOf(op1) <= Double.valueOf(op2);
            }
        };
    }

    public static final ValidationOperator LENGTH(final int op2)
    {
        return new SimpleValidationOperator(LENGTH_EXCEEDED)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && op1.length() <= op2;
            }
        };
    }

    public static final ValidationOperator LENGTH_BETWEEN(final int op2, final int op3)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && op1.length() >= op2 && op1.length() <= op3;
            }
        };
    }

    public static final ValidationOperator FILE_SIZE(final int op2)
    {
        return new SimpleValidationOperator(SIZE_EXCEEDED)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1);
            }
        };
    }

    public static final ValidationOperator ONE_OF(final String... ops)
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && Lists.<String> newArrayList(ops).contains(op1);
            }
        };
    }

    public static final ValidationOperator MATCHES(final String op2)
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && op1.matches(op2);
            }
        };
    }

    public static final ValidationOperator ADVANCE_ONLY_NOT_NULL()
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                return !Utils.isNull(op1);
            }

            @Override
            public boolean advance(String op1)
            {
                return !Utils.isNull(op1);
            }
        };
    }

    public static final ValidationOperator IS_BETWEEN(final int op2, final int op3)
    {
        return new SimpleValidationOperator(OUT_OF_RANGE)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && IS_NUMERIC().operate(op1) && Double.valueOf(op1) >= op2 && Double.valueOf(op1) <= op3;
            }
        };
    }

    public static final ValidationOperator ONE_SET(final String op2)
    {
        return new SimpleValidationOperator(BOTH_SET)
        {
            public boolean operate(String op1)
            {
                return !(NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && NOT_NULL().operate(op2) && NOT_EMPTY().operate(op2));
            }
        };
    }

    public static final ValidationOperator NOT_EQUAL(final String op2)
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && NOT_NULL().operate(op2) && NOT_EMPTY().operate(op2) && !op1.equals(op2);
            }
        };
    }

    public static final ValidationOperator IS_URL()
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                String urlRegEx = "@^(https)://[^\\s/$.?#].[^\\s]*$@iS";
                return NOT_NULL().operate(op1) && NOT_EMPTY().operate(op1) && op1.matches(urlRegEx);
            }
        };
    }

    public static final ValidationOperator ONE_OF_WITH_SPLIT(final String separator, final String... op2)
    {
        return new SimpleValidationOperator(NOT_VALID)
        {
            public boolean operate(String op1)
            {
                for (String value : op1.split(separator))
                {
                    if (!ONE_OF(op2).operate(value))
                    {
                        return false;
                    }
                }
                return true;
            }
        };
    }

    public static final ValidationOperator MAX_TOKEN_COUNT(final String separator, final int op2)
    {
        return new SimpleValidationOperator(COUNT_EXCEEDED)
        {
            public boolean operate(String op1)
            {
                return op1.split(separator).length <= op2;
            }
        };
    }

    /**
     * Use only with Operations class.
     * 
     * ORDER: NOT_NULL(), IS_EMPTY(), IS_NUMERIC()
     * 
     * @param key
     *            , should not be null.
     * @param value
     * @param operations
     * @throws RequestValidationException
     */
    public static final void validate(String key, String value, ValidationOperator... operations) throws RequestValidationException
    {
        int cursor = 0;
        for (ValidationOperator operation : operations)
        {
            SimpleValidationOperator op = (SimpleValidationOperator) operation;
            if (cursor == 0 && !op.advance(value))
            {
                // avoid further tests
                break;
            }
            test(operation.operate(value), op.getCode(), key);
            cursor++;
        }
    }

    private static final void test(boolean satisfied, OneGroupExceptionCode exceptionCode, String... params) throws RequestValidationException
    {
        if (!satisfied)
        {
            throw new RequestValidationException(exceptionCode, true, params);
        }
    }

    public static void main(String[] args)
    {

        try
        {
            Integer minArea = Integer.MAX_VALUE;
            ;
            Integer maxArea = Integer.MAX_VALUE;
            String minRentPrice = "0";
            String maxRentPrice = "" + Long.MAX_VALUE;
            String minSalePrice = "0" + Long.MAX_VALUE + 1;
            String maxSalePrice = "" + Long.MAX_VALUE;
            System.out.println("minArea:" + minArea);
            System.out.println("maxArea:" + maxArea);
            System.out.println("minRentPrice:" + minRentPrice);
            System.out.println("maxRentPrice:" + maxRentPrice);
            System.out.println("minSalePrice:" + minSalePrice);
            System.out.println("maxSalePrice:" + maxSalePrice);

            validate("min_area", String.valueOf(minArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
            validate("max_area", String.valueOf(maxArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minArea)), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
            validate("min_rent_price", minRentPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
            validate("max_rent_price", maxRentPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minRentPrice)), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
            validate("min_sale_price", minSalePrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
            validate("max_sale_price", maxSalePrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minSalePrice)), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        }
        catch (RequestValidationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
