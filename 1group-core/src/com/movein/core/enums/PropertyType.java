package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertyMarket;

/**
 * The ways how a property can be marketed based on Building Types. The name
 * <code>PropertyType</code> was kept in keeping with the URD. 
 * 
 * TODO : Change the name of the enum to a more appropriate name.
 * 
 * @author nyalfernandes
 *
 */
public enum PropertyType 
{
    RESIDENTIAL,
    APARTMENT,
    INDEPENDENT_FLOOR,
    INDEPENDENT_HOUSE,
    PLOT,
    COMMERCIAL,
    FACTORY,
    OFFICE_SPACE,
    SHOPS,
    SHOW_ROOMS,    	
	LAND,	
	AGRICULTURAL,
	INDUSTRIAL;
	
	 private String name;
	    private static List<String> nameList = new ArrayList<String>();

	    static
	    {
	        for (PropertyType propertyType : values())
	        {
	            nameList.add(propertyType.name);
	        }
	    }

	    PropertyType(String name)
	    {
	        this.name = name;
	    }

	    @Override
	    public String toString()
	    {
	        return this.name;
	    }

	    public static PropertyType convert(String str)
	    {
	        for (PropertyType propertyType : PropertyType.values())
	        {
	            if (propertyType.toString().equals(str))
	            {
	                return propertyType;
	            }
	        }
	        return null;
	    }

	    public static PropertyType find(String name)
	    {
	        for (PropertyType type : PropertyType.values())
	        {
	            if (type.name.equals(name))
	            {
	                return type;
	            }
	        }
	        return null;
	    }

	    public static String[] propertyTypeNames()
	    {
	        return nameList.toArray(new String[nameList.size()]);
	    }
	
}
