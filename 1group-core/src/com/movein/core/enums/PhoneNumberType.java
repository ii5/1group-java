package one.group.core.enums;

/**
 * Types of phone numbers that can be added. Currently only MOBILE is supported.
 * @author nyalfernandes
 *
 */
public enum PhoneNumberType 
{
	MOBILE,
	HOME,
	WORK,
	;
}
