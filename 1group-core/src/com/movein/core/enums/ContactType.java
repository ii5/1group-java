package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum ContactType 
{
	AGENT,
	DEVELOPER,
	ANONYMOUS, // Raw Contact, Consider changing the enum to RAW.
	;
}
