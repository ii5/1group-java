package one.group.core.enums;

public enum MediaType 
{
	AUDIO,
	VIDEO,
	IMAGE;
}
