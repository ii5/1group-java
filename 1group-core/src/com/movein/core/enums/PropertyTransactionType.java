package one.group.core.enums;

/**
 * Ways a property can be marketed for sale.
 * @author nyalfernandes
 *
 */
public enum PropertyTransactionType 
{
	SELL,
	RENT,
	SELL_RENT,
	;
}
