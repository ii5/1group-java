package one.group.core.enums;

/**
 * The property design type. The name was kept to be in sync with the URD.
 * 
 * 
 * @author nyalfernandes
 * 
 * TODO : Change the name to a more appropriate type.
 *
 */
public enum PropertySubType 
{
	APARTMENT,
	DUPLEX,
	BUNGALOW,
	;
}
