package one.group.core.enums.status;

public enum MessageStatus 
{
    NEW, 
	QUEUED,
	SENT,
	RECEIVED, // Receivers Status
	DELIVERED,
	READ,
	;
}
