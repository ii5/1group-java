package one.group.core.enums.status;

/**
 * Denotes active and inactive states.
 * 
 * @author nyalfernandes
 *
 */
public enum Status 
{
	ACTIVE,
	INACTIVE,
	;
}
