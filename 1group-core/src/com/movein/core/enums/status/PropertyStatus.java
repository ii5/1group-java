package one.group.core.enums.status;

/**
 * The status through which a property must flow based on certain criteria.
 * 
 * @author nyalfernandes
 *
 */
public enum PropertyStatus 
{
	OPEN,
	CLOSED,
	EXPIRED,
	ARCHIVED,
}
