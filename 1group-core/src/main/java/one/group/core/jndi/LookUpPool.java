package one.group.core.jndi;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * 
 * @author nyalf
 *
 */
public class LookUpPool 
{
	private BlockingDeque<Context> contextDeque = new LinkedBlockingDeque<Context>();
	private int poolSize = 100;
	
	public void initialise() throws Exception
	{
		for (int i = 0; i < poolSize; i++)
		{
			Properties p = new Properties();
			p.load(new FileInputStream("context.properties"));
			Context context = new InitialContext(p);
			contextDeque.addLast(context);
		}
	}
	
//	public static 
	
}
