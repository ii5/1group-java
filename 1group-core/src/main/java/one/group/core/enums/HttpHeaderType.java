package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum HttpHeaderType
{
    AUTHORISATION("authorization"),
    ;
    
    private String name;
    
    private HttpHeaderType(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
