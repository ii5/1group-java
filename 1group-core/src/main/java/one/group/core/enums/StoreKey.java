package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum StoreKey
{
    URL_ENDPOINT,
    CURRENT_ACCOUNT_ID,
    CURRENT_SESSION_ID,
    FROM_BPO,
    MBEAN_SERVICE_URL,
    MBEAN_OBJECT_NAME,
    CURRENT_WS_ACCOUNT,
    CURRENT_WS_CLIENT,
    ;
}
