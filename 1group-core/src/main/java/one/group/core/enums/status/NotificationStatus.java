package one.group.core.enums.status;

public enum NotificationStatus
{
    READ, UNREAD;
}
