package one.group.core.enums.status;

/**
 * The status of a request as it flows through different modules of the
 * application.
 * 
 * @author nyalfernandes
 * 
 */
public enum RequestStatus
{
    GHOST, RECEIVED, AUTHENTICATING, VALIDATING, PROCESSING, BAD_REQUEST, INSUFFICIENT_DETAILS, ;
}
