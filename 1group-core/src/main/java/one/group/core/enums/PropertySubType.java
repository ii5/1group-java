package one.group.core.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The property design type. The name was kept to be in sync with the URD.
 * 
 * 
 * @author nyalfernandes
 * 
 *         TODO : Change the name to a more appropriate type.
 * 
 */
public enum PropertySubType
{
    APARTMENT(PropertyType.RESIDENTIAL, "apartment"), INDEPENDENT_HOUSE(PropertyType.RESIDENTIAL, "independent_house"), INDEPENDENT_FLOOR(PropertyType.RESIDENTIAL, "independent_floor"), ROW_HOUSE(
            PropertyType.RESIDENTIAL, "row_house"), VILLA(PropertyType.RESIDENTIAL, "villa"), PLOT(PropertyType.RESIDENTIAL, "plot"),

    FACTORY(PropertyType.COMMERCIAL, "factory"), OFFICE_SPACE(PropertyType.COMMERCIAL, "office_space"), SHOPS(PropertyType.COMMERCIAL, "shops"), SHOWROOMS(PropertyType.COMMERCIAL, "showrooms"),

    AGRICULTURAL(PropertyType.LAND, "agricultural"), INDUSTRIAL(PropertyType.LAND, "industrial"), BANGLOW(PropertyType.RESIDENTIAL, "banglow"), BUILDER_FLOOR(PropertyType.RESIDENTIAL, "builder_floor"), SCHOOL(
            PropertyType.COMMERCIAL, "school"), HOSPITAL(PropertyType.COMMERCIAL, "hospital"), HOTEL(PropertyType.COMMERCIAL, "hotel"), WAREHOUSE(PropertyType.COMMERCIAL, "warehouse"), OFFICE(
            PropertyType.COMMERCIAL, "office"), IT_PARK(PropertyType.COMMERCIAL, "it_park"),

    UNDEFINED(PropertyType.UNDEFINED, ""),
    RESIDENT_PLOT(PropertyType.LAND, "resident_plot"),
    ;

    private String name;
    private PropertyType propertyType;
    private static List<String> nameList = new ArrayList<String>();
    private static Map<PropertyType, List<PropertySubType>> mapOfTypeVsSubType = new HashMap<PropertyType, List<PropertySubType>>();

    static
    {
        for (PropertySubType subType : values())
        {
            nameList.add(subType.name);
            if (mapOfTypeVsSubType.containsKey(subType.getPropertyType()))
            {
                mapOfTypeVsSubType.get(subType.getPropertyType()).add(subType);
            }
            else
            {
                List<PropertySubType> subTypeList = new ArrayList<PropertySubType>();
                subTypeList.add(subType);
                mapOfTypeVsSubType.put(subType.getPropertyType(), subTypeList);
            }
        }
    }

    PropertySubType(PropertyType propertyType, String name)
    {
        this.name = name;
        this.propertyType = propertyType;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static PropertySubType convert(String str)
    {
        for (PropertySubType subType : PropertySubType.values())
        {
            if (subType.toString().equals(str))
            {
                return subType;
            }
        }
        return null;
    }

    public static String[] subTypeNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public static List<PropertySubType> getSubTypesOf(PropertyType type)
    {
        return mapOfTypeVsSubType.get(type);
    }
}
