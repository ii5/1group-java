package one.group.core.enums;

public enum MonitorApplicationType {

	CACHE,
	DATABASE,
	KAKFA,
	ZOOKEEPER,
	SYNC;
}
