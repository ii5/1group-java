package one.group.core.enums;

public enum ApplicationActionType 
{
	SEND_EMAIL,
	SEND_SMS;
}
