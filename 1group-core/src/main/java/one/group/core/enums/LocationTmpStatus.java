/**
 * 
 */
package one.group.core.enums;

public enum LocationTmpStatus
{
    ACTIVE("active"), INACTIVE("inactive"), ;

    private String name;

    private LocationTmpStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

}
