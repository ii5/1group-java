package one.group.core.enums;

/**
 * 
 * @author sanilshet
 *
 */
public enum RoomType
{

    RK("rk"), BHK("bhk");

    private String name;

    RoomType(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static RoomType convert(String str)
    {
        for (RoomType roomType : RoomType.values())
        {
            if (roomType.toString().equals(str))
            {
                return roomType;
            }
        }
        return null;
    }

    public static String[] roomTypeNames()
    {
        return new String[] { RoomType.BHK.toString(), RoomType.RK.toString() };
    }
}
