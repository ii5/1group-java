package one.group.core.enums;

public enum StarType
{
    BROADCAST;

    private StarType type;

    /**
     * @return the type
     */
    public StarType getType()
    {
        return type;
    }
}
