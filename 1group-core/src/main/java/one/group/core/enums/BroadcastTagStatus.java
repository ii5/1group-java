package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum BroadcastTagStatus
{
    DRAFT("draft"),
    ADDED("added"),
    DISCARDED("discarded"),
    CLOSED("closed"),
    NOTINAPP("notinapp"), ;
    
    private String name;
    private static Map<String, BroadcastTagStatus> nameVsEnumMap = new HashMap<String, BroadcastTagStatus>();
    
    static
    {
        for (BroadcastTagStatus s : values())
        {
            nameVsEnumMap.put(s.toString(), s); 
        }
    }

    private BroadcastTagStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
    
    public static BroadcastTagStatus parse(String name)
    {
        return nameVsEnumMap.get(name);
    }
}
