package one.group.core.enums;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum SortField
{
    GROUP_SOURCE("group_source", "source_s", "source", new String[] {"source"}),
    GROUP_NAME("group_name", "groupName", "groupName", new String[] {"groupName"}),
    GROUP_ASSIGNED_TO("assigned_to", "assignedTo", "assignedTo", new String[] {"assignedTo"}),
    CITY_ID("city_id", "cityId", "cityId", new String[] {"city", "id"}),
    CITY_NAME("city_name", "cityName", "cityName", new String[] {"city", "cityName"}),
    LOCATION_ID("location_id", "locationId", "locationId", new String[] {"location", "id"}),
    LOCATION_NAME("location_name", "locationName", "locationName", new String[] {"location", "name"}),
    GROUP_STATUS("group_status", "status_s", "status", new String[] {"status"}),
    GROUP_UNREAD_MSG_COUNT("unreadmsg_count", "unreadmsgCount", "unreadmsg_count", new String[] {"unreadmsgCount"}),
    GROUP_TOTAL_MSG_COUNT("totalmsg_count", "totalmsgCount", "totalmsgCount", new String[] {"totalmsgCount"}),
    GROUP_UNREAD_FROM("unread_from", "unreadFrom", "unreadFrom", new String[] {"unreadFrom"}),
    GROUP_BPO_STATUS_UPDATE_TIME("bpo_status_update_time", "bpoStatusUpdateTime", "bpoStatusUpdateTime", new String[] {"bpoStatusUpdateTime"}),
    MESSAGE_SENT_TIME("message_sent_time", "messageSentTime", "messageSentTime", new String[] {"messageSentTime"}),
    BROADCAST_TYPE("broadcast_type", "broadcastType", "broadcastType", new String[] {"broadcastType"}),
    BROADCAST_ID("broadcast_id", "broadcastId", "broadcastId", new String[]{"broadcast", "id"}),
    MESSAGE_TYPE("message_type", "messageType_s", "messageType", new String[] {"messageType"}),
    PROPERTY_TYPE("property_type", "propertyType_s", "propertyType", new String[] {"propertyType"}),
    PROPERTY_SUB_TYPE("property_sub_type", "propertySubType_s", "propertySubType", new String[] {"broadcast", "propertySubType"}),
    TRANSACTION_TYPE("transaction_type", "transactionType_s", "transactionType", new String[] {"transactionType"}),
    ROOMS("rooms", "rooms", "rooms", new String[] {"rooms"}),
    SIZE("size", "size", "size", new String[] {"size"}),
    CREATED_BY("created_by", "createdBy", "createdBy", new String[] {"createdBy"}),
    CREATED_TIME("created_time", "createdTime", "createdTime", new String[] {"createdTime"}),
    UPDATED_BY("updated_by", "updatedBy", "updatedBy", new String[] {"updatedBy"}),
    UPDATED_TIME("updated_time", "updatedTime", "updatedTime", new String[] {"updatedTime"}),
    USERNAME("username", "userName", "userName", new String[] {"admin", "userName"}),
    EMAIL("email", "email", "email", new String[] {"email"}),
    USER_STATUS("user_status", "status_s", "status", new String[] {"status"}),
    ROLE("role", "role", "role", new String[] {"role"}),
    PRIORITY("priority", "priority", "priority", new String[] {"priority"}),
    UNREADMSGCOUNT("unreadmsg_count", "unreadmsgCount", "unreadmsgCount", new String[] {"unreadmsgCount"}),
    MESSAGE_CREATED_BY("message_created_by", "messageCreatedBy", "messageCreatedBy", new String[] {"broadcast", "messageCreatedBy"}),
    BRAODCAST_TAG_ADDED_BY("broadcast_tag_added_by", "addedBy", "addedBy", new String[]{"tag","addedBy"}),
    BRAODCAST_TAG_ADDED_TIME("broadcast_tag_added_time", "addedTime", "addedTime", new String[]{"tag","addedTime"}),
    BRAODCAST_TAG_CLOSED_BY("broadcast_tag_closed_by", "closedBy", "closedBy", new String[]{"tag","closedBy"}),
    BRAODCAST_TAG_CLOSED_TIME("broadcast_tag_closed_time", "closedTime", "closedTime", new String[]{"tag","closedTime"}),
    BROADCAST_TAG_STATUS("broadcast_tag_status", "status", "status", new String[] {"broadcastTag", "status"}),
    
    ;
    
    private String name;
    private String solrFieldName;
    private String dbFieldName;
    private String[] dbDomainNames;
    private static Map<String, SortField> nameVsEnumMap = new HashMap<String, SortField>();
    
    static
    {
        for (SortField s : values())
        {
            nameVsEnumMap.put(s.toString(), s);
        }
    }
    
    private SortField(String name, String solrFieldName, String dbFieldName, String[] dbDomainNames)
    {
        this.name = name;
        this.solrFieldName = solrFieldName;
        this.dbFieldName = dbFieldName;
        this.dbDomainNames = dbDomainNames;
    }
    
    public String[] getDbDomainNames()
    {
        return dbDomainNames;
    }
    
    public static SortField parse(String name)
    {
        return nameVsEnumMap.get(name);
    }
    
    public static Collection<String> nameList()
    {
        return nameVsEnumMap.keySet();
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
    public String solrField()
    {
        return this.solrFieldName;
    }
    
    public String dbField()
    {
        return this.dbFieldName;
    }
    
}
