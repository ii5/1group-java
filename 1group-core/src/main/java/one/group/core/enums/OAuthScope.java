package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum OAuthScope
{
    STANDARD_CLIENT("standard_client");

    private String name;
    private static Map<String, OAuthScope> map = new HashMap<String, OAuthScope>();

    static
    {
        for (OAuthScope type : values())
        {
            map.put(type.getName(), type);
        }
    }

    OAuthScope(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
    
    public static OAuthScope getType(String name)
    {
        return map.get(name);
    }
}
