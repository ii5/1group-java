package one.group.core.enums;

/**
 * <code>UserRole</code> identifies the different types of role a user of movein
 * can fit into.
 * 
 * The user role are mutually exclusive, so a user with multiple roles should be
 * granted each role.
 * 
 * @author nyalfernandes
 * 
 */
public enum UserRole
{
    CUSTOMER_SUPPORT, MOVEIN_USER, ADMIN;
}
