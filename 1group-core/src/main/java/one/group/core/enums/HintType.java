package one.group.core.enums;

/**
 * Different types of hint that can be provided to clients.
 * 
 * @author nyalfernandes
 *
 */
public enum HintType
{
    FETCH("fetch"),
    DELETE("delete"),
    ;
    
    private String name;
    
    HintType(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
}
