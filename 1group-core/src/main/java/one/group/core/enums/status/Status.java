package one.group.core.enums.status;

/**
 * Denotes active and inactive states.
 * 
 * @author nyalfernandes
 * 
 */
public enum Status
{
    ACTIVE("active"),
    INACTIVE("inactive"),
    UNREGISTERED("unregistered"),
    SEEDED("seeded"),
    ADMIN("admin");
    
    private String name;

    private Status(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }
}
