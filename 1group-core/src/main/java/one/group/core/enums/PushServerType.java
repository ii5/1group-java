package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum PushServerType
{
    PUSHER, APNS, GCM, WEBSOCKET;
}
