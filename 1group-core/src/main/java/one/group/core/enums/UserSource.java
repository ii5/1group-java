package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSource
{
    APP("app"),
    BPO("bpo"),
    SYNC("sync");

    private String name;
    private static Map<String, UserSource> valueVsEnum = new HashMap<String, UserSource>();

    static
    {
        for (UserSource g : values())
        {
            valueVsEnum.put(g.toString(), g);
        }
    }

    private UserSource(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }
    
    public static UserSource parse(String name)
    {
        return valueVsEnum.get(name);
    }
    
    public static String[] stringNames()
    {
        return valueVsEnum.keySet().toArray(new String[valueVsEnum.keySet().size()]);
    }
}
