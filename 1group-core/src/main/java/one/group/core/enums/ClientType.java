package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Identifies the different client device types.
 * 
 * @author nyalfernandes
 * 
 */
public enum ClientType
{
    ANDROID("android", "mobile"),
    IOS("ios", "mobile"),
    WEB("web", "web");

    private String name;
    private String platform;

    ClientType(String name, String platform)
    {
        this.name = name;
        this.platform = platform;
    }

    public String toString()
    {
        return this.name;
    }

    public String getPlatform()
    {
        return this.platform;
    }

    public static String[] platforms()
    {
        List<String> ps = new ArrayList<String>();
        for (ClientType type : values())
        {
            if (!ps.contains(type.getPlatform()))
            {
                ps.add(type.getPlatform());
            }
        }

        return ps.toArray(new String[ps.size()]);
    }

    public static String[] names()
    {
        List<String> ps = new ArrayList<String>();
        for (ClientType type : values())
        {
            if (ps.contains(type.toString()))
            {
                ps.add(type.toString());
            }
        }

        return ps.toArray(new String[ps.size()]);
    }

    public static String[] mobileNames()
    {
        return new String[] { ANDROID.toString(), IOS.toString() };
    }

    public static ClientType convert(String clientType)
    {
        for (ClientType type : ClientType.values())
        {
            if (type.toString().equals(clientType))
            {
                return type;
            }
        }
        return null;
    }
}
