package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

public enum Amenity
{
    GARDEN("garden"),
    PARKING("parking"),
    POOL("pool"),
    INTERCOM("intercom"),
    SECURITY("security"),
    CLUBHOUSE("clubhouse"),
    GYM("gym"),
    LIFT("lift"),
    PLAY_AREA("play_area"),
    MAINTENANCE_STAFF("maintenance_staff"),
    RAIN_HARVESTING("rain_harvesting");

    private String name;

    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (Amenity type : values())
        {
            nameList.add(type.name);
        }
    }

    Amenity(String name)
    {
        this.name = name;
    }

    public static Amenity convert(String str)
    {
        for (Amenity amenity : Amenity.values())
        {
            if (amenity.toString().equals(str))
            {
                return amenity;
            }
        }
        return null;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static String[] amenityNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
