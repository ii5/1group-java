package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum OAuthTokenType
{
    BEARER("bearer"), ;

    private String name;
    private static Map<String, OAuthTokenType> map = new HashMap<String, OAuthTokenType>();
    
    static
    {
        for (OAuthTokenType type : values())
        {
            map.put(type.getName(), type);
        }
    }

    OAuthTokenType(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
    
    public static OAuthTokenType getType(String name)
    {
        return map.get(name);
    }
}
