package one.group.core.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum MediaExtensionType
{
    jpg("jpg", "jpeg", "JPG", "JPEG"),
    png("png", "PNG");

    private static List<String> nameList = new ArrayList<String>();
    private String[] names;

    static
    {
        for (MediaExtensionType type : values())
        {
            nameList.addAll(Arrays.asList(type.names));
        }
    }

    MediaExtensionType(String... values)
    {
        this.names = values;
    }

    public static MediaExtensionType find(String name)
    {
        for (MediaExtensionType type : MediaExtensionType.values())
        {
            if (nameList.contains(name))
            {
                return type;
            }
        }
        return null;
    }

    public static String[] mediaExtentionName()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
