package one.group.core.enums;

public enum PhoneNumberSource
{
    APP("app"), WHATSAPP("whatsapp"), BPO("bpo");

    private String name;

    private PhoneNumberSource(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }
}
