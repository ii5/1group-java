package one.group.core.enums;

/**
 * Possible message types.
 * 
 * @author nyalfernandes
 * 
 */
public enum MessageType
{
    TEXT("text"), PHONE_CALL("phone_call"), ACCOUNT("account"), BROADCAST("broadcast"), MEDIA("media"), PHOTO("photo"), PROPERTY_LISTING("property_listing");

    private String name;

    MessageType(final String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }

    public static MessageType parse(String name)
    {
        for (MessageType type : values())
        {
            if (type.getName().equals(name))
            {
                return type;
            }
        }
        return null;
    }

    public static String[] MessageTypes()
    {
        return new String[] { MessageType.TEXT.toString(), MessageType.PHONE_CALL.toString(), MessageType.PHOTO.toString(), MessageType.BROADCAST.toString() };
    }
}
