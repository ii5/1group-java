package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class Parameters
{
    /**
     * GENERAL: Data in any object form that needs to be pushed to clients.
     */
    public static final String PUSH_SERVICE_DATA = "PUSH_SERVICE_DATA";

    /**
     * GENERAL: Array of {@link PushServerType} including all mediums through
     * which this data should be sent.
     */
    public static final String PUSH_SERVICE_TYPE_LIST = "PUSH_SERVICE_TYPES";;

    /**
     * {@link PushServerType#PUSHER}: All channels of pusher through which the
     * data has to be sent.
     */
    public static final String PUSHER_CHANNEL_LIST = "PUSHER_CHANNEL_LIST";

    /**
     * 
     * {@link PushServerType#PUSHER}: The event name of a channel if any.
     */
    public static final String PUSHER_EVENT_NAME = "PUSHER_EVENT_NAME";

    /**
     * {@link PushServerType#GCM}: The registration ids of devices.
     */
    public static final String GCM_DEVICE_LIST = "GCM_DEVICE_LIST";
}
