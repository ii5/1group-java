package one.group.core.enums.status;

public enum DeviceStatus
{
    ACTIVE("active"),
    INACTIVE("inactive"),
    UNINSTALLED("uninstalled"), ;

    private String name;

    private DeviceStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

}
