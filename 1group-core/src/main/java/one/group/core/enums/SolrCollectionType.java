package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum SolrCollectionType
{

    GETTINGSTARTED("gettingstarted"), MESSAGES("MESSAGES"), TECHPRODUCTS("techproducts"), ONEGROUP("onegroup");

    private String name;

    private SolrCollectionType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
