package one.group.core.enums.status;

/**
 * The status through which a property must flow based on certain criteria.
 * 
 * @author nyalfernandes
 * 
 */
public enum BroadcastStatus
{
    ACTIVE("active"), CLOSED("closed"), EXPIRED("expired"), ARCHIVED("archived"), RENEW("renew");

    private String name;

    BroadcastStatus(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static BroadcastStatus parse(String str)
    {
        for (BroadcastStatus propertyStatus : BroadcastStatus.values())
        {
            if (propertyStatus.toString().equals(str))
            {
                return propertyStatus;
            }
        }
        return null;
    }
}
