/**
 * 
 */
package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashishthorat
 *
 */
public enum WaSyncStatus
{
    NEW("new"), SYNCLOST("sync_lost");

    private String name;

    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (WaSyncStatus type : values())
        {
            nameList.add(type.name);
        }
    }

    WaSyncStatus(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static WaSyncStatus convert(String str)
    {
        for (WaSyncStatus waSyncStatus : WaSyncStatus.values())
        {
            if (waSyncStatus.toString().equals(str))
            {
                return waSyncStatus;
            }
        }
        return null;
    }

    public static String[] bpoToolStatusNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
