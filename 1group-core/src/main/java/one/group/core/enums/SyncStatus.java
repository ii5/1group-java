package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum SyncStatus
{
    NEW("new"),
    PAUSED("paused"),
    SYNC_LOST("sync_lost"),
    UNDEFINED("undefined"),
    ;
    
    private String name;
    private static Map<String, SyncStatus> valueVsEnumMap = new HashMap<String, SyncStatus>();
    
    static
    {
        for (SyncStatus s : values())
        {
            valueVsEnumMap.put(s.toString(), s);
        }
    }
    
    SyncStatus(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
    
    public static SyncStatus parse(String s)
    {
        return valueVsEnumMap.get(s);
    }
    
    public static String[] stringNames()
    {
        return valueVsEnumMap.keySet().toArray(new String[valueVsEnumMap.keySet().size()]);
    }
}
