package one.group.core.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum APIVersion
{
    VERSION2("v2", false, true), 
    VERSION2_1("v2.1", false, true), // November 19 2016
    VERSION2_2("v2.2", true, true), // November 23 2016
    VERSION2_3("v2.3", false, false), // December 03 2015 : MOV-2685
    VERSION2_4("v2.4", false, false), // December 24 2015
    VERSION2_5("v2.5", false, false), // January 13 2016
    VERSION2_6("v2.6", false, false), // For Roll Back changes - Jan-15-2016
    VERSION2_7("v2.7", false, false), // preserve token & new cities Feb-24-2016
    VERSION2_8("v2.8", false, false), // news feed update march - 14 - 2016
    VERSION2_9("v2.9", false, false), // Network layer integration march-23-2016
    VERSION2_10("v2.10", false, false), // NewsFeed Expiry Time set
    VERSION1("v1", false, false), 
    VERSION3("v3", false, false), 
    VERSION4("v4", false, false), 
    VERSION5("v5", false, false), ;

    private static Set<APIVersion> allSupportedVersionSet = new HashSet<APIVersion>();
    private static Set<APIVersion> allVersionsSet = new HashSet<APIVersion>();
    private static Map<String, APIVersion> allVersionMap = new HashMap<String, APIVersion>();

    private String version;
    private boolean isCurrentVersion;
    private boolean isSupportedVersion;

    private APIVersion(String version, boolean isCurrentVersion, boolean isSupportedVersion)
    {
        this.version = version;
        this.isCurrentVersion = isCurrentVersion;
        this.isSupportedVersion = isSupportedVersion;
    }

    static
    {
        int currentVersionCounter = 0;

        for (APIVersion apiVersion : values())
        {
            allVersionsSet.add(apiVersion);
            allVersionMap.put(apiVersion.getVersion(), apiVersion);
            if (apiVersion.isSupportedVersion())
            {
                allSupportedVersionSet.add(apiVersion);
            }

            if (apiVersion.isCurrentVersion == true)
            {
                currentVersionCounter++;
            }

            if (currentVersionCounter > 1)
            {
                throw new IllegalStateException("isCurrentVersion set multiple times.");
            }
        }
    }

    public String getVersion()
    {
        return this.version;
    }

    public boolean isCurrentVersion()
    {
        return this.isCurrentVersion;
    }

    public boolean isSupportedVersion()
    {
        return this.isSupportedVersion;
    }

    public static boolean isVersionCurrent(String version)
    {
        APIVersion v = allVersionMap.get(version);

        return v != null && v.isCurrentVersion();
    }

    public static boolean isVersionSupported(String version)
    {
        APIVersion v = allVersionMap.get(version);

        return v != null && v.isSupportedVersion();
    }

    public static Set<APIVersion> getAllSupportedVersion()
    {
        return allSupportedVersionSet;
    }

    public static APIVersion parse(String version)
    {
        return allVersionMap.get(version);
    }

    @Override
    public String toString()
    {
        return getVersion();
    }

    public static APIVersion currentVersion()
    {
        for (APIVersion version : allVersionsSet)
        {
            if (version.isCurrentVersion())
            {
                return version;
            }
        }

        return null;
    }

}
