package one.group.core.enums;

public enum ApplicationAction 
{
	SEND_EMAIL,
	SEND_SMS;
}
