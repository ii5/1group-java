package one.group.core.enums;

public enum LeaseType
{
    SELL("sell"), RENT("rent");

    private String val;

    private LeaseType(String val)
    {
        this.val = val;

    }

    public String getValue()
    {
        return val;
    }

}
