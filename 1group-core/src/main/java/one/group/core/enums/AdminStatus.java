/**
 * 
 */
package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum AdminStatus
{

    ACTIVE("active"), INACTIVE("inactive"), ;

    private String val;
    
    private static Map<String, AdminStatus> nameVsEnumMap = new HashMap<String, AdminStatus>();
    
    static
    {
        for (AdminStatus s : values())
        {
            nameVsEnumMap.put(s.toString(), s);
        }
    }

    private AdminStatus(String val)
    {
        this.val = val;
    }

    @Override
    public String toString()
    {
        return this.val;
    }

    public String getValue()
    {
        return this.val;
    }
    
    public static AdminStatus parse(String s)
    {
        return nameVsEnumMap.get(s);
    }
    
}
