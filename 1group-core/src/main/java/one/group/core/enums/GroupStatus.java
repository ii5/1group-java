package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum GroupStatus
{
    NEW("new"), ACTIVE("active"), IRRELEVANT("irrelevant"), INSUFFICIENT_MESSAGES("insufficient_messages"), SYNC_DISABLED("sync_disabled"), INACTIVE("inactive");

    private String name;
    private static Map<String, GroupStatus> nameVsEnumMap = new HashMap<String, GroupStatus>();

    static
    {
        for (GroupStatus s : values())
        {
            nameVsEnumMap.put(s.toString(), s);
        }
    }

    private GroupStatus(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static GroupStatus parse(String name)
    {
    	if (name == null)
    	{
    		return null;
    	}
        name = name.toLowerCase();
        name = name.replace(" ", "_");
        return nameVsEnumMap.get(name);
    }

    public static GroupStatus convert(String name)
    {
        return nameVsEnumMap.get(name);
    }

    public static String[] stringValues()
    {
        return nameVsEnumMap.keySet().toArray(new String[nameVsEnumMap.size()]);
    }

}
