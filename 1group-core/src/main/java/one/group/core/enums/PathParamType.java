package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum PathParamType
{
    VERSION("version"),
    CHAT_THREAD_IDS("chat_thread_ids"), 
    BROADCAST_IDS("broadcast_ids"), 
    ACCOUNT_IDS("account_ids"), 
    SEARCH_IDS("search_ids"), 
    MESSAGE_IDS("message_ids"),
    PHONE_NUMBERS("phone_numbers"),
    SHORT_REFERENCE("short_reference"),
    ;

    private String name;

    private PathParamType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
