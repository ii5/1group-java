package one.group.core.enums.status;

public enum NearByLocalityStatus
{
    ACTIVE,
    INACTIVE;
}
