package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum GroupSelectionStatus
{
    SELECTED("selected"),
    NOT_SELECTED("not_selected"),
    ;
    
    private String name;
    private static Map<String, GroupSelectionStatus> valueVsEnum = new HashMap<String, GroupSelectionStatus>();
    
    static
    {
        for (GroupSelectionStatus g : values())
        {
            valueVsEnum.put(g.toString(), g);
        }
    }
    
    private GroupSelectionStatus(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
    public static GroupSelectionStatus parse(String name)
    {
        return valueVsEnum.get(name);
    }
    
    public static String[] stringNames()
    {
        return valueVsEnum.keySet().toArray(new String[valueVsEnum.keySet().size()]);
    }
}
