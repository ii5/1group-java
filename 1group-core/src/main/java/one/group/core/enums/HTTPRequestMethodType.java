package one.group.core.enums;

/**
 * Identifies the method types of a HTTP request.
 * 
 * @author nyalfernandes
 * 
 */
public enum HTTPRequestMethodType
{
    OPTIONS,
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    TRACE,
    CONNECT, ;

}
