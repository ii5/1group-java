package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum AccountType
{
    AGENT("agent"),
    DEVELOPER("developer"),
    LEAD("lead"),
    ANONYMOUS("anonymous"),
    ACCOUNT("account"),
    ;

    private String name;

    private AccountType(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }
}
