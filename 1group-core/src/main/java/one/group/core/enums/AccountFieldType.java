/**
 * 
 */
package one.group.core.enums;

/**
 * @author ashishthorat
 *
 */
public enum AccountFieldType
{
    FULLNAME("fullname"), URL("url"), ;

    private String name;

    private AccountFieldType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

}
