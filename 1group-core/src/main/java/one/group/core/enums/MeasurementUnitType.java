package one.group.core.enums;

/**
 * The measurement unit types.
 * 
 * @author nyalfernandes
 * 
 *         TODO : Add descriptions.
 * 
 */
public enum MeasurementUnitType
{
    SQFT("sqft"), SQMT("sqmts");

    private String displayString;

    MeasurementUnitType(final String displayString)
    {
        this.displayString = displayString;
    }

    public String getDisplayString()
    {
        return this.displayString;
    }
}
