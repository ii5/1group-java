package one.group.core.enums;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum FilterField
{

    GROUP_SOURCE("group_source", "source_s", "source", new String[] { "source" }, GroupSource.class), GROUP_NAME("group_name", "groupName", "groupName", new String[] { "groupName" }, String.class), GROUP_ASSIGNED_TO(
            "assigned_to", "assignedTo", "assignedTo", new String[] { "assignedTo" }, String.class), GROUP_ID("group_id", "groupId", "groupId", new String[] { "group", "id" }, String.class), CITY_ID(
            "city_id", "cityId", "cityId", new String[] { "city", "id" }, String.class), CITY_NAME("city_name", "cityName", "cityName", new String[] { "city", "cityName" }, String.class), LOCATION_ID(
            "location_id", "locationId", "locationId", new String[] { "location", "id" }, String.class), GROUP_STATUS("group_status", "status_s", "status", new String[] { "status" },
            GroupStatus.class), USER_STATUS("user_status", "status_s", "status", new String[] { "status" }, AdminStatus.class), ID("id", "id", "id", new String[] { "id" }, String.class), MESSAGE_TYPE(
            "message_type", "messageType_s", "messageType", new String[] { "messageType" }, MessageType.class), BROADCAST_ID("broadcast_id", "broadcastId", "broadcastId", new String[] { "broadcast",
            "id" }, String.class), BRAODCAST_TAG_ADDED_BY("broadcast_tag_added_by", "addedBy", "addedBy", new String[] { "tag", "addedBy" }, String.class), BRAODCAST_TAG_ADDED_TIME(
            "broadcast_tag_added_time", "addedTime", "addedTime", new String[] { "tag", "addedTime" }, Date.class), BRAODCAST_TAG_CLOSED_BY("broadcast_tag_closed_by", "closedBy", "closedBy",
            new String[] { "tag", "closedBy" }, String.class), BRAODCAST_TAG_CLOSED_TIME("broadcast_tag_closed_time", "closedTime", "closedTime", new String[] { "tag", "closedTime" }, Date.class), BROADCAST_TYPE(
            "broadcast_type", "broadcastType", "broadcastType", new String[] { "broadcastType" }, BroadcastType.class), BROADCAST_TAG_STATUS("broadcast_tag_status", "status", "status",
            new String[] { "status" }, BroadcastTagStatus.class), MESSAGE_CREATED_BY("message_created_by", "messageCreatedBy", "messageCreatedBy", new String[] { "broadcast", "messageCreatedBy" },
            String.class), MESSAGE_STATUS("message_status", "messageStatus_s", "messageStatus", new String[] { "message", "status" }, MessageStatus.class), SENDER_PHONE_NUMBER("sender_phone_number",
            "senderPhoneNumber", "senderPhoneNumber", null, String.class), PROPERTY_TYPE("property_type", "propertyType_s", "propertyType", new String[] { "propertyType" }, PropertyType.class), PROPERTY_SUB_TYPE(
            "property_sub_type", "propertySubType_s", "propertySubType", new String[] { "broadcast", "propertySubType" }, PropertySubType.class), TRANSACTION_TYPE("transaction_type",
            "transactionType_s", "transactionType", new String[] { "transactionType" }, PropertyTransactionType.class), ROOMS("rooms", "rooms", "rooms", new String[] { "rooms" }, Rooms.class), CREATED_BY(
            "created_by", "createdBy", "createdBy", new String[] { "createdBy" }, String.class), CREATED_TIME("created_time", "createdTime", "createdTime", new String[] { "createdTime" }, Date.class), UPDATED_BY(
            "updated_by", "updatedBy", "updatedBy", new String[] { "updatedBy" }, String.class), UPDATED_TIME("updatedTime", "updatedTime", "updatedTime", new String[] { "updatedTime" }, Date.class), USERNAME(
            "username", "userName", "userName", new String[] { "admin", "userName" }, String.class), EMAIL("email", "email", "email", new String[] { "email" }, String.class), PRIORITY("priority",
            "priority", "priority", new String[] { "priority" }, Integer.class), UNREADMSGCOUNT("unreadmsg_count", "unreadmsgCount", "unreadmsgCount", new String[] { "unreadmsgCount" }, Integer.class), TOTALMSGCOUNT(
            "totalmsg_count", "totalmsgCount", "totalmsgCount", new String[] { "totalmsgCount" }, Integer.class), UNREADMSGCOUNT_QA("unreadmsg_count_qa", "unreadmsgCountQa", "unreadmsgCountQa",
            new String[] { "unreadmsgCountQa" }, Integer.class), EVER_SYNC("ever_sync", "everSync", "everSync", new String[] { "everSync" }, Boolean.class), AUTH_ROLES("auth_roles", "roles", "roles",
            new String[] { "auth", "roles" }, String.class), BPO_MESSAGE_STATUS("bpo_message_status", "bpoMessageStatus_s", "bpoMessageStatus", new String[] { "message", "bpoMessageStatus" },
            BpoMessageStatus.class), DRAFTED_TIME("drafted_time", "draftedTime", "draftedTime", new String[] { "draftedTime" }, Date.class), DRAFTED_BY("drafted_by", "draftedBy", "draftedBy",
            new String[] { "draftedBy" }, String.class), SIZE("size", "size", "size", new String[] { "size" }, Long.class), ;

    private String name;
    private String solrFieldName;
    private String dbFieldName;
    private Class<?> type;
    private String[] dbDomainNames;

    private static Map<String, FilterField> nameVsEnumMap = new HashMap<String, FilterField>();

    static
    {
        for (FilterField s : values())
        {
            nameVsEnumMap.put(s.toString(), s);
        }
    }

    private FilterField(String name, String solrFieldName, String dbFieldName, String[] dbDomainNames, Class<?> type)
    {
        this.name = name;
        this.solrFieldName = solrFieldName;
        this.dbFieldName = dbFieldName;
        this.dbDomainNames = dbDomainNames;
        this.type = type;
    }

    public String[] getDbDomainNames()
    {
        return dbDomainNames;
    }

    public static FilterField parse(String name)
    {
        return nameVsEnumMap.get(name);
    }

    public static Collection<String> nameList()
    {
        return nameVsEnumMap.keySet();
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public String solrField()
    {
        return this.solrFieldName;
    }

    public String dbField()
    {
        return this.dbFieldName;
    }

    public Class<?> getType()
    {
        return type;
    }

    public void setType(Class<?> type)
    {
        this.type = type;
    }

    public Set<?> castIntoType(Set<String> values)
    {

        if (this.type.isEnum())
        {
            Set<Enum> enumSet = new HashSet<Enum>();
            for (String value : values)
            {
                enumSet.add(Enum.valueOf((Class<Enum>) type, value.toUpperCase()));
            }

            return enumSet;
        }
        else if (this.type.equals(Date.class))
        {
            Set<Date> dateSet = new HashSet<Date>();
            for (String value : values)
            {
                dateSet.add(new Date(Long.parseLong(value)));
            }

            return dateSet;
        }
        else
        {
            return values;
        }

        // return null;

    }

    public Object castIntoTypeSingle(String value)
    {

        if (this.type.isEnum())
        {
            return Enum.valueOf((Class<Enum>) type, value.toUpperCase());
        }
        else
        {
            return value;
        }
    }

    public static void main(String[] args)
    {
        Set<String> test = new HashSet<String>();
        test.add("1");
        test.add("2");

        System.out.println(FilterField.SIZE.castIntoType(test));
    }

}
