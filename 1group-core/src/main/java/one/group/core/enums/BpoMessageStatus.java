/**
 * 
 */
package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ashishthorat
 * 
 */
public enum BpoMessageStatus
{

    READ("read"), BULK_READ("bulk_read"), UNREAD("unread"), INSUFFICIENT_INFO("insufficient_info"), UNQUALIFIED("unqualified"), EXPIRED("expired"), JUNK("junk");

    private String name;
    private static Map<String, BpoMessageStatus> valueVsEnumMap = new HashMap<String, BpoMessageStatus>();

    static
    {
        for (BpoMessageStatus s : values())
        {
            valueVsEnumMap.put(s.toString(), s);
        }
    }

    BpoMessageStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static BpoMessageStatus parse(String name)
    {
        return valueVsEnumMap.get(name);
    }

    public static String[] stringValues()
    {
        return valueVsEnumMap.keySet().toArray(new String[valueVsEnumMap.keySet().size()]);
    }
}
