package one.group.core.enums;

/**
 * Parameters that are a part of the REST request.
 * 
 * @author nyalfernandes
 * 
 */
public enum RequestParameters
{
    ACCOUNT_ID("account_id"), CHAT_THREAD_ID("chat_thread_id"), CLIENT_ID("client_id"),

    // SORTING
    SORT("sort"),

    // PAGINATION
    OFFSET("offset"),
    LIMIT("limit"),

    ;

    private String name;

    RequestParameters(final String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }
}
