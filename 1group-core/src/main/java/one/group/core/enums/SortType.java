package one.group.core.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum SortType
{

    ASCENDING("asc"),
    DESCENDING("desc"),
   ;
    
    private String name;
    private static Map<String, SortType> nameVsEnumMap = new HashMap<String, SortType>();
    
    static
    {
        for (SortType s : values())
        {
            nameVsEnumMap.put(s.toString(), s);
        }
    }
    
    private SortType(String name)
    {
        this.name = name;
    }
    
    public static SortType parse(String name)
    {
        return nameVsEnumMap.get(name);
    }
    
    public static Collection<String> nameList()
    {
        return nameVsEnumMap.keySet();
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
