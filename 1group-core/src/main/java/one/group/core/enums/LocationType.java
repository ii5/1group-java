package one.group.core.enums;

public enum LocationType
{
    LOCALITY("locality"),
    SUB_LOCALITY("sublocality"),
    ROAD("road"),
    PROJECT("project");

    private String name;

    LocationType(String name)
    {
        this.name = name;

    }
    
    @Override
    public String toString() 
    {
    	return this.name;
    }

}
