package one.group.core.enums;

public enum ClientUpdateType
{
    SOFT_UPDATE("soft_update"),
    HARD_UPDATE("hard_update"),
    NO_UPDATE("no_update");

    private String name;

    private ClientUpdateType(final String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static ClientUpdateType convert(String clientUpdateType)
    {
        for (ClientUpdateType type : ClientUpdateType.values())
        {
            if (type.toString().equals(clientUpdateType))
            {
                return type;
            }
        }
        return null;
    }

    public static String[] names()
    {
        return new String[] { SOFT_UPDATE.toString(), HARD_UPDATE.toString(), NO_UPDATE.toString() };
    }
}
