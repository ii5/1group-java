package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface CastType<T extends Enum<T>>
{
    public Enum<T> parseValue(String name);
}
