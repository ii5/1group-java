package one.group.core.enums;

/**
 * Identifies all types of invitations within the movein context.
 * 
 * @author nyalfernandes
 * 
 */
public enum InvitationType
{
    REGISTRATION, ;
}
