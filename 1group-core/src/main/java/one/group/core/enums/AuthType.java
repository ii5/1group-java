/**
 * 
 */
package one.group.core.enums;

public enum AuthType
{
    TYPE1(1), TYPE2(0), ;

    private int val;

    private AuthType(int val)
    {
        this.val = val;
    }

    @Override
    public String toString()
    {
        return Integer.toString(this.val);
    }

    public int getValue()
    {
        return this.val;
    }

}
