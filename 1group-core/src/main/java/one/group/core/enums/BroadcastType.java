package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

public enum BroadcastType
{
    PROPERTY_LISTING("property_listing"), 
    REQUIREMENT("requirement"),
    UNDEFINED(""),
    ;

    private String name;

    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (BroadcastType type : values())
        {
            nameList.add(type.name);
        }
    }

    BroadcastType(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static BroadcastType convert(String str)
    {
        for (BroadcastType broadcastType : BroadcastType.values())
        {
            if (broadcastType.toString().equals(str))
            {
                return broadcastType;
            }
        }
        return null;
    }

    public static String[] broadcastTypeNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
