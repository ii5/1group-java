package one.group.core.enums;

public enum BookMarkType
{
    PROPERTY_LISTING,
    ACCOUNT;
    
    private  BookMarkType type;

    /**
     * @return the type
     */
    public BookMarkType getType()
    {
        return type;
    }
}
