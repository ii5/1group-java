package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Ways a property can be marketed for sale.
 * 
 * @author nyalfernandes
 * 
 */
public enum PropertyTransactionType
{
    SALE("sale"), 
    RENT("rent"), 
    SALE_RENT("sale_rent"), 
    UNDEFINED(""),
    ;

    private String name;
    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (PropertyTransactionType transactionType : values())
        {
            nameList.add(transactionType.name);
        }
    }

    private PropertyTransactionType(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static PropertyTransactionType find(String name)
    {
        for (PropertyTransactionType type : PropertyTransactionType.values())
        {
            if (type.name.equals(name))
            {
                return type;
            }
        }
        return null;
    }

    public static PropertyTransactionType convert(String str)
    {
        for (PropertyTransactionType propertyTransactionType : PropertyTransactionType.values())
        {
            if (propertyTransactionType.toString().equals(str))
            {
                return propertyTransactionType;
            }
        }
        return null;
    }

    public static String[] propertyTransactionTypeNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
