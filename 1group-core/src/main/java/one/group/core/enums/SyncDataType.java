package one.group.core.enums;

/**
 * Constants denoting the sync data types
 * 
 * @author nyalfernandes
 * 
 */
public enum SyncDataType
{
    HINT("hint"), INVALIDATION("invalidation"), MESSAGE("message"), CHAT_THREAD("chat_thread"), SUBSCRIBE("subscribe"), CHAT_CURSOR("chat_cursor"), ACCOUNT_RELATION("account_relation"), LOCATION(
            "location"), NEWS("news"), PROPERTY_LISTING_SEARCH("property_listing_search"), SPECIAL_UPDATE("special_update"), APP_UPDATE("app_update"), SEARCH("search"), LOG("log"), NOTIFICATION(
            "notification"), BROADCAST("broadcast"), ACCOUNT_PROFILE("account_profile");

    private String name;

    private SyncDataType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

}
