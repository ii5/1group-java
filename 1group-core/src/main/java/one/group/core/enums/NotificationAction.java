package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum NotificationAction
{
    DISPLAY("display"),
    SEARCH("search"),
    ;
    
    private String name;
    
    NotificationAction(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
