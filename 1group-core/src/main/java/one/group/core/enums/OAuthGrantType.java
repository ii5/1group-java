package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum OAuthGrantType
{
    CLIENT_CREDENTIALS("client_credentials"),
    AUTHORIZATION_CODE("authorization_code"),
    IMPLICIT_PASSWORD("implicit,password"),
    REFREST_TOKEN("refresh_token"), ;

    private String name;
    private static List<String> names;

    static 
    {
        names = new ArrayList<String>();
        for (OAuthGrantType type : values())
        {
            names.add(type.name);
        }
    }
    
    OAuthGrantType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
    
    public static String[] names()
    {
        return names.toArray(new String[names.size()]);
    }
    
    public static String commaSeparatedNames()
    {
        return StringUtils.join(names, ",");
    }
    
}
