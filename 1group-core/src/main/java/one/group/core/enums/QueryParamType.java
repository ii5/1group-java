package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum QueryParamType
{
    SORT_TYPE("sortType"),
    SORTY_KEY("sortKey"),
    FILTER_KEY("filterKey"),
    FILTER_VALUE("filterValue"),
    LIMIT("limit"),
    OFFSET("offset"),
    INDEX_BY("index_by"),
    PAGE_NO("pgnum"),
    PAGE_ID("pgid"),
    BROADCAST_IDS("broadcast_ids"),
    SUBSCRIBE("subscribe"),
    ACCOUNT("account"),
    LOCATION_IDS("location_ids"),
    BROADCAST("broadcast"),
    RESULT("result"),
    STATUS("status"),
    GROUP_IDS("group_ids"),
    BPO_MESSAGE_STATUS("bpo_msg_status"),
    BPO_MESSAGE_STATUS_UPDATE_TIME_LESS_THAN("bpo_message_status_update_time_lt"),
    GROUP_STATUS("group_status"),
    MESSAGE_STATUS("message_status"),
    CITY_IDS("city_ids"),
    ROLES("roles"),
    ADMIN_IDS("admin_ids"),
    PRIORITY("priority"),
    USERNAME("username"),
    EMAIL("email"),
    FIELD("field"),
    CLEAN_CITY_ASSIGNMENT("clean_city_assignment"),
    RELEASE_ALL_GROUPS("release_all_groups"),
    MESSAGE_IDS("message_ids"),
    ARCHIVE_TIME_PERIOD("archive_time_period"),
    
    ;

    private String name;

    private QueryParamType(String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
