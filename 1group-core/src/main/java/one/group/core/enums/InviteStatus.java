package one.group.core.enums;

public enum InviteStatus
{
    SELECTED("selected"),
    NOT_SELECTED("not_selected"),
    INVITE_SENT("invite_sent"),
    IN_1GROUP("in_1group");

    private String name;

    private InviteStatus()
    {

    }

    private InviteStatus(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }
    
    public static InviteStatus convert(String inviteStatus)
    {
        for (InviteStatus status : InviteStatus.values())
        {
            if (status.toString().equals(inviteStatus))
            {
                return status;
            }
        }
        return null;
    }
}
