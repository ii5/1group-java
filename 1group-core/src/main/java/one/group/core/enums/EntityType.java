package one.group.core.enums;

public enum EntityType
{

    ACCOUNT("account_profile"), AMENITY("AMENITY"), CITY("CITY"), LOCATION("LOCATION"), PROJECT("PROJECT"), PROPERTY_LISTING("PROPERTY_LISTING"),

    PROPERTY_LISTING_SEARCH("PROPERTY_LISTING_SEARCH"), ASSOCIATION("ASSOCIATION"), NEWS_FEED("news_feed"), CHAT("chat"), CHAT_THREAD("chat_thread"), CHAT_CURSOR("chat_cursor"), ACCOUNT_RELATION(
            "account_relation"), BROADCAST("broadcast"),
    // TEST("test"),
    MESSAGE("message"), SEARCH("search");
    private String name;

    private EntityType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static EntityType parse(String name)
    {
        for (EntityType hint : values())
        {
            if (hint.toString().equals(name))
            {
                return hint;
            }
        }

        return null;
    }
}