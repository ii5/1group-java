package one.group.core.enums;

public enum CommisionSplit
{
    DIRECT("direct"), VIA("via");

    private String name;

    CommisionSplit(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static CommisionSplit convert(String str)
    {
        for (CommisionSplit commisionType : CommisionSplit.values())
        {
            if (commisionType.toString().equals(str))
            {
                return commisionType;
            }
        }
        return null;
    }

    public static String[] commisionTypeNames()
    {
        return new String[] { CommisionSplit.VIA.toString(), CommisionSplit.DIRECT.toString() };
    }

}
