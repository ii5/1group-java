package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum TopicType
{
    APP("app"),
    LOG("log"),
    ;
    
    private String name;
    
    TopicType(String name)
    {
        this.name = name;
    }
    
    public static TopicType parse(String name)
    {
        for (TopicType t : values())
        {
            if (t.toString().equalsIgnoreCase(name))
            {
                return t;
            }
        }
        
        return null;
    }
    
    public String toString()
    {
        return this.name;
    }
    
}
