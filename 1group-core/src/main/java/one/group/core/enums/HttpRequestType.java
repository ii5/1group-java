package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum HttpRequestType
{
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    ;
    
    public static List<String> toStringArray()
    {
        List<String> requestTypeStringList = new ArrayList<String>();
        for (HttpRequestType t : values())
        {
            requestTypeStringList.add(t.toString());
        }
        
        return requestTypeStringList;
    }
}
