package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

public enum PropertyMarket
{
    SECONDARY("secondary"),
    PRIMARY("primary"),
    UNDEFINED(""),
    ;

    private String name;
    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (PropertyMarket market : values())
        {
            nameList.add(market.name);
        }
    }

    PropertyMarket(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static PropertyMarket convert(String str)
    {
        for (PropertyMarket propertyMarket : PropertyMarket.values())
        {
            if (propertyMarket.toString().equals(str))
            {
                return propertyMarket;
            }
        }
        return null;
    }

    public static PropertyMarket find(String name)
    {
        for (PropertyMarket type : PropertyMarket.values())
        {
            if (type.name.equals(name))
            {
                return type;
            }
        }
        return null;
    }

    public static String[] propertyMarketNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
