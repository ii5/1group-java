package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * The ways how a property can be marketed based on Building Types. The name
 * <code>PropertyType</code> was kept in keeping with the URD.
 * 
 * TODO : Change the name of the enum to a more appropriate name.
 * 
 * @author nyalfernandes
 * 
 */
public enum PropertyType
{
    RESIDENTIAL("residential", "Res."), 
    COMMERCIAL("commercial", "Comm."), 
    LAND("land", "Land"),
    UNDEFINED("", ""),
    ;

    private String name;
    private String titleAbbr;

    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (PropertyType type : values())
        {
            nameList.add(type.name);
        }
    }

    PropertyType(String name, String titleAbbr)
    {
        this.name = name;
        this.titleAbbr = titleAbbr;
    }

    public String getTitleAbbr()
    {
        return titleAbbr;
    }

    public String toString()
    {
        return this.name;
    }

    public static PropertyType convert(String str)
    {
        for (PropertyType propertyType : PropertyType.values())
        {
            if (propertyType.toString().equals(str))
            {
                return propertyType;
            }
        }
        return null;
    }

    public static String[] propertyTypeNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
