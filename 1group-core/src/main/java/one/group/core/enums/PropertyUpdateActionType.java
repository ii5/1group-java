package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum PropertyUpdateActionType
{
    RENEW("renew"),
    ;
    
    private String name;
    private static Map<String, PropertyUpdateActionType> nameVsEnumMap = new HashMap<String, PropertyUpdateActionType>();

    static
    {
        for (PropertyUpdateActionType type : values())
        {
            nameVsEnumMap.put(type.toString(), type);
        }
    }
    
    PropertyUpdateActionType(String action)
    {
        this.name = action;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public static String[] names()
    {
        return nameVsEnumMap.keySet().toArray(new String[nameVsEnumMap.size()]);
    }
    
    public static PropertyUpdateActionType parse(String name)
    {
        return nameVsEnumMap.get(name);
    }
}
