package one.group.core.enums.status;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum AccountOnlineStatus
{
    ONLINE("online"),
    OFFLINE("offline");

    private String name;

    AccountOnlineStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
