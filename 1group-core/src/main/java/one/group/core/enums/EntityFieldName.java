package one.group.core.enums;

public enum EntityFieldName
{

    ACCOUNT_ID("account_id"),
    LOCATION_ID("location_id"),
    STATUS("status");

    private String name;

    private EntityFieldName(String name)
    {
        this.name = name;
    }
}
