package one.group.core.enums;

import java.util.ArrayList;
import java.util.List;

public enum Rooms
{

    ONE_RK("1rk"),
    ONE_BHK("1bhk"),
    ONE_HALF("1hbhk"),
    TWO("2bhk"),
    TWO_HALF("2hbhk"),
    THREE("3bhk"),
    FOUR("4bhk"),
    FIVE_PLUS("5pbhk"),
    UNDEFINED(""),
    ;

    private String name;
    private static List<String> nameList = new ArrayList<String>();

    static
    {
        for (Rooms room : values())
        {
            nameList.add(room.name);
        }
    }

    Rooms(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static Rooms convert(String str)
    {
        for (Rooms roomType : Rooms.values())
        {
            if (roomType.toString().equals(str))
            {
                return roomType;
            }
        }
        return null;
    }

    public static String[] roomsNames()
    {
        return nameList.toArray(new String[nameList.size()]);
    }
}
