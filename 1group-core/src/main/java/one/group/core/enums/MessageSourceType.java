package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum MessageSourceType
{
    DIRECT("direct"), ME("me"), WHATSAPP("whatsapp"), ;

    private String name;
    private static Map<String, MessageSourceType> valueVsEnumMap = new HashMap<String, MessageSourceType>();

    public String getName()
    {
        return name;
    }

    static
    {
        for (MessageSourceType s : values())
        {
            valueVsEnumMap.put(s.toString(), s);
        }
    }

    MessageSourceType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static MessageSourceType parse(String s)
    {
        return valueVsEnumMap.get(s);
    }
}
