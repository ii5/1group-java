package one.group.core.enums;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum HTTPResponseType
{
    OK(200),
    MOVED_PERMANENTLY(301),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    TOO_MANY_REQUESTS(429),
    INTERNAL_SERVER_ERROR(500),
    SERVICE_UNAVAILABLE(503),
    ;

    private int code;

    private HTTPResponseType(final int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return this.code;
    }

    public boolean isException()
    {
        return !this.equals(OK);
    }

}
