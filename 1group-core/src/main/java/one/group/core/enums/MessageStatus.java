package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum MessageStatus
{
    ACTIVE("active"),
    EDITED("edited"),
    PENDING_REVIEW("pending_review"),
    INSUFFICIENT_INFO("insufficient_info"),
    UNQUALIFIED("unqualified"),
    CLOSED("closed"),
    UNDEFINED("undefined"),
    EXPIRED("expired"),
    ARCHIVED("archived");

    private String name;
    private static Map<String, MessageStatus> valueVsEnumMap = new HashMap<String, MessageStatus>();

    static
    {
        for (MessageStatus s : values())
        {
            valueVsEnumMap.put(s.toString(), s);
        }
    }

    MessageStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    public static MessageStatus parse(String name)
    {
        return valueVsEnumMap.get(name);
    }

    public static String[] stringValues()
    {
        return valueVsEnumMap.keySet().toArray(new String[valueVsEnumMap.keySet().size()]);
    }

}
