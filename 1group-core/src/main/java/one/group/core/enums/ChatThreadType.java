package one.group.core.enums;

public enum ChatThreadType
{
    BY_CREATION("creation"),
    BY_ACTIVITY("activity");

    private String type;

    ChatThreadType(final String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return this.type;
    }
}
