package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum CommissionType
{
    DIRECT("direct"), 
    VIA("via"),
    UNDEFINED(""),
    ;

    private String name;
    private static Map<String,CommissionType> nameVsEnumMap = new HashMap<String, CommissionType>();
    
    static
    {
    	for (CommissionType t : values())
    	{
    		nameVsEnumMap.put(t.toString(), t);
    	}
    }

    CommissionType(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static CommissionType convert(String str)
    {
        return nameVsEnumMap.get(str);
    }

    public static String[] commisionTypeNames()
    {
        return nameVsEnumMap.keySet().toArray(new String[nameVsEnumMap.size()]);
    }

}
