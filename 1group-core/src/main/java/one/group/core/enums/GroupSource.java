package one.group.core.enums;

import java.util.HashMap;
import java.util.Map;

public enum GroupSource
{

    WHATSAPP("whatsapp"), ME("me"), DIRECT("direct"), STARRED("starred"), ONEGROUP("1group");

    private String name;
    private static Map<String, GroupSource> nameVsEnumMap = new HashMap<String, GroupSource>();

    public String getName()
    {
        return name;
    }

    static
    {
        for (GroupSource c : values())
        {
            nameVsEnumMap.put(c.toString(), c);
        }
    }

    private GroupSource(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return this.name;
    }

    public static GroupSource parse(String name)
    {
        return nameVsEnumMap.get(name);
    }
}
