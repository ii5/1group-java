package one.group.core.message;

import one.group.core.enums.MessageType;

public class ChatMessage implements Message
{

    private String content;

    private MessageType type;

    public Object getContent()
    {
        return content;
    }

    public MessageType getType()
    {
        return type;
    }

}
