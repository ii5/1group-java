package one.group.core.message;

import one.group.core.enums.MessageType;

/**
 * <code>Message</code>
 * 
 * @author nyalfernandes
 * 
 */
public interface Message
{
    /**
     * Returns the content of the message.
     * 
     * @return
     */
    public Object getContent();

    /**
     * Returns the mesage type. One of {@link MessageType}
     * 
     * @return
     */
    public MessageType getType();
}
