package one.group.core;

/**
 * 
 * @author nyalfernandes
 *
 */
public class ScoreConstants
{
    public static final float   NEAR_EXPIRATION_PENALTY_GROWTH_RATE     = 0.9955f;
    public static final int     MAX_NEAR_EXPIRATION_PENALTY             = -75;
    public static final float   AGENT_DIVERSITY_PENALTY_GROWTH_RATE     = 2.5f;
    public static final int     MAX_AGENT_DIVERSITY_PENALTY             = -250;
    public static final int     MAX_DESCRIPTION_SCORE                   = 30;
    public static final float   DESCRIPTION_SCORE_GROWTH_RATE           = 0.01f;
    public static final int     CREATED_BY_SELF                         = 300;
    public static final int     IS_CONTACT                              = 50;
    public static final int     IS_RATED_POSITIVELY                     = 75;
    public static final int     IS_RATED_NEGATIVELY                     = -225;
    public static final int     IS_CONTACT_OF                           = 5;
    public static final int     IS_RATED_POSITIVELY_BY                  = 10;
    public static final int     IS_RATED_NEGATIVELY_BY                  = -50;
    public static final int     DIRECT_COMMISSION                       = 100;
    public static final int     NULL_COMMISSION                         = -50;
    public static final int     HOT_PROPERTY_LISTING                    = 75;
    public static final int     UNDEFINED_PRICE                         = -75;
    public static final int     UNDEFINED_BHK                           = -25;
    public static final int     UNDEFINED_AREA                          = -25;
    public static final int     MINIMUM_NEWS_FEED_INCLUSION_SCORE       = 0;
    public static final int     MAX_AMENITY_SCORE                       = 10;
    public static final int     MAX_RECENT_SEARCHES_COUNT_SCORE         = 100;
    public static final float   RECENT_SEARCHES_COUNT_GROWTH_RATE       = 1.05f;
    public static final int     MATCHES_SAVED_SEARCH                    = 500;
    public static final int     NOT_IN_TARGET_LOCALITIES                = -350;
    public static final float   RECENT_SEARCHES_AGE_PENALTY_GROWTH_RATE = 1.015f;
    public static final float   SAVED_SEARCH_AGE_PENALTY_GROWTH_RATE    = 1.01f;
    public static final int     MAX_MATCHES_SAVED_SEARCH_PENALTY        = -125;
    
}
