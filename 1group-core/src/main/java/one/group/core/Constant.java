package one.group.core;

public class Constant
{
    public static final String USER = "User";
    public static final String ADMIN = "Admin";

    public static final String NO_CONTEXT = "NOCONTEXT";

    public static final String DEFAULT_TIMEZONE = "UTC";
    public static final int DEFAULT_SYNC_RESULT = 50;
    public static final int MIN_SYNC_INDEX = -1;
    public static final String VALID_PHONE_NUMBER_REGEX = "\\s*\\+\\d{12}\\s*";
    public static final String VALID_INDIA_PHONE_NUMBER_REGEX = "^(\\+91)[0-9]{10}$";
    public static final String PATH_PARAM_IDS_REGEX = "[0-9]+(,[0-9]+)*";
    public static final String PATH_PARAM_PHONE_NUMBER_REGEX = "\\+91+(\\d{10})+([,]\\+91+(\\d{10}))*$";
    public static final int MAX_PATH_PARAM_IDS = 20;
    public static final String ALL_KEY = "ALL";

    // Default messages
    public static final String MSG_NULL_OBJ = "Object passed cannot be null.";
    public static final String MSG_NOT_TRUE = "Expression expected to be evaluated to true. But is false!";
    public static final String MSG_NOT_FALSE = "Expression expected to be evaluated to false. But is true!";
    public static final String LENGTH_EXCEEDS = "Length exceeds maximum charaters";
    public static final String NO_PARAM_PASSED = "No Parameter Exists";
    public static final String INVALID_MOBILE = "Invalid Mobile Number";
    public static final String FIELD_LENGTH_NOT_VALID = "Field length is not valid.";
    public static final String INVALID_FIELD_VALUE = "Invalid";

    // REDIS constants
    public static final long REDIS_FIRST_POSITION = 0L;
    public static final long REDIS_LAST_POSITION = -1L;
    public static final int REDIS_DEFAULT_EXPIRE_TIME = 300;
    public static final String REDIS_SORTED_SET_MAX_SCORE = "+inf";
    public static final String REDIS_SORTED_SET_MIN_SCORE = "-inf";
    public static final int REDIS_MAX_RESULTS = 20;

    public static final String REGEX_PLACEHOLDER = ".*\\[.*\\].*";

    // PUSHER constants
    public static final boolean PUSHER_DEFAULT_SECURE = true;
    public static final int PUSHER_DEFAULT_TIMEOUT = 10000;
    public static final int PUSHER_MAX_CONNECTIONS = 2;
    public static final String PUSHER_DEFAULT_EVENT_NAME = "";

    // MEDIA
    public static final int MIN_MEDIA_HEIGHT = 100;
    public static final int MIN_MEDIA_WIDTH = 100;
    public static final int MIN_PHOTO_THUMBNAIL_HEIGHT = 160;
    public static final int MIN_PHOTO_THUMBNAIL_WIDTH = 160;
    public static final String DEFAULT_PHOTO_EXTENSION = "jpg";
    public static final int MAX_PHOTO_SIZE = 2000000;
    public static final String MAX_PHOTO_SIZE_STR = "2MB";

    // pagination
    public static final int PAGE_MAX_RESULTS = 20;
    public static final int PAGE_SINCE_INDEX = 0;
    public static final int PAGE_BEFORE_INDEX = 0;
    public static final int PAGE_DEFAULT_LIMIT = 20;
    public static final int PAGE_DEFAULT_MAX_LIMIT = 100;

    // Character sets for random String
    public static final char[] CHARSET_ALPHABETS = "abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    public static final char[] CHARSET_UPPERCASE_ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    public static final char[] CHARSET_NUMERIC = "0123456789".toCharArray();
    public static final char[] CHARSET_NUMERIC_WITHOUT_ZERO = "123456789".toCharArray();
    public static final char[] CHARSET_UPPERCASE_ALPHANUMERIC = new StringBuilder().append(CHARSET_UPPERCASE_ALPHABETS).append(CHARSET_NUMERIC).toString().toCharArray();
    public static final char[] CHARSET_ALPHANUMERIC = new StringBuilder().append(CHARSET_ALPHABETS).append(CHARSET_NUMERIC).toString().toCharArray();

    public static final char[] SHORT_REFERENCE_CHARSET_UPPERCASE_ALPHABETS = "ABCDEFGHJKMNPQRSTUVWXYZ".toCharArray();
    public static final char[] SHORT_REFERENCE_CHARSET_NUMERIC = "23456789".toCharArray();
    public static final char[] SHORT_REFERENCE_CHARSET_UPPERCASE_ALPHANUMERIC = new StringBuilder().append(SHORT_REFERENCE_CHARSET_UPPERCASE_ALPHABETS).append(SHORT_REFERENCE_CHARSET_NUMERIC)
            .toString().toCharArray();
    // Redis internal Hashkeys, This is being used in Redis.So please don't
    // change it
    public static final String REDIS_HASHKEY_FROM_ACCOUNT_ID = "from_account_id";
    public static final String REDIS_HASHKEY_TO_ACCOUNT_ID = "to_account_id";
    public static final String REDIS_HASHKEY_READ_INDEX = "read_index";
    public static final String REDIS_HASHKEY_RECEIVED_INDEX = "received_index";

    // Upload directory path
    // public static final String CDN_IMAGE_FOLDER_PATH = "/var/www/html/cdn/";
    // public static final String CDN_IMAGE_URL = "http://10.50.249.15/cdn/";
    // public static final String CHAT_MESSAGE_PHOTO_THUMBNAIL_RELATIVE_PATH =
    // CDN_IMAGE_FOLDER_PATH + "thumbnail/";
    // public static final String CHAT_MESSAGE_PHOTO_THUMBNAIL_ABSOLUTE_PATH =
    // CDN_IMAGE_URL + "thumbnail/";
    // public static final String ACCOUNT_PHOTO_ABSOLUTE_PATH = CDN_IMAGE_URL +
    // "accounts/";
    // public static final String ACCOUNT_PHOTO_RELATIVE_PATH =
    // CDN_IMAGE_FOLDER_PATH + "accounts/";

    // chat thread
    public static final String CHAT_THREAD_BY_ACTIVITY = "activity";
    public static final String CHAT_THREAD_BY_JOINED = "joined";
    public static final String DEFAULT_CHAT_THREAD_CURSOR = "-1";
    public static final String DEFAULT_CHAT_THREAD_INDEX_BY = CHAT_THREAD_BY_ACTIVITY;
    public static final int CHAT_MESSAGE_CHAR_LIMIT = 4000;
    public static final String CHAT_MESSAGE_UNREAD_NOTIFICATION = "Hi, you have unread chat message.";

    // ChatKey
    public static final String CHAT_KEY_PROPERTY_LISTING_ID = "property_listing_id";
    public static final String CHAT_KEY_ACCOUNT_ID = "account_id";
    public static final String CHAT_KEY_CHAT_THREAD_ID = "chat_thread_id";
    public static final String CACHE_ACCOUNT_ACCOUNT_ID = "account_id";
    // 31 days
    public static final long ONE_MONTH_IN_MS = 2678400000L;
    public static final long ONE_HOUR_IN_MS = 3600000L;
    public static final long FIVE_MINUTE_IN_MS = 5 * 60 * 1000L;
    public static final int ONE_MONTH_IN_MINS = 44640;

    public static final String SYNC_SERVER_PROPERTIES_FILE = "";

    // Sign In Constants
    public static final String APP_NAME = "MoveIn";
    public static final String APP_VERSION = "1.0";
    // public static final String OAUTH_GRANT_TYPE = "client_credentials";
    // public static final String OAUTH_ALL_GRANT_TYPES =
    // "client_credentials,authorization_code,implicit,password,refresh_token";
    // public static final String[] OAUTH_ALL_GRANT_TYPES_ARRAY =
    // {"client_credentials", "authorization_code" ,"implicit,password",
    // "refresh_token"};
    public static final String OAUTH_SCOPE = "standard_client";
    public static final String AUTHORITIES_ROLE_USER = "ROLE_USER";
    public static final String AUTHORITIES_ROLE_ADMIN = "ROLE_ADMIN";
    public static final String OAUTH_RESOURCE_ID = "rest_api";
    public static final int OAUTH_ACCESS_TOKEN_VALIDITY = 2147483600;
    public static final int MIN_OTP_LENGTH = 4;
    public static final int MAX_OTP_LENGTH = 6;
    public static final int PUSHER_CHANNEL_ID_LENGTH = 12;
    public static final int MOBILE_NUMBER_LENGTH = 13;
    // public static final List<String> VALID_CLIENT_TYPES =
    // Arrays.asList("mobile", "web");
    // public static final List<String> VALID_DEVICE_PLATFORM =
    // Arrays.asList("android", "ios");

    // max characters limit
    public static final int MAX_TEXT_LENGTH = 250;

    // property module
    public static final String PROPERTY_MARKET = "secondary";
    public static final String EXTERNAL_SOURCE = "99acres";
    public static final String RENEW = "RENEW";
    public static final long TIME_TO_BE_EXPIRE_IN_DAYS = 45l;
    public static final long TIME_TO_CLOSE_IN_DAYS = 55l;
    public static final String EXACT_MATCH = "exact";
    public static final String NEAR_MATCH = "near";
    public static final String NOTIFICATION_NEW_MATCH_FOUND = "New match found for %s";

    public static final long TIME_TO_BE_EXPIRE_IN_MILLISECONDS = 86400000l * TIME_TO_BE_EXPIRE_IN_DAYS; // 45day
    public static final long TIME_TO_BE_CLOSED_IN_MILLISECONDS = 86400000l * TIME_TO_CLOSE_IN_DAYS; // 55days
    // sync module
    public static final Integer SUBSCRIBE_TIME = 44640;

    public static final String STRING_TRUE = "true";
    public static final String STRING_FALSE = "false";
    public static final boolean BOOLEAN_TRUE = true;
    public static final boolean BOOLEAN_FALSE = false;

    // sync contacts
    public static final String DELTA_CONTACTS = "DELTA_CONTACTS";
    public static final String CONTACTS_SYNC_FLAG_FLIPPED = "CONTACTS_SYNC_FLAG_FLIPPED";
    public static final int DEFAULT_ACCOUNT_RELATION_SCORE = 0;

    // General constants
    public static final int MINUS_ONE = -1;
    public static final int INDEX_NOT_SET = -2;
    public static final int MIN_SUBSCRIBE_TIME_IN_MINUTES = 1;
    public static final int MAX_SUBSCRIBE_TIME_IN_MINUTES = 44641;
    public static final int SUSBCRIBTION_CONVERSION_IN_SECONDS = 60;
    public static final String LOCATIONS_DB_KEY = "locations_db";

    public static final String ASYNC_HANDLER_KEY_FORMAT = "[action][[entityType]]";

    public static final String CUSTOMER_CARE_NUMBER = "+919811198111";
    public static final String WELCOME_TEXT = "Welcome to move.in!";

    public static final double MIN_NEWS_FEED_SCORE = 1;
    public static final String NEWS_VALUE_TEMPLATE = "[propertyListingId]:[accountId]:[newsScore]:[listingTime]";
    public static final String UTF8 = "UTF8";
    public static final String CDN_PATH = "cdn_img_url";
    public static final String CDN_FOLDER_PATH = "cdn_img_folder_path";

    public static final String APPLICATION_URLENCODED = "application/x-www-form-urlencoded";
    public static String UTF8_ENCODING = "UTF-8";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";

    public static final String SMS_LAST_CHAT_MESSAGE_RECEIVED_TMS = "last_chat_message_received_ts";
    public static final String SMS_LAST_RECEIVED_INDEX_ADVANCED_TMS = "last_received_index_advanced";
    public static final String SMS_LAST_SMS_SENT_TMS = "last_sms_sent_tms";
    public static final long SMS_TOTAL_DAYS_IN_MILLISECONDS = 600000;
    public static final long SMS_NOTIFY_AFTER_IN_MILLISECONS = 2000;
    public static final String SMS_KEY_PATTERN = "accounts:*:sms_notification";
    public static final String SMS_DEFAULT_CURSOR = "0";
    public static final int SMS_FETCH_COUNTER = 10;
    public static final String SMS_CONTENT_CHAT_NOTIFICATION = "Your one time password for verifying your mobile number on \"MOVEIN\" is CHAT and it's valid for next 2 mins. Please do not share this with anyone.";
    public static final String FULL_RELEASE = "FULL_RELEASE";

    // GCM Notification constants
    public static final String GCM_TITLE = "1Group";
    public static final String GCM_ICON = "appicon";
    public static final String GCM_SPLASHSCREEN_ACTION = "in.move.SPLASH_SCREEN";

    // KAFKA
    public static final String CONSUMER_CLIENT_PREFIX = "1GC";
    public static final int CONSUMER_BUFFER_SIZE = 100 * 1024;
    public static final int CONSUMER_CLIENT_TIMEOUT = 100000;

    public static final String ERROR_OFFSET_FILE_TEMPLATE = "[topic]-error-offsets.log";
    public static final String SYNC_SERVER_OFFSET_FILE_TEMPLATE = "[topic]-offsets.log";
    public static final String CHAT_LOG_ERROR_OFFSET_FILE_TEMPLATE = "chatLogError-offsets.log";

    public static final String INVITE_NAME_BLACK_LIST_CHARS = "[()*&^%$#@!`~/|\\[{\\]};:\\\\'\".>,<0123456789+= ]";

    public static final String DEFAULT_SMS_SENDER = "smsCountry";
    public static final short MAX_SMS_CONTENT_LENGTH = 160;

    // 1Group V2

    public static final int DEFAULT_PAGINATION_PAGE = 1;

}
