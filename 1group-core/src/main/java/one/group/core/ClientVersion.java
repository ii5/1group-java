package one.group.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import one.group.core.enums.ClientType;

public class ClientVersion
{

    private static Map<ClientType, Map<String, ClientVersion>> typeVsVersionMap = new HashMap<ClientType, Map<String, ClientVersion>>();

    private ClientType clientType;
    private String version;
    private Boolean isSupported;
    private Boolean isCurrentVersion;
    private static Map<ClientType, Properties> typeVsProperties = new HashMap<ClientType, Properties>();

    static
    {
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream("android.client.version.properties");
            Properties androidProperties = new Properties();
            androidProperties.load(fis);
            typeVsProperties.put(ClientType.ANDROID, androidProperties);

            fis = new FileInputStream("ios.client.version.properties");
            Properties iosProperties = new Properties();
            iosProperties.load(fis);
            typeVsProperties.put(ClientType.IOS, iosProperties);

            fis = new FileInputStream("web.client.version.properties");
            Properties webProperties = new Properties();
            webProperties.load(fis);
            typeVsProperties.put(ClientType.WEB, webProperties);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (fis != null)
                try
                {
                    fis.close();
                }
                catch (IOException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        init();
    }

    public static List<String> getSupportedVersion(ClientType type)
    {
        List<String> supportedVersionList = new ArrayList<String>();
        Map<String, ClientVersion> clientVersion = typeVsVersionMap.get(type);
        for (String key : clientVersion.keySet())
        {
            ClientVersion value = clientVersion.get(key);
            if (value.isSupported)
            {
                supportedVersionList.add(key);
            }
        }

        return supportedVersionList;
    }

    public static String getCurrentVersion(ClientType type)
    {
        String currentVersion = null;
        if (type == null)
        {
        	return null;
        }
        Map<String, ClientVersion> clientVersion = typeVsVersionMap.get(type);
        for (String key : clientVersion.keySet())
        {
            ClientVersion value = clientVersion.get(key);
            if (value.isCurrentVersion())
            {
                currentVersion = key;
            }
        }
        return currentVersion;
    }

    public static boolean isSupportedVersion(ClientType clientType, String version)
    {
    	if (clientType == null || version == null) return false;
        return ClientVersion.getSupportedVersion(clientType).contains(version);
    }

    public static boolean isCurrentVersion(ClientType clientType, String version)
    {
    	if (clientType == null) return false;
    	
        return ClientVersion.getCurrentVersion(clientType).equals(version);
    }

    public static void init()
    {
        for (ClientType type : typeVsProperties.keySet())
        {
            Properties properties = typeVsProperties.get(type);
            Map<String, ClientVersion> map = new HashMap<String, ClientVersion>();

            for (String name : properties.stringPropertyNames())
            {
                ClientVersion clientVersion = new ClientVersion();
                clientVersion.setClientType(type);
                clientVersion.setVersion(name);

                String supportedAndCurrent = properties.getProperty(name);
                String[] supportedAndCurrentArray = supportedAndCurrent.split(",");

                clientVersion.setSupported(Boolean.valueOf(supportedAndCurrentArray[0]));
                clientVersion.setCurrentVersion(Boolean.valueOf(supportedAndCurrentArray[1]));

                map.put(name, clientVersion);
            }
            typeVsVersionMap.put(type, map);
        }
    }

    public Map<ClientType, Map<String, ClientVersion>> getTypeVsVersionMap()
    {
        return typeVsVersionMap;
    }

    public Map<ClientType, Properties> getTypeVsProperties()
    {
        return typeVsProperties;
    }

    public void setTypeVsProperties(Map<ClientType, Properties> typeVsProperties)
    {
        this.typeVsProperties = typeVsProperties;
    }

    public void setTypeVsVersionMap(Map<ClientType, Map<String, ClientVersion>> typeVsVersionMap)
    {
        this.typeVsVersionMap = typeVsVersionMap;
    }

    public ClientType getClientType()
    {
        return clientType;
    }

    public void setClientType(ClientType clientType)
    {
        this.clientType = clientType;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public Boolean isSupported()
    {
        return isSupported;
    }

    public void setSupported(Boolean isSupported)
    {
        this.isSupported = isSupported;
    }

    public Boolean isCurrentVersion()
    {
        return isCurrentVersion;
    }

    public void setCurrentVersion(Boolean isCurrentVersion)
    {
        this.isCurrentVersion = isCurrentVersion;
    }
}
