package one.group.schedulers;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiLogReportTask
{
    private static final String fileToFTPKey = "fileToFTP";
    private static final Logger loggers = LoggerFactory.getLogger(ApiLogReportTask.class);
    private static Properties configProperties = new Properties();

    public static Properties getConfigProperties()
    {
        return configProperties;
    }

    public static void setConfigProperties(Properties configProperties)
    {
        ApiLogReportTask.configProperties = configProperties;
    }

    public ApiLogReportTask()
    {
    }

    public void performTask()
    {
        try
        {
            ftpFile();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    class ApiLogConfig
    {

        public static final String ftpServerAddress = "ftpServerAddress";
        public static final String userId = "userId";
        public static final String password = "password";
        public static final String remoteDirectory = "remoteDirectory";
        public static final String localDirectory = "localDirectory";

    }

    public static String getReportsDateFolder()
    {
        DateTime datetime = DateTime.now();
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd-MM-yyyy");
        return dtfOut.print(datetime);
    }

    public boolean ftpFile()
    {
        StandardFileSystemManager manager = new StandardFileSystemManager();
        try
        {
            Date now = new Date();

            String fileToFTP = configProperties.getProperty(fileToFTPKey);
            String serverAddress = configProperties.getProperty(ApiLogConfig.ftpServerAddress);
            String userId = configProperties.getProperty(ApiLogConfig.userId);
            String password = configProperties.getProperty(ApiLogConfig.password);
            String remoteDirectory = configProperties.getProperty(ApiLogConfig.remoteDirectory);
            String localDirectory = configProperties.getProperty(ApiLogConfig.localDirectory);

            remoteDirectory = remoteDirectory + getReportsDateFolder();

            // check if the file exists
            String filepath = localDirectory + fileToFTP;
            File file = new File(filepath);
            if (!file.exists())
                throw new RuntimeException("Error. Local file not found");

            // Initializes the file manager
            manager.init();

            // Setup our SFTP configuration
            FileSystemOptions opts = new FileSystemOptions();
            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
            SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
            SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);

            // Create the SFTP URI using the host name, userid, password, remote
            // path and file name
            String sftpUri = "sftp://" + userId + ":" + password + "@" + serverAddress + "/" + remoteDirectory + "/" + fileToFTP;

            // Create local file object
            FileObject localFile = manager.resolveFile(file.getAbsolutePath());

            // Create remote file object
            FileObject remoteFile = manager.resolveFile(sftpUri, opts);

            // Copy local file to sftp server
            remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
            System.out.println("File upload successful");
            localFile.delete();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        finally
        {
            manager.close();
        }

        return true;
    }

}
