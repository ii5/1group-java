package one.group.schedulers;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ChatSMSNotificationJob extends QuartzJobBean
{
    private static final Logger loggers = LoggerFactory.getLogger(ChatSMSNotificationJob.class);

    private ChatSMSNotificationTask chatSMSNotificationTask;

    public ChatSMSNotificationTask getChatSMSNotificationTask()
    {
        return chatSMSNotificationTask;
    }

    public void setChatSMSNotificationTask(ChatSMSNotificationTask chatSMSNotificationTask)
    {
        this.chatSMSNotificationTask = chatSMSNotificationTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException
    {
        try
        {
            loggers.info("Chat SMS Notifcation Job Started");

            chatSMSNotificationTask.sendNotification();

            loggers.info("Chat SMS Notifcation Job Ended");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
