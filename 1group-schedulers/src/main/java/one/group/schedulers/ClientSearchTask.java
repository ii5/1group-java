package one.group.schedulers;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.Client;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.services.ClientService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ClientSearchTask
{
    private List<DeviceStatus> statusList = new ArrayList<DeviceStatus>();

    /**
     * Format: yyyy-MM-dd, e.g. 2015-11-21
     */
    private int maxInactiveDays = 30;

    /**
     * log,deactivate,purge
     */
    private String action = "log";

    private ClientService clientService;  
    
    private ClientJpaDAO clientJpaDAO;
    
	

	public Map<String, ClientSearchAction> actionMap = new HashMap<String, ClientSearchAction>();

    private static final String[] actionArray = { "log", "deactivate", "purge" };
    
    public ClientSearchTask()
    {
        actionMap.put("log", new LogClientSearchAction());
        actionMap.put("deactivate", new DeactivateClientSearchAction());
        actionMap.put("purge", new PurgeClientSearchAction());
    }

    public ClientService getClientService() 
    {
		return clientService;
	}

	public void setClientService(ClientService clientService) 
	{
		this.clientService = clientService;
	}

	public ClientJpaDAO getClientJpaDAO()
	{
		return clientJpaDAO;
	}

	public void setClientJpaDAO(ClientJpaDAO clientJpaDAO) 
	{
		this.clientJpaDAO = clientJpaDAO;
	}
    public List<DeviceStatus> getStatusList()
    {
        return statusList;
    }

    public void setStatusList(List<DeviceStatus> statusList)
    {
        this.statusList = statusList;
    }

    public int getMaxInactiveDays()
    {
        return maxInactiveDays;
    }
    
    public void setMaxInactiveDays(int maxInactiveDays)
    {
        this.maxInactiveDays = maxInactiveDays;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }
    
    @Transactional
    public void performTask()
    {
        Set<String> clientSet = new HashSet<String>();
        try
        {
            Validation.isTrue(maxInactiveDays >= 30, "Max inactive days should be greater than 0.");
            Validation.isTrue(!Utils.oneOf(getAction(), actionArray), "Action should be one of " + Arrays.asList(actionArray) + ".");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date beforeTimeDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(beforeTimeDate);
            calendar.add(Calendar.DAY_OF_YEAR, -maxInactiveDays);
            beforeTimeDate = calendar.getTime();
            
            
            List<Client> clientCollection = clientService.findClientForSchedulers(getStatusList(), beforeTimeDate);
            System.out.println(clientCollection.size() + "SIze ");            
            actionMap.get(getAction()).performAction(clientCollection);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    interface ClientSearchAction
    {
        public void performAction(List<Client> clientList);
    }

    public class LogClientSearchAction implements ClientSearchAction
    {
        private final Logger logger = LoggerFactory.getLogger(LogClientSearchAction.class);
        
        @Transactional
        public void performAction(List<Client> clientList)
        {
            logger.info("Writing clientList to file. "+ clientList);
            FileWriter fw = null;
            try
            {
                File f = new File("clientSearchList.txt");
                if (!f.exists())
                {
                    f.createNewFile();
                }

                fw = new FileWriter(f);
                fw.write(clientList.toString());
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                Utils.close(fw);
            }
        }
    }
    
    public class DeactivateClientSearchAction implements ClientSearchAction
    {
        private final Logger logger = LoggerFactory.getLogger(DeactivateClientSearchAction.class);
        
        public void performAction(List<Client> clientList)
        {
            logger.info("Deactivating client list " + clientList + ".");
            clientService.deactivateClient(clientList);
        }
    }
    
    public class PurgeClientSearchAction implements ClientSearchAction
    {
        private final Logger logger = LoggerFactory.getLogger(PurgeClientSearchAction.class);
        
        public void performAction(List<Client> clientList)
        {
            logger.info("Purging client list " + clientList + ".");
            clientService.purgeClient(clientList);
        }
    }
}
