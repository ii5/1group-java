package one.group.schedulers;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import one.group.core.Constant;
import one.group.dao.SearchDAO;
import one.group.entities.jpa.Search;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.SearchService;
import one.group.utils.Utils;

public class AutoExpireRequirementSearchTask
{
    private static final Logger logger = LoggerFactory.getLogger(AutoExpireRequirementSearchTask.class);

    private SearchService searchService;
    private SearchDAO searchDAO;

    public SearchDAO getSearchDAO()
    {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO)
    {
        this.searchDAO = searchDAO;
    }

    public SearchService getSearchService()
    {
        return searchService;
    }

    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }

    @Transactional
    public void autoExpireRequirementSearch() throws ParseException, Abstract1GroupException
    {
        Long currentTime = Utils.getSystemTime();
        Date expiryTime = new Date(currentTime - Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS);
        List<Search> expiredSearches = searchDAO.fetchAllExpiredRequirementSearch(expiryTime);

//        logger.info("Total expired requirement search - [" + expiredSearches.size() + "]");
//        for (Search search : expiredSearches)
//        {
//            WSPropertyListingSearchQueryRequest wsSearchQueryRequest = new WSPropertyListingSearchQueryRequest(search);
//            String searchId = search.getId();
//            searchDAO.deleteSearch(search);
//
//            WSRequest request = new WSRequest();
//            request.setSearch(wsSearchQueryRequest);
//            searchService.pushSearchToSyncLog(request, SyncActionType.DELETE);
//            logger.info("Search removed : [" + searchId + "]");
//        }
    }
}
