package one.group.schedulers;

import java.text.ParseException;
import java.util.List;

import one.group.entities.jpa.PropertyListing;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.PropertyListingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoExpirePropertyListingTask
{
    private static final Logger loggers = LoggerFactory.getLogger(AutoExpirePropertyListingTask.class);

    private PropertyListingService propertyService;

    public PropertyListingService getPropertyService()
    {
        return propertyService;
    }

    public void setPropertyService(PropertyListingService propertyService)
    {
        this.propertyService = propertyService;
    }

    public void autoExpirePropertyListings() throws ParseException, Abstract1GroupException, InterruptedException
    {
        List<PropertyListing> activePropertyListing = propertyService.getActiveAndExpiredPropertyListing();
        loggers.info(activePropertyListing.size() + "- Total active and expired ");
        for (int i = 0; i < activePropertyListing.size(); i++)
        {
            PropertyListing propertyListing = propertyService.autoExpirePropertyListing(activePropertyListing.get(i).getIdAsString().toString());
            
            if (propertyListing != null)
            {
                loggers.info("Property " + propertyListing.getIdAsString() + " has been updated");
            }
            else
            {
            	loggers.info("Property has not been updated");
            }
            Thread.sleep(2500);
        }
    }
}
