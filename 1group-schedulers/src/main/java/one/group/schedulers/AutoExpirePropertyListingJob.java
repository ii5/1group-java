package one.group.schedulers;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AutoExpirePropertyListingJob extends QuartzJobBean
{
    private AutoExpirePropertyListingTask autoExpirePropertyListingTask;

    public AutoExpirePropertyListingTask getAutoExpirePropertyListingTask()
    {
        return autoExpirePropertyListingTask;
    }

    public void setAutoExpirePropertyListingTask(AutoExpirePropertyListingTask autoExpirePropertyListingTask)
    {
        this.autoExpirePropertyListingTask = autoExpirePropertyListingTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException
    {
        try
        {
            autoExpirePropertyListingTask.autoExpirePropertyListings();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
