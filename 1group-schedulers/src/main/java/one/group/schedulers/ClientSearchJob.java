package one.group.schedulers;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ClientSearchJob extends QuartzJobBean
{
    private ClientSearchTask clientSearchTask;
    
    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException
    {
        try
        {
            clientSearchTask.performTask();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
