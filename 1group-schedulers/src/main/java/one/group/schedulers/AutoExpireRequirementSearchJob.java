package one.group.schedulers;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AutoExpireRequirementSearchJob extends QuartzJobBean
{

    private AutoExpireRequirementSearchTask autoExpireRequirementSearch;

    public AutoExpireRequirementSearchTask getAutoExpireReuirementSearch()
    {
        return autoExpireRequirementSearch;
    }

    public void setAutoExpireReuirementSearch(AutoExpireRequirementSearchTask autoExpireRequirementSearch)
    {
        this.autoExpireRequirementSearch = autoExpireRequirementSearch;
    }

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException
    {
        try
        {
            autoExpireRequirementSearch.autoExpireRequirementSearch();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
