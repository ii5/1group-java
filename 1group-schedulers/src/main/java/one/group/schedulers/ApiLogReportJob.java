package one.group.schedulers;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ApiLogReportJob extends QuartzJobBean
{
    private ApiLogReportTask apiLogReportTask;

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException
    {
        try
        {
            apiLogReportTask.performTask();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
