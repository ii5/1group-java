package one.group.schedulers;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.dao.AccountDAO;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSChatNotificationTMS;
import one.group.entities.api.response.WSNotification;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.AccountService;
import one.group.services.SMSService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatSMSNotificationTask
{

    private static final Logger logger = LoggerFactory.getLogger(ChatSMSNotificationTask.class);

    private AccountDAO accountDAO;
    private AccountService accountService;
    private SMSService smsService;

    private SimpleSyncLogProducer kafkaProducer;
    private KafkaConfiguration kafkaConfiguration;

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public SMSService getSmsService()
    {
        return smsService;
    }

    public void setSmsService(SMSService smsService)
    {
        this.smsService = smsService;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    @Transactional
    public void sendNotification() throws ParseException, Abstract1GroupException, IOException
    {

        List<String> accounts = accountService.fetchAllAccountToSendChatNotification();
        logger.info("account found - " + accounts.size());
        for (String accountKey : accounts)
        {
            WSChatNotificationTMS notificationTMS = accountService.fetchChatSmsNotificationTimestamp(accountKey);
            long currentTimestamp = Utils.getSystemTime();
            Long lastChatMessageReceivedTms = notificationTMS.getLastChatMessageReceivedTms();
            Long lastReceivedIndexAdvancedTms = notificationTMS.getLastReceivedIndexAdvancedTms();
            Long lastSmsSent = notificationTMS.getLastSmsSent();

            if (lastChatMessageReceivedTms == null)
            {
                logger.info("Don't do anything here :) 0");
            }
            else if ((currentTimestamp - lastChatMessageReceivedTms) > Constant.SMS_TOTAL_DAYS_IN_MILLISECONDS)
            {
                logger.info("Don't do anything here :) 1 " + (currentTimestamp - lastChatMessageReceivedTms) + ">" + Constant.SMS_TOTAL_DAYS_IN_MILLISECONDS);
            }
            else if ((lastReceivedIndexAdvancedTms != null && (lastReceivedIndexAdvancedTms - lastChatMessageReceivedTms) <= Constant.SMS_NOTIFY_AFTER_IN_MILLISECONS))
            {
                logger.info("Don't do anything here :) 2 " + (lastReceivedIndexAdvancedTms - lastChatMessageReceivedTms) + "<=" + Constant.SMS_NOTIFY_AFTER_IN_MILLISECONS);
            }
            // After sending message on day x, again send message on day x +
            // SMS_TOTAL_DAYS_IN_MILLISECONDS
            else if ((lastSmsSent != null && currentTimestamp - lastSmsSent < Constant.SMS_TOTAL_DAYS_IN_MILLISECONDS))
            {
                logger.info("Don't do anything here :) 3 " + (lastSmsSent != null && currentTimestamp - lastSmsSent < Constant.SMS_TOTAL_DAYS_IN_MILLISECONDS));
            }
            else
            {
//                String accountId = accountKey.split(":")[1];
//
//                // Send SMS notification to user
//                WSAccount wsAccount = accountService.fetchActiveAccountDetails(accountId);
//                String mobileNumber = wsAccount.getPrimaryMobile();
//
//                if (!Utils.isNullOrEmpty(mobileNumber))
//                {
//                    // Send SMS to number
//                    // smsService.sendSMS(mobileNumber, smsContent);
//                }
//                String title = Constant.GCM_TITLE;
//                String body = Constant.CHAT_MESSAGE_UNREAD_NOTIFICATION;
//                WSNotification notification = new WSNotification(title, body);
//                SyncLogEntry syncLogEntry = new SyncLogEntry();
//                syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, notification);
//                syncLogEntry.setType(SyncDataType.NOTIFICATION);
//                List<String> targetAccountIds = new ArrayList<String>();
//                targetAccountIds.add(accountId);
//                syncLogEntry.setAdditionalData(SyncEntryKey.TARGET_ACCOUNT_ID, targetAccountIds);
//                syncLogEntry.setAssociatedEntityType(EntityType.CHAT);
//                syncLogEntry.setAction(SyncActionType.UNREAD);
//
//                kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
//
//                // Update SMS notification sent time stamp
//                accountService.updateChatSmsNotificationTimestamp(accountId, Constant.SMS_LAST_SMS_SENT_TMS, String.valueOf(currentTimestamp));
//
//                logger.info("SMS Nofitication sent: [accountId: " + accountId + ", mobileNumber:" + mobileNumber + "]");

            }

        }

    }
}
