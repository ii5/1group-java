package one.group.services.testData;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.LocationType;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.core.enums.PhoneNumberType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.Otp;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.PropertyListing;
import one.group.utils.Utils;

import org.apache.commons.lang.StringUtils;

public class SeedData
{
    public PropertyListing getPropertyListingData()
    {

        String amenities = null;
        List<String> listAmenities = new ArrayList<String>();
        for (int i = 0; i < 3; i++)
        {
            listAmenities.add("Amenity-" + i);
        }

        amenities = StringUtils.join(listAmenities, ",");

        PropertyListing property = createPropertySeedData(amenities, new Date(), "2hbhk", CommissionType.DIRECT, "Description of Property Test ", false,
                Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, 6, false), new Long("231233123"), new Long("56345453"), 328, BroadcastStatus.ACTIVE, PropertySubType.APARTMENT,
                PropertyType.RESIDENTIAL, PropertyMarket.PRIMARY);

        return property;
    }

    public Location getLocation()
    {
        List<City> cityList = new ArrayList<City>();
        for (int i = 0; i < 3; i++)
        {
            City city = createCitySeedData("Test" + i);
            cityList.add(city);
        }
        Location location = createLocationSeedData("3592", "0101000020E6100000EE4BEC9051415240FACF3F660C2E3340", "Sanjay Nagar", "13533", Status.ACTIVE, LocationType.SUB_LOCALITY, "400612",
                cityList.get(0));
        return location;
    }

    public PhoneNumber getPhoneNumber()
    {
        PhoneNumber number = (createPhoneNumberSeedData("+91" + new Integer(1833243230 + 5), PhoneNumberType.MOBILE));
        return number;
    }

    public Agent getAccountseedData()
    {
        Media[] mediaArray = new Media[20];
        PhoneNumber[] numberArray = new PhoneNumber[15];

        for (int i = 0; i < mediaArray.length; i++)
        {
            Media media = createMediaSeedData("Media-" + i, MediaType.IMAGE, "http://10.50.249.13/cdn/accounts/" + i + ".jpg");
            mediaArray[i] = media;
        }

        Agent account1 = createAgentSeedData("Mitesh", "Chavda", "mchavda@ii5.com", AccountType.AGENT, "Always at ii5", "The Great Gujrat Establishment", new Date(), Status.ACTIVE, mediaArray[0]);

        return account1;
    }

    public Client getClientSeedData()
    {
        String[] deviceType = { "Android", "iOS", "Web" };
        Random random = new SecureRandom();
        int r = random.nextInt(3);
        Client c = createClientSeedData(deviceType[r] + "-CLI-Test", "1", "production", deviceType[r], deviceType[r], UUID.randomUUID().toString(), new Integer(r).toString(), "PUSH-ALERT",
                "PUSH-BADGE", "PUSH-SOUND", DeviceStatus.ACTIVE);
        return c;
    }

    public Otp getClientOtp(Client c, Account account)
    {
        Otp otp = new Otp();
        otp.setOtp("1234");
        otp.setPhoneNumber(account.getPrimaryPhoneNumber());
        otp.setExpiryTime(new Date());
        return otp;
    }

    private static Location createLocationSeedData(String id, String geopoint, String name, String parent_id, Status status, LocationType type, String Zipcode, City city_id)
    {

        Location location = new Location();
        location.setCity(city_id);
        location.setLocalityName(name);
        location.setParentId(parent_id);
        location.setStatus(status);
        location.setType(type);

        return location;
    }

    private static PropertyListing createPropertySeedData(String amenities, Date availableDate, String bhk, CommissionType commisionSplit, String description, boolean isHot, String referenceId,
            long rentPrice, long salePrice, int size, BroadcastStatus status, PropertySubType subType, PropertyType type, PropertyMarket propertyMarket)
    {
        PropertyListing p = new PropertyListing();
        p.setAmenities(amenities);
        p.setRooms(bhk);
        p.setCommissionType(commisionSplit);
        p.setDescription(description);
        p.setIsHot(isHot);
        p.setShortReference(referenceId);
        p.setRentPrice(rentPrice);
        p.setSalePrice(salePrice);
        p.setArea(size);
        p.setStatus(status);
        p.setSubType(subType);
        p.setType(type);
        p.setPropertyMarket(propertyMarket);
        return p;
    }

    private static Agent createAgentSeedData(String firstname, String lastname, String email, AccountType accountType, String address, String establishmentName, Date workingSince, Status status,
            Media accountPicture)
    {
        Agent account = new Agent();
        account.setFullName(firstname);
        account.setCompanyName(lastname);
        account.setEmail(email);
        account.setType(accountType);
        account.setAddress(address);
        account.setEstablishmentName(establishmentName);
        account.setWorkingSince(workingSince);
        account.setStatus(status);
        account.setFullPhoto(accountPicture);
        return account;
    }

    public static Client createClientSeedData(String appName, String appVersion, String development, String deviceModel, String devicePlatform, String deviceToken, String deviceVersion,
            String pushAlert, String pushBadge, String pushSound, DeviceStatus status)
    {
        Client client = new Client();

        client.setAppName(appName);
        client.setAppVersion(appVersion);
        client.setDevelopment(development);
        client.setDeviceModel(deviceModel);
        client.setDevicePlatform(devicePlatform);
        client.setDeviceToken(deviceToken);
        client.setDeviceVersion(deviceVersion);
        client.setPushAlert(pushAlert);
        client.setPushBadge(pushBadge);
        client.setPushSound(pushSound);
        client.setPushChannel(client.getIdAsString());
        client.setStatus(status);

        return client;
    }

    private static PhoneNumber createPhoneNumberSeedData(String number, PhoneNumberType type)
    {
        PhoneNumber pNumber = new PhoneNumber();
        pNumber.setNumber(number);
        pNumber.setType(type);
        return pNumber;
    }

    public static Media createMediaSeedData(String name, MediaType type, String url)
    {
        Media media = new Media();
        media.setName(name);
        media.setDimension(MediaDimensionType.FULL);
        media.setType(type);
        media.setUrl(url);
        media.setWidth(100);
        media.setHeight(100);
        media.setSize(500);
        return media;
    }

    public static one.group.entities.api.request.WSPhoto createWsMediaSeedData(String name, MediaType type, String url)
    {
        one.group.entities.api.request.WSPhoto wsphoto = new one.group.entities.api.request.WSPhoto();
        wsphoto.setHeight(100);
        wsphoto.setLocation(url);
        wsphoto.setSize((long) 500);
        wsphoto.setWidth(100);
        return wsphoto;

    }

    private static Amenity createAmenitiesSeedData(String name, String type)
    {
        Amenity a = new Amenity();
        a.setName(name);
        a.setType(type);
        return a;
    }

    private static City createCitySeedData(String name)
    {
        City a = new City();
        a.setName(name);
        return a;
    }

}
