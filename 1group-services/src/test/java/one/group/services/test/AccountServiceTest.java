/**
 * 
 */
package one.group.services.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import one.group.core.enums.AccountType;
import one.group.core.enums.MediaType;
import one.group.core.enums.status.Status;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.response.WSAccount;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.PhoneNumber;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.services.AccountService;
import one.group.services.PhonenumberService;
import one.group.services.testData.SeedData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * @author ashishthorat
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class AccountServiceTest
{
    @Autowired
    protected ApplicationContext ac;

    SeedData seedData = new SeedData();
    Agent account = seedData.getAccountseedData();

    AccountJpaDAO accountJpaDAO;
    AccountService accountService;
    PhonenumberService phonenumberService;
    PhoneNumber number;

    @Before
    public void setUp()
    {
        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());
    }

    @Test
    public void testFetchAccountDetails()
    {

        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);

        assertNotNull(account.getId());
        String accountId = account.getId();

        WSAccount wsAccount = accountService.fetchAccountDetails(accountId);
        assertNotNull(wsAccount.getId());
        assertEquals(account.getId(), wsAccount.getId());
    }

    @Test(expected = NullPointerException.class)
    public void testFetchInvalidAccountDetails()
    {
        String accountId = "0080119359330992";
        WSAccount wsAccount = accountService.fetchAccountDetails(accountId);
        assertNotNull(wsAccount.getId());
    }

    @Test
    public void testFetchActiveAccountDetails() throws AccountNotFoundException
    {
        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);
        assertNotNull(account.getId());
        String accountId = account.getId();
        WSAccount wsAccount = accountService.fetchActiveAccountDetails(accountId);
        assertNotNull(wsAccount.getId());
        assertEquals(account.getId(), wsAccount.getId());
        assertEquals(Status.ACTIVE, wsAccount.getStatus());
    }

    @Test(expected = AccountNotFoundException.class)
    public void testFetchInActiveAccountDetails() throws AccountNotFoundException
    {
        account.setStatus(Status.INACTIVE);
        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);
        assertNotNull(account.getId());
        String accountId = account.getId();
        WSAccount wsAccount = accountService.fetchActiveAccountDetails(accountId);
        assertNotNull(wsAccount.getId());

    }

    @Test
    public void testFetchAccountByMobileNumber()
    {
        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);
        assertNotNull(account.getId());
        String mobileNumber = account.getPrimaryPhoneNumber().getNumber();
        WSAccount wsAccount = accountService.fetchAccountByMobileNumber(mobileNumber);
        assertNotNull(wsAccount.getId());
        assertEquals(mobileNumber, wsAccount.getPrimaryMobile());
    }

    @Test(expected = NullPointerException.class)
    public void testFetchAccountByInvalidMobileNumber()
    {
        String mobileNumber = "+919820098200";
        WSAccount wsAccount = accountService.fetchAccountByMobileNumber(mobileNumber);
        assertNotNull(wsAccount.getId());
    }

    @Test
    public void testSaveAccount() throws Abstract1GroupException
    {
        String accountId = account.getId();
        AccountType type = account.getType();
        Status Status = account.getStatus();
        String primaryPhoneNumber = number.getNumber();
        String address = account.getAddress();
        String email = account.getEmail();
        String firstname = account.getFullName();
        String lastname = account.getCompanyName();
        String establishmentName = account.getEstablishmentName();
        Date registrationTime = null;
        List<String> localityList = null;
        one.group.entities.api.request.WSPhoto photo = SeedData.createWsMediaSeedData("Media-test", MediaType.IMAGE, "http://10.50.249.13/cdn/accounts/" + "test" + ".jpg");

        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);
        WSAccount wsAccount = accountService.saveOrUpdateAccount(accountId, type, Status, primaryPhoneNumber, address, email, firstname, lastname, establishmentName, localityList, photo, photo,
                registrationTime);

        assertNotNull(wsAccount.getId());
        assertEquals(type, wsAccount.getType());
        assertEquals(Status, wsAccount.getStatus());
        assertEquals(primaryPhoneNumber, wsAccount.getPrimaryMobile());
    }
}
