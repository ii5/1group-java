/**
 * 
 */
package one.group.services.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import one.group.cache.dao.CacheBaseDAO;
import one.group.core.Constant;
import one.group.core.enums.MessageType;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.request.WSSendChatMessage;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.cache.ChatKey;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.PropertyListing;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.LocationJpaDAO;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.LocationService;
import one.group.services.PhonenumberService;
import one.group.services.PropertyListingService;
import one.group.services.testData.SeedData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author ashishthorat
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ChatMessageServiceTest
{
    @Autowired
    protected ApplicationContext ac;

    ChatMessageService chatMessageService;
    String fromAccountId, toAccountId, text;
    PropertyListingJpaDAO propertyListingJpaDAO;
    PropertyListingService propertyListingService;
    CacheBaseDAO cacheBaseDAO;

    @Before
    public void setUp()
    {
        chatMessageService = (ChatMessageService) ac.getBean("chatMessageService");
        cacheBaseDAO = (CacheBaseDAO) ac.getBean("cacheBaseDAO");

    }

    @Test
    public void testTextSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        PropertyListing propertyListing = getPropertyListing();
        MessageType type = MessageType.TEXT;
        text = "Hello";
        toAccountId = "0848995627217167";
        fromAccountId = propertyListing.getAccount().getId();

        String chatThread = "chat-thread-" + toAccountId + "-" + fromAccountId;
        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setText(text);
        WSChatMessage WSChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        assertEquals(chatThread, WSChatMessage.getChatThreadId());
        assertNotNull(WSChatMessage);

        getCleanRedis(fromAccountId, toAccountId, chatThread);
    }

    @Test(expected = NullPointerException.class)
    public void testNullTextSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        MessageType type = MessageType.TEXT;
        text = "Hello";
        toAccountId = "0848995627217167";
        fromAccountId = null;

        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setText(text);
        WSChatMessage WSChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        assertNotNull(WSChatMessage);
    }

    @Test
    public void testPropertyListingSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {

        PropertyListing propertyListing = getPropertyListing();

        MessageType type = MessageType.PROPERTY_LISTING;
        toAccountId = "0848995627217167";
        String propertyListingId = propertyListing.getId();
        String fromAccountId = propertyListing.getAccount().getIdAsString();
        String short_reference = propertyListing.getShortReference();
        String chatThread = "chat-thread-0848995627217167-" + fromAccountId;

        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setPropertyListingId(propertyListingId);
        wsSendChatMessage.setPropertyListingShortReference(short_reference);
        WSChatMessage WSChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);

        assertNotNull(WSChatMessage);
        assertEquals(propertyListingId, WSChatMessage.getPropertyListingId());
        assertEquals(fromAccountId, WSChatMessage.getFromAccountId());
        assertEquals(chatThread, WSChatMessage.getChatThreadId());
    }

    @Test(expected = BroadcastProcessException.class)
    public void testNullPropertyListingSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        PropertyListing propertyListing = getPropertyListing();
        MessageType type = MessageType.PROPERTY_LISTING;
        toAccountId = "0848995627217167";

        String propertyListingId = null;
        String fromAccountId = propertyListing.getAccount().getId();
        String short_reference = null;

        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setPropertyListingId(propertyListingId);
        wsSendChatMessage.setPropertyListingShortReference(short_reference);
        WSChatMessage WSChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        assertNotNull(WSChatMessage);
    }

    @Test
    public void testPhoneCallSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        PropertyListing propertyListing = getPropertyListing();
        MessageType type = MessageType.PHONE_CALL;
        toAccountId = "0848995627217167";
        text = "Hello";
        String propertyListingId = propertyListing.getIdAsString();
        String fromAccountId = propertyListing.getAccount().getId();

        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setPropertyListingId(propertyListingId);
        wsSendChatMessage.setText(text);
        WSChatMessage WSChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        assertEquals(propertyListingId, WSChatMessage.getPropertyListingId());
        assertEquals(fromAccountId, WSChatMessage.getFromAccountId());
    }

    @Test
    public void testInvalidMessageTypeSaveChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        PropertyListing propertyListing = getPropertyListing();
        MessageType type = MessageType.MEDIA;
        toAccountId = "0848995627217167";
        text = "Hello";
        String propertyListingId = propertyListing.getIdAsString();
        String fromAccountId = propertyListing.getAccount().getId();

        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setPropertyListingId(propertyListingId);
        wsSendChatMessage.setText(text);
        WSChatMessage wsChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        assertNotNull(wsChatMessage.getChatThreadId());

    }

    @Test
    public void testInvalidFetchLastChatMessageOfChatThread() throws JsonMappingException, JsonGenerationException, IOException
    {
        String chatThread = "chat-thread-0848995627217167-2629350279210441";
        WSChatMessage wsChatMessage = chatMessageService.fetchMessageOfChatThreadAndType(chatThread);
        assertEquals(null, wsChatMessage.getChatThreadId());
        System.out.println("wsChatMessage" + wsChatMessage);
    }

    private void getCleanRedis(String fromAccountId, String toAccountId, String chatThreadId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, fromAccountId);
        String key = ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values);
        System.out.println("key" + key);
        cacheBaseDAO.removeKey(key);

        Map<String, String> values1 = new HashMap<String, String>();
        values1.put(Constant.CHAT_KEY_ACCOUNT_ID, fromAccountId);
        String key1 = ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(values);
        System.out.println("key" + key1);
        cacheBaseDAO.removeKey(key1);

        Map<String, String> values2 = new HashMap<String, String>();
        values2.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);
        String key2 = ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values);
        System.out.println("key" + key2);
        cacheBaseDAO.removeKey(key2);

        Map<String, String> values3 = new HashMap<String, String>();
        values3.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);
        String key3 = ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(values);
        System.out.println("key" + key3);
        cacheBaseDAO.removeKey(key3);

        Map<String, String> values4 = new HashMap<String, String>();
        values4.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);
        String key4 = ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD.getFormedKey(values);
        System.out.println("key" + key4);
        cacheBaseDAO.removeKey(key4);
    }

    private PropertyListing getPropertyListing()
    {

        SeedData seedData = new SeedData();
        Agent account = seedData.getAccountseedData();
        PropertyListing propertyListing = seedData.getPropertyListingData();

        AccountJpaDAO accountJpaDAO;
        AccountService accountService;
        PhonenumberService phonenumberService;
        PhoneNumber number;
        PropertyListingJpaDAO propertyListingJpaDAO;
        PropertyListingService propertyListingService;
        LocationService localityService;
        LocationJpaDAO localityJpaDao;
        Location location;

        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

        propertyListingService = (PropertyListingService) ac.getBean("propertyListingService");
        propertyListingJpaDAO = (PropertyListingJpaDAO) ac.getBean("propertyListingJpaDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());

        localityJpaDao = (LocationJpaDAO) ac.getBean("localityJpaDao");
        localityService = (LocationService) ac.getBean("localityService");
        String id = "3592";
        location = localityJpaDao.findById(id);

        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);

        propertyListing.setAccount(account);
        propertyListing.setLocation(location);
        propertyListingJpaDAO.persist(propertyListing);

        return propertyListing;
    }
}
