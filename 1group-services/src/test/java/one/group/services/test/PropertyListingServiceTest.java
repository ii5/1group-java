/**
 * 
 */
package one.group.services.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.PropertyListing;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.LocationJpaDAO;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.services.AccountService;
import one.group.services.LocationService;
import one.group.services.PhonenumberService;
import one.group.services.PropertyListingService;
import one.group.services.testData.SeedData;
import one.group.utils.validation.exception.ValidationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * @author ashishthorat
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class PropertyListingServiceTest
{
    @Autowired
    protected ApplicationContext ac;

    SeedData seedData = new SeedData();
    Agent account = seedData.getAccountseedData();
    PropertyListing propertyListing = seedData.getPropertyListingData();

    AccountJpaDAO accountJpaDAO;
    AccountService accountService;
    PhonenumberService phonenumberService;
    PhoneNumber number;
    PropertyListingJpaDAO propertyListingJpaDAO;
    PropertyListingService propertyListingService;
    LocationService localityService;
    LocationJpaDAO localityJpaDao;
    Location location;

    String description, type, subType, commisionType, rooms, localityId, status = null, propertyMarket, accountId, propertyListingId, createdFor = "", externalListingTime = null,
            externalSource = null, customSublocation = null, action = null;
    int area;
    Long sellPrice, rentPrice;
    Boolean isHot;
    List<String> amenities = new ArrayList<String>(Arrays.asList("garden", "parking", "pool", "intercom"));

    @Before
    public void setUp()
    {
        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

        propertyListingService = (PropertyListingService) ac.getBean("propertyListingService");
        propertyListingJpaDAO = (PropertyListingJpaDAO) ac.getBean("propertyListingJpaDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());

        localityJpaDao = (LocationJpaDAO) ac.getBean("localityJpaDao");
        localityService = (LocationService) ac.getBean("localityService");
        String id = "1231";
        location = localityJpaDao.findById(id);

        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);

        propertyListing.setAccount(account);
        propertyListing.setLocation(location);
        propertyListing.setAmenities(amenities.toString());
        propertyListingJpaDAO.persist(propertyListing);

        description = propertyListing.getDescription();
        type = propertyListing.getType().toString();
        commisionType = propertyListing.getCommissionType().toString();
        subType = propertyListing.getSubType().toString();
        area = propertyListing.getArea();
        localityId = propertyListing.getLocation().getId();
        sellPrice = propertyListing.getSalePrice();
        rentPrice = propertyListing.getRentPrice();
        // status = propertyListing.getStatus().toString();
        propertyMarket = propertyListing.getPropertyMarket().toString();

        isHot = propertyListing.getIsHot();
        accountId = propertyListing.getAccount().getId();
        propertyListingId = propertyListing.getId();
        rooms = propertyListing.getRooms();
    }

    // @Test(expected = IllegalArgumentException.class)
    // public void testGetProperyListingOfAccount() throws
    // General1GroupException
    // {
    // String accountId = account.getId();
    // String accountIdFromToken = account.getId();
    // List<WSPropertyListing> WSPropertyListing =
    // propertyListingService.getProperyListingOfAccount(accountId,
    // accountIdFromToken);
    //
    // assertNotNull(WSPropertyListing);
    // assertEquals(WSPropertyListing.get(0).getShortReference(),
    // propertyListing.getShortReference());
    // assertNull(propertyListingService.getProperyListingOfAccount(null));
    // }

    @Test(expected = IllegalArgumentException.class)
    public void testgetProperyListingOfAccount() throws General1GroupException
    {
        String accountId = account.getId();
        Map<String, Integer> propertyListingIds = propertyListingService.getProperyListingOfAccount(accountId);
        assertNotNull(propertyListingIds);
        assertTrue(propertyListingIds.size() > 0);
        assertNull(propertyListingService.getProperyListingOfAccount(null));
    }

    @Test
    public void testGetPropertyListingCountOfAccount()
    {
        String accountId = account.getId();
        String accountIdFromToken = account.getId();
        int propertyListingCount = propertyListingService.getPropertyListingCountOfAccount(accountId, accountIdFromToken, false);

        assertNotNull(propertyListingCount);
        assertTrue(propertyListingCount > 0);
        assertEquals(0, propertyListingService.getPropertyListingCountOfAccount(null, null, false));
    }

    // @Test(expected = ValidationException.class)
    // public void testGetPropertyListingByPropertyListingId() throws
    // Abstract1GroupException
    // {
    // String propertyListingId, accountId, subscribe;
    //
    // propertyListingId = propertyListing.getId();
    // accountId = propertyListing.getAccount().getId();
    // subscribe = null;
    // WSPropertyListing wsPropertyListing =
    // propertyListingService.getPropertyListingByPropertyListingId(propertyListingId,
    // accountId, subscribe);
    // assertEquals(propertyListingId, wsPropertyListing.getId());
    // assertNotNull(wsPropertyListing);
    // assertNotNull(propertyListingService.getPropertyListingByPropertyListingId(null,
    // null, subscribe));
    //
    // }

    @Test(expected = General1GroupException.class)
    public void testInvalidCreatedForSavePropertyListing() throws Abstract1GroupException, ParseException
    {
        createdFor = "1615950387673728"; // Error Occured for Null Values
        // //Changed Required
        area = 342;
        description = "New ffdPropertyListining";

        WSPropertyListing wsPropertyListing = propertyListingService.savePropertyListing(description, type, subType, String.valueOf(area), commisionType, rooms, localityId, amenities,
                String.valueOf(sellPrice), String.valueOf(rentPrice), status, propertyMarket, String.valueOf(isHot), action, accountId, propertyListingId, createdFor, externalListingTime,
                externalSource, customSublocation);

        assertNotNull(wsPropertyListing);
    }

    @Test
    public void testNullChangedRequiredFieldsForSavePropertyListing() throws Abstract1GroupException, ParseException
    {
        String area = null;
        String description = null;
        String subType = null;
        String commisionType = null;
        String sellPrice = null;
        String rooms = null;
        amenities.clear();
        String localityId = null;
        String type = null;

        WSPropertyListing wsPropertyListing = propertyListingService.savePropertyListing(description, type, subType, area, commisionType, rooms, localityId, amenities, sellPrice,
                String.valueOf(rentPrice), status, propertyMarket, String.valueOf(isHot), action, accountId, propertyListingId, createdFor, externalListingTime, externalSource, customSublocation);

        assertEquals(propertyListingId, wsPropertyListing.getId());
        assertNotNull(wsPropertyListing.getId());
        assertNotSame(description, wsPropertyListing.getDescription());
    }

    @Test
    public void testChangedRequiredFieldsForSavePropertyListing() throws Abstract1GroupException, ParseException
    {
        String description = "Property Changed";
        WSPropertyListing wsPropertyListing = propertyListingService.savePropertyListing(description, type, subType, String.valueOf(area), commisionType, rooms, localityId, amenities,
                String.valueOf(sellPrice), String.valueOf(rentPrice), status, propertyMarket, String.valueOf(isHot), action, accountId, propertyListingId, createdFor, externalListingTime,
                externalSource, customSublocation);

        assertNotSame(propertyListingId, wsPropertyListing.getId());
        assertNotNull(wsPropertyListing.getId());
        assertEquals(description, wsPropertyListing.getDescription());
    }

    @Test
    public void testSamePropertyListingIdForSavePropertyListing() throws Abstract1GroupException, ParseException
    {

        String shortReference = propertyListing.getShortReference();
        WSPropertyListing wsPropertyListing = propertyListingService.savePropertyListing(description, type, subType, String.valueOf(area), commisionType, rooms, localityId, amenities,
                String.valueOf(sellPrice), String.valueOf(rentPrice), status, propertyMarket, String.valueOf(isHot), action, accountId, propertyListingId, createdFor, externalListingTime,
                externalSource, customSublocation);

        assertNotSame(propertyListingId, wsPropertyListing.getId());
        assertNotNull(wsPropertyListing.getId());
        assertEquals(shortReference, wsPropertyListing.getShortReference());
    }

    @Test
    public void testNullPropertyListingIdForSavePropertyListing() throws Abstract1GroupException, ParseException
    {
        propertyListingId = null;
        String shortReference = propertyListing.getShortReference();

        WSPropertyListing wsPropertyListing = propertyListingService.savePropertyListing(description, type, subType, String.valueOf(area), commisionType, rooms, localityId, amenities,
                String.valueOf(sellPrice), String.valueOf(rentPrice), status, propertyMarket, String.valueOf(isHot), action, accountId, propertyListingId, createdFor, externalListingTime,
                externalSource, customSublocation);

        assertNotSame(propertyListingId, wsPropertyListing.getId());
        assertNotNull(wsPropertyListing.getId());
        assertNotSame(shortReference, wsPropertyListing.getShortReference());
    }

    @Test
    public void testAutoExpirePropertyListing() throws ParseException, Abstract1GroupException
    {

        PropertyListing PropertyListing = propertyListingService.autoExpirePropertyListing(propertyListingId);
        assertNull(PropertyListing);

    }

    @Test
    public void testGetActiveAndExpiredPropertyListing()
    {
        List<PropertyListing> PropertyListing = propertyListingService.getActiveAndExpiredPropertyListing();
        assertTrue(PropertyListing.size() > 0);
        assertNotNull(PropertyListing);
    }

    // @Test(expected = NullPointerException.class)
    // public void testSavePropertySearchRelation()
    // {
    // propertyListingService.savePropertySearchRelation(null, null);
    //
    // }

    @Test(expected = ValidationException.class)
    public void testBookMarkProperty() throws Exception
    {
        propertyListingService.bookMarkProperty(null, null, null);
    }
}
