/**
 * 
 */
package one.group.services.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import one.group.core.enums.status.DeviceMode;
import one.group.core.enums.status.DeviceStatus;
import one.group.dao.OtpDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.response.WSClient;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Otp;
import one.group.entities.jpa.PhoneNumber;
import one.group.exceptions.movein.General1GroupException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.services.AccountService;
import one.group.services.ClientService;
import one.group.services.OtpService;
import one.group.services.PhonenumberService;
import one.group.services.testData.SeedData;
import one.group.utils.validation.exception.ValidationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ashishthorat
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ClientServiceTest
{
    @Autowired
    protected ApplicationContext ac;

    SeedData seedData = new SeedData();
    Agent account = seedData.getAccountseedData();
    Client client = seedData.getClientSeedData();

    ClientJpaDAO clientJpaDAO;
    ClientService clientService;
    AccountJpaDAO accountJpaDAO;
    AccountService accountService;
    OtpService otpService;
    PhonenumberService phonenumberService;
    PhoneNumber number;
    OtpDAO otpDAO;

    @Before
    public void setUp()
    {
        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        clientService = (ClientService) ac.getBean("clientService");
        clientJpaDAO = (ClientJpaDAO) ac.getBean("clientJpaDAO");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");
        otpService = (OtpService) ac.getBean("otpService");
        otpDAO = (OtpDAO) ac.getBean("otpDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());
        account.setPrimaryPhoneNumber(number);
    }

    @Test(expected = ValidationException.class)
    public void testfetchAllPushChannelsOfClientsOfAccount()
    {

        accountJpaDAO.persist(account);
        client.setAccount(account);
        clientJpaDAO.persist(client);

        List<String> Channels = clientService.fetchAllPushChannelsOfClientsOfAccount(account.getId());
        assertTrue(Channels.size() > 0);
        assertNotNull(Channels);
        assertNull(clientService.fetchAllPushChannelsOfClientsOfAccount(null));
    }

    @Test(expected = ValidationException.class)
    public void testFetchByAccountAndDevicePlatform()
    {
        accountJpaDAO.persist(account);
        client.setAccount(account);
        clientJpaDAO.persist(client);

        List<String> deviceIdList = clientService.fetchByAccountAndDevicePlatform(account.getId(), client.getDevicePlatform());

        assertTrue(deviceIdList.size() > 0);
        assertNotNull(deviceIdList);
        assertNotNull(clientService.fetchByAccountAndDevicePlatform(null, null));
    }

    @Test(expected = ValidationException.class)
    public void testFetchClientByPushChannel()
    {
        clientJpaDAO.persist(client);
        String pushChannel = client.getPushChannel();
        WSClient wsclient = clientService.fetchClientByPushChannel(pushChannel);

        assertEquals(wsclient.getPushChannel(), pushChannel);
        assertNotNull(wsclient);
        assertNull(clientService.fetchClientByPushChannel(null));
    }

    @Test
    public void testFetchClientFromOtpAndMobile() throws General1GroupException
    {
        otpService = (OtpService) ac.getBean("otpService");

        accountJpaDAO.persist(account);
        String mobileNumber = account.getPrimaryPhoneNumber().getNumber();
        System.out.println("mobileNumber" + mobileNumber);
        String otp = "1243";
        otpService.saveOTP(otp, mobileNumber);

        // Create Client
        client.setAccount(account);
        clientJpaDAO.persist(client);

        clientService.checkValidOtpAndDeleteIfValid(otp, mobileNumber);
        Otp otpObj = otpDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);
        assertNull(otpObj);
    }

    @Test
    public void testSaveClient()
    {
        accountJpaDAO.persist(account);

        String againstAccountId = account.getId();
        String appName = client.getAppName();
        String appVersion = client.getAppVersion();
        DeviceMode development = DeviceMode.PRODUCTION;
        String deviceToken = client.getDeviceToken();
        String deviceVersion = client.getDeviceVersion();
        boolean isOnline = true;
        String pushChannel = client.getPushChannel();
        String pushAlert = client.getPushAlert();
        String pushBadge = client.getPushBadge();
        String pushSound = client.getPushSound();
        DeviceStatus status = client.getStatus();
        String devicePlatform = client.getDevicePlatform();
        String cpsId = "1Group";
        WSClient wsClient = clientService.saveClient(account, appName, appVersion, development, deviceToken, deviceVersion, isOnline, pushChannel, pushAlert, pushBadge, pushSound, status,
                devicePlatform, null, cpsId);

        assertEquals(wsClient.getPushChannel(), pushChannel);
        assertEquals(wsClient.getStatus(), status);
        assertNotNull(wsClient);
    }
}
