/**
 * 
 */
package one.group.services.test;

import one.group.dao.OtpDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Otp;
import one.group.entities.jpa.PhoneNumber;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.services.AccountService;
import one.group.services.OtpService;
import one.group.services.PhonenumberService;
import one.group.services.testData.SeedData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author ashishthorat
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class OtpServiceTest
{
    @Autowired
    protected ApplicationContext ac;

    SeedData seedData = new SeedData();
    Agent account = seedData.getAccountseedData();
    Client client = seedData.getClientSeedData();

    AccountJpaDAO accountJpaDAO;
    AccountService accountService;
    ClientJpaDAO clientJpaDAO;
    OtpService otpService;
    PhonenumberService phonenumberService;
    PhoneNumber number;
    OtpDAO otpDAO;

    @Before
    public void setUp()
    {
        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        clientJpaDAO = (ClientJpaDAO) ac.getBean("clientJpaDAO");
        otpService = (OtpService) ac.getBean("otpService");
        otpDAO = (OtpDAO) ac.getBean("otpDAO");

        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());
        account.setPrimaryPhoneNumber(number);
    }

    @Test
    public void testSaveOtp()
    {
        accountJpaDAO.persist(account);
        clientJpaDAO.persist(client);
        String otp = "1243";
        String mobileNumber = account.getPrimaryPhoneNumber().getNumber();
        otpService.saveOTP(otp, mobileNumber);
    }

    @Test
    public void testCheckAndSaveOtp()
    {
        accountJpaDAO.persist(account);
        String otp = "1243";
        String mobileNumber = account.getPrimaryPhoneNumber().getNumber();
        otpService.saveOTP(otp, mobileNumber);

        assertTrue(otpService.checkAndSaveOtp(otp, mobileNumber));
    }

    @Test
    public void testDeleteOTP()
    {
        accountJpaDAO.persist(account);
        String otp = "1243";
        String mobileNumber = account.getPrimaryPhoneNumber().getNumber();
        otpService.saveOTP(otp, mobileNumber);
        otpService.deleteOTP(otp, mobileNumber);

        Otp otpObj = otpDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);
        assertNull(otpObj);
    }
}
