package one.group.services.test;

import javax.transaction.Transactional;

import one.group.dao.PhoneNumberDAO;
import one.group.entities.jpa.PhoneNumber;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.services.AccountService;
import one.group.services.PhonenumberService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class OneGroupEntitiesTest
{
    @Autowired
    protected ApplicationContext ac;

    AccountJpaDAO accountJpaDAO;
    AccountService accountService;
    PhonenumberService phonenumberService;
    PhoneNumber number;
    PhoneNumberDAO phoneNumberDAO;

    @Before
    public void setUp()
    {
        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

    }

    @Test
    public void testSave()
    {
        System.out.println(accountJpaDAO);

    }

}
