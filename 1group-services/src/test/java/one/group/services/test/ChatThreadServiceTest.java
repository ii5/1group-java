/**
 * 
 */
package one.group.services.test;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.MessageType;
import one.group.dao.ChatThreadDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.request.WSSendChatMessage;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.WSChatThread;
import one.group.entities.api.response.WSChatThreadCursor;
import one.group.entities.api.response.WSChatThreadList;
import one.group.entities.api.response.WSChatThreadReference;
import one.group.entities.api.response.WSParticipants;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.PropertyListing;
import one.group.exceptions.movein.ChatProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.LocationJpaDAO;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.ChatThreadService;
import one.group.services.LocationService;
import one.group.services.PhonenumberService;
import one.group.services.PropertyListingService;
import one.group.services.testData.SeedData;
import one.group.utils.Utils;
import one.group.utils.validation.exception.ValidationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * @author ashishthorat
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
        "classpath:applicationContext-configuration-PushService.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ChatThreadServiceTest
{
    private static final Logger logger = LoggerFactory.getLogger(ChatThreadServiceTest.class);

    @Autowired
    protected ApplicationContext ac;

    ChatThreadService chatThreadService;
    String fromAccountId, toAccountId, text;
    PropertyListingJpaDAO propertyListingJpaDAO;
    PropertyListingService propertyListingService;
    ChatMessageService chatMessageService;
    WSChatMessage wsChatMessage;
    WSChatThreadCursor wsChatThreadCursor;
    int readIndex;
    int receiveIndex;

    Integer maxChatThreadIndex;
    String offsetStr;
    String limitStr;
    int limit, offset;
    HashMap<String, Integer> limitAndOffset = new HashMap();

    @Before
    public void setUp()
    {
        chatThreadService = (ChatThreadService) ac.getBean("chatThreadService");
    }

    @Test
    public void testThread() throws JsonMappingException, JsonGenerationException, BroadcastProcessException, ValidationException, SyncLogProcessException, IOException, InterruptedException
    {
        try
        {
            for (int i = 0; i < 15; i++)
            {
                // Thread th = new Thread(new MultiChatThread(this));
                // th.start();
                // Thread.sleep(10);
                testChatThread();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testChatThreadByActivityIndex() throws JsonMappingException, JsonGenerationException, ChatProcessException, IOException, BroadcastProcessException
    {
        String indexBy = Constant.DEFAULT_CHAT_THREAD_INDEX_BY;
        wsChatMessage = getChatMessage();
        fromAccountId = wsChatMessage.getFromAccountId();

        for (int i = 0; i < 10; i++)
        {
            getChatMessage();

        }
        // Fetch max chat thread index
        Integer maxChatThreadIndex = chatThreadService.getMaxChatThreadIndex(fromAccountId, indexBy);

        Map<String, Integer> limitAndOffset = Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        int limit = limitAndOffset.get("limit");
        int offset = limitAndOffset.get("offset");

        // Fetch chat thread list of an account And Return Latest Send Message
        List<WSChatThreadReference> chatThreads = chatThreadService.fetchChatThreadOfAccount(fromAccountId, indexBy, offset, limit);
        String ChatThreadMessage = chatThreads.get(0).getChatThread().getChatMessages().get(0).getText();

        WSChatThreadList wsChatThreadList = new WSChatThreadList(chatThreads, maxChatThreadIndex);
        assertNotNull(wsChatThreadList);
        assertEquals(ChatThreadMessage, wsChatMessage.getText());

    }

    @Test
    public void testChatThread() throws JsonMappingException, JsonGenerationException, BroadcastProcessException, IOException, ValidationException, SyncLogProcessException
    {
        WSChatThreadCursor wsChatThreadCursor;
        ChatThreadDAO chatThreadDAO = (ChatThreadDAO) ac.getBean("chatThreadDAO");
        wsChatMessage = getChatMessage();

        String accountId = "7462845665269548";
        String fromAccountId = "0848995627217167";
        String chatThreadId = wsChatMessage.getChatThreadId();

        // Message Sending fromAcconutId
        int fromAccountIdreadIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, fromAccountId));
        int fromAccountIdreceiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, fromAccountId));

        System.out.println("fromAcconutId  readIndex: " + fromAccountIdreadIndex);
        System.out.println("fromAcconutId  receiveIndex: " + fromAccountIdreceiveIndex);

        // Message Recieve accountId index
        int toAccountIdReadIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId));
        int toAccountIdReceiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId));
        System.out.println("accountId readIndex: " + toAccountIdReadIndex);
        System.out.println("accountId receiveIndex: " + toAccountIdReceiveIndex);

        wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, fromAccountIdreadIndex, fromAccountIdreceiveIndex);

        assertEquals(wsChatThreadCursor.getReadIndex().intValue(), fromAccountIdreadIndex);
        assertEquals(wsChatThreadCursor.getReceivedIndex().intValue(), fromAccountIdreceiveIndex);
    }

    @Test
    public void testFetchChatThreadParticipants() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, ChatProcessException, IOException
    {
        wsChatMessage = getChatMessage();
        String chatThreadId = wsChatMessage.getChatThreadId();

        List<WSParticipants> wsParticipantsList = chatThreadService.fetchChatThreadParticipants(chatThreadId);
        assertTrue(wsParticipantsList.size() > 0);
    }

    @Test
    public void testUpdateCursorAndThredParticipants() throws JsonMappingException, JsonGenerationException, BroadcastProcessException, IOException, ValidationException, SyncLogProcessException
    {
        WSChatThreadCursor wsChatThreadCursor;
        ChatThreadDAO chatThreadDAO = (ChatThreadDAO) ac.getBean("chatThreadDAO");
        wsChatMessage = getChatMessage();

        String accountId = "7462845665269548";
        String fromAccountId = "0848995627217167";
        String chatThreadId = wsChatMessage.getChatThreadId();

        // Message Sending fromAcconutId
        int fromAccountIdreadIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId));
        int fromAccountIdreceiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId));

        System.out.println("fromAcconutId  readIndex" + fromAccountIdreadIndex);
        System.out.println("fromAcconutId  receiveIndex" + fromAccountIdreceiveIndex);

        wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, fromAccountIdreadIndex, fromAccountIdreceiveIndex);

        int readIndex = wsChatThreadCursor.getReadIndex();
        int receiveIndex = wsChatThreadCursor.getReceivedIndex();
        System.out.println("After Update  readIndex" + readIndex);
        System.out.println("After Update  receiveIndex" + receiveIndex);

        List<WSParticipants> wsParticipantsList = chatThreadService.fetchChatThreadParticipants(chatThreadId);
        int ParticipantsReadIndex = wsParticipantsList.get(1).getCursors().getReadIndex();

        System.out.println("wsParticipantsList-00" + wsParticipantsList.get(0).getCursors().getReadIndex());
        System.out.println("wsParticipantsList-11" + wsParticipantsList.get(1).getCursors().getReadIndex());

        assertSame(readIndex, ParticipantsReadIndex);
    }

    @Test
    public void testInvalidFetchChatThreadParticipants()
    {
        toAccountId = "0848995767217167";
        fromAccountId = "9227691561742237";

        String chatThreadId = "chat-thread-" + toAccountId + "-" + fromAccountId;
        List<WSParticipants> wsParticipantsList = chatThreadService.fetchChatThreadParticipants(chatThreadId);
        assertTrue(wsParticipantsList.isEmpty());
    }

    @Test
    public void testFetchChatThreadDetails() throws JsonMappingException, JsonGenerationException, ChatProcessException, General1GroupException, IOException, BroadcastProcessException
    {
        int limit = -20;
        int offset = -20;
        wsChatMessage = getChatMessage();
        String chatThreadId = wsChatMessage.getChatThreadId();
        fromAccountId = wsChatMessage.getFromAccountId();

        WSChatThread WSChatThread = chatThreadService.fetchChatThreadDetails(fromAccountId, chatThreadId, offset, limit);
        assertNotNull(WSChatThread);
        assertEquals(chatThreadId, WSChatThread.getChatThreadId());
    }

    @Test(expected = NullPointerException.class)
    public void testInvalidAccountForFetchChatThreadDetails() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, ChatProcessException, General1GroupException,
            IOException
    {
        int limit = 0;
        int offset = 0;
        wsChatMessage = getChatMessage();
        String chatThreadId = wsChatMessage.getChatThreadId();
        fromAccountId = null;

        WSChatThread WSChatThread = chatThreadService.fetchChatThreadDetails(fromAccountId, chatThreadId, offset, limit);
        assertNull(WSChatThread);
    }

    @Test
    public void testSameChatMessage() throws JsonMappingException, JsonGenerationException, BroadcastProcessException, IOException
    {
        chatMessageService = (ChatMessageService) ac.getBean("chatMessageService");

        text = "Movein";
        toAccountId = "7462845665269548";
        fromAccountId = "0848995627217167";
        Date dt = new Date();

        MessageType type = MessageType.TEXT;
        String chatThread = "chat-thread-" + toAccountId + "-" + fromAccountId;
        // First Message
        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setText(text);
        wsSendChatMessage.setClientSentTime(dt.getTime());

        wsChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        String firstMessageClientSentTime = wsChatMessage.getClientSentTime();
        int firstMessageIndex = wsChatMessage.getIndex();

        // Second Message
        WSSendChatMessage wsSendChatMessage1 = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage1.setFromAccountId(fromAccountId);
        wsSendChatMessage1.setText(text);
        wsSendChatMessage1.setClientSentTime(dt.getTime());

        wsChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage1);
        int secondMessageIndex = wsChatMessage.getIndex();
        String SecondMessageClientSentTime = wsChatMessage.getClientSentTime();

        assertNotSame(firstMessageClientSentTime, SecondMessageClientSentTime);
        assertSame(firstMessageIndex, secondMessageIndex);
    }

    public void testUpdateChatThreadCursor() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, ValidationException, SyncLogProcessException, IOException
    {
        WSChatThreadCursor wsChatThreadCursor;
        ChatThreadDAO chatThreadDAO = (ChatThreadDAO) ac.getBean("chatThreadDAO");
        wsChatMessage = getChatMessage();

        String accountId = "7462845665269548";
        String fromAccountId = "0848995627217167";
        String chatThreadId = wsChatMessage.getChatThreadId();

        // Message Sending fromAcconutId
        int fromAccountIdreadIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId));
        int fromAccountIdreceiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId));

        System.out.println("fromAcconutId  readIndex" + fromAccountIdreadIndex);
        System.out.println("fromAcconutId  receiveIndex" + fromAccountIdreceiveIndex);

        // Message Recieve accountId index
        int toAccountIdReadIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId));
        int toAccountIdReceiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId));
        System.out.println("accountId readIndex" + toAccountIdReadIndex);
        System.out.println("accountId receiveIndex" + toAccountIdReceiveIndex);

        // if readIndex = 0,receiveIndex = 0 then No Update
        // wsChatThreadCursor =
        // chatThreadService.updateChatThreadCursor(chatThreadId, accountId, 0,
        // 0);
        // assertSame(wsChatThreadCursor.getReadIndex(), toAccountIdReadIndex);
        // assertSame(wsChatThreadCursor.getReceivedIndex(),
        // toAccountIdReceiveIndex);

        // if readIndex > fromAccountIdreadIndex and receiveIndex =0;
        // then toAccountIdReadIndex == fromAccountIdreadIndex
        // toAccountIdReceiveIndex == fromAccountIdreceiveIndex

        wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, fromAccountIdreadIndex + 2, 0);
        assertSame(wsChatThreadCursor.getReadIndex(), fromAccountIdreadIndex);
        assertSame(wsChatThreadCursor.getReceivedIndex(), fromAccountIdreceiveIndex);

        // if readIndex > fromAccountIdreadIndex and receiveIndex >
        // fromAccountIdreceiveIndex;
        // then toAccountIdReadIndex == fromAccountIdreadIndex
        // toAccountIdReceiveIndex == fromAccountIdreceiveIndex
        wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, fromAccountIdreadIndex + 4, fromAccountIdreceiveIndex + 4);
        assertSame(wsChatThreadCursor.getReadIndex(), fromAccountIdreadIndex);
        assertSame(wsChatThreadCursor.getReceivedIndex(), fromAccountIdreceiveIndex);
        // // assertSame(wsChatThreadCursor.getReadIndex(),
        // fromAccountIdreadIndex);
        // assertSame(wsChatThreadCursor.getReceivedIndex(),
        // fromAccountIdreceiveIndex);
        wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, 2, 3);
        assertSame(wsChatThreadCursor.getReadIndex(), toAccountIdReadIndex);
        assertSame(wsChatThreadCursor.getReceivedIndex(), toAccountIdReceiveIndex);
    }

    @Test(expected = ValidationException.class)
    public void testUpdateInvalidChatThreadCursor() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, ValidationException, SyncLogProcessException, IOException
    {
        wsChatMessage = getChatMessage();
        ChatThreadDAO chatThreadDAO = (ChatThreadDAO) ac.getBean("chatThreadDAO");
        String accountId = "7462845665269548";
        String chatThreadId = wsChatMessage.getChatThreadId();

        int readIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId, accountId));
        int receiveIndex = Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId, accountId));

        WSChatThreadCursor wsChatThreadCursor = chatThreadService.updateChatThreadCursor(chatThreadId, accountId, -10, -4);

        assertSame(wsChatThreadCursor.getReadIndex(), readIndex);
        assertSame(wsChatThreadCursor.getReceivedIndex(), receiveIndex);
    }

    @Test
    public void testFetchChatThreadOfAccount() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, ChatProcessException, IOException
    {
        wsChatMessage = getChatMessage();

        fromAccountId = wsChatMessage.getFromAccountId();
        String indexBy = Constant.CHAT_THREAD_BY_ACTIVITY;

        List<WSChatThreadReference> wsChatThreadReference = chatThreadService.fetchChatThreadOfAccount(fromAccountId, indexBy, 0, 10);
        assertNotNull(wsChatThreadReference);
        assertTrue(wsChatThreadReference.size() > 0);
    }

    @Test
    public void testGetMaxChatThreadIndex() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException, ChatProcessException
    {
        wsChatMessage = getChatMessage();

        fromAccountId = wsChatMessage.getFromAccountId();
        String indexBy = Constant.CHAT_THREAD_BY_ACTIVITY;

        Integer maxChatThreadIndex = chatThreadService.getMaxChatThreadIndex(fromAccountId, indexBy);
        assertNotNull(maxChatThreadIndex);
    }

    // @Test(expected = ValidationException.class)
    // public void testUpdateInvalidChatThreadCursor() throws
    // PropertyListingProcessException, JsonMappingException,
    // JsonGenerationException, ValidationException, SyncLogProcessException,
    // IOException
    // {
    // wsChatMessage = getChatMessage();
    // ChatThreadDAO chatThreadDAO = (ChatThreadDAO)
    // ac.getBean("chatThreadDAO");
    // String accountId = "7462845665269548";
    // String chatThreadId = wsChatMessage.getChatThreadId();
    //
    // int readIndex =
    // Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReadIndex(chatThreadId,
    // accountId));
    // int receiveIndex =
    // Integer.parseInt(chatThreadDAO.fetchChatThreadCursorReceivedIndex(chatThreadId,
    // accountId));
    //
    // WSChatThreadCursor wsChatThreadCursor =
    // chatThreadService.updateChatThreadCursor(chatThreadId, accountId, -10,
    // -4);
    //
    // assertSame(wsChatThreadCursor.getReadIndex(), readIndex);
    // assertSame(wsChatThreadCursor.getReceivedIndex(), receiveIndex);
    // }
    //
    // @Test
    // public void testFetchChatThreadOfAccount() throws
    // PropertyListingProcessException, JsonMappingException,
    // JsonGenerationException, ChatProcessException, IOException
    // {
    // wsChatMessage = getChatMessage();
    // WSChatMessage wsLastChatMessage =
    // chatMessageService.fetchLastChatMessageOfChatThread(wsChatMessage.getChatThreadId());
    // Integer maxChatMessageIndex = wsLastChatMessage.getIndex();
    //
    // fromAccountId = wsChatMessage.getFromAccountId();
    // String indexBy = Constant.CHAT_THREAD_BY_ACTIVITY;
    //
    // List<WSChatThreadReference> wsChatThreadReference =
    // chatThreadService.fetchChatThreadOfAccount(fromAccountId, indexBy, 0,
    // 10);
    // assertNotNull(wsChatThreadReference);
    // assertTrue(wsChatThreadReference.size() > 0);
    // }
    //
    // @Test
    // public void testGetMaxChatThreadIndex() throws
    // PropertyListingProcessException, JsonMappingException,
    // JsonGenerationException, IOException, ChatProcessException
    // {
    // wsChatMessage = getChatMessage();
    //
    // fromAccountId = wsChatMessage.getFromAccountId();
    // String indexBy = Constant.CHAT_THREAD_BY_ACTIVITY;
    //
    // Integer maxChatThreadIndex =
    // chatThreadService.getMaxChatThreadIndex(fromAccountId, indexBy);
    // assertNotNull(maxChatThreadIndex);
    // }

    @Test
    public void testFetchChatThreadsOfAccountByProperty() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        wsChatMessage = getPropertyChatMessage();

        String accountId = wsChatMessage.getFromAccountId();
        String chatThreadId = wsChatMessage.getChatThreadId();
        String propertyListingId = wsChatMessage.getPropertyListingId();
        String indexBy = Constant.CHAT_THREAD_BY_ACTIVITY;

        int limit = 10;
        int offset = 0;
        List<WSChatThreadReference> wsChatThreadReference = chatThreadService.fetchChatThreadsOfAccountByProperty(accountId, propertyListingId, indexBy, offset, limit);
        assertEquals(chatThreadId, wsChatThreadReference.get(0).getChatThread().getChatThreadId());
    }

    private WSChatMessage getChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        chatMessageService = (ChatMessageService) ac.getBean("chatMessageService");
        Date dt = new Date();

        text = "Movein" + dt.getTime();
        toAccountId = "7462845665269548";
        fromAccountId = "0848995627217167";

        MessageType type = MessageType.TEXT;
        String chatThread = "chat-thread-" + toAccountId + "-" + fromAccountId;
        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setText(text);

        wsChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        return wsChatMessage;
    }

    @Test
    public void testGetOffsetAndLimit()
    {

        // if Offstr and limitstr = null
        // MaxThreadIndex < 20 then Offset always 0 and Limit equal to
        // maxThread+1
        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 11);
        assertEquals(offset, 0);

        maxChatThreadIndex = 15;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 16);
        assertEquals(offset, 0);

        maxChatThreadIndex = 19;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 20);
        assertEquals(offset, 0);

        // if Offstr and limitstr = null
        // MaxThreadIndex > 20 then Offset increment by 1 for each increment in
        // 20 and Limit =
        // 20

        maxChatThreadIndex = 20;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 20);
        assertEquals(offset, 1);

        maxChatThreadIndex = 21;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 20);
        assertEquals(offset, 2);

        maxChatThreadIndex = 28;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 20);
        assertEquals(offset, 9);

        maxChatThreadIndex = 180;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 20);
        assertEquals(offset, 161);

        maxChatThreadIndex = 0;
        offsetStr = null;
        limitStr = null;
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 1);
        assertEquals(offset, 0);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-10";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 10);
        assertEquals(offset, 1);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-5";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 5);
        assertEquals(offset, 6);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-10";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 10);
        assertEquals(offset, 1);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-11";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 11);
        assertEquals(offset, 0);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-19";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 11);
        assertEquals(offset, 0);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "-30";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 11);
        assertEquals(offset, 0);

        maxChatThreadIndex = 20;
        offsetStr = null;
        limitStr = "-42";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 21);
        assertEquals(offset, 0);

        maxChatThreadIndex = 50;
        offsetStr = null;
        limitStr = "-79";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 51);
        assertEquals(offset, 0);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "30";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 30);
        assertEquals(offset, 10);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "50";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 50);
        assertEquals(offset, 10);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "50";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 50);
        assertEquals(offset, 10);

        maxChatThreadIndex = 10;
        offsetStr = null;
        limitStr = "50";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");

        assertEquals(limit, 50);
        assertEquals(offset, 10);

        maxChatThreadIndex = 10;
        offsetStr = "15";
        limitStr = "-2";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");
        assertEquals(limit, 2);
        assertEquals(offset, 9);

        // if Offset > MaxChatThredIndex and limit is Positive then limit and
        // offset =0;
        maxChatThreadIndex = 10;
        offsetStr = "15";
        limitStr = "2";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");
        assertEquals(limit, 0);
        assertEquals(offset, 0);

        maxChatThreadIndex = 10;
        offsetStr = "10";
        limitStr = "2";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");
        assertEquals(limit, 2);
        assertEquals(offset, 10);

        maxChatThreadIndex = 10;
        offsetStr = "10";
        limitStr = "0";
        limitAndOffset = (HashMap<String, Integer>) Utils.getOffsetAndLimit(offsetStr, limitStr, maxChatThreadIndex);
        limit = limitAndOffset.get("limit");
        offset = limitAndOffset.get("offset");
        assertEquals(limit, 0);
        assertEquals(offset, 0);
    }

    private WSChatMessage getPropertyChatMessage() throws BroadcastProcessException, JsonMappingException, JsonGenerationException, IOException
    {
        SeedData seedData = new SeedData();
        Agent account = seedData.getAccountseedData();
        PropertyListing propertyListing = seedData.getPropertyListingData();

        AccountJpaDAO accountJpaDAO;
        AccountService accountService;
        PhonenumberService phonenumberService;
        PhoneNumber number;
        PropertyListingJpaDAO propertyListingJpaDAO;
        PropertyListingService propertyListingService;
        LocationService localityService;
        LocationJpaDAO localityJpaDao;
        Location location;

        accountJpaDAO = (AccountJpaDAO) ac.getBean("accountJpaDAO");
        accountService = (AccountService) ac.getBean("accountService");
        phonenumberService = (PhonenumberService) ac.getBean("phonenumberService");
        PhoneNumberDAO phoneNumberDAO = (PhoneNumberDAO) ac.getBean("phoneNumberDAO");

        propertyListingService = (PropertyListingService) ac.getBean("propertyListingService");
        propertyListingJpaDAO = (PropertyListingJpaDAO) ac.getBean("propertyListingJpaDAO");

        PhoneNumber phoneNumber = seedData.getPhoneNumber();
        phonenumberService.savePhoneNumber(phoneNumber.getNumber(), phoneNumber.getType());
        number = phoneNumberDAO.fetchPhoneNumber(phoneNumber.getNumber());

        localityJpaDao = (LocationJpaDAO) ac.getBean("localityJpaDao");
        localityService = (LocationService) ac.getBean("localityService");
        String id = "3592";
        location = localityJpaDao.findById(id);

        account.setPrimaryPhoneNumber(number);
        accountJpaDAO.persist(account);

        propertyListing.setAccount(account);
        propertyListing.setLocation(location);
        propertyListingJpaDAO.persist(propertyListing);

        chatMessageService = (ChatMessageService) ac.getBean("chatMessageService");

        text = "New Property Listings Adedd";
        toAccountId = "7462845665269548";
        fromAccountId = "0848995627217167";

        MessageType type = MessageType.PROPERTY_LISTING;
        String chatThread = "chat-thread-" + toAccountId + "-" + fromAccountId;
        WSSendChatMessage wsSendChatMessage = new WSSendChatMessage(type, toAccountId);
        wsSendChatMessage.setFromAccountId(fromAccountId);
        wsSendChatMessage.setPropertyListingShortReference(propertyListing.getShortReference());
        wsSendChatMessage.setPropertyListingId(propertyListing.getId());
        wsChatMessage = chatMessageService.saveChatMessage(wsSendChatMessage);
        return wsChatMessage;
    }

}
