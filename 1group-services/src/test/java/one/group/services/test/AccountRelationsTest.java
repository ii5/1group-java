package one.group.services.test;
//package one.group.services.test;
//
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Set;
//import java.util.StringTokenizer;
//import java.util.TreeMap;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.movein.core.enums.status.Status;
//import com.movein.entities.api.request.WSContacts;
//import com.movein.entities.api.request.WSNativeContact;
//import com.movein.entities.api.response.WSAccountRelations;
//import com.movein.entities.jpa.Account;
//import com.movein.entities.jpa.AccountRelations;
//import com.movein.entities.jpa.Agent;
//import com.movein.entities.jpa.PhoneNumber;
//import com.movein.jpa.dao.AccountJpaDAO;
//import one.group.services.AccountRelationService;
//import one.group.services.AccountService;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "classpath:applicationContext-services.xml", "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml",
//        "classpath:applicationContext-configuration-JPA.xml", "classpath:applicationContext-DAO-cache.xml", "classpath:applicationContext-configuration-Cache.xml",
//        "classpath:applicationContext-configuration-Cache.xml", "classpath:applicationContext-configuration-PushService.xml" })
//@Transactional
//public class AccountRelationsTest
//{
//    private static String CATEGORY_SYNC_ONLY = "S";
//    private static String CATEGORY_SYNC_BLOCK_UNBLOCK = "SBU";
//    private static String CATEGORY_SYNC_SCORE = "SS";
//
//    int[] scenarioActions = { 1, 2, 2, 3, 4, 1, 2, 3 };
//    int[] scenarioActionsSum = { 1, 3, 5, 8, 12, 13, 15, 18 };
//
//    int MAX_ACTION_COUNT_PER_SCENARIO = 5;
//
//    private static String COMMA = ",";
//    private static String UNDERSCORE = "_";
//
//    @Autowired
//    protected ApplicationContext applicationContext;
//
//    private AccountJpaDAO accountJpaDAO;
//    private AccountRelationService accountRelationService;
//    private AccountService accountService;
//
//    private Account account1;
//
//    private Account account2;
//
//    private Map<String, WSAccountRelations> resultMap = new TreeMap<String, WSAccountRelations>();
//
//    private Map<String, String> phoneNumberMap = new TreeMap<String, String>();
//
//    private long startPhoneNumber = 5000000000l;
//
//    private List<String> mainResultList = new ArrayList<String>();
//
//    private long currentPhoneNumber = startPhoneNumber;
//
//    public AccountJpaDAO getAccountJpaDAO()
//    {
//        return accountJpaDAO;
//    }
//
//    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
//    {
//        this.accountJpaDAO = accountJpaDAO;
//    }
//
//    public AccountRelationService getAccountRelationService()
//    {
//        return accountRelationService;
//    }
//
//    public void setAccountRelationService(AccountRelationService accountRelationService)
//    {
//        this.accountRelationService = accountRelationService;
//    }
//
//    public long getCurrentPhoneNumber()
//    {
//        return currentPhoneNumber;
//    }
//
//    public void setCurrentPhoneNumber(long currentPhoneNumber)
//    {
//        this.currentPhoneNumber = currentPhoneNumber;
//    }
//
//    public Account getAccount1()
//    {
//        return account1;
//    }
//
//    public void setAccount1(Account account1)
//    {
//        this.account1 = account1;
//    }
//
//    public Account getAccount2()
//    {
//        return account2;
//    }
//
//    public void setAccount2(Account account2)
//    {
//        this.account2 = account2;
//    }
//
//    public AccountService getAccountService()
//    {
//        return accountService;
//    }
//
//    public void setAccountService(AccountService accountService)
//    {
//        this.accountService = accountService;
//    }
//
//    @Rollback(true)
//    private Account createAccount(String fullName, String companyName, Status accountStatus, String key)
//    {
//        Account account = new Agent();
//        account.setFullName(fullName);
//        account.setCompanyName(companyName);
//        account.setStatus(accountStatus);
//        PhoneNumber number = new PhoneNumber();
//        String numberStr = "+91" + getCurrentPhoneNumber();
//        phoneNumberMap.put(key, numberStr);
//        number.setNumber(numberStr);
//        account.setPrimaryPhoneNumber(number);
//        accountJpaDAO.persist(account);
//        setCurrentPhoneNumber(++currentPhoneNumber);
//        return account;
//    }
//
//    private void init()
//    {
//        mainResultList.clear();
//        AccountRelationService accountRelationService = (AccountRelationService) applicationContext.getBean("accountRelationService");
//        setAccountRelationService(accountRelationService);
//
//        AccountJpaDAO accountJpaDAO = (AccountJpaDAO) applicationContext.getBean("accountJpaDAO");
//        setAccountJpaDAO(accountJpaDAO);
//
//        AccountService accountService = (AccountService) applicationContext.getBean("accountService");
//        setAccountService(accountService);
//
//    }
//
//    private WSNativeContact getNativeContact(String fullName, String companyName, List<String> numbers)
//    {
//        WSNativeContact wsNativeContact = new WSNativeContact();
//        wsNativeContact.setFullName(fullName);
//        wsNativeContact.setCompanyName(companyName);
//        wsNativeContact.setNumbers(numbers);
//        return wsNativeContact;
//    }
//
//    private WSContacts getNativeContact(String phoneNumber)
//    {
//        List<String> numbers = new ArrayList<String>();
//        List<WSNativeContact> wsNativeContacts = new ArrayList<WSNativeContact>();
//        numbers.add(phoneNumber);
//        wsNativeContacts.add(getNativeContact("fname", "lname", numbers));
//        WSContacts contacts = new WSContacts();
//        contacts.setNativeContacts(wsNativeContacts);
//        contacts.setInitialContactsSyncComplete(true);
//        return contacts;
//    }
//
//    private void preSetOnlySync(int scenario, int indx) throws Exception
//    {
//        String account1Name = "Acc" + scenario;
//        String account2Name = "Acc" + (scenario + 1);
//        indx = indx + 1;
//        String key = CATEGORY_SYNC_ONLY + UNDERSCORE + scenario + UNDERSCORE + indx + UNDERSCORE + "1";
//        String key2 = CATEGORY_SYNC_ONLY + UNDERSCORE + scenario + UNDERSCORE + indx + UNDERSCORE + "2";
//        switch (scenario)
//        {
//        // A1 is active A2 is Active and A2 has already synced contact of A1
//        case 1:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            break;
//        // A1 is active and A2 is active
//        case 2:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            break;
//        // A1 is inactive, A2 is inactive and A1 had synced A2 and A2 had synced
//        // A1
//        case 3:
//            account1 = createAccount(account1Name, account1Name, Status.UNREGISTERED, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            break;
//
//        // A1 is inactive, A2 is inactive and A2 had synced A1
//        case 4:
//            account1 = createAccount(account1Name, account1Name, Status.UNREGISTERED, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            break;
//        // A1 is inactive, A2 is inactive and A2 had not synced A1 and A1 has
//        // not synced A2
//        case 5:
//            account1 = createAccount(account1Name, account1Name, Status.UNREGISTERED, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            break;
//
//        // A1 is active, A2 is inactive and A2 had synced A1 and A1 has
//        // synced A2
//        case 6:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            break;
//
//        // A1 is active, A2 is inactive and A2 had synced A1
//        case 7:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            break;
//
//        // A1 is active, A2 is inactive and A2 had not synced A1 and A1 had not
//        // synced A2
//        case 8:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            break;
//
//        }
//
//    }
//
//    private void preSetSyncBlockUnBlockScore(int scenario, boolean isBlockSet, boolean isScoreSet, int score, int indx) throws Exception
//    {
//        String account1Name = "Acc" + scenario;
//        String account2Name = "Acc" + (scenario + 1);
//        String key = "";
//        if (isBlockSet)
//            key = CATEGORY_SYNC_BLOCK_UNBLOCK + UNDERSCORE + scenario + UNDERSCORE + indx;
//        if (isScoreSet)
//            key = CATEGORY_SYNC_SCORE + UNDERSCORE + scenario + UNDERSCORE + indx;
//
//        String key1 = key + UNDERSCORE + "1";
//        String key2 = key + UNDERSCORE + "2";
//
//        switch (scenario)
//        {
//        // A1 is inactive A2 is inactive
//        case 1:
//            account1 = createAccount(account1Name, account1Name, Status.UNREGISTERED, key1);
//            account2 = createAccount(account2Name, account2Name, Status.UNREGISTERED, key2);
//            break;
//        // A1 is active and A2 is active
//        case 2:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//                accountRelationService.createOrUpdateAccountRelationBlock(account2.getId(), account1.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), score);
//            }
//            break;
//        // A1 is active and A2 is active
//        case 3:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//                accountRelationService.createOrUpdateAccountRelationBlock(account2.getId(), account1.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), score);
//            }
//            break;
//
//        // A1 is active and A2 is active
//        case 4:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//                accountRelationService.createOrUpdateAccountRelationBlock(account2.getId(), account1.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), score);
//            }
//            break;
//        // A1 is active and A2 is active
//        case 5:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//                accountRelationService.createOrUpdateAccountRelationBlock(account2.getId(), account1.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), score);
//            }
//            break;
//
//        // A1 is active and A2 is active
//        case 6:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//            }
//            break;
//
//        // A1 is active and A2 is active
//        case 7:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            if (isBlockSet)
//            {
//                accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//            }
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), 0);
//            }
//            break;
//
//        // A1 is active and A2 is active
//        case 8:
//            account1 = createAccount(account1Name, account1Name, Status.ACTIVE, key1);
//            account2 = createAccount(account2Name, account2Name, Status.ACTIVE, key2);
//            if (isScoreSet)
//            {
//                accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), 0);
//                accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), 0);
//            }
//            break;
//
//        }
//
//    }
//
//    private void actionSyncOnly(int scenario, int actionCount) throws Exception
//    {
//        switch (scenario)
//        {
//        // Action: Account 1 is syncing Account 2
//        case 1:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                // A1 -> S(A2)
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 2:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                // A1 -> S(A2)
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//
//                // A2 -> S(A1)
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                }
//
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 3:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId());
//                    break;
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 4:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId());
//                    break;
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//                case 3:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 5:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId());
//                    break;
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//                case 3:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                case 4:
//                    accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//                    break;
//
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 6:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 7:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//                }
//            }
//            break;
//
//        // Action: Account 1 is syncing Account 2
//        case 8:
//            for (int i = 0; i < actionCount; i++)
//            {
//                preSetOnlySync(scenario, i);
//                switch (i + 1)
//                {
//                case 1:
//                    accountRelationService.syncContactsForAccount(account1.getId(), getNativeContact(account2.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                case 2:
//                    accountRelationService.syncContactsForAccount(account2.getId(), getNativeContact(account1.getPrimaryPhoneNumber().getNumber()));
//                    break;
//                case 3:
//                    accountRelationService.syncContactsForAccount(account2.getId());
//                    break;
//
//                }
//            }
//            break;
//
//        }
//    }
//
//    private void populateResultMap(String key)
//    {
//
//        account1 = accountJpaDAO.fetchAccountByPhoneNumber(phoneNumberMap.get(key + UNDERSCORE + "1"));
//        account2 = accountJpaDAO.fetchAccountByPhoneNumber(phoneNumberMap.get(key + UNDERSCORE + "2"));
//        Set<AccountRelations> accountRelationsSet1 = account1.getAccountRelations();
//        Set<AccountRelations> accountRelationsSet2 = account2.getAccountRelations();
//        AccountRelations accountRelation1 = null, accountRelation2 = null;
//        if (accountRelationsSet1.size() > 0)
//        {
//            accountRelation1 = accountRelationsSet1.iterator().next();
//            accountRelation2 = accountRelationsSet2.iterator().next();
//            String val1 = accountRelation1.getAccount().getId() + COMMA + accountRelation1.isContact() + COMMA + accountRelation1.isContactOf() + COMMA + accountRelation1.isBlocked() + COMMA
//                    + accountRelation1.isBlockedBy() + COMMA + accountRelation1.getScore() + COMMA + accountRelation1.getScoredAs() + COMMA + accountRelation1.getOtherAccountId();
//            String val2 = accountRelation2.getAccount().getId() + COMMA + accountRelation2.isContact() + COMMA + accountRelation2.isContactOf() + COMMA + accountRelation2.isBlocked() + COMMA
//                    + accountRelation2.isBlockedBy() + COMMA + accountRelation2.getScore() + COMMA + accountRelation2.getScoredAs() + COMMA + accountRelation2.getOtherAccountId();
//            ;
//            WSAccountRelations accountRelations = new WSAccountRelations();
//            accountRelations.setAccountId(accountRelation1.getAccount().getId());
//            accountRelations.setContactId(accountRelation1.getOtherAccountId());
//            accountRelations.setIsContact(accountRelation1.isContact());
//            accountRelations.setIsContactOf(accountRelation1.isContactOf());
//            accountRelations.setIsBlocked(accountRelation1.isBlocked());
//            accountRelations.setIsBlockedBy(accountRelation1.isBlockedBy());
//            accountRelations.setScore(accountRelation1.getScore());
//            accountRelations.setScoredAs(accountRelation1.getScoredAs());
//
//            resultMap.put(key + UNDERSCORE + "1", accountRelations);
//            accountRelations = new WSAccountRelations();
//            accountRelations.setAccountId(accountRelation2.getAccount().getId());
//            accountRelations.setContactId(accountRelation2.getOtherAccountId());
//            accountRelations.setIsContact(accountRelation2.isContact());
//            accountRelations.setIsContactOf(accountRelation2.isContactOf());
//            accountRelations.setIsBlocked(accountRelation2.isBlocked());
//            accountRelations.setIsBlockedBy(accountRelation2.isBlockedBy());
//            accountRelations.setScore(accountRelation2.getScore());
//            accountRelations.setScoredAs(accountRelation2.getScoredAs());
//            resultMap.put(key + UNDERSCORE + "2", accountRelations);
//
//            val1 = key + UNDERSCORE + "1" + "=" + val1;
//            val2 = key + UNDERSCORE + "2" + "=" + val2;
//            mainResultList.add(val1);
//            mainResultList.add(val2);
//        }
//        else
//        {
//            resultMap.put(key, null);
//        }
//
//    }
//
//    public void verifyResults(String category, int scenario, int action)
//    {
//        for (int i = 1; i <= action; i++)
//        {
//            String key = category + UNDERSCORE + scenario + UNDERSCORE + i;
//            populateResultMap(key);
//        }
//    }
//
//    public void testSyncOnlyScenarios(int scenario) throws Exception
//    {
//        int actionCount = scenarioActions[scenario - 1];
//        switch (scenario)
//        {
//
//        case 1:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 2:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 3:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 4:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 5:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 6:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 7:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        case 8:
//            actionSyncOnly(scenario, actionCount);
//            verifyResults(CATEGORY_SYNC_ONLY, scenario, actionCount);
//            break;
//        }
//
//    }
//
//    private void actionSyncBlockUnblock(int scenario) throws Exception
//    {
//        preSetSyncBlockUnBlockScore(scenario, true, false, 0, 1);
//        accountRelationService.createOrUpdateAccountRelationBlock(account1.getId(), account2.getId());
//
//        preSetSyncBlockUnBlockScore(scenario, true, false, 0, 2);
//        accountRelationService.createOrUpdateAccountRelationBlock(account2.getId(), account1.getId());
//
//        preSetSyncBlockUnBlockScore(scenario, true, false, 0, 3);
//        try
//        {
//            accountRelationService.updateAccountRelationUnblock(account1.getId(), account2.getId());
//        }
//        catch (Exception ex)
//        {
//            Assert.assertEquals(scenario, 1);
//            System.out.println("exception 1..." + scenario);
//            // ex.printStackTrace();
//        }
//
//        preSetSyncBlockUnBlockScore(scenario, true, false, 0, 4);
//        try
//        {
//            accountRelationService.updateAccountRelationUnblock(account2.getId(), account1.getId());
//        }
//        catch (Exception ex)
//        {
//            Assert.assertTrue("excepted scenarios 1,6,7 actual scenario: " + scenario, (scenario == 1 || scenario == 6 || scenario == 7));
//            System.out.println("exception 2..." + scenario);
//        }
//    }
//
//    private void actionSyncScore(int scenario, int score, boolean isDefault1, boolean isDefault2) throws Exception
//    {
//        preSetSyncBlockUnBlockScore(scenario, false, true, score, 1);
//
//        accountRelationService.updateAccountRelationScore(account1.getId(), account2.getId(), score + 1);
//
//        preSetSyncBlockUnBlockScore(scenario, false, true, score, 2);
//
//        accountRelationService.updateAccountRelationScore(account2.getId(), account1.getId(), score + 1);
//
//    }
//
//    public void testSyncBlockUnBlockScenarios(int scenario) throws Exception
//    {
//        switch (scenario)
//        {
//        case 1:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 2:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 3:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 4:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 5:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 6:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 7:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        case 8:
//            actionSyncBlockUnblock(scenario);
//            verifyResults(CATEGORY_SYNC_BLOCK_UNBLOCK, scenario, 4);
//            break;
//        }
//
//    }
//
//    public void testSyncScoreScenarios(int scenario) throws Exception
//    {
//
//        switch (scenario)
//        {
//        case 1:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 2:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 3:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 4:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 5:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 6:
//            actionSyncScore(scenario, scenario, false, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 7:
//            actionSyncScore(scenario, scenario, true, false);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        case 8:
//            actionSyncScore(scenario, scenario, true, true);
//            verifyResults(CATEGORY_SYNC_SCORE, scenario, 2);
//            break;
//        }
//
//    }
//
//    @Test
//    public void testSyncBlockScore()
//    {
//        try
//        {
//            init();
//            for (int i = 1; i < scenarioActions.length; i++)
//            {
//                testSyncOnlyScenarios(i);
//                testSyncBlockUnBlockScenarios(i);
//                testSyncScoreScenarios(i);
//            }
//            testEverything(CATEGORY_SYNC_ONLY);// Test Sync contacts
//            testEverything(CATEGORY_SYNC_BLOCK_UNBLOCK);// Test Sync Block and
//                                                        // // Unblock contacts
//            testEverything(CATEGORY_SYNC_SCORE);// Test Sync and Score contacts
//        }
//        catch (Exception ex)
//        {
//            ex.printStackTrace();
//        }
//
//    }
//
//    public void testEverything(String category) throws Exception
//    {
//        Properties resultProps = new Properties();
//        InputStream testSyncContactsProps = getClass().getClassLoader().getResourceAsStream("testSyncContacts.properties");
//        resultProps.load(testSyncContactsProps);
//
//        for (int scenario = 1; scenario < scenarioActions.length; scenario++)
//        {
//            for (int action = 1; action < MAX_ACTION_COUNT_PER_SCENARIO; action++)
//            {
//                String key1 = category + UNDERSCORE + scenario + UNDERSCORE + action + UNDERSCORE + 1;
//                String key2 = category + UNDERSCORE + scenario + UNDERSCORE + action + UNDERSCORE + 2;
//                String val1 = (String) resultProps.get(key1);
//                String val2 = (String) resultProps.get(key2);
//
//                if (val1 == null || val2 == null)
//                    continue;
//
//                StringTokenizer st1 = new StringTokenizer(val1, COMMA);
//                StringTokenizer st2 = new StringTokenizer(val2, COMMA);
//
//                st1.nextToken();// account id 1
//                boolean isContactExpected1 = Boolean.valueOf(st1.nextToken());
//                boolean isContactOfExpected1 = Boolean.valueOf(st1.nextToken());
//                boolean isBlockedExpected1 = Boolean.valueOf(st1.nextToken());
//                boolean isBlockedByExpected1 = Boolean.valueOf(st1.nextToken());
//                int scoreExpected1 = Integer.parseInt(st1.nextToken());
//                int scoredAsExpected1 = Integer.parseInt(st1.nextToken());
//
//                st2.nextToken();// account id 2
//                boolean isContactExpected2 = Boolean.valueOf(st2.nextToken());
//                boolean isContactOfExpected2 = Boolean.valueOf(st2.nextToken());
//                boolean isBlockedExpected2 = Boolean.valueOf(st2.nextToken());
//                boolean isBlockedByExpected2 = Boolean.valueOf(st2.nextToken());
//                int scoreExpected2 = Integer.parseInt(st2.nextToken());
//                int scoredAsExpected2 = Integer.parseInt(st2.nextToken());
//
//                WSAccountRelations accountRelation1 = resultMap.get(key1);
//                WSAccountRelations accountRelation2 = resultMap.get(key2);
//
//                boolean isContactActual1 = accountRelation1.getIsContact();
//                boolean isContactOfActual1 = accountRelation1.getIsContactOf();
//                boolean isBlockedActual1 = accountRelation1.getIsBlocked();
//                boolean isBlockedByActual1 = accountRelation1.getIsBlockedBy();
//                int scoreActual1 = accountRelation1.getScore();
//                int scoredAsActual1 = accountRelation1.getScoredAs();
//
//                boolean isContactActual2 = accountRelation2.getIsContact();
//                boolean isContactOfActual2 = accountRelation2.getIsContactOf();
//                boolean isBlockedActual2 = accountRelation2.getIsBlocked();
//                boolean isBlockedByActual2 = accountRelation2.getIsBlockedBy();
//                int scoreActual2 = accountRelation2.getScore();
//                int scoredAsActual2 = accountRelation2.getScoredAs();
//
//                Assert.assertEquals("Failed test scenario: " + key1 + " for contacts, expected is " + isContactExpected1 + " actual is " + isContactActual1, isContactExpected1, isContactActual1);
//                Assert.assertEquals("Failed test scenario: " + key1 + " for contacts, expected is " + isContactOfExpected1 + " actual is " + isContactOfActual1, isContactOfExpected1,
//                        isContactOfActual1);
//
//                Assert.assertEquals("Failed test scenario: " + key1 + " for block, expected is " + isBlockedExpected1 + " actual is " + isBlockedActual1, isBlockedExpected1, isBlockedActual1);
//                Assert.assertEquals("Failed test scenario: " + key1 + " for block, expected is " + isBlockedByExpected1 + " actual is " + isBlockedByActual1, isBlockedByExpected1, isBlockedByActual1);
//
//                Assert.assertEquals("Failed test scenario: " + key1 + " for score, expected is " + scoreExpected1 + " actual is " + scoreActual1, scoreExpected1, scoreActual1);
//                Assert.assertEquals("Failed test scenario: " + key1 + " for score, expected is " + scoredAsExpected1 + " actual is " + scoredAsActual1, scoredAsExpected1, scoredAsActual1);
//
//                Assert.assertEquals("Failed test scenario: " + key2 + " for contacts, expected is " + isContactExpected2 + " actual is " + isContactActual2, isContactExpected2, isContactActual2);
//                Assert.assertEquals("Failed test scenario: " + key2 + " for contacts, expected is " + isContactOfExpected2 + " actual is " + isContactOfActual2, isContactOfExpected2,
//                        isContactOfActual2);
//
//                Assert.assertEquals("Failed test scenario: " + key2 + " for block, expected is " + isBlockedExpected2 + " actual is " + isBlockedActual2, isBlockedExpected2, isBlockedActual2);
//                Assert.assertEquals("Failed test scenario: " + key2 + " for block, expected is " + isBlockedByExpected2 + " actual is " + isBlockedByActual2, isBlockedByExpected2, isBlockedByActual2);
//
//                Assert.assertEquals("Failed test scenario: " + key2 + " for score, expected is " + scoreExpected2 + " actual is " + scoreActual2, scoreExpected2, scoreActual2);
//                Assert.assertEquals("Failed test scenario: " + key2 + " for score, expected is " + scoredAsExpected2 + " actual is " + scoredAsActual2, scoredAsExpected2, scoredAsActual2);
//
//            }
//        }
//    }
//
//    private void printResults()
//    {
//        for (int i = 0; i < mainResultList.size(); i++)
//        {
//            System.out.println(mainResultList.get(i));
//        }
//
//    }
//
//}
