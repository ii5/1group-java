import java.util.ArrayList;
import java.util.List;

import one.group.dao.AccountDAO;
import one.group.dao.ClientDAO;
import one.group.entities.jpa.Account;
import one.group.services.ChatMessageService;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class PusherClientTest
{

    private static int messageRcvdCount = 0;
    private static int channelSubscriptionCount = 0;
    private static int channelRequestedCount = 0;

    public static void main(String[] args) throws Exception
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-*.xml");

        ClientDAO clientDAO = context.getBean("clientDAO", ClientDAO.class);
        AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);

        ChatMessageService chatMessageService = context.getBean("chatMessageService", ChatMessageService.class);

        List<String> pusherChannels = new ArrayList<String>();

        for (Account account : accountDAO.getAllAccounts())
        {
            pusherChannels.addAll(clientDAO.fetchAllPushChannelsOfClientsOfAccount(account.getIdAsString()));
        }

        System.out.println(pusherChannels.size());

        for (String channel : pusherChannels)
        {
            PusherClientTest.listenToChannel(channel);
        }

        // chatMessageService.pushChatMessageToClient(pusherChannels, new
        // WSChatMessage());
        // chatMessageService.pushChatMessageToClient(pusherChannels, new
        // WSChatMessage());

        Thread.sleep(5000);
        System.out.println("========= " + messageRcvdCount);
    }

    public static void listenToChannel(final String channelId) throws Exception
    {
        Pusher pusher = new Pusher("b9b317bdb90f387e932a");

        pusher.connect(new ConnectionEventListener()
        {
            private final String channel = channelId;

            public void onError(String message, String code, Exception e)
            {
                System.err.println("EXCEPTION:" + channel + ":" + message + ":" + code);
                e.printStackTrace();
            }

            public void onConnectionStateChange(ConnectionStateChange change)
            {
                if (change.getCurrentState().equals(ConnectionState.CONNECTED))
                {
                    channelRequestedCount++;
                }

                System.err.println("STATE CHANGE:" + channel + ":" + "R=" + channelRequestedCount + ":S=" + channelSubscriptionCount + ":" + change.getPreviousState() + " > "
                        + change.getCurrentState());
            }
        });

        Channel channel = pusher.subscribe(channelId, new ChannelEventListener()
        {

            public void onEvent(String channelName, String eventName, String data)
            {
                System.out.println("CHANN:MSG:" + channelName + ":" + eventName + ":" + data);
            }

            public void onSubscriptionSucceeded(String channelName)
            {
                channelSubscriptionCount++;
                System.out.println("CHANN:" + channelName + ":SUBSCRIBED" + ":" + channelSubscriptionCount);
            }
        });

        channel.bind("", new SubscriptionEventListener()
        {
            public void onEvent(String channelName, String eventName, String data)
            {
                messageRcvdCount++;
                System.out.println("EVENT:MSG:" + channelName + ":" + eventName + ":" + data + ":" + messageRcvdCount);
            }
        });

        Thread.sleep(1200);
    }
}
