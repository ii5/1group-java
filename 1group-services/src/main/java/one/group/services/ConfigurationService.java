package one.group.services;

import one.group.entities.jpa.Configuration;


public interface ConfigurationService
{

    public Configuration getValueByKey(String key);

    public Configuration saveCOnfiguration(Configuration config);

    public void remove(Configuration config);

}
