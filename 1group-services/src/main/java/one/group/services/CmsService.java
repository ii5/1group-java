/**
 * 
 */
package one.group.services;

import one.group.exceptions.movein.General1GroupException;
import one.group.services.helpers.CmsServiceObject;

/**
 * @author ashishthorat
 *
 */
public interface CmsService
{
    public String UploadFile(CmsServiceObject config) throws General1GroupException;

    public String getFile(CmsServiceObject config) throws General1GroupException;
}
