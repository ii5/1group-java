package one.group.services;


public interface CustomOAuthorizationService
{
    public void saveOAuthAuthorization(String clientId, String resourceId, String clientSecret, String scope, String authorizedGrantTypes,  String authorities,
            int accessTokenValidity,String additionalInformation, String autoApprove);

    public boolean isClientValid(String clientId);
}
