package one.group.services;

import one.group.services.helpers.PushServiceObject;

/**
 * Interface <code>PushService</code> pushes a message to a destination after
 * processing a request.
 * 
 * @author nyalfernandes
 * 
 */
public interface PushService
{
    /**
     * Pushes a message to a destination after processing a request. Once a
     * message has been pushed there is no confirmation acted on by the
     * application. All further updates must be requested independently by the
     * client.
     * 
     */
    public void forwardRequest(PushServiceObject config);

}
