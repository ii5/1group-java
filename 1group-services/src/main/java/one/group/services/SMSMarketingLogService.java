package one.group.services;

import java.util.List;

import one.group.entities.jpa.SmsMarketingLog;

/**
 * 
 * @author miteshchavda
 *
 */
public interface SMSMarketingLogService
{
    public void saveSMSMarketingLog(String phoneNumber, String message, String campaign);

    public List<SmsMarketingLog> fetchAllLog();
}
