package one.group.services.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import one.group.core.enums.EntityType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSClient;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.ApplicationLogger;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SyncLogProducer;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ApplicationLoggerImpl implements ApplicationLogger
{
    private static final Logger logger = Logger.getLogger(ApplicationLoggerImpl.class);
    private SyncLogProducer kafkaProducer;
    
    private KafkaConfiguration kafkaConfiguration;
    
    public void log(WSAccount account, WSClient client, EntityType associatedEntityType, String associatedEntityId, String request, Date requestTime, String response, Date responseTime, String url, HTTPRequestMethodType requestType, Map<String, String> additionalData) throws SyncLogProcessException
    {
        SyncLogEntry syncLog = new SyncLogEntry();
        String key = null;
        
        syncLog.setAction(SyncActionType.ADD);
        syncLog.setType(SyncDataType.LOG);
        syncLog.setOriginatingAccount(account);
        syncLog.setOriginatingClient(client);
        syncLog.setAssociatedEntityId(associatedEntityId);
        syncLog.setAssociatedEntityType(associatedEntityType);

        syncLog.getApiDetails().setRequestFrom(request);
        syncLog.getApiDetails().setRequestTime(requestTime);
        syncLog.getApiDetails().setResponseFrom(response);
        syncLog.getApiDetails().setResponseTime(responseTime);
        syncLog.getApiDetails().setUrl(url);
        syncLog.getApiDetails().setType(requestType);
        syncLog.getApiDetails().setAdditionalRequestData(additionalData);
        
        if (syncLog.getOriginatingAccount() != null)
        {
            key = syncLog.getOriginatingAccount().getId();
        }
        
        logger.debug(key + ":" + kafkaConfiguration.getTopic(TopicType.LOG));
        kafkaProducer.write(syncLog, kafkaConfiguration.getTopic(TopicType.LOG).getName(), key);
        
    }
    
    public SyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }
    
    public void setKafkaProducer(SyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }
    
    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }
    
    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }
}
