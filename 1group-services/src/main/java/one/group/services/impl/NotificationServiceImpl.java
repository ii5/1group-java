package one.group.services.impl;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.status.NotificationStatus;
import one.group.dao.NotificationDAO;
import one.group.entities.api.response.v2.WSReferralTemplates;
import one.group.entities.jpa.NotificationTemplate;
import one.group.services.NotificationService;

public class NotificationServiceImpl implements NotificationService
{
    private NotificationDAO notificationDAO;

    public NotificationDAO getNotificationDAO()
    {
        return notificationDAO;
    }

    public void setNotificationDAO(NotificationDAO notificationDAO)
    {
        this.notificationDAO = notificationDAO;
    }

    public long fetchCountByStatus(String userId, NotificationStatus status)
    {
        return notificationDAO.fetchCountByStatus(userId, status);
    }

    public List<WSReferralTemplates> fetchAllNotificationTemplates()
    {
        List<WSReferralTemplates> wsReferralTemplates = new ArrayList<WSReferralTemplates>();
        List<NotificationTemplate> templates = notificationDAO.fetchNotificationTemplates();
        for (NotificationTemplate template : templates)
        {
            WSReferralTemplates wsReferralTemplate = new WSReferralTemplates();
            wsReferralTemplate.setId(template.getId());
            wsReferralTemplate.setTemplate(template.getTemplate());
            wsReferralTemplates.add(wsReferralTemplate);
        }

        return wsReferralTemplates;
    }

}
