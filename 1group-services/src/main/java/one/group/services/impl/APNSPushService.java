package one.group.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import one.group.core.enums.PushServerType;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.socket.SocketEntity;
import one.group.services.helpers.AbstractPushService;
import one.group.services.helpers.PushServiceObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class APNSPushService extends AbstractPushService
{

    private static final Logger logger = LoggerFactory.getLogger(APNSPushService.class);
    private String apnsCertificateLocation;
    private String apnsCertificatePassword;
    private int apnsMaxConnection;
    private String apnsMaxTimeOut;
    private String apnsContentType;
    private boolean handle;
    private boolean prodMode;

    public boolean isHandle()
    {
        return handle;
    }

    public void setHandle(boolean handle)
    {
        this.handle = handle;
    }

    public void setApnsCertificateLocation(String apnsCertificateLocation)
    {
        this.apnsCertificateLocation = apnsCertificateLocation;
    }

    public void setApnsCertificatePassword(String apnsCertificatePassword)
    {
        this.apnsCertificatePassword = apnsCertificatePassword;
    }

    public void setApnsMaxConnection(int apnsMaxConnection)
    {
        this.apnsMaxConnection = apnsMaxConnection;
    }

    public void setApnsMaxTimeOut(String apnsMaxTimeOut)
    {
        this.apnsMaxTimeOut = apnsMaxTimeOut;
    }

    public void setApnsContentType(String apnsContentType)
    {
        this.apnsContentType = apnsContentType;
    }

    public boolean isProdMode()
    {
        return prodMode;
    }

    public void setProdMode(boolean prodMode)
    {
        this.prodMode = prodMode;
    }

    @Override
    protected boolean additionalConfigsPresent(PushServiceObject config)
    {
        return true;
    }

    @Override
    protected void initiateRequest(PushServiceObject config)
    {
        try
        {
            if (config.getServiceList().contains(PushServerType.APNS) && isHandle())
            {
                final List<String> deviceIds = config.getApnsDeviceList();
                final SocketEntity se = (SocketEntity) config.getData();
                final WSSyncResult synResult = (WSSyncResult) se.getResponse().getData();
                final WSSyncUpdate data = (WSSyncUpdate) synResult.getUpdates().get(0);

                // final String dataToBeSent = Utils.getJsonString(data);
                logger.info(PushServerType.APNS + " pushing [" + config.getData() + "] to " + config.getApnsDeviceList() + ".");
                final ApnsService service = getApnsService(isProdMode());
                service.start();
                try
                {
                    // String payload =
                    // APNS.newPayload().sound("default").customField("chat",
                    // data).instantDeliveryOrSilentNotification().build();
                    String alertMessage = "";
                    if (data.getMessage() != null)
                    {
                        alertMessage = data.getMessage().getText();
                    }

                    String payload = APNS.newPayload().alertBody(alertMessage).customField("chat", data).build();
                    logger.info("Posting [" + payload + "] to APNS.");
                    service.push(deviceIds, payload);
                    // deleteInactiveDevices(service);
                }
                catch (Exception e)
                {
                    logger.error("Error generated for APNS push method" + config, e);
                }
                finally
                {
                    if (service != null)
                    {
                        service.stop();
                    }
                }
            }

            if (getSuccessor() != null)
            {
                logger.debug("Passing request to successor: " + getSuccessor().getClass().getSimpleName());
                getSuccessor().forwardRequest(config);
            }
        }
        catch (Exception e)
        {
            logger.error("Error while pushing data to APNS. " + config, e);
        }

    }

    public ApnsService getApnsService(boolean production)
    {
        ApnsServiceBuilder builder = null;
        if (production)
        {
            // builder = APNS.newService().withCert(apnsCertificateLocation,
            // apnsCertificatePassword).asPool(apnsMaxConnection).asQueued().withProductionDestination();
            builder = APNS.newService().withCert(apnsCertificateLocation, apnsCertificatePassword).withProductionDestination();
        }
        else
        {
            builder = APNS.newService().withCert(apnsCertificateLocation, apnsCertificatePassword).withSandboxDestination();
        }
        return builder.build();
    }

    // TODO:
    private void deleteInactiveDevices(ApnsService service)
    {
        // get the list of the devices that no longer have your app installed
        // from apple
        Map<String, Date> inactiveDevices = service.getInactiveDevices();
        service.testConnection();
        for (String deviceToken : inactiveDevices.keySet())
        {
            // mark device null in client table
            // TODO:
            // userDao.deleteByDeviceId(deviceToken);
        }

    }

}
