package one.group.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import one.group.core.enums.BookMarkType;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.EntityType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.AccountDAO;
import one.group.dao.AccountRelationDAO;
import one.group.dao.AmenityDAO;
import one.group.dao.BookMarkDAO;
import one.group.dao.LocationDAO;
import one.group.dao.LocationMatchDAO;
import one.group.dao.NearByLocalityDAO;
import one.group.dao.PropertyListingDAO;
import one.group.dao.PropertySearchRelationDAO;
import one.group.dao.SearchDAO;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSMatchingPropertyListing;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.api.response.v2.WSBroadcastSearchQuery;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.BookMark;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.entities.jpa.Search;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.SearchExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.LocationService;
import one.group.services.PropertyListingService;
import one.group.services.SearchService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.commons.lang.StringUtils;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchServiceImpl implements SearchService
{
    private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
    private SearchDAO searchDAO;
    private AccountDAO accountDAO;
    private PropertyListingDAO propertyListingDAO;
    private NearByLocalityDAO nearByLocalityDAO;
    private LocationDAO locationDAO;
    private AmenityDAO amenityDAO;
    private LocationMatchDAO locationMatchDAO;
    private AccountRelationDAO accountRelationDAO;
    private LocationService locationService;
    private PropertyListingService propertyListingService;
    private SimpleSyncLogProducer kafkaProducer;
    private KafkaConfiguration kafkaConfiguration;
    private PropertySearchRelationDAO propertySearchRelationDAO;

    private BookMarkDAO bookMarkDAO;
    private AccountRelationService accountRelationService;

    public PropertySearchRelationDAO getPropertySearchRelationDAO()
    {
        return propertySearchRelationDAO;
    }

    public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO)
    {
        this.propertySearchRelationDAO = propertySearchRelationDAO;
    }

    public SearchDAO getSearchDAO()
    {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO)
    {
        this.searchDAO = searchDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public PropertyListingDAO getPropertyListingDAO()
    {
        return propertyListingDAO;
    }

    public void setPropertyListingDAO(PropertyListingDAO propertyListingDAO)
    {
        this.propertyListingDAO = propertyListingDAO;
    }

    public NearByLocalityDAO getNearByLocalityDAO()
    {
        return nearByLocalityDAO;
    }

    public void setNearByLocalityDAO(NearByLocalityDAO nearByLocalityDAO)
    {
        this.nearByLocalityDAO = nearByLocalityDAO;
    }

    public AmenityDAO getAmenityDAO()
    {
        return amenityDAO;
    }

    public void setAmenityDAO(AmenityDAO amenityDAO)
    {
        this.amenityDAO = amenityDAO;
    }

    public LocationMatchDAO getLocationMatchDAO()
    {
        return locationMatchDAO;
    }

    public void setLocationMatchDAO(LocationMatchDAO locationMatchDAO)
    {
        this.locationMatchDAO = locationMatchDAO;
    }

    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public AccountRelationDAO getAccountRelationDAO()
    {
        return accountRelationDAO;
    }

    public void setAccountRelationDAO(AccountRelationDAO accountRelationDAO)
    {
        this.accountRelationDAO = accountRelationDAO;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public BookMarkDAO getBookMarkDAO()
    {
        return bookMarkDAO;
    }

    public void setBookMarkDAO(BookMarkDAO bookMarkDAO)
    {
        this.bookMarkDAO = bookMarkDAO;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-saveSearch")
    public WSBroadcastSearchQueryRequest saveSearch(String searchId, String accountId, WSRequest request) throws General1GroupException, SyncLogProcessException

    {
        Account account = accountDAO.fetchAccountById(accountId);
        if (account == null)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }

        WSBroadcastSearchQueryRequest wsSearchQueryRequest = request.getSearch();

        Search search = new Search();
        if (searchId != null)
        {
            search = searchDAO.fetchSearchBySearchId(searchId);
        }

        search.setCreatedBy(accountId);
        search.setCityId(wsSearchQueryRequest.getCityId());
        search.setLocationId(String.valueOf(wsSearchQueryRequest.getLocationId()));
        search.setMinPrice(!Utils.isNull(wsSearchQueryRequest.getMinPrice()) ? Long.valueOf(wsSearchQueryRequest.getMinPrice()) : 0);
        search.setMaxPrice(!Utils.isNull(wsSearchQueryRequest.getMaxPrice()) ? Long.valueOf(wsSearchQueryRequest.getMaxPrice()) : Integer.MAX_VALUE);

        search.setMinArea(!Utils.isNull(wsSearchQueryRequest.getMinArea()) ? Integer.valueOf(wsSearchQueryRequest.getMinArea()) : 0);
        search.setMaxArea(!Utils.isNull(wsSearchQueryRequest.getMaxArea()) ? Integer.valueOf(wsSearchQueryRequest.getMaxArea()) : Integer.MAX_VALUE);

        String propertyType = wsSearchQueryRequest.getPropertyType();
        PropertyType propertyTypeValue = propertyType != null ? PropertyType.convert(propertyType) : null;
        search.setPropertyType(propertyTypeValue);

        String transactionType = wsSearchQueryRequest.getTransactionType();
        PropertyTransactionType transactionTypeValue = transactionType != null ? PropertyTransactionType.convert(transactionType) : null;
        search.setTransactionType(transactionTypeValue);

        search.setRooms(StringUtils.join(wsSearchQueryRequest.getRooms(), ","));

        search.setSearchTime(new Date());

        searchDAO.saveSearch(search);

        wsSearchQueryRequest.setAccountId(accountId);
        wsSearchQueryRequest.setSearchId(search.getId());
        wsSearchQueryRequest.setSearchTime(String.valueOf(search.getSearchTime().getTime()));

        return wsSearchQueryRequest;
    }

    @Profiled(tag = "SearchService-checkUniqueClientSearch")
    public boolean checkUniqueClientSearch(String clientName, String accountId)
    {
        Search search = searchDAO.fetchSearchByRequirementName(clientName, accountId);

        if (search == null)
        {
            return false;
        }
        return true;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-checkSearchOwnership")
    public boolean checkSearchOwnership(String searchId, String accountId)
    {
        Search search = searchDAO.fetchSearchBySearchId(searchId);
        if (search != null && search.getCreatedBy().equals(accountId))
        {
            return true;
        }
        return false;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-updateSearchTime")
    public WSBroadcastSearchQueryRequest updateSearchTime(String searchId, String accountId) throws General1GroupException, AccountNotFoundException
    {
        Search search = searchDAO.fetchSearchBySearchId(searchId);
        if (search == null)
        {
            throw new General1GroupException(SearchExceptionCode.SEARCH_NOT_EXIST, true, searchId);
        }
        else
        {
            if (!search.getCreatedBy().equals(accountId))
            {
                throw new AccountNotFoundException(AccountExceptionCode.FORBIDDEN, true, accountId);
            }

            Date searchTime = new Date();
            search.setSearchTime(searchTime);
            searchDAO.saveSearch(search);

            WSBroadcastSearchQueryRequest wsSearchQueryRequest = new WSBroadcastSearchQueryRequest(search);

            return wsSearchQueryRequest;
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-searchPropertyListingBySearchQuery")
    public Set<WSMatchingPropertyListing> searchPropertyListingBySearchQuery(WSBroadcastSearchQueryRequest wsSearchQueryRequest, String accountId, String propertyDetails, String accountDetails,
            String locationDetails, String matchingDetails, String subscribe) throws Abstract1GroupException
    {
        // don't remove this( part of optimzation)
        // List<Location> locationList =
        // getNearByAndMatchLocations(String.valueOf(wsSearchQueryRequest.getLocationId()));
        // List<PropertyListingScoreObject> propertyListingList =
        // propertyListingDAO.searchPropertyListing(locationList,
        // PropertyStatus.ACTIVE);

        // Fetch property listings
        List<PropertyListingScoreObject> propertyListingList = propertyListingDAO.searchPropertyListingByExactAndNearBy(String.valueOf(wsSearchQueryRequest.getLocationId()), BroadcastStatus.ACTIVE);

        // Filter property listings & assign score
        Location ofLocation = locationDAO.fetchById("" + wsSearchQueryRequest.getLocationId());
        List<Location> exactMatchingLocationList = locationService.getMatchingLocation(ofLocation, true);

        Set<WSMatchingPropertyListing> matchedPropertyListings = null;// TODO
                                                                      // scoreService.filterAndScoreSearchResults(wsSearchQueryRequest,
                                                                      // propertyListingList,
                                                                      // exactMatchingLocationList);

        if (!Utils.isNull(propertyDetails) && propertyDetails.equals("true") && matchedPropertyListings.size() > 0)
        {
            Map<String, BookMark> propertyListingIdVsBookMarkMap = new HashMap<String, BookMark>();
            List<BookMark> bookmarkList = bookMarkDAO.getBookMarksByReference(accountId, BookMarkType.PROPERTY_LISTING);
            for (BookMark bookMark : bookmarkList)
            {
                String propertyListingId = bookMark.getTargetEntity();
                propertyListingIdVsBookMarkMap.put(propertyListingId, bookMark);
            }

            List<String> accountRelationAccountIdsList = new ArrayList<String>();
            List<String> propertyListingIdsList = new ArrayList<String>();
            for (WSMatchingPropertyListing propertyListing : matchedPropertyListings)
            {
                accountRelationAccountIdsList.add(propertyListing.getAccountId());

                propertyListingIdsList.add(propertyListing.getPropertyListingId());
            }

            Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();
            Map<String, WSAccount> propertyListingIdVsAccountMap = new HashMap<String, WSAccount>();
            if (!Utils.isNull(accountDetails) && accountDetails.equals("true"))
            {
                accountVsRelationsMap = accountRelationService.fetchRawAccountRelationsAsMap(accountId, accountRelationAccountIdsList);
                propertyListingIdVsAccountMap = propertyListingService.fetchAccountsAssocitedWithPropertyListingByIds(propertyListingIdsList);
            }

            List<PropertyListing> propertyListings = propertyListingService.fetchPropertyListingsByPropertyListingIds(propertyListingIdsList);

            Map<String, WSPropertyListing> wsPropertyListingMap = propertyListingService.getWSPropertyListingByPropertyListingIds(propertyListings, accountId, subscribe, accountDetails,
                    locationDetails, matchingDetails, propertyListingIdVsBookMarkMap, accountVsRelationsMap, propertyListingIdVsAccountMap);

            for (WSMatchingPropertyListing propertyListing : matchedPropertyListings)
            {
                propertyListing.setPropertyListingDetails(wsPropertyListingMap.get(propertyListing.getPropertyListingId()));
            }
        }
        return matchedPropertyListings;

    }

    @Profiled(tag = "SearchService-fetchPropertyAgainstSearchMapForAccount")
    public Map<PropertyListingScoreObject, List<Search>> fetchPropertyAgainstSearchMapForAccount(String accountId)
    {
        Map<PropertyListingScoreObject, List<Search>> propertyAgainstSearch = new HashMap<PropertyListingScoreObject, List<Search>>();

        List<PropertySearchRelation> propertySearchRelationList = propertySearchRelationDAO.getPropertySearchRelationsOfAccount(accountId);

        for (PropertySearchRelation relation : propertySearchRelationList)
        {
            PropertyListingScoreObject plScoreObject = new PropertyListingScoreObject(relation.getPropertyListing(), relation.getPropertyListing().getAccount(), relation.getPropertyListing()
                    .getLocation());
            if (propertyAgainstSearch.containsKey(plScoreObject))
            {
                propertyAgainstSearch.get(plScoreObject).add(relation.getSearchId());
            }
            else
            {
                List<Search> searchList = new ArrayList<Search>();
                searchList.add(relation.getSearchId());
                propertyAgainstSearch.put(plScoreObject, searchList);
            }
        }

        return propertyAgainstSearch;
    }

    @Profiled(tag = "SearchService-pushSearchToSyncLog")
    public void pushSearchToSyncLog(WSRequest request, SyncActionType actionType) throws SyncLogProcessException
    {
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(actionType);
        syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING_SEARCH);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, request);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-getSearch")
    public WSBroadcastSearchQueryRequest getSearch(String searchId, String accountId) throws General1GroupException, AccountNotFoundException
    {
        Validation.isTrue(!Utils.isNullOrEmpty(searchId), "Search id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null");

        Search search = searchDAO.fetchSearchBySearchId(searchId);
        if (search == null)
        {
            throw new General1GroupException(SearchExceptionCode.SEARCH_NOT_EXIST, true, searchId);
        }
        else
        {
            if (!search.getCreatedBy().equals(accountId))
            {
                throw new AccountNotFoundException(AccountExceptionCode.FORBIDDEN, true, accountId);
            }

            WSBroadcastSearchQueryRequest wsSearchQueryRequest = new WSBroadcastSearchQueryRequest(search);

            return wsSearchQueryRequest;
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public WSBroadcastSearchQuery getSearchOfAccount(String accountId) throws General1GroupException
    {
        Search search = null;
        try
        {
            search = searchDAO.fetchLatestSearchByAccountId(accountId);
        }
        catch (NoResultException nre)
        {
            throw new General1GroupException(SearchExceptionCode.SEARCH_NOT_EXIST, false, accountId);

        }

        WSBroadcastSearchQuery searchRsult = new WSBroadcastSearchQuery(search);
        return searchRsult;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SearchService-saveSearch")
    public WSBroadcastSearchQueryRequest updateSearchForAccount(String accountId, WSBroadcastSearchQueryRequest queryRequest) throws General1GroupException, SyncLogProcessException

    {
        Account account = accountDAO.fetchAccountById(accountId);
        if (account == null)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }

        Search search = null;

        try
        {
            search = searchDAO.fetchLatestSearchByAccountId(accountId);
        }
        catch (Exception e)
        {
            logger.info("Its ok. No default search found.");
        }

        if (search == null)
        {
            search = new Search();
        }
        search.setCreatedBy(accountId);
        search.setCityId(queryRequest.getCityId());
        if (queryRequest.getLocationId() != null)
        {
            search.setLocationId(String.valueOf(queryRequest.getLocationId()));
        }
        else
        {
            search.setLocationId(null);
        }

        search.setMinPrice(!Utils.isNull(queryRequest.getMinPrice()) ? Long.valueOf(queryRequest.getMinPrice()) : 0);
        search.setMaxPrice(!Utils.isNull(queryRequest.getMaxPrice()) ? Long.valueOf(queryRequest.getMaxPrice()) : Integer.MAX_VALUE);

        search.setMinArea(!Utils.isNull(queryRequest.getMinArea()) ? Integer.valueOf(queryRequest.getMinArea()) : 0);
        search.setMaxArea(!Utils.isNull(queryRequest.getMaxArea()) ? Integer.valueOf(queryRequest.getMaxArea()) : Integer.MAX_VALUE);
        if (queryRequest.getPropertyType() != null)
        {
            search.setPropertyType(PropertyType.convert(queryRequest.getPropertyType()));
        }
        else
        {
            search.setPropertyType(null);
        }

        if (queryRequest.getTransactionType() != null)
        {
            search.setTransactionType(PropertyTransactionType.convert(queryRequest.getTransactionType()));
        }
        else
        {
            search.setTransactionType(null);
        }

        if (queryRequest.getRooms() != null)
        {
            List<String> roomList = queryRequest.getRooms();
            search.setRooms(StringUtils.join(roomList, ","));
        }
        else
        {
            search.setRooms(null);
        }

        if (queryRequest.getPropertySubType() != null)
        {
            search.setPropertySubType(PropertySubType.convert(queryRequest.getPropertySubType()));
        }
        else
        {
            search.setPropertySubType(null);
        }

        search.setBroadcastType(null);
        if (queryRequest.getBroadcastType() != null)
        {
            search.setBroadcastType(BroadcastType.convert(queryRequest.getBroadcastType()));
        }
        search.setSearchTime(new Date());

        searchDAO.saveSearch(search);

        queryRequest.setAccountId(accountId);
        queryRequest.setSearchId(search.getId());
        queryRequest.setSearchTime(String.valueOf(search.getSearchTime().getTime()));

        return queryRequest;
    }
}
