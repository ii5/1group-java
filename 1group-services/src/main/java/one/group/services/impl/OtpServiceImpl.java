package one.group.services.impl;

import javax.transaction.Transactional;

import one.group.dao.ClientDAO;
import one.group.dao.OtpDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.jpa.Otp;
import one.group.entities.jpa.PhoneNumber;
import one.group.services.OtpService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OtpServiceImpl implements OtpService
{
	
	private static final Logger logger = LoggerFactory.getLogger(OtpServiceImpl.class);
	
    private OtpDAO otpDAO;

    private ClientDAO clientDAO;

    private PhoneNumberDAO phoneNumberDAO;

    private long otpExpiryTime;

    public long getOtpExpiryTime()
    {
        return otpExpiryTime;
    }

    public void setOtpExpiryTime(long otpExpiryTime)
    {
        this.otpExpiryTime = otpExpiryTime;
    }

    public OtpDAO getOtpDAO()
    {
        return otpDAO;
    }

    public void setOtpDAO(OtpDAO otpDAO)
    {
        this.otpDAO = otpDAO;
    }

    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }

    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }

    public ClientDAO getClientDAO()
    {
        return clientDAO;
    }

    public void setClientDAO(ClientDAO clientDAO)
    {
        this.clientDAO = clientDAO;
    }

    @Transactional
    @Profiled(tag = "OtpService-saveOTP")
    public void saveOTP(String otp, String mobileNumber)
    {
        PhoneNumber phoneNumber = phoneNumberDAO.fetchPhoneNumber(mobileNumber);
        logger.info("PhoneNumber : " + phoneNumber);
        logger.info(otp + " - " + mobileNumber);

        Otp otpO = new Otp();
        otpO.setOtp(otp);
        otpO.setPhoneNumber(phoneNumber);
        otpDAO.saveOTP(otpO);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "OtpService-deleteOTP")
    public void deleteOTP(String otp, String mobileNumber)
    {
        Otp otpObj = otpDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);
        otpDAO.deleteOtp(otpObj);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "OtpService-checkAndSaveOtp")
    public boolean checkAndSaveOtp(String otp, String mobileNumber)
    {
        Otp otpObj = otpDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);
        if (otpObj == null)
        {
            saveOTP(otp, mobileNumber);
        }

        return otpObj != null;
    }

}
