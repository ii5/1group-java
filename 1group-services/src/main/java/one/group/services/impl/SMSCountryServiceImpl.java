package one.group.services.impl;

import static one.group.core.Constant.UTF8_ENCODING;

import java.io.IOException;
import java.net.URLEncoder;

import one.group.services.SMSService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SMSServiceImpl.
 * 
 * @author shweta.sankhe
 */
public class SMSCountryServiceImpl implements SMSService
{

    private static final Logger logger = LoggerFactory.getLogger(SMSCountryServiceImpl.class);

    private static final String SMS_OTP_MESSAGE = "sms.otp.message";

    /** The mtype. */
    private static String mtype = "N";

    /** The dr. */
    private static String DR = "Y";

    /** The user. */
    private String user;

    /** The password. */
    private String password;

    /** The sender id. */
    private String senderId;

    private MessageResouceService messageResouceService;

    private SMSCountryPushService smsPushService;

    /**
     * Sets the user. * @param user the new user
     */
    public void setUser(String user)
    {
        this.user = user;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the new password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Sets the sender id.
     *
     * @param senderId
     *            the new sender id
     */
    public void setSenderId(String senderId)
    {
        this.senderId = senderId;
    }

    public SMSCountryPushService getSmsPushService()
    {
        return smsPushService;
    }

    public void setSmsPushService(SMSCountryPushService smsPushService)
    {
        this.smsPushService = smsPushService;
    }

    @Profiled(tag = "SMSService-sendSingleSMS")
    public void sendSingleSMSPooled(final String mobileNumber, final String otp) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");
        String message = messageResouceService.getMessage(SMS_OTP_MESSAGE, otp);
        final String postData = "User=" + URLEncoder.encode(user, UTF8_ENCODING) + "&passwd=" + password + "&mobilenumber=" + newMobileNumber + "&message=" + URLEncoder.encode(message, UTF8_ENCODING)
                + "&sid=" + senderId + "&mtype=" + mtype + "&DR=" + DR;

        logger.info("Sending OTP message[" + message + "] . to Mobile Number" + mobileNumber);

        smsPushService.initiateRequest(postData);

    }

    public void setMessageResouceService(MessageResouceService messageResouceService)
    {
        this.messageResouceService = messageResouceService;
    }

    public void sendSingleSMS(String mobileNumber, String otp) throws IOException
    {
        // TODO Auto-generated method stub

    }

    public void sendSMS(String mobileNumber, String smsContent) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");
        final String postData = "User=" + URLEncoder.encode(user, UTF8_ENCODING) + "&passwd=" + password + "&mobilenumber=" + newMobileNumber + "&message="
                + URLEncoder.encode(smsContent, UTF8_ENCODING) + "&sid=" + senderId + "&mtype=" + mtype + "&DR=" + DR;

        logger.info("sendSMS[" + smsContent + "] . to Mobile Number" + mobileNumber);

        smsPushService.initiateRequest(postData);

    }

}
