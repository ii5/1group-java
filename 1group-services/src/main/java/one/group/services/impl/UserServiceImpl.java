package one.group.services.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import one.group.core.enums.AccountType;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.core.enums.status.Status;
import one.group.dao.CityDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.dao.UserDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.City;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.User;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.services.UserService;
import one.group.utils.Utils;

public class UserServiceImpl implements UserService
{
    private UserDAO userDao;
    private CityDAO cityDAO;
    private PhoneNumberDAO phoneNumberDAO;
    
    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }
    
    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }
    
    public CityDAO getCityDAO()
    {
        return cityDAO;
    }
    
    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public UserDAO getUserDao()
    {
        return userDao;
    }

    public void setUserDao(UserDAO userDao)
    {
        this.userDao = userDao;
    }

    @Transactional(rollbackOn={Exception.class})
    public User saveUser(String userId, String fullName, String createdBy, String shortReferece, String email, String cityId, AccountType accountType, UserSource userSource, GroupSelectionStatus groupSelectionStatus, SyncStatus syncStatus, String mobileNumber, String localStorage, boolean skipIfExist) throws Abstract1GroupException
    {
        City city = null;
        User user = null;
        PhoneNumber number = null;
        
        if (userId == null && mobileNumber == null)
        {
            user = new User();
        }
        else if (userId != null)
        {
            user = userDao.fetchUserByUserId(userId);
            if (user == null)
            {
                throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, userId);
            }
            
            if (skipIfExist)
            {
            	return user;
            }
            
            String waPhoneNumber = user.getWaPhoneNumber();
            if(waPhoneNumber != null && mobileNumber != null)
            {
                if (!waPhoneNumber.equals(mobileNumber))
                {
                    throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_WITH_SAME_WA_NUMBER, false, mobileNumber);
                }
            }
        }
        else if (mobileNumber != null)
        {
            Account a = userDao.fetchUserByNumber(mobileNumber);
            number = phoneNumberDAO.fetchPhoneNumber(mobileNumber);
            
            if (number == null)
            {
                number = new PhoneNumber();
                number.setNumber(mobileNumber);
                number.setType(PhoneNumberType.MOBILE);
                number.setSource(PhoneNumberSource.WHATSAPP);
                phoneNumberDAO.savePhonenumber(number);
            }
            
            if (a == null)
            {
                a = new Account();
                a.setPrimaryPhoneNumber(number);
                a.setStatus(Status.SEEDED);
            }
            else
            {
            	if (skipIfExist)
                {
                	return a;
                }
            }
            
            user = a;
        }
        
        if (cityId != null)
        {
            city = cityDAO.getCityById(cityId);
        }
        
        user.setWaPhoneNumber(Utils.<String>returnArg1OnNull(user.getWaPhoneNumber(), mobileNumber));
        user.setFullName(Utils.<String>returnArg1OnNull(user.getFullName(), fullName));
        user.setCreatedBy(Utils.<String>returnArg1OnNull(user.getCreatedBy(), createdBy));
//        user.setCreatedTime(Utils.<Date>returnArg1OnNull(user.getCreatedTime(), new Date()));
        user.setShortReference(Utils.<String>returnArg1OnNull(user.getShortReference(), shortReferece));
        user.setEmail(Utils.<String>returnArg1OnNull(user.getEmail(), email));
        user.setCity(Utils.<City>returnArg1OnNull(user.getCity(), city));
        user.setType(Utils.<AccountType>returnArg1OnNull(user.getType(), accountType));
        user.setSource(Utils.<UserSource>returnArg1OnNull(user.getSource(), userSource));
        user.setGroupSelectionStatus(Utils.<GroupSelectionStatus>returnArg1OnNull(user.getGroupSelectionStatus(), groupSelectionStatus));
        user.setSyncStatus(Utils.<SyncStatus>returnArg1OnNull(user.getSyncStatus(), syncStatus));
        user.setLocalStorage(Utils.<String>returnArg1OnNull(user.getLocalStorage(), localStorage));
        user.setWaPhoneNumber(Utils.<String>returnArg1OnNull(user.getWaPhoneNumber(), mobileNumber));
        
        user = userDao.saveUser(user);
//        System.out.println(user);
        
        return user;
    }

    public List<String> saveAllForSync(List<String> mobileNumbers)
    {
        for (String mobileNumber : mobileNumbers)
        {
            
        }
        
        return mobileNumbers;
    }

}
