package one.group.services.impl;

import static one.group.core.Constant.UTF8_ENCODING;

import java.io.IOException;
import java.net.URLEncoder;

import one.group.services.SMSService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SMSServiceImpl.
 * 
 * @author shweta.sankhe
 */
public class SMSNetcoreServiceImpl implements SMSService
{

    private static final Logger logger = LoggerFactory.getLogger(SMSNetcoreServiceImpl.class);

    private static final String SMS_OTP_MESSAGE = "sms.otp.message";

    /** The user. */
    private String user;

    /** The password. */
    private String password;

    /** The sender id. */
    private String senderId;

    private String feedId;

    private MessageResouceService messageResouceService;

    private SMSNetcorePushService smsPushService;

    /**
     * Sets the user. * @param user the new user
     */
    public void setUser(String user)
    {
        this.user = user;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the new password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Sets the sender id.
     *
     * @param senderId
     *            the new sender id
     */
    public void setSenderId(String senderId)
    {
        this.senderId = senderId;
    }

    public SMSNetcorePushService getSmsPushService()
    {
        return smsPushService;
    }

    public void setSmsPushService(SMSNetcorePushService smsPushService)
    {
        this.smsPushService = smsPushService;
    }

    public String getFeedId()
    {
        return feedId;
    }

    public void setFeedId(String feedId)
    {
        this.feedId = feedId;
    }

    @Profiled(tag = "SMSService-sendSingleSMS")
    public void sendSingleSMSPooled(final String mobileNumber, final String otp) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");
        String message = messageResouceService.getMessage(SMS_OTP_MESSAGE, otp);
        final String postData = "feedid=" + feedId + "&username=" + user + "&password=" + password + "&To=" + newMobileNumber + "&Text=" + URLEncoder.encode(message, UTF8_ENCODING);

        logger.info("Sending OTP message[" + message + "] . to Mobile Number" + mobileNumber);

        smsPushService.initiateRequest(postData);

    }

    public void setMessageResouceService(MessageResouceService messageResouceService)
    {
        this.messageResouceService = messageResouceService;
    }

    public void sendSingleSMS(String mobileNumber, String otp) throws IOException
    {
        // TODO Auto-generated method stub

    }

    public void sendSMS(String mobileNumber, String smsContent) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");
        final String postData = "feedid=" + feedId + "&username=" + user + "&password=" + password + "&To=" + newMobileNumber + "&Text=" + URLEncoder.encode(smsContent, UTF8_ENCODING);

        logger.info("sendSMS[" + smsContent + "] . to Mobile Number [" + mobileNumber + "]");

        smsPushService.initiateRequest(postData);

    }

}
