package one.group.services.impl;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

/**
 * The Class MessageResouceService.
 *
 * @author shweta.sankhe
 */
public class MessageResouceService implements MessageSourceAware
{
    
    /** The message source. */
    private MessageSource messageSource;

    public void setMessageSource(MessageSource messageSource)
    {
        this.messageSource = messageSource;
    }

    /**
     * Gets the message.
     *
     * @param propertyName the property name
     * @param args the args
     * @return the message
     */
    public String getMessage(final String propertyName, String... args)
    {
        return messageSource.getMessage(propertyName, args, Locale.getDefault());
    }

}