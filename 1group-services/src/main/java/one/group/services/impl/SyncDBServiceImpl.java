package one.group.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import one.group.core.enums.HTTPResponseType;
import one.group.dao.AccountDAO;
import one.group.dao.SyncUpdateDAO;
import one.group.dao.SyncUpdateReadCursorsDAO;
import one.group.dao.SyncUpdateWriteCursorsDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.SyncUpdate;
import one.group.entities.jpa.SyncUpdateReadCursors;
import one.group.entities.jpa.SyncUpdateWriteCursors;
import one.group.exceptions.codes.ClientExceptionCode;
import one.group.exceptions.codes.SyncExceptionCode;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.log4j.BasicConfigurator;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SyncDBServiceImpl implements SyncDBService
{
    private static final Logger logger = LoggerFactory.getLogger(SyncDBServiceImpl.class);
    private SyncUpdateDAO syncUpdateDAO;
    private SyncUpdateReadCursorsDAO syncLogReadCursorsDAO;
    private SyncUpdateWriteCursorsDAO syncLogWriteCursorsDAO;
    private AccountDAO accountDAO;
    private SubscriptionService subscriptionService;

    @Transactional
    @Profiled(tag = "SyncDBService-deleteClient")
    public void deleteClient(String clientId)
    {

        SyncUpdateReadCursors readCursor = syncLogReadCursorsDAO.fetchByClient(clientId);
        if (readCursor != null)
        {
            syncLogReadCursorsDAO.delete(readCursor);
        }

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-createClient")
    public int createClient1(String clientId) throws ClientProcessException
    {

        SyncUpdateReadCursors readCursor = syncLogReadCursorsDAO.fetchByClient(clientId);
        try
        {
            if (readCursor != null)
            {
                throw new ClientProcessException(ClientExceptionCode.CLIENT_ALREADY_EXISTS, true);
            }

            Account account = accountDAO.fetchAccountOfClient(clientId);
            if (account == null)
            {
                throw new ClientProcessException(HTTPResponseType.NOT_FOUND, ClientExceptionCode.CLIENT_NOT_FOUND.getExceptionName(), true,
                        "Weird! The account for the requesting client does not exist! Hmmm!");
            }

            SyncUpdateWriteCursors writeCursors = syncLogWriteCursorsDAO.fetchByAccount(account.getIdAsString());
            int maxAckUpdate = -1;
            if (writeCursors != null)
            {
                maxAckUpdate = writeCursors.getTotalUpdates();
            }
            readCursor = new SyncUpdateReadCursors(clientId, maxAckUpdate);
            syncLogReadCursorsDAO.saveSyncLogReadCursors(readCursor);
            return maxAckUpdate;

        }
        catch (ClientProcessException mie)
        {
            logger.error("", mie);
            throw mie;
        }

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-appendUpdate")
    public void appendUpdate1(String accountId, WSSyncUpdate syncUpdate)
    {
        Validation.notNull(syncUpdate, "Sync Update entity passed should not be null.");
        Validation.notNull(accountId, "Account Id passed should not be null.");

        SyncUpdateWriteCursors writeCursors = syncLogWriteCursorsDAO.fetchByAccount(accountId);
        if (writeCursors == null)
        {
            // We assume its a new account
            writeCursors = new SyncUpdateWriteCursors(accountId, -1);
        }
        int totalUpdates = writeCursors.getTotalUpdates();
        writeCursors.setTotalUpdates(++totalUpdates);
        syncLogWriteCursorsDAO.saveSyncLogWriteCursors(writeCursors);
        syncUpdate.setIndex(totalUpdates);

        // Every append is a new entry log
        SyncUpdate syncLog = new SyncUpdate(accountId, totalUpdates, Utils.getJsonString(syncUpdate));
        syncUpdateDAO.saveAccountUpdate(syncLog);

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-appendUpdates")
    public void appendUpdates(String accountId, List<ResponseEntity> syncUpdates)
    {
        Validation.notNull(syncUpdates, "Sync Update List passed should not be null.");
        Validation.notNull(accountId, "Account Id passed should not be null.");
        List<SyncUpdate> syncUpdateList = new ArrayList<SyncUpdate>();

        SyncUpdateWriteCursors writeCursors = syncLogWriteCursorsDAO.fetchByAccount(accountId);
        if (writeCursors == null)
        {
            // We assume its a new account
            writeCursors = new SyncUpdateWriteCursors(accountId, -1);
        }
        int totalUpdates = writeCursors.getTotalUpdates();
        writeCursors.setTotalUpdates(totalUpdates + syncUpdates.size());

        for (ResponseEntity responseEntity : syncUpdates)
        {
            int updateIndex = ++totalUpdates;
            WSSyncUpdate syncUpdate = (WSSyncUpdate) responseEntity;
            syncUpdate.setIndex(updateIndex);
            SyncUpdate syncLog = new SyncUpdate(accountId, updateIndex, Utils.getJsonString(syncUpdate));
            syncUpdateList.add(syncLog);
        }

        syncLogWriteCursorsDAO.saveSyncLogWriteCursors(writeCursors);
        syncUpdateDAO.saveAccountUpdates(syncUpdateList);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-advanceReadCursor")
    public int advanceReadCursor1(String accountId, String clientId, int requestedCursorIndex) throws SyncLogProcessException
    {

        SyncUpdateReadCursors readCursors = syncLogReadCursorsDAO.fetchByClient(clientId);
        SyncUpdateWriteCursors writeCursors = syncLogWriteCursorsDAO.fetchByAccount(accountId);
        if (readCursors == null || writeCursors == null)
        {
            throw new SyncLogProcessException(SyncExceptionCode.NO_DATA_YET, false);
        }

        int maxAckedUpdateOfClient = readCursors.getMaxAckedUpdate();
        int totalUpdatesToAccount = writeCursors.getTotalUpdates();

        maxAckedUpdateOfClient = Math.min(Math.max(maxAckedUpdateOfClient, requestedCursorIndex), totalUpdatesToAccount);
        readCursors.setMaxAckedUpdate(maxAckedUpdateOfClient);

        // Hit the database only if there is any difference
        if (maxAckedUpdateOfClient != requestedCursorIndex)
        {
            syncLogReadCursorsDAO.saveSyncLogReadCursors(readCursors);
        }

        return maxAckedUpdateOfClient;

    }

    @Profiled(tag = "SyncDBService-readUpdates")
    public WSSyncResult readUpdates(String accountId, String clientId, int maxResults) throws SyncLogProcessException
    {
        SyncUpdateReadCursors readCursors = syncLogReadCursorsDAO.fetchByClient(clientId);
        if (readCursors == null)
        {
            throw new SyncLogProcessException(SyncExceptionCode.NO_DATA_YET, false);
        }

        int maxAckedUpdateOfClient = readCursors.getMaxAckedUpdate();
        WSSyncResult syncResult = new WSSyncResult(maxAckedUpdateOfClient);

        int fromUpdateIndex = maxAckedUpdateOfClient + 1;
        int toUpdateIndex = maxAckedUpdateOfClient + maxResults;

        List<SyncUpdate> syncLogs = syncUpdateDAO.fetchAllByAccount(accountId, fromUpdateIndex, toUpdateIndex);
        if (syncLogs == null)
        {
            throw new SyncLogProcessException(SyncExceptionCode.NO_DATA_YET, false);
        }

        for (SyncUpdate syncUpdate : syncLogs)
        {
            try
            {
                int updateIndex = syncUpdate.getUpdateIndex();
                String update = syncUpdate.getUpdate();
                WSSyncUpdate wsSyncUpdate = (WSSyncUpdate) Utils.getInstanceFromJson(update, WSSyncUpdate.class);

                syncResult.addUpdate(wsSyncUpdate);

            }
            catch (Exception e)
            {
                logger.error("Error parsing sync entry from db for : " + syncUpdate);
            }

        }

        return syncResult;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-advanceReadCursor")
    public int advanceReadCursor(String accountId, String clientId, int requestedCursorIndex) throws SyncLogProcessException
    {
        int index = syncUpdateDAO.advanceReadCursor(accountId, clientId, requestedCursorIndex);

        if (index == -2)
        {
            throw new SyncLogProcessException(SyncExceptionCode.NO_DATA_YET, false);
        }

        return index;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-appendUpdates")
    public void appendUpdate(String accountId, WSSyncUpdate syncUpdate)
    {
        syncUpdateDAO.appendUpdate(accountId, syncUpdate);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SyncDBService-createClient")
    public int createClient(String clientId) throws ClientProcessException
    {
        int index = syncUpdateDAO.createClient(clientId);

        if (index == -2)
        {
            throw new ClientProcessException(ClientExceptionCode.CLIENT_ALREADY_EXISTS, true);
        }
        else if (index == -3)
        {
            throw new ClientProcessException(HTTPResponseType.NOT_FOUND, ClientExceptionCode.CLIENT_NOT_FOUND.getExceptionName(), true,
                    "Weird! The account for the requesting client does not exist! Hmmm!");
        }

        return index;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public SyncUpdateDAO getSyncLogDAO()
    {
        return syncUpdateDAO;
    }

    public SyncUpdateReadCursorsDAO getSyncLogReadCursorsDAO()
    {
        return syncLogReadCursorsDAO;
    }

    public SyncUpdateWriteCursorsDAO getSyncLogWriteCursorsDAO()
    {
        return syncLogWriteCursorsDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public void setSyncLogDAO(SyncUpdateDAO syncLogDAO)
    {
        this.syncUpdateDAO = syncLogDAO;
    }

    public void setSyncLogReadCursorsDAO(SyncUpdateReadCursorsDAO syncLogReadCursorsDAO)
    {
        this.syncLogReadCursorsDAO = syncLogReadCursorsDAO;
    }

    public void setSyncLogWriteCursorsDAO(SyncUpdateWriteCursorsDAO syncLogWriteCursorsDAO)
    {
        this.syncLogWriteCursorsDAO = syncLogWriteCursorsDAO;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    // @Transactional
    public static void main(String[] args) throws Exception
    {
        System.out.println(Long.MAX_VALUE);
        BasicConfigurator.configure();
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-configuration.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-Pusher.xml");
        SyncDBServiceImpl syncService = context.getBean("syncDBService", SyncDBServiceImpl.class);

        syncService.init(context);
    }

    @Transactional
    public void init(ClassPathXmlApplicationContext context) throws Exception
    {
        final SyncDBServiceImpl syncService = context.getBean("syncDBService", SyncDBServiceImpl.class);
        final SyncDBServiceImpl syncService1 = context.getBean("syncDBService", SyncDBServiceImpl.class);
        AccountJpaDAO accountDAO = context.getBean("accountJpaDAO", AccountJpaDAO.class);
        ClientJpaDAO clientDAO = context.getBean("clientJpaDAO", ClientJpaDAO.class);

        List<Account> accountList = accountDAO.getAllAccounts();

        final String account1 = accountList.get(1).getId();
        final String account2 = accountList.get(0).getIdAsString();

        List<Client> client1List = clientDAO.getAllClientsOfAccount(account1);
        List<Client> client2List = clientDAO.getAllClientsOfAccount(account2);

        final String client1 = client1List.get(0).getId();
        final String client2 = client2List.get(1).getId();
        // final String client3 = client2List.get(0).getId();
        // final String client4 = client2List.get(1).getId();

        final WSSyncUpdate e = new WSSyncUpdate();
        // e.setType(SyncDataType.MESSAGE);
        // e.setChatMessage(new WSChatMessage(MessageType.TEXT, "fromACCID",
        // "chatThId"));

        for (int i = 0; i < 50; i++)
        {
            new Thread()
            {
                public void run()
                {

                    syncService.appendUpdate(account2, e);

                };
            }.start();
        }

        for (int i = 0; i < 50; i++)
        {
            new Thread()
            {
                public void run()
                {
                    syncService.appendUpdate(account2, e);

                };
            }.start();
        }

        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account2, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account2, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account2, e);
        syncService.appendUpdate(account2, e);
        syncService.appendUpdate(account2, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);
        syncService.appendUpdate(account1, e);

        // 50:
        // 105
        // 12

        System.out.println("Client 1: " + syncService.createClient(client1));
        // System.out.println(syncService.advanceReadCursor(account1, client1,
        // 10));
        System.out.println("Client 2: " + syncService.createClient(client2));
        System.out.println("account 1: " + syncService.readUpdates(account1, client1, Integer.MAX_VALUE).getUpdates().size());
        System.out.println("account 2: " + syncService.readUpdates(account2, client2, Integer.MAX_VALUE).getUpdates().size());
    }

    @Transactional
    @Profiled(tag = "SyncDBService-deleteEarlierUpdatesOfAccount")
    public void deleteEarlierUpdatesOfAccount(int index, String accountId)
    {
        syncUpdateDAO.deleteEarlierUpdatesOfAccount(index, accountId);

    }
}
