package one.group.services.impl;

import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.core.enums.EntityType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PushServerType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.entities.api.response.GCMData;
import one.group.entities.api.response.WSGCMRequest;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.socket.SocketEntity;
import one.group.services.AccountRelationService;
import one.group.services.helpers.AbstractPushService;
import one.group.services.helpers.GCMConnectionFactory;
import one.group.services.helpers.PushServiceObject;
import one.group.services.helpers.SimpleExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class GCMPushService extends AbstractPushService
{
    private static final Logger logger = LoggerFactory.getLogger(GCMPushService.class);

    private GCMConnectionFactory connectionFactory;

    private static ExecutorService executorQueue;

    private AccountDAO accountDAO;

    private AccountRelationService accountRelationService;

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    private static BlockingDeque<Runnable> runnableBlockingDeque = new LinkedBlockingDeque<Runnable>();

    static
    {
        executorQueue = Executors.newFixedThreadPool(20);

        for (int i = 0; i < 20; i++)
        {
            executorQueue.submit(new SimpleExecutor(runnableBlockingDeque));
        }
    }

    public void setConnectionFactory(GCMConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    @Override
    protected boolean additionalConfigsPresent(PushServiceObject config)
    {
        return true;
    }

    @Override
    protected void initiateRequest(PushServiceObject config)
    {
        try
        {
            if (config.getServiceList().contains(PushServerType.GCM) && connectionFactory.isHandle())
            {
                logger.info(PushServerType.GCM + " pushing [" + config.getData() + "] to " + config.getGcmDeviceList() + ".");
                final List<String> deviceIds = config.getGcmDeviceList();

                final SocketEntity se = (SocketEntity) config.getData();
                final WSSyncResult data = (WSSyncResult) se.getResponse().getData();
                final WSSyncUpdate update = (WSSyncUpdate) data.getUpdates().get(0);

                final WSGCMRequest wsGCMRequest = getWSGCMRequestObject(update);
                wsGCMRequest.setRegistrationIds(deviceIds);
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        // connectionFactory.postData(wsGCMResponse, deviceIds);
                        connectionFactory.postData(wsGCMRequest);
                    }
                };
                runnableBlockingDeque.addLast(t);

            }

            if (getSuccessor() != null)
            {
                logger.debug("Passing request to successor: " + getSuccessor().getClass().getSimpleName());
                getSuccessor().forwardRequest(config);
            }
        }
        catch (Exception e)
        {
            logger.error("Error while pushing data to GCM. " + config, e);
        }

    }

    private WSGCMRequest getWSGCMRequestObject(WSSyncUpdate wsSyncUpdate)
    {
        WSGCMRequest wsGCMRequest = new WSGCMRequest();
        GCMData gcmData = new GCMData();

        if (wsSyncUpdate.getType().equals(SyncDataType.MESSAGE))
        {
            WSMessageResponse message = wsSyncUpdate.getMessage();
            MessageType messageType = message.getType();
            String chatThreadId = message.getChatThreadId();
            String senderId = message.getCreatedBy().getId();

            // set data
            gcmData.setType(SyncDataType.MESSAGE.toString());
            gcmData.setSubType(messageType.toString());
            gcmData.setSenderId(senderId);
            gcmData.setSenderName(message.getCreatedBy().getFullName());
            if (message.getCreatedBy().getPhoto() != null && message.getCreatedBy().getPhoto().getThumbnail() != null)
            {
                gcmData.setSenderPic(message.getCreatedBy().getPhoto().getThumbnail().getLocation());
            }

            gcmData.setSenderLastActiveTime(message.getCreatedBy().getLastActivityTime());
            gcmData.setSenderPrimaryNumber(message.getCreatedBy().getPrimaryMobile());
            gcmData.setSenderCity(message.getCreatedBy().getCity().getName());
            gcmData.setChatThreadId(chatThreadId);

            String messageText = "";
            if (messageType.equals(MessageType.TEXT))
            {
                messageText = message.getText();
            }
            else if (messageType.equals(MessageType.PHOTO))
            {
                messageText = "sent you a photo";
            }
            else if (messageType.equals(MessageType.BROADCAST))
            {
                messageText = "sent you a property listing";
            }
            else if (messageType.equals(MessageType.PHONE_CALL))
            {
                messageText = "tried to call you through 1Group";
            }

            if (messageText.length() > 100)
            {
                messageText = messageText.substring(0, 100) + "...";
            }
            gcmData.setMessage(messageText);

        }
        else if (wsSyncUpdate.getType().equals(SyncDataType.NEWS))
        {
            gcmData.setId(null);
            gcmData.setType(SyncDataType.NEWS.toString());
        }
        else if (wsSyncUpdate.getType().equals(SyncDataType.SPECIAL_UPDATE))
        {
            gcmData.setId(null);
            gcmData.setType(SyncDataType.SPECIAL_UPDATE.toString());
        }
        else if (wsSyncUpdate.getType().equals(SyncDataType.APP_UPDATE))
        {
            gcmData.setId(null);
            gcmData.setType(SyncDataType.APP_UPDATE.toString());
        }
        else if (wsSyncUpdate.getType().equals(SyncDataType.SEARCH))
        {
            gcmData.setId(null);
            gcmData.setType(SyncDataType.SEARCH.toString());
        }
        else if (wsSyncUpdate.getType().equals(SyncDataType.NOTIFICATION))
        {
            // removed existing code & kept blank for future use
        }
        else if ((wsSyncUpdate.getType().equals(SyncDataType.INVALIDATION)) && (wsSyncUpdate.getEntity().getNamespace().equals(EntityType.BROADCAST)))
        {
            String messageText = "New property messages match your search!";
            gcmData.setMessage(messageText);
            gcmData.setType(SyncDataType.BROADCAST.toString());
        }
        else if ((wsSyncUpdate.getType().equals(SyncDataType.INVALIDATION)) && (wsSyncUpdate.getEntity().getNamespace().equals(EntityType.ACCOUNT_RELATION)))
        {
            // Send GCM TO Native Contacts when one of them registers
            String messageText = "New member has joined 1Group. Click here to welcome them!";
            gcmData.setMessage(messageText);
            gcmData.setType(SyncDataType.ACCOUNT_RELATION.toString());
        }

        wsGCMRequest.setData(gcmData);

        return wsGCMRequest;
    }
}
