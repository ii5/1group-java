package one.group.services.impl;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.dao.PropertySearchRelationDAO;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.services.PropertySearchRelationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * @author sanilshet
 *
 */
public class PropertySearchRelationServiceImpl implements PropertySearchRelationService
{
	
	private static final Logger logger = LoggerFactory.getLogger(PropertyListingServiceImpl.class);
	
	PropertySearchRelationDAO propertySearchRelationDAO;
	
	public PropertySearchRelationDAO getPropertySearchRelationDAO() 
	{
		return propertySearchRelationDAO;
	}

	public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO) 
	{
		this.propertySearchRelationDAO = propertySearchRelationDAO;
	}
	
	@Transactional(rollbackOn = {PersistenceException.class})
	public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation) 
	{
		try
		{
			propertySearchRelationDAO.savePropertySearchRelation(propertySearchRelation);
		}
		catch (PersistenceException persistenceexp)
		{			
			logger.warn("Error while persisting property search relation property" + persistenceexp.getMessage());
			throw persistenceexp;
		}
	}
}
