package one.group.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.InviteStatus;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.UserSource;
import one.group.core.enums.status.AccountOnlineStatus;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.dao.CityDAO;
import one.group.dao.ClientDAO;
import one.group.dao.MediaDAO;
import one.group.dao.OtpDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.request.v2.WSPhoto;
import one.group.entities.api.response.WSAccountStatus;
import one.group.entities.api.response.WSAccountStatusResult;
import one.group.entities.api.response.WSChatNotificationTMS;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSHeartBeat;
import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountListResultAdmin;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Developer;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.helpers.AccountObject;
import one.group.entities.jpa.helpers.ClientObject;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.LocationExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.LocalityNotFoundException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ClientService;
import one.group.services.LocationService;
import one.group.services.helpers.Configuration;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.ScanResult;

public class AccountServiceImpl implements AccountService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private AccountDAO accountDAO;

    private AccountJpaDAO accountJpaDAO;

    private OtpDAO otpDAO;

    private MediaDAO mediaDAO;

    private PhoneNumberDAO phoneNumberDAO;

    private ClientService clientService;

    private AccountRelationService accountRelationService;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private LocationService locationService;

    private Configuration configuration;

    private ClientDAO clientDAO;

    private CityDAO cityDAO;

    private BroadcastService broadcastService;

    public ClientDAO getClientDAO()
    {
        return clientDAO;
    }

    public void setClientDAO(ClientDAO clientDAO)
    {
        this.clientDAO = clientDAO;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public MediaDAO getMediaDAO()
    {
        return mediaDAO;
    }

    public void setMediaDAO(MediaDAO mediaDAO)
    {
        this.mediaDAO = mediaDAO;
    }

    public OtpDAO getOtpDAO()
    {
        return otpDAO;
    }

    public void setOtpDAO(OtpDAO otpDAO)
    {
        this.otpDAO = otpDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }

    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public AccountJpaDAO getAccountJpaDAO()
    {
        return accountJpaDAO;
    }

    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
    {
        this.accountJpaDAO = accountJpaDAO;
    }

    public Configuration getConfiguration()
    {
        return configuration;
    }

    public void setConfiguration(Configuration configuration)
    {
        this.configuration = configuration;
    }

    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public BroadcastService getBroadcastService()
    {
        return broadcastService;
    }

    public void setBroadcastService(BroadcastService broadcastService)
    {
        this.broadcastService = broadcastService;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-hearTheBeat")
    public WSHeartBeat hearTheBeat(String authToken, List<String> requestedAccountIds) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        String accountId = clientService.getAccountFromAuthToken(authToken).getIdAsString();
        markOnline(accountId);

        WSHeartBeat data = new WSHeartBeat();

        Set<String> onlineAccountSet = accountDAO.fetchOnlineAccounts();
        // The requesting account has requested for all his online contacts
        if (requestedAccountIds == null)
        {
            Set<WSAccountRelations> accountContacts = accountRelationService.fetchContactsOfAccount(accountId);
            Set<String> allianceAccountSet = null;
            if (accountContacts != null)
            {
                allianceAccountSet = new HashSet<String>();
                for (WSAccountRelations relations : accountContacts)
                {
                    allianceAccountSet.add(relations.getContactId());
                }
            }

            try
            {
                // intersect sets
                allianceAccountSet.retainAll(onlineAccountSet);
                allianceAccountSet.remove(accountId);
                data.setOnlineAccountIds(allianceAccountSet);
            }
            catch (Exception e)
            {
                // This is intentional, we do not want to throw any errors after
                // valid authentication. Just say no online accounts.
                logger.error("Error while checking heartbeat.", e);
            }
        }
        // The requesting account has requested the status of a specific account
        else
        {
            for (String requestedAccount : requestedAccountIds)
            {
                if (onlineAccountSet.contains(requestedAccount))
                {
                    data.addOnlineAccountId(requestedAccount);
                }
            }
        }

        return data;
    }

    public void markOffline(String accountId)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");
        accountDAO.deleteOnlineKey(accountId);
    }

    public void markOnline(String accountId)
    {
        Validation.notNull(accountId, "Account id passed should not be null.");
        accountDAO.createOnlineKey(accountId);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchActiveAccountFromAuthToken")
    public WSAccount fetchActiveAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        WSAccount wsAccount = fetchAccountFromAuthToken(authToken);
        if (!wsAccount.getStatus().equals(Status.ACTIVE) && !wsAccount.getStatus().equals(Status.SEEDED) && !wsAccount.getStatus().equals(Status.ADMIN))
        {
            throw new AccountNotFoundException(AccountExceptionCode.FORBIDDEN, true, wsAccount.getId());
        }

        return wsAccount;

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountFromAuthToken")
    public WSAccount fetchAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        Validation.notNull(authToken, "Auth token should not be null.");
        Account account = clientService.getAccountFromAuthToken(authToken);
        if (account == null)
        {
            return null;
        }
        WSAccount wsAccount = new WSAccount(account);
        return wsAccount;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountFromAuthToken")
    public WSAccount fetchAccountWithOutLocationsFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        Validation.notNull(authToken, "Auth token should not be null.");
        Account account = clientService.getAccountFromAuthToken(authToken);
        if (account == null)
        {
            return null;
        }
        boolean isLocation = false;
        boolean isPhoto = false;
        WSAccount wsAccount = new WSAccount(account, isLocation, isPhoto);
        return wsAccount;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountIdFromAuthToken")
    public String fetchAccountIdFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        Validation.notNull(authToken, "Auth token should not be null.");
        Account account = clientService.getAccountFromAuthToken(authToken);
        if (account == null)
        {
            return null;
        }
        String accountId = account.getId();

        return accountId;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountByMobileNumber")
    public WSAccount fetchAccountByMobileNumber(String mobileNumber)
    {
        Account account = accountDAO.fetchAccountByPhoneNumber(mobileNumber);

        if (account != null)
        {
            return new WSAccount(account);
        }
        return null;

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-getAccountDetailsForAdminByMobileNumber")
    public WSAccount getAccountDetailsForAdminByMobileNumber(String mobileNumber)
    {
        Account account = accountDAO.fetchAccountByPhoneNumber(mobileNumber);

        if (account != null)
        {
            WSAccount wsAccount = new WSAccount(account);
            if (account.getCreatedTime() != null)
            {
                wsAccount.setCreatedTime("" + account.getCreatedTime().getTime());
            }

            if (account.getRegistrationTime() != null)
            {
                wsAccount.setRegistrationTime("" + account.getRegistrationTime().getTime());
            }

            List<Client> client = clientDAO.getLastActivityTime(wsAccount.getId());
            for (Client c : client)
            {
                if (c.getLastActivityTime() != null)
                {
                    wsAccount.setLastActivityTime("" + c.getLastActivityTime().getTime());
                }
            }

            // int propertyListingCount =
            // propertyListingService.fetchActivePropertyListingCountOfAccount(wsAccount.getId());
            // wsAccount.setPropertyListingCount(propertyListingCount); TODO

            return wsAccount;

        }
        return null;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAllAccounts")
    public List<WSAccount> fetchAllAccounts()
    {
        List<WSAccount> wsAccountList = new ArrayList<WSAccount>();

        List<Account> accountList = accountDAO.getAllAccounts();
        for (Account account : accountList)
        {
            WSAccount wsAccount = new WSAccount(account);
            wsAccountList.add(wsAccount);
        }

        return wsAccountList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-saveOrUpdateAccount")
    public WSAccount saveOrUpdateAccount(String accountId, AccountType type, Status status, String primaryPhoneNumber, String address, String email, String fullName, String companyName,
            String establishmentName, String cityId, WSPhoto fullPhoto, WSPhoto thumbnailPhoto, Date registrationTime) throws Abstract1GroupException
    {
        PhoneNumber number = null;
        Account account = null;
        String shortReference = null;

        if (!(accountId != null && (account = accountDAO.fetchAccountById(accountId)) != null))
        {
            if (type.equals(AccountType.AGENT))
            {
                account = new Agent();
            }
            else if (type.equals(AccountType.DEVELOPER))
            {
                account = new Developer();
            }
            else
            {
                account = new Account();
            }
            account.setCreatedBy(account.getId());
        }

        if (primaryPhoneNumber != null)
        {
            number = phoneNumberDAO.fetchPhoneNumber(primaryPhoneNumber);
        }

        account.setType(this.<AccountType> returnArg1OnNull(account.getType(), type));
        account.setStatus(this.<Status> returnArg1OnNull(account.getStatus(), status));
        account.setPrimaryPhoneNumber(this.<PhoneNumber> returnArg1OnNull(account.getPrimaryPhoneNumber(), number));
        account.setAddress(this.<String> returnArg1OnNull(account.getAddress(), address));
        account.setEmail(this.<String> returnArg1OnNull(account.getEmail(), email));
        account.setFullName(this.<String> returnArg1OnNull(account.getFullName(), fullName));

        account.setCompanyName(this.<String> returnArg1OnNull(account.getCompanyName(), companyName));
        if (Utils.isEmpty(companyName))
        {
            account.setCompanyName(null);
        }

        if (status != null && account.getShortReference() == null)
        {

            do
            {
                shortReference = Utils.generateAccountReference();
                Account tAccount = accountDAO.fetchAccountByShortReference(shortReference);
                if (tAccount != null)
                {
                    shortReference = null;
                }
            }
            while (shortReference == null);
            account.setShortReference(shortReference);

            if (!status.equals(Status.SEEDED))
            {
                account.setFullPhoto(null);
                account.setThumbnailPhoto(null);
            }
        }

        if (!Utils.isNull(cityId))
        {
            City city = cityDAO.getCityById(cityId);
            if (city == null)
            {
                throw new LocalityNotFoundException(LocationExceptionCode.LOCATION_NOT_FOUND, true, "");
            }
            account.setCity(city);
        }

        if (fullPhoto != null && thumbnailPhoto != null)
        {
            account.setFullPhoto(buildMedia(fullPhoto, MediaType.IMAGE, MediaDimensionType.FULL, MediaDimensionType.FULL + account.getFullName()));
            account.setThumbnailPhoto(buildMedia(thumbnailPhoto, MediaType.IMAGE, MediaDimensionType.THUMBNAIL, MediaDimensionType.THUMBNAIL + account.getFullName()));
        }

        if (registrationTime != null)
        {
            account.setRegistrationTime(registrationTime);
        }

        account.setUpdatedBy(accountId);
        account.setUpdatedTime(new Date());
        account = accountDAO.saveAccount(account);

        return new WSAccount(account);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountService-saveIfNotExist")
    public Account saveIfNotExist(AccountType type, Status status, String mobileNumber) throws Abstract1GroupException
    {
        PhoneNumber number = null;
        Account account = null;
        String shortReference = null;

        if (type.equals(AccountType.AGENT))
        {
            account = new Agent();
        }
        else if (type.equals(AccountType.DEVELOPER))
        {
            account = new Developer();
        }
        else
        {
            account = new Account();
        }

        // added for API-126
        String accountId = account.getId();
        account.setCreatedBy(accountId);

        if (mobileNumber != null)
        {
            number = phoneNumberDAO.fetchPhoneNumber(mobileNumber);
        }

        account.setType(type);
        account.setStatus(status);
        account.setSource(UserSource.APP);
        account.setPrimaryPhoneNumber(number);
        try
        {
            accountDAO.persistAccount(account);
        }
        catch (PersistenceException pe)
        {
            logger.warn("Error while persisting account: " + account + ". " + pe.getMessage());
            throw pe;
        }

        return account;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-updateAccountProfilePhoto")
    public WSAccount updateAccountProfilePhoto(String authToken, String mediaName, MediaType type, MediaDimensionType dimension, String url, int height, int width, int size)
            throws Abstract1GroupException
    {
        // TODO: Add Validations
        Account account = clientService.getAccountFromAuthToken(authToken);

        WSAccount wsAcc = null;
        if (account != null)
        {
            Media media = new Media();
            media.setName(mediaName);
            media.setType(type);
            media.setDimension(dimension);
            media.setHeight(height);
            media.setWidth(width);
            media.setUrl(url);
            media.setSize(size);

            if (dimension.equals(MediaDimensionType.FULL))
            {
                account.setFullPhoto(media);
            }
            else
            {
                account.setThumbnailPhoto(media);
            }

            account = accountDAO.saveAccount(account);
            wsAcc = new WSAccount(account);
        }

        return wsAcc;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountDetails")
    public WSAccount fetchAccountDetails(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id passed should not be empty");
        Account account = accountDAO.fetchAccountById(accountId);
        if (account == null)
        {
            return null;
        }

        return new WSAccount(account);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchActiveAccountDetails")
    public WSAccount fetchActiveAccountDetails(String accountId) throws AccountNotFoundException
    {
        WSAccount wsAccount = fetchAccountDetails(accountId);
        if (wsAccount == null)
        {
            return null;
        }

        if (!wsAccount.getStatus().equals(Status.ACTIVE))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, wsAccount.getId());
        }

        return wsAccount;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountStatus")
    public WSAccountStatusResult fetchAccountStatus(String authToken, Collection<String> requestedAccountIds) throws Abstract1GroupException
    {
        Validation.notNull(authToken, "Auth token passed should not be null.");
        Validation.notNull(requestedAccountIds, "Requested Account Ids should not be null.");
        List<WSAccountStatus> accountStatusList = new ArrayList<WSAccountStatus>();
        WSAccount requestingWSAccount = fetchAccountFromAuthToken(authToken);

        if (requestingWSAccount.getStatus().equals(Status.INACTIVE))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_INACTIVE, true, requestingWSAccount.getId());
        }

        for (String requestedAccountId : requestedAccountIds)
        {
            Account reqAccount = accountDAO.fetchAccountById(requestedAccountId);
            if (reqAccount != null)
            {
                Long checkedInTime = accountDAO.fetchOnlineAccountTime(reqAccount.getIdAsString());
                WSAccountStatus status = new WSAccountStatus();
                status.setAccountId(reqAccount.getIdAsString());
                Long elapsedTime = null;
                status.setStatus(AccountOnlineStatus.OFFLINE);

                if (checkedInTime != null)
                {
                    Long currentTime = new Date().getTime();
                    elapsedTime = (currentTime - checkedInTime);
                    // MOV-2685 : Change online time to 5 minutes.
                    if (elapsedTime < Constant.FIVE_MINUTE_IN_MS && elapsedTime > 0)
                    {
                        status.setStatus(AccountOnlineStatus.ONLINE);
                    }

                    if (status.getStatus().equals(AccountOnlineStatus.OFFLINE))
                    {
                        status.setLastSeenTime(String.valueOf(checkedInTime));
                    }
                }

                accountStatusList.add(status);
            }
        }

        WSAccountStatusResult result = new WSAccountStatusResult();
        result.setResults(accountStatusList);

        return result;
    }

    // helpers
    private <T> T returnArg1OnNull(T arg1, T arg2)
    {
        return arg2 == null ? arg1 : arg2;
    }

    private Media buildMedia(WSPhoto photo, MediaType type, MediaDimensionType dimension, String name)
    {
        Media media = null;
        if (photo != null)
        {
            media = new Media();
            media.setName(name);
            media.setType(MediaType.IMAGE);
            media.setDimension(dimension);
            media.setHeight(photo.getHeight());
            media.setWidth(photo.getWidth());
            media.setUrl(photo.getLocation());
            media.setSize(new Long(photo.getSize()).intValue());
        }
        return media;
    }

    public void pushHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(hintType, entityType, accountId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(actionType);
        syncLogEntry.setAssociatedEntityType(entityType);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-removeProfilePic")
    public void removeProfilePic(String entityId, String entityType, List<String> images, String cdnImageFolder, String cdnImageURL)
    {
        Validation.notNull(entityId, "Entity id passed should not be null.");
        Validation.notNull(entityType, "Entity type passed should not be null.");
        try
        {
            Account account = accountDAO.fetchAccountById(entityId);
            Map<String, Object> parameters = new HashMap<String, Object>();
            List<String> imagesUrls = new ArrayList<String>();
            for (int i = 0; i < images.size(); i++)
            {
                String url = cdnImageURL + entityType + File.separator + images.get(i);
                String thumbNailUrl = cdnImageURL + entityType + File.separator + "thumbnail" + File.separator + images.get(i);
                imagesUrls.add(url);
                imagesUrls.add(thumbNailUrl);
                String relativePathFull = cdnImageFolder + "accounts" + File.separator;
                File fullFile = new File(relativePathFull + images.get(i));
                fullFile.delete();
                File thumbFile = new File(relativePathFull + File.separator + "thumbnail" + File.separator + images.get(i));
                thumbFile.delete();
            }
            parameters.put("urlList", imagesUrls);
            mediaDAO.removeMedia(entityType, parameters);
            account.setFullPhoto(null);
            account.setThumbnailPhoto(null);
        }
        catch (Exception e)
        {
            logger.error("Error while removing image.", e);
        }

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountByShortReference")
    public WSAccount fetchAccountByShortReference(String shortReference)
    {
        Account account = accountDAO.fetchAccountByShortReference(shortReference);

        if (account != null)
        {
            return new WSAccount(account);
        }
        return null;
    }

    @Profiled(tag = "accountServiceImpl-fetchAllAccountToSendChatNotification")
    public List<String> fetchAllAccountToSendChatNotification()
    {
        boolean fetchMore = true;
        String cursor = Constant.SMS_DEFAULT_CURSOR;
        List<String> accounts = new ArrayList<String>();

        while (fetchMore)
        {
            ScanResult<String> sr = accountDAO.fetchAccountsToSendSmsNotification(cursor, Constant.SMS_KEY_PATTERN, Constant.SMS_FETCH_COUNTER);
            cursor = sr.getStringCursor();
            if (!cursor.equals("0"))
            {
                accounts.addAll(sr.getResult());
            }
            else
            {
                fetchMore = false;
            }
        }
        return accounts;
    }

    @Profiled(tag = "accountServiceImpl-fetchChatSmsNotificationTimestamp")
    public WSChatNotificationTMS fetchChatSmsNotificationTimestamp(String accountKey)
    {
        Map<String, String> tms = accountDAO.fetchChatSmsNotificationTimestamp(accountKey);
        Long lastChatMessageReceivedTms = tms.containsKey(Constant.SMS_LAST_CHAT_MESSAGE_RECEIVED_TMS) ? Long.valueOf(tms.get(Constant.SMS_LAST_CHAT_MESSAGE_RECEIVED_TMS)) : null;
        Long lastReceivedIndexAdvancedTms = tms.containsKey(Constant.SMS_LAST_RECEIVED_INDEX_ADVANCED_TMS) ? Long.valueOf(tms.get(Constant.SMS_LAST_RECEIVED_INDEX_ADVANCED_TMS)) : null;
        Long lastSmsSent = tms.containsKey(Constant.SMS_LAST_SMS_SENT_TMS) ? Long.valueOf(tms.get(Constant.SMS_LAST_SMS_SENT_TMS)) : null;

        WSChatNotificationTMS notificationTMS = new WSChatNotificationTMS(lastChatMessageReceivedTms, lastReceivedIndexAdvancedTms, lastSmsSent);
        return notificationTMS;
    }

    @Profiled(tag = "accountServiceImpl-updateChatSmsNotificationTimestamp")
    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp)
    {
        accountDAO.updateChatSmsNotificationTimestamp(accountId, timestampKey, timestamp);
    }

    @Profiled(tag = "accountServiceImpl-fetchCount")
    public long fetchCount()
    {
        // String count = accountDAO.fetchCount();
        long countInLong = 0;
        // if (count == null)
        // {
        countInLong = accountJpaDAO.fetchCount();
        updateCount(countInLong);
        // }
        // else
        // {
        // countInLong = Long.parseLong(count);
        // }
        return countInLong;
    }

    public void incrementCount()
    {
        // long currentCount = fetchCount();
        // currentCount = currentCount + 1;
        // updateCount(currentCount);
    }

    public void updateCount(long count)
    {
        accountDAO.updateCount(String.valueOf(count));
    }

    @Profiled(tag = "accountServiceImpl-fetchAllActiveAccounts")
    public List<WSAccount> fetchAllActiveAccounts()
    {
        Status status = Status.ACTIVE;
        List<Account> accountList = accountDAO.fetchAllAccountsByStatus(status);
        List<WSAccount> wsAccount = new ArrayList<WSAccount>();
        for (Account account : accountList)
        {
            wsAccount.add(new WSAccount(account));
        }
        return wsAccount;
    }

    @Profiled(tag = "accountServiceImpl-fetchAccountInviteCode")
    public String fetchAccountInviteCode(String accountId)
    {

        String inviteCode = accountDAO.fetchAccountInviteCodeFromCache(accountId);

        if (!Utils.isNullOrEmpty(inviteCode))
        {
            return inviteCode;
        }

        // Return null every time if not exist
        return null;
    }

    @Profiled(tag = "accountServiceImpl-saveInviteCodeForAccount")
    public void saveInviteCodeForAccount(String accountId, String inviteCode)
    {
        int expiryInSeconds = configuration.getInviteCodeExpiryTime();
        accountDAO.saveAccountInviteCodeInCache(accountId, inviteCode, expiryInSeconds);
    }

    @Profiled(tag = "accountServiceImpl-fetchAllSeededAccounts")
    public List<Account> fetchAllSeededAccounts()
    {
        return accountDAO.fetchAllAccountsByStatus(Status.SEEDED);

    }

    @Transactional
    @Profiled(tag = "accountServiceImpl-updateSeededAccountShortReference")
    public void updateSeededAccountShortReference(Account account)
    {
        accountDAO.saveAccount(account);

    }

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<Account> accounts)
    {
        Map<String, WSInviteContact> phoneNumbers = new HashMap<String, WSInviteContact>();
        if (accounts != null && !accounts.isEmpty())
        {
            for (Account account : accounts)
            {
                String phoneNumber = account.getPrimaryPhoneNumber().getNumber();

                if (account.getStatus().equals(Status.ACTIVE))
                {
                    WSInviteContact ic = new WSInviteContact();
                    ic.setNumber(phoneNumber);
                    ic.setStatus(InviteStatus.NOT_SELECTED);
                    ic.setStatus(InviteStatus.IN_1GROUP);
                    phoneNumbers.put(phoneNumber, ic);
                }
            }
        }
        return phoneNumbers;
    }

    @Profiled(tag = "AccountServiceImpl-fetchAccountOfNativeContactsOfAccount")
    public List<Account> fetchAccountOfNativeContactsOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return accountDAO.fetchAllStatusOfNativeContactsByAccountId(accountId);

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-getAccountListByMobileNumbersForAdmin")
    public WSAccountListResultAdmin getAccountListByMobileNumbersForAdmin(Set<String> mobileNumbers)
    {
        WSAccountListResultAdmin wsAccountListResultAdmin = new WSAccountListResultAdmin();

        if (!Utils.isNull(mobileNumbers))
        {

            Set<Account> accounts = accountDAO.fetchAccountsByPhoneNumbers(mobileNumbers);
            Set<String> accountIds = new HashSet<String>();
            for (Account account : accounts)
            {
                accountIds.add(account.getId());
            }

            if (accounts != null && !accounts.isEmpty())
            {
                Map<String, Long> accountIdVsBroadcastCountMap = broadcastService.fetchAccountAndBroadcastTagCountMapByStatus(accountIds, BroadcastTagStatus.ADDED, BroadcastStatus.ACTIVE);
                List<WSAccount> wsAccounts = new ArrayList<WSAccount>();
                for (Account account : accounts)
                {
                    String accountId = account.getId();
                    WSAccount wsAccount = new WSAccount(account);
                    if (account.getCreatedTime() != null)
                    {
                        wsAccount.setCreatedTime("" + account.getCreatedTime().getTime());
                    }

                    if (account.getRegistrationTime() != null)
                    {
                        wsAccount.setRegistrationTime("" + account.getRegistrationTime().getTime());
                    }

                    List<Client> client = clientDAO.getLastActivityTime(wsAccount.getId());
                    for (Client c : client)
                    {
                        if (c.getLastActivityTime() != null)
                        {
                            wsAccount.setLastActivityTime("" + c.getLastActivityTime().getTime());
                        }
                    }

                    // int propertyListingCount =
                    // propertyListingService.fetchActivePropertyListingCountOfAccount(wsAccount.getId());
                    // wsAccount.setPropertyListingCount(propertyListingCount);
                    // TODO

                    wsAccount.setLocalStorage(account.getLocalStorage());

                    wsAccounts.add(wsAccount);
                    long broadcastTagCount = (accountIdVsBroadcastCountMap.get(accountId) == null) ? 0 : accountIdVsBroadcastCountMap.get(accountId);
                    wsAccount.setPropertyListingCount((int) broadcastTagCount);
                }
                wsAccountListResultAdmin.setAccounts(wsAccounts);
            }

        }

        return wsAccountListResultAdmin;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-ifNullOnlyUpdateRegistrationTimeOfAccount")
    public boolean ifNullOnlyUpdateRegistrationTimeOfAccount(String accountId, Date registrationTime)
    {
        Account account = accountDAO.fetchAccountById(accountId);
        if (account != null)
        {
            if (Utils.isNull(account.getRegistrationTime()))
            {
                account.setRegistrationTime(registrationTime);
                accountDAO.updateAccount(account);
                logger.info("Account updated [" + accountId + "][" + account.getPrimaryPhoneNumber().getNumber() + "]");
            }
            else
            {
                logger.info("Account already has registration time [" + accountId + "][" + account.getPrimaryPhoneNumber().getNumber() + "]");
            }
            return true;
        }
        else
        {
            logger.info("Account not found [" + accountId + "]");
        }
        return false;
    }

    @Profiled(tag = "accountServiceImpl-fetchAllAccountIdsSubscribedToLocations")
    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds)
    {
        return accountDAO.fetchAllAccountIdsSubscribedToLocations(locationIds);
    }

    @Profiled(tag = "accountServiceImpl-fetchIdsOfAllActiveAccount")
    public Set<String> fetchIdsOfAllActiveAccount()
    {
        Set<String> accountIds = new HashSet<String>();
        List<AccountObject> accountObjects = accountDAO.fetchAllAccountIdsByStatus(Status.ACTIVE);
        if (accountObjects != null && !accountObjects.isEmpty())
        {
            for (AccountObject accountObject : accountObjects)
            {
                accountIds.add(accountObject.getId());
            }
        }
        return accountIds;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-updateShortReferenceForUnRegisteredAccountByAdmin")
    public void updateShortReferenceForUnRegisteredAccountByAdmin(String mobileNumber)
    {
        Account account = accountDAO.fetchAccountByPhoneNumber(mobileNumber);

        // only update short reference if it is null

        if (account.getShortReference() == null)
        {
            String shortReference = null;
            do
            {
                shortReference = Utils.generateAccountReference();
                Account tAccount = accountDAO.fetchAccountByShortReference(shortReference);
                if (tAccount != null)
                {
                    shortReference = null;
                }
            }
            while (shortReference == null);
            account.setShortReference(shortReference);

            accountDAO.saveAccount(account);
        }
    }

    public WSAccount saveIfNotExist(AccountType type, Status status, String primaryPhoneNumber, String address, String email, String firstname, String lastname, String establishmentName,
            List<String> localityList, WSPhoto fullPhoto, WSPhoto thumbnailPhoto) throws Abstract1GroupException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountDetailsInBulk")
    public Map<String, WSAccount> fetchAccountDetailsInBulk(Set<String> accountIdSet)
    {
        Map<String, WSAccount> map = new HashMap<String, WSAccount>();
        if (accountIdSet != null && !accountIdSet.isEmpty())
        {
            List<Account> accountList = accountDAO.fetchAllAccountByIds(accountIdSet);
            Map<String, String> accountIdVsTimeMap = fetchLastActivityTimeByAccountIdInBulk(accountIdSet);
            for (Account account : accountList)
            {
                boolean isCity = false;
                boolean isPhoto = false;
                if (account.getCity() != null)
                {
                    isCity = true;
                }

                if (account.getFullPhoto() != null)
                {
                    isPhoto = true;
                }

                WSAccount wsAccount = new WSAccount(account, isCity, isPhoto);
                if (accountIdVsTimeMap.get(account.getId()) != null)
                {
                    Long lastActivityTime = Long.valueOf(accountIdVsTimeMap.get(account.getId()).toString());
                    wsAccount.setLastActivityTime(lastActivityTime + "");

                    Long elapsedTime = null;
                    wsAccount.setOnline(false);

                    if (lastActivityTime != null)
                    {
                        Long currentTime = new Date().getTime();
                        elapsedTime = (currentTime - lastActivityTime);

                        if (elapsedTime < Constant.FIVE_MINUTE_IN_MS && elapsedTime > 0)
                        {
                            wsAccount.setOnline(true);
                        }
                    }
                }
                map.put(wsAccount.getId(), wsAccount);
            }
        }
        return map;
    }

    public Set<String> fetchPhoneNumbersOfNativeContactsOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return accountDAO.fetchAllPhoneNumbersOfNativeContactsByAccountId(accountId);

    }

    public WSAccount fetchAccountByAccountId(String accountId)
    {
        Account account = accountDAO.fetchAccountByAccountId(accountId);
        if (account == null)
        {
            return null;
        }
        WSAccount wsAccount = new WSAccount(account, false);
        return wsAccount;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchAccountWithOutLocationsFromId")
    public WSAccount fetchAccountWithOutLocationsFromId(String accountId)
    {
        Validation.notNull(accountId, "accountId should not be null.");
        Account account = accountDAO.fetchAccountByAccountId(accountId);
        if (account == null)
        {
            return null;
        }
        boolean isLocation = false;
        boolean isPhoto = false;
        WSAccount wsAccount = new WSAccount(account, isLocation, isPhoto);
        return wsAccount;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "accountServiceImpl-fetchLastActivityTimeByAccountIdInBulk")
    public Map<String, String> fetchLastActivityTimeByAccountIdInBulk(Set<String> accountIdSet)
    {
        Map<String, String> accountIdVsActivitymap = new HashMap<String, String>();
        if (accountIdSet == null || accountIdSet.isEmpty())
        {
            return accountIdVsActivitymap;
        }

        List<ClientObject> clientObjectList = clientDAO.getLastActivityOfAccounts(accountIdSet);
        for (ClientObject clientObject : clientObjectList)
        {
            if (clientObject.getLastActivityTime() != null)
            {
                accountIdVsActivitymap.put(clientObject.getAccountId(), clientObject.getLastActivityTime().getTime() + "");
            }
        }
        return accountIdVsActivitymap;
    }
}
