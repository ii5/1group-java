package one.group.services.impl;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import one.group.core.enums.PushServerType;
import one.group.entities.socket.SocketEntity;
import one.group.services.helpers.AbstractPushService;
import one.group.services.helpers.MbeanClient;
import one.group.services.helpers.MoveinEnumGsonTypeAdapterFactory;
import one.group.services.helpers.PushServiceObject;
import one.group.services.helpers.SimpleExecutor;
import one.group.services.helpers.WebSocketServiceAction;

public class WebSocketPushService extends AbstractPushService
{

    private static final Logger logger = LoggerFactory.getLogger(WebSocketPushService.class);

    private static BlockingDeque<Runnable> runnableBlockingDeque = new LinkedBlockingDeque<Runnable>();

    private static ExecutorService executorQueue;
    
    private MbeanClient client;

    private int poolSize = 100;

    public void initialise()
    {
        executorQueue = Executors.newFixedThreadPool(poolSize);
        for (int i = 0; i < poolSize; i++)
        {
            executorQueue.submit(new SimpleExecutor(runnableBlockingDeque));
        }
    }

    public MbeanClient getClient() 
    {
		return client;
	}
    
    public void setClient(MbeanClient client) 
    {
		this.client = client;
	}
    
    @Override
    protected boolean additionalConfigsPresent(PushServiceObject config)
    {
        return true;
    }

    @Override
    protected void initiateRequest(final PushServiceObject config)
    {
        try
        {
            logger.info("Entered " + this.getClass().getSimpleName());
            if (config.getServiceList().contains(PushServerType.WEBSOCKET))
            {
            	client.broadcast((SocketEntity) config.getData(), config.getAccountId());
//                WebSocketServiceAction t = new WebSocketServiceAction(config);
//                runnableBlockingDeque.addLast(t);
            }

            if (getSuccessor() != null)
            {
                logger.info("Passing request to successor: " + getSuccessor().getClass().getSimpleName());
                getSuccessor().forwardRequest(config);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception during forwaring request from WebSocketPushService.", e);
        }
    }

    private Gson getGson()
    {
        GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).registerTypeAdapterFactory(new MoveinEnumGsonTypeAdapterFactory());
        Gson gson = builder.create();

        return gson;
    }

}
