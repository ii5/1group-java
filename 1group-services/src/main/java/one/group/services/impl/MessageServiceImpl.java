package one.group.services.impl;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import one.group.dao.MessageDAO;
import one.group.entities.socket.Message;
import one.group.services.MessageService;

/**
 * 
 * @author nyalf
 *
 */
public class MessageServiceImpl implements MessageService {
	
	private MessageDAO messageDAO;
	
	public MessageDAO getMessageDAO() 
	{
		return messageDAO;
	}
	
	public void setMessageDAO(MessageDAO messageDAO) 
	{
		this.messageDAO = messageDAO;
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void saveMessagesToDB(List<Message> messagesToUpdate) 
	{
		messageDAO.saveMessagesToDB(messagesToUpdate);
	}
}
