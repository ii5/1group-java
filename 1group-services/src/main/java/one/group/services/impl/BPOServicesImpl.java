package one.group.services.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.request.MapSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import one.group.core.enums.AccountType;
import one.group.core.enums.AdminStatus;
import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.SolrCollectionType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.dao.AdminDAO;
import one.group.dao.AuthAssignmentDAO;
import one.group.dao.AuthItemChildDAO;
import one.group.dao.AuthItemDAO;
import one.group.dao.BroadcastDAO;
import one.group.dao.BroadcastTagDAO;
import one.group.dao.CityDAO;
import one.group.dao.GroupMembersDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.LocationDAO;
import one.group.dao.MessageDAO;
import one.group.dao.StarredBroadcastDAO;
import one.group.dao.WaUserCityDAO;
import one.group.entities.api.request.bpo.WSAdminUserRequest;
import one.group.entities.api.request.bpo.WSGroupMembersMetadataRequest;
import one.group.entities.api.request.bpo.WSGroupMetaDataRequest;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.response.bpo.WSAuthItemResponse;
import one.group.entities.api.response.bpo.WSBroadcastConsolidatedResponse;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.bpo.WSLocationMetaData;
import one.group.entities.api.response.bpo.WSMessageResponse;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserMetaData;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSBroadcastListResult;
import one.group.entities.api.response.v2.WSCity;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.api.response.v2.WSTag;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.AuthAssignment;
import one.group.entities.jpa.AuthItem;
import one.group.entities.jpa.AuthItemChild;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.City;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.User;
import one.group.entities.jpa.WaMessagesArchive;
import one.group.entities.jpa.helpers.BPOCityLocationData;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;
import one.group.entities.jpa.helpers.WSLocationMetaDataComparator;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.MessageExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.jpa.dao.GroupsJpaDAO;
import one.group.services.BPOServices;
import one.group.services.MessageService;
import one.group.services.UserService;
import one.group.services.WaArchiveMessageService;
import one.group.services.helpers.WSGroupMetaDataCityNameComparator;
import one.group.solr.SolrServerFactory;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class BPOServicesImpl implements BPOServices
{
    private static final Logger logger = LoggerFactory.getLogger(BPOServicesImpl.class);
    private MessageDAO messageDAO;
    private GroupsDAO groupDAO;
    private BroadcastDAO broadcastDAO;
    private BroadcastTagDAO broadcastTagDAO;
    private LocationDAO locationDAO;
    private CityDAO cityDAO;
    private AdminDAO adminDAO;
    private WaUserCityDAO waUserCityDAO;
    private AuthAssignmentDAO authAssignmentDAO;
    private GroupMembersDAO groupMembersDAO;
    private GroupsJpaDAO groupsJpaDAO;
    private AuthItemChildDAO authItemChildDAO;
    private AuthItemDAO authItemDAO;
    private UserService userService;
    private AccountDAO accountDAO;
    private StarredBroadcastDAO starredBroadcastDAO;
    private MessageService messageService;
    private WaArchiveMessageService waArchiveMessageService;
    private int allowableWindow = -15;
    
    public MessageService getMessageService() 
    {
		return messageService;
	}
    
    public void setMessageService(MessageService messageService) 
    {
		this.messageService = messageService;
	}
    
    public WaArchiveMessageService getWaArchiveMessageService() 
    {
		return waArchiveMessageService;
	}
    
    public void setWaArchiveMessageService(WaArchiveMessageService waArchiveMessageService) 
    {
		this.waArchiveMessageService = waArchiveMessageService;
	}
    
    public int getAllowableWindow() 
    {
		return allowableWindow;
	}
    
    public void setAllowableWindow(int allowableWindow) 
    {
		this.allowableWindow = allowableWindow;
	}

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public UserService getUserService()
    {
        return userService;
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    public AuthItemDAO getAuthItemDAO()
    {
        return authItemDAO;
    }

    public void setAuthItemDAO(AuthItemDAO authItemDAO)
    {
        this.authItemDAO = authItemDAO;
    }

    public AuthItemChildDAO getAuthItemChildDAO()
    {
        return authItemChildDAO;
    }

    public void setAuthItemChildDAO(AuthItemChildDAO authItemChildDAO)
    {
        this.authItemChildDAO = authItemChildDAO;
    }

    public GroupMembersDAO getGroupMembersDAO()
    {
        return groupMembersDAO;
    }

    public void setGroupMembersDAO(GroupMembersDAO groupMembersDAO)
    {
        this.groupMembersDAO = groupMembersDAO;
    }

    public AuthAssignmentDAO getAuthAssignmentDAO()
    {
        return authAssignmentDAO;
    }

    public void setAuthAssignmentDAO(AuthAssignmentDAO authAssignmentDAO)
    {
        this.authAssignmentDAO = authAssignmentDAO;
    }

    public WaUserCityDAO getWaUserCityDAO()
    {
        return waUserCityDAO;
    }

    public void setWaUserCityDAO(WaUserCityDAO waUserCityDAO)
    {
        this.waUserCityDAO = waUserCityDAO;
    }

    public AdminDAO getAdminDAO()
    {
        return adminDAO;
    }

    public void setAdminDAO(AdminDAO adminDAO)
    {
        this.adminDAO = adminDAO;
    }

    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    public BroadcastTagDAO getBroadcastTagDAO()
    {
        return broadcastTagDAO;
    }

    public void setBroadcastTagDAO(BroadcastTagDAO broadcastTagDAO)
    {
        this.broadcastTagDAO = broadcastTagDAO;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public void setBroadcastDAO(BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    public GroupsDAO getGroupDAO()
    {
        return groupDAO;
    }

    public void setGroupDAO(GroupsDAO groupDAO)
    {
        this.groupDAO = groupDAO;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public GroupsJpaDAO getGroupsJpaDAO()
    {
        return groupsJpaDAO;
    }

    public void setGroupsJpaDAO(GroupsJpaDAO groupsJpaDAO)
    {
        this.groupsJpaDAO = groupsJpaDAO;
    }

    public StarredBroadcastDAO getStarredBroadcastDAO()
    {
        return starredBroadcastDAO;
    }

    public void setStarredBroadcastDAO(StarredBroadcastDAO starredBroadcastDAO)
    {
        this.starredBroadcastDAO = starredBroadcastDAO;
    }

    public List<WSGroupMetaData> fetchGroupLabelAndIdByStatus(List<GroupStatus> statusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<Groups> groupList = groupDAO.fetchGroupLabelAndIdByStatus(statusList, sort, filter);
        List<WSGroupMetaData> groupMetadata = new ArrayList<WSGroupMetaData>();

        for (Groups group : groupList)
        {
            WSGroupMetaData metaData = new WSGroupMetaData();
            metaData.setId(group.getId());
            metaData.setName(group.getGroupName());
            metaData.setValue(group.getGroupName());
            metaData.setLabel(group.getGroupName());

            groupMetadata.add(metaData);
        }

        return groupMetadata;
    }

    public List<WSGroupMetaData> fetchGroupMetaDataByStatus(List<GroupStatus> statusList)
    {
        long start = System.currentTimeMillis();
        List<WSGroupMetaData> groupMetaData = new ArrayList<WSGroupMetaData>();

        Map<String, Integer> groupIdVsMessageCountMap = messageDAO.fetchMessageCountByGroupId(false, null, null);

        Map<GroupStatus, Set<Groups>> statusVsGroupMap = groupDAO.fetchByStatusAndEverSync(statusList, true);

        for (GroupStatus status : statusVsGroupMap.keySet())
        {
            WSGroupMetaData data = new WSGroupMetaData();
            data.setStatus(status);

            int messageCount = 0;
            for (Groups g : statusVsGroupMap.get(status))
            {
                if (groupIdVsMessageCountMap.get(g.getId()) != null)
                {
                    messageCount = messageCount + groupIdVsMessageCountMap.get(g.getId());
                }
            }

            data.setCount(statusVsGroupMap.get(status).size());
            data.setTotalMessages(messageCount);

            groupMetaData.add(data);
        }
        long end = System.currentTimeMillis();

        return groupMetaData;
    }

    public Map<String, Map<String, String>> fetchLocationsByCity(List<String> cityIdList)
    {
        Map<String, Map<String, String>> cityVsLocationMap = new HashMap<String, Map<String, String>>();

        List<BPOCityLocationData> listCityLocationData = cityDAO.getAllCityLocations(cityIdList, Arrays.asList(Status.ACTIVE));

        for (BPOCityLocationData d : listCityLocationData)
        {
            Map<String, String> locationData = cityVsLocationMap.get(d.getCityName());

            if (locationData == null)
            {
                locationData = new HashMap<String, String>();
                cityVsLocationMap.put(d.getCityName(), locationData);
            }

            locationData.put(d.getLocationId(), d.getLocationDisplayName());

        }

        return cityVsLocationMap;
    }

    public List<WSLocationMetaData> fetchLocationsMetaDataByCity(List<String> cityIdList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<WSLocationMetaData> locationMetaDataList = new ArrayList<WSLocationMetaData>();

        List<BPOCityLocationData> listCityLocationData = cityDAO.getAllCityLocations(cityIdList, Arrays.asList(Status.ACTIVE));

        for (BPOCityLocationData d : listCityLocationData)
        {
            WSLocationMetaData l = new WSLocationMetaData();
            l.setId(d.getLocationId());
            l.setLabel(d.getLocationDisplayName());
            l.setValue(d.getLocationDisplayName());
            locationMetaDataList.add(l);
        }

        if (sort != null)
        {
            if (sort.get(SortField.LOCATION_NAME) != null)
            {
                Collections.sort(locationMetaDataList, new WSLocationMetaDataComparator(sort.get(SortField.LOCATION_NAME)));
            }
        }

        return locationMetaDataList;
    }

    public List<WSLocationMetaData> fetchLocationMetaData()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WSGroupMetaData> fetchDetailedGroupMetaData(List<GroupStatus> groupStatusList, List<String> groupIdList, int msgCountGreaterThan, int start, int rows, String sortByField,
            String sortByType, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {

        SortType citySortType = null;

        if (sort != null)
        {
            citySortType = sort.get(SortField.CITY_NAME);
            sort.remove(SortField.CITY_NAME);
        }

        List<Groups> groupList = groupDAO.fetchGroupsByInputs(groupStatusList, groupIdList, msgCountGreaterThan, start, rows, sortByField, sortByType, sort, filter);
        List<WSGroupMetaData> groupMetaDataList = new ArrayList<WSGroupMetaData>();
        Map<String, List<WSGroupMetaData>> cityIdVsGroupMetaDataMap = new HashMap<String, List<WSGroupMetaData>>();
        Map<String, List<WSGroupMetaData>> locationIdVsGroupMetaDataMap = new HashMap<String, List<WSGroupMetaData>>();
        Map<String, List<WSGroupMetaData>> assignedToVsGroupmetaDataMap = new HashMap<String, List<WSGroupMetaData>>();

        if (groupList != null)
        {
            for (Groups group : groupList)
            {
                WSGroupMetaData m = new WSGroupMetaData(group);

                if (m.getCityId() != null)
                {
                    List<WSGroupMetaData> groupMetaDataCityList = cityIdVsGroupMetaDataMap.get(m.getCityId());
                    if (groupMetaDataCityList == null)
                    {
                        groupMetaDataCityList = new ArrayList<WSGroupMetaData>();
                        cityIdVsGroupMetaDataMap.put(m.getCityId(), groupMetaDataCityList);
                    }

                    cityIdVsGroupMetaDataMap.get(m.getCityId()).add(m);
                }

                if (m.getLocationId() != null)
                {
                    List<WSGroupMetaData> groupMetaDataLocationList = locationIdVsGroupMetaDataMap.get(m.getCityId());
                    if (groupMetaDataLocationList == null)
                    {
                        groupMetaDataLocationList = new ArrayList<WSGroupMetaData>();
                        locationIdVsGroupMetaDataMap.put(m.getLocationId(), groupMetaDataLocationList);
                    }

                    locationIdVsGroupMetaDataMap.get(m.getLocationId()).add(m);
                }

                if (group.getAssignedTo() != null)
                {
                    List<WSGroupMetaData> groupMetaDataAdminList = assignedToVsGroupmetaDataMap.get(group.getAssignedTo());

                    if (groupMetaDataAdminList == null)
                    {
                        groupMetaDataAdminList = new ArrayList<WSGroupMetaData>();
                        assignedToVsGroupmetaDataMap.put(group.getAssignedTo(), groupMetaDataAdminList);
                    }

                    assignedToVsGroupmetaDataMap.get(group.getAssignedTo()).add(m);
                }

                groupMetaDataList.add(m);
            }
        }

        List<City> cityList = cityIdVsGroupMetaDataMap.isEmpty() ? new ArrayList<City>() : cityDAO.getAllCitiesByCityIds(cityIdVsGroupMetaDataMap.keySet());

        List<Location> locationList = locationIdVsGroupMetaDataMap.isEmpty() ? new ArrayList<Location>() : locationDAO.fetchAllLocations(locationIdVsGroupMetaDataMap.keySet());
        
        List<Admin> adminList = null;

        if (!assignedToVsGroupmetaDataMap.isEmpty())
        {
            Map<FilterField, Set<String>> filterNew = new HashMap<FilterField, Set<String>>();
            filterNew.put(FilterField.ID, assignedToVsGroupmetaDataMap.keySet());
            adminList = adminDAO.fetchAdminUsersByInput(null, null, null, null, filterNew, 0, Integer.MAX_VALUE);
        }

        if (adminList != null)
        {
            for (Admin admin : adminList)
            {
                List<WSGroupMetaData> adminGroupMetaDataList = assignedToVsGroupmetaDataMap.get(admin.getId());

                if (adminGroupMetaDataList != null)
                {
                    for (WSGroupMetaData m : adminGroupMetaDataList)
                    {
                        m.setUsername(admin.getUserName());
                        ;
                    }
                }
            }
        }

        for (Location location : locationList)
        {
            WSLocation wsLoc = new WSLocation(location);
            List<WSGroupMetaData> metaDataList = locationIdVsGroupMetaDataMap.get(location.getId());

            if (metaDataList != null)
            {
                for (WSGroupMetaData m : metaDataList)
                {
                    m.setLocation(wsLoc);
                }
            }
        }

        for (City city : cityList)
        {
            WSCity wsCity = new WSCity(city);
            List<WSGroupMetaData> metaDataList = cityIdVsGroupMetaDataMap.get(city.getId());

            if (metaDataList != null)
            {
                for (WSGroupMetaData m : metaDataList)
                {
                    m.setCity(wsCity);
                }
            }
        }

        if (citySortType != null)
        {
            Collections.sort(groupMetaDataList, new WSGroupMetaDataCityNameComparator(citySortType));
        }

        // for(WSGroupMetaData m : groupMetaDataList)
        // {
        // System.out.println(m.getCity());
        // }

        // System.out.println(groupMetaDataList);

        return groupMetaDataList;
    }

    public List<WSGroupMetaData> fetchGroupsOfMember(String mobileNumber)
    {
        List<GroupMembers> groupMembers = groupMembersDAO.fetchGroupMembersByMobileNo(mobileNumber);
        Map<String, GroupMembers> idsVsMembersMap = new HashMap<String, GroupMembers>();
        List<WSGroupMetaData> groupMetadataResponse = new ArrayList<WSGroupMetaData>();

        for (GroupMembers gm : groupMembers)
        {
            idsVsMembersMap.put(gm.getGroupId(), gm);
        }

        if (idsVsMembersMap.isEmpty())
        {
            return groupMetadataResponse;
        }

        groupMetadataResponse = fetchDetailedGroupMetaData(null, new ArrayList<String>(idsVsMembersMap.keySet()), -1, 0, Integer.MAX_VALUE, null, null, null, null);

        for (WSGroupMetaData gm : groupMetadataResponse)
        {
            GroupMembers m = idsVsMembersMap.get(gm.getId());
            if (m != null)
            {
                gm.setCurrentSync(m.isCurrentSync());
            }
            else
            {
                gm.setCurrentSync(false);
            }
        }

        return groupMetadataResponse;
    }

    public List<WSLocation> fetchLocationsBasedOnInput(String locationId, String cityId, boolean withCity)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WSLocationMetaData> fetchLocationMetaData(Status status, String cityId, String locationId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WSMessageResponse> fetchUnreadMessages(int limit, int offset)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WSMessageResponse> fetchReadMessages(int limit, int offset)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WSGroupMetaData> fetchGroupsByGroupIds(List<String> groupIdList)
    {

        if (groupIdList == null || groupIdList.isEmpty())
        {
            return new ArrayList<WSGroupMetaData>();
        }

        List<Groups> groupList = groupDAO.fetchGroupsByGroupIds(groupIdList);
        List<WSGroupMetaData> groupMetaDataList = new ArrayList<WSGroupMetaData>();

        for (Groups g : groupList)
        {
            WSGroupMetaData gm = new WSGroupMetaData(g);

            groupMetaDataList.add(gm);
        }

        return groupMetaDataList;

    }

    @Transactional
    public List<WSLocation> fetchLocationsByLocationIds(List<String> locationIds)
    {

        List<Location> locationList = locationDAO.fetchAllLocations(locationIds);
        List<WSLocation> wsLocationList = new ArrayList<WSLocation>();

        for (Location l : locationList)
        {
            WSLocation wsL = new WSLocation();
            wsL.setCity(new WSCity(l.getCity()));
            wsL.setId(l.getId());
            wsL.setName(l.getLocalityName());
            wsL.setLocationDisplayName(l.getLocationDisplayName());
            wsL.setParentId(l.getParentId());
            wsL.setStatus(l.getStatus());
            wsL.setType(l.getType().toString());
            wsL.setZipCode(l.getZipCode());

            wsLocationList.add(wsL);
        }

        return wsLocationList;
    }

    public List<WSUserMetaData> fetchUsers(String role, boolean onlyActive)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public WSBroadcastListResult fetchBroadcasts(String cityId, String locationId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Transactional(rollbackOn = { Exception.class })
    public List<WSMessageResponse> fetchMessages(List<MessageStatus> messageStatusList, List<GroupStatus> groupStatusList, List<String> groupIdList, List<String> locationIdList,
            List<String> cityIdList, List<BpoMessageStatus> bpoMessageStatusList, String bpoMessageUpdateTime, int start, int rows, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {

        Set<String> broadcastTagStatusFilter = filter.remove(FilterField.BROADCAST_TAG_STATUS);

        List<Message> messageList = messageDAO.fetchMessagesByStatusAndGroupIds(messageStatusList, groupStatusList, groupIdList, bpoMessageStatusList, bpoMessageUpdateTime, sort, filter, start, rows,
                null);
        Map<String, WSMessageResponse> broadcastIdVsMessageResponse = new HashMap<String, WSMessageResponse>();
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();

        for (Message m : messageList)
        {
            WSMessageResponse response = new WSMessageResponse(m);

            if (m.getBroadcastId() != null && !m.getBroadcastId().isEmpty())
            {
                broadcastIdVsMessageResponse.put(m.getBroadcastId(), response);
            }

            wsMessageResponseList.add(response);
        }

        // List<BroadcastTag> broadcastTagList =
        // broadcastTagDAO.fetchBroadcastTagByBroadcastIds(broadcastIdVsMessageResponse.keySet());

        Map<FilterField, Set<String>> broadcastFilter = new HashMap<FilterField, Set<String>>();
        broadcastFilter.put(FilterField.BROADCAST_ID, broadcastIdVsMessageResponse.keySet());
        if (broadcastTagStatusFilter != null)
        {
            broadcastFilter.put(FilterField.BROADCAST_TAG_STATUS, broadcastTagStatusFilter);
        }

        List<BroadcastTagsConsolidated> broadcastTagList = new ArrayList<BroadcastTagsConsolidated>();

        if (!broadcastIdVsMessageResponse.keySet().isEmpty())
        {
            broadcastTagList = broadcastDAO.fetchBroadcastsByCityAndLocation(null, null, null, broadcastFilter, 0, Integer.MAX_VALUE);
        }

        for (BroadcastTagsConsolidated tag : broadcastTagList)
        {
            String broadcastId = tag.getBroadcastId();
            WSMessageResponse message = broadcastIdVsMessageResponse.get(broadcastId);
            WSTag wsTag = new WSTag(tag);
            WSBroadcast wsBroadcast = null;
            if (message.getBroadcast() != null)
            {
                wsBroadcast = message.getBroadcast();
            }
            else
            {
                wsBroadcast = new WSBroadcast();
                message.setBroadcast(wsBroadcast);
            }

            wsBroadcast.setId(broadcastId);
            wsBroadcast.setMessageId(message.getWaMessageId());
            wsBroadcast.setStatus(tag.getBroadcastStatus());
            wsBroadcast.addTag(wsTag);
        }

        return wsMessageResponseList;
    }

    public List<WSUserMetaData> fetchUsersByRolesAndStatus(List<String> roleList, List<AdminStatus> adminStatusList, String fieldName, Map<SortField, SortType> sort,
            Map<FilterField, Set<String>> filter, int start, int rows)
    {
        List<Admin> adminList = adminDAO.fetchAdminUsersByRoleListAndStatusList(roleList, adminStatusList, sort, filter, start, rows);
        List<WSUserMetaData> userMetaDataList = new ArrayList<WSUserMetaData>();

        if (adminList == null || adminList.isEmpty())
        {
            return new ArrayList<WSUserMetaData>();
        }

        for (Admin a : adminList)
        {
            WSUserMetaData user = new WSUserMetaData();
            user.setId(a.getId());

            if (fieldName == null || fieldName.equals("username"))
            {
                user.setLabel(a.getUserName());
                user.setUsername(a.getUserName());
                user.setValue(a.getUserName());
            }
            else if (fieldName.equals("email"))
            {
                user.setLabel(a.getEmail());
                user.setEmail(a.getEmail());
                user.setValue(a.getEmail());
            }

            userMetaDataList.add(user);
        }

        return userMetaDataList;
    }

    public List<WSBroadcastConsolidatedResponse> fetchBroadcastsWithTagsByCityAndLocation(List<String> cityIdList, List<String> locationIdList, Map<SortField, SortType> sort,
            Map<FilterField, Set<String>> filter, int start, int rows)
    {
        List<String> broadcastIds = null;

        if (filter.get(FilterField.SENDER_PHONE_NUMBER) != null)
        {
            broadcastIds = messageDAO.fetchBroadcastIdsByMessageCreator(filter.get(FilterField.SENDER_PHONE_NUMBER));

            if (broadcastIds != null && !broadcastIds.isEmpty())
            {
                Set<String> broadcastIdSet = filter.get(FilterField.BROADCAST_ID);
                if (broadcastIdSet == null)
                {
                    broadcastIdSet = new HashSet<String>();
                    filter.put(FilterField.BROADCAST_ID, broadcastIdSet);
                }

                broadcastIdSet.addAll(broadcastIds);
            }
            else
            {
                return Collections.<WSBroadcastConsolidatedResponse> emptyList();
            }
        }

        filter.remove(FilterField.SENDER_PHONE_NUMBER);
        List<BroadcastTagsConsolidated> broadcastTags = broadcastDAO.fetchBroadcastsByCityAndLocation(cityIdList, locationIdList, sort, filter, start, rows);
        List<WSBroadcastConsolidatedResponse> broadcastTagResponse = new ArrayList<WSBroadcastConsolidatedResponse>();

        if (broadcastTags == null || broadcastTags.isEmpty())
        {
            return Collections.<WSBroadcastConsolidatedResponse> emptyList();
        }

        Map<String, List<WSBroadcastConsolidatedResponse>> messageIdVsResponseMap = new HashMap<String, List<WSBroadcastConsolidatedResponse>>();
        for (BroadcastTagsConsolidated b : broadcastTags)
        {
            WSBroadcastConsolidatedResponse wsB = new WSBroadcastConsolidatedResponse(b);
            broadcastTagResponse.add(wsB);

            if (b.getMessageId() != null && !b.getMessageId().trim().isEmpty())
            {
                List<WSBroadcastConsolidatedResponse> responseList = messageIdVsResponseMap.get(b.getMessageId());
                if (responseList == null)
                {
                    responseList = new ArrayList<WSBroadcastConsolidatedResponse>();
                    messageIdVsResponseMap.put(b.getMessageId(), responseList);
                }

                messageIdVsResponseMap.get(b.getMessageId()).add(wsB);
            }
        }

        if (!messageIdVsResponseMap.isEmpty())
        {
            List<String> fields = new ArrayList<String>();
            fields.add("id");
            fields.add("text");
            fields.add("groupId");
            List<Message> messageList = messageDAO.fetchMessagesFromIds(new ArrayList<String>(messageIdVsResponseMap.keySet()), fields);
            if (messageList != null)
            {
                Map<String, City> idVsCityMap = new HashMap<String, City>();
                for (Message m : messageList)
                {
                    List<WSBroadcastConsolidatedResponse> broadcastTagList = messageIdVsResponseMap.get(m.getId());

                    if (broadcastTagList != null)
                    {
                        for (WSBroadcastConsolidatedResponse r : broadcastTagList)
                        {
                            r.setText(m.getText());
                            String groupId = m.getGroupId();
                            r.setGroupId(groupId);
                            List<String> groupIds = new ArrayList<String>();
                            groupIds.add(groupId);
                            List<Groups> groups = groupDAO.fetchGroupsByGroupIds(groupIds);
                            if (groups != null && groups.size() > 0)
                            {
                                Groups group = groups.get(0);
                                String groupName = group.getGroupName();
                                r.setGroupName(groupName);
                                String cityId = group.getCityId();
                                if (cityId != null)
                                {
                                    if (idVsCityMap.get(cityId) == null)
                                    {
                                        City city = cityDAO.getCityById(cityId);
                                        idVsCityMap.put(cityId, city);
                                    }
                                    r.setGroupCityName(idVsCityMap.get(cityId).getCityName());
                                }
                            }
                        }
                    }
                }
            }
        }

        return broadcastTagResponse;
    }

    public List<WSUserCity> fetchByPriorityCityAndAdmin(String priority, String cityId, String adminId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        return waUserCityDAO.fetchByPriorityCityAndAdmin((priority == null) ? null : Integer.parseInt(priority), cityId, adminId, sort, filter, start, rows);
    }

    @Transactional
    public List<WSUserMetaData> fetchUserByUserNameAndStatus(String username, List<AdminStatus> adminStatusList, String email, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter,
            int start, int rows)
    {

        if (filter != null && filter.get(FilterField.AUTH_ROLES) != null)
        {
            Set<String> roles = filter.get(FilterField.AUTH_ROLES);
            if (!roles.isEmpty())
            {
                List<String> userIdList = authAssignmentDAO.fetchUserIdsByRoles(roles);
                if (userIdList != null && !userIdList.isEmpty())
                {
                    if (filter.get(FilterField.ID) != null)
                    {
                        filter.get(FilterField.ID).addAll(userIdList);
                    }
                    else
                    {
                        filter.put(FilterField.ID, new HashSet<String>(userIdList));
                    }
                }
            }
        }

        List<Admin> adminList = adminDAO.fetchAdminUsersByInput(username, adminStatusList, email, Utils.subSetOfSort(sort, SortField.USERNAME, SortField.EMAIL, SortField.USER_STATUS),
                Utils.subSetOfFilter(filter, FilterField.USERNAME, FilterField.EMAIL, FilterField.USER_STATUS, FilterField.ID), start, rows);

        List<WSUserMetaData> userMetaDataList = new ArrayList<WSUserMetaData>();
        Map<String, WSUserMetaData> idVsUserMap = new HashMap<String, WSUserMetaData>();

        for (Admin admin : adminList)
        {
            WSUserMetaData userMetaData = new WSUserMetaData();

            userMetaData.setAuthKey(admin.getAuthKey());
            userMetaData.setCreatedAt((admin.getCreatedTime() == null) ? null : admin.getCreatedTime().getTime() + "");
            userMetaData.setEmail(admin.getEmail());
            userMetaData.setId(admin.getId());
            userMetaData.setPassword(admin.getPassword());
            userMetaData.setPasswordHash(admin.getPasswordHash());
            userMetaData.setPasswordResetToken(admin.getPasswordResetToken());
            userMetaData.setUpdatedAt((admin.getUpdatedTime() == null) ? null : admin.getUpdatedTime().getTime() + "");
            userMetaData.setUsername(admin.getUserName());
            userMetaData.setStatus(admin.getStatus());

            userMetaDataList.add(userMetaData);
            idVsUserMap.put(admin.getId(), userMetaData);
        }

        List<AuthAssignment> authAssignmentList = authAssignmentDAO.fetchAuthAssignmentItemsByUserIdList(idVsUserMap.keySet());

        if (authAssignmentList != null)
        {
            for (AuthAssignment a : authAssignmentList)
            {
                WSUserMetaData m = idVsUserMap.get(a.getUser());
                if (m != null)
                {
                    m.addRole(a.getAuthItem().getName());
                }
            }
        }

        return userMetaDataList;
    }

    public List<WSGroupMetaData> fetchUnassignedGroupsBasedOnCity(String cityIds, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<Groups> groupList = null;
        if (cityIds != null && !cityIds.isEmpty())
        {
            for (String cityId : cityIds.split(","))
            {
                // source = me & cityId match & sort by unreadmsg count desc
                sort.put(SortField.UNREADMSGCOUNT, SortType.DESCENDING);
                filter.put(FilterField.CITY_ID, new HashSet<String>(Arrays.asList(cityId)));
                filter.put(FilterField.GROUP_SOURCE, new HashSet<String>(Arrays.asList(GroupSource.ME.toString())));
                groupList = groupDAO.fetchUnassignedGroupsBasedOnCity(null, sort, filter);

                if (groupList != null && !groupList.isEmpty())
                {
                    break;
                }
            }
        }

        if (groupList == null || groupList.isEmpty())
        {
            // source = me & sort by unreadmsg count desc
            sort.put(SortField.UNREADMSGCOUNT, SortType.DESCENDING);
            filter.remove(FilterField.CITY_ID);
            filter.put(FilterField.GROUP_SOURCE, new HashSet<String>(Arrays.asList(GroupSource.ME.toString())));
            groupList = groupDAO.fetchUnassignedGroupsBasedOnCity(null, sort, filter);
        }

        if (groupList == null || groupList.isEmpty())
        {
            if (cityIds != null && !cityIds.isEmpty())
            {
                for (String cityId : cityIds.split(","))
                {
                    // source = whatsapp & cityId match & sort by unreadmsg
                    // count desc
                    sort.put(SortField.UNREADMSGCOUNT, SortType.DESCENDING);
                    filter.put(FilterField.CITY_ID, new HashSet<String>(Arrays.asList(cityId)));
                    filter.put(FilterField.GROUP_SOURCE, new HashSet<String>(Arrays.asList(GroupSource.WHATSAPP.toString())));
                    groupList = groupDAO.fetchUnassignedGroupsBasedOnCity(null, sort, filter);

                    if (groupList != null && !groupList.isEmpty())
                    {
                        break;
                    }
                }
            }
        }

        if (groupList == null || groupList.isEmpty())
        {
            // source = whatsapp & sort by unreadmsg count desc
            sort.put(SortField.UNREADMSGCOUNT, SortType.DESCENDING);
            filter.remove(FilterField.CITY_ID);
            filter.put(FilterField.GROUP_SOURCE, new HashSet<String>(Arrays.asList(GroupSource.WHATSAPP.toString())));
            groupList = groupDAO.fetchUnassignedGroupsBasedOnCity(null, sort, filter);
        }

        List<WSGroupMetaData> groupMetadataList = new ArrayList<WSGroupMetaData>();

        if (groupList != null)
        {
            for (Groups g : groupList)
            {
                WSGroupMetaData gm = new WSGroupMetaData(g);
                groupMetadataList.add(gm);
            }
        }

        return groupMetadataList;
    }

    @Transactional(rollbackOn = { Exception.class })
    public void updateAdminUser(WSAdminUserRequest bpoUserRequest) throws Abstract1GroupException
    {
        // private String username;
        // private String email;
        // private String password;
        // private String passwordHash;
        // private String authKey;
        // private Date createdTime;
        // private Date updatedTime;
        // private AdminStatus status;
        // private String id;
        // private String cityId;

        Admin admin = null;
        if (bpoUserRequest.getId() == null)
        {
            admin = new Admin();
        }
        else
        {
            admin = adminDAO.fetchAdminById(bpoUserRequest.getId());
            if (admin == null)
            {
                throw new General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, bpoUserRequest.getId());
            }
        }

        City city = null;
        if (bpoUserRequest.getCityId() != null)
        {
            city = cityDAO.getCityById(bpoUserRequest.getCityId());
        }

        admin.setUserName(this.<String> returnArg1OnNull(admin.getUserName(), bpoUserRequest.getUsername()));
        admin.setEmail(this.<String> returnArg1OnNull(admin.getEmail(), bpoUserRequest.getEmail()));
        admin.setPassword(this.<String> returnArg1OnNull(admin.getPassword(), bpoUserRequest.getPassword()));
        admin.setPasswordHash(this.<String> returnArg1OnNull(admin.getPasswordHash(), bpoUserRequest.getPasswordHash()));
        admin.setAuthKey(this.<String> returnArg1OnNull(admin.getAuthKey(), bpoUserRequest.getAuthKey()));
        admin.setStatus(this.<AdminStatus> returnArg1OnNull(admin.getStatus(), bpoUserRequest.getStatus()));
        admin.setCity(this.<City> returnArg1OnNull(admin.getCity(), city));

        adminDAO.saveAdmin(admin);

        List<String> authAlreadyAssigned = authAssignmentDAO.fetchAuthAssignmentItemNamesByUserId(admin.getId());

        if (bpoUserRequest.getRoles() != null && !bpoUserRequest.getRoles().isEmpty())
        {
            if (authAlreadyAssigned != null && !authAlreadyAssigned.isEmpty())
            {
                authAssignmentDAO.clearAssignmentOfUser(Arrays.asList(admin.getId()));
            }

            Collection<AuthItem> authItems = authItemDAO.fetchAuthItemByNameList(bpoUserRequest.getRoles());

            if (authItems != null && !authItems.isEmpty())
            {
                for (AuthItem authItem : authItems)
                {
                    AuthAssignment a = new AuthAssignment();
                    a.setAuthItem(authItem);
                    a.setUser(admin.getId());

                    authAssignmentDAO.saveAuthAssignmentDAO(a);
                }
            }
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void updateUserCity(List<WSAdminUserRequest> userRequestList)
    {
        Map<String, Map<String, Integer>> userVsAssignCityMap = new HashMap<String, Map<String, Integer>>();
        Map<String, Set<String>> userVsUnAssignCityMap = new HashMap<String, Set<String>>();
        List<String> userIdList = new ArrayList<String>();
        List<String> allUnassignUserList = new ArrayList<String>();

        for (WSAdminUserRequest u : userRequestList)
        {
            String adminId = u.getId();
            String unAssignCityId = u.getUnassignCityId();
            String cityId = u.getCityId();
            Integer priority = u.getPriority();

            Map<String, Integer> assignCityMap = userVsAssignCityMap.get(adminId);
            Set<String> unAssignCitySet = userVsUnAssignCityMap.get(adminId);

            userIdList.add(adminId);

            if (assignCityMap == null)
            {
                assignCityMap = new HashMap<String, Integer>();
                userVsAssignCityMap.put(adminId, assignCityMap);
            }

            if (unAssignCitySet == null)
            {
                unAssignCitySet = new HashSet<String>();
                userVsUnAssignCityMap.put(adminId, unAssignCitySet);
            }

            if (cityId != null)
            {
                userVsAssignCityMap.get(adminId).put(cityId, priority);
            }

            if (u.getCleanCityAssignment())
            {
                allUnassignUserList.add(u.getId());
                break;
            }

            if (unAssignCityId != null)
            {
                userVsUnAssignCityMap.get(adminId).add(unAssignCityId);
            }

        }

        if (!allUnassignUserList.isEmpty())
        {
            waUserCityDAO.cleanByUserIdList(allUnassignUserList);
        }

        for (Entry<String, Set<String>> entry : userVsUnAssignCityMap.entrySet())
        {
            String userId = entry.getKey();
            Set<String> cityIdSet = entry.getValue();
            if (!cityIdSet.isEmpty())
            {
                waUserCityDAO.deleteByUserIdAndCityList(userId, cityIdSet);
            }
        }

        for (Entry<String, Map<String, Integer>> entry : userVsAssignCityMap.entrySet())
        {
            String userId = entry.getKey();
            Map<String, Integer> cityIdMap = entry.getValue();
            if (!cityIdMap.isEmpty())
            {
                waUserCityDAO.saveBulkByUserIdAndCityIdList(userId, cityIdMap);
            }
        }

    }

    @Transactional
    public Map<String, Set<WSMessage>> saveMessages(Map<String, Set<WSMessage>> postMessages) throws Abstract1GroupException
    {
        Map<String, Set<WSMessage>> updatedMessages = new HashMap<String, Set<WSMessage>>();
        if (postMessages != null)
        {
            List<String> messageIds = new ArrayList<String>();
            Set<String> senderPhoneNumbers = new HashSet<String>();
            Map<String, Message> idVsMessageMap = new HashMap<String, Message>();
            Map<String, String> numberVsIdMap = new HashMap<String, String>();
            List<Message> messagesToUpdate = new ArrayList<Message>();
            Date fifteenDayWindow = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(fifteenDayWindow);
            c.add(Calendar.DATE, allowableWindow);
            fifteenDayWindow = c.getTime();
            
            for (Entry<String, Set<WSMessage>> messageEntry : postMessages.entrySet())
            {
                String groupId = messageEntry.getKey();
                Set<WSMessage> wsMessageSet = messageEntry.getValue();
                messageIds.clear();
                senderPhoneNumbers.clear();
                messagesToUpdate.clear();
                Set<WSMessage> wsMessageSetOutsideWindow = new HashSet<WSMessage>();

                for (WSMessage wsMessage : wsMessageSet)
                {
                	String clientSentTimeString = wsMessage.getClientSentTime();
                	Date clientSentTime = (clientSentTimeString != null && !clientSentTimeString.trim().isEmpty() ? new Date(Long.valueOf(clientSentTimeString)) : new Date());
                	
                	if (fifteenDayWindow.after(clientSentTime))
                	{
                		wsMessageSetOutsideWindow.add(wsMessage);
                		continue;
                	}
                	
                    if (!messageIds.contains(wsMessage.getId()) && wsMessage.getId() != null)
                    {
                        if (idVsMessageMap.get(wsMessage.getId()) == null)
                        {
                            messageIds.add(wsMessage.getId());
                        }
                    }

                    if (wsMessage.getCreatedByMobileNumber() != null)
                    {
                        if (numberVsIdMap.get(wsMessage.getCreatedByMobileNumber()) == null)
                        {
                            senderPhoneNumbers.add(wsMessage.getCreatedByMobileNumber());
                        }
                    }
                }

                wsMessageSet.removeAll(wsMessageSetOutsideWindow);
                
                List<Message> messageList = messageDAO.fetchMessagesFromIdsWithException(messageIds, Arrays.asList("id"));
                Set<Account> accountSet = accountDAO.fetchAccountsByPhoneNumbers(senderPhoneNumbers);

                if (messageList != null)
                {
                    for (Message m : messageList)
                    {
                        idVsMessageMap.put(m.getId(), m);
                    }
                }

                if (accountSet != null)
                {
                    for (Account a : accountSet)
                    {
                        numberVsIdMap.put(a.getPrimaryPhoneNumber().getNumber(), a.getId());
                    }
                }

                for (WSMessage wsMessage : wsMessageSet)
                {
                	Message toUpdate = populateMessageForSync(wsMessage, idVsMessageMap, numberVsIdMap, groupId);
                	if (toUpdate != null)
                	{
                		messagesToUpdate.add(toUpdate);
                	}
                }

                try
                {
                	messageService.saveMessagesToDB(messagesToUpdate);
                }
                catch (Exception e)
                {
                	logger.error("Ignoring exception while inserting messages into DB: " + e.getMessage());
                }
                
            }

        }

        return null;
    }

    private Message populateMessageForSync(WSMessage messageRequest, Map<String, Message> existingMessageMap, Map<String, String> numberVsIdMap, String groupId)
    {
        Message m = null;
        if (messageRequest == null)
        {
            return null;
        }

        if (existingMessageMap == null)
        {
            existingMessageMap = new HashMap<String, Message>();
        }

        if (existingMessageMap.get(messageRequest.getId()) != null)
        {
            // do not update
            return null;
        }
        else
        {
            m = new Message();
            if (messageRequest.getId() == null || messageRequest.getId().isEmpty())
            {
                m.setId(Utils.randomUUID());
            }
            else
            {
                m.setId(messageRequest.getId());
            }
        }

        m.setGroupId(groupId);
        m.setBpoMessageStatus(BpoMessageStatus.UNREAD);
        m.setBpoStatusUpdateTime(new Date());

        m.setMessageSource(MessageSourceType.WHATSAPP);
        m.setClientSentTime((messageRequest.getClientSentTime() == null ? new Date() : new Date(Long.valueOf(messageRequest.getClientSentTime()))));
        m.setMessageSentTime(m.getClientSentTime());
        m.setCreatedTime(new Date());

        m.setText(messageRequest.getText());
        m.setMessageType((messageRequest.getType() == null ? MessageType.TEXT : MessageType.parse(messageRequest.getType())));
        m.setMessageStatus(MessageStatus.PENDING_REVIEW);
        m.setSenderPhoneNumber(messageRequest.getCreatedByMobileNumber());
        m.setCreatedBy(numberVsIdMap.get(messageRequest.getCreatedByMobileNumber()));

        return m;
    }

    @Transactional(rollbackOn = { Exception.class })
    public WSGroupMetaData saveGroup(WSGroupMetaDataRequest groupRequest, boolean createNew, boolean releaseAllGroups) throws Abstract1GroupException
    {

        if (releaseAllGroups)
        {
            groupDAO.releaseAllGroups();
            return null;
        }
        
        List<Groups> groupList = groupDAO.fetchGroupsByGroupIds(Arrays.asList(groupRequest.getId()));
        Groups group = null;
        if (groupList != null && !groupList.isEmpty())
        {
        	group = groupList.get(0);
        }
        
        if (createNew)
        {
            if (groupRequest.getId() == null || groupRequest.getId().trim().isEmpty())
            {
                groupRequest.setId(Utils.randomUUID());
            }
            
            group = populateGroup(groupRequest, group, true);
            groupDAO.saveGroup(group);
        }
        else
        {
            group = populateGroup(groupRequest, group, true);
            groupDAO.update(Arrays.asList(group));
        }
        

//        if ((groupList == null || groupList.isEmpty() || groupList.get(0) == null) && !createNew)
//        {
//            throw new General1GroupException(GroupExceptionCode.GROUP_NOT_FOUND, false, groupRequest.getId());
//        }
//        else if ((groupList != null && !groupList.isEmpty() && groupList.get(0) != null))
//        {
//            group = groupList.get(0);
//        }
//        else
//        {
//            if (groupRequest.getId() == null || groupRequest.getId().isEmpty())
//            {
//                groupRequest.setId(Utils.randomUUID());
//            }
//            createNew = true;
//        }
//
//        group = populateGroup(groupRequest, group, createNew);
//
//        groupDAO.saveGroup(group);

        List<GroupMembers> existingGroupMembers = groupMembersDAO.fetchGroupMembersByGroupId(group.getId());
        List<String> memberPhoneNumbers = new ArrayList<String>();
        
        if (existingGroupMembers != null)
        {
        	for (GroupMembers m : existingGroupMembers)
        	{
        		memberPhoneNumbers.add(m.getMobileNumber());
        	}
        }

        if (groupRequest.getParticipants() != null)
        {
            for (WSGroupMembersMetadataRequest participants : groupRequest.getParticipants())
            {

                if (participants.getParticipantId() == null || participants.getParticipantId().trim().isEmpty())
                {
                    if (participants.getMobileNumber() == null || participants.getMobileNumber().trim().isEmpty())
                    {
                        throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
                    }
                    else if (groupRequest.getSkipUserCreation() == null || groupRequest.getSkipUserCreation().equals(Boolean.TRUE))
                    {
                    	if (!memberPhoneNumbers.contains(participants.getMobileNumber()))
                    	{
                    		User user = userService.saveUser(null, null, null, null, null, null, AccountType.ACCOUNT, UserSource.SYNC, GroupSelectionStatus.NOT_SELECTED, SyncStatus.UNDEFINED, participants.getMobileNumber(), null, true);
                    		participants.setParticipantId(user.getId());
                    	}
                    	else
                    	{
                    		continue;
                    	}
                    }
                }

                groupMembersDAO.saveGroupMembers(participants, existingGroupMembers);
            }
        }

        WSGroupMetaData response = new WSGroupMetaData(group);

        return response;
    }

    private Groups populateGroup(WSGroupMetaDataRequest request, Groups group, boolean fillId)
    {

        List<String> participants = new ArrayList<String>();

        if (group == null)
        {
            group = new Groups();
        }

//        if (fillId)
//        {
            group.setId(request.getId());
//        }

        if (request.getParticipants() != null)
        {
            List<String> groupParticipants = group.getParticipants();
            List<WSGroupMembersMetadataRequest> requestParticipants = new ArrayList<WSGroupMembersMetadataRequest>();

            
            if (requestParticipants != null)
            {
	            for (WSGroupMembersMetadataRequest participant : request.getParticipants())
	            {
	                if (groupParticipants == null)
	                {
	                	groupParticipants = new ArrayList<String>();
	                }
	                
	                if (!groupParticipants.contains(participant.getParticipantId()))
	                {
	                	groupParticipants.add(participant.getParticipantId());
	                }
	            }
	            
	            participants.addAll(groupParticipants);
            }

        }

        group.setAssignedTime(this.<Date> returnArg1OnNull(group.getAssignedTime(), request.getAssignedTime()));
        group.setAssignedTo(this.<String> returnArg1OnNull(group.getAssignedTo(), request.getAssignedTo()));
        group.setBpoUpdatedBy(this.<String> returnArg1OnNull(group.getBpoUpdatedBy(), request.getBpoUpdatedBy()));
        group.setBpoUpdatedTime(this.<Date> returnArg1OnNull(group.getBpoUpdatedTime(), request.getBpoUpdatedTime()));
        group.setCityId(this.<String> returnArg1OnNull(group.getCityId(), request.getCityId()));
        group.setCreatedBy(this.<String> returnArg1OnNull(group.getCreatedBy(), request.getCreatedBy()));
        group.setCreatedTime(this.<Date> returnArg1OnNull(group.getCreatedTime(), request.getCreatedTime()));
        group.setCreatorMobileNo(this.<String> returnArg1OnNull(group.getCreatorMobileNo(), request.getCreatorMobileNo()));
        group.setDuplicateCount(this.<Integer> returnArg1OnNull(group.getDuplicateCount(), request.getDuplicateCount()));
        group.setEverSync(this.<Boolean> returnArg1OnNull(group.getEverSync(), request.getEverSync()));
        group.setGroupName(this.<String> returnArg1OnNull(group.getGroupName(), request.getGroupName()));
        group.setInvalidExpiredCount(this.<Integer> returnArg1OnNull(group.getInvalidExpiredCount(), request.getInvalidExpiredCount()));
        group.setLatestMessageTimestamp(this.<Date> returnArg1OnNull(group.getLatestMessageTimestamp(), request.getLatestMessageTimestamp()));
        group.setLocationId(this.<String> returnArg1OnNull(group.getLocationId(), request.getLocationId()));
        group.setMediaId(this.<String> returnArg1OnNull(group.getMediaId(), request.getMediaId()));
        group.setNotes(this.<String> returnArg1OnNull(group.getNotes(), request.getNotes()));
        group.setParticipants(this.<List<String>> returnArg1OnNull(group.getParticipants(), participants));
        group.setPermitSince(this.<Date> returnArg1OnNull(group.getPermitSince(), request.getPermitSince()));
        group.setReleaseTime(this.<Date> returnArg1OnNull(group.getReleaseTime(), request.getReleaseTime()));
        group.setSource(this.<GroupSource> returnArg1OnNull(group.getSource(), request.getSource()));
        group.setStatus(this.<GroupStatus> returnArg1OnNull(group.getStatus(), request.getStatus()));
        group.setStatusUpdateTime(this.<Date> returnArg1OnNull(group.getStatusUpdateTime(), request.getStatusUpdateTime()));
        group.setTotalmsgCount(this.<Integer> returnArg1OnNull(group.getTotalmsgCount(), request.getTotalmsgCount()));
        group.setUnreadFrom(this.<Date> returnArg1OnNull(group.getUnreadFrom(), request.getUnreadFrom()));
        group.setUnreadmsgCount(this.<Integer> returnArg1OnNull(group.getUnreadmsgCount(), request.getUnreadmsgCount()));
        group.setUnreadmsgCountQa(this.<Integer> returnArg1OnNull(group.getUnreadmsgCount(), request.getUnreadmsgCount()));
        group.setUpdatedBy(this.<String> returnArg1OnNull(group.getUpdatedBy(), request.getUpdatedBy()));
        group.setUpdatedTime(this.<Date> returnArg1OnNull(group.getUpdatedTime(), request.getUpdatedTime()));
        group.setValidExpiredCount(this.<Integer> returnArg1OnNull(group.getValidExpiredCount(), request.getValidExpiredCount()));

        return group;
    }

    public <T> T returnArg1OnNull(T arg1, T arg2)
    {
        return arg2 == null ? arg1 : arg2;
    }

    @Transactional(rollbackOn = { Exception.class })
    public boolean updateMessageStatus(List<String> messageIds, MessageStatus messageStatus, BpoMessageStatus bpoMessageStatus, String updatedBy, String bpoUpdatedBy)
    {
        return messageDAO.updateMessageStatus(messageIds, messageStatus, bpoMessageStatus, updatedBy, bpoUpdatedBy);
    }
   
    @Transactional(rollbackOn = { Exception.class })
    public WSMessageResponse fetchMessageWithBroadcastDetails(String messageId) throws Abstract1GroupException
    {
        Message message = messageDAO.fetchMessageFromId(messageId);
        if (message == null)
        {
            throw new General1GroupException(MessageExceptionCode.MESSAGE_NOT_FOUND, false, messageId);
        }
        WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
        String broadcastId = message.getBroadcastId();
        if (broadcastId != null)
        {
            Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);
            if (broadcast != null)
            {
                WSBroadcast wsBroadcast = new WSBroadcast();
                wsBroadcast.setId(broadcastId);
                wsBroadcast.setViewCount(broadcast.getViewCount());
                wsBroadcast.setMessageId(messageId);
                BroadcastStatus broadcastStatus = broadcast.getStatus();
                wsBroadcast.setStatus(broadcastStatus);
                wsBroadcast.setStarred(starredBroadcastDAO.isBroadcastStarred(broadcastId));

                List<BroadcastTag> broadcastTags = broadcastTagDAO.fetchBroadcastTagByBroadcastId(broadcastId, broadcast.getStatus());
                if (broadcastTags != null)
                {
                    for (BroadcastTag broadcastTag : broadcastTags)
                    {
                        WSTag wsTag = new WSTag(broadcastTag);
                        wsBroadcast.addTag(wsTag);
                    }
                }
                wsMessageResponse.setBroadcast(wsBroadcast);
            }
        }
        return wsMessageResponse;
    }

    public List<WSUserCity> fetchUnassignedCityAdmin()
    {
        return waUserCityDAO.fetchUnassignedCityAdmin();
    }

    public WSMessageResponse fetchMessageCountsByGroup(String groupId)
    {
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        long unreadMessageCount = messageDAO.fetchUnreadMessageCount(groupId);
        long totalLiveMessageCount = messageDAO.fetchTotalMessageCount(groupId);
        List<String> groupIds = new ArrayList<String>();
        groupIds.add(groupId);
        List<Groups> groups = groupDAO.fetchGroupsByGroupIds(groupIds);
        long totalArchivedMessageCount = 0;
        long totalMessageCount = 0;
        if (groups != null && !groups.isEmpty())
        {
            Groups group = groups.get(0);
            totalArchivedMessageCount = (group.getInvalidExpiredCount() == null ? 0 : group.getInvalidExpiredCount()) + (group.getValidExpiredCount() == null ? 0 : group.getValidExpiredCount())
                    + (group.getDuplicateCount() == null ? 0 : group.getDuplicateCount());
            totalMessageCount = totalLiveMessageCount + totalArchivedMessageCount;
        }
        wsMessageResponse.setWaGroupId(groupId);
        wsMessageResponse.setTotalMessageCount(totalMessageCount);
        wsMessageResponse.setUnreadMessageCount(unreadMessageCount);

        return wsMessageResponse;
    }

    public WSAuthItemResponse fetchAuthorisationForUser(String userId)
    {
        System.out.println("User id: " + userId);
        List<String> authItemNameList = authAssignmentDAO.fetchAuthAssignmentItemNamesByUserId(userId);
        Collection<AuthItemChild> itemChildCollection = authItemChildDAO.fetchAll();

        System.out.println(authItemNameList);
        System.out.println(itemChildCollection);

        Map<String, Set<String>> parentVsChildMap = new HashMap<String, Set<String>>();
        HashSet<String> masterSet = new HashSet<String>();
        Collection<AuthItem> authItemCollection = new ArrayList<AuthItem>();
        WSAuthItemResponse response = new WSAuthItemResponse();

        if (itemChildCollection != null)
        {
            for (AuthItemChild child : itemChildCollection)
            {
                String parent = child.getParent();
                String childS = child.getChild();

                Set<String> childSet = parentVsChildMap.get(parent);

                if (childSet == null)
                {
                    childSet = new HashSet<String>();
                    childSet.add(childS);
                    parentVsChildMap.put(parent, childSet);
                }
                else
                {
                    childSet.add(childS);
                }
            }
        }

        for (String authItemName : authItemNameList)
        {
            response.addAuthAssignment(userId, authItemName);
            masterSet.addAll(getRecursive(parentVsChildMap, authItemName));
        }

        if (!masterSet.isEmpty())
        {
            authItemCollection = authItemDAO.fetchAuthItemByNameList(masterSet);
        }

        System.out.println(authItemCollection);
        System.out.println(masterSet);
        System.out.println(parentVsChildMap);

        if (authItemCollection != null)
        {
            for (AuthItem item : authItemCollection)
            {
                Set<String> children = parentVsChildMap.get(item.getName());
                response.addAuthItem(item.getName(), item, children);
            }
        }

        return response;

    }

    public void updateGroupsUnreadFrom(SocketEntity socketEntity) throws Exception
    {
        int start = 0;
        int rows = 1000;
        int initStart = start;
        int initRows = rows;
        List<SolrInputDocument> solrInputDocumentsToUpdate = new ArrayList<SolrInputDocument>();
        SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        Map<String, Groups> idVsGroupsMap = new HashMap<String, Groups>();
        do
        {
            idVsGroupsMap = groupDAO.fetchGroups(start, rows);
            if (idVsGroupsMap != null)
            {
                Map<FilterField, Set<String>> filter = new HashMap<FilterField, Set<String>>(1);
                filter.put(FilterField.GROUP_ID, idVsGroupsMap.keySet());
                filter.put(FilterField.BPO_MESSAGE_STATUS, new HashSet<String>(Arrays.asList(BpoMessageStatus.UNREAD.toString())));

                Map<SortField, SortType> sort = new HashMap<SortField, SortType>();
                // sort.put(SortField.CREATED_TIME, SortType.ASCENDING);

                // This query also fetches the unread messages so used for the
                // unread msg count
                List<String> fields = Arrays.asList("id", "groupId", "createdTime", "bpoStatusUpdateTime");
                Map<String, Map<String, Object>> groupIdVsMessageMapForEarliestMessageInGroup = messageDAO.fetchLatestMessageBasedOnGroupQuery(sort, filter, true, SortField.CREATED_TIME,
                        SortType.ASCENDING, "groupId", fields, initStart, initRows);

                // This query also fetches all messages so used for the total
                // count
                filter.remove(FilterField.BPO_MESSAGE_STATUS);
                Map<String, Map<String, Object>> groupIdVsMessageMapForLatestUpdatedMessageInGroup = messageDAO.fetchLatestMessageBasedOnGroupQuery(sort, filter, true,
                        SortField.GROUP_BPO_STATUS_UPDATE_TIME, SortType.DESCENDING, "groupId", fields, initStart, initRows);
                
                Map<String, Map<String, Object>> groupIdVsMessageMapForLatestMessageInGroup = messageDAO.fetchLatestMessageBasedOnGroupQuery(sort, filter, true, SortField.CREATED_TIME,
                        SortType.DESCENDING, "groupId", fields, initStart, initRows);

                logger.info("Groups with earliest message count : " + groupIdVsMessageMapForEarliestMessageInGroup.size());
                logger.info("Groups with latest status update : " + groupIdVsMessageMapForLatestUpdatedMessageInGroup.size());
                for (String groupId : idVsGroupsMap.keySet())
                {

                    Map<String, Object> earliestUnreadMessage = groupIdVsMessageMapForEarliestMessageInGroup.get(groupId);
                    Date unreadFrom = null;
                    if (earliestUnreadMessage != null)
                    {
                        unreadFrom = earliestUnreadMessage.get("createdTime") == null ? null : (Date) earliestUnreadMessage.get("createdTime");
                    }

                    Map<String, Object> latestBPOStatusUpdatedMessage = groupIdVsMessageMapForLatestUpdatedMessageInGroup.get(groupId);
                    Date latestBPOStatusUpdateTime = null;
                    if (latestBPOStatusUpdatedMessage != null)
                    {
                        latestBPOStatusUpdateTime = latestBPOStatusUpdatedMessage.get("bpoStatusUpdateTime") == null ? null : (Date) latestBPOStatusUpdatedMessage.get("bpoStatusUpdateTime");
                    }

                    if (latestBPOStatusUpdateTime != null)
                    {
                        if (unreadFrom != null && latestBPOStatusUpdateTime.compareTo(unreadFrom) > 0)
                        {
                            unreadFrom = latestBPOStatusUpdateTime;
                        }
                    }
                    
                    Map<String, Object> latestMessageOfGroup = groupIdVsMessageMapForLatestMessageInGroup.get(groupId);
                    Date latestMessageTime = null;
                    if (latestMessageOfGroup != null)
                    {
                    	latestMessageTime = latestMessageOfGroup.get("createdTime") == null ? null : (Date) latestMessageOfGroup.get("createdTime");
                    }
                    
			if (unreadFrom != null)
                    {
                        SolrInputDocument solrInputDoc = new SolrInputDocument();
                        Map<String, Object> updateField = new HashMap<String, Object>(1);
                        Map<String, Object> updateFieldUnreadmsgCount = new HashMap<String, Object>(1);
                        Map<String, Object> updateFieldTotalmsgCount = new HashMap<String, Object>(1);
                        Map<String, Object> updateFieldLatestMessageTimestamp = new HashMap<String, Object>(1);

                        updateField.put("set", unreadFrom);

                        updateFieldUnreadmsgCount.put("set", earliestUnreadMessage == null ? 0 : earliestUnreadMessage.get("numFound"));
                        updateFieldTotalmsgCount.put("set", latestBPOStatusUpdatedMessage == null ? 0 : latestBPOStatusUpdatedMessage.get("numFound"));
                        updateFieldLatestMessageTimestamp.put("set", latestMessageTime == null ? null : latestMessageTime);

                        solrInputDoc.addField("id", groupId);
                        solrInputDoc.addField("unreadFrom", updateField);
                        solrInputDoc.addField("unreadmsgCount", updateFieldUnreadmsgCount);
                        solrInputDoc.addField("totalmsgCount", updateFieldTotalmsgCount);
                        if (latestMessageTime != null)
                        {
                        	solrInputDoc.addField("latestMessageTimestamp", updateFieldLatestMessageTimestamp);
                        }

                        solrInputDocumentsToUpdate.add(solrInputDoc);
                    }
                }
            }

            start = start + rows;

            // solrInputDocumentsToUpdate.clear();

        }
        while (idVsGroupsMap != null && !idVsGroupsMap.isEmpty());

        if (!solrInputDocumentsToUpdate.isEmpty())
        {
            c.add(solrInputDocumentsToUpdate);
//            c.commit();
        }
        logger.info("Updated Docs : " + solrInputDocumentsToUpdate.size());
    }

    public void updateMessageMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, -15);
        SimpleDateFormat out = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String dateString = out.format(cal.getTime());
        SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        List<Message> messageList = null;
        int start = 0;
        int rows = 1000;
        do
        {
            try
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "entityType:message AND clientSentTime:[* TO " + dateString + "] AND bpoMessageStatus_s:" + BpoMessageStatus.UNREAD.name() + " AND messageSource_s:"
                        + MessageSourceType.WHATSAPP.name());
                params.put("fl", "id");
                params.put("start", "" + start);
                params.put("rows", "" + rows);

                QueryResponse response = c.query(new MapSolrParams(params));
                messageList = response.getBeans(Message.class);

                List<SolrInputDocument> updateList = new ArrayList<SolrInputDocument>();

                for (Message m : messageList)
                {
                    Map<String, Object> updatedFields = new HashMap<String, Object>();
                    updatedFields.put("set", BpoMessageStatus.EXPIRED.name());
                    SolrInputDocument solrInputDocument = new SolrInputDocument();
                    solrInputDocument.addField("id", m.getId());
                    solrInputDocument.addField("bpoMessageStatus_s", updatedFields);

                    updateList.add(solrInputDocument);
                }

                logger.info(params + "");

                if (!updateList.isEmpty())
                {
                    c.add(updateList);
                }

//                c.commit();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            // start = start + rows;

        }
        while (messageList != null && !messageList.isEmpty());
    }

    public void markEmptyGroups()
    {
        SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        int start = 0;
        int rows = 1000;
        List<Groups> groupsList = null;

        do
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put("q", "entityType:groups AND everSync:true AND status_s:NEW");
            params.put("rows", rows + "");
            params.put("start", start + "");
            params.put("fl", "id");
            try
            {
                QueryResponse response = c.query(new org.apache.solr.common.params.MapSolrParams(params));
                SolrDocumentList solrDocumentList = response.getResults();

                if (solrDocumentList != null)
                {
                    List<String> groupIds = new ArrayList<String>();
                    List<SolrInputDocument> updatedDocumentList = new ArrayList<SolrInputDocument>();
                    for (SolrDocument solrDoc : solrDocumentList)
                    {
                        String groupId = solrDoc.get("id") == null ? null : solrDoc.get("id").toString();
                        if (groupId != null)
                        {
                            groupIds.add(groupId);
                        }
                    }

                    if (groupIds != null && !groupIds.isEmpty())
                    {
                        SolrQuery messageSolrQuery = new SolrQuery("entityType:messages AND -messageStatus_s:" + MessageStatus.EXPIRED.name());
                        messageSolrQuery.addField("id");

                        messageSolrQuery.setFilterQueries("groupId:(" + Utils.getCollectionAsString(groupIds, " ") + ")");

                        messageSolrQuery.setFacet(true);
                        messageSolrQuery.addFacetField("groupId");

                        QueryResponse messageResponse = c.query(messageSolrQuery);

                        List<Count> countList = messageResponse.getFacetField("groupId").getValues();

                        if (countList != null)
                        {
                            for (Count count : countList)
                            {
                                String groupId = count.getName();
                                Long groupCount = count.getCount();

                                if (groupCount == null || groupCount < 1)
                                {
                                    Map<String, String> statusUpdateField = new HashMap<String, String>();
                                    statusUpdateField.put("set", GroupStatus.INSUFFICIENT_MESSAGES.name());
                                    Map<String, Date> statusUpdateTime = new HashMap<String, Date>();
                                    statusUpdateTime.put("set", new Date());

                                    SolrInputDocument solrInputDocument = new SolrInputDocument();
                                    solrInputDocument.addField("status_s", statusUpdateField);
                                    solrInputDocument.addField("statusUpdateTime", statusUpdateTime);
                                    solrInputDocument.addField("id", groupId);

                                    updatedDocumentList.add(solrInputDocument);
                                }

                            }
                        }
                    }

                    if (!updatedDocumentList.isEmpty())
                    {
                        c.add(updatedDocumentList);
//                        c.commit();
                    }

                }

            }
            catch (Exception e)
            {
                logger.info("Exception while marking groups as empty.");
            }

            start = start + rows;

        }
        while (groupsList != null && !groupsList.isEmpty());

    }
    
    public void archiveMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
    	try
	    {
    		String requestTimePeriod = socketEntity.getQueryParam(QueryParamType.ARCHIVE_TIME_PERIOD);
    		String requestLimit = socketEntity.getQueryParam(QueryParamType.LIMIT);
    		SolrClient c = SolrServerFactory.getInstance().getQueryClient(SolrCollectionType.ONEGROUP);
	    	Map<String, String> params = new HashMap<String, String>();
	    	List<String> messageIds = new ArrayList<String>();
	    	List<WaMessagesArchive> archiveMessageList = new ArrayList<WaMessagesArchive>();
	    	int start = 0;
	    	int rows = 100;
	    	String timePeriod = "* TO NOW-15DAYS";
	    	timePeriod = (requestTimePeriod != null && !requestTimePeriod.trim().isEmpty()) ? requestTimePeriod : timePeriod;
	    	rows = (requestLimit != null && !requestLimit.trim().isEmpty()) ? Integer.parseInt(requestLimit) : rows;
	    	
	    	do
	    	{
	    		messageIds.clear();
	    		archiveMessageList.clear();
		    	params.put("q", "entityType:message AND messageSource_s:WHATSAPP AND messageType_s:TEXT AND createdTime:[" + timePeriod + "]");
	//	    	params.put("fl", "id");
		    	params.put("start", start+"");
		    	params.put("rows", rows+"");
		    	
		    	QueryResponse response = c.query(new MapSolrParams(params));
		    	List<Message> messageList = response.getBeans(Message.class);
		    	
		    	
		    	for (Message m : messageList)
		    	{
		    		messageIds.add(m.getId());
		    		archiveMessageList.add(new WaMessagesArchive(m));
		    	}
		    	
		    	if (archiveMessageList != null && !archiveMessageList.isEmpty())
		    	{
		    		try
		    		{
		    			waArchiveMessageService.saveMessagesToDB(archiveMessageList);
		    			c.deleteById(messageIds);
		    			c.commit(false, false, true);
		    		}
		    		catch (Exception e)
		    		{
		    			e.printStackTrace();
		    		}
		    	}
	//	    	System.out.println(messageList);
	//	    	start = start + rows;
	    	} while (messageIds != null && !messageIds.isEmpty());
	    	
	    	c.close();
	    	
    	}
    	catch (Exception e)
    	{
    		throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
    	}
    	
    }

    private Set<String> getRecursive(Map<String, Set<String>> parentMap, String toSearch)
    {
        Set<String> childList = parentMap.get(toSearch);
        Set<String> childSet = new HashSet<String>();
        if (childList != null)
        {
            childSet = new HashSet<String>(childList);
            childSet.add(toSearch);
            for (String child : childList)
            {
                Set<String> moreChildren = getRecursive(parentMap, child);
                childSet.add(child);
                if (moreChildren != null)
                {
                    childSet.addAll(moreChildren);
                }
            }
        }

        return childSet;
    }
}
