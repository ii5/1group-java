package one.group.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.EntityType;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.HintType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.dao.GroupMembersDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.LocationDAO;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSAccountUtilityDataHolder;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSChatCursor;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadList;
import one.group.entities.api.response.v2.WSChatThreadListMetaDataResult;
import one.group.entities.api.response.v2.WSChatThreadReference;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSPaginationData;
import one.group.entities.cache.ChatKey;
import one.group.entities.jpa.City;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.Location;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AuthorizationExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GroupExceptionCode;
import one.group.exceptions.codes.LocationExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ChatMessageService;
import one.group.services.CityService;
import one.group.services.GroupsService;
import one.group.services.SearchService;
import one.group.services.SubscriptionService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;

import org.apache.solr.common.SolrInputDocument;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class GroupsServiceImpl implements GroupsService
{
    private final static Logger logger = LoggerFactory.getLogger(GroupsServiceImpl.class);
    private GroupsDAO groupsDAO;
    private GroupMembersDAO groupMembersDAO;
    private ChatMessageService chatMessageService;
    private AccountService accountService;
    private AccountRelationService accountRelationService;
    private BroadcastService broadcastService;
    private LocationDAO locationDAO;
    private SearchService searchService;
    private CityService cityService;
    private SubscriptionService subscriptionService;
    private int totalMessagePerPage = 30;
    private int searchThreshold = 500;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    public int getSearchThreshold()
    {
        return searchThreshold;
    }

    public void setSearchThreshold(int searchThreshold)
    {
        this.searchThreshold = searchThreshold;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public int getTotalMessagePerPage()
    {
        return totalMessagePerPage;
    }

    public void setTotalMessagePerPage(int totalMessagePerPage)
    {
        this.totalMessagePerPage = totalMessagePerPage;
    }

    public GroupsDAO getGroupsDAO()
    {
        return groupsDAO;
    }

    public void setGroupsDAO(GroupsDAO groupsDAO)
    {
        this.groupsDAO = groupsDAO;
    }

    public GroupMembersDAO getGroupMembersDAO()
    {
        return groupMembersDAO;
    }

    public void setGroupMembersDAO(GroupMembersDAO groupMembersDAO)
    {
        this.groupMembersDAO = groupMembersDAO;
    }

    public ChatMessageService getChatMessageService()
    {
        return chatMessageService;
    }

    public void setChatMessageService(ChatMessageService chatMessageService)
    {
        this.chatMessageService = chatMessageService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public BroadcastService getBroadcastService()
    {
        return broadcastService;
    }

    public void setBroadcastService(BroadcastService broadcastService)
    {
        this.broadcastService = broadcastService;
    }

    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    public SearchService getSearchService()
    {
        return searchService;
    }

    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }

    public CityService getCityService()
    {
        return cityService;
    }

    public void setCityService(CityService cityService)
    {
        this.cityService = cityService;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "GroupService-createGroupIfNotExistAndAddMembers")
    public synchronized Groups createGroupIfNotExistAndAddMembers(String accountId, String memberOneId, String memberTwoId, GroupSource groupSource)
    {
        List<String> memberIds = new ArrayList<String>();
        memberIds.add(memberOneId);
        memberIds.add(memberTwoId);

        Groups groups = new Groups();

        logger.info("start: fetching group info[" + memberIds + "] groupSource[" + groupSource + "]");
        long start = Utils.getSystemTime();
        String groupId = null;
        try
        {
            if (groupSource.equals(GroupSource.DIRECT))
            {
                groupId = fetchGroupIdFromParticipants(memberOneId, memberTwoId, groupSource);
            }
            else
            {
                groupId = groupMembersDAO.fetchGroupIdFromParticipantIdAndSource(accountId, groupSource);
            }

        }
        catch (NoResultException nre)
        {
            logger.info("Its ok no group id found for [" + memberOneId + ":" + memberTwoId + ":" + groupSource + "]");
        }
        // List<Groups> groupsList =
        // groupsDAO.fetchGroupIdFromMembers(memberIds, groupSource);

        logger.info("end: fetching group info[" + memberIds + "] groupSource[" + groupSource + "] groupId[" + groupId + "] time[" + (Utils.getSystemTime() - start) + "]");

        // create group if not exist
        if (groupId == null)
        {
            String chatKey = ChatKey.GROUP_ID.getKey();

            groupId = Utils.generateIdForGroup(memberOneId, memberTwoId, chatKey, groupSource.toString());
            logger.info("creating group[" + groupId + "]");

            // To form group name
            String groupName = groupSource.name() + ":" + groupId;

            groups.setId(groupId);
            groups.setGroupName(groupName);
            groups.setEverSync(false);
            groups.setSource(groupSource);
            groups.setTotalmsgCount(0);
            groups.setCreatedBy(accountId);
            groups.setCreatedTime(new Date());
            groups.setParticipants(memberIds);
            groups.setStatus(GroupStatus.ACTIVE);

            List<String> participantsStatus = new ArrayList<String>();
            for (String participantId : memberIds)
            {
                participantsStatus.add(participantId + ":" + GroupStatus.ACTIVE.name());
            }
            groups.setParticipantsStatus(participantsStatus);

            groupsDAO.saveGroup(groups);

            // save members
            List<GroupMembers> groupMembers = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groupId);
                groupMember.setActiveMember(true);
                groupMember.setSource(groupSource);
                groupMembers.add(groupMember);
            }
            groupMembersDAO.saveGroupMembers(groupMembers);
            logger.info("group created[" + memberIds + "] groupSource[" + groupSource + "] id[" + groups.getId() + "]");
        }
        else
        {
            groups = fetchGroupDetailsFromId(groupId);
            logger.info("group exist[" + memberIds + "] groupSource[" + groupSource + "] id[" + groups.getId() + "]");
        }

        return groups;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "GroupService-formGroupObject")
    public Groups formGroupObject(String chatThreadId, String accountId, List<String> memberIds, GroupSource groupSource)
    {
        String chatKey = ChatKey.GROUP_ID.getKey();
        String memberIdOne = memberIds.get(0);
        String memberIdTwo = memberIds.get(1);

        String groupId = Utils.generateIdForGroup(memberIdOne, memberIdTwo, chatKey, groupSource.toString());
        if (chatThreadId != null && !chatThreadId.isEmpty())
        {
            groupId = chatThreadId;
        }

        Groups groups = new Groups();

        // To form group name
        String groupName = groupSource.name() + ":" + groupId;

        groups.setId(groupId);
        groups.setGroupName(groupName);
        groups.setEverSync(false);
        groups.setSource(groupSource);
        groups.setTotalmsgCount(0);
        groups.setCreatedBy(accountId);
        groups.setCreatedTime(new Date());
        groups.setParticipants(memberIds);
        groups.setStatus(GroupStatus.ACTIVE);

        List<String> participantsStatus = new ArrayList<String>();
        for (String participantId : memberIds)
        {
            participantsStatus.add(participantId + ":" + GroupStatus.ACTIVE.name());
        }
        groups.setParticipantsStatus(participantsStatus);

        return groups;
    }

    @Profiled(tag = "GroupService-updateGroups")
    public void updateGroups(Groups groups)
    {
        groupsDAO.saveGroup(groups);
    }

    @Profiled(tag = "GroupService-updateReadAndReceivedIndex")
    public int updateReadAndReceivedIndex(String groupId, String participantId, int readIndex, int receivedIndex, String receivedMessageId, String readMessageId)
    {
        return groupMembersDAO.updateReadAndReceivedIndex(groupId, participantId, receivedIndex, readIndex, receivedMessageId, readMessageId);
    }

    @Profiled(tag = "GroupService-fetchCursorMapForParticipants")
    public Map<String, Map<String, WSChatThreadCursor>> fetchCursorMapForParticipants(List<String> participantIds)
    {
        List<GroupMembers> groupMembers = groupMembersDAO.fetchMembersByParticipants(participantIds);
        Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = new HashMap<String, Map<String, WSChatThreadCursor>>();

        if (groupMembers != null)
        {
            for (GroupMembers groupMember : groupMembers)
            {
                WSChatThreadCursor wsGroupMember = new WSChatThreadCursor(groupMember);
                Map<String, WSChatThreadCursor> map = new HashMap<String, WSChatThreadCursor>();
                if (accountVsCursorMap.get(groupMember.getGroupId()) != null)
                {
                    map = accountVsCursorMap.get(groupMember.getGroupId());
                }
                map.put(groupMember.getParticipantId(), wsGroupMember);
                accountVsCursorMap.put(groupMember.getGroupId(), map);
            }
        }
        return accountVsCursorMap;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "GroupService-createDefaultGroupsForAccountAndReturnMembers")
    public Map<List<Groups>, List<GroupMembers>> createDefaultGroupsForAccountAndReturnMembers(String accountId, String cityId, String fullname, String mobileNumber)
    {
        String postFixName = ":" + fullname + ":" + mobileNumber;
        List<GroupMembers> groupMembersList = new ArrayList<GroupMembers>();
        List<Groups> groupsList = new ArrayList<Groups>();

        Map<List<Groups>, List<GroupMembers>> groupVsMembersMap = new HashMap<List<Groups>, List<GroupMembers>>();
        // List<Groups> existingGroups =
        // groupsDAO.fetchDefaultGroupsByAccountId(accountId);
        // if (existingGroups != null && existingGroups.size() == 0)
        {
            List<String> memberIds = new ArrayList<String>();
            memberIds.add(accountId);
            memberIds.add(accountId);

            List<String> participantsStatus = new ArrayList<String>();
            List<String> participantIdList = new ArrayList<String>();
            for (String participantId : memberIds)
            {
                participantsStatus.add(participantId + ":" + GroupStatus.ACTIVE.name());
                participantIdList.add(participantId);
            }

            // create only one entry in group member table
            memberIds.remove(accountId);

            String chatKey = ChatKey.GROUP_ID.getKey();
            // create 1group

            Groups groups = new Groups();
            String oneGroupGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ONEGROUP.toString());
            groups.setId(oneGroupGroupId);
            groups.setCreatedBy(accountId);
            groups.setCreatedTime(new Date());
            groups.setGroupName("1Group" + postFixName);
            groups.setEverSync(false);
            groups.setSource(GroupSource.ONEGROUP);
            groups.setTotalmsgCount(0);
            groups.setParticipants(participantIdList);
            groups.setParticipantsStatus(participantsStatus);
            groups.setStatus(GroupStatus.ACTIVE);
            groups.setCityId(cityId);
            groupsList.add(groups);

            // save members for
            List<GroupMembers> groupMembers = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groups.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.ONEGROUP);
                groupMembersList.add(groupMember);
            }
            // groupMembersList.add
            // groupMembersDAO.saveGroupMembers(groupMembers);

            // create me
            Groups groupsMe = new Groups();
            String meGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ME.toString());
            groupsMe.setId(meGroupId);
            groupsMe.setCreatedBy(accountId);
            groupsMe.setCreatedTime(new Date());
            groupsMe.setGroupName("Me" + postFixName);
            groupsMe.setEverSync(true);
            groupsMe.setSource(GroupSource.ME);
            groupsMe.setTotalmsgCount(0);
            groupsMe.setParticipants(participantIdList);
            groupsMe.setParticipantsStatus(participantsStatus);
            groupsMe.setStatus(GroupStatus.ACTIVE);
            groupsMe.setCityId(cityId);
            groupsList.add(groupsMe);

            List<GroupMembers> groupMembersMe = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groupsMe.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.ME);
                groupMembersList.add(groupMember);
            }
            // groupMembersDAO.saveGroupMembers(groupMembersMe);

            // create starred
            Groups groupsStarred = new Groups();
            String starredGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.STARRED.toString());
            groupsStarred.setId(starredGroupId);
            groupsStarred.setCreatedBy(accountId);
            groupsStarred.setCreatedTime(new Date());
            groupsStarred.setGroupName("Starred" + postFixName);
            groupsStarred.setEverSync(false);
            groupsStarred.setSource(GroupSource.STARRED);
            groupsStarred.setTotalmsgCount(0);
            groupsStarred.setParticipants(participantIdList);
            groupsStarred.setParticipantsStatus(participantsStatus);
            groupsStarred.setStatus(GroupStatus.ACTIVE);
            groupsStarred.setCityId(cityId);
            groupsList.add(groupsStarred);

            List<GroupMembers> groupMembersStarred = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groupsStarred.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.STARRED);
                groupMembersList.add(groupMember);
            }

            // groupsDAO.saveGroupsBulk(groupsList);
            groupVsMembersMap.put(groupsList, groupMembersList);
        }

        return groupVsMembersMap;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "GroupService-createDefaultGroupsForAccount")
    public void createDefaultGroupsForAccount(String accountId, String cityId, String fullname, String mobileNumber)
    {
        String postFixName = ":" + fullname + ":" + mobileNumber;

        List<Groups> existingGroups = groupsDAO.fetchDefaultGroupsByAccountId(accountId);
        if (existingGroups != null && existingGroups.size() == 0)
        {
            List<String> memberIds = new ArrayList<String>();
            memberIds.add(accountId);
            memberIds.add(accountId);

            List<String> participantsStatus = new ArrayList<String>();
            List<String> participantIdList = new ArrayList<String>();
            for (String participantId : memberIds)
            {
                participantsStatus.add(participantId + ":" + GroupStatus.ACTIVE.name());
                participantIdList.add(participantId);
            }

            // create only one entry in group member table
            memberIds.remove(accountId);

            String chatKey = ChatKey.GROUP_ID.getKey();
            // create 1group

            Groups groups = new Groups();
            String oneGroupGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ONEGROUP.toString());
            groups.setId(oneGroupGroupId);
            groups.setCreatedBy(accountId);
            groups.setCreatedTime(new Date());
            groups.setGroupName("1Group" + postFixName);
            groups.setEverSync(false);
            groups.setSource(GroupSource.ONEGROUP);
            groups.setTotalmsgCount(0);
            groups.setParticipants(participantIdList);
            groups.setParticipantsStatus(participantsStatus);
            groups.setStatus(GroupStatus.ACTIVE);
            groups.setCityId(cityId);
            groupsDAO.saveGroup(groups);

            // save members for
            List<GroupMembers> groupMembers = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groups.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.ONEGROUP);
                groupMembers.add(groupMember);
            }
            groupMembersDAO.saveGroupMembers(groupMembers);

            // create me
            Groups groupsMe = new Groups();
            String meGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.ME.toString());
            groupsMe.setId(meGroupId);
            groupsMe.setCreatedBy(accountId);
            groupsMe.setCreatedTime(new Date());
            groupsMe.setGroupName("Me" + postFixName);
            groupsMe.setEverSync(true);
            groupsMe.setSource(GroupSource.ME);
            groupsMe.setTotalmsgCount(0);
            groupsMe.setParticipants(participantIdList);
            groupsMe.setParticipantsStatus(participantsStatus);
            groupsMe.setStatus(GroupStatus.ACTIVE);
            groupsMe.setCityId(cityId);
            groupsDAO.saveGroup(groupsMe);

            List<GroupMembers> groupMembersMe = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groupsMe.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.ME);
                groupMembersMe.add(groupMember);
            }
            groupMembersDAO.saveGroupMembers(groupMembersMe);

            // create starred
            Groups groupsStarred = new Groups();
            String starredGroupId = Utils.generateIdForGroup(accountId, accountId, chatKey, GroupSource.STARRED.toString());
            groupsStarred.setId(starredGroupId);
            groupsStarred.setCreatedBy(accountId);
            groupsStarred.setCreatedTime(new Date());
            groupsStarred.setGroupName("Starred" + postFixName);
            groupsStarred.setEverSync(false);
            groupsStarred.setSource(GroupSource.STARRED);
            groupsStarred.setTotalmsgCount(0);
            groupsStarred.setParticipants(participantIdList);
            groupsStarred.setParticipantsStatus(participantsStatus);
            groupsStarred.setStatus(GroupStatus.ACTIVE);
            groupsStarred.setCityId(cityId);
            groupsDAO.saveGroup(groupsStarred);

            List<GroupMembers> groupMembersStarred = new ArrayList<GroupMembers>();
            for (String memberId : memberIds)
            {
                GroupMembers groupMember = new GroupMembers();
                groupMember.setParticipantId(memberId);
                groupMember.setReadIndex(-1);
                groupMember.setReceivedIndex(-1);
                groupMember.setGroupId(groupsStarred.getId());
                groupMember.setActiveMember(true);
                groupMember.setSource(GroupSource.STARRED);
                groupMembersStarred.add(groupMember);
            }
            groupMembersDAO.saveGroupMembers(groupMembersStarred);
        }
    }

    @Profiled(tag = "GroupService-fetchAccountMapData")
    public WSAccountUtilityDataHolder fetchAccountMapData(String accountId, List<String> accountIdList)
    {

        Set<String> accountIdsSet = new HashSet<String>(accountIdList);
        accountIdsSet.remove(null);

        Map<String, WSAccount> idVsAccountMap = new HashMap<String, WSAccount>();
        Map<String, WSAccountRelations> otherAccountVsRelationMap = new HashMap<String, WSAccountRelations>();
        Map<String, Long> accountVsBroadcastCountMap = new HashMap<String, Long>();
        Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = new HashMap<String, Map<String, WSChatThreadCursor>>();
        Map<String, String> starredBroadcastMap = new HashMap<String, String>();

        if (!accountIdList.isEmpty())
        {
            accountIdList = new ArrayList<String>(accountIdsSet);

            idVsAccountMap = accountService.fetchAccountDetailsInBulk(accountIdsSet);
            otherAccountVsRelationMap = accountRelationService.fetchAccountRelationsAsMap(accountId, accountIdList);
            accountVsBroadcastCountMap = broadcastService.fetchAccountAndBroadcastCountMapFromSolr(accountIdList);
            accountVsCursorMap = fetchCursorMapForParticipants(accountIdList);
        }

        List<String> starredBroadcastIdList = broadcastService.fetchStarredBroadcastoIdsOfAccount(accountId);
        if (starredBroadcastIdList != null && !starredBroadcastIdList.isEmpty())
        {
            for (String broadcastId : starredBroadcastIdList)
            {
                starredBroadcastMap.put(broadcastId, broadcastId);
            }
        }

        WSAccountUtilityDataHolder dataHolder = new WSAccountUtilityDataHolder();
        dataHolder.setIdVsAccountMap(idVsAccountMap);
        dataHolder.setAccountVsBroadcastCountMap(accountVsBroadcastCountMap);
        dataHolder.setAccountVsCursorMap(accountVsCursorMap);
        dataHolder.setOtherAccountVsRelationMap(otherAccountVsRelationMap);
        dataHolder.setStarredBroadcastMap(starredBroadcastMap);

        return dataHolder;

    }

    @Profiled(tag = "GroupService-formWSMessageResponse")
    public WSMessageResponse formWSMessageResponse(WSMessageResponse wsMessageResponse, WSAccountUtilityDataHolder dataHolder)
    {
        if (wsMessageResponse.getChatThreadId() != null)
        {
            String groupId = wsMessageResponse.getChatThreadId();
            String createdById = wsMessageResponse.getCreatedById();
            String toAccountId = wsMessageResponse.getToAccountId();

            Map<String, WSAccount> idVsAccountMap = dataHolder.getIdVsAccountMap();
            Map<String, WSAccountRelations> otherAccountVsRelationMap = dataHolder.getOtherAccountVsRelationMap();
            Map<String, Long> accountVsBroadcastCountMap = dataHolder.getAccountVsBroadcastCountMap();
            Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = dataHolder.getAccountVsCursorMap();
            Map<String, String> starredBroadcastMap = dataHolder.getStarredBroadcastMap();
            Map<String, WSBroadcast> idVsBroadcastMap = dataHolder.getIdVsBroadcastMap();

            WSChatThreadCursor defaultCursor = new WSChatThreadCursor();
            defaultCursor.setReadIndex(-1);
            defaultCursor.setReceivedIndex(-1);
            defaultCursor.setReceivedMessageId(wsMessageResponse.getId());
            defaultCursor.setReadMessageId(wsMessageResponse.getId());

            WSAccount createdBy = (idVsAccountMap.get(createdById) == null) ? new WSAccount() : idVsAccountMap.get(createdById);
            WSAccountRelations createdByRelation = otherAccountVsRelationMap.get(createdById);
            if (createdByRelation != null)
            {
                createdBy.setIsBlocked(createdByRelation.getIsBlocked());
                createdBy.setIsContact(createdByRelation.getIsContact());
                createdBy.setScore(createdByRelation.getScore());
            }
            if (accountVsBroadcastCountMap.get(createdById) != null)
            {
                createdBy.setBroadcastCount(accountVsBroadcastCountMap.get(createdById));
            }

            // set default cursor if data not exist for account relation
            createdBy.setCursors(defaultCursor);
            if (accountVsCursorMap.get(groupId) != null)
            {
                if (accountVsCursorMap.get(groupId).get(createdById) != null)
                {
                    WSChatThreadCursor cursorCreatedBy = accountVsCursorMap.get(groupId).get(createdById);
                    if (cursorCreatedBy != null)
                    {
                        createdBy.setCursors(cursorCreatedBy);
                    }
                }
            }
            wsMessageResponse.setCreatedBy(createdBy);

            WSAccount toAccount = idVsAccountMap.get(toAccountId);
            if (toAccount != null)
            {
                WSAccountRelations toAccountRelation = otherAccountVsRelationMap.get(toAccountId);
                if (toAccountRelation != null)
                {
                    toAccount.setIsBlocked(toAccountRelation.getIsBlocked());
                    toAccount.setIsContact(toAccountRelation.getIsContact());
                    toAccount.setScore(toAccountRelation.getScore());
                }
                if (accountVsBroadcastCountMap.get(toAccountId) != null)
                {
                    toAccount.setBroadcastCount(accountVsBroadcastCountMap.get(toAccountId));
                }

                // set default cursor if data not exist for account relation
                toAccount.setCursors(defaultCursor);
                if (accountVsCursorMap.get(groupId) != null)
                {
                    if (accountVsCursorMap.get(groupId).get(toAccountId) != null)
                    {
                        WSChatThreadCursor cursorToAccount = accountVsCursorMap.get(groupId).get(toAccountId);
                        if (cursorToAccount != null)
                        {
                            toAccount.setCursors(cursorToAccount);
                        }
                    }
                }
                wsMessageResponse.setToAccount(toAccount);
            }
            if (wsMessageResponse.getType().equals(MessageType.BROADCAST))
            {
                WSBroadcast wsBroadcast = new WSBroadcast();
                if (idVsBroadcastMap.get(wsMessageResponse.getBroadcastId()) == null)
                {
                    wsBroadcast = broadcastService.fetchBroadcastById(wsMessageResponse.getBroadcastId());
                }
                else
                {
                    wsBroadcast = idVsBroadcastMap.get(wsMessageResponse.getBroadcastId());
                }
                String isStarred = starredBroadcastMap.get(wsMessageResponse.getBroadcastId());
                if (isStarred != null)
                {
                    wsBroadcast.setStarred(true);
                }
                wsMessageResponse.setBroadcast(wsBroadcast);

            }
            return wsMessageResponse;
        }
        return new WSMessageResponse();
    }

    private Map<String, Integer> fetchMapOfGroupVsReadMessageIndexOfAccount(String accountId, List<String> groupIdList)
    {
        Map<String, Integer> groupIdVsIndex = new HashMap<String, Integer>();
        List<GroupMembers> groupMembersList = groupMembersDAO.fetchGroupMembersByAccountIdAndGroupId(accountId, groupIdList);
        List<String> messageIdList = new ArrayList<String>();
        for (GroupMembers gm : groupMembersList)
        {
            if (gm.getReadMessageId() != null)
            {
                messageIdList.add(gm.getReadMessageId());
            }
        }
        if (!messageIdList.isEmpty())
        {
            List<Message> messageList = chatMessageService.fetchMessageDetails(messageIdList);
            for (Message message : messageList)
            {
                groupIdVsIndex.put(message.getGroupId(), message.getIndex());
            }
        }
        return groupIdVsIndex;
    }

    @Profiled(tag = "GroupService-fetchMetaDataChatThread")
    public WSChatThreadListMetaDataResult fetchMetaDataChatThread(String accountId) throws JsonMappingException, JsonGenerationException, IOException, General1GroupException
    {
        List<GroupSource> list = new ArrayList<GroupSource>();
        list.add(GroupSource.DIRECT);
        list.add(GroupSource.ME);
        list.add(GroupSource.ONEGROUP);
        list.add(GroupSource.STARRED);

        long start = System.currentTimeMillis();
        List<String> groupIdList = groupMembersDAO.fetchGroupIdListByAccountId(accountId);
        logger.info("fetchGroupIdListByAccountId[" + (System.currentTimeMillis() - start) + "]");

        start = System.currentTimeMillis();
        List<Groups> groupsList = groupsDAO.fetchGroupsByGroupIdsAndSource(groupIdList, list);
        logger.info("fetchGroupsByGroupIdsAndSource[" + (System.currentTimeMillis() - start) + "]");
        start = System.currentTimeMillis();

        // groupsList = groupsDAO.fetchAllGroupsByAccountId(accountId, list);
        WSChatThreadListMetaDataResult metaData = new WSChatThreadListMetaDataResult();

        List<WSChatThreadReference> referenceOneToOneList = new ArrayList<WSChatThreadReference>();
        int index = 3;

        List<String> accountIdList = new ArrayList<String>();
        for (Groups group : groupsList)
        {
            if (group.getParticipants() != null)
            {
                accountIdList.addAll(group.getParticipants());
            }
        }

        Map<String, WSMessageResponse> oneGroupCreatedByVsMessageResponse = new HashMap<String, WSMessageResponse>();

        for (Groups groups : groupsList)
        {
            if (groups.getSource().equals(GroupSource.ONEGROUP))
            {

                String createdBy = groups.getCreatedBy();
                WSMessageResponse wsMessageResponseOneGroup = chatMessageService.fetchLastBroadcastMessageOfOneGroup(groups.getCreatedBy());
                oneGroupCreatedByVsMessageResponse.put(createdBy, wsMessageResponseOneGroup);
                if (wsMessageResponseOneGroup != null && wsMessageResponseOneGroup.getId() != null)
                {
                    if (wsMessageResponseOneGroup.getToAccountId() != null)
                    {
                        accountIdList.add(wsMessageResponseOneGroup.getToAccountId());
                    }
                    if (wsMessageResponseOneGroup.getCreatedById() != null)
                    {
                        accountIdList.add(wsMessageResponseOneGroup.getCreatedById());
                    }
                }

            }

            else if (groups.getSource().equals(GroupSource.STARRED))
            {
                WSMessageResponse wsMessageResponseStarred = chatMessageService.fetchLastStarredMessageOfAccount(groups.getCreatedBy());
                if (wsMessageResponseStarred.getId() != null)
                {
                    if (wsMessageResponseStarred.getToAccountId() != null)
                    {
                        accountIdList.add(wsMessageResponseStarred.getToAccountId());
                    }

                    if (wsMessageResponseStarred.getCreatedById() != null)
                    {
                        accountIdList.add(wsMessageResponseStarred.getCreatedById());
                    }
                }
            }
        }

        logger.info("loop1[" + (System.currentTimeMillis() - start) + "]");
        start = System.currentTimeMillis();

        WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);
        logger.info("fetchAccountMapData[" + (System.currentTimeMillis() - start) + "]");
        start = System.currentTimeMillis();

        Map<String, Integer> groupIdVsReadIndexMap = fetchMapOfGroupVsReadMessageIndexOfAccount(accountId, groupIdList);
        logger.info("fetchMapOfGroupVsReadMessageIndexOfAccount[" + (System.currentTimeMillis() - start) + "]");
        start = System.currentTimeMillis();

        for (Groups groups : groupsList)
        {
            String groupId = groups.getId();
            if (groups.getSource().equals(GroupSource.ONEGROUP))
            {
                List<WSMessageResponse> messageResponseOneGroup = new ArrayList<WSMessageResponse>();
                WSMessageResponse wsMessageResponseOneGroup = oneGroupCreatedByVsMessageResponse.get(groups.getCreatedBy());
                if (wsMessageResponseOneGroup != null && wsMessageResponseOneGroup.getId() != null)
                {
                    wsMessageResponseOneGroup = formWSMessageResponse(wsMessageResponseOneGroup, dataHolder);
                    messageResponseOneGroup.add(wsMessageResponseOneGroup);
                }

                WSChatThread wsChatThreadOneGroup = new WSChatThread(groupId, 1, messageResponseOneGroup);
                if (wsMessageResponseOneGroup != null && wsMessageResponseOneGroup.getId() != null)
                {
                    wsChatThreadOneGroup.setLatestMessageTime(wsMessageResponseOneGroup.getCreatedTime());
                }
                else
                {
                    wsChatThreadOneGroup.setLatestMessageTime(-1 + "");
                }

                Integer totalMessageCount = groups.getTotalmsgCount();

                int unreadMessageCount = 0;
                if (totalMessageCount != null)
                {
                    unreadMessageCount = totalMessageCount;
                }
                else
                {
                    WSAccount wsAccount = accountService.fetchAccountDetails(accountId);
                    String cityId = wsAccount.getCity().getId();
                    if (cityId != null)
                    {
                        Long totalMessages = broadcastService.fetchBroadcastCountByCity(cityId);
                        if (totalMessages != null)
                        {
                            unreadMessageCount = totalMessages.intValue();
                        }
                    }
                }
                wsChatThreadOneGroup.setUnreadMessageCount(unreadMessageCount);
                wsChatThreadOneGroup.setMessages(messageResponseOneGroup);
                WSChatThreadReference referenceOneGroup = new WSChatThreadReference(wsChatThreadOneGroup, 0);
                metaData.setOneGroup(referenceOneGroup);
            }

            else if (groups.getSource().equals(GroupSource.STARRED))
            {
                List<WSMessageResponse> messageResponseStarred = new ArrayList<WSMessageResponse>();
                WSMessageResponse wsMessageResponseStarred = chatMessageService.fetchLastStarredMessageOfAccount(groups.getCreatedBy());
                if (wsMessageResponseStarred.getId() != null)
                {
                    wsMessageResponseStarred = formWSMessageResponse(wsMessageResponseStarred, dataHolder);
                    messageResponseStarred.add(wsMessageResponseStarred);
                }
                WSChatThread wsChatThreadStarred = new WSChatThread(groupId, 1, messageResponseStarred);

                if (wsMessageResponseStarred.getId() != null)
                {
                    wsChatThreadStarred.setLatestMessageTime(wsMessageResponseStarred.getCreatedTime());
                    wsChatThreadStarred.setMaxChatMessageIndex(wsMessageResponseStarred.getIndex());
                }
                else
                {
                    wsChatThreadStarred.setLatestMessageTime(-1 + "");
                    wsChatThreadStarred.setMaxChatMessageIndex(-1);
                }

                wsChatThreadStarred.setUnreadMessageCount(0);
                wsChatThreadStarred.setMessages(messageResponseStarred);

                WSChatThreadReference referenceStarred = new WSChatThreadReference(wsChatThreadStarred, 1);
                metaData.setStarred(referenceStarred);
            }

            else if (groups.getSource().equals(GroupSource.ME))
            {
                List<WSMessageResponse> messageResponseMe = new ArrayList<WSMessageResponse>();
                WSMessageResponse wsMessageResponseMe = chatMessageService.fetchLastBroadcastMessageOfMe(groups.getCreatedBy());
                if (wsMessageResponseMe.getId() != null)
                {
                    wsMessageResponseMe = formWSMessageResponse(wsMessageResponseMe, dataHolder);
                    messageResponseMe.add(wsMessageResponseMe);
                }
                WSChatThread wsChatThreadMe = new WSChatThread(groupId, 1, messageResponseMe);

                if (wsMessageResponseMe.getId() != null)
                {
                    wsChatThreadMe.setLatestMessageTime(wsMessageResponseMe.getCreatedTime());
                    wsChatThreadMe.setMaxChatMessageIndex(wsMessageResponseMe.getIndex());
                }
                else
                {
                    wsChatThreadMe.setLatestMessageTime(-1 + "");
                    wsChatThreadMe.setMaxChatMessageIndex(-1);
                }

                wsChatThreadMe.setUnreadMessageCount(0);

                wsChatThreadMe.setMessages(messageResponseMe);

                WSChatThreadReference referenceMe = new WSChatThreadReference(wsChatThreadMe, 2);
                metaData.setMe(referenceMe);
            }

            else if (groups.getSource().equals(GroupSource.DIRECT))
            {
                Map<String, GroupStatus> accountVsGroupStatus = groups.getParticipantsStatusMap();
                // TODO need to remove null as already created data will be
                // handle by null
                if (accountVsGroupStatus == null || accountVsGroupStatus.get(accountId).equals(GroupStatus.ACTIVE))
                {
                    List<WSMessageResponse> messageResponseOneToOne = new ArrayList<WSMessageResponse>();
                    WSMessageResponse wsMessageResponseOneToOne = chatMessageService.fetchLastMessageOfGroup(groupId);
                    if (wsMessageResponseOneToOne.getId() != null)
                    {
                        wsMessageResponseOneToOne = formWSMessageResponse(wsMessageResponseOneToOne, dataHolder);
                        messageResponseOneToOne.add(wsMessageResponseOneToOne);
                    }
                    if (!messageResponseOneToOne.isEmpty())
                    {
                        WSChatThread wsChatThreadOneToOne = new WSChatThread(groups.getId(), 1, messageResponseOneToOne);

                        if (wsMessageResponseOneToOne.getId() != null)
                        {
                            wsChatThreadOneToOne.setLatestMessageTime(wsMessageResponseOneToOne.getCreatedTime());
                            wsChatThreadOneToOne.setMaxChatMessageIndex(wsMessageResponseOneToOne.getIndex());
                        }
                        else
                        {
                            wsChatThreadOneToOne.setLatestMessageTime(-1 + "");
                            wsChatThreadOneToOne.setMaxChatMessageIndex(-1);
                        }
                        Integer lastReadMessageIndex = groupIdVsReadIndexMap.get(groupId);
                        Integer unreadMessageCount = groups.getTotalmsgCount();
                        if (lastReadMessageIndex != null)
                        {
                            unreadMessageCount = unreadMessageCount - lastReadMessageIndex - 1;
                        }
                        wsChatThreadOneToOne.setUnreadMessageCount(unreadMessageCount);
                        wsChatThreadOneToOne.setMessages(messageResponseOneToOne);

                        WSChatThreadReference referenceOneToOne = new WSChatThreadReference(wsChatThreadOneToOne, index);
                        referenceOneToOneList.add(referenceOneToOne);
                    }
                    index++;
                }
            }
        }
        logger.info("loop2[" + (System.currentTimeMillis() - start) + "]");

        metaData.setDirect(referenceOneToOneList);
        return metaData;

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "GroupService-updateChatThreadCursor")
    public WSChatThreadCursor updateChatThreadCursor(String chatThreadId, String accountId, int receivedIndex, int readIndex, String receivedMessageId, String readMessageId)
            throws Abstract1GroupException
    {
        WSChatThreadCursor wsChatThreadCursor = new WSChatThreadCursor();
        GroupMembers groupMembers = groupMembersDAO.fetchGroupMembersByAccountIdAndGroupId(accountId, chatThreadId);

        logger.info("group member found[" + chatThreadId + "]");

        if (readMessageId == null && receivedMessageId == null)
        {
            wsChatThreadCursor.setReadIndex(groupMembers.getReadIndex());
            wsChatThreadCursor.setReceivedIndex(groupMembers.getReceivedIndex());
            wsChatThreadCursor.setReadMessageId(groupMembers.getReadMessageId());
            wsChatThreadCursor.setReceivedMessageId(groupMembers.getReceivedMessageId());
            return wsChatThreadCursor;
        }
        // To handle default values
        int defaultIndex = Integer.valueOf(Constant.DEFAULT_CHAT_THREAD_CURSOR);
        receivedIndex = (receivedIndex < 0) ? (groupMembers != null) ? groupMembers.getReceivedIndex() : defaultIndex : receivedIndex;
        receivedMessageId = (receivedMessageId == null || receivedMessageId.isEmpty() == true) ? (groupMembers != null) ? groupMembers.getReceivedMessageId() : null : receivedMessageId;

        readIndex = (readIndex < 0) ? (groupMembers != null) ? groupMembers.getReadIndex() : defaultIndex : readIndex;
        readMessageId = (readMessageId == null || readMessageId.isEmpty() == true) ? (groupMembers != null) ? groupMembers.getReadMessageId() : null : readMessageId;

        // Update cursors
        logger.info("start: updating cursor[" + chatThreadId + "," + accountId + "," + receivedIndex + "," + readIndex + "," + receivedMessageId + "," + readMessageId + "]");
        int status = groupMembersDAO.updateReadAndReceivedIndex(chatThreadId, accountId, receivedIndex, readIndex, receivedMessageId, readMessageId);
        logger.info("end: updating cursor[" + status + "]");

        wsChatThreadCursor.setReadIndex(readIndex);
        wsChatThreadCursor.setReceivedIndex(receivedIndex);
        wsChatThreadCursor.setReadMessageId(readMessageId);
        wsChatThreadCursor.setReceivedMessageId(receivedMessageId);

        WSChatCursor wsChatCursor = new WSChatCursor();
        wsChatCursor.setCreatedBy(accountId);
        wsChatCursor.setChatThreadId(chatThreadId);
        wsChatCursor.setCursor(wsChatThreadCursor);
        chatMessageService.pushChatCursorToSyncLog(wsChatCursor, null);

        return wsChatThreadCursor;
    }

    @Profiled(tag = "GroupService-fetchGroupsFromParticipantsAndType")
    public Groups fetchGroupsFromParticipantsAndType(String accountId, List<String> participantsIds, GroupSource groupSource)
    {
        String participantsOne = participantsIds.get(0);
        String participantsTwo = participantsIds.get(1);

        String groupId = null;
        Groups group = null;
        try
        {
            if (groupSource.equals(GroupSource.DIRECT))
            {
                groupId = groupMembersDAO.fetchGroupIdFromParticipants(participantsOne, participantsTwo, groupSource);
            }
            else
            {
                groupId = groupMembersDAO.fetchGroupIdFromParticipantIdAndSource(accountId, groupSource);
            }

        }
        catch (NoResultException nre)
        {
            logger.info("Its ok no group id found for [" + participantsOne + ":" + participantsTwo + ":" + groupSource + "]");
        }

        if (groupId != null)
        {
            group = fetchGroupDetailsFromId(groupId);
        }
        return group;
    }

    @Profiled(tag = "GroupService-fetchGroupsIdFromParticipantsAndType")
    public String fetchGroupsIdFromParticipantsAndType(List<String> participantsIds, GroupSource groupSource)
    {
        List<Groups> groupsList = groupsDAO.fetchGroupIdFromMembers(participantsIds, groupSource);
        String groupId = null;
        if (groupsList != null && !groupsList.isEmpty())
        {
            groupId = groupsList.get(0).getId(); // TODO
        }
        return groupId;
    }

    @Profiled(tag = "GroupService-fetchStarredMessageOfAccount")
    public WSChatThread fetchStarredMessageOfAccount(String accountId, int currentPageNo, String pgId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<WSMessageResponse> messageResponseStarred = new ArrayList<WSMessageResponse>();
        List<String> accountIdList = new ArrayList<String>();
        List<String> groupParticipants = new ArrayList<String>();
        groupParticipants.add(accountId);
        groupParticipants.add(accountId);

        Groups group = fetchGroupsFromParticipantsAndType(accountId, groupParticipants, GroupSource.STARRED);

        int offset = totalMessagePerPage * currentPageNo;
        int limit = totalMessagePerPage;
        List<WSMessageResponse> wsMessageResponseList = chatMessageService.fetchStarredBroadcastOfAccount(accountId, offset, limit);

        if (!wsMessageResponseList.isEmpty())
        {
            for (WSMessageResponse wsMessagResponse : wsMessageResponseList)
            {
                accountIdList.add(wsMessagResponse.getToAccountId());
                accountIdList.add(wsMessagResponse.getCreatedById());
            }

            WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);

            for (WSMessageResponse wsMessageResponse : wsMessageResponseList)
            {
                wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
                messageResponseStarred.add(wsMessageResponse);
            }
        }
        String groupId = group.getId();
        WSChatThread wsChatThreadStarred = new WSChatThread(groupId, 1, wsMessageResponseList);
        wsChatThreadStarred.setMaxChatMessageIndex(wsMessageResponseList.size());

        int totalMessageCount = group.getTotalmsgCount();
        WSPaginationData pagination = new WSPaginationData(pgId, currentPageNo, totalMessageCount, totalMessagePerPage);
        wsChatThreadStarred.setPagination(pagination);
        return wsChatThreadStarred;
    }

    @Profiled(tag = "GroupService-fetchDirectChatThreadOfAccount")
    @Transactional(rollbackOn = { Exception.class })
    public WSChatThread fetchDirectChatThreadOfAccount(String accountId, String participantOne, String participantTwo, int currentPageNo, String pgId) throws JsonMappingException,
            JsonGenerationException, IOException
    {

        List<String> participantsIds = new ArrayList<String>();
        participantsIds.add(participantOne);
        participantsIds.add(participantTwo);
        Groups group = fetchGroupsFromParticipantsAndType(accountId, participantsIds, GroupSource.DIRECT);
        WSChatThread wsChatThread = new WSChatThread();
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();
        List<WSMessageResponse> wsMessageResponseDirectList = new ArrayList<WSMessageResponse>();
        if (group == null)
        {
            group = createGroupIfNotExistAndAddMembers(accountId, participantOne, participantTwo, GroupSource.DIRECT);
        }
        else
        {
            int offset = totalMessagePerPage * currentPageNo;
            int limit = totalMessagePerPage;
            List<MessageType> messageTypeList = new ArrayList<MessageType>();
            messageTypeList.add(MessageType.BROADCAST);
            messageTypeList.add(MessageType.TEXT);
            messageTypeList.add(MessageType.PHOTO);
            wsMessageResponseList = chatMessageService.fetchMessageOfChatThreadBySourceAndType(group.getId(), MessageSourceType.DIRECT, messageTypeList, offset, limit);
            if (wsMessageResponseList != null && !wsMessageResponseList.isEmpty())
            {
                Collections.reverse(wsMessageResponseList);
            }
        }
        WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, participantsIds);

        if (!wsMessageResponseList.isEmpty())
        {
            for (WSMessageResponse wsMessageResponse : wsMessageResponseList)
            {
                wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
                wsMessageResponseDirectList.add(wsMessageResponse);
            }
        }
        wsChatThread.setMessages(wsMessageResponseDirectList);
        wsChatThread.setChatThreadId(group.getId());
        wsChatThread.setMaxChatMessageIndex(group.getTotalmsgCount() - 1);
        wsChatThread.setUnreadMessageCount(0);
        if (!wsMessageResponseList.isEmpty())
        {
            WSMessageResponse wsLastMessageResponse = chatMessageService.fetchLastMessageOfGroup(group.getId());
            wsChatThread.setLatestMessageTime(wsLastMessageResponse.getCreatedTime());
        }

        int totalMessageCount = group.getTotalmsgCount();
        WSPaginationData pagination = new WSPaginationData(pgId, currentPageNo, totalMessageCount, totalMessagePerPage);
        wsChatThread.setPagination(pagination);

        return wsChatThread;
    }

    @Profiled(tag = "GroupService-fetchGroupDetailsFromId")
    public Groups fetchGroupDetailsFromId(String groupId)
    {
        Groups group = null;

        List<String> groupIdList = new ArrayList<String>();
        groupIdList.add(groupId);
        List<Groups> groupList = groupsDAO.fetchGroupsByGroupIds(groupIdList);

        if (groupList != null && !groupList.isEmpty())
        {
            group = groupList.get(0); // TODO
        }
        return group;
    }

    @Profiled(tag = "GroupService-fetch1GroupdMessage")
    public WSChatThread fetch1GroupdMessage(String accountId, WSBroadcastSearchQueryRequest searchQueryRequest, int currentPageNo, String pgId) throws JsonMappingException, JsonGenerationException,
            IOException, Abstract1GroupException
    {
        List<WSMessageResponse> messageResponseOneGroupList = new ArrayList<WSMessageResponse>();
        List<String> accountIdList = new ArrayList<String>();
        List<String> groupParticipants = new ArrayList<String>();
        groupParticipants.add(accountId);
        groupParticipants.add(accountId);

        PropertyType propertyType = !Utils.isNullOrEmpty(searchQueryRequest.getPropertyType()) ? PropertyType.convert(searchQueryRequest.getPropertyType()) : null;
        PropertySubType propertySubType = !Utils.isNullOrEmpty(searchQueryRequest.getPropertySubType()) ? PropertySubType.convert(searchQueryRequest.getPropertySubType()) : null;
        BroadcastType broadcastType = !Utils.isNullOrEmpty(searchQueryRequest.getBroadcastType()) ? BroadcastType.convert(searchQueryRequest.getBroadcastType()) : null;
        PropertyTransactionType transactionType = !Utils.isNullOrEmpty(searchQueryRequest.getTransactionType()) ? PropertyTransactionType.convert(searchQueryRequest.getTransactionType()) : null;
        int cityId = Integer.valueOf(searchQueryRequest.getCityId());
        String cityIdStr = String.valueOf(cityId);
        City city = cityService.getCityById(cityIdStr);
        if (city == null)
        {
            throw new General1GroupException(GeneralExceptionCode.NOT_FOUND, false, cityIdStr);
        }

        String locationId = !Utils.isNullOrEmpty(searchQueryRequest.getLocationId()) ? searchQueryRequest.getLocationId() : null;
        Location location = null;
        if (locationId != null)
        {
            location = locationDAO.fetchById(locationId);
            if (location == null)
            {
                throw new General1GroupException(LocationExceptionCode.LOCATION_NOT_FOUND, false, locationId);
            }
        }
        Integer minArea = !Utils.isNull(searchQueryRequest.getMinArea()) ? searchQueryRequest.getMinArea() : null;
        Integer maxArea = !Utils.isNull(searchQueryRequest.getMaxArea()) ? searchQueryRequest.getMaxArea() : null;
        Long minPrice = !Utils.isNull(searchQueryRequest.getMinPrice()) ? Long.valueOf(searchQueryRequest.getMinPrice()) : null;
        Long maxPrice = !Utils.isNull(searchQueryRequest.getMaxPrice()) ? Long.valueOf(searchQueryRequest.getMaxPrice()) : null;
        List<String> roomList = !Utils.isNull(searchQueryRequest.getRooms()) ? searchQueryRequest.getRooms() : null;
        List<Rooms> roomEnumList = new ArrayList<Rooms>();
        if (roomList != null)
        {
            for (String room : roomList)
            {
                roomEnumList.add(Rooms.convert(room));
            }
        }
        Groups group = fetchGroupsFromParticipantsAndType(accountId, groupParticipants, GroupSource.ONEGROUP);

        List<String> broadcastIdList = new ArrayList<String>();
        if (Utils.isNull(pgId))
        {
            broadcastIdList = broadcastService.searchBroadcastsAndCacheItAndUpdateViewCount(accountId, propertyType, broadcastType, transactionType, cityId, location, minArea, maxArea, maxPrice,
                    minPrice, roomEnumList, propertySubType, 500, 0);

            // Update total message count of group
            Groups groupToUpdate = new Groups();
            groupToUpdate.setId(group.getId());
            groupToUpdate.setTotalmsgCount(broadcastIdList.size());
            groupsDAO.update(Arrays.asList(groupToUpdate));

            group.setTotalmsgCount(broadcastIdList.size());

            pgId = UUID.randomUUID().toString();
        }

        int start = (totalMessagePerPage * currentPageNo);
        int end = start + totalMessagePerPage - 1;
        // Redis starts index from 0 thats why defaultIncremental is set
        broadcastIdList = broadcastService.fetchBroadcastofAccountFromCache(accountId, start, end);
        List<WSMessageResponse> wsMessageResponseList = chatMessageService.fetchMessageFromBroadcastIds(broadcastIdList);

        if (!wsMessageResponseList.isEmpty())
        {
            for (WSMessageResponse wsMessageResponse : wsMessageResponseList)
            {
                if (wsMessageResponse.getToAccountId() != null)
                {
                    accountIdList.add(wsMessageResponse.getToAccountId());
                }

                if (wsMessageResponse.getCreatedById() != null)
                {
                    accountIdList.add(wsMessageResponse.getCreatedById());
                }
            }

            WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);

            // Fetch broadcast details in bulk
            Map<String, WSBroadcast> idVsBroadcastMap = broadcastService.fetchBroadcastMapById(broadcastIdList);
            dataHolder.setIdVsBroadcastMap(idVsBroadcastMap);

            for (WSMessageResponse wsMessageResponse : wsMessageResponseList)
            {
                wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
                messageResponseOneGroupList.add(wsMessageResponse);

                subscriptionService.subscribe(accountId, wsMessageResponse.getId(), EntityType.MESSAGE);
                subscriptionService.subscribe(accountId, wsMessageResponse.getCreatedBy().getId(), EntityType.ACCOUNT, Constant.MAX_SUBSCRIBE_TIME_IN_MINUTES);

            }

            // if (messageResponseOneGroupList != null &&
            // !messageResponseOneGroupList.isEmpty())
            // {
            // Collections.reverse(messageResponseOneGroupList);
            // }
        }

        String groupId = group.getId();
        WSChatThread wsChatThreadOneGroup = new WSChatThread(groupId, 1, messageResponseOneGroupList);
        wsChatThreadOneGroup.setMaxChatMessageIndex(messageResponseOneGroupList.size());

        if (messageResponseOneGroupList.size() > 0)
        {
            // wsChatThreadOneGroup.setLatestMessageTime(latestMessageTime);
        }
        int totalMessageCount = group.getTotalmsgCount();
        WSPaginationData pagination = new WSPaginationData(pgId, currentPageNo, totalMessageCount, totalMessagePerPage);
        wsChatThreadOneGroup.setPagination(pagination);

        // update search
        WSBroadcastSearchQueryRequest searchRequest = searchService.updateSearchForAccount(accountId, searchQueryRequest);
        // appendToSyncLogAsEntityReferenceForSearch(accountId,
        // searchRequest.getSearchId(), SyncActionType.FETCH);
        return wsChatThreadOneGroup;

    }

    @Profiled(tag = "GroupService-deleteGroup")
    public void deleteGroup(String accountId, String groupId) throws General1GroupException, SyncLogProcessException
    {
        Groups group = fetchGroupDetailsFromId(groupId);
        if (group == null)
        {
            throw new General1GroupException(GroupExceptionCode.GROUP_NOT_FOUND, false);
        }
        if (!(group.getParticipants().contains(accountId)))
        {
            throw new General1GroupException(AuthorizationExceptionCode.USER_ACCESS_DENIED, false);
        }

        if (group.getSource().equals(GroupSource.DIRECT))
        {
            Groups groupToUpdate = new Groups();
            groupToUpdate.setId(group.getId());
            groupToUpdate.setStatus(GroupStatus.INACTIVE);
            groupToUpdate.setUpdatedBy(accountId);
            groupToUpdate.setUpdatedTime(new Date());

            List<String> participantsStatus = new ArrayList<String>();
            for (String participantId : group.getParticipants())
            {
                if (participantId.equals(accountId))
                {
                    participantsStatus.add(participantId + ":" + GroupStatus.INACTIVE.name());
                }
                else
                {
                    participantsStatus.add(participantId + ":" + GroupStatus.ACTIVE.name());
                }
            }
            groupToUpdate.setParticipantsStatus(participantsStatus);

            groupsDAO.update(Arrays.asList(groupToUpdate));

            appendToSyncLogAsEntityReference(accountId, group.getId(), SyncActionType.DELETE_CHAT_THREAD);
        }
        else
        {
            throw new General1GroupException(GroupExceptionCode.GROUP_NOT_FOUND, false);
        }
    }

    @Profiled(tag = "GroupService-fetchPhonecallMessage")
    public List<WSMessageResponse> fetchPhonecallMessage(String accountId) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {
        List<Message> messageList = chatMessageService.fetchPhonecallMessages(accountId);
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();
        if (messageList != null && !messageList.isEmpty())
        {
            List<String> accountIdList = new ArrayList<String>();
            for (Message message : messageList)
            {
                accountIdList.add(message.getToAccountId());
                accountIdList.add(message.getCreatedBy());
            }

            WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);

            for (Message message : messageList)
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
                wsMessageResponseList.add(wsMessageResponse);
            }
        }
        return wsMessageResponseList;
    }

    @Profiled(tag = "GroupService-fetchMeMessageOfAccount")
    public WSChatThread fetchMeMessageOfAccount(String accountId, int currentPageNo, String pgId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();
        List<WSMessageResponse> wsMessageResponseMeList = new ArrayList<WSMessageResponse>();
        List<String> accountIdList = new ArrayList<String>();
        accountIdList.add(accountId);
        accountIdList.add(accountId);

        Groups group = fetchGroupsFromParticipantsAndType(accountId, accountIdList, GroupSource.ME);

        int offset = totalMessagePerPage * currentPageNo;
        int limit = totalMessagePerPage;
        List<Message> messageList = chatMessageService.fetchMeMessagesOfAccount(accountId, offset, limit);

        if (messageList != null && !messageList.isEmpty())
        {
            WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);

            for (Message message : messageList)
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
                wsMessageResponseMeList.add(wsMessageResponse);
            }

            if (wsMessageResponseMeList != null && !wsMessageResponseMeList.isEmpty())
            {
                Collections.reverse(wsMessageResponseMeList);
            }
        }
        String groupId = group.getId();
        WSChatThread wsChatThreadMe = new WSChatThread(groupId, 1, wsMessageResponseMeList);
        wsChatThreadMe.setMaxChatMessageIndex(wsMessageResponseMeList.size());

        long totalMessageCount = chatMessageService.getTotalMessageOfAccountOfMeGroup(accountId);
        WSPaginationData pagination = new WSPaginationData(pgId, currentPageNo, totalMessageCount, totalMessagePerPage);
        wsChatThreadMe.setPagination(pagination);
        return wsChatThreadMe;
    }

    @Profiled(tag = "GroupService-fetchChatThreadListByBroadcast")
    public WSChatThreadList fetchChatThreadListByBroadcast(String accountId, String broadcastId) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSChatThreadList wsChatThreadList = new WSChatThreadList();
        List<String> groupIdList = chatMessageService.fetchUniqueMessageByBroadcastIdandAccountId(accountId, broadcastId);
        List<WSChatThreadReference> wsChatThreadReferenceList = new ArrayList<WSChatThreadReference>();

        Map<String, Groups> idVsGroupMap = new HashMap<String, Groups>();
        List<String> accountIdList = new ArrayList<String>();
        for (String groupId : groupIdList)
        {
            Groups group = fetchGroupDetailsFromId(groupId);
            accountIdList.addAll(group.getParticipants());
            idVsGroupMap.put(groupId, group);
        }
        WSAccountUtilityDataHolder dataHolder = fetchAccountMapData(accountId, accountIdList);

        for (String groupId : groupIdList)
        {
            Groups group = idVsGroupMap.get(groupId);

            WSChatThreadReference wsChatThreadReference = new WSChatThreadReference();

            WSChatThread wsChatThread = new WSChatThread();
            wsChatThread.setChatThreadId(group.getId());
            wsChatThread.setMaxChatMessageIndex(group.getTotalmsgCount() - 1);

            List<WSMessageResponse> wsMessageResponeList = new ArrayList<WSMessageResponse>();
            WSMessageResponse wsMessageResponse = chatMessageService.fetchLastMessageOfGroup(groupId);
            wsMessageResponse = formWSMessageResponse(wsMessageResponse, dataHolder);
            wsMessageResponeList.add(wsMessageResponse);
            wsChatThread.setMessages(wsMessageResponeList);
            wsChatThread.setLatestMessageTime(wsMessageResponse.getCreatedTime());
            int lastReadIndex = dataHolder.getAccountVsCursorMap().get(groupId).get(accountId).getReadIndex();
            wsChatThread.setUnreadMessageCount(group.getTotalmsgCount() - 1 - lastReadIndex);

            wsChatThreadReference.setChatThread(wsChatThread);
            wsChatThreadReferenceList.add(wsChatThreadReference);
        }
        wsChatThreadList.setChatThreads(wsChatThreadReferenceList);
        return wsChatThreadList;
    }

    private void appendToSyncLogAsEntityReference(String accountId, String groupId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.DELETE, EntityType.CHAT_THREAD, groupId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.CHAT_THREAD);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    private void appendToSyncLogAsEntityReferenceForSearch(String accountId, String searchId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.SEARCH, searchId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.SEARCH);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Profiled(tag = "GroupService-updateGroupDetails")
    public void updateGroupDetails(SolrInputDocument groups)
    {
        groupsDAO.updateGroup(groups);
    }

    @Profiled(tag = "fetchGroupIdFromParticipants")
    @Transactional(rollbackOn = { Exception.class })
    public String fetchGroupIdFromParticipants(String participantsOne, String participantsTwo, GroupSource source)
    {
        return groupMembersDAO.fetchGroupIdFromParticipants(participantsOne, participantsTwo, source);
    }

    @Profiled(tag = "fetchGroupIdListByAccountId")
    public List<String> fetchGroupIdListByAccountId(String accountId)
    {
        return groupMembersDAO.fetchGroupIdListByAccountId(accountId);
    }

    @Profiled(tag = "fetchMemberIdsOfGroup")
    public List<String> fetchMemberIdsOfGroup(String groupId)
    {
        return groupMembersDAO.fetchMemberIdListByGroupId(groupId);
    }

    @Profiled(tag = "saveGroupMembersBulk")
    @Transactional(rollbackOn = { Exception.class })
    public void saveGroupMembersBulk(List<GroupMembers> groupMembers)
    {
        groupMembersDAO.saveGroupMembers(groupMembers);
    }

}
