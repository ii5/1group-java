package one.group.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import one.group.dao.ClientAnalyticsDAO;
import one.group.entities.api.request.v2.WSClientAnalytics;
import one.group.entities.api.response.WSClientAnalyticsResponse;
import one.group.entities.jpa.ClientAnalytics;
import one.group.services.ClientAnalyticsService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class ClientAnalyticsServiceImpl implements ClientAnalyticsService
{

    private ClientAnalyticsDAO clientAnalyticsDAO;

    public ClientAnalyticsDAO getClientAnalyticsDAO()
    {
        return clientAnalyticsDAO;
    }

    public void setClientAnalyticsDAO(ClientAnalyticsDAO clientAnalyticsDAO)
    {
        this.clientAnalyticsDAO = clientAnalyticsDAO;
    }

    @Transactional(rollbackOn = Exception.class)
    public void saveTraces(List<WSClientAnalytics> wsClientAnalyticsList, String accountId, String clientId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(clientId), "Client Id passed should not be null or empty.");

        if (wsClientAnalyticsList != null && !wsClientAnalyticsList.isEmpty())
        {
            for (WSClientAnalytics wsClientAnalytics : wsClientAnalyticsList)
            {
                Date clientEventTime = wsClientAnalytics.getClientEventTime() == null ? null : new Date(Long.valueOf(wsClientAnalytics.getClientEventTime()));

                ClientAnalytics clientAnalytics = new ClientAnalytics();

                clientAnalytics.setAccountId(accountId);
                clientAnalytics.setClientId(clientId);
                clientAnalytics.setAction(wsClientAnalytics.getAction());
                clientAnalytics.setContext(wsClientAnalytics.getContext());
                clientAnalytics.setEntityId(wsClientAnalytics.getEntityId());
                clientAnalytics.setEntityType(wsClientAnalytics.getEntityType());
                clientAnalytics.setClientEventTime(clientEventTime);
                clientAnalytics.setCreatedTime(new Date());

                clientAnalyticsDAO.saveclientAnalytics(clientAnalytics);
            }
        }
    }

    public List<WSClientAnalyticsResponse> fetchTracesByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");

        List<ClientAnalytics> analytics = clientAnalyticsDAO.fetchTracesByAccountId(accountId);
        List<WSClientAnalyticsResponse> WSanalyticsList = new ArrayList<WSClientAnalyticsResponse>();
        for (ClientAnalytics analytic : analytics)
        {
            WSClientAnalyticsResponse wsAnalytics = new WSClientAnalyticsResponse(analytic);
            WSanalyticsList.add(wsAnalytics);
        }
        return WSanalyticsList;
    }
}