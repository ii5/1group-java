package one.group.services.impl;

import one.group.dao.GeneralDAO;
import one.group.entities.api.response.v2.WSPaginationData;
import one.group.services.PaginationService;

public class PaginationServiceImpl implements PaginationService
{
    private GeneralDAO generalDAO;

    public GeneralDAO getGeneralDAO()
    {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO)
    {
        this.generalDAO = generalDAO;
    }

    public WSPaginationData getPaginationData(String paginationKey, int pageNo, final String refreshPaginationKey)
    {
        int perPageRecords = 5;
        WSPaginationData pagination = new WSPaginationData();
        long totalRecords = generalDAO.fetchTotalRecordsCount(paginationKey);
        pagination.setTotalRecords(totalRecords);

        String keys[] = paginationKey.split(":");
        pagination.setId(keys[2]);
        if (totalRecords == 0)
        {
            keys = refreshPaginationKey.split(":");
            pagination.setId(keys[2]);
        }
        long pageCount = Math.round(Math.ceil((float) totalRecords / perPageRecords));
        pagination.setPageCount(pageCount);

        if (pageNo > 1)
        {
            pagination.setPreviousPage(pageNo - 1);
        }

        pagination.setCurrentPage(pageNo);
        if (pageNo < pageCount)
        {
            pagination.setNextPage(pageNo + 1);
        }

        return pagination;
    }

    public static void main(String[] args)
    {
        int totalRecords = 52;
        int perPage = 3;
        float totalPage = (float) totalRecords / perPage;
        long totalll = Math.round(Math.ceil(totalPage));
        System.out.println(totalll);
    }
}
