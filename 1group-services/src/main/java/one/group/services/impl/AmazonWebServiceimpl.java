package one.group.services.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import one.group.core.enums.CmsType;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.CmsService;
import one.group.services.helpers.CmsConnectionFactory;
import one.group.services.helpers.CmsServiceObject;
import one.group.utils.Utils;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class AmazonWebServiceimpl implements CmsService
{

    private CmsConnectionFactory connectionFactory;

    private AmazonS3 amazonS3;

    private static final Logger logger = LoggerFactory.getLogger(AmazonWebServiceimpl.class);

    public CmsConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(CmsConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    private AmazonS3 getAmazonS3() throws IOException
    {
        if (amazonS3 == null)
        {
            amazonS3 = connectionFactory.getResource();
        }
        return amazonS3;
    }

    @Profiled(tag = "AmazonWebService-UploadFile")
    public String UploadFile(CmsServiceObject config) throws General1GroupException
    {
        File file = config.getAwsFile();
        String key = config.getKey();
        String bucketName = config.getBucketName();
        CmsType cmsType = config.getCmsType();
        String fileUplodedUrl = null;
        try
        {
            logger.info("Entered " + this.getClass().getSimpleName());
            if (cmsType.equals(CmsType.AWS))
            {
                logger.info("Generating pre-signed URL.");
                {
                    logger.warn("Initiating Thread");

                    getAmazonS3().putObject(new PutObjectRequest(bucketName, key, file));
                    // Get Uploded Image Url
                    GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key);
                    generatePresignedUrlRequest.setMethod(HttpMethod.GET);
                    URL presignedUrl = getAmazonS3().generatePresignedUrl(generatePresignedUrlRequest);
                    String url[] = presignedUrl.toString().split("\\?");
                    fileUplodedUrl = url[0].replace(connectionFactory.getCdnUrl(), "http://" + connectionFactory.getCloudFrontUrl());
                    logger.info("Pre-Signed URL = " + presignedUrl.toString());
                }
            }
        }
        catch (AmazonServiceException ase)
        {
            logger.error("Caught an AmazonServiceException, which means your request made it " + "to Amazon S3, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());

            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ase);
        }
        catch (AmazonClientException ace)
        {
            logger.error("Caught an AmazonClientException, which means the client encountered " + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());

            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ace);
        }
        catch (IOException ie)
        {
            logger.error("Error Message: " + ie.getMessage());

            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ie);
        }
        return fileUplodedUrl;
    }

    @Profiled(tag = "AmazonWebService-getFile")
    public String getFile(CmsServiceObject config) throws General1GroupException
    {
        String key = config.getKey();
        String line = null;
        BufferedReader reader = null;
        String bucketName = config.getBucketName();
        CmsType cmsType = config.getCmsType();
        try
        {
            if (cmsType.equals(CmsType.AWS))
            {
                S3Object s3object = getAmazonS3().getObject(new GetObjectRequest(bucketName, key));

                reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
                line = reader.readLine();
            }
        }
        catch (AmazonServiceException ase)
        {
            logger.error("Caught an AmazonServiceException, which means your request made it " + "to Amazon S3, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ase);
        }
        catch (AmazonClientException ace)
        {
            logger.error("Caught an AmazonClientException, which means the client encountered " + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ace);
        }
        catch (IOException ie)
        {
            logger.error("Error Message: " + ie.getMessage());
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during image upload To Amazon Web Service.", ie);
        }
        finally
        {
            Utils.close(reader);
        }
        return line;
    }
}
