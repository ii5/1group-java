package one.group.services.impl;

import java.util.List;

import one.group.core.enums.BookMarkType;
import one.group.dao.BookMarkDAO;
import one.group.entities.jpa.BookMark;
import one.group.services.BookMarkService;

public class BookMarkServiceImpl implements BookMarkService
{

    private BookMarkDAO bookMarkDAO;

    /**
     * @return the bookMarkDAO
     */
    public BookMarkDAO getBookMarkDAO()
    {
        return bookMarkDAO;
    }

    /**
     * @param bookMarkDAO
     *            the bookMarkDAO to set
     */
    public void setBookMarkDAO(BookMarkDAO bookMarkDAO)
    {
        this.bookMarkDAO = bookMarkDAO;
    }

    /**
     * 
     */
    public List<BookMark> getAllBookMarksByReference(String referenceId, BookMarkType type)
    {
        List<BookMark> bookMark = bookMarkDAO.getBookMarksByReference(referenceId, type);
        return bookMark;
    }

}
