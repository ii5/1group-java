package one.group.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.core.enums.SyncDataType;
import one.group.dao.SearchNotificationDAO;
import one.group.entities.api.response.GCMData;
import one.group.entities.api.response.WSGCMRequest;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.SearchNotification;
import one.group.services.ClientService;
import one.group.services.SearchNotificationService;
import one.group.services.helpers.GCMConnectionFactory;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchNotificationServiceImpl implements SearchNotificationService
{
    private static final Logger logger = LoggerFactory.getLogger(SearchNotificationServiceImpl.class);
    private SearchNotificationDAO searchNotificationDAO;
    private ClientService clientService;
    private String searchNotificationBody;
    private GCMConnectionFactory connectionFactory;

    private String searchNotificationStartTime;

    private String searchNotificationEndTime;

    private String searchTimeZone;

    public SearchNotificationDAO getSearchNotificationDAO()
    {
        return searchNotificationDAO;
    }

    public void setSearchNotificationDAO(SearchNotificationDAO searchNotificationDAO)
    {
        this.searchNotificationDAO = searchNotificationDAO;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public String getSearchNotificationBody()
    {
        return searchNotificationBody;
    }

    public void setSearchNotificationBody(String searchNotificationBody)
    {
        this.searchNotificationBody = searchNotificationBody;
    }

    public GCMConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(GCMConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public String getSearchNotificationStartTime()
    {
        return searchNotificationStartTime;
    }

    public void setSearchNotificationStartTime(String searchNotificationStartTime)
    {
        this.searchNotificationStartTime = searchNotificationStartTime;
    }

    public String getSearchNotificationEndTime()
    {
        return searchNotificationEndTime;
    }

    public void setSearchNotificationEndTime(String searchNotificationEndTime)
    {
        this.searchNotificationEndTime = searchNotificationEndTime;
    }

    public String getSearchTimeZone()
    {
        return searchTimeZone;
    }

    public void setSearchTimeZone(String searchTimeZone)
    {
        this.searchTimeZone = searchTimeZone;
    }

    @Transactional(rollbackOn = { Exception.class })
    public void saveNotification(String accountId)
    {
        try
        {
            long searchCount = 0;
            searchCount = searchNotificationDAO.getSearchNotificationCountByAccountIdAndSentFlag(accountId, true);

            String SearchNotificationStartTime = getSearchNotificationStartTime();
            String splitedStartTime[] = SearchNotificationStartTime.split(":");
            int beginHour = Integer.parseInt(splitedStartTime[0]);
            int beginMinute = Integer.parseInt(splitedStartTime[1]);
            int beginSecond = Integer.parseInt(splitedStartTime[2]);

            String SearchNotificationEndTime = getSearchNotificationEndTime();
            String splitedEndTime[] = SearchNotificationEndTime.split(":");
            int endHour = Integer.parseInt(splitedEndTime[0]);
            int endMinute = Integer.parseInt(splitedEndTime[1]);
            int endSecond = Integer.parseInt(splitedEndTime[2]);

            boolean sendNotificationTimeFrame = Utils.checkBetweenTimeFrame(beginHour, beginMinute, beginSecond, endHour, endMinute, endSecond, getSearchTimeZone());

            if (sendNotificationTimeFrame)
            {
                // if Notification comes within time Frame then save
                // notification and send GCM Message
                // if notification come for same account within time frame then
                // skip.
                if (searchCount == 0)
                {
                    SearchNotification searchNotification = new SearchNotification();
                    searchNotification.setUserId(accountId);
                    searchNotification.setUpdatedTime(new Date());
                    searchNotification.setSent(true);
                    searchNotificationDAO.saveSearchNotification(searchNotification);
                    // GCM SEND
                    sendSearchNotificationByAccountId(accountId);
                }
            }
            else
            {
                // if Notification comes outside of time Frame then save
                // notification with UNSEND status
                // it will be SEND when cron is run
                // if notification come for same account outside time frame then
                // skip saving Notification.
                searchCount = searchNotificationDAO.getSearchNotificationCountByAccountIdAndSentFlag(accountId, false);
                if (searchCount == 0)
                {
                    SearchNotification searchNotification = new SearchNotification();
                    searchNotification.setUserId(accountId);
                    searchNotification.setUpdatedTime(new Date());
                    searchNotification.setSent(false);
                    searchNotificationDAO.saveSearchNotification(searchNotification);
                }
            }
        }
        catch (PersistenceException pe)
        {
            logger.debug("Ignore, Already exist[" + accountId + "]", pe);
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void fetchAndSendNotificationToAll()
    {
        List<SearchNotification> notificationList = searchNotificationDAO.fetchAllNotifications();

        for (SearchNotification notification : notificationList)
        {
            String accountId = notification.getUserId();
            List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);

            List<String> cpsIdListOfActiveClient = new ArrayList<String>();
            if (activeClients != null && activeClients.size() > 0)
            {
                for (Client client : activeClients)
                {
                    cpsIdListOfActiveClient.add(client.getCpsId());
                }
            }

            WSGCMRequest gcmRequestData = new WSGCMRequest();

            GCMData gcmData = new GCMData();

            gcmData.setMessage(searchNotificationBody);
            gcmData.setType(SyncDataType.SEARCH.toString());
            gcmRequestData.setData(gcmData);

            gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);

            connectionFactory.postData(gcmRequestData);
        }
        searchNotificationDAO.removeAllSearchNotification();
    }

    public void sendSearchNotificationByAccountId(String accountId)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");

        List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);
        List<String> cpsIdListOfActiveClient = new ArrayList<String>();
        if (activeClients != null && activeClients.size() > 0)
        {
            for (Client client : activeClients)
            {
                cpsIdListOfActiveClient.add(client.getCpsId());
            }
        }

        WSGCMRequest gcmRequestData = new WSGCMRequest();

        GCMData gcmData = new GCMData();

        gcmData.setMessage(searchNotificationBody);
        gcmData.setType(SyncDataType.SEARCH.toString());
        gcmRequestData.setData(gcmData);
        gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);

        connectionFactory.postData(gcmRequestData);

    }

    @Transactional(rollbackOn = { Exception.class })
    public void schedulerExecution()
    {
        List<SearchNotification> notificationList = searchNotificationDAO.fetchAllNotifications();

        for (SearchNotification notification : notificationList)
        {
            if (!notification.isSent())
            {
                String accountId = notification.getUserId();
                List<Client> activeClients = clientService.fetchAllActiveClientsOfAccount(accountId);

                List<String> cpsIdListOfActiveClient = new ArrayList<String>();
                if (activeClients != null && activeClients.size() > 0)
                {
                    for (Client client : activeClients)
                    {
                        cpsIdListOfActiveClient.add(client.getCpsId());
                    }
                }

                WSGCMRequest gcmRequestData = new WSGCMRequest();

                GCMData gcmData = new GCMData();

                gcmData.setMessage(searchNotificationBody);
                gcmData.setType(SyncDataType.SEARCH.toString());
                gcmRequestData.setData(gcmData);
                gcmRequestData.setRegistrationIds(cpsIdListOfActiveClient);

                connectionFactory.postData(gcmRequestData);

                notification.setSent(true);
                notification.setUpdatedTime(new Date());
                searchNotificationDAO.saveSearchNotification(notification);
            }
            else
            {
                searchNotificationDAO.removeSearchNotification(notification.getId());
            }
        }
    }
}
