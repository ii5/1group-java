package one.group.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import one.group.core.enums.InviteStatus;
import one.group.dao.NativeContactsDAO;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.WSInvites;
import one.group.entities.api.response.WSInvitesList;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSPaginationData;
import one.group.entities.api.response.v2.WSReferralContacts;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.MarketingList;
import one.group.entities.jpa.NativeContacts;
import one.group.services.AccountService;
import one.group.services.InviteLogService;
import one.group.services.MarketingListService;
import one.group.services.NativeContactsService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;

public class NativeContactsServiceImpl implements NativeContactsService
{
    NativeContactsDAO nativeContactsDAO;
    AccountService accountService;
    MarketingListService marketingListService;
    InviteLogService inviteLogService;
    private int totalContactsPerPage = 10;

    public NativeContactsDAO getNativeContactsDAO()
    {
        return nativeContactsDAO;
    }

    public void setNativeContactsDAO(NativeContactsDAO nativeContactsDAO)
    {
        this.nativeContactsDAO = nativeContactsDAO;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public MarketingListService getMarketingListService()
    {
        return marketingListService;
    }

    public void setMarketingListService(MarketingListService marketingListService)
    {
        this.marketingListService = marketingListService;
    }

    public InviteLogService getInviteLogService()
    {
        return inviteLogService;
    }

    public void setInviteLogService(InviteLogService inviteLogService)
    {
        this.inviteLogService = inviteLogService;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "NativeContactsServiceImpl-fetchAllInviteeOfAccount")
    public WSInvites fetchAllInvitesOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        Set<WSInviteContact> recommended = new TreeSet<WSInviteContact>();
        Set<WSInviteContact> other = new TreeSet<WSInviteContact>();

        List<NativeContacts> nativeContactsList = nativeContactsDAO.fetchAllNativeContactsByAccountId(accountId);
        if (nativeContactsList != null && nativeContactsList.size() > 0)
        {
            List<Account> accounts = accountService.fetchAccountOfNativeContactsOfAccount(accountId);
            Map<String, WSInviteContact> activeContacts = accountService.getMapOfPhoneNumberAndStatus(accounts);

            List<MarketingList> marketingList = marketingListService.fetchAllMarketingListWhichAreNativeContacts(accountId);
            Map<String, WSInviteContact> marketingContacts = marketingListService.getMapOfPhoneNumberAndStatus(marketingList);

            List<String> inviteLogList = inviteLogService.fetchAllPhoneNumberOfInvitedLogOfAccount(accountId);
            Map<String, WSInviteContact> invitedContact = inviteLogService.getMapOfPhoneNumberAndStatus(inviteLogList);

            for (NativeContacts nativeContacts : nativeContactsList)
            {
                String phoneNumber = nativeContacts.getPhoneNumber();
                WSInviteContact ic = new WSInviteContact();
                ic.setGroupCount(0);
                if (Utils.isNullOrEmpty(nativeContacts.getFullName()))
                {
                    ic.setName(null);
                }
                else
                {
                    ic.setName(nativeContacts.getFullName());
                }

                ic.setStatus(InviteStatus.NOT_SELECTED);
                ic.setNumber(phoneNumber);

                if (activeContacts.containsKey(phoneNumber))
                {
                    ic.setStatus(InviteStatus.IN_1GROUP);
                }
                else if (invitedContact.containsKey(phoneNumber))
                {
                    ic.setStatus(InviteStatus.INVITE_SENT);
                }
                else if (marketingContacts.containsKey(phoneNumber))
                {
                    ic.setStatus(InviteStatus.SELECTED);
                }
                else
                {
                    ic.setStatus(InviteStatus.NOT_SELECTED);
                }

                if (marketingContacts.containsKey(phoneNumber))
                {
                    ic.setGroupCount(marketingContacts.get(phoneNumber).getGroupCount());
                    recommended.add(ic);
                }
                else
                {
                    other.add(ic);
                }
            }
        }
        WSInvitesList inviteList = new WSInvitesList();
        inviteList.setOther(other);
        inviteList.setRecommended(recommended);

        WSInvites wsInvites = new WSInvites();
        wsInvites.setInvites(inviteList);

        return wsInvites;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "NativeContactsServiceImpl-fetchReferralsOfAccount")
    public WSReferralContacts fetchReferralsOfAccount(String accountId, int currentPageNo, String pgId)
    {

        int offset = totalContactsPerPage * currentPageNo;
        int limit = totalContactsPerPage;

        long startTime = System.currentTimeMillis();
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        WSAccount wsAccountOwner = accountService.fetchAccountDetails(accountId);

        List<Account> accounts = accountService.fetchAccountOfNativeContactsOfAccount(accountId);
        // System.out.println(System.currentTimeMillis() - startTime);

        Set<String> registeredPhoneNumbers = accountService.fetchPhoneNumbersOfNativeContactsOfAccount(accountId);
        if (registeredPhoneNumbers == null || registeredPhoneNumbers.isEmpty())
            registeredPhoneNumbers.add("");
        // System.out.println(System.currentTimeMillis() - startTime);

        long registeredContactCount = 0;
        WSReferralContacts wsReferralContacts = new WSReferralContacts();
        wsReferralContacts.setSyncFinished(wsAccountOwner.isInitialSyncComplete());

        List<WSNativeContact> nativeContactsList = nativeContactsDAO.fetchAllNativeContactsByAccountIdFiltered(accountId, registeredPhoneNumbers, offset, limit);
        System.out.println("nativeContactsList size>>" + nativeContactsList.size());

        for (Account account : accounts)
        {
            if (registeredContactCount < 10)
            {
                WSAccount wsAccount = new WSAccount(account);
                List<WSAccount> wsAccounts = wsReferralContacts.getRegisteredContactsList();
                if (wsAccounts == null)
                {
                    wsAccounts = new ArrayList<WSAccount>();
                }
                wsAccounts.add(wsAccount);
                wsReferralContacts.setRegisteredContactsList(wsAccounts);
            }
            registeredContactCount++;
        }

        wsReferralContacts.setContactsList(nativeContactsList);
        wsReferralContacts.setRegisteredUserCount(registeredContactCount);
        // System.out.println(System.currentTimeMillis() - startTime);

        int totalMessageCount = nativeContactsDAO.fetchAllNativeContactsByAccountIdCount(accountId, registeredPhoneNumbers);
        System.out.println("totalMessageCount>>" + totalMessageCount);
        WSPaginationData pagination = new WSPaginationData(pgId, currentPageNo, totalMessageCount, totalContactsPerPage);
        wsReferralContacts.setPagination(pagination);

        return wsReferralContacts;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "NativeContactsServiceImpl-fetchReferralsOfAccount")
    public WSReferralContacts fetchReferralsOfAccountOld(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");
        List<NativeContacts> nativeContactsList = nativeContactsDAO.fetchAllNativeContactsByAccountId(accountId);
        List<Account> accounts = accountService.fetchAccountOfNativeContactsOfAccount(accountId);
        WSReferralContacts wsReferralContacts = new WSReferralContacts();
        long registeredContactCount = 0;
        for (NativeContacts nativeContact : nativeContactsList)
        {
            String fullName = (nativeContact.getFullName() == null) ? "" : nativeContact.getFullName();
            String companyName = (nativeContact.getCompanyName() == null) ? "" : nativeContact.getCompanyName();
            String phoneNumber = (nativeContact.getPhoneNumber() == null) ? "" : nativeContact.getPhoneNumber();
            List<WSNativeContact> wsNativeContacts = wsReferralContacts.getContactsList();
            boolean isRegisteredUser = false;
            for (Account account : accounts)
            {
                if (account.getPrimaryPhoneNumber().getNumber().equals(phoneNumber))
                {
                    if (registeredContactCount < 10)
                    {
                        WSAccount wsAccount = new WSAccount(account);
                        isRegisteredUser = true;
                        List<WSAccount> wsAccounts = wsReferralContacts.getRegisteredContactsList();
                        if (wsAccounts == null)
                        {
                            wsAccounts = new ArrayList<WSAccount>();
                        }
                        wsAccounts.add(wsAccount);
                        wsReferralContacts.setRegisteredContactsList(wsAccounts);
                    }
                    registeredContactCount++;
                    break;
                }
            }

            if (!isRegisteredUser)
            {
                if (wsNativeContacts == null || wsNativeContacts.isEmpty())
                {
                    wsNativeContacts = new ArrayList<WSNativeContact>();
                    WSNativeContact wsNativeContactNew = new WSNativeContact();
                    wsNativeContactNew.setCompanyName(companyName);
                    wsNativeContactNew.setFullName(fullName);
                    List<String> numbers = new ArrayList<String>();
                    numbers.add(phoneNumber);
                    wsNativeContactNew.setNumbers(numbers);
                    wsNativeContacts.add(wsNativeContactNew);

                }
                else
                {
                    boolean clubbedNumbers = false;
                    for (WSNativeContact wsNativeContact : wsNativeContacts)
                    {

                        if (wsNativeContact.getFullName().toLowerCase().equals(fullName.toLowerCase()))
                        {
                            wsNativeContact.getNumbers().add(phoneNumber);
                            clubbedNumbers = true;
                            break;
                        }
                    }
                    if (!clubbedNumbers)
                    {
                        List<String> numbers = new ArrayList<String>();
                        WSNativeContact wsNativeContactNew = new WSNativeContact();
                        wsNativeContactNew.setFullName(fullName);
                        wsNativeContactNew.setCompanyName(companyName);
                        numbers.add(phoneNumber);
                        wsNativeContactNew.setNumbers(numbers);
                        wsNativeContacts.add(wsNativeContactNew);

                    }
                }
            }
            wsReferralContacts.setContactsList(wsNativeContacts);
        }
        wsReferralContacts.setRegisteredUserCount(registeredContactCount);
        return wsReferralContacts;
    }
}
