package one.group.services.impl;

import java.util.ArrayList;
import java.util.List;

import one.group.dao.LocationAdminDAO;
import one.group.entities.api.response.WSLocation;
import one.group.entities.jpa.LocationAdmin;
import one.group.services.LocationAdminService;

import org.perf4j.aop.Profiled;

public class LocationAdminServiceImpl implements LocationAdminService
{

    private LocationAdminDAO locationAdminDAO;

    public LocationAdminDAO getLocationAdminDAO()
    {
        return locationAdminDAO;
    }

    public void setLocationAdminDAO(LocationAdminDAO locationAdminDAO)
    {
        this.locationAdminDAO = locationAdminDAO;
    }

    @Profiled(tag = "LocationAdminService-getLocalitiesByCityId")
    public List<WSLocation> getLocalitiesByCityId(String cityId)
    {
        List<WSLocation> localities = new ArrayList<WSLocation>();
        try
        {
            List<LocationAdmin> locality = locationAdminDAO.fetchLocalitiesOfCityById(cityId);
            for (int i = 0; i < locality.size(); i++)
            {
                WSLocation localityReference = new WSLocation(locality.get(i), true);
                localities.add(localityReference);
            }
        }
        catch (Exception e)
        {

        }
        return localities;
    }

}
