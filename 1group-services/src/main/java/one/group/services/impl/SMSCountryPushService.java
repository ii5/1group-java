package one.group.services.impl;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.services.helpers.SMSConnectionFactory;
import one.group.services.helpers.SimpleExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SMSCountryPushService
{
    private static final Logger logger = LoggerFactory.getLogger(SMSCountryPushService.class);

    private SMSConnectionFactory smsCountryConnectionFactory;

    private static ExecutorService executorQueue;

    private static BlockingDeque<Runnable> runnableBlockingDeque = new LinkedBlockingDeque<Runnable>();

    static
    {
        executorQueue = Executors.newFixedThreadPool(20);

        for (int i = 0; i < 20; i++)
        {
            executorQueue.submit(new SimpleExecutor(runnableBlockingDeque));
        }
    }

    public SMSConnectionFactory getSmsCountryConnectionFactory()
    {
        return smsCountryConnectionFactory;
    }

    public void setSmsCountryConnectionFactory(SMSConnectionFactory smsCountryConnectionFactory)
    {
        this.smsCountryConnectionFactory = smsCountryConnectionFactory;
    }

    protected void initiateRequest(final String dataToPost)
    {
        try
        {
            if (smsCountryConnectionFactory.isHandle())
            {
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        smsCountryConnectionFactory.postData(dataToPost);
                    }
                };
                runnableBlockingDeque.addLast(t);
            }
            else
            {
                logger.info("SMS is disabled");
            }
        }
        catch (Exception e)
        {
            logger.error("Error while sending SMS. " + dataToPost, e);
        }

    }

}
