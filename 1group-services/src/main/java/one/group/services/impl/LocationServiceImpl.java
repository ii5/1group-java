package one.group.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.Status;
import one.group.dao.LocationAdminDAO;
import one.group.dao.LocationDAO;
import one.group.dao.LocationMatchDAO;
import one.group.dao.NearByLocalityDAO;
import one.group.dao.ParentLocationsDAO;
import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.WSCityLocations;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.WSLocation;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.LocationAdmin;
import one.group.entities.jpa.LocationMatch;
import one.group.entities.jpa.NearByLocation;
import one.group.entities.jpa.ParentLocations;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.CityService;
import one.group.services.LocationService;

import org.perf4j.aop.Profiled;

public class LocationServiceImpl implements LocationService
{
    private LocationDAO locationDAO;

    private LocationAdminDAO locationAdminDAO;

    private LocationMatchDAO locationMatchDAO;

    private NearByLocalityDAO nearByLocalityDAO;

    private ParentLocationsDAO parentLocationDAO;

    private CityService cityService;

    public CityService getCityService()
    {
        return cityService;
    }

    public void setCityService(CityService cityService)
    {
        this.cityService = cityService;
    }

    public ParentLocationsDAO getParentLocationDAO()
    {
        return parentLocationDAO;
    }

    public void setParentLocationDAO(ParentLocationsDAO parentLocationDAO)
    {
        this.parentLocationDAO = parentLocationDAO;
    }

    public NearByLocalityDAO getNearByLocalityDAO()
    {
        return nearByLocalityDAO;
    }

    public void setNearByLocalityDAO(NearByLocalityDAO nearByLocalityDAO)
    {
        this.nearByLocalityDAO = nearByLocalityDAO;
    }

    public LocationMatchDAO getLocationMatchDAO()
    {
        return locationMatchDAO;
    }

    public void setLocationMatchDAO(LocationMatchDAO locationMatchDAO)
    {
        this.locationMatchDAO = locationMatchDAO;
    }

    public LocationAdminDAO getLocationAdminDAO()
    {
        return locationAdminDAO;
    }

    public void setLocationAdminDAO(LocationAdminDAO locationAdminDAO)
    {
        this.locationAdminDAO = locationAdminDAO;
    }

    /**
     * @return the locationDAO
     */
    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    /**
     * @param locationDAO
     *            the locationDAO to set
     */
    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    @Profiled(tag = "LocationService-getLocalities")
    @Transactional
    public WSCityLocations getLocalities(List<String> listCityIds) throws General1GroupException
    {
        List<WSCity> wsCityList = new ArrayList<WSCity>();
        List<Location> locationList = new ArrayList<Location>();

        // Fetch all locations and City
        if (!listCityIds.isEmpty())
        {
            wsCityList = cityService.getAllCitiesByCityIds(listCityIds);
            locationList = locationDAO.fetchAllLocationsByCityIds(listCityIds);
        }
        else
        {
            List<Boolean> publishList = new ArrayList<Boolean>();
            publishList.add(true);
            wsCityList = cityService.getAllCities(publishList);

            List<Status> statusList = new ArrayList<Status>();
            statusList.add(Status.ACTIVE);
            locationList.addAll(locationDAO.fetchAllLocationByStatus(statusList));
        }

        HashMap<String, List<WSLocation>> cityIdVsWslocationMap = new HashMap<String, List<WSLocation>>();
        for (Location location : locationList)
        {
            WSLocation wsLocality = new WSLocation(location);
            String cityId = location.getCity().getId();

            // Map each location against CityID
            if (cityIdVsWslocationMap.containsKey(cityId))
            {
                List<WSLocation> wsMapLocality = cityIdVsWslocationMap.get(cityId);
                wsMapLocality.add(wsLocality);
                cityIdVsWslocationMap.put(cityId, wsMapLocality);
            }
            else
            {
                List<WSLocation> wsListlocation = new ArrayList<WSLocation>();
                wsListlocation.add(wsLocality);
                cityIdVsWslocationMap.put(cityId, wsListlocation);
            }
        }

        // set locations for each City
        for (WSCity wscity : wsCityList)
        {
            String cityId = wscity.getId();
            wscity.setLocations(cityIdVsWslocationMap.get(cityId));
        }

        WSCityLocations wsCityLocation = new WSCityLocations();
        wsCityLocation.setCityLocations(wsCityList);
        return wsCityLocation;
    }

    @Profiled(tag = "LocationService-compareLocations")
    public int compareLocations(List<String> cityIds)
    {
        Collection<LocationAdmin> locationsAdmin = locationAdminDAO.getAllLocations();
        Collection<Location> lct = locationDAO.getAllLocations();

        Collection<String> locationAdmnIds = new ArrayList<String>();
        for (LocationAdmin ld : locationsAdmin)
        {
            locationAdmnIds.add(ld.getId().toString());
        }

        Collection<String> locationIds = new ArrayList<String>();
        for (Location l : lct)
        {
            locationIds.add(l.getId());
        }
        locationIds.removeAll(locationAdmnIds);
        return locationIds.size();
    }

    @Profiled(tag = "LocationService-compareParentLocations")
    public boolean compareParentLocations(List<String> cityIds)
    {
        Collection<LocationAdmin> locationsAdmin = locationAdminDAO.getAllLocationsByCity(cityIds);
        Collection<String> locationAdmnIds = new ArrayList<String>();
        for (LocationAdmin ld : locationsAdmin)
        {
            locationAdmnIds.add(ld.getId().toString());
        }

        Collection<ParentLocations> parentLocations = parentLocationDAO.getAllParentLocationsByCity(cityIds);
        Collection<String> parentLocationIds = new ArrayList<String>();
        for (ParentLocations pid : parentLocations)
        {
            parentLocationIds.add(pid.getId());
        }

        parentLocationIds.containsAll(locationAdmnIds);
        return parentLocationIds.containsAll(locationAdmnIds);
    }

    @Transactional
    @Profiled(tag = "LocationService-updateLiveLocationTable")
    public void updateLiveLocationTable(String cityId)
    {
        locationDAO.updateLiveLocationTable(cityId);
    }

    public void uploadFile()
    {

    }

    @Profiled(tag = "LocationService-getLocalitiesByCityId")
    @Transactional
    public List<WSLocation> getLocalitiesByCityId(String cityId)
    {
        List<WSLocation> localities = new ArrayList<WSLocation>();
        try
        {
            List<Location> locality = locationDAO.fetchLocalitiesOfCityById(cityId);
            for (int i = 0; i < locality.size(); i++)
            {
                WSLocation localityReference = new WSLocation(locality.get(i), true);
                localities.add(localityReference);
            }
        }
        catch (Exception e)
        {

        }
        return localities;
    }

    @Profiled(tag = "LocationService-getMatchingLocation")
    public List<Location> getMatchingLocation(Location ofLocation, Boolean isExact)
    {
        List<Location> locationMatchList = new ArrayList<Location>();
        locationMatchList.add(ofLocation);

        if (isExact == null)
        {
            for (LocationMatch locationMatch : locationMatchDAO.fetchLocationMatchByLocationId(ofLocation.getId()))
            {
                if (!locationMatchList.contains(locationMatch))
                {
                    locationMatchList.add(locationMatch.getMatchLocation());
                }
            }
        }
        else
        {
            for (LocationMatch locationMatch : locationMatchDAO.fetchExactLocationMatchByLocationId(ofLocation.getId(), isExact))
            {
                if (!locationMatchList.contains(locationMatch))
                {
                    locationMatchList.add(locationMatch.getMatchLocation());
                }
            }
        }

        return locationMatchList;
    }

    @Transactional
    @Profiled(tag = "LocationService-getMatchingLocation")
    public List<Location> getMatchingLocation(List<String> locationList, Boolean isExact)
    {
        List<Location> locationMatchList = new ArrayList<Location>();
        locationMatchList.addAll(locationDAO.fetchAllLocations(locationList));

        if (isExact == null)
        {
            for (String locationId : locationList)
            {
                for (LocationMatch locationMatch : locationMatchDAO.fetchLocationMatchByLocationId(locationId))
                {
                    if (!locationMatchList.contains(locationMatch))
                    {
                        locationMatchList.add(locationMatch.getMatchLocation());
                    }
                }
            }
        }
        else
        {
            for (LocationMatch locationMatch : locationMatchDAO.fetchExactLocationMatchByLocationId(locationList, isExact))
            {
                if (!locationMatchList.contains(locationMatch))
                {
                    locationMatchList.add(locationMatch.getMatchLocation());
                }
            }
        }

        return locationMatchList;
    }

    @Profiled(tag = "LocationService-getMatchingLocationIds")
    public List<String> getMatchingLocationIds(List<String> locationList, Boolean isExact)
    {
        List<String> locationMatchList = new ArrayList<String>();
        locationMatchList.addAll(locationList);

        if (isExact == null)
        {
            List<String> locationMatchRawList = locationMatchDAO.fetchLocationMatchByLocationIdList(locationList);

            for (String locationMatchId : locationMatchRawList)
            {
                if (!locationMatchList.contains(locationMatchId))
                {
                    locationMatchList.add(locationMatchId);
                }
            }

        }
        else
        {
            List<String> locationMatchRawIdList = locationMatchDAO.fetchExactLocationMatchIdListByLocationIdList(locationList, isExact);
            for (String locationMatchId : locationMatchRawIdList)
            {
                if (!locationMatchList.contains(locationMatchId))
                {
                    locationMatchList.add(locationMatchId);
                }
            }
        }

        return locationMatchList;
    }

    @Profiled(tag = "LocationService-getMatchingAndNearByLocation")
    public List<Location> getMatchingAndNearByLocation(Location ofLocation, Boolean isExact)
    {
        List<Location> locationMatchList = new ArrayList<Location>();

        locationMatchList.addAll(getMatchingLocation(ofLocation, isExact));
        List<NearByLocation> nearByLocalityList = nearByLocalityDAO.fetchNearByLocalityByLocationId(ofLocation.getId());

        for (NearByLocation locality : nearByLocalityList)
        {
            if (!locationMatchList.contains(locality.getNearByLocation()))
            {
                locationMatchList.add(locality.getNearByLocation());
            }
        }

        return locationMatchList;
    }

    @Profiled(tag = "LocationService-getMatchingAndNearByLocation")
    public List<Location> getMatchingAndNearByLocation(List<String> ofLocations, Boolean isExact)
    {
        List<Location> locationMatchList = new ArrayList<Location>();

        List<LocationMatch> rawLocationMatchList = locationMatchDAO.fetchExactLocationMatchByLocationId(ofLocations, isExact);

        for (LocationMatch locationMatch : rawLocationMatchList)
        {
            if (!locationMatchList.contains(locationMatch.getMatchLocation()))
            {
                locationMatchList.add(locationMatch.getMatchLocation());
            }
        }

        return locationMatchList;
    }

    @Profiled(tag = "LocationService-getAllMatchingAndNearByLocationIds")
    public List<String> getAllMatchingAndNearByLocationIds(List<String> ofLocations, Boolean isExact)
    {
        List<String> locationMatchList = new ArrayList<String>();

        List<LocationMatch> rawLocationMatchList = locationMatchDAO.fetchExactLocationMatchByLocationId(ofLocations, isExact);

        for (LocationMatch locationMatch : rawLocationMatchList)
        {
            if (!locationMatchList.contains(locationMatch.getMatchLocation()))
            {
                locationMatchList.add(locationMatch.getMatchLocation().getId());
            }
        }

        return locationMatchList;
    }

    @Profiled(tag = "LocationService-fetchLocalityNameByLocationId")
    public Location fetchLocalityNameByLocationId(String locationId)
    {
        return locationDAO.fetchById(locationId);
    }

    @Profiled(tag = "LocationService-getCities")
    @Transactional
    public WSCityResult getCities() throws General1GroupException
    {
        List<WSCity> wsCityList = new ArrayList<WSCity>();
        wsCityList = cityService.getAllCities();

        WSCityResult wsCities = new WSCityResult();
        wsCities.setCities(wsCityList);
        return wsCities;
    }

    public WSCityResult getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        List<WSCity> wsCityList = new ArrayList<WSCity>();
        wsCityList = cityService.getAllCities(cityIdList, isPublishedList, sort, filter, start, rows);

        WSCityResult wsCities = new WSCityResult();
        wsCities.setCities(wsCityList);
        return wsCities;
    }

    @Profiled(tag = "locationServiceImpl-fetchLocationByidMap")
    @Transactional(rollbackOn = { Exception.class })
    public Map<String, one.group.entities.api.response.v2.WSLocation> fetchLocationMapById(Set<String> locationIdSet)
    {
        Map<String, one.group.entities.api.response.v2.WSLocation> idVsLocationMap = new HashMap<String, one.group.entities.api.response.v2.WSLocation>();

        if (locationIdSet == null || locationIdSet.isEmpty())
        {
            return idVsLocationMap;
        }

        List<Location> locationList = locationDAO.fetchAllLocations(locationIdSet);
        for (Location location : locationList)
        {
            idVsLocationMap.put(location.getId(), new one.group.entities.api.response.v2.WSLocation(location));
        }
        return idVsLocationMap;
    }
}
