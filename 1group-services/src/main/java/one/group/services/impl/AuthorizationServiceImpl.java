package one.group.services.impl;

import static one.group.core.Constant.OAUTH_ACCESS_TOKEN_VALIDITY;
import static one.group.core.Constant.OAUTH_RESOURCE_ID;
import static one.group.core.Constant.OAUTH_SCOPE;

import java.io.InputStream;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.UriInfo;

import one.group.authenticate.CustomClientDetailsService;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.v2.WSClient;
import one.group.exceptions.movein.ClientProcessException;
import one.group.services.AuthorizationService;
import one.group.services.SyncDBService;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthorizationServiceImpl implements AuthorizationService
{
    private static final Logger logger = LoggerFactory.getLogger(AuthorizationServiceImpl.class);

    private CustomClientDetailsService customClientDetailsService;

    private SyncDBService syncDBService;

    private DefaultTokenServices tokenServices;

    private DefaultOAuth2RequestFactory oAuth2RequestFactory;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11, new SecureRandom());

    public void setoAuth2RequestFactory(DefaultOAuth2RequestFactory oAuth2RequestFactory)
    {
        this.oAuth2RequestFactory = oAuth2RequestFactory;
    }

    public void setTokenServices(DefaultTokenServices tokenService)
    {
        this.tokenServices = tokenService;
    }

    public void setCustomClientDetailsService(CustomClientDetailsService customClientDetailsService)
    {
        this.customClientDetailsService = customClientDetailsService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    /**
     * 
     * Method generates oauth access token for given client and otp
     * 
     * @param nameValuePairs
     * @return OAuth token details Map
     */
    protected Map<String, Object> requestAccessToken(UriInfo uriInfo, List<NameValuePair> nameValuePairs)
    {

        Map<String, Object> tokenDetails = null;

        String url = (uriInfo.getBaseUri().toString()).replace("api/", "") + "oauth/token";
        HttpPost httpPost = new HttpPost(url);
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpResponse httpResponse = null;
        InputStream responseBody;
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httpResponse = httpclient.execute(httpPost);
            responseBody = httpResponse.getEntity().getContent();
            tokenDetails = mapper.readValue(responseBody, Map.class);
            tokenDetails.put("statuscode", httpResponse.getStatusLine().getStatusCode());

        }
        catch (Exception e)
        {
            ResponseBuilder.buildResponse(null, HTTPResponseType.INTERNAL_SERVER_ERROR, true, "Something went really wrong! :(", "500", e);
        }

        return tokenDetails;
    }

    @Profiled(tag = "AuthorizationService-getTokenDetails")
    public OAuth2AccessToken getTokenDetails(String clientId, String otp, String grantType, UriInfo uriInfo)
    {
        Map<String, String> requestParameters = new HashMap<String, String>();
        requestParameters.put("client_id", clientId);
        requestParameters.put("grant_type", grantType);
        requestParameters.put("client_secret", otp);
        ClientCredentialsTokenGranter tokenGranter = new ClientCredentialsTokenGranter(tokenServices, customClientDetailsService, oAuth2RequestFactory);
        ClientDetails clientDetails = customClientDetailsService.loadClientByClientId(clientId);
        TokenRequest request = oAuth2RequestFactory.createTokenRequest(requestParameters, clientDetails);
        OAuth2AccessToken token = tokenGranter.grant(grantType, request);
        return token;

    }

    @Profiled(tag = "AuthorizationService-createOauthClient")
    public void createOauthClient(String otp, String grantType, WSClient wsClient, String authorities) throws ClientProcessException
    {
        // Commenting conditional check because signin_in will always create a
        // new client.
        // if (!customClientDetailsService.isClientValid(wsClient.getId()))
        // {
        /* create new oauth client */
        String hashedOtp = passwordEncoder.encode(otp);
        try
        {
            customClientDetailsService.saveClientDetail(wsClient.getId(), OAUTH_RESOURCE_ID, hashedOtp, OAUTH_SCOPE, grantType, null, authorities, OAUTH_ACCESS_TOKEN_VALIDITY,
                    OAUTH_ACCESS_TOKEN_VALIDITY, null, null);

            int maxAckUpdateIndex = syncDBService.createClient(wsClient.getId());
            System.out.println("Client Max maxAckUpdateIndex===="+maxAckUpdateIndex);
            wsClient.setInitialSyncIndex(maxAckUpdateIndex);

        }
        catch (PersistenceException pe)
        {
            logger.warn("Error saving client.", pe);
        }

        // }
    }
}
