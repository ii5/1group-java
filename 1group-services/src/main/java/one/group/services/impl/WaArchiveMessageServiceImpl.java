package one.group.services.impl;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import one.group.dao.WaMessagesArchiveDAO;
import one.group.entities.jpa.WaMessagesArchive;
import one.group.services.WaArchiveMessageService;

public class WaArchiveMessageServiceImpl implements WaArchiveMessageService
{
	private WaMessagesArchiveDAO waMessageArchiveDAO;
	
	public WaMessagesArchiveDAO getWaMessageArchiveDAO() 
	{
		return waMessageArchiveDAO;
	}
	
	public void setWaMessageArchiveDAO(WaMessagesArchiveDAO waMessageArchiveDAO) 
	{
		this.waMessageArchiveDAO = waMessageArchiveDAO;
	}
	
	@Transactional(value=TxType.REQUIRES_NEW)
	public void saveMessagesToDB(List<WaMessagesArchive> messagesToUpdate) 
	{
		waMessageArchiveDAO.saveWAMessagesArchive(messagesToUpdate);
	}

}
