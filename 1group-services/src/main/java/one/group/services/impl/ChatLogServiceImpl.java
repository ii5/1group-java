package one.group.services.impl;

import javax.transaction.Transactional;

import one.group.dao.ChatLogDAO;
import one.group.entities.api.response.WSChatMessage;
import one.group.jpa.dao.ChatLogJpaDAO;
import one.group.services.ChatLogService;

public class ChatLogServiceImpl implements ChatLogService
{
    private ChatLogDAO chatLogDAO;

    private ChatLogJpaDAO chatLogJpaDAO;

    public ChatLogDAO getChatLogDAO()
    {
        return chatLogDAO;
    }

    public ChatLogJpaDAO getChatLogJpaDAO()
    {
        return chatLogJpaDAO;
    }

    public void setChatLogJpaDAO(ChatLogJpaDAO chatLogJpaDAO)
    {
        this.chatLogJpaDAO = chatLogJpaDAO;
    }

    public void setChatLogDAO(ChatLogDAO chatLogDAO)
    {
        this.chatLogDAO = chatLogDAO;
    }

    @Transactional(rollbackOn = { Exception.class })
    public void saveChatLog(WSChatMessage wsChatMessage, String toAccountId)
    {

    }
}
