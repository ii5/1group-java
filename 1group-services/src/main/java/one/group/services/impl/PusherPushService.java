package one.group.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.core.enums.PushServerType;
import one.group.services.helpers.AbstractPushService;
import one.group.services.helpers.MoveinEnumGsonTypeAdapterFactory;
import one.group.services.helpers.PushServiceAction;
import one.group.services.helpers.PushServiceObject;
import one.group.services.helpers.PusherConnectionFactory;
import one.group.services.helpers.SimpleExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pusher.rest.Pusher;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PusherPushService extends AbstractPushService
{

    private PusherConnectionFactory connectionFactory;

    private Pusher pusher;

    private static final Logger logger = LoggerFactory.getLogger(PusherPushService.class);

    private static BlockingDeque<Runnable> runnableBlockingDeque = new LinkedBlockingDeque<Runnable>();

    private static ExecutorService executorQueue;

    public PusherConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(final PusherConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    private Pusher getPusher()
    {
        if (pusher == null)
        {
            pusher = connectionFactory.getResource();
        }

        return pusher;
    }

    public void initialise()
    {
        if (connectionFactory != null)
        {
            int poolSize = connectionFactory.getPoolSize();
            executorQueue = Executors.newFixedThreadPool(poolSize);

            for (int i = 0; i < poolSize; i++)
            {
                executorQueue.submit(new SimpleExecutor(runnableBlockingDeque));
            }
        }
        else
        {
            logger.warn("Connection Factory is null, all default configurations will be taken into consideration.");
        }
    }

    @Override
    protected boolean additionalConfigsPresent(PushServiceObject config)
    {
        if (config.getPusherEventName() == null)
        {
            logger.error("Pusher event name needs to be present.");
            return false;
        }

        if (config.getPusherChannelList() == null)
        {
            logger.error("Pusher channel list is empty! Why did you call this service if you did not want to send a message to anyone?");
            return false;
        }

        return true;
    }

    @Override
    protected void initiateRequest(final PushServiceObject config)
    {
        try
        {
            logger.info("Entered " + this.getClass().getSimpleName());
            if (config.getServiceList().contains(PushServerType.PUSHER))
            {
                List<String> channels = config.getPusherChannelList();
                logger.info(PushServerType.PUSHER + " pushing [" + config.getData() + "] to ( " + channels.size() + ")" + channels + ".");
                List<String> batchChannelList = new ArrayList<String>();
                Iterator<String> it = channels.iterator();
                int i = 0;
                while (it.hasNext())
                {
                    final String channel = it.next();
                    batchChannelList.add(channel);
                    if (i % (connectionFactory.getBatchSize() - 1) == 0 || !it.hasNext())
                    {
                        logger.debug(batchChannelList + "");
                        PushServiceAction t = new PushServiceAction(batchChannelList, config, getPusher(), getGson(), connectionFactory.getRetryCount());
                        runnableBlockingDeque.addLast(t);
                        batchChannelList = new ArrayList<String>();
                    }

                    i++;
                }
            }

            if (getSuccessor() != null)
            {
                logger.debug("Passing request to successor: " + getSuccessor().getClass().getSimpleName());
                getSuccessor().forwardRequest(config);
            }
        }
        catch (Exception e)
        {
            logger.error("Exception during forwaring request to Pusher.", e);
        }
    }

    private Gson getGson()
    {
        GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).registerTypeAdapterFactory(new MoveinEnumGsonTypeAdapterFactory());
        Gson gson = builder.create();

        return gson;
    }

    public static void main(String[] args)
    {

    }
}
