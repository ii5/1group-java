package one.group.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.services.SMSService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static one.group.core.Constant.APPLICATION_URLENCODED;
import static one.group.core.Constant.CONTENT_TYPE;
import static one.group.core.Constant.UTF8_ENCODING;

/**
 * The Class SMSServiceImpl.
 * 
 * @author shweta.sankhe
 */
public class SMSServiceImpl implements SMSService
{

    private static final Logger logger = LoggerFactory.getLogger(SMSServiceImpl.class);

    private static final String SMS_OTP_MESSAGE = "sms.otp.message";

    /** The balance url. */
    private static String BALANCE_URL = "http://api.smscountry.com/SMSCwebservice_User_GetBal.asp";

    /** The sms url. */
    private static String SMSCOUNTRY_SMS_URL = "http://smscountry.com/SMSCwebservice_Bulk.aspx";

    private static String NETCORE_SMS_URL = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi";

    /** The bulk delivery report url. */
    private static String BULK_DELIVERY_REPORT_URL = "http://api.smscountry.com/smscwebservices_bulk_reports.aspx";

    /** The mtype. */
    private static String mtype = "N";

    /** The dr. */
    private static String DR = "Y";

    /** The user. */
    private String netcoreUser;

    /** The password. */
    private String netcorePassword;

    /** The sender id. */
    private String netcoreSenderId;

    /** The user. */
    private String smsCountryUser;

    /** The password. */
    private String smsCountryPassword;

    /** The sender id. */
    private String smsCountrySenderId;

    private MessageResouceService messageResouceService;

    /**
     * Sets the user. * @param user the new user
     */

    public void setNetcoreUser(String netcoreUser)
    {
        this.netcoreUser = netcoreUser;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the new password
     */
    public void setNetcorePassword(String netcorePassword)
    {
        this.netcorePassword = netcorePassword;
    }

    /**
     * Sets the sender id.
     * 
     * @param senderId
     *            the new sender id
     */
    public void setNetcoreSenderId(String netcoreSenderId)
    {
        this.netcoreSenderId = netcoreSenderId;
    }

    public void setSmsCountryUser(String smsCountryUser)
    {
        this.smsCountryUser = smsCountryUser;
    }

    public void setSmsCountryPassword(String smsCountryPassword)
    {
        this.smsCountryPassword = smsCountryPassword;
    }

    public void setSmsCountrySenderId(String smsCountrySenderId)
    {
        this.smsCountrySenderId = smsCountrySenderId;
    }

    public String getNetcoreUser()
    {
        return netcoreUser;
    }

    public String getNetcorePassword()
    {
        return netcorePassword;
    }

    public String getNetcoreSenderId()
    {
        return netcoreSenderId;
    }

    public String getSmsCountryUser()
    {
        return smsCountryUser;
    }

    public String getSmsCountryPassword()
    {
        return smsCountryPassword;
    }

    public String getSmsCountrySenderId()
    {
        return smsCountrySenderId;
    }

    @Profiled(tag = "SMSService-sendSingleSMS")
    public void sendSingleSMS(final String mobileNumber, final String otp) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");
        String message = messageResouceService.getMessage(SMS_OTP_MESSAGE, otp);
        final String smsCountryPostData = "User=" + URLEncoder.encode(smsCountryUser, UTF8_ENCODING) + "&passwd=" + smsCountryPassword + "&mobilenumber=" + newMobileNumber + "&message="
                + URLEncoder.encode(message, UTF8_ENCODING) + "&sid=" + smsCountrySenderId + "&mtype=" + mtype + "&DR=" + DR;

        final String netcorePostData = "feedid=" + netcoreSenderId + "&username=" + netcoreUser + "&password=" + netcorePassword + "&To=" + newMobileNumber + "&Text="
                + URLEncoder.encode(message, UTF8_ENCODING);

        // If You Are Behind The Proxy Server Set IP And PORT else Comment Below
        // 4 Lines
        // Properties sysProps =
        // System.getProperties();//sysProps.put("proxySet",
        // "true");//sysProps.put("proxyHost", "Proxy Ip");
        // sysProps.put("proxyPort", "PORT");
        logger.info("Sending OTP message[" + message + "] . to Mobile Number" + mobileNumber);
        new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    createRequestAndPrintDecodedString(SMSCOUNTRY_SMS_URL, HTTPRequestMethodType.POST.toString(), smsCountryPostData);
                    createRequestAndPrintDecodedString(NETCORE_SMS_URL, HTTPRequestMethodType.POST.toString(), netcorePostData);

                }
                catch (IOException e)
                {
                    logger.error("Error While sending OTP", e);
                }
            }
        }.start();

    }

    @Profiled(tag = "SMSService-sendSMS")
    public void sendSMS(final String mobileNumber, final String smsContent) throws IOException
    {
        String newMobileNumber = mobileNumber.replaceAll("\\+", "");

        final String smsCountryPostData = "User=" + URLEncoder.encode(smsCountryUser, UTF8_ENCODING) + "&passwd=" + smsCountryPassword + "&mobilenumber=" + newMobileNumber + "&message="
                + URLEncoder.encode(smsContent, UTF8_ENCODING) + "&sid=" + smsCountrySenderId + "&mtype=" + mtype + "&DR=" + DR;

        // If You Are Behind The Proxy Server Set IP And PORT else Comment Below
        // 4 Lines
        // Properties sysProps =
        // System.getProperties();//sysProps.put("proxySet",
        // "true");//sysProps.put("proxyHost", "Proxy Ip");
        // sysProps.put("proxyPort", "PORT");

        new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    createRequestAndPrintDecodedString(SMSCOUNTRY_SMS_URL, HTTPRequestMethodType.POST.toString(), smsCountryPostData);
                }
                catch (IOException e)
                {
                    logger.error("Error While sending OTP", e);
                }
            }
        }.start();

    }

    /**
     * Gets the SMS credit balance.
     * 
     * @return the SMS credit balance
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void getSMSCreditBalance() throws IOException
    {
        final String smsCountryPostData = "User=" + URLEncoder.encode(smsCountryUser, UTF8_ENCODING) + "&passwd=" + smsCountryPassword;
        createRequestAndPrintDecodedString(BALANCE_URL, HTTPRequestMethodType.POST.toString(), smsCountryPostData);
    }

    /**
     * Read from delivery report.
     * 
     * @param fromDate
     *            the from date
     * @param toDate
     *            the to date
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    // TODO: can be used later for report
    private void readFromDeliveryReport(final String fromDate, final String toDate) throws IOException
    {
        String smsCountryPostData = "User=" + URLEncoder.encode(smsCountryUser, UTF8_ENCODING) + "&passwd=" + smsCountryPassword + "&fromdate=" + fromDate + "&todate=" + toDate;
        // "user=xxxx&passwd=xxxx&fromdate=DD/MM/YYYY 00:00:00
        // &todate=DD/MM/YYYY 23:59:59";
        createRequestAndPrintDecodedString(BULK_DELIVERY_REPORT_URL, HTTPRequestMethodType.POST.toString(), smsCountryPostData);
    }

    /**
     * Prints the decoded string.
     * 
     * @param urlconnection
     *            the urlconnection
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void printDecodedString(final HttpURLConnection urlconnection) throws IOException
    {
        String retval = "";
        BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));
        String decodedString;
        while ((decodedString = in.readLine()) != null)
        {
            retval = decodedString;
        }
        in.close();
        System.out.println(retval);
    }

    /**
     * Creates the request and print decoded string.
     * 
     * @param urlName
     *            the url name
     * @param requestMethod
     *            the request method
     * @param postData
     *            the post data
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void createRequestAndPrintDecodedString(final String urlName, final String requestMethod, final String postData) throws IOException
    {
        URL url = new URL(urlName);
        HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
        urlconnection.setRequestMethod(requestMethod);
        urlconnection.setRequestProperty(CONTENT_TYPE, APPLICATION_URLENCODED);
        urlconnection.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
        out.write(postData);
        out.close();
        printDecodedString(urlconnection);
    }

    public void setMessageResouceService(MessageResouceService messageResouceService)
    {
        this.messageResouceService = messageResouceService;
    }

    public void sendSingleSMSPooled(String mobileNumber, String otp) throws IOException
    {
        // TODO Auto-generated method stub

    }

    // public static void main( String[] args) throws Exception{
    // String postData="";
    // String retval = "";
    // String User ="ii5";
    // String passwd = "iypa9zMXY4@";
    // String mobilenumber = "919321964594";

    // String message =
    // "Your one time password for verifying your mobile number on \"WEBSITE\" is XXXX and it's valid for next 2 mins. Please do not share this with anyone.";

    // message = message.replaceAll("XXXX",
    // "1234" );
    // String sid = "MOVEIN";
    // postData += "User=" + URLEncoder.encode(User,UTF8_ENCODING) + "&passwd="
    // + passwd + "&mobilenumber=" + mobilenumber
    // + "&message=" + URLEncoder.encode(message,UTF8_ENCODING) + "&sid=" + sid
    // + "&mtype=" + mtype + "&DR=" + DR;
    // URL url = new URL(SMS_URL);
    // HttpURLConnection urlconnection = (HttpURLConnection)
    // url.openConnection();
    // urlconnection.setRequestMethod(HTTPRequestMethodType.POST.toString());
    // urlconnection.setRequestProperty(CONTENT_TYPE,APPLICATION_URLENCODED);
    // urlconnection.setDoOutput(true);
    // OutputStreamWriter out = new
    // OutputStreamWriter(urlconnection.getOutputStream());
    // out.write(postData);
    // out.close();
    // BufferedReader in = new BufferedReader( new
    // InputStreamReader(urlconnection.getInputStream()));
    // String decodedString;
    // while ((decodedString = in.readLine()) != null) {
    // retval += decodedString;
    // }
    // in.close();
    // System.out.println(retval);
    // }

}
