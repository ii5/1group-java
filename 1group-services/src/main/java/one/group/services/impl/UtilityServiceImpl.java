package one.group.services.impl;

import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import one.group.core.enums.CmsType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSWhatsAppConnectRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSWhatsAppConnect;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountService;
import one.group.services.CmsService;
import one.group.services.MarketingListService;
import one.group.services.PropertyListingService;
import one.group.services.UtilityService;
import one.group.services.helpers.CmsConnectionFactory;
import one.group.services.helpers.CmsServiceObject;
import one.group.services.helpers.NotificationConfiguration;
import one.group.utils.Utils;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.perf4j.aop.Profiled;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class UtilityServiceImpl implements UtilityService
{
    private static final Logger logger = Logger.getLogger(UtilityServiceImpl.class);
    private CmsService cmsService;

    private CmsConnectionFactory connectionFactory;

    private AccountService accountService;

    private NotificationConfiguration notificationConfiguration;

    private MarketingListService marketingListService;

    private PropertyListingService propertyListingService;

    public MarketingListService getMarketingListService()
    {
        return marketingListService;
    }

    public void setMarketingListService(MarketingListService marketingListService)
    {
        this.marketingListService = marketingListService;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public NotificationConfiguration getNotificationConfiguration()
    {
        return notificationConfiguration;
    }

    public void setNotificationConfiguration(NotificationConfiguration notificationConfiguration)
    {
        this.notificationConfiguration = notificationConfiguration;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public CmsConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(CmsConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public CmsService getCmsService()
    {
        return cmsService;
    }

    public void setCmsService(CmsService cmsService)
    {
        this.cmsService = cmsService;
    }

    @Profiled(tag = "UtilityService-uploadFile")
    public String uploadFile(File fullPhotoFile, String photoName, String imageType) throws General1GroupException
    {
        String key = "";
        String bucket = connectionFactory.getBucket();

        if ("fullPhoto".equals(imageType))
        {
            key = connectionFactory.getImageFolderAPI() + photoName;
        }
        else if ("thumbnail".equals(imageType))
        {
            key = connectionFactory.getThumbnailFolderAPI() + photoName;
        }

        CmsServiceObject config = new CmsServiceObject();
        config.setBucketName(bucket);
        config.setAwsFile(fullPhotoFile);
        config.setKey(key);
        config.setCmsType(CmsType.AWS);
        String awsFileUploadedUrl = cmsService.UploadFile(config);
        return awsFileUploadedUrl;
    }

    @Profiled(tag = "UtilityService-isInvited")
    private boolean isInvited(String inviteStatus, int groupCount, int newsFeedCount, boolean everSync, boolean inMarketingList, boolean hasPropertyListing) throws JsonMappingException,
            JsonGenerationException, IOException, RequestValidationException
    {
        validate("groupCountThreshold", String.valueOf(notificationConfiguration.getGroupCountThreshold()), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        validate("newsFeedCountThreshold", String.valueOf(notificationConfiguration.getNewsFeedCountThreshold()), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        int groupCountThreshold = Integer.valueOf(notificationConfiguration.getGroupCountThreshold());
        int newsFeedThreshold = Integer.valueOf(notificationConfiguration.getNewsFeedCountThreshold());
        if (inviteStatus.equalsIgnoreCase("blacklist"))
        {
            return false;
        }
        else if (inviteStatus.equalsIgnoreCase("whitelist"))
        {
            return true;
        }
        else if (inMarketingList || hasPropertyListing)
        {
            return true;
        }
        else
        {
            if (groupCount >= groupCountThreshold && newsFeedCount >= newsFeedThreshold)
            {
                return true;
            }
            else if (everSync)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    @Profiled(tag = "UtilityService-getWhatsAppInfoFromPhoneNumber")
    private WSWhatsAppConnect getWhatsAppInfoFromPhoneNumber(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        String baseUrl = notificationConfiguration.getWhatsAppSyncUrl();

        WSWhatsAppConnectRequest request = new WSWhatsAppConnectRequest();
        request.setPhoneNumber(phoneNumber);

        Response response = callWebEndPoint(baseUrl, null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);

        WSResponse wsResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSWhatsAppConnect result = (WSWhatsAppConnect) Utils.getInstanceFromJson(Utils.getJsonString(wsResponse.getData()), WSWhatsAppConnect.class);

        return result;
    }

    private Response callWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {
        ClientConfig config = new ClientConfig();
        // config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }

}
