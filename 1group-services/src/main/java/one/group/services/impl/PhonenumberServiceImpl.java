package one.group.services.impl;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.dao.AccountDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.response.WSPhoneNumber;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.PhoneNumber;
import one.group.services.PhonenumberService;
import one.group.utils.validation.Validation;

import org.apache.log4j.Logger;
import org.perf4j.aop.Profiled;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PhonenumberServiceImpl implements PhonenumberService
{
    private static final Logger logger = Logger.getLogger(PhonenumberServiceImpl.class);

    private PhoneNumberDAO phoneNumberDAO;

    private AccountDAO accountDAO;

    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }

    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    @Profiled(tag = "PhoneNumberService-fetchPhoneNumber")
    public WSPhoneNumber fetchPhoneNumber(String mobileNumber, boolean populateAssociatedAccount)
    {
        PhoneNumber number = phoneNumberDAO.fetchPhoneNumber(mobileNumber);
        if (number == null)
        {
            return null;
        }

        WSPhoneNumber wsNumber = new WSPhoneNumber(number);

        if (populateAssociatedAccount)
        {
            Account account = accountDAO.fetchAccountByPhoneNumber(mobileNumber);
            wsNumber.setAssociatedAccountId(account == null ? null : account.getIdAsString());
        }

        return wsNumber;
    }

    @Transactional(rollbackOn = { PersistenceException.class })
    @Profiled(tag = "PhoneNumberService-savePhoneNumber")
    public void savePhoneNumber(String mobileNumber, PhoneNumberType type, PhoneNumberSource source)
    {
        Validation.notNull(mobileNumber, "Mobile Number should not be null.");
        Validation.isTrue(type.equals(PhoneNumberType.MOBILE), "Only mobile numbers supported as of now.");

        PhoneNumber number = new PhoneNumber();
        try
        {
            number.setNumber(mobileNumber);
            number.setType(type);
            number.setSource(source);

            phoneNumberDAO.savePhonenumber(number);
        }
        catch (PersistenceException pe)
        {
            logger.warn("Ignoring: Error while persisting phone number " + number + ". " + pe.getMessage());
            throw pe;
        }
    }

}
