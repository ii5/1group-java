package one.group.services.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.PipelineEntityScoreComparator;
import one.group.core.Constant;
import one.group.core.enums.AdminStatus;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.EntityFieldName;
import one.group.core.enums.EntityType;
import one.group.core.enums.GroupSource;
import one.group.core.enums.HintType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.PropertyUpdateActionType;
import one.group.core.enums.Rooms;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.AccountDAO;
import one.group.dao.AdminDAO;
import one.group.dao.AmenityDAO;
import one.group.dao.BroadcastDAO;
import one.group.dao.BroadcastTagDAO;
import one.group.dao.GeneralDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.LocationDAO;
import one.group.dao.MessageDAO;
import one.group.dao.StarDAO;
import one.group.dao.StarredBroadcastDAO;
import one.group.dao.SubscriptionDAO;
import one.group.entities.api.request.v2.WSBroadcastTag;
import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSubscribeEntity;
import one.group.entities.api.response.v2.WSAccountUtilityDataHolder;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSBroadcastListResult;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSPaginationData;
import one.group.entities.api.response.v2.WSTag;
import one.group.entities.cache.BroadcastKey;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.Search;
import one.group.entities.jpa.StarredBroadcast;
import one.group.entities.jpa.helpers.BroadcastCountObject;
import one.group.entities.jpa.helpers.BroadcastObject;
import one.group.entities.jpa.helpers.BroadcastTagCountObject;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.BroadcastExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.BadRequestException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ChatThreadService;
import one.group.services.CityService;
import one.group.services.GroupsService;
import one.group.services.LocationService;
import one.group.services.PaginationService;
import one.group.services.SubscriptionService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.commons.lang.StringUtils;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author ashishthorat
 * 
 */
public class BroadcastServiceImpl implements BroadcastService
{
    private StarDAO starDAO;

    private static final Logger logger = LoggerFactory.getLogger(BroadcastServiceImpl.class);

    private BroadcastDAO broadcastDAO;

    private AccountDAO accountDAO;

    private LocationDAO locationDAO;

    private AmenityDAO amenityDAO;

    private SubscriptionDAO subscriptionDAO;

    private ChatThreadService chatThreadService;

    private SubscriptionService subscriptionService;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private GeneralDAO generalDAO;

    private PaginationService paginationService;

    private BroadcastTagDAO broadcastTagDAO;

    private StarredBroadcastDAO starredBroadcastDAO;

    private MessageDAO messageDAO;

    private AccountRelationService accountRelationService;

    private LocationService locationService;

    private AccountService accountService;

    private GroupsService groupsService;

    private GroupsDAO groupsDAO;

    private AdminDAO adminDAO;

    private CityService cityService;

    public AdminDAO getAdminDAO()
    {
        return adminDAO;
    }

    public void setAdminDAO(AdminDAO adminDAO)
    {
        this.adminDAO = adminDAO;
    }

    public GroupsDAO getGroupsDAO()
    {
        return groupsDAO;
    }

    public void setGroupsDAO(GroupsDAO groupsDAO)
    {
        this.groupsDAO = groupsDAO;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public StarredBroadcastDAO getStarredBroadcastDAO()
    {
        return starredBroadcastDAO;
    }

    public void setStarredBroadcastDAO(StarredBroadcastDAO starredBroadcastDAO)
    {
        this.starredBroadcastDAO = starredBroadcastDAO;
    }

    public BroadcastTagDAO getBroadcastTagDAO()
    {
        return broadcastTagDAO;
    }

    public void setBroadcastTagDAO(BroadcastTagDAO broadcastTagDAO)
    {
        this.broadcastTagDAO = broadcastTagDAO;
    }

    public GeneralDAO getGeneralDAO()
    {
        return generalDAO;
    }

    public void setGeneralDAO(GeneralDAO generalDAO)
    {
        this.generalDAO = generalDAO;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public void setBroadcastDAO(BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    public AmenityDAO getAmenityDAO()
    {
        return amenityDAO;
    }

    public void setAmenityDAO(AmenityDAO amenityDAO)
    {
        this.amenityDAO = amenityDAO;
    }

    public SubscriptionDAO getSubscriptionDAO()
    {
        return subscriptionDAO;
    }

    public void setSubscriptionDAO(SubscriptionDAO subscriptionDAO)
    {
        this.subscriptionDAO = subscriptionDAO;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public StarDAO getStarDAO()
    {
        return starDAO;
    }

    public void setStarDAO(StarDAO starDAO)
    {
        this.starDAO = starDAO;
    }

    public ChatThreadService getChatThreadService()
    {
        return chatThreadService;
    }

    public void setChatThreadService(ChatThreadService chatThreadService)
    {
        this.chatThreadService = chatThreadService;
    }

    public PaginationService getPaginationService()
    {
        return paginationService;
    }

    public void setPaginationService(PaginationService paginationService)
    {
        this.paginationService = paginationService;
    }

    public CityService getCityService()
    {
        return cityService;
    }

    public void setCityService(CityService cityService)
    {
        this.cityService = cityService;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "BroadcastService-saveBroadcast")
    public WSBroadcast saveBroadcast(boolean isAdmin, String description, String type, String subType, String area, String commisionType, String rooms, String localityId, List<String> amenities,
            String salePrice, String rentPrice, String status, String propertyMarket, String isHot, PropertyUpdateActionType action, String accountId, String broadcastId, String externalListingTime,
            String externalSource, String customSublocation, String broadcastType) throws General1GroupException, ParseException
    {
        /*
         * Broadcast newBroadcast = null; Broadcast broadcast = null; String
         * oldShortReference = null; String shortReference = null; Account
         * account = null; SyncActionType syncActionType = SyncActionType.ADD;
         * boolean isAdminOrOwner = false; if (broadcastId != null) { broadcast
         * = broadcastDAO.fetchBroadcast(broadcastId); // only owner and admin
         * can edit broadcast isAdminOrOwner = isAdmin ||
         * accountId.equals(broadcast.getAccount().getIdAsString()) ? true :
         * false; if (!isAdminOrOwner) { throw new
         * General1GroupException(HTTPResponseType.FORBIDDEN, "403", false,
         * "You can edit only your own property"); } if (broadcast == null) {
         * throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "404",
         * false, "Property for id " + broadcastId + " not found"); } if
         * (broadcast.getStatus() != BroadcastStatus.ACTIVE) { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false,
         * "Only active property listings can be edited"); } } newBroadcast =
         * new Broadcast(); newBroadcast.setStatus(BroadcastStatus.ACTIVE);
         * newBroadcast = broadcastId != null ? broadcast : newBroadcast; if
         * (broadcastId == null) { do { shortReference =
         * Utils.randomString(Constant
         * .SHORT_REFERENCE_CHARSET_UPPERCASE_ALPHANUMERIC, 6, false); Broadcast
         * tbroadcast = broadcastDAO.fetchByShortReference(shortReference); if
         * (tbroadcast != null) { shortReference = null; } } while
         * (shortReference == null);
         * newBroadcast.setShortReference(shortReference);
         * newBroadcast.setCreatedBy(accountId); } // for administration client
         * adding broadcasts if (externalListingTime != null && externalSource
         * != null) { newBroadcast.setExternalSource(externalSource);
         * SimpleDateFormat sdf = new
         * SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); Long time =
         * Long.valueOf(externalListingTime); Date getExtTime = new Date(time);
         * SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
         * "yyyy-MM-dd HH:mm:ss.SSS"); String currentDate =
         * simpleDateFormat.format(new Date()); Date current =
         * sdf.parse(currentDate); long diff = current.getTime() -
         * getExtTime.getTime(); long diffDays = diff / (24 * 60 * 60 * 1000);
         * long timeToBeClosed = Constant.TIME_TO_BE_EXPIRE_IN_DAYS; if
         * (getExtTime.getTime() > current.getTime()) { getExtTime = new Date();
         * } if (diffDays > timeToBeClosed) { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false,
         * "Property listing has been closed"); }
         * newBroadcast.setCreatedTime(getExtTime); } else if (broadcastId !=
         * null && broadcast.getExternalSource() != null) {
         * newBroadcast.setExternalSource(broadcast.getExternalSource()); }
         * BroadcastType broadcastTypeValue = broadcastType != null ?
         * BroadcastType.convert(broadcastType) : null;
         * newBroadcast.setBroadcastType(this.<BroadcastType>
         * returnArg1OnNull(newBroadcast.getBroadcastType(),
         * broadcastTypeValue)); PropertyMarket propertyMarketValue =
         * propertyMarket != null ? PropertyMarket.convert(propertyMarket) :
         * null; newBroadcast.setPropertyMarket(this.<PropertyMarket>
         * returnArg1OnNull(newBroadcast.getPropertyMarket(),
         * propertyMarketValue)); // set description
         * newBroadcast.setDescription(description != null ? description :
         * broadcast.getDescription());
         * newBroadcast.setDescriptionLength(description != null ?
         * description.length() : newBroadcast.getDescriptionLength());
         * CommissionType commisionTypeValue = commisionType != null ?
         * CommissionType.convert(commisionType) : null;
         * newBroadcast.setCommissionType(this.<CommissionType>
         * returnArg1OnNull(newBroadcast.getCommissionType(),
         * commisionTypeValue)); // set rooms newBroadcast.setRooms(rooms !=
         * null ? rooms : broadcast.getRooms());
         * newBroadcast.setSalePrice(salePrice != null ? new Long(salePrice) :
         * newBroadcast.getSalePrice()); newBroadcast.setRentPrice(rentPrice !=
         * null ? new Long(rentPrice) : newBroadcast.getRentPrice()); // set
         * area newBroadcast.setArea(area != null ? new Long(area) :
         * newBroadcast.getArea()); PropertyType propertyTypeValue = type !=
         * null ? PropertyType.convert(type) : null;
         * newBroadcast.setPropertyType(this.<PropertyType>
         * returnArg1OnNull(newBroadcast.getPropertyType(), propertyTypeValue));
         * PropertySubType propertySubTypeValue = subType != null ?
         * PropertySubType.convert(subType) : null;
         * newBroadcast.setSubType(this.<PropertySubType>
         * returnArg1OnNull(newBroadcast.getSubType(), propertySubTypeValue));
         * // set account, check if admin is requesting api , if then dont set
         * // account if (!isAdmin) { account =
         * accountDAO.fetchAccountById(accountId);
         * newBroadcast.setAccount(account); } // set locality Location locality
         * = localityId != null ? locationDAO.fetchById(localityId) :
         * broadcast.getLocation(); if (locality == null) { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true,
         * "No location found for id " + localityId); }
         * newBroadcast.setLocation(this.<Location>
         * returnArg1OnNull(newBroadcast.getLocation(), locality)); // set
         * amenities assigned to broadcast
         * newBroadcast.setAmenities(this.addAmenities(amenities,
         * newBroadcast)); // set is hot default to false
         * newBroadcast.setIsHot(isHot != null ? Boolean.valueOf(isHot) :
         * false); // renew property if in expired state if (action != null) {
         * newBroadcast.setShortReference(oldShortReference); syncActionType =
         * SyncActionType.RENEW; renewBroadcast(newBroadcast, action); } //
         * create Broadcast if (broadcastId == null) {
         * newBroadcast.setCreatedTime(new Date());
         * broadcastDAO.saveBroadcast(newBroadcast); } else { syncActionType =
         * SyncActionType.EDIT;
         * newBroadcast.setCreatedTime(newBroadcast.getCreatedTime());
         * newBroadcast.setUpdatedBy(accountId); newBroadcast.setUpdatedTime(new
         * Date()); newBroadcast.setRenewalTime(new Date());
         * broadcastDAO.saveBroadcast(newBroadcast); }
         */

        WSBroadcast wsBroadcst = new WSBroadcast();
        // wsBroadcst.setSyncActionType(syncActionType);
        // wsBroadcst.setCreatedBy(newBroadcast.getCreatedBy());
        return wsBroadcst;
    }

    // helpers
    private <T> T returnArg1OnNull(T arg1, T arg2)
    {
        return arg2 == null ? arg1 : arg2;
    }

    public WSBroadcast getBroadcastByBroadcastId(String broadcastId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException
    {
        return null;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "BroadcastService-getBroadcastOfAccount")
    public List<WSBroadcast> getBroadcastOfAccount(String accountId) throws General1GroupException
    {
        List<WSBroadcast> wsBroadcasts = new ArrayList<WSBroadcast>();

        List<Broadcast> broadcasts = broadcastDAO.fetchBroadcastByAccountId(accountId);
        for (Broadcast broadcast : broadcasts)
        {
            WSBroadcast wsBroadcast = new WSBroadcast(broadcast);
            wsBroadcasts.add(wsBroadcast);
        }
        return wsBroadcasts;
    }

    public void closeBroadcast(PropertyListing broadcast) throws Abstract1GroupException
    {

    }

    public Boolean isBroadcastsExistOfAccount(String accountId)
    {
        return null;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "BroadcastService-star broadcast")
    public void starBroadcast(String accountId, String broadcastId) throws General1GroupException, BroadcastProcessException
    {
        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);
        Account account = accountDAO.fetchAccountById(accountId);

        if (broadcast == null)
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
        }

        if (!broadcast.getStatus().equals(BroadcastStatus.ACTIVE))
            throw new BroadcastProcessException(BroadcastExceptionCode.ACTIVE_BROADCAST_EDITED, false, broadcastId);

        StarredBroadcast starredBroadcast = starredBroadcastDAO.getStarredBroadcastByAccountIdAndBroadcastId(accountId, broadcastId);

        if (Utils.isNull(starredBroadcast))
        {
            StarredBroadcast starredBroadcastobj = new StarredBroadcast();
            starredBroadcastobj.setCreatedBy(account.getId());
            starredBroadcastobj.setBroadcast(broadcast);
            starredBroadcastobj.setCreatedBy(accountId);
            starredBroadcastDAO.saveStarredBroadcast(starredBroadcastobj);

            List<String> participantsIds = new ArrayList<String>();
            participantsIds.add(accountId);
            participantsIds.add(accountId);

            Groups groups = groupsService.fetchGroupsFromParticipantsAndType(accountId, participantsIds, GroupSource.STARRED);
            if (!Utils.isNull(groups))
            {
                Groups groupToUpdate = new Groups();
                groupToUpdate.setId(groups.getId());
                int totalMessageCount = groups.getTotalmsgCount();
                groupToUpdate.setTotalmsgCount(totalMessageCount + 1);
                groupsDAO.update(Arrays.asList(groupToUpdate));
            }
        }
    }

    @Profiled(tag = "BroadcastService-appendToSyncLog")
    public WSBroadcast appendToSyncLog(WSBroadcast wsBroadcast) throws Exception
    {
        if (wsBroadcast.getSyncActionType() != null && wsBroadcast.getSyncActionType().equals(SyncActionType.EDIT_CLOSE))
        {
            this.appendToSyncLogAsEntityReference(wsBroadcast.getId(), wsBroadcast.getOldBroadcastId(), wsBroadcast.getSyncActionType());
        }
        if (wsBroadcast.getId() != null)
        {
            this.appendToSyncLogAsEntityReference(wsBroadcast.getId(), SyncActionType.ADD, wsBroadcast.getWsMessageResponse());
        }
        wsBroadcast.setOldBroadcastId(null);
        wsBroadcast.setSyncActionType(null);
        return wsBroadcast;
    }

    /**
     * This function is used to append sync log entry (Kafka) after any changes
     * are made to broadcast
     * 
     * @param broadcastId
     * @param syncActionType
     * @throws SyncLogProcessException
     */
    private void appendToSyncLogAsEntityReference(String broadcastId, SyncActionType syncActionType, WSMessageResponse wsMessage) throws SyncLogProcessException
    {
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.BROADCAST);
        syncLogEntry.setAssociatedEntityId(broadcastId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, wsMessage);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    /**
     * This function is used to log broadcast to kafka as sync log Also an
     * requesting account is subscribed to his own broadcast
     * 
     * @param broadcast
     */
    private void subscribeAndAppendToSyncLog(String broadcastId, String accountId, int subcribeTime) throws SyncLogProcessException
    {
        subscriptionDAO.addSubscription(accountId, broadcastId, EntityType.BROADCAST, subcribeTime);
        WSSubscribeEntity subcribeEntity = new WSSubscribeEntity(Constant.SUBSCRIBE_TIME, broadcastId, accountId);
        subcribeEntity.setType(EntityType.BROADCAST);
        // create sync entity object of type subscribe entity
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.SUBSCRIBE);
        syncLogEntry.setAssociatedEntityType(EntityType.BROADCAST);
        syncLogEntry.setAssociatedEntityId(broadcastId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, subcribeEntity);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    /**
     * This function is used to renew broadcast, checks status of requesting
     * broadcast is active and time has passed beyond one month from date of
     * creation
     * 
     * @param broadcast
     * @param action
     * 
     * @throws ParseException
     * @throws General1GroupException
     */
    private void renewBroadcast(Broadcast broadcast, PropertyUpdateActionType action) throws ParseException, General1GroupException
    {
        // check if current property listing status is expired, then only renew
        // property
        /*
         * Date propertyLastRenewedTime = broadcast.getRenewalTime() != null ?
         * broadcast.getRenewalTime() : broadcast.getCreatedTime();
         * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"
         * ); String currentDate = sdf.format(new Date()); Date current =
         * sdf.parse(currentDate); long diff = current.getTime() -
         * propertyLastRenewedTime.getTime(); long timeToExpire =
         * Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS; long timeToClose =
         * Constant.TIME_TO_BE_CLOSED_IN_MILLISECONDS; if
         * ((broadcast.getStatus().equals(PropertyStatus.EXPIRED) ||
         * broadcast.getStatus().equals(PropertyStatus.ACTIVE) || diff >=
         * timeToExpire) && action.equals(PropertyUpdateActionType.RENEW)) { //
         * check logical status of property listing if (diff < timeToClose) {
         * broadcast.setRenewalTime(new Date());
         * broadcast.setStatus(PropertyStatus.ACTIVE); } else { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false,
         * "Property has been already closed"); } } else { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false,
         * "Only expired or active property listing can be renewed"); }
         */
    }

    /**
     * This function is used to close broadcast , also checks logical status of
     * broadcast and status equals to active
     * 
     * @param status
     * @param broadcast
     * @throws General1GroupException
     * 
     */
    @Profiled(tag = "BroadcastService-closeBroadcast")
    public void closeBroadcast(Broadcast broadcast, String status) throws General1GroupException
    {
        // check if current property listing status is active, then only close
        // property
        /*
         * if ((broadcast.getStatus().equals(PropertyStatus.ACTIVE) ||
         * broadcast.getStatus().equals(PropertyStatus.EXPIRED)) &&
         * PropertyStatus.parse(status).equals(PropertyStatus.CLOSED)) {
         * broadcast.setStatus(BroadcastStatus.CLOSED);
         * broadcast.setShortReference(null); // decrement count of Active
         * PropertyListing // decrementBroadcastCount(); } else { throw new
         * General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false,
         * "Property has been already closed"); }
         */
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchAllBroadcastOfAccount")
    public WSBroadcastListResult fetchAllBroadcastOfAccount(String accountId, String paginationKey, int pageNo, String refreshPaginationKey) throws JsonMappingException, JsonGenerationException,
            IOException
    {
        String queryString = "select * from broadcast";// Broadcast.QUERY_GET_ALL_BROADCAST_BY_ACCOUNT_ID;
        Map<EntityFieldName, Object> fieldVsValueMap = new HashMap<EntityFieldName, Object>();
        fieldVsValueMap.put(EntityFieldName.ACCOUNT_ID, accountId);

        Set<String> all = generalDAO.saveAndFetchRecords(accountId, EntityType.BROADCAST, queryString, fieldVsValueMap, paginationKey, pageNo, refreshPaginationKey);

        WSBroadcastListResult result = new WSBroadcastListResult();
        List<WSBroadcast> broadcastList = new ArrayList<WSBroadcast>();
        for (String broadcastString : all)
        {
            WSBroadcast broadcast = (WSBroadcast) Utils.getInstanceFromJson(broadcastString, WSBroadcast.class);
            broadcastList.add(broadcast);
        }

        WSPaginationData pagination = new WSPaginationData();
        if (!all.isEmpty())
        {
            pagination = paginationService.getPaginationData(paginationKey, pageNo, refreshPaginationKey);
            result.setPagination(pagination);
        }

        result.setBroadcasts(broadcastList);

        return result;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-transitionStatus")
    public WSBroadcast transitionStatus(String broadcastId, BroadcastStatus status, String accountId) throws General1GroupException, BadRequestException, BroadcastProcessException
    {
        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);

        if (broadcast == null)
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
        }
        else if (!broadcast.getStatus().equals(BroadcastStatus.ACTIVE))
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.ACTIVE_BROADCAST_EDITED, false, broadcastId);
        }
        else if (!broadcast.getAccount().getId().equals(accountId))
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
        }
        List<BroadcastTag> broadcastTagList = broadcastTagDAO.fetchBroadcastTagByBroadcastId(broadcastId, BroadcastStatus.ACTIVE);

        Date UpdatedTime = new Date();
        WSBroadcast wsBroadcast = new WSBroadcast();
        if (status.equals(BroadcastStatus.CLOSED))
        {
            broadcast.setUpdatedTime(UpdatedTime);
            broadcast.setUpdatedBy(accountId);
            broadcast.setStatus(status);
            broadcastDAO.saveBroadcast(broadcast);

            // Update Status of broadcastTag
            for (BroadcastTag broadcastTag : broadcastTagList)
            {
                broadcastTag.setStatus(BroadcastTagStatus.CLOSED);
                broadcastTag.setUpdatedTime(UpdatedTime);
                broadcastTag.setUpdatedBy(accountId);
                broadcastTagDAO.saveBroadcastTag(broadcastTag);
            }
            wsBroadcast.setSyncActionType(SyncActionType.DELETE);
        }
        else if (status.equals(BroadcastStatus.RENEW))
        {
            broadcast.setUpdatedTime(UpdatedTime);
            broadcast.setUpdatedBy(accountId);
            // broadcast.setStatus(status);
            broadcastDAO.saveBroadcast(broadcast);

            // Update Status of broadcastTag
            for (BroadcastTag broadcastTag : broadcastTagList)
            {
                // broadcastTag.setStatus();
                broadcastTag.setUpdatedTime(UpdatedTime);
                broadcastTag.setUpdatedBy(accountId);
                broadcastTagDAO.saveBroadcastTag(broadcastTag);
            }
            wsBroadcast.setSyncActionType(SyncActionType.FETCH);
        }

        // update status of message in solr
        messageDAO.updateMessageStatusBybroadcastId(broadcastId, status, accountId);

        wsBroadcast.setUpdatedTime(UpdatedTime);
        wsBroadcast.setId(broadcast.getId());
        return wsBroadcast;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-unStarBroadcast")
    public void unStarBroadcast(String accountId, String broadcastId) throws General1GroupException, BroadcastProcessException
    {
        Validation.isTrue(!Utils.isNull(accountId), "accountId should not be null");
        Validation.isTrue(!Utils.isNull(broadcastId), "broadcastId should not be null");

        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);
        if (broadcast == null)
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
        }
        if (!broadcast.getStatus().equals(BroadcastStatus.ACTIVE))
            throw new BroadcastProcessException(BroadcastExceptionCode.ACTIVE_BROADCAST_EDITED, false, broadcastId);

        StarredBroadcast starredBroadcast = starredBroadcastDAO.getStarredBroadcastByAccountIdAndBroadcastId(accountId, broadcastId);

        if (!Utils.isNull(starredBroadcast))
        {
            starredBroadcastDAO.unStarBroadcast(starredBroadcast);

            List<String> participantsIds = new ArrayList<String>();
            participantsIds.add(accountId);
            participantsIds.add(accountId);
            Groups groups = groupsService.fetchGroupsFromParticipantsAndType(accountId, participantsIds, GroupSource.STARRED);

            if (!Utils.isNull(groups))
            {
                int totalMessageCount = groups.getTotalmsgCount();
                if (totalMessageCount != 0)
                {
                    Groups groupToUpdate = new Groups();
                    groupToUpdate.setId(groups.getId());
                    groupToUpdate.setTotalmsgCount((totalMessageCount - 1));
                    groupsDAO.update(Arrays.asList(groupToUpdate));
                }
            }
        }
        else
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-getBroadcastByBroadcastId")
    public List<WSMessageResponse> getBroadcastByBroadcastIds(String accountId, List<String> listbroadcastIds) throws Abstract1GroupException, JsonMappingException, JsonGenerationException,
            IOException
    {

        List<Broadcast> broadcastList = broadcastDAO.getBroadcastByBroadcastIds(listbroadcastIds, BroadcastStatus.ACTIVE);
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();

        if (!broadcastList.isEmpty())
        {
            Map<String, WSBroadcast> broadcastIdVsWsBroadcastMap = new HashMap<String, WSBroadcast>();
            List<String> broadcastIds = new ArrayList<String>();
            for (Broadcast broadcast : broadcastList)
            {
                String broadcastId = broadcast.getId();
                WSBroadcast wsbroadcast = new WSBroadcast(broadcast);
                broadcastIdVsWsBroadcastMap.put(broadcastId, wsbroadcast);
                broadcastIds.add(broadcastId);
            }

            // fetch Message from solr
            List<Message> solrMessageList = messageDAO.fetchMessageByBroadcastIds(broadcastIds);
            Set<String> accountIdsSet = new HashSet<String>();

            for (Message message : solrMessageList)
            {
                accountIdsSet.add(message.getToAccountId());
                accountIdsSet.add(message.getCreatedBy());
            }
            accountIdsSet.remove(null);
            List<String> accountIdList = new ArrayList<String>(accountIdsSet);
            if (solrMessageList != null && !solrMessageList.isEmpty())
            {
                WSAccountUtilityDataHolder dataHolder = groupsService.fetchAccountMapData(accountId, accountIdList);
                for (Message message : solrMessageList)
                {
                    WSMessageResponse wsMessageResponse = new WSMessageResponse(message);

                    WSBroadcast wsbroadcast = broadcastIdVsWsBroadcastMap.get(message.getBroadcastId());
                    wsMessageResponse.setBroadcast(wsbroadcast);

                    wsMessageResponse = groupsService.formWSMessageResponse(wsMessageResponse, dataHolder);
                    wsMessageResponseList.add(wsMessageResponse);
                }
            }
        }
        return wsMessageResponseList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-saveBroadcast")
    public WSBroadcast saveBroadcast(String adminId, String broadcastId, String messageId, String accountId, Set<WSBroadcastTag> wsbroadcastTagSet) throws General1GroupException, ParseException,
            SyncLogProcessException, JsonMappingException, JsonGenerationException, IOException, BroadcastProcessException, AccountNotFoundException
    {
        Validation.isTrue(!Utils.isNull(accountId), "accountId should not be null");
        Validation.isTrue(!Utils.isNull(messageId), "messageId should not be null");
        Validation.isTrue(!Utils.isNull(adminId), "adminId should not be null");

        Broadcast newBroadcast = null;
        Broadcast broadcast = null;
        SyncActionType syncActionType = null;

        Admin admin = adminDAO.fetchAdminById(adminId);
        if (Utils.isNull(admin))
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.ADMIN_ACCOUNT_NOT_FOUND, false, adminId);
        }

        if (!admin.getStatus().equals(AdminStatus.ACTIVE))
        {
            throw new AccountNotFoundException(AccountExceptionCode.FORBIDDEN, true, adminId);
        }
        if (broadcastId != null)
        {
            broadcast = broadcastDAO.fetchBroadcast(broadcastId);
            if (Utils.isNull(broadcast))
            {
                throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
            }
            else
            {
                if (!broadcast.getStatus().equals(BroadcastStatus.ACTIVE))
                {
                    throw new BroadcastProcessException(BroadcastExceptionCode.ACTIVE_BROADCAST_EDITED, false, broadcastId);
                }

                if (!broadcast.getMessageId().equals(messageId))
                {
                    throw new BroadcastProcessException(BroadcastExceptionCode.INVALID_MESSAGE_ID_FOUND, false, messageId);
                }
            }
        }
        if (wsbroadcastTagSet.isEmpty())
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_TAG_NOT_FOUND, false, messageId);
        }

        Account account = accountDAO.fetchAccountById(accountId);
        if (Utils.isNull(account))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }

        // Message message = messageDAO.fetchMessageFromId(messageId);
        Message message = new Message();
        message.setId(messageId);

        newBroadcast = new Broadcast();
        newBroadcast.setStatus(BroadcastStatus.ACTIVE);
        newBroadcast.setAccount(account);
        newBroadcast.setCreatedBy(adminId);
        newBroadcast.setMessageId(messageId);
        newBroadcast.setCreatedTime(new Date());
        newBroadcast.setUpdatedBy(adminId);
        newBroadcast.setUpdatedTime(new Date());
        broadcastDAO.saveBroadcast(newBroadcast);

        WSBroadcast wsbroadcast = new WSBroadcast();
        // save Old broadcast
        if (broadcastId != null)
        {

            broadcast.setUpdatedBy(adminId);
            broadcast.setUpdatedTime(new Date());
            broadcast.setStatus(BroadcastStatus.CLOSED);
            broadcast.setMessageId(null);
            broadcastDAO.saveBroadcast(broadcast);

            // Remove Old broadcastTags of closed broadcast
            List<String> broadcastTagIds = broadcastTagDAO.getBroadcastTagIdsByBroadcastId(broadcastId);
            broadcastTagDAO.deleteBroadcasTagsByIds(broadcastTagIds);

            wsbroadcast.setSyncActionType(syncActionType.EDIT_CLOSE);
            wsbroadcast.setOldBroadcastId(broadcastId);
        }

        Set<BroadcastTag> broadcasttagSet = new HashSet<BroadcastTag>();
        for (WSBroadcastTag wsBroadcastTag : wsbroadcastTagSet)
        {
            List<String> titleList = new ArrayList<String>();
            BroadcastTag broadcastTag = new BroadcastTag();

            Location location = locationDAO.fetchById(wsBroadcastTag.getLocationId());
            if (location != null)
            {
                broadcastTag.setLocation(location);
                titleList.add(location.getLocalityName());
            }
            String broadcastType = wsBroadcastTag.getBroadcastType();
            BroadcastType broadcastTypeValue = broadcastType != null ? BroadcastType.convert(broadcastType) : null;

            if (broadcastTypeValue != null)
            {
                broadcastTag.setBroadcastType(broadcastTypeValue);
                titleList.add(broadcastTypeValue.toString());
            }

            String commisionType = wsBroadcastTag.getCommissionType();
            CommissionType commisionTypeValue = commisionType != null ? CommissionType.convert(commisionType) : null;
            broadcastTag.setCommissionType(commisionTypeValue);

            String propertyType = wsBroadcastTag.getPropertyType();
            PropertyType propertyTypeValue = propertyType != null ? PropertyType.convert(propertyType) : null;
            if (propertyTypeValue != null)
            {
                broadcastTag.setPropertyType(propertyTypeValue);
                titleList.add(propertyTypeValue.toString());
            }

            String rooms = wsBroadcastTag.getRooms();
            Rooms roomValue = rooms != null ? Rooms.convert(rooms) : null;
            if (roomValue != null)
            {
                broadcastTag.setRooms(roomValue);
                titleList.add(roomValue.toString());
            }

            String propertySubType = wsBroadcastTag.getPropertySubType();
            PropertySubType propertySubTypeValue = propertySubType != null ? PropertySubType.convert(propertySubType) : null;
            broadcastTag.setPropertySubType(propertySubTypeValue);

            String propertyMarket = wsBroadcastTag.getPropertyMarket();
            PropertyMarket propertyMarketValue = propertyMarket != null ? PropertyMarket.convert(propertyMarket) : null;
            broadcastTag.setPropertyMarket(propertyMarketValue);

            String transactionType = wsBroadcastTag.getTransactionType();
            PropertyTransactionType propertyTransactionTypeValue = transactionType != null ? PropertyTransactionType.convert(transactionType) : null;
            broadcastTag.setTransactionType(propertyTransactionTypeValue);

            String broadcastTagStatusType = wsBroadcastTag.getStatus();
            BroadcastTagStatus broadcastTagStatusTypeValue = broadcastTagStatusType != null ? BroadcastTagStatus.parse(broadcastTagStatusType) : null;
            broadcastTag.setStatus(broadcastTagStatusTypeValue);

            if (broadcastTagStatusTypeValue.equals(BroadcastTagStatus.DRAFT))
            {
                String draftedBy = wsBroadcastTag.getDraftedBy();
                broadcastTag.setDraftedBy(draftedBy);

                String drafted_time = wsBroadcastTag.getDraftedTime();
                broadcastTag.setDraftedTime(new Date(Long.valueOf(drafted_time)));
            }
            else if (broadcastTagStatusTypeValue.equals(BroadcastTagStatus.ADDED))
            {
                String addedBy = wsBroadcastTag.getAddedBy();
                broadcastTag.setAddedBy(addedBy);

                String added_time = wsBroadcastTag.getAddedTime();
                broadcastTag.setAddedTime(new Date(Long.valueOf(added_time)));
            }

            if (broadcastTypeValue.equals(BroadcastType.REQUIREMENT))
            {
                broadcastTag.setMaximumSize(Long.valueOf(wsBroadcastTag.getMaxSize()));
                broadcastTag.setMinimumSize(Long.valueOf(wsBroadcastTag.getMinSize()));

                broadcastTag.setMaximumPrice(Long.valueOf(wsBroadcastTag.getMaxPrice()));
                broadcastTag.setMinimumPrice(Long.valueOf(wsBroadcastTag.getMinPrice()));

                titleList.add(wsBroadcastTag.getMinSize() + "");
                titleList.add(wsBroadcastTag.getMaxSize() + "");
            }
            else
            {
                broadcastTag.setSize(Long.valueOf(wsBroadcastTag.getSize()));
                broadcastTag.setPrice(Long.valueOf(wsBroadcastTag.getPrice()));
                titleList.add(wsBroadcastTag.getSize() + "");
            }

            String title = StringUtils.join(titleList, "-");
            broadcastTag.setTitle(title);
            broadcastTag.setCityId(wsBroadcastTag.getCityId());
            broadcastTag.setCustomSublocation(wsBroadcastTag.getCustomSublocation());

            String createdBy = wsBroadcastTag.getCreatedBy() != null ? wsBroadcastTag.getCreatedBy() : adminId;
            broadcastTag.setCreatedBy(createdBy);

            String updatedBy = wsBroadcastTag.getUpdatedBy() != null ? wsBroadcastTag.getUpdatedBy() : adminId;
            broadcastTag.setUpdatedBy(updatedBy);

            Date createdTime = wsBroadcastTag.getCreatedTime() != null ? new Date(Long.valueOf(wsBroadcastTag.getCreatedTime())) : new Date();
            broadcastTag.setCreatedTime(createdTime);

            // Date updatedTime = wsBroadcastTag.getUpdatedTime() != null ? new
            // Date(Long.valueOf(wsBroadcastTag.getUpdatedTime())) : new Date();
            broadcastTag.setUpdatedTime(createdTime);

            broadcastTag.setBroadcast(newBroadcast);

            broadcasttagSet.add(broadcastTag);
        }
        // save broadcastTag
        for (BroadcastTag broadcastTag : broadcasttagSet)
        {
            broadcastTagDAO.saveBroadcastTag(broadcastTag);
        }
        // update broadcastId,messageType in solr for respective Message
        message.setBroadcastId(newBroadcast.getId());
        message.setMessageType(MessageType.BROADCAST);
        message.setMessageStatus(MessageStatus.ACTIVE);
        message.setUpdatedBy(accountId);
        message.setUpdatedTime(new Date());
        message.setCreatedBy(accountId);
        // messageDAO.saveMessage(message);
        messageDAO.update(message);

        Message responseMessage = messageDAO.fetchMessageFromId(messageId);

        responseMessage.setBroadcastId(newBroadcast.getId());
        responseMessage.setMessageType(MessageType.BROADCAST);
        responseMessage.setMessageStatus(MessageStatus.ACTIVE);
        responseMessage.setUpdatedBy(accountId);
        responseMessage.setUpdatedTime(new Date());
        responseMessage.setCreatedBy(accountId);

        WSMessageResponse wsMessage = new WSMessageResponse(responseMessage);

        wsbroadcast.setId(newBroadcast.getId());
        wsbroadcast.setMessageId(newBroadcast.getMessageId());
        wsbroadcast.setStatus(newBroadcast.getStatus());
        wsbroadcast.setWsMessageResponse(wsMessage);
        return wsbroadcast;
    }

    private Set<Search> getMatchingSearches(List<Search> search, List<BroadcastTag> tagList)
    {
        Set<Search> searchSet = new HashSet<Search>();

        for (BroadcastTag tag : tagList)
        {

            for (Search search2 : search)
            {
                if (searchSet.contains(search2))
                {
                    break;
                }
                if (tag.getCityId().equals(search2.getCityId()))
                {
                    searchSet.add(search2);
                }
                else if (tag.getTransactionType().equals(search2.getTransactionType()) || tag.getRooms().equals(search2.getRooms()) || tag.getPropertyType().equals(search2.getPropertyType()))
                {
                    searchSet.add(search2);
                }
                else if (tag.getPrice() >= search2.getMinArea() && tag.getPrice() <= search2.getMaxPrice())
                {
                    searchSet.add(search2);
                }
                else if (tag.getSize() >= search2.getMinArea() && tag.getSize() <= search2.getMaxArea())
                {
                    searchSet.add(search2);
                }
            }
        }

        return searchSet;
    }

    public long fetchCount()
    {
        long count = broadcastDAO.fetchCount();
        return count;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchAccountAndBroadcastCountMap")
    public Map<String, Long> fetchAccountAndBroadcastCountMap(List<String> accountIds)
    {
        List<BroadcastCountObject> broadcastCountList = broadcastDAO.fetchBroadcastCountByAccountIds(accountIds);

        Map<String, Long> map = new HashMap<String, Long>();
        if (map != null)
        {
            for (BroadcastCountObject object : broadcastCountList)
            {
                map.put(object.getAccountId(), object.getCount());
            }
        }
        return map;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchLastStarredBroadcastOfAccount")
    public WSBroadcast fetchLastStarredBroadcastOfAccount(String accountId)
    {
        List<StarredBroadcast> starredBroadcastList = starredBroadcastDAO.getStarredBroadcastByAccountId(accountId);
        if (starredBroadcastList != null && starredBroadcastList.size() > 0)
        {
            Broadcast broadcast = starredBroadcastList.get(0).getBroadcast();
            return new WSBroadcast(broadcast);
        }
        return null;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchLastStarredBroadcastOfAccount")
    public WSBroadcast fetchBroadcastById(String broadcastId)
    {
        WSBroadcast wsBroadcast = new WSBroadcast();
        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);

        if (broadcast != null)
        {
            wsBroadcast = new WSBroadcast(broadcast);
        }
        return wsBroadcast;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-getBroadcastDetailsByAccountId")
    public List<WSMessageResponse> getBroadcastDetailsByAccountId(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException
    {
        // fetch Broadcast
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();

        List<MessageSourceType> messageSourceTypeList = new ArrayList<MessageSourceType>();
        messageSourceTypeList.add(MessageSourceType.ME);
        messageSourceTypeList.add(MessageSourceType.WHATSAPP);

        List<MessageType> messageTypeList = new ArrayList<MessageType>();
        messageTypeList.add(MessageType.BROADCAST);
        boolean latestToOldest = false;
        List<Message> messageList = messageDAO.fetchAllMessagesByAccountIdAndMessageSourceTypeList(accountId, messageTypeList, messageSourceTypeList, latestToOldest);

        List<String> accountIdList = new ArrayList<String>();
        accountIdList.add(accountId);
        accountIdList.add(accountId);

        if (messageList != null && !messageList.isEmpty())
        {
            WSAccountUtilityDataHolder dataHolder = groupsService.fetchAccountMapData(accountId, accountIdList);
            for (Message message : messageList)
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponse = groupsService.formWSMessageResponse(wsMessageResponse, dataHolder);
                wsMessageResponseList.add(wsMessageResponse);
            }
        }
        return wsMessageResponseList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-removeRelationAndCloseBroadcast")
    public void removeRelationAndCloseBroadcast(String broadcastId, String messageId)
    {
        BroadcastStatus broadcastStatus = BroadcastStatus.CLOSED;
        broadcastDAO.removeRelationOfBroadcastAndMessageUpdateStatus(broadcastId, messageId, broadcastStatus);

        // Remove Old broadcastTags of closed broadcast
        List<String> broadcastTagIds = broadcastTagDAO.getBroadcastTagIdsByBroadcastId(broadcastId);
        broadcastTagDAO.deleteBroadcasTagsByIds(broadcastTagIds);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchAllStarredBroadcastOfAccount")
    public List<WSBroadcast> fetchAllStarredBroadcastOfAccount(String accountId, int offset, int limit)
    {
        List<StarredBroadcast> starredBroadcastList = starredBroadcastDAO.getStarredBroadcastByAccountId(accountId, offset, limit);
        List<WSBroadcast> wsBroadcastList = new ArrayList<WSBroadcast>();

        if (starredBroadcastList != null && starredBroadcastList.size() > 0)
        {
            for (StarredBroadcast starredBroadcast : starredBroadcastList)
            {
                Broadcast broadcast = starredBroadcast.getBroadcast();
                WSBroadcast wsBroadcast = new WSBroadcast(broadcast);
                wsBroadcastList.add(wsBroadcast);
            }
        }
        return wsBroadcastList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-updateBroadcastViewCount")
    public void updateBroadcastViewCount(List<String> broadcastIdList)
    {
        broadcastDAO.updateBroadcastViewCount(broadcastIdList);
    }

    private void appendToSyncLogAsEntityReference(String newBroadcastId, String oldBroadcastId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, newBroadcastId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.MESSAGE);
        syncLogEntry.setAssociatedEntityId(oldBroadcastId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-searchBroadcastsAndCacheItAndUpdateViewCount")
    public List<String> searchBroadcastsAndCacheItAndUpdateViewCount(String accountId, PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, int cityId,
            Location location, Integer minArea, Integer maxArea, Long maxPrice, Long minPrice, List<Rooms> roomList, PropertySubType propertSubType, int limit, int offset)
    {
        List<BroadcastObject> broadcastObjectList = new ArrayList<BroadcastObject>();
        Map<String, String> broadcastMap = new HashMap<String, String>();
        List<String> broadcastIdList = new ArrayList<String>();

        // if (broadcastType != null &&
        // (broadcastType.equals(BroadcastType.PROPERTY_LISTING) ||
        // broadcastType.equals(BroadcastType.REQUIREMENT)))
        // {
        List<BroadcastStatus> broadcastStatusList = new ArrayList<BroadcastStatus>();
        broadcastStatusList.add(BroadcastStatus.ACTIVE);

        List<BroadcastTagStatus> broadcastTagStatusList = new ArrayList<BroadcastTagStatus>();
        broadcastTagStatusList.add(BroadcastTagStatus.ADDED);

        broadcastObjectList = broadcastTagDAO.searchBroadcastTypePropertyListing(propertType, broadcastType, transactionType, cityId, location, minArea, maxArea, maxPrice, minPrice, roomList,
                propertSubType, broadcastStatusList, broadcastTagStatusList, limit, offset);
        // }
        // else if (broadcastType != null &&
        // broadcastType.equals(BroadcastType.REQUIREMENT))
        // {
        // broadcastObjectList =
        // broadcastTagDAO.searchBroadcastTypeRequirement(propertType,
        // broadcastType, transactionType, cityId, location, area, price);
        // }
        // else
        // {
        // int offset = 0;
        // int limit = 1000;
        // broadcastObjectList = broadcastTagDAO.searchBroadcastByCityId(cityId,
        // offset, limit);
        // }

        // Reset broadcast set key data
        broadcastDAO.removeAllBroadcast(accountId);

        if (broadcastObjectList != null && !broadcastObjectList.isEmpty())
        {
            Set<String> broadcastIdSet = new HashSet<String>();
            if (broadcastObjectList != null && !broadcastObjectList.isEmpty())
            {
                for (BroadcastObject broadcastObject : broadcastObjectList)
                {
                    broadcastIdSet.add(broadcastObject.getId());
                }
            }
            List<MessageSourceType> messageSourceList = new ArrayList<MessageSourceType>();
            messageSourceList.add(MessageSourceType.ME);
            messageSourceList.add(MessageSourceType.WHATSAPP);

            List<MessageStatus> messageStatusList = new ArrayList<MessageStatus>();
            messageStatusList.add(MessageStatus.ACTIVE);

            List<Message> messageList = messageDAO.fetchMessageByBroadcastIdAndSourceAndStatus(broadcastIdSet, messageSourceList, messageStatusList);

            Map<String, String> values = new HashMap<String, String>();
            values.put("account_id", accountId);
            String searchBroadcastKey = BroadcastKey.SEARCHED_BROADCAST_OF_ACCOUNT.getFormedKey(values);

            Set<PipelineCacheEntity> messagePipelinSet = new TreeSet<PipelineCacheEntity>(new PipelineEntityScoreComparator());
            for (Message message : messageList)
            {
                String broadcastId = message.getBroadcastId();
                if (broadcastMap.get(broadcastId) == null)
                {
                    PipelineCacheEntity pce = new PipelineCacheEntity(searchBroadcastKey);
                    if (!Utils.isNull(message.getUpdatedTime()))
                    {
                        pce.setScore(message.getUpdatedTime().getTime());
                    }
                    else
                    {
                        pce.setScore(message.getCreatedTime().getTime());
                    }

                    pce.setValue(broadcastId);
                    messagePipelinSet.add(pce);

                    broadcastMap.put(broadcastId, broadcastId);
                    broadcastIdList.add(broadcastId);
                }
            }

            if (!messagePipelinSet.isEmpty())
            {
                // save all broadcast in cache
                broadcastDAO.saveBroadcastBulk(messagePipelinSet);

                // Update view count
                broadcastDAO.updateBroadcastViewCount(broadcastIdList);
            }

        }
        return broadcastIdList;
    }

    @Profiled(tag = "BroadcastService-fetchBroadcastofAccountFromCache")
    public List<String> fetchBroadcastofAccountFromCache(String accountId, int offset, int limit)
    {
        Set<String> broadcastIdSet = broadcastDAO.fetchBroadcast(accountId, offset, limit);
        List<String> broadcastIdList = new ArrayList<String>(broadcastIdSet);
        return broadcastIdList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-fetchStarredBroadcastoIdsOfAccount")
    public List<String> fetchStarredBroadcastoIdsOfAccount(String accountId)
    {
        List<StarredBroadcast> broadcastList = starredBroadcastDAO.getStarredBroadcastIdsByAccountId(accountId);
        List<String> broadcastIdList = new ArrayList<String>();
        if (broadcastList != null && !broadcastList.isEmpty())
        {
            for (StarredBroadcast broadcast : broadcastList)
            {
                broadcastIdList.add(broadcast.getBroadcastId());
            }
        }
        return broadcastIdList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "BroadcastService-updateBroadcastTags")
    public void updateBroadcastTags(one.group.entities.api.request.v2.WSBroadcast wsBroadcast) throws Abstract1GroupException
    {
        List<WSBroadcastTag> tagList = wsBroadcast.getTags();
        Map<String, WSBroadcastTag> idVsTagMap = new HashMap<String, WSBroadcastTag>();
        Map<String, Location> idVsLocationMap = new HashMap<String, Location>();
        Broadcast broadcast = broadcastDAO.fetchBroadcast(wsBroadcast.getBroadcastId());

        if (broadcast == null)
        {
            throw new General1GroupException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, true, wsBroadcast.getBroadcastId());
        }

        for (WSBroadcastTag tag : tagList)
        {
            if (tag.getId() != null && !tag.getId().trim().isEmpty())
            {
                idVsTagMap.put(tag.getId(), tag);
            }
            else
            {
                tag.setId(Utils.randomUUID());
                idVsTagMap.put(tag.getId(), tag);
            }

            if (tag.getLocationId() != null && !tag.getLocationId().trim().isEmpty())
            {
                idVsLocationMap.put(tag.getLocationId(), null);
            }
        }

        List<Location> locationList = locationDAO.fetchAllLocations(idVsLocationMap.keySet());

        for (Location location : locationList)
        {
            idVsLocationMap.put(location.getId(), location);
        }

        List<BroadcastTag> broadcastTagList = broadcastTagDAO.fetchBroadcastTagByIds(idVsTagMap.keySet());
        List<BroadcastTag> listToPersist = new ArrayList<BroadcastTag>();
        Map<String, BroadcastTag> idVsBroadTagMap = new HashMap<String, BroadcastTag>();

        for (BroadcastTag t : broadcastTagList)
        {
            idVsBroadTagMap.put(t.getId(), t);
        }

        if (!idVsTagMap.isEmpty())
        {
            for (String id : idVsTagMap.keySet())
            {
                BroadcastTag t = new BroadcastTag();
                t.setId(id);

                if (idVsBroadTagMap != null && idVsBroadTagMap.get(id) != null)
                {
                    t = idVsBroadTagMap.get(id);
                }

                listToPersist.add(populateBroadcastTag(idVsTagMap.get(id), t, idVsLocationMap, broadcast));
            }
        }

        // if (broadcastTagList != null)
        // {
        // for (BroadcastTag t : broadcastTagList)
        // {
        // populateBroadcastTag(idVsTagMap.get(t.getId()), t, idVsLocationMap,
        // broadcast);
        // }
        // }

        broadcastTagDAO.saveBroadcastTags(listToPersist);

    }

    private BroadcastTag populateBroadcastTag(WSBroadcastTag wsTag, BroadcastTag tag, Map<String, Location> idVsLocationMap, Broadcast broadcast)
    {
        if (wsTag != null)
        {

            // Title

            tag.setBroadcast(broadcast);
            tag.setAddedBy(Utils.<String> returnArg1OnNull(tag.getAddedBy(), wsTag.getAddedBy()));
            tag.setAddedTime(Utils.<Date> returnArg1OnNull(tag.getAddedTime(), wsTag.getAddedTime() == null ? null : new Date(Long.parseLong(wsTag.getAddedTime()))));
            tag.setBroadcastType(Utils.<BroadcastType> returnArg1OnNull(tag.getBroadcastType(), wsTag.getBroadcastType() == null ? null : BroadcastType.convert(wsTag.getBroadcastType())));
            tag.setCityId(Utils.<String> returnArg1OnNull(tag.getCityId(), wsTag.getCityId()));
            tag.setCommissionType(Utils.<CommissionType> returnArg1OnNull(tag.getCommissionType(), wsTag.getCommissionType() == null ? null : CommissionType.convert(wsTag.getCommissionType())));
            tag.setCreatedBy(Utils.<String> returnArg1OnNull(tag.getCreatedBy(), wsTag.getCreatedBy()));
            tag.setCreatedTime(Utils.<Date> returnArg1OnNull(tag.getCreatedTime(), wsTag.getCreatedTime() == null ? null : new Date(Long.parseLong(wsTag.getCreatedTime()))));
            tag.setCustomSublocation(Utils.<String> returnArg1OnNull(tag.getCustomSublocation(), wsTag.getCustomSublocation()));
            tag.setDraftedBy(Utils.<String> returnArg1OnNull(tag.getDraftedBy(), wsTag.getDraftedBy()));
            tag.setDraftedTime(Utils.<Date> returnArg1OnNull(tag.getDraftedTime(), wsTag.getDraftedTime() == null ? null : new Date(Long.parseLong(wsTag.getDraftedTime()))));
            tag.setMaximumPrice(Utils.<Long> returnArg1OnNull(tag.getMaximumPrice(), wsTag.getMaxPrice()));
            tag.setMaximumSize(Utils.<Long> returnArg1OnNull(tag.getMaximumSize(), wsTag.getMaxSize()));
            tag.setMinimumPrice(Utils.<Long> returnArg1OnNull(tag.getMinimumPrice(), wsTag.getMinPrice()));
            tag.setMinimumSize(Utils.<Long> returnArg1OnNull(tag.getMinimumSize(), wsTag.getMinSize()));
            tag.setPrice(Utils.<Long> returnArg1OnNull(tag.getPrice(), wsTag.getPrice() == null ? null : Long.parseLong(wsTag.getPrice())));
            tag.setPropertyType(Utils.<PropertyType> returnArg1OnNull(tag.getPropertyType(), wsTag.getPropertyType() == null ? null : PropertyType.convert(wsTag.getPropertyType())));
            tag.setPropertySubType(Utils.<PropertySubType> returnArg1OnNull(tag.getPropertySubType(), wsTag.getPropertySubType() == null ? null : PropertySubType.convert(wsTag.getPropertySubType())));
            tag.setPropertyMarket(Utils.<PropertyMarket> returnArg1OnNull(tag.getPropertyMarket(), wsTag.getPropertyMarket() == null ? null : PropertyMarket.convert(wsTag.getPropertyMarket())));
            tag.setRooms(Utils.<Rooms> returnArg1OnNull(tag.getRooms(), wsTag.getRooms() == null ? null : Rooms.convert(wsTag.getRooms())));
            tag.setSize(Utils.<Long> returnArg1OnNull(tag.getSize(), wsTag.getSize()));
            tag.setStatus(Utils.<BroadcastTagStatus> returnArg1OnNull(tag.getStatus(), wsTag.getStatus() == null ? null : BroadcastTagStatus.parse(wsTag.getStatus())));
            tag.setTransactionType(Utils.<PropertyTransactionType> returnArg1OnNull(tag.getTransactionType(),
                    wsTag.getTransactionType() == null ? null : PropertyTransactionType.convert(wsTag.getTransactionType())));
            tag.setUpdatedBy(Utils.<String> returnArg1OnNull(tag.getUpdatedBy(), wsTag.getUpdatedBy()));
            tag.setUpdatedTime(Utils.<Date> returnArg1OnNull(tag.getUpdatedTime(), wsTag.getUpdatedTime() == null ? null : new Date(Long.parseLong(wsTag.getUpdatedTime()))));
            tag.setLocation(Utils.<Location> returnArg1OnNull(tag.getLocation(), idVsLocationMap.get(wsTag.getLocationId())));
            tag.setTitle(formTagTitle(tag));
            tag.setDiscardReason(Utils.<String> returnArg1OnNull(tag.getDiscardReason(), wsTag.getDiscardReason()));
            tag.setClosedBy(Utils.<String> returnArg1OnNull(tag.getClosedBy(), wsTag.getClosedBy()));
            tag.setClosedTime(Utils.<Date> returnArg1OnNull(tag.getClosedTime(), wsTag.getClosedTime() == null ? null : new Date(Long.parseLong(wsTag.getClosedTime()))));

        }
        return tag;
    }

    private static String formTagTitle(BroadcastTag tag)
    {
        String separator = " - ";
        if (tag == null)
        {
            return null;
        }

        StringBuilder builder = new StringBuilder();
        builder.append((tag.getLocation() == null) ? "[LOC]" : tag.getLocation().getLocalityName()).append(separator);
        builder.append(tag.getPrice()).append(" - ");
        builder.append((tag.getPropertyType() == null) ? "[PTYPE]" : tag.getPropertyType().getTitleAbbr());
        return builder.toString();
    }

    public void appendToSyncLogForEditRenewBroadcast(WSBroadcast wsBroadcast, String requestedAccountId) throws SyncLogProcessException
    {
        this.appendToSyncLogAsEntityReferenceForEditRenewBroadcast(wsBroadcast.getId(), requestedAccountId, wsBroadcast.getSyncActionType());
    }

    private void appendToSyncLogAsEntityReferenceForEditRenewBroadcast(String broadcastId, String requestedAccountId, SyncActionType syncActionType) throws SyncLogProcessException
    {

        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, broadcastId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.MESSAGE);
        syncLogEntry.setAssociatedEntityId(requestedAccountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());

    }

    public Long fetchBroadcastCountByCity(String cityId)
    {
        return broadcastTagDAO.fetchBroadcastCountByCity(cityId);
    }

    public Map<String, Long> fetchAccountAndBroadcastCountMapFromSolr(List<String> accountIds)
    {
        if (accountIds == null || accountIds.isEmpty())
        {
            return new HashMap<String, Long>();
        }
        return messageDAO.fetchBroadcastMessageCountByAccountId(accountIds);
    }

    public Map<String, Long> fetchAccountAndBroadcastTagCountMapByStatus(Set<String> accountIds, BroadcastTagStatus broadcastTagStatus, BroadcastStatus broadcastStatus)
    {
        List<BroadcastTagCountObject> broadcastTagCountList = broadcastDAO.fetchBroadcastTagCountByAccountIds(accountIds, broadcastTagStatus, broadcastStatus);

        Map<String, Long> map = new HashMap<String, Long>();
        if (map != null)
        {
            for (BroadcastTagCountObject object : broadcastTagCountList)
            {
                map.put(object.getAccountId(), object.getCount());
            }
        }
        return map;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "broadcastServiceImpl-fetchBroadcastMapById")
    public Map<String, WSBroadcast> fetchBroadcastMapById(List<String> broadcastIdList)
    {
        Map<String, WSBroadcast> idVsBroadcastMap = new HashMap<String, WSBroadcast>();
        if (broadcastIdList == null || broadcastIdList.isEmpty())
        {
            return idVsBroadcastMap;
        }
        List<Broadcast> broadcastList = broadcastDAO.getBroadcastByBroadcastIds(broadcastIdList, BroadcastStatus.ACTIVE);
        Set<String> broadcastIdSet = new HashSet<String>(broadcastIdList);
        List<BroadcastTag> broadcastTagList = broadcastTagDAO.fetchBroadcastTagByBroadcastIds(broadcastIdSet);

        Map<String, Set<WSTag>> idVsBroadcastTagList = new HashMap<String, Set<WSTag>>();
        Set<String> locationIdSet = new HashSet<String>();
        Set<String> cityIdSet = new HashSet<String>();
        for (BroadcastTag bt : broadcastTagList)
        {
            locationIdSet.add(bt.getLocationId());
            cityIdSet.add(bt.getCityId());
        }

        Map<String, WSCity> idVsCityMap = cityService.fetchCityMapById(cityIdSet);
        Map<String, WSLocation> idVsLocationMap = locationService.fetchLocationMapById(locationIdSet);

        for (BroadcastTag bt : broadcastTagList)
        {
            String broadcastId = bt.getBroadcastId();
            Set<WSTag> tagList = new HashSet<WSTag>();
            if (idVsBroadcastTagList.get(broadcastId) != null)
            {
                tagList = idVsBroadcastTagList.get(broadcastId);
            }
            boolean withCity = false;
            boolean withLocation = false;
            WSTag wsTag = new WSTag(bt, withCity, withLocation);

            WSLocation wsLocation = idVsLocationMap.get(bt.getLocationId());
            WSCity wsCity = idVsCityMap.get(bt.getCityId());

            wsTag.setLocationId(wsLocation.getId());
            wsTag.setLocationName(wsLocation == null ? "" : wsLocation.getName());
            wsTag.setLocationDisplayName(wsLocation == null ? "" : wsLocation.getLocationDisplayName());

            wsTag.setCityId(wsTag.getCityId());
            wsTag.setCityName(wsCity.getName());

            wsTag.setLocation(wsLocation);

            tagList.add(wsTag);
            idVsBroadcastTagList.put(broadcastId, tagList);

            locationIdSet.add(bt.getLocationId());
            cityIdSet.add(bt.getCityId());
        }

        for (Broadcast broadcast : broadcastList)
        {
            WSBroadcast wsBroadcast = new WSBroadcast(broadcast, false);
            wsBroadcast.setTags(idVsBroadcastTagList.get(broadcast.getId()));
            idVsBroadcastMap.put(wsBroadcast.getId(), wsBroadcast);
        }
        return idVsBroadcastMap;
    }
}
