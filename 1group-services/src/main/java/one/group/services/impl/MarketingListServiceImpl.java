package one.group.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.enums.InviteStatus;
import one.group.core.enums.status.Status;
import one.group.dao.MarketingListDAO;
import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.WSMarketingList;
import one.group.entities.jpa.MarketingList;
import one.group.entities.jpa.TargetedMarketingList;
import one.group.services.MarketingListService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarketingListServiceImpl implements MarketingListService
{
    private static final Logger logger = LoggerFactory.getLogger(MarketingListServiceImpl.class);
    MarketingListDAO marketingListDAO;

    public MarketingListDAO getMarketingListDAO()
    {
        return marketingListDAO;
    }

    public void setMarketingListDAO(MarketingListDAO marketingListDAO)
    {
        this.marketingListDAO = marketingListDAO;
    }

    @Profiled(tag = "MarketingListImpl-checkPhoneNumberExists")
    public MarketingList checkPhoneNumberExists(String phoneNumber)
    {
        return marketingListDAO.checkPhoneNumberExists(phoneNumber);
    }

    @Transactional(rollbackOn = { Exception.class })
    public void checkAndUpsertAll(List<String> phoneNumberList)
    {
        int counter = 0;
        for (String phoneNumber : phoneNumberList)
        {
            MarketingList marketingList = checkPhoneNumberExists(phoneNumber);
            if (marketingList == null)
            {
                // if phone number not exist then insert new record
                marketingList = new MarketingList();
                marketingList.setPhoneNumber(phoneNumber);
            }
            marketingList.setDataSource(Status.SEEDED.toString());
            marketingListDAO.save(marketingList);
            logger.info(counter + " - Processed record [" + phoneNumber + "]");
            counter++;
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void checkAndSaveInvitedPhoneNumbers(String phoneNumber)
    {
        try
        {
            MarketingList marketingList = new MarketingList();
            marketingList.setPhoneNumber(phoneNumber);
            marketingList.setDataSource("referral");
            marketingList.setWsGroupCount(0);
            marketingListDAO.save(marketingList);
        }
        catch (Exception e)
        {
            logger.info("Error while saving invited phonnumber to marketing list");
        }

    }

    @Transactional(rollbackOn = { Exception.class })
    public List<MarketingList> fetchAllMarketingListData()
    {
        Collection<MarketingList> marketingList = marketingListDAO.fetchAll();

        List<MarketingList> marketingListArray = new ArrayList<MarketingList>();
        if (marketingList != null && !marketingList.isEmpty())
        {
            marketingListArray.addAll(marketingList);
        }
        return marketingListArray;
    }

    @Transactional(rollbackOn = { Exception.class })
    public void updateMarketingList(List<TargetedMarketingList> targetedMarketingList)
    {
        Collection<MarketingList> marketingNumberCollection = marketingListDAO.fetchAll();
        List<MarketingList> marketingNumberList = (marketingNumberCollection == null) ? new ArrayList<MarketingList>() : new ArrayList<MarketingList>(marketingNumberCollection);
        Set<MarketingList> uniqueMarketingNumberList = new HashSet<MarketingList>();
        Set<MarketingList> updatedMarketingNumberList = new HashSet<MarketingList>();
        int updatedCounter = 0;
        int newCounter = 0;

        for (TargetedMarketingList target : targetedMarketingList)
        {
            MarketingList temp = new MarketingList();
            temp.setPhoneNumber(target.getPhoneNumber());
            int tempIndex = marketingNumberList.indexOf(temp);

            if (tempIndex != -1)
            {
                if (target.getExistingWin().equalsIgnoreCase("true"))
                {
                    MarketingList entry = marketingNumberList.get(tempIndex);
                    entry.setDataSource(target.getDataSource());
                    updatedMarketingNumberList.add(entry);
                }
                // logger.info("Entry already exists: " +
                // marketingNumberList.get(tempIndex));
                updatedCounter++;
            }
            else
            {
                if (target.getExistingWin().equalsIgnoreCase("true"))
                {
                    temp.setDataSource(target.getDataSource());
                }

                uniqueMarketingNumberList.add(temp);
                // logger.info("New Entry: " + temp);
                newCounter++;
            }

        }

        logger.info("Updates: " + updatedCounter);
        logger.info("New: " + newCounter);

        if (!marketingNumberList.isEmpty())
        {
            marketingListDAO.saveAll(updatedMarketingNumberList);
        }

        if (!uniqueMarketingNumberList.isEmpty())
        {
            marketingListDAO.saveAll(uniqueMarketingNumberList);
        }

    }

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<MarketingList> marketingList)
    {
        Map<String, WSInviteContact> phoneNumbers = new HashMap<String, WSInviteContact>();
        if (marketingList != null && !marketingList.isEmpty())
        {
            for (MarketingList marketing : marketingList)
            {
                String phoneNumber = marketing.getPhoneNumber();
                WSInviteContact ic = new WSInviteContact();
                ic.setStatus(InviteStatus.NOT_SELECTED);
                ic.setNumber(phoneNumber);
                ic.setGroupCount(marketing.getWsGroupCount());
                phoneNumbers.put(phoneNumber, ic);
            }
        }
        return phoneNumbers;
    }

    @Profiled(tag = "MarketingListImpl-MarketingListServiceImpl")
    public List<MarketingList> fetchAllMarketingListWhichAreNativeContacts(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return marketingListDAO.fetchAllMarketingListWhichAreNativeContacts(accountId);
    }

    @Profiled(tag = "MarkeingListServiceImpl-fetchMarketingListByPhoneNumberList")
    public HashSet<WSMarketingList> fetchMarketingListByPhoneNumberList(List<String> phoneNumbers)
    {
        Validation.notNull(phoneNumbers, "phone numbers should not be null");

        HashSet<WSMarketingList> wsMarketingList = new HashSet<WSMarketingList>();
        List<MarketingList> marketingList = marketingListDAO.fetchAllMarketingListByPhoneNumberList(phoneNumbers);

        if (marketingList != null)
        {
            for (MarketingList marketingListObj : marketingList)
            {
                wsMarketingList.add(new WSMarketingList(marketingListObj));
            }
        }

        return wsMarketingList;
    }
}
