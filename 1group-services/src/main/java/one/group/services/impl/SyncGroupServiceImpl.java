package one.group.services.impl;

import java.security.NoSuchAlgorithmException;

import one.group.dao.SyncGroupDAO;
import one.group.entities.api.response.v2.WSAccount;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.services.AccountService;
import one.group.services.SyncGroupService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;

public class SyncGroupServiceImpl implements SyncGroupService
{
    private SyncGroupDAO syncGroupDAO;

    private AccountService accountService;

    public SyncGroupDAO getSyncGroupDAO()
    {
        return syncGroupDAO;
    }

    public void setSyncGroupDAO(SyncGroupDAO syncGroupDAO)
    {
        this.syncGroupDAO = syncGroupDAO;
    }

    public void saveCount(String count)
    {
        syncGroupDAO.addCount(count);
    }

    @Profiled(tag = "SyncGroupService-fetchGroupContent")
    public String fetchGroupContent(String groupkey)
    {
        return syncGroupDAO.fetchGroupContent("total_count");
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public String getInviteCode(String token) throws AuthenticationException, ClientProcessException, AccountNotFoundException, NoSuchAlgorithmException
    {
        Validation.notNull(token, "Auth token passed should not be null.");
        WSAccount requestingWSAccount = accountService.fetchAccountFromAuthToken(token);
        String phoneNumber = requestingWSAccount.getPrimaryMobile();
        return Utils.generateInviteCode(phoneNumber);
    }

    public boolean isFullRelease()
    {
        return syncGroupDAO.isFullRelease();
    }

}
