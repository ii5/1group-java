package one.group.services.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import one.group.core.enums.InviteStatus;
import one.group.dao.InviteLogDAO;
import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.WSMarketingList;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.jpa.InviteLog;
import one.group.services.InviteLogService;
import one.group.services.MarketingListService;
import one.group.services.SMSService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InviteLogServiceImpl implements InviteLogService
{
    private final static Logger logger = LoggerFactory.getLogger(InviteLogServiceImpl.class);

    private InviteLogDAO inviteLogDAO;
    private MarketingListService marketingListService;
    private SMSService smsCountryService;

    private long inviteDurationInMillies;

    public long getInviteDurationInMillies()
    {
        return inviteDurationInMillies;
    }

    public void setInviteDurationInMillies(long inviteDurationInMillies)
    {
        this.inviteDurationInMillies = inviteDurationInMillies;
    }

    public InviteLogDAO getInviteLogDAO()
    {
        return inviteLogDAO;
    }

    public void setInviteLogDAO(InviteLogDAO inviteLogDAO)
    {
        this.inviteLogDAO = inviteLogDAO;
    }

    public MarketingListService getMarketingListService()
    {
        return marketingListService;
    }

    public void setMarketingListService(MarketingListService marketingListService)
    {
        this.marketingListService = marketingListService;
    }

    public SMSService getSmsCountryService()
    {
        return smsCountryService;
    }

    public void setSmsCountryService(SMSService smsCountryService)
    {
        this.smsCountryService = smsCountryService;
    }

    @Transactional
    public void saveInviteLog(InviteLog inviteLog)
    {
        inviteLogDAO.saveInviteLog(inviteLog);
    }

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<String> inviteList)
    {
        Map<String, WSInviteContact> phoneNumbers = new HashMap<String, WSInviteContact>();
        if (inviteList != null && !inviteList.isEmpty())
        {
            for (String inviteLog : inviteList)
            {
                String phoneNumber = inviteLog;
                WSInviteContact ic = new WSInviteContact();
                ic.setStatus(InviteStatus.INVITE_SENT);
                ic.setNumber(phoneNumber);
                ic.setGroupCount(0);
                phoneNumbers.put(phoneNumber, ic);
            }
        }
        return phoneNumbers;
    }

    @Profiled(tag = "InviteLogServiceImpl-fetchAllPhoneNumberOfInvitedLogOfAccount")
    public List<String> fetchAllPhoneNumberOfInvitedLogOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account id should not be null or empty");

        return inviteLogDAO.findAllInvitedByAccount(accountId, inviteDurationInMillies);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "InviteLogServiceImpl-saveInviteLogAndSendInvitation")
    public void sendInvitationAndUpdateMarketingList(List<String> phoneNumbers, WSAccount wsAccount) throws IOException
    {
        Validation.notNull(wsAccount, "WSAccount should not be null");

        // Fetch all marketing list objects for invite list
        HashSet<WSMarketingList> wsMarketingList = marketingListService.fetchMarketingListByPhoneNumberList(phoneNumbers);

        String accountId = wsAccount.getId();
        String senderFullName = wsAccount.getFullName();

        // send invitation for each phone numbers received, and save each in
        // marketing list and invite log
        for (String phoneNumber : phoneNumbers)
        {
            // Replace all character with (*) except (0x0-0x7F / 32-127 )
            String fullName = senderFullName.replaceAll("[^\\x00-\\x7F]", "*");

            // Send SMS to phone number
            if (fullName.length() > 60)
            {
                fullName = fullName.substring(0, 57) + "...";
            }
            smsCountryService.sendSMS(phoneNumber, fullName + " has invited you to join their private property group. Join here: http://1group.com/m/invite");

            // White list number by adding number into marketing list table
            WSMarketingList wsMarketingListObj = new WSMarketingList(phoneNumber);
            if (!wsMarketingList.contains(wsMarketingListObj))
            {
                marketingListService.checkAndSaveInvitedPhoneNumbers(phoneNumber);
            }

            // Save invitation log
            InviteLog inviteLog = new InviteLog();
            inviteLog.setFromAccountId(accountId);
            inviteLog.setToPhoneNumber(phoneNumber);
            inviteLog.setSentTime(new Date());
            inviteLogDAO.saveInviteLog(inviteLog);
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendInvitation(List<String> phoneNumbers, String notificationTemplate) throws IOException
    {
        for (String phoneNumber : phoneNumbers)
        {
            smsCountryService.sendSMS(phoneNumber, notificationTemplate);
        }
    }
}
