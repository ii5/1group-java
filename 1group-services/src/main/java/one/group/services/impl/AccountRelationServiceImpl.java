package one.group.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import javax.transaction.Transactional;

import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.dao.AccountRelationDAO;
import one.group.dao.CityDAO;
import one.group.dao.MediaDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.entities.api.request.WSContactsInfo;
import one.group.entities.api.request.v2.WSContacts;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSPhotoReference;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.NativeContacts;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.ChatThreadService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountRelationServiceImpl implements AccountRelationService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountRelationServiceImpl.class);

    private AccountDAO accountDAO;

    private AccountRelationDAO accountRelationDAO;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private ChatThreadService chatThreadService;

    private AccountService accountService;

    private CityDAO cityDAO;

    private MediaDAO mediaDAO;

    private PhoneNumberDAO phoneNumberDAO;

    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public MediaDAO getMediaDAO()
    {
        return mediaDAO;
    }

    public void setMediaDAO(MediaDAO mediaDAO)
    {
        this.mediaDAO = mediaDAO;
    }

    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }

    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public AccountRelationDAO getAccountRelationDAO()
    {
        return accountRelationDAO;
    }

    public void setAccountRelationDAO(AccountRelationDAO accountRelationDAO)
    {
        this.accountRelationDAO = accountRelationDAO;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public ChatThreadService getChatThreadService()
    {
        return chatThreadService;
    }

    public void setChatThreadService(ChatThreadService chatThreadService)
    {
        this.chatThreadService = chatThreadService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountRelationService-createOrUpdateAccountRelationBlock")
    public void createOrUpdateAccountRelationBlock(String requestByAccountId, String otherAccountId) throws SyncLogProcessException
    {
        Account requestByAccount, otherAccount = null;

        requestByAccount = accountDAO.fetchAccountById(requestByAccountId);
        otherAccount = accountDAO.fetchAccountById(otherAccountId);

        // Check for Account Relation Exists
        List<AccountRelations> accountRelationList = accountRelationDAO.fetchByRequestByAccountAndToAccount(requestByAccount, otherAccount);

        if (accountRelationList.isEmpty())
        {
            // 1. For Create Relation other Fields Should be false Except
            // isBlocked,isBlockedBy
            // 2. Setting is_blocked = true for Requesting Account and
            // is_blocked_by = true for Blocked Account(Requested Account)
            createUpdateAccountRelation(requestByAccount, otherAccount, false, false, true, false, 0, 0);

        }
        else
        {
            // For Update Relation other Fields Should be Null Except
            // isBlocked,isBlockedBy

            AccountRelations accountRelation = accountRelationList.get(0);
            // 1. if First Record account_id not equal to Requested
            // account_id then
            // 2. set existing flag & true Flag for first Relation, and vice
            // versa for second Mirror Relation
            // else
            // 3. setting True flag & null(No Changed) for first Relation,
            // and
            // vice versa for second Mirror Relation

            if (!requestByAccount.equals(accountRelation.getAccount()))
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, accountRelation.isBlocked(), true, null, null);
            }
            else
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, true, null, null, null);
            }
        }
        logger.info("Pushing to synclog from updateAccountRelationScore for accountId :" + requestByAccountId);
        pushHintToSyncLog(HintType.FETCH, EntityType.ACCOUNT_RELATION, requestByAccountId, SyncActionType.BLOCK, otherAccountId);

        // Remove chat thread subscription. If system wants to send chat message
        // then we need subscription to deliver chat message
        chatThreadService.removeChatThreadSubscription(requestByAccountId, otherAccountId);
    }

    @Profiled(tag = "AccountRelationService-createUpdateAccountRelation")
    @Transactional(rollbackOn = { Exception.class })
    public void createUpdateAccountRelation(Account account, Account otherAccount, Boolean isContact, Boolean isContactOf, Boolean isBlocked, Boolean isBlockedBy, Integer score, Integer scoredAs)

    {
        Boolean isContactAry[] = { isContact, isContactOf };
        Boolean isBlockedAry[] = { isBlocked, isBlockedBy };
        Integer scoreAry[] = { score, scoredAs };
        List<AccountRelations> existingRelations = accountRelationDAO.fetchByRequestByAccountAndToAccount(account, otherAccount);
        if (!account.getIdAsString().equals(otherAccount.getIdAsString()))
        {
            AccountRelations relations = new AccountRelations(account, otherAccount.getIdAsString());
            AccountRelations relationMirror = new AccountRelations(otherAccount, account.getIdAsString());
            AccountRelations relation = null;
            for (int i = 0; i < 2; i++)
            {
                if (existingRelations.size() > 0)
                {// update
                    relation = existingRelations.get(i);
                    relation = createUpdateAccountRelation(relation, isContactAry[i % 2], isContactAry[1 - i], isBlockedAry[i % 2], isBlockedAry[1 - i], scoreAry[i % 2], scoreAry[1 - i]);
                    relation.setUpdatedTime(new Timestamp(System.currentTimeMillis()));
                    accountDAO.updateAccountRelation(relation);
                }
                else
                {// insert
                    relation = (i == 0 ? relations : relationMirror);
                    relation.setCreatedTime(new Timestamp(System.currentTimeMillis()));
                    relation = createUpdateAccountRelation(relation, isContactAry[i % 2], isContactAry[1 - i], isBlockedAry[i % 2], isBlockedAry[1 - i], scoreAry[i % 2], scoreAry[1 - i]);

                    if (!accountRelationDAO.isAccountRelationExist(relation))
                        accountDAO.createAccountRelation(relation);
                }
            }
        }
    }

    private AccountRelations createUpdateAccountRelation(AccountRelations relation, Boolean isContact, Boolean isContactOf, Boolean isBlocked, Boolean isBlockedBy, Integer score, Integer scoredAs)
    {
        relation.setContact(isContact == null ? relation.isContact() : isContact);
        relation.setContactOf(isContactOf == null ? relation.isContactOf() : isContactOf);
        relation.setBlocked(isBlocked == null ? relation.isBlocked() : isBlocked);
        relation.setBlockedBy(isBlockedBy == null ? relation.isBlockedBy() : isBlockedBy);
        relation.setScore(score == null ? relation.getScore() : score);
        relation.setScoredAs(scoredAs == null ? relation.getScoredAs() : scoredAs);

        return relation;
    }

    // @Transactional(rollbackOn = { Exception.class })
    // @Profiled(tag = "AccountRelationService-fetchContactsOfAccount")
    public Set<WSAccountRelations> fetchContactsOfAccount(String accountId)
    {
        Account account = (Agent) accountDAO.fetchAccountById(accountId);
        Set<AccountRelations> accountRelations = account.getAccountRelations();
        Set<WSAccountRelations> wsAccountRelations = new LinkedHashSet<WSAccountRelations>();
        for (AccountRelations relations : accountRelations)
        {
            // returning all account relations with flags
            WSAccountRelations wsAccountRelation = new WSAccountRelations(relations.getOtherAccountId(), relations.isContact(), relations.isContactOf(), relations.isBlocked(), relations.isBlockedBy());
            wsAccountRelations.add(wsAccountRelation);
        }
        return wsAccountRelations;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountRelationService-fetchContactsOfAccount")
    public Set<WSAccount> fetchContactsOfAccountDetail(String accountId) throws AccountNotFoundException
    {
        long start = System.currentTimeMillis();
        final Set<WSAccount> contacts = new HashSet<WSAccount>();
        List<String> contactList = accountRelationDAO.fetchContactsByAccountId(accountId);
        long end = System.currentTimeMillis();
        logger.info("Fetch Contacts : " + (end - start));
        start = System.currentTimeMillis();
        if (contactList == null || contactList.isEmpty())
        {
            return new HashSet<WSAccount>();
        }

        List<Account> contactFullList = accountDAO.fetchSelectiveDataAccountsByIds(new HashSet<String>(contactList));
        end = System.currentTimeMillis();
        logger.info("Fetch Account Information : " + (end - start));

        start = System.currentTimeMillis();
        final Map<String, Map<String, String>> accountMetaDataMap = new HashMap<String, Map<String, String>>();
        final Set<String> cityIdSet = new HashSet<String>();
        final Set<String> photoIdSet = new HashSet<String>();
        final Set<String> primaryPhoneNumberSet = new HashSet<String>();

        Map<String, WSAccountRelations> mapOtherAccountVsRelation = fetchAccountRelationsAsMap(accountId, contactList);
        final Map<String, WSAccountRelations> otherAccountVsRelationMap = new HashMap<String, WSAccountRelations>(mapOtherAccountVsRelation);

        contactFullList.stream().forEach(new Consumer<Account>()
        {
            public void accept(Account a)
            {
                WSAccount wsAccount = new WSAccount(a, false, false, false);

                WSAccountRelations wsAccountRelation = otherAccountVsRelationMap.get(a.getId());
                if (wsAccountRelation != null)
                {
                    wsAccount.setIsBlocked(wsAccountRelation.getIsBlocked());
                    wsAccount.setIsContact(wsAccountRelation.getIsContact());
                    wsAccount.setScore(wsAccountRelation.getScore());
                    if (wsAccountRelation.getIsBlocked())
                    {
                        wsAccount.setCanChat(false);
                    }
                }

                contacts.add(wsAccount);

                Map<String, String> metaData = accountMetaDataMap.get(a.getId());

                if (metaData == null)
                {
                    metaData = new HashMap<String, String>();
                    accountMetaDataMap.put(a.getId(), metaData);
                }

                if (a.getCityId() != null)
                {
                    metaData.put("city", a.getCityId());
                    cityIdSet.add(a.getCityId());
                }

                if (a.getFullPhotoId() != null)
                {
                    metaData.put("fullPhoto", a.getFullPhotoId());
                    photoIdSet.add(a.getFullPhotoId());
                }

                if (a.getThumbnailPhotoId() != null)
                {
                    metaData.put("thumbnailPhoto", a.getThumbnailPhotoId());
                    photoIdSet.add(a.getThumbnailPhotoId());
                }

                if (a.getPrimaryPhoneNumberId() != null)
                {
                    metaData.put("primaryNumber", a.getPrimaryPhoneNumberId());
                    if (!primaryPhoneNumberSet.add(a.getPrimaryPhoneNumberId()))
                    {
                        logger.info("Duplicate mobile for account : " + a.getId());
                    }
                }
                else
                {
                    logger.info("No mobile: " + a.getId());
                }

            }

        });

        logger.info("Initial Mobile Numbers fetched " + primaryPhoneNumberSet.size());
        logger.info("Initial Cities fetched " + cityIdSet.size());
        logger.info("Initial thumbnail fetched " + photoIdSet.size());

        end = System.currentTimeMillis();
        logger.info("Form meta data : " + (end - start));

        start = System.currentTimeMillis();
        final List<City> cityList = cityDAO.getAllCitiesByCityIds(cityIdSet);
        final Map<String, City> idVsCityMap = new ConcurrentHashMap<String, City>();
        if (cityList != null && !cityList.isEmpty())
        {
            cityList.parallelStream().forEach(new Consumer<City>()
            {
                public void accept(City t)
                {
                    if (t == null)
                        return;

                    idVsCityMap.put(t.getId(), t);
                }
            });
        }
        end = System.currentTimeMillis();

        logger.info("Fetch City List (" + idVsCityMap.size() + "): " + (end - start));

        start = System.currentTimeMillis();
        final List<Media> mediaList = mediaDAO.fetchMediaByIds(photoIdSet);
        final Map<String, Media> idVsMediaMap = new ConcurrentHashMap<String, Media>();
        if (mediaList != null && !mediaList.isEmpty())
        {
            mediaList.parallelStream().forEach(new Consumer<Media>()
            {
                public void accept(Media t)
                {
                    if (t == null)
                        return;

                    idVsMediaMap.put(t.getId(), t);
                }
            });

        }
        end = System.currentTimeMillis();
        logger.info("Fetch Media List (" + idVsMediaMap.size() + "): " + (end - start));

        start = System.currentTimeMillis();
        final List<PhoneNumber> phoneNumberList = phoneNumberDAO.fetchPhoneNumbersByIds(primaryPhoneNumberSet);
        final Map<String, PhoneNumber> idVsNumberMap = new ConcurrentHashMap<String, PhoneNumber>();
        if (phoneNumberList != null && !phoneNumberList.isEmpty())
        {
            phoneNumberList.parallelStream().forEach(new Consumer<PhoneNumber>()
            {
                public void accept(PhoneNumber t)
                {
                    if (t == null)
                        return;

                    idVsNumberMap.put(t.getId(), t);
                }
            });
        }
        end = System.currentTimeMillis();
        logger.info("Fetch PhoneNumber List (" + idVsNumberMap.size() + "): " + (end - start));
        primaryPhoneNumberSet.removeAll(idVsNumberMap.keySet());
        logger.info("Intersection: " + primaryPhoneNumberSet);

        start = System.currentTimeMillis();
        contacts.parallelStream().forEach(new Consumer<WSAccount>()
        {
            public void accept(WSAccount wsAccount)
            {
                if (wsAccount == null)
                    return;

                Map<String, String> metaData = accountMetaDataMap.get(wsAccount.getId());

                if (metaData == null)
                    return;

                if (cityList != null && !cityList.isEmpty() && metaData.get("city") != null)
                {
                    if (idVsCityMap.get(metaData.get("city")) != null)
                        wsAccount.setCity(new WSCity(idVsCityMap.get(metaData.get("city"))));
                }

                if (mediaList != null && !mediaList.isEmpty())
                {

                    Media thumbnail = (metaData.get("fullPhoto") == null) ? null : idVsMediaMap.get(metaData.get("thumbnailPhoto"));
                    Media full = metaData.get("fullPhoto") == null ? null : idVsMediaMap.get(metaData.get("fullPhoto"));
                    if (thumbnail != null || full != null)
                    {
                        WSPhotoReference photoReference = new WSPhotoReference(full, thumbnail);
                        wsAccount.setPhoto(photoReference);
                    }
                }

                if (phoneNumberList != null && !phoneNumberList.isEmpty() && metaData.get("primaryNumber") != null)
                {
                    if (idVsNumberMap.get(metaData.get("primaryNumber")) != null)
                        wsAccount.setPrimaryMobile(idVsNumberMap.get(metaData.get("primaryNumber")).getNumber());
                }

            }
        });
        end = System.currentTimeMillis();
        logger.info("Form response (" + contacts.size() + "): " + (end - start));

        return contacts;
    }

    // private void processAccountRelations(Account account, Account
    // otherAccount, Set<String> deltaContacts, boolean isContact, boolean
    // isBlocked, int score)
    // {
    // AccountRelations accountRelation =
    // accountRelationDAO.fetchByAccountAndOtherAccount(account.getId(),
    // otherAccount.getId());
    // AccountRelations accountRelationMirror =
    // accountRelationDAO.fetchByAccountAndOtherAccount(otherAccount.getId(),
    // account.getId());
    // if (accountRelation == null)
    // {
    // accountRelation = new AccountRelations();
    // accountRelationMirror = new AccountRelations();
    // deltaContacts.add(otherAccount.getId());
    // accountRelation.setCreatedTime(new
    // Timestamp(System.currentTimeMillis()));
    // accountRelationMirror.setCreatedTime(new
    // Timestamp(System.currentTimeMillis()));
    // }
    //
    // if (!accountRelation.isContact())
    // {
    // accountRelation =
    // accountRelationDAO.createUpdateAccountRelation(accountRelation, false);
    // accountRelation.setAccount(account);
    // accountRelation.setOtherAccountId(otherAccount.getId());
    // accountRelation.setContact(isContact);
    //
    // accountRelationMirror =
    // accountRelationDAO.createUpdateAccountRelation(accountRelationMirror,
    // false);
    // accountRelationMirror.setAccount(otherAccount);
    // accountRelationMirror.setOtherAccountId(account.getId());
    // accountRelationMirror.setContactOf(isContact);
    //
    // accountRelationDAO.callFlush();
    //
    // deltaContacts.add(otherAccount.getId());
    //
    // }
    // }

    @Transactional(rollbackOn = { Exception.class })
    private void persistAccountRelationsBulk(Account account, Set<Account> otherAccounts, Set<String> deltaContacts, boolean isForwardDirection)
    {

        long start = Utils.getSystemTime();
        logger.debug("persistAccountRelationsBulk start:" + start);

        // Set<AccountRelations> existingAccountRelations =
        // account.getAccountRelations();
        List<AccountRelations> newAccountRelations = new ArrayList<AccountRelations>();
        List<AccountRelations> updateAccountRelations = new ArrayList<AccountRelations>();
        List<String> otherAccountIds = new ArrayList<String>();

        for (Account otherAccount : otherAccounts)
        {
            if (!otherAccountIds.contains(otherAccount.getId()))
            {
                otherAccountIds.add(otherAccount.getId());
            }
        }

        Map<String, AccountRelations> accountVsOtherAccountsRelation = fetchRawAccountRelationsAsMap(account.getId());
        Map<String, AccountRelations> otherAccountsVsAccountRelation = new HashMap<String, AccountRelations>();
        if (!otherAccountIds.isEmpty())
        {
            otherAccountsVsAccountRelation = fetchRawInverseAccountRelationsAsMap(account.getId(), otherAccountIds);
        }

        for (Account otherAccount : otherAccounts)
        {
            if (otherAccount != null && (otherAccount.getStatus().equals(Status.ACTIVE) || otherAccount.getStatus().equals(Status.SEEDED)) && account.getStatus().equals(Status.ACTIVE)
                    && !(account.equals(otherAccount)))
            {
                AccountRelations accountRelation = new AccountRelations(account, otherAccount.getId());
                AccountRelations accountRelationMirror = new AccountRelations(otherAccount, account.getId());

                if (accountVsOtherAccountsRelation.containsKey(otherAccount.getId()) || deltaContacts.contains(otherAccount.getId()))
                {// This is an update
                    AccountRelations existingAccountRelation = accountVsOtherAccountsRelation.get(otherAccount.getId());
                    AccountRelations existingMirrorAccountRelation = otherAccountsVsAccountRelation.get(otherAccount.getId());

                    if (!existingAccountRelation.isContact())
                    {
                        if (isForwardDirection)
                        {
                            existingAccountRelation.setContact(true);
                            existingMirrorAccountRelation.setContactOf(true);
                        }
                        else
                        {
                            existingMirrorAccountRelation.setContact(true);
                            existingAccountRelation.setContactOf(true);

                        }
                        // accountRelationDAO.updateAccountRelation(existingAccountRelation);
                        // accountRelationDAO.updateAccountRelation(existingMirrorAccountRelation);
                        updateAccountRelations.add(existingAccountRelation);
                        updateAccountRelations.add(existingMirrorAccountRelation);
                        existingAccountRelation.setUpdatedTime(new Timestamp(System.currentTimeMillis()));
                        existingMirrorAccountRelation.setUpdatedTime(new Timestamp(System.currentTimeMillis()));
                        deltaContacts.add(otherAccount.getId());
                    }
                }
                else
                {// this is new insert
                    deltaContacts.add(otherAccount.getId());
                    accountRelation.setCreatedTime(new Timestamp(System.currentTimeMillis()));
                    accountRelationMirror.setCreatedTime(new Timestamp(System.currentTimeMillis()));
                    if (isForwardDirection)
                    {
                        accountRelation.setContact(true);
                        accountRelationMirror.setContactOf(true);
                    }
                    else
                    {
                        accountRelationMirror.setContact(true);
                        accountRelation.setContactOf(true);

                    }
                    newAccountRelations.add(accountRelation);
                    newAccountRelations.add(accountRelationMirror);
                }
                // accountRelationDAO.persistAllAccountRelation(newAccountRelations);
            }
        }
        accountRelationDAO.saveOrUpdateAllAccountRelation(updateAccountRelations);
        accountRelationDAO.persistAllAccountRelation(newAccountRelations);

        logger.debug("persistAccountRelationsBulk end: " + (Utils.getSystemTime() - start));
    }

    // @Transactional(rollbackOn = { Exception.class })
    // @Profiled(tag = "AccountRelationService-syncContactsForAccount")
    // public WSContactsInfo syncContactsForAccount(String accountId, WSContacts
    // contacts) throws Abstract1GroupException, IOException
    // {
    // boolean isInitialContactsSyncFlagFlipped = false;
    // Set<String> deltaContacts = new HashSet<String>();
    // Account account = accountDAO.fetchAccountById(accountId);
    // List<String> phoneNumberList = new ArrayList<String>();
    // List<WSNativeContact> nativeContacts = contacts.getNativeContacts();
    // boolean isInitialContactsSyncComplete =
    // contacts.isInitialContactsSyncComplete();
    // for (WSNativeContact wsNativeContact : nativeContacts)
    // {
    // String fullName = wsNativeContact.getFullName();
    // String companyName = wsNativeContact.getCompanyName();
    // List<String> phoneNumbers = (List<String>) wsNativeContact.getNumbers();
    // if (phoneNumbers == null)
    // continue;
    // logger.debug("Extracted info for a row. fullName :" + fullName +
    // " companyName :" + companyName + " phoneNumbers :" + phoneNumbers);
    // for (String phoneNumber : phoneNumbers)
    // {
    // Account otherAccount = accountDAO.fetchAccountByPhoneNumber(phoneNumber);
    // if (account.equals(otherAccount))
    // continue;
    // if (otherAccount != null &&
    // otherAccount.getStatus().equals(Status.ACTIVE) &&
    // account.getStatus().equals(Status.ACTIVE))
    // {
    // logger.debug("Got a Movein Account: " + otherAccount.getId() +
    // " ,relationship will be established and entered in AccountRelations table");
    // // Moving contact so going in AccountRelations table
    // processAccountRelations(account, otherAccount, deltaContacts, true,
    // false, 0);
    // }
    // // Also going in Native contacts
    // if (phoneNumber != null && !phoneNumber.trim().isEmpty())
    // {
    //
    // NativeContacts nativeContact = accountDAO.fetchNativeContacts(account,
    // phoneNumber);
    // if (nativeContact == null)
    // {
    // nativeContact = new NativeContacts();
    // nativeContact.setCreatedTime(new Timestamp(System.currentTimeMillis()));
    // }
    // nativeContact = accountDAO.updateNativeContacts(nativeContact);
    // nativeContact.setAccount(account);
    // nativeContact.setCompanyName(companyName);
    // nativeContact.setFullName(fullName);
    // nativeContact.setPhoneNumber(phoneNumber);
    //
    // }
    //
    // phoneNumberList.add(phoneNumber);
    // }
    // }
    // if (isInitialContactsSyncComplete)
    // {
    // isInitialContactsSyncFlagFlipped =
    // !account.isInitialContactsSyncComplete();
    // account.setInitialContactsSyncComplete(isInitialContactsSyncComplete);
    // accountDAO.saveAccount(account);
    // }
    // WSContactsInfo contactsInfo = new WSContactsInfo();
    // contactsInfo.setContactsSyncFlagFlipped(isInitialContactsSyncFlagFlipped);
    // contactsInfo.setDeltaContacts(deltaContacts);
    // return contactsInfo;
    // }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountRelationService-syncContactsForAccountBulk")
    public WSContactsInfo syncContactsForAccountBulk(String accountId, WSContacts contacts) throws Abstract1GroupException
    {
        boolean isInitialContactsSyncFlagFlipped = false;
        Set<String> deltaContacts = new HashSet<String>();
        Account account = accountDAO.fetchAccountById(accountId);
        List<WSNativeContact> nativeContacts = contacts.getNativeContacts();
        boolean isInitialContactsSyncComplete = contacts.isInitialContactsSyncComplete();
        Set<String> allPhoneNumbers = getAllPhoneNumbers(nativeContacts);
        Set<Account> otherAccounts = accountDAO.fetchAccountsByPhoneNumbers(allPhoneNumbers);

        persistAccountRelationsBulk(account, otherAccounts, deltaContacts, true);
        persistNativeContactsBulk(account, nativeContacts);

        if (isInitialContactsSyncComplete)
        {
            isInitialContactsSyncFlagFlipped = !account.isInitialContactsSyncComplete();
            account.setInitialContactsSyncComplete(isInitialContactsSyncComplete);
            accountDAO.saveAccount(account);
        }
        WSContactsInfo contactsInfo = new WSContactsInfo();
        contactsInfo.setContactsSyncFlagFlipped(isInitialContactsSyncFlagFlipped);
        contactsInfo.setDeltaContacts(deltaContacts);
        return contactsInfo;
    }

    private Set<String> getAllPhoneNumbers(List<WSNativeContact> nativeContacts)
    {
        Set<String> allPhoneNumbers = new HashSet<String>();
        for (WSNativeContact wsNativeContact : nativeContacts)
        {
            allPhoneNumbers.addAll(wsNativeContact.getNumbers());

        }
        return allPhoneNumbers;
    }

    private WSNativeContact getNativeContactForPhoneNumber(List<WSNativeContact> nativeContacts, String phoneNumber)
    {
        WSNativeContact nativeContactToReturn = null;
        for (WSNativeContact nativeContact : nativeContacts)
        {
            if (nativeContact.getNumbers().contains(phoneNumber))
            {
                nativeContactToReturn = nativeContact;
                break;
            }

        }
        return nativeContactToReturn;
    }

    private void persistNativeContactsBulk(Account account, List<WSNativeContact> nativeContacts)
    {
        Set<String> allPhoneNumbers = new HashSet<String>();
        for (WSNativeContact nativeContact : nativeContacts)
        {
            List<String> phoneNumbers = nativeContact.getNumbers();
            allPhoneNumbers.addAll(phoneNumbers);
        }

        List<NativeContacts> existingNativeContacts = accountDAO.fetchNativeContactsForMultiplePhoneNumbers(account, allPhoneNumbers);
        List<NativeContacts> newNativeContacts = new ArrayList<NativeContacts>();
        for (String phoneNumber : allPhoneNumbers)
        {
            NativeContacts nativeContact = new NativeContacts();
            nativeContact.setAccount(account);
            nativeContact.setPhoneNumber(phoneNumber);
            WSNativeContact contact = getNativeContactForPhoneNumber(nativeContacts, phoneNumber);
            if (existingNativeContacts.contains(nativeContact))
            {
                NativeContacts existingNativeContact = accountDAO.fetchNativeContacts(account, phoneNumber);
                // Existing native contact, we need to update
                existingNativeContact = accountDAO.updateNativeContacts(existingNativeContact);
                existingNativeContact.setCompanyName(contact.getCompanyName());
                existingNativeContact.setFullName(contact.getFullName());
            }
            else
            {
                // New native contact, we need to persist
                nativeContact.setCreatedTime(new Timestamp(System.currentTimeMillis()));
                nativeContact.setFullName(contact.getFullName());
                nativeContact.setCompanyName(contact.getCompanyName());
                newNativeContacts.add(nativeContact);
            }
        }
        accountDAO.persistAllNativeContacts(newNativeContacts);
    }

    // @Transactional
    // @Profiled(tag = "AccountRelationService-syncContactsForAccount")
    // public Set<String> syncContactsForAccount(String accountId) throws
    // Abstract1GroupException
    // {
    // Set<String> deltaContacts = new HashSet<String>();
    // Account account = accountDAO.fetchAccountById(accountId);
    // String phoneNumber = account.getPrimaryPhoneNumber().getNumber();
    // logger.debug("Registered user account id: " + accountId +
    // " and phone number :" + phoneNumber);
    // Set<Account> newContacts =
    // accountDAO.fetchAccountsLinkedWithPhonenumber(phoneNumber);
    // logger.debug("Got contacts linked with phone number: size" +
    // newContacts.size());
    // // Scenario 1
    // // Account 1 is registering whose phone number is in Account 2 contact
    // // list and Account 2 has already synced
    // // their contact list.
    // for (Account newContact : newContacts)
    // {
    // if (account != null && newContact != null &&
    // newContact.getStatus().equals(Status.ACTIVE))
    // {
    // // createUpdateAccountRelation(account, newContact, false, true,
    // // false, false, 0, 0);
    // // deltaContacts.add(newContact.getId());
    // processAccountRelations(newContact, account, deltaContacts, true, false,
    // 0);
    // }
    // }
    //
    // // Scenario 2
    // // Account 1 is registering who had synced their contact list and had
    // // Account 2 in their contact list while initial sync
    // Set<String> linkedPhoneNumbers =
    // accountDAO.fetchPhonenumbersLinkedWithAccount(account);
    // for (String linkedPhoneNumber : linkedPhoneNumbers)
    // {
    // Account linkedAccount =
    // accountDAO.fetchAccountByPhoneNumber(linkedPhoneNumber);
    // if (account != null && linkedAccount != null &&
    // linkedAccount.getStatus().equals(Status.ACTIVE))
    // {
    // processAccountRelations(account, linkedAccount, deltaContacts, true,
    // false, 0);
    // }
    // }
    // logger.info("Pushing to synclog from syncContactsForAccount for accountId :"
    // + accountId);
    // return deltaContacts;
    // }

    @Transactional
    @Profiled(tag = "AccountRelationService-syncContactsForAccountBulkRegistration")
    public Set<String> syncContactsForAccountBulk(String accountId) throws Abstract1GroupException
    {
        long start = Utils.getSystemTime();
        logger.info("syncContactsForAccountBulk start: " + start);

        Set<String> deltaContacts = new HashSet<String>();
        Account account = accountDAO.fetchAccountById(accountId);

        String phoneNumber = account.getPrimaryPhoneNumber().getNumber();
        start = Utils.getSystemTime();
        Set<Account> newContacts = accountDAO.fetchAccountsLinkedWithPhonenumber(phoneNumber);
        logger.info("fetchAccountsLinkedWithPhonenumber end" + (Utils.getSystemTime() - start));
        start = Utils.getSystemTime();
        persistAccountRelationsBulk(account, newContacts, deltaContacts, false);
        logger.info("persistAccountRelationsBulk end" + (Utils.getSystemTime() - start));
        newContacts.clear();
        Set<String> linkedPhoneNumbers = accountDAO.fetchPhonenumbersLinkedWithAccount(account);
        if (linkedPhoneNumbers != null && !linkedPhoneNumbers.isEmpty())
            newContacts.addAll(accountDAO.fetchAccountsByPhoneNumbers(linkedPhoneNumbers));

        start = Utils.getSystemTime();
        persistAccountRelationsBulk(account, newContacts, deltaContacts, true);
        logger.info("persistAccountRelationsBulk end 2" + (Utils.getSystemTime() - start));
        logger.info("syncContactsForAccountBulk end: " + (Utils.getSystemTime() - start));

        return deltaContacts;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountRelationService-updateAccountRelationUnblock")
    public void updateAccountRelationUnblock(String requestByAccountId, String otherAccountId) throws General1GroupException, SyncLogProcessException
    {
        Account requestByAccount, otherAccount = null;
        Set<AccountRelations> relations = null;

        requestByAccount = accountDAO.fetchAccountById(requestByAccountId);
        otherAccount = accountDAO.fetchAccountById(otherAccountId);

        // Check for Account Relation Exists
        List<AccountRelations> accountRelationList = accountRelationDAO.fetchByRequestByAccountAndToAccount(requestByAccount, otherAccount);
        if (accountRelationList.isEmpty())
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception during UnBlock Account.", new IllegalStateException());
        }
        else
        {

            AccountRelations blockaccount = accountRelationDAO.fetchByAccountAndOtherAccount(requestByAccount.getId(), otherAccount.getId());
            // Allowed To unblock only if it is blocked by Requesting
            // Account
            if (blockaccount.isBlockedBy() && !blockaccount.isBlocked())
            {
                throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception during UnBlock Account.", new IllegalStateException());
            }

            AccountRelations accountRelation = accountRelationList.get(0);
            // 1. if First Record account_id not equal to Requested
            // account_id then
            // 2. set existing flag & false Flag for first Relation, and
            // vice
            // versa for second Mirror Relation
            // else
            // 3. setting false flag & null(No Changed) for first Relation,
            // and
            // vice versa for second Mirror Relation
            if (!requestByAccount.equals(accountRelation.getAccount()))
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, accountRelation.isBlocked(), false, null, null);
            }
            else
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, false, null, null, null);
            }
        }

        logger.info("Pushing to synclog from updateAccountRelationUnblock for accountId :" + requestByAccountId);
        pushHintToSyncLog(HintType.FETCH, EntityType.ACCOUNT_RELATION, requestByAccountId, SyncActionType.UNBLOCK, otherAccountId);

        // Subscription to chat thread
        chatThreadService.addChatThreadSubscription(requestByAccountId, otherAccountId);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccountRelationService-updateAccountRelationScore")
    public void updateAccountRelationScore(String requestByAccountId, String otherAccountId, int score) throws General1GroupException, SyncLogProcessException
    {
        Account requestByAccount, otherAccount = null;
        Set<AccountRelations> relations = null;
        requestByAccount = accountDAO.fetchAccountById(requestByAccountId);
        otherAccount = accountDAO.fetchAccountById(otherAccountId);

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchByRequestByAccountAndToAccount(requestByAccount, otherAccount);
        // Check for Account Relation Exists
        if (accountRelationList.isEmpty())
        {
            // For Create Relation other Fields Should be false Except
            // Score,scoredAs,Default Score is 0
            createUpdateAccountRelation(requestByAccount, otherAccount, false, false, false, false, score, 0);

        }
        else
        {
            // For Update Relation other Fields Should be null Except
            // Score,scoredAs,Default Score is 0
            AccountRelations accountRelation = accountRelationList.get(0);
            // 1. if First Record account_id not equal to Requested
            // account_id then
            // 2. set existing score & new Score for first Relation, and
            // vice
            // versa for second Mirror Relation
            // else
            // 3. setting new Score & null(No Changed) for first Relation,
            // and
            // vice versa for second Mirror Relation
            if (!requestByAccount.equals(accountRelation.getAccount()))
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, null, null, accountRelation.getScore(), score);
            }
            else
            {
                createUpdateAccountRelation(requestByAccount, otherAccount, null, null, null, null, score, null);
            }
        }

        logger.info("Pushing to synclog from updateAccountRelationScore for accountId :" + requestByAccountId);
        pushHintToSyncLog(HintType.FETCH, EntityType.ACCOUNT_RELATION, requestByAccountId, SyncActionType.SCORE, otherAccountId);
    }

    @Profiled(tag = "AccountRelationService-fetchAccountRelation")
    @Transactional(rollbackOn = { Exception.class })
    public WSAccountRelations fetchAccountRelation(String accountId, String otherAccountId)
    {
        AccountRelations accountRelation = accountRelationDAO.fetchByAccountAndOtherAccount(accountId, otherAccountId);
        if (accountRelation == null)
        {
            return null;
        }
        WSAccountRelations wsAccountRelation = new WSAccountRelations(accountRelation);
        return wsAccountRelation;

    }

    @Profiled(tag = "AccountRelationService-createAccountBlockByKey")
    public void createAccountBlockByKey(String otherAccountId, String requestByAccountId)
    {
        accountRelationDAO.createAccountBlockByKey(otherAccountId, requestByAccountId);
    }

    @Profiled(tag = "AccountRelationService-removeAccountFromBlockedBy")
    public void removeAccountFromBlockedBy(String otherAccountId, String requestByAccountId)
    {
        accountRelationDAO.removeAccountFromBlockedBy(otherAccountId, requestByAccountId);

    }

    @Profiled(tag = "AccountRelationService-pushHintToSyncLog")
    public void pushHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType, String targetAccountId) throws SyncLogProcessException
    {
        // All clients of account should receive invalidation(account_id) of
        // targeted account
        WSEntityReference entityReference = new WSEntityReference(hintType, entityType, targetAccountId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();

        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(actionType);
        syncLogEntry.setAssociatedEntityType(entityType);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);
        syncLogEntry.setAdditionalData(SyncEntryKey.TARGET_ACCOUNT_ID, targetAccountId);
        logger.info("About to write to kafka log: " + syncLogEntry);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Transactional
    @Profiled(tag = "AccountRelationService-fetchAccountRelationsAsMap")
    public Map<String, WSAccountRelations> fetchAccountRelationsAsMap(String accountId)
    {
        Map<String, WSAccountRelations> accountVsRelationsMap = new HashMap<String, WSAccountRelations>();

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchByAccount(accountId);

        for (AccountRelations relation : accountRelationList)
        {
            accountVsRelationsMap.put(relation.getOtherAccountId(), new WSAccountRelations(relation));
        }

        return accountVsRelationsMap;
    }

    @Transactional
    @Profiled(tag = "AccountRelationService-fetchRawAccountRelationsAsMap")
    public Map<String, AccountRelations> fetchRawAccountRelationsAsMap(String accountId)
    {
        Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchByAccount(accountId);

        for (AccountRelations relation : accountRelationList)
        {
            accountVsRelationsMap.put(relation.getOtherAccountId(), relation);
        }

        return accountVsRelationsMap;
    }

    @Profiled(tag = "AccountRelationService-pushSyncHintToSyncLog")
    public void pushSyncHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType, WSContactsInfo contactsInfo) throws SyncLogProcessException
    {
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(actionType);
        syncLogEntry.setAssociatedEntityType(entityType);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.CONTACTS, contactsInfo);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Profiled(tag = "AccountRelationService-pushToSyncLog")
    public void pushToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType syncActionType, WSContactsInfo contactsInfo) throws SyncLogProcessException
    {
        // The entry in sync log is just put for audit purpose, there is no
        // processing of this entry on sync server
        // News feed generation during registration is happening in
        // AccountServiceImpl's
        // generateNewsFeedDuringRegistration method
        pushHintToSyncLog(hintType, entityType, accountId, syncActionType, null);
        // if (contactsInfo != null && contactsInfo.getDeltaContacts() != null
        // && !(contactsInfo.getDeltaContacts().isEmpty()))
        // {
        // deltaContacts.remove(accountId);
        pushSyncHintToSyncLog(hintType, EntityType.ACCOUNT_RELATION, accountId, syncActionType, contactsInfo);
        // }
    }

    @Profiled(tag = "AccountRelationService-isAccountBlocked")
    public boolean isAccountBlocked(String requestByAccountId, String otherAccountId)
    {
        List<AccountRelations> accountRelations = accountRelationDAO.fetchAccountRelationBetweenAccounts(requestByAccountId, otherAccountId);

        if (accountRelations == null)
        {
            return false;
        }
        else
        {
            for (AccountRelations accountRelation : accountRelations)
            {
                if (accountRelation.getOtherAccountId().equals(requestByAccountId) && accountRelation.isBlocked() == true)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList)
    {
        return accountRelationDAO.fetchAccountRelations(accountId, otherAccountIdList, AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID_LIST);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AccuntRelationServiceImpl-fetchAccountRelationsAsMap")
    public Map<String, WSAccountRelations> fetchAccountRelationsAsMap(String accountId, List<String> otherAccountIdList)
    {
        Map<String, WSAccountRelations> accountVsRelationsMap = new HashMap<String, WSAccountRelations>();

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchAccountRelations(accountId, otherAccountIdList, AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID_LIST);

        for (AccountRelations relation : accountRelationList)
        {
            accountVsRelationsMap.put(relation.getOtherAccountId(), new WSAccountRelations(relation));
        }

        return accountVsRelationsMap;
    }

    public Map<String, AccountRelations> fetchRawAccountRelationsAsMap(String accountId, List<String> otherAccountIdList)
    {
        Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchAccountRelations(accountId, otherAccountIdList, AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID_LIST);

        if (accountRelationList != null)
        {
            for (AccountRelations relation : accountRelationList)
            {
                accountVsRelationsMap.put(relation.getOtherAccountId(), relation);
            }
        }

        return accountVsRelationsMap;
    }

    public Set<WSAccountRelations> fetchAccountRelations(String accountId)
    {
        Set<WSAccountRelations> accountRelationSet = new HashSet<WSAccountRelations>();
        for (AccountRelations relation : accountRelationDAO.fetchByAccount(accountId))
        {
            accountRelationSet.add(new WSAccountRelations(relation));
        }

        return accountRelationSet;
    }

    private Map<String, AccountRelations> fetchRawInverseAccountRelationsAsMap(String otherAccountId, List<String> accountIdList)
    {
        Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();

        List<AccountRelations> accountRelationList = accountRelationDAO.fetchAccountRelations(otherAccountId, accountIdList, AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_LIST_OTHER_ACCOUNT_ID);

        for (AccountRelations relation : accountRelationList)
        {
            accountVsRelationsMap.put(relation.getAccount().getId(), relation);
        }

        return accountVsRelationsMap;
    }
}