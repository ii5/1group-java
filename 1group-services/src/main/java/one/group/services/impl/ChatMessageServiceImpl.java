package one.group.services.impl;

import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.cache.dao.BroadcastCacheDAO;
import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.EntityType;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.HintType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.dao.AccountDAO;
import one.group.dao.BroadcastDAO;
import one.group.dao.BroadcastGroupRelationDAO;
import one.group.dao.ChatMessageDAO;
import one.group.dao.ChatThreadDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.MessageDAO;
import one.group.dao.SubscriptionDAO;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountUtilityDataHolder;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSChatCursor;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSPhotoReference;
import one.group.entities.api.response.v2.WSSharedItem;
import one.group.entities.jpa.Broadcast;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AuthorizationExceptionCode;
import one.group.exceptions.codes.BroadcastExceptionCode;
import one.group.exceptions.codes.ChatExceptionCode;
import one.group.exceptions.codes.GroupExceptionCode;
import one.group.exceptions.codes.MessageExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.ChatProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.MessageProcessException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ChatMessageService;
import one.group.services.GroupsService;
import one.group.services.PushService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;

import org.apache.solr.common.SolrInputDocument;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ChatMessageServiceImpl implements ChatMessageService
{
    private static final Logger logger = LoggerFactory.getLogger(ChatMessageServiceImpl.class);

    private ChatMessageDAO chatMessageDAO;

    private ChatThreadDAO chatThreadDAO;

    private BroadcastDAO broadcastDAO;

    private SubscriptionDAO subscriptionDAO;

    private AccountDAO accountDAO;

    private PushService pushService;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private AccountRelationService accountRelationService;

    private MessageDAO messageDAO;

    private GroupsDAO groupsDAO;

    private AccountService accountService;

    private BroadcastGroupRelationDAO broadcastGroupRelationDAO;

    private GroupsService groupsService;

    private BroadcastService broadcastService;

    private BroadcastCacheDAO broadcastCacheDAO;

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public ChatMessageDAO getChatMessageDAO()
    {
        return chatMessageDAO;
    }

    public ChatThreadDAO getChatThreadDAO()
    {
        return chatThreadDAO;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public SubscriptionDAO getSubscriptionDAO()
    {
        return subscriptionDAO;
    }

    public void setChatMessageDAO(final ChatMessageDAO chatMessageDAO)
    {
        this.chatMessageDAO = chatMessageDAO;
    }

    public void setChatThreadDAO(final ChatThreadDAO chatThreadDAO)
    {
        this.chatThreadDAO = chatThreadDAO;
    }

    public void setBroadcastDAO(final BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    public void setPushService(final PushService pushService)
    {
        this.pushService = pushService;
    }

    public void setSubscriptionDAO(SubscriptionDAO subscriptionDAO)
    {
        this.subscriptionDAO = subscriptionDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public GroupsDAO getGroupsDAO()
    {
        return groupsDAO;
    }

    public void setGroupsDAO(GroupsDAO groupsDAO)
    {
        this.groupsDAO = groupsDAO;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public BroadcastGroupRelationDAO getBroadcastGroupRelationDAO()
    {
        return broadcastGroupRelationDAO;
    }

    public void setBroadcastGroupRelationDAO(BroadcastGroupRelationDAO broadcastGroupRelationDAO)
    {
        this.broadcastGroupRelationDAO = broadcastGroupRelationDAO;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    public BroadcastService getBroadcastService()
    {
        return broadcastService;
    }

    public void setBroadcastService(BroadcastService broadcastService)
    {
        this.broadcastService = broadcastService;
    }

    public BroadcastCacheDAO getBroadcastCacheDAO()
    {
        return broadcastCacheDAO;
    }

    public void setBroadcastCacheDAO(BroadcastCacheDAO broadcastCacheDAO)
    {
        this.broadcastCacheDAO = broadcastCacheDAO;
    }

    @Profiled(tag = "ChatMessageService-fetchChatMessagesOfChatThread")
    public List<WSChatMessage> fetchChatMessagesOfChatThread(String chatThreadId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<WSChatMessage> wsChatMessagesList = new ArrayList<WSChatMessage>();
        Set<String> chatMessages = chatMessageDAO.fetchChatMessagesOfChatThread(chatThreadId, offset, limit);
        for (String chatMessageKey : chatMessages)
        {
            WSChatMessage wsChatMessage = fetchChatMessageContent(chatThreadId, chatMessageKey);
            wsChatMessagesList.add(wsChatMessage);
        }
        return wsChatMessagesList;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ChatMessageService-saveChatMessage")
    public WSMessageResponse saveChatMessage(final WSMessage wsMessage) throws Abstract1GroupException, JsonMappingException, JsonGenerationException, IOException
    {
        Date createdTime = new Date();
        Broadcast broadcast = null;
        MessageType type = MessageType.parse(wsMessage.getType());
        WSMessageResponse wsMessageResponse = new WSMessageResponse();

        // Generate chat thread id
        String createdById = wsMessage.getCreatedById();
        String toAccountId = wsMessage.getToAccountId();

        List<String> groupMemberIds = new ArrayList<String>();
        groupMemberIds.add(createdById);
        groupMemberIds.add(toAccountId);
        GroupSource groupSource = GroupSource.DIRECT;
        if (createdById.equals(toAccountId))
        {
            groupSource = GroupSource.ME;
        }

        Groups groups = null;
        synchronized (groupsService)
        {
            groups = groupsService.createGroupIfNotExistAndAddMembers(createdById, createdById, toAccountId, groupSource);
        }

        if (groups == null)
        {
            throw new General1GroupException(GroupExceptionCode.GROUP_NOT_FOUND, true, createdById, toAccountId);
        }
        String chatThreadId = groups.getId();

        Message message = new Message();
        String messageId = Utils.generateIdForMessage();
        message.setId(messageId);
        message.setMessageType(type);
        message.setClientSentTime(new Date(Long.valueOf(wsMessage.getClientSentTime())));
        message.setCreatedBy(wsMessage.getCreatedById());
        message.setCreatedTime(createdTime);
        message.setGroupId(chatThreadId);
        message.setUpdatedTime(createdTime);
        message.setUpdatedBy(wsMessage.getCreatedById());
        message.setVersion(0);
        message.setToAccountId(toAccountId);
        message.setMessageSource(MessageSourceType.DIRECT);
        message.setMessageStatus(MessageStatus.UNDEFINED);
        message.setBpoMessageStatus(BpoMessageStatus.UNREAD);
        message.setBpoStatusUpdateBy(createdById);
        message.setBpoStatusUpdateTime(createdTime);
        message.setMessageSentTime(createdTime);

        if (wsMessage.getCreatedById().equals(wsMessage.getToAccountId()))
        {
            message.setMessageSource(MessageSourceType.ME);
            message.setMessageStatus(MessageStatus.PENDING_REVIEW);
        }

        if (type.equals(MessageType.TEXT))
        {
            message.setText(wsMessage.getText());
        }
        else if (type.equals(MessageType.PHOTO))
        {
            message.setText(Utils.getJsonString(wsMessage.getPhoto()));
        }
        else if (type.equals(MessageType.BROADCAST))
        {
            String broadcastId = wsMessage.getBroadcastId();

            if (!Utils.isNullOrEmpty(broadcastId))
            {
                List<String> broadcastIdList = new ArrayList<String>();
                broadcastIdList.add(broadcastId);

                List<Message> messageList = messageDAO.fetchMessageByBroadcastIds(broadcastIdList);
                if (messageList == null || messageList.isEmpty())
                {
                    throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
                }

                broadcast = broadcastDAO.fetchBroadcast(broadcastId);

            }

            if (broadcastId == null)
            {
                throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
            }

            message.setBroadcastId(broadcastId);
            message.setText(wsMessage.getText());
        }
        else if (type.equals(MessageType.PHONE_CALL))
        {
            if (!Utils.isNullOrEmpty(wsMessage.getBroadcastId()))
            {
                String broadcastId = wsMessage.getBroadcastId();

                List<String> broadcastIdList = new ArrayList<String>();
                broadcastIdList.add(broadcastId);

                List<Message> messageList = messageDAO.fetchMessageByBroadcastIds(broadcastIdList);
                if (messageList == null || messageList.isEmpty())
                {
                    throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_NOT_FOUND, false, broadcastId);
                }
                broadcast = broadcastDAO.fetchBroadcast(broadcastId);

                message.setBroadcastId(broadcastId);
            }
        }

        // Fetch to createdBy & toAccount details
        Set<String> accountIdSet = new HashSet<String>();
        accountIdSet.add(toAccountId);
        accountIdSet.add(createdById);
        Map<String, WSAccount> idVsAccountMap = accountService.fetchAccountDetailsInBulk(accountIdSet);
        WSAccount toAccount = idVsAccountMap.get(toAccountId);
        WSAccount createdBy = idVsAccountMap.get(createdById);

        // To fetch last chat message and if it is same as new one then return
        // last chat message so that there will be no duplicate message
        WSMessageResponse wsLastMessage = fetchLastMessageOfGroupAllType(chatThreadId);

        if (wsLastMessage != null && wsLastMessage.getId() != null)
        {
            int lastMessageIndex = wsLastMessage.getIndex();
            String lastMessageCreatedTime = wsLastMessage.getCreatedTime();
            String lastMessageId = wsLastMessage.getId();
            String lastMessageStatus = wsLastMessage.getStatus().toString();

            wsLastMessage.setIndex(null);
            wsLastMessage.setId(null);
            wsLastMessage.setCreatedTime(null);
            wsLastMessage.setStatus(null);

            WSMessageResponse wsMessageResponseTemp = new WSMessageResponse(message);
            wsMessageResponseTemp.setId(null);
            wsMessageResponseTemp.setCreatedTime(null);

            if (wsLastMessage.equals(wsMessageResponseTemp))
            {
                if (message.getMessageType().equals(MessageType.BROADCAST))
                {
                    WSBroadcast wsBroadcast = new WSBroadcast(broadcast);
                    wsLastMessage.setBroadcast(wsBroadcast);
                }

                Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = new HashMap<String, Map<String, WSChatThreadCursor>>();
                List<String> accountIdList = new ArrayList<String>();
                accountIdList.add(toAccountId);
                accountIdList.add(createdById);

                accountVsCursorMap = groupsService.fetchCursorMapForParticipants(accountIdList);
                String groupId = message.getGroupId();
                toAccount.setCursors(accountVsCursorMap.get(groupId).get(toAccountId));
                createdBy.setCursors(accountVsCursorMap.get(groupId).get(createdById));

                wsLastMessage.setCreatedBy(createdBy);
                wsLastMessage.setToAccount(toAccount);
                wsLastMessage.setIndex(lastMessageIndex);
                wsLastMessage.setCreatedTime(lastMessageCreatedTime);
                wsLastMessage.setId(lastMessageId);
                wsLastMessage.setStatus(MessageStatus.parse(lastMessageStatus));
                wsLastMessage.setMessageRepeated(true);

                return wsLastMessage;
            }

        }

        try
        {
            // Update group information
            List<String> participantsStatus = new ArrayList<String>();
            participantsStatus.add(createdById + ":" + GroupStatus.ACTIVE.name());
            participantsStatus.add(toAccountId + ":" + GroupStatus.ACTIVE.name());

            SolrInputDocument sid = new SolrInputDocument();
            sid.addField("id", chatThreadId);
            sid.addField("status_s", Collections.singletonMap("set", GroupStatus.ACTIVE.name()));
            sid.addField("participantsStatus", Collections.singletonMap("set", participantsStatus));
            sid.addField("totalmsgCount", Collections.singletonMap("inc", 1));
            sid.addField("updatedTime", Collections.singletonMap("set", new Date()));

            List<String> participants = new ArrayList<String>();
            participants.add(createdById);
            participants.add(toAccountId);
            sid.addField("participants", Collections.singletonMap("set", participants));

            logger.info("start: updating group meta data[" + chatThreadId + "][" + messageId + "]");
            groupsService.updateGroupDetails(sid);
            logger.info("end: updating group meta data[" + chatThreadId + "][" + messageId + "]");

            // Add message information
            Integer index = 0;
            if (groups.getTotalmsgCount() > 0)
            {
                index = groups.getTotalmsgCount(); // groupsDAO.fetchLatestMessageIndexOfGroup(chatThreadId);
            }
            message.setIndex(index);
            message.setSenderPhoneNumber(createdBy.getPrimaryMobile());

            logger.info("start: saving message[" + messageId + "]");
            messageDAO.saveMessage(message);
            logger.info("done: saving message[" + messageId + "]");

            logger.info("end: updating cursor[" + chatThreadId + "," + createdById + "," + index + "," + index + "," + messageId + "," + messageId + "]");
            int status = groupsService.updateReadAndReceivedIndex(chatThreadId, createdById, index, index, messageId, messageId);
            logger.info("end: updating cursor[" + chatThreadId + "]" + status);
        }
        catch (Exception e)
        {
            logger.error("error while sending message", e);
            throw new General1GroupException(ChatExceptionCode.CHAT_MESSAGE_SENDING_ERROR, true);
        }

        // set Chat Cursor for Direct Message
        // if (message.getMessageSource().equals(MessageSourceType.DIRECT))
        // {
        Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = new HashMap<String, Map<String, WSChatThreadCursor>>();
        List<String> accountIdList = new ArrayList<String>();
        accountIdList.add(toAccountId);
        accountIdList.add(createdById);

        accountVsCursorMap = groupsService.fetchCursorMapForParticipants(accountIdList);
        String groupId = message.getGroupId();
        toAccount.setCursors(accountVsCursorMap.get(groupId).get(toAccountId));
        createdBy.setCursors(accountVsCursorMap.get(groupId).get(createdById));
        // }

        wsMessageResponse = new WSMessageResponse(message);
        wsMessageResponse.setToAccount(toAccount);
        wsMessageResponse.setCreatedBy(createdBy);

        if (type.equals(MessageType.BROADCAST))
        {
            if (!Utils.isNull(wsMessage.getBroadcastId()))
            {
                WSBroadcast wsBroadcast = new WSBroadcast(broadcast);
                wsMessageResponse.setBroadcast(wsBroadcast);
            }
        }

        // subscribe to chat thread for a life time
        if (!Utils.isNull(message.getIndex()) && message.getIndex().equals(0))
        {
            subscriptionDAO.addSubscription(createdById, chatThreadId, EntityType.CHAT);

            // If toAccount is blocked then dont add subscription of chat
            // thread
            boolean isBlocked = accountRelationService.isAccountBlocked(toAccountId, createdById);
            if (!isBlocked)
            {
                subscriptionDAO.addSubscription(toAccountId, chatThreadId, EntityType.CHAT);
            }
        }

        return wsMessageResponse;
    }

    @Profiled(tag = "ChatMessageService-fetchChatMessageContent")
    public WSChatMessage fetchChatMessageContent(String chatThreadId, String chatMessageKey) throws JsonMappingException, JsonGenerationException, IOException
    {
        // String chatMessageKey = chatMessage.getElement();
        String chatMessageContent = chatMessageDAO.fetchChatMessageContent(chatThreadId, chatMessageKey);
        Double chatMessageIndex = chatMessageDAO.fetchChatMessageIndex(chatThreadId, chatMessageKey);

        WSChatMessage wsChatMessage = (WSChatMessage) Utils.getInstanceFromJson(chatMessageContent, WSChatMessage.class);
        wsChatMessage.setIndex(Double.valueOf(chatMessageIndex).intValue());
        return wsChatMessage;

    }

    @Profiled(tag = "ChatMessageService-pushChatMessageToSyncLog")
    public void pushChatMessageToSyncLog(WSMessageResponse wsMessage, String requestHash) throws SyncLogProcessException
    {
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.MESSAGE);
        syncLogEntry.setAssociatedEntityId(wsMessage.getChatThreadId());
        syncLogEntry.setAssociatedEntityType(EntityType.CHAT);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, wsMessage);
        syncLogEntry.setAdditionalData(SyncEntryKey.REQUEST_HASH, requestHash);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Profiled(tag = "ChatMessageService-pushChatCursorToSyncLog")
    public void pushChatCursorToSyncLog(WSChatCursor wsChatCursor, String originClientId) throws SyncLogProcessException
    {
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.CHAT_CURSOR);
        syncLogEntry.setAssociatedEntityId(wsChatCursor.getChatThreadId());
        syncLogEntry.setAssociatedEntityType(EntityType.CHAT);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, wsChatCursor);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    @Profiled(tag = "ChatMessageService-fetchPhonecallMessages")
    public List<Message> fetchPhonecallMessages(String accountId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<Message> messages = messageDAO.fetchPhonecallTypeMessagesOfAccount(accountId);
        return messages;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ChatMessageService-fetchLastMessageOfGroup")
    public WSMessageResponse fetchLastMessageOfGroup(String groupId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<Message> message = messageDAO.fetchLastMessageOfGroupNotTypePhonecall(groupId);
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        if (message != null && message.size() > 0)
        {
            wsMessageResponse = new WSMessageResponse(message.get(0));
        }
        return wsMessageResponse;
    }

    @Profiled(tag = "ChatMessageService-fetchLastBroadcastMessageOfMe")
    public WSMessageResponse fetchLastBroadcastMessageOfMe(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException
    {
        int offset = 0;
        int limit = 1;
        List<Message> messages = messageDAO.fetchMessagesByForMeAndWhatsappByAccountId(accountId, offset, limit);
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        if (messages != null && messages.size() > 0)
        {
            Message message = messages.get(0);
            if (message != null)
            {
                wsMessageResponse = new WSMessageResponse(message);
            }
        }
        return wsMessageResponse;
    }

    @Profiled(tag = "ChatMessageService-fetchLastStarredMessageOfAccount")
    public WSMessageResponse fetchLastStarredMessageOfAccount(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException
    {
        WSBroadcast wsBroadcast = broadcastService.fetchLastStarredBroadcastOfAccount(accountId);
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        if (wsBroadcast != null)
        {
            String messageId = wsBroadcast.getMessageId();
            Message message = messageDAO.fetchMessageFromId(messageId);
            if (message != null)
            {
                wsMessageResponse = new WSMessageResponse(message);
            }
        }
        return wsMessageResponse;
    }

    @Profiled(tag = "ChatMessageService-fetchSharedItemsOfChatThread")
    public WSSharedItem fetchSharedItemsOfChatThread(String fromAccountId, String chatThreadId) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSSharedItem wsSharedItem = new WSSharedItem();
        List<Groups> groupsList = groupsDAO.fetchAllGroupsByAccountIdAndGroupId(fromAccountId, chatThreadId);
        List<String> accountIdList = new ArrayList<String>();
        for (Groups group : groupsList)
        {
            accountIdList.addAll(group.getParticipants());
        }
        WSAccountUtilityDataHolder dataHolder = groupsService.fetchAccountMapData(fromAccountId, accountIdList);
        List<Message> messages = messageDAO.fetchSharedMessagesByType(fromAccountId, chatThreadId);
        List<WSMessageResponse> sharedBroadcasts = new ArrayList<WSMessageResponse>();
        List<WSPhotoReference> sharedMedia = new ArrayList<WSPhotoReference>();
        for (Message message : messages)
        {
            if (message.getMessageType().equals(MessageType.BROADCAST))
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponse = groupsService.formWSMessageResponse(wsMessageResponse, dataHolder);
                sharedBroadcasts.add(wsMessageResponse);
            }
            else if (message.getMessageType().equals(MessageType.PHOTO))
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponse = groupsService.formWSMessageResponse(wsMessageResponse, dataHolder);
                sharedMedia.add(wsMessageResponse.getPhoto());
            }
            wsSharedItem.setAccountId(fromAccountId);
            wsSharedItem.setSharedBroadcasts(sharedBroadcasts);
            wsSharedItem.setSharedMedia(sharedMedia);
        }
        return wsSharedItem;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ChatMessageService-fetchMeMessagesOfAccount")
    public List<Message> fetchMeMessagesOfAccount(String accountId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<Message> messageList = messageDAO.fetchMessagesByForMeAndWhatsappByAccountId(accountId, offset, limit);

        return messageList;
    }

    @Profiled(tag = "ChatMessageService-editChatMessage")
    public WSMessageResponse editChatMessage(final String messageId, final WSMessage wsMessage) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {

        WSMessageResponse wsMessageResponse = new WSMessageResponse();

        // Generate chat thread id
        String createdById = wsMessage.getCreatedById();
        String toAccountId = wsMessage.getToAccountId();

        Message message = messageDAO.fetchMessageFromId(messageId);

        if (message == null)
        {
            throw new MessageProcessException(MessageExceptionCode.MESSAGE_NOT_FOUND, true, messageId);
        }
        if (!message.getMessageSource().equals(MessageSourceType.ME))
        {
            throw new ChatProcessException(AuthorizationExceptionCode.USER_ACCESS_DENIED, false);
        }

        String groupId = message.getGroupId();

        validate("group_id", groupId, NOT_NULL(), NOT_EMPTY());

        if (!groupId.equals(wsMessage.getChatThreadId()))
        {
            throw new ChatProcessException(ChatExceptionCode.CHAT_THREAD_NOT_FOUND, false, groupId);
        }

        Message messageToUpdate = new Message();
        messageToUpdate.setId(messageId);
        MessageType type = MessageType.parse(wsMessage.getType());
        messageToUpdate.setMessageType(type);
        messageToUpdate.setUpdatedTime(new Date());
        messageToUpdate.setUpdatedBy(wsMessage.getCreatedById());
        messageToUpdate.setVersion(message.getVersion() + 1);
        messageToUpdate.setToAccountId(toAccountId);
        messageToUpdate.setMessageSource(MessageSourceType.ME);
        // changed status from Edited To Peding_review On 13 Oct-2016
        messageToUpdate.setMessageStatus(MessageStatus.PENDING_REVIEW);

        messageToUpdate.setBpoMessageStatus(BpoMessageStatus.UNREAD);
        messageToUpdate.setBpoStatusUpdateBy(null);
        messageToUpdate.setBpoStatusUpdateTime(null);

        if (type.equals(MessageType.TEXT))
        {
            messageToUpdate.setText(wsMessage.getText());
        }
        else if (type.equals(MessageType.PHOTO))
        {
            messageToUpdate.setText(Utils.getJsonString(wsMessage.getPhoto()));
        }

        String broadcastId = message.getBroadcastId();
        messageToUpdate.setBroadcastId(null);

        // Fetch to createdBy & toAccount details
        WSAccount toAccount = accountService.fetchAccountDetails(toAccountId);
        WSAccount createdBy = accountService.fetchAccountDetails(createdById);

        // save message
        messageDAO.update(messageToUpdate);

        // create response from message
        message.setText(messageToUpdate.getText());
        message.setMessageType(messageToUpdate.getMessageType());
        message.setUpdatedTime(messageToUpdate.getUpdatedTime());
        message.setUpdatedBy(messageToUpdate.getUpdatedBy());
        message.setVersion(messageToUpdate.getVersion());
        message.setToAccountId(toAccountId);
        message.setMessageSource(messageToUpdate.getMessageSource());
        message.setMessageStatus(messageToUpdate.getMessageStatus());
        message.setBpoMessageStatus(messageToUpdate.getBpoMessageStatus());
        message.setBpoStatusUpdateBy(null);
        message.setBpoStatusUpdateTime(null);

        // remove relation between broadcast
        if (!Utils.isNullOrEmpty(broadcastId))
        {
            try
            {
                broadcastService.removeRelationAndCloseBroadcast(broadcastId, messageId);
            }
            catch (Exception e)
            {
                logger.error("Error while removing relation", e);
            }
        }
        wsMessageResponse = new WSMessageResponse(message);
        wsMessageResponse.setToAccount(toAccount);
        wsMessageResponse.setCreatedBy(createdBy);

        return wsMessageResponse;
    }

    @Profiled(tag = "ChatMessageService-fetchStarredBroadcastOfAccount")
    public List<WSMessageResponse> fetchStarredBroadcastOfAccount(String accountId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<WSBroadcast> wsBroadcastList = broadcastService.fetchAllStarredBroadcastOfAccount(accountId, offset, limit);
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();
        if (wsBroadcastList != null && !wsBroadcastList.isEmpty())
        {
            for (WSBroadcast wsBroadcast : wsBroadcastList)
            {
            	if (wsBroadcast.getMessageId() != null && !wsBroadcast.getMessageId().trim().isEmpty())
            	{
	                Message message = messageDAO.fetchMessageFromId(wsBroadcast.getMessageId());
	                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
	                wsMessageResponseList.add(wsMessageResponse);
            	}
            }
        }
        return wsMessageResponseList;
    }

    @Profiled(tag = "ChatMessageService-fetchMessageOfChatThreadAndType")
    public List<WSMessageResponse> fetchMessageOfChatThreadBySourceAndType(String chatThreadId, MessageSourceType messageSourceType, List<MessageType> messageTypeList, int offset, int limit)
            throws JsonMappingException, JsonGenerationException, IOException
    {
        // Groups group = groupsService.fetchGroupDetailsFromId(chatThreadId);
        List<Message> messages = messageDAO.fetchMessagesByGroupIdAndSourceAndType(chatThreadId, messageSourceType, messageTypeList, offset, limit);
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();
        for (Message message : messages)
        {
            WSMessageResponse wsMessageReponse = new WSMessageResponse(message);
            wsMessageResponseList.add(wsMessageReponse);
        }
        return wsMessageResponseList;
    }

    @Profiled(tag = "ChatMessageService-fetchStarredBroadcastOfAccount")
    public List<String> fetchUniqueMessageByBroadcastIdandAccountId(String accountId, String broadcastId)
    {
        List<String> accountIdList = new ArrayList<String>();
        accountIdList.add(accountId);
        List<String> broadcastIdList = new ArrayList<String>();
        broadcastIdList.add(broadcastId);

        List<String> groupIds = messageDAO.fetchMessageByBroadcastIdsAndSourceAndAccount(accountIdList, broadcastIdList, MessageSourceType.DIRECT, MessageStatus.UNDEFINED);
        return groupIds;
    }

    @Profiled(tag = "ChatMessageService-fetchListOfMessages")
    public List<WSMessageResponse> fetchListOfMessages(String fromAccountId, List<String> messageIds) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<GroupSource> list = new ArrayList<GroupSource>();
        list.add(GroupSource.DIRECT);
        list.add(GroupSource.ME);
        list.add(GroupSource.ONEGROUP);
        list.add(GroupSource.STARRED);
        List<Groups> groupsList = groupsDAO.fetchAllGroupsByAccountId(fromAccountId, list);
        List<String> accountIdList = new ArrayList<String>();

        List<WSMessageResponse> wsMessageResponses = new ArrayList<WSMessageResponse>();
        for (Groups group : groupsList)
        {
            accountIdList.addAll(group.getParticipants());
        }
        WSAccountUtilityDataHolder dataHolder = groupsService.fetchAccountMapData(fromAccountId, accountIdList);
        List<Message> messages = messageDAO.fetchMessagesFromIds(messageIds, null);
        for (Message message : messages)
        {
            WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
            wsMessageResponse = groupsService.formWSMessageResponse(wsMessageResponse, dataHolder);
            wsMessageResponses.add(wsMessageResponse);
        }
        return wsMessageResponses;
    }

    @Profiled(tag = "ChatMessageService-fetchMessageFromBroadcastIds")
    public List<WSMessageResponse> fetchMessageFromBroadcastIds(List<String> broadcastIdList) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<WSMessageResponse> wsMessageResponseList = new ArrayList<WSMessageResponse>();

        if (broadcastIdList != null && !broadcastIdList.isEmpty())
        {
            // Fetch message details by broadcast id
            List<MessageSourceType> messageSourceTypeList = new ArrayList<MessageSourceType>();
            messageSourceTypeList.add(MessageSourceType.ME);
            messageSourceTypeList.add(MessageSourceType.WHATSAPP);

            List<MessageStatus> messageStatusList = new ArrayList<MessageStatus>();
            messageStatusList.add(MessageStatus.ACTIVE);

            MessageType messageType = MessageType.BROADCAST;

            List<Message> messageList = messageDAO.fetchMessageByBroadcastIdsAndStatusAndSource(broadcastIdList, messageStatusList, messageSourceTypeList, messageType);
            for (Message message : messageList)
            {
                WSMessageResponse wsMessageResponse = new WSMessageResponse(message);
                wsMessageResponseList.add(wsMessageResponse);
            }
        }
        return wsMessageResponseList;
    }

    @Profiled(tag = "ChatMessageService-appendToSyncLogAsEntityReference")
    public void appendToSyncLogAsEntityReference(String accountId, String messageId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, messageId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.MESSAGE);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());

    }

    @Profiled(tag = "ChatMessageService-fetchLastBroadcastMessageOfOneGroup")
    public WSMessageResponse fetchLastBroadcastMessageOfOneGroup(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException
    {
        Set<String> broadcastIds = broadcastCacheDAO.fetchBroadcast(accountId, -1, -1);
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        if (broadcastIds != null && !broadcastIds.isEmpty())
        {
            String broadcastId = broadcastIds.iterator().next();
            List<Message> messages = messageDAO.fetchMessagesByBroadcastIdAndMessageStatus(broadcastId, MessageStatus.ACTIVE);
            if (messages != null && messages.size() > 0)
            {
                Message message = messages.get(0);
                if (message != null)
                {
                    wsMessageResponse = new WSMessageResponse(message);
                }
            }
        }
        return wsMessageResponse;
    }

    public long getTotalMessageOfAccountOfMeGroup(String accountId)
    {
        return messageDAO.countAllMessagesByAccountIdForMeAndWhatsApp(accountId);
    }

    public List<Message> fetchMessageDetails(List<String> messageIdList)
    {
        List<Message> messageList = new ArrayList<Message>();
        if (messageIdList != null && !messageIdList.isEmpty())
        {
            messageList.addAll(messageDAO.fetchMessagesFromIds(messageIdList, null));
        }
        return messageList;
    }

    @Profiled(tag = "ChatMessageService-fetchLastMessageOfGroupAllType")
    public WSMessageResponse fetchLastMessageOfGroupAllType(String groupId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<Message> message = messageDAO.fetchLastMessageOfGroup(groupId);
        WSMessageResponse wsMessageResponse = new WSMessageResponse();
        if (message != null && message.size() > 0)
        {
            wsMessageResponse = new WSMessageResponse(message.get(0));
        }
        return wsMessageResponse;
    }

}