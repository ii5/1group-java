package one.group.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.enums.FilterField;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.dao.CityDAO;
import one.group.entities.api.response.WSCity;
import one.group.entities.jpa.City;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.CityService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CityServiceImpl implements CityService
{
    private static final Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);

    private CityDAO cityDAO;

    /**
     * @return the cityDao
     */
    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    /**
     * @param cityDao
     *            the cityDao to set
     */
    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public List<WSCity> getAllCities()
    {
        List<WSCity> cityList = new ArrayList<WSCity>();
        List<City> cities = cityDAO.getAllCities();

        for (City city : cities)
        {
            WSCity cityReference = new WSCity(city);
            cityList.add(cityReference);
        }
        return cityList;
    }

    public List<WSCity> getAllCities(List<Boolean> isPublishedList)
    {
        List<WSCity> cityList = new ArrayList<WSCity>();
        List<City> cities = cityDAO.getAllCities(isPublishedList);

        for (City city : cities)
        {
            WSCity cityReference = new WSCity(city);
            cityList.add(cityReference);
        }
        return cityList;
    }

    @Transactional(rollbackOn = { Exception.class })
    public City getCityById(String cityId) throws General1GroupException
    {
        City city = cityDAO.getCityById(cityId);
        if (city == null)
        {
            throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", true, "City id " + cityId + " not found");
        }
        return city;
    }

    public List<WSCity> getAllCitiesByCityIds(List<String> cityids)
    {
        List<WSCity> WSListCities = new ArrayList<WSCity>();
        List<City> cities = cityDAO.getAllCitiesByCityIds(cityids);

        if (!cities.isEmpty())
        {
            for (City city : cities)
            {
                WSCity wsCity = new WSCity(city);
                WSListCities.add(wsCity);
            }
        }
        return WSListCities;
    }

    public List<WSCity> getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        List<City> cityList = cityDAO.getAllCities(cityIdList, isPublishedList, sort, filter, start, rows);
        List<WSCity> wsCityList = new ArrayList<WSCity>();

        for (City c : cityList)
        {
            WSCity city = new WSCity(c);
            wsCityList.add(city);
        }

        return wsCityList;
    }

    @Profiled(tag = "cityServiceImpl-fetchCityMapById")
    @Transactional(rollbackOn = { Exception.class })
    public Map<String, WSCity> fetchCityMapById(Set<String> cityIdSet)
    {
        Map<String, WSCity> idVsCityMap = new HashMap<String, WSCity>();
        if (cityIdSet == null || cityIdSet.isEmpty())
        {
            return idVsCityMap;
        }

        List<City> cityList = cityDAO.getAllCitiesByCityIds(cityIdSet);
        for (City city : cityList)
        {
            WSCity wsCity = new WSCity(city);
            idVsCityMap.put(city.getId(), wsCity);
        }

        return idVsCityMap;
    }
}
