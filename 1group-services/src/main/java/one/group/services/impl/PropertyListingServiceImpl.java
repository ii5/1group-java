package one.group.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.BookMarkType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.EntityType;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.HintType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.PropertyUpdateActionType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.dao.AmenityDAO;
import one.group.dao.BookMarkDAO;
import one.group.dao.CityDAO;
import one.group.dao.LocationDAO;
import one.group.dao.LocationMatchDAO;
import one.group.dao.PhoneNumberDAO;
import one.group.dao.PropertyListingDAO;
import one.group.dao.PropertySearchRelationDAO;
import one.group.dao.SearchDAO;
import one.group.dao.SubscriptionDAO;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSBookMarkPropertyListings;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSMatchingClients;
import one.group.entities.api.response.WSNewsFeed;
import one.group.entities.api.response.WSNotification;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.api.response.WSPropertyListingsResult;
import one.group.entities.api.response.WSSubscribeEntity;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.BookMark;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.entities.jpa.Search;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.PropertyTypeNotFoundException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.ChatThreadService;
import one.group.services.ClientService;
import one.group.services.PropertyListingService;
import one.group.services.PropertySearchRelationService;
import one.group.services.SubscriptionService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.commons.lang.StringUtils;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author sanilshet
 * 
 */
public class PropertyListingServiceImpl implements PropertyListingService
{
    private static final Logger logger = LoggerFactory.getLogger(PropertyListingServiceImpl.class);

    private PropertyListingDAO propertyDAO;

    private AccountDAO accountDAO;

    private LocationDAO locationDAO;

    private CityDAO cityDAO;

    private AmenityDAO amenityDAO;

    private SubscriptionDAO subscriptionDAO;

    private BookMarkDAO bookMarkDAO;

    private BookMarkServiceImpl bookMarkService;

    private PhoneNumberDAO phoneNumberDAO;

    private ChatThreadService chatThreadService;

    private ClientService clientService;

    private SubscriptionService subscriptionService;

    private AccountRelationService accountRelationService;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private PropertySearchRelationDAO propertySearchRelationDAO;

    private SearchDAO searchDAO;

    private LocationMatchDAO locationMatchDAO;

    private PropertySearchRelationService propertySearchRelationService;

    public PropertySearchRelationService getPropertySearchRelationService()
    {
        return propertySearchRelationService;
    }

    public void setPropertySearchRelationService(PropertySearchRelationService propertySearchRelationService)
    {
        this.propertySearchRelationService = propertySearchRelationService;
    }

    public LocationMatchDAO getLocationMatchDAO()
    {
        return locationMatchDAO;
    }

    public void setLocationMatchDAO(LocationMatchDAO locationMatchDAO)
    {
        this.locationMatchDAO = locationMatchDAO;
    }

    public SearchDAO getSearchDAO()
    {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO)
    {
        this.searchDAO = searchDAO;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public PropertySearchRelationDAO getPropertySearchRelationDAO()
    {
        return propertySearchRelationDAO;
    }

    public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO)
    {
        this.propertySearchRelationDAO = propertySearchRelationDAO;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public PhoneNumberDAO getPhoneNumberDAO()
    {
        return phoneNumberDAO;
    }

    public void setPhoneNumberDAO(PhoneNumberDAO phoneNumberDAO)
    {
        this.phoneNumberDAO = phoneNumberDAO;
    }

    public AmenityDAO getAmenityDAO()
    {
        return amenityDAO;
    }

    public void setAmenityDAO(AmenityDAO amenityDAO)
    {
        this.amenityDAO = amenityDAO;
    }

    public CityDAO getCityDAO()
    {
        return cityDAO;
    }

    public void setCityDAO(CityDAO cityDAO)
    {
        this.cityDAO = cityDAO;
    }

    public LocationDAO getLocationDAO()
    {
        return locationDAO;
    }

    public void setLocationDAO(LocationDAO locationDAO)
    {
        this.locationDAO = locationDAO;
    }

    public BookMarkServiceImpl getBookMarkService()
    {
        return bookMarkService;
    }

    public void setBookMarkService(BookMarkServiceImpl bookMarkService)
    {
        this.bookMarkService = bookMarkService;
    }

    public SubscriptionDAO getSubscriptionDAO()
    {
        return subscriptionDAO;
    }

    public void setSubscriptionDAO(SubscriptionDAO subscriptionDAO)
    {
        this.subscriptionDAO = subscriptionDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public PropertyListingDAO getPropertyDAO()
    {
        return propertyDAO;
    }

    public void setPropertyDAO(PropertyListingDAO propertyDAO)
    {
        this.propertyDAO = propertyDAO;
    }

    public BookMarkDAO getBookMarkDAO()
    {
        return bookMarkDAO;
    }

    public void setBookMarkDAO(BookMarkDAO bookMarkDAO)
    {
        this.bookMarkDAO = bookMarkDAO;
    }

    public ChatThreadService getChatThreadService()
    {
        return chatThreadService;
    }

    public void setChatThreadService(ChatThreadService chatThreadService)
    {
        this.chatThreadService = chatThreadService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    /**
     * This method fetches Property Listings for a account, this method returns
     * property listing for own accounts as well as other accounts property
     * listings.if requesting account is owner then active and expired property
     * listings are returned, else only active properties are returned
     * 
     * @param accountId
     *            - requesting accounts account id
     * @param accountIdFromToken
     *            - account id drawn from access token
     * 
     * @return {@link WSBookMarkPropertyListings}
     * @throws General1GroupException
     * @see {@link WSPropertyListing}
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getProperyListingOfAccount")
    public List<WSPropertyListing> getProperyListingOfAccount(String accountId, String accountIdFromToken, String accountDetails, String locationDetails, String matchingDetails, String subscribe)
            throws General1GroupException
    {
        // check if account exists in database
        Account account = accountDAO.fetchAccountById(accountId);
        if (account == null)
        {
            throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", false, "Account not found for " + accountId);
        }
        List<WSPropertyListing> wsPropertyListings = new ArrayList<WSPropertyListing>();
        try
        {
            List<BroadcastStatus> fetchListingOfStatusLists = new ArrayList<BroadcastStatus>();
            // if requesting account is accessing his own property then show him
            // property listing with status active and expired
            fetchListingOfStatusLists.add(BroadcastStatus.ACTIVE);

            if (accountId.equals(accountIdFromToken))
            {
                fetchListingOfStatusLists.add(BroadcastStatus.EXPIRED);
            }
            List<PropertyListing> propertyListings = propertyDAO.fetchPropertyListingbyAccountId(accountId, fetchListingOfStatusLists);

            if (propertyListings == null || propertyListings.isEmpty())
            {
                return wsPropertyListings;
            }

            WSAccount wsAccount = new WSAccount(account);

            // Prepare meta data required to fetch information related to
            // property listing
            List<String> bookmarkedPropertyListingIds = new ArrayList<String>();
            Map<String, WSAccount> propertyListingIdVsAccountMap = new HashMap<String, WSAccount>();
            for (PropertyListing propertyListing : propertyListings)
            {
                String propertyListingId = propertyListing.getId();

                bookmarkedPropertyListingIds.add(propertyListingId);
                propertyListingIdVsAccountMap.put(propertyListingId, wsAccount);
            }

            // setting up propertyListingsVsBookmarkMap
            Map<String, BookMark> propertyListingIdVsBookMarkMap = new HashMap<String, BookMark>();
            if (bookmarkedPropertyListingIds != null && bookmarkedPropertyListingIds.size() > 0)
            {
                List<BookMark> isBookMarked = bookMarkDAO.checkIsBookMarkedForPropertyListings(accountIdFromToken, bookmarkedPropertyListingIds);
                for (BookMark bookMark : isBookMarked)
                {
                    String propertyListingId = bookMark.getTargetEntity();
                    propertyListingIdVsBookMarkMap.put(propertyListingId, bookMark);
                }
            }

            // setting up accountVsRelationMap
            List<String> accountRelationAccountIdsList = new ArrayList<String>();
            accountRelationAccountIdsList.add(accountId);
            Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();
            accountVsRelationsMap = accountRelationService.fetchRawAccountRelationsAsMap(accountIdFromToken, accountRelationAccountIdsList);

            Map<String, WSPropertyListing> wsPropertyListingMap = getWSPropertyListingByPropertyListingIds(propertyListings, accountIdFromToken, subscribe, accountDetails, locationDetails,
                    matchingDetails, propertyListingIdVsBookMarkMap, accountVsRelationsMap, propertyListingIdVsAccountMap);

            if (wsPropertyListingMap != null)
            {
                for (WSPropertyListing wsPropertyListing : wsPropertyListingMap.values())
                {
                    wsPropertyListings.add(wsPropertyListing);
                }
            }

            return wsPropertyListings;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return wsPropertyListings;
    }

    @Profiled(tag = "PropertyListingService-getPropertyListingIdsOfAccount")
    public List<String> getPropertyListingIdsOfAccount(String accountId, String accountIdFromToken) throws General1GroupException
    {
        List<String> ids = new ArrayList<String>();
        Account account = accountDAO.fetchAccountById(accountId);
        if (account == null)
        {
            throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", false, "Account not found for " + accountId);
        }
        try
        {
            List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
            // if requesting account is accessing his own property then show him
            // property listing with status active and expired
            statusList.add(BroadcastStatus.ACTIVE);

            if (accountId.equals(accountIdFromToken))
            {
                statusList.add(BroadcastStatus.EXPIRED);
            }

            List<PropertyListing> propertyListings = propertyDAO.fetchPropertyListingbyAccountId(accountId, statusList);
            for (PropertyListing propertyListing : propertyListings)
            {
                ids.add(propertyListing.getId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }

        return ids;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getProperyListingOfAccount")
    public Map<String, Integer> getProperyListingOfAccount(String accountId) throws General1GroupException
    {
        // check if account exists in database
        Account account = accountDAO.fetchAccountById(accountId);
        Map<String, Integer> propertyListingIds = new HashMap<String, Integer>();
        if (account == null)
        {
            throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", false, "Account not found for " + accountId);
        }
        try
        {
            List<PropertyListing> propertyListings = propertyDAO.fetchPropertyListingbyAccountId(accountId);
            for (PropertyListing propertyListing : propertyListings)
            {
                long timeInMilliSeconds = propertyListing.getCreatedTime().getTime();
                int timeInSeconds = (int) (timeInMilliSeconds / 1000);
                propertyListingIds.put(propertyListing.getId(), timeInSeconds);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return propertyListingIds;
    }

    /**
     * This method fetches Property Listings count for a account, this method
     * returns property listing for own accounts as well as other accounts
     * property listings.if requesting account is owner then active and expired
     * property listings are returned, else only active properties are returned
     * 
     * @param accountId
     *            - requesting accounts account id
     * @param accountIdFromToken
     *            - account id drawn from access token
     * 
     * @return {@link Integer} - total count
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getPropertyListingCountOfAccount")
    public int getPropertyListingCountOfAccount(String accountId, String accountIdFromToken, boolean checkStatus)
    {
        int propertyListingCount = 0;
        try
        {
            List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
            // if requesting account is accessing his own property then show him
            // property listing with status active and expired
            statusList.add(BroadcastStatus.ACTIVE);

            if (accountId.equals(accountIdFromToken))
            {
                statusList.add(BroadcastStatus.EXPIRED);
            }
            // this is for get all property count for a account for invite code
            // check
            if (checkStatus)
            {
                statusList.add(BroadcastStatus.EXPIRED);
                statusList.add(BroadcastStatus.CLOSED);
            }
            long propertyListingsCount = propertyDAO.fetchPropertyListingCountByAccountId(accountId, statusList);
            return (int) propertyListingsCount;
        }
        catch (Exception e)
        {
            logger.info("Error while fetching property listing count");
        }
        return propertyListingCount;
    }

    /**
     * Function to get property listing by short reference
     * 
     * @param shortReference
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getPropertyListingByShortRef")
    public WSPropertyListing getPropertyListingByShortRef(String shortReference)
    {
        WSPropertyListing wsPropertyListing = new WSPropertyListing();
        try
        {
            PropertyListing propertyListing = propertyDAO.fetchPropertyListingByShortReference(shortReference);

            return formWSPropertyListing(propertyListing, null, "", "", "", "");
        }
        catch (Exception e)
        {
            logger.info("Error while fetching property details");
        }

        return wsPropertyListing;
    }

    /**
     * This function gets property listing by property listing id, Also other
     * account ( requesting account) can subscribe to this property listing to
     * get updates for that property listing
     * 
     * @param propertyListingId
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getPropertyListingByPropertyListingId")
    public WSPropertyListing getPropertyListingByPropertyListingId(String propertyListingId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException
    {
        Validation.notNull(propertyListingId, "Property listing id passed should not be null.");
        // Subscribe and append to sync log
        if (subscribe != null)
        {
            this.subscribeAndAppendToSyncLog(propertyListingId, accountId, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
        }

        PropertyListing propertyListing = propertyDAO.fetchPropertyListing(propertyListingId);
        if (propertyListing == null)
        {
            return null;
        }

        // WSPropertyListing wsPropertyListing = new
        // WSPropertyListing(propertyListing);
        WSPropertyListing wsPropertyListing = formWSPropertyListing(propertyListing, accountId, accountDetails, locationDetails, matchingDetails, subscribe);

        // updated on 11 feb 2016

        // set book marked property listing true or false
        if (wsPropertyListing != null && !accountId.equals(propertyListing.getAccount().getId()))
        {
            List<String> pIds = new ArrayList<String>();
            pIds.add(propertyListingId);
            List<BookMark> isBookMarked = bookMarkDAO.checkIsBookMarkedForPropertyListings(accountId, pIds);
            if (isBookMarked != null)
            {
                for (BookMark bookMark : isBookMarked)
                {
                    if (bookMark.getTargetEntity().equals(wsPropertyListing.getId()))
                    {
                        wsPropertyListing.setIsBookmarked(true);
                    }
                }
            }
        }

        List<PropertySearchRelation> allMatchingClients = null;

        if (!propertyListingId.isEmpty())
        {
            List<PropertyListing> propertyListingList = new ArrayList<PropertyListing>();
            propertyListingList.add(propertyListing);

            allMatchingClients = propertySearchRelationDAO.getAllMatchingClientsByIds(propertyListingList, accountId);
        }
        if (allMatchingClients != null)
        {
            Set<WSMatchingClients> matchingClients = new java.util.HashSet<WSMatchingClients>();

            for (PropertySearchRelation psr : allMatchingClients)
            {
                WSMatchingClients wsMatchingDetails = new WSMatchingClients(psr);
                matchingClients.add(wsMatchingDetails);
            }
            wsPropertyListing.setMatchingClients(matchingClients);
        }
        return wsPropertyListing;
    }

    /**
     * This method is used to 1- save new property listing that expects required
     * parameters for creating one. 2 - edit existing property listing and
     * creates a new property listing with new id but same short reference 3 -
     * close property listing, short reference is null in this case 4 - mark as
     * hot/un hot property listing, no new property listing is created , only
     * state change 5 - Renew property listing if requesting property listing is
     * expired
     * 
     * 
     * @param description
     *            - description for a property ( optional)
     * @param type
     *            - property type(residential)
     * @param subType
     *            - property sub type(apartment,row house, villa) - Optional
     * @param area
     *            - area of property (optional)
     * @param commisionType
     *            - commission type for a property ( direct or via)
     * @param rooms
     *            - rooms ( 1bhk, 2bhk, 3hbhk, 4bhk, 5pbhk)
     * @param localityId
     *            - location id related to a property listing - required
     * @param amenities
     *            - Array of amenities associated with a property listing -
     *            optional
     * @param salePrice
     * @param rentPrice
     * @param status
     * @param propertyMarket
     * @param isHot
     * @param action
     * @param accountId
     * @param propertyListingId
     * @param createdFor
     * @param externalListingTime
     * @param externalSource
     * @param customSublocation
     * @return
     * @throws Abstract1GroupException
     * @throws ParseException
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-savePropertyListing")
    public WSPropertyListing savePropertyListing(boolean isAdmin, String description, String type, String subType, String area, String commisionType, String rooms, String localityId,
            List<String> amenities, String salePrice, String rentPrice, String status, String propertyMarket, String isHot, PropertyUpdateActionType action, String accountId,
            String propertyListingId, String createdFor, String externalListingTime, String externalSource, String customSublocation) throws Abstract1GroupException, ParseException

    {
        PropertyListing newPropertyListing = null;
        PropertyListing propertyListing = null;
        String oldShortReference = null;
        String shortReference = null;
        Account account = null;
        SyncActionType syncActionType = SyncActionType.ADD;
        boolean isAdminOrOwner = false;
        // collect all required fields that are needed to create new Listing
        // always , else if status and is hot fields are changed we don't need
        // to create new property listing
        List<Object> requiredFields = new ArrayList<Object>();
        requiredFields.add(type);
        requiredFields.add(description);
        requiredFields.add(subType);
        requiredFields.add(area);
        requiredFields.add(commisionType);
        requiredFields.add(salePrice);
        requiredFields.add(rentPrice);
        requiredFields.add(rooms);
        requiredFields.add(amenities);
        requiredFields.add(localityId);
        // check if any required fields have changed value
        List<Object> changedRequiredFields = new ArrayList<Object>();
        for (Object item : requiredFields)
        {
            if (item != null)
            {
                changedRequiredFields.add(item);
            }
        }
        // check for id and fetch property listing object
        if (propertyListingId != null)
        {
            propertyListing = propertyDAO.fetchPropertyListing(propertyListingId);
            // only owner and admin can edit property listing
            isAdminOrOwner = isAdmin || accountId.equals(propertyListing.getAccount().getIdAsString()) ? true : false;
            if (!isAdminOrOwner)
            {
                throw new General1GroupException(HTTPResponseType.FORBIDDEN, "403", false, "You can edit only your own property");
            }
            if (propertyListing == null)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "404", false, "Property for id " + propertyListingId + " not found");
            }

            oldShortReference = propertyListing.getShortReference();
        }
        // here we check if any of required fields are edited/added then we
        // always create a new property listing id
        if (changedRequiredFields.size() > 1)
        {
            if (propertyListingId != null && propertyListing.getStatus() != BroadcastStatus.ACTIVE)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Only active property listings can be edited");
            }
            newPropertyListing = new PropertyListing();
            newPropertyListing.setStatus(BroadcastStatus.ACTIVE);
            propertyListing = propertyListingId != null ? propertyListing : newPropertyListing;
        }
        else
        {
            newPropertyListing = propertyListing;
        }
        // set new property listings short reference equal to old property
        // listing, else for new property listing request create new one
        if (propertyListingId != null)
        {
            newPropertyListing.setShortReference(oldShortReference);
        }
        else
        {
            do
            {
                shortReference = Utils.randomString(Constant.SHORT_REFERENCE_CHARSET_UPPERCASE_ALPHANUMERIC, 6, false);
                PropertyListing tPropertyListing = propertyDAO.fetchByShortReference(shortReference);
                if (tPropertyListing != null)
                {
                    shortReference = null;
                }
            }
            while (shortReference == null);
            newPropertyListing.setShortReference(shortReference);
        }

        // set sync action type to null if no parameters is added in request
        // during edit
        if (changedRequiredFields.size() <= 1)
        {
            syncActionType = null;
        }

        // for administration client adding property listings
        if (externalListingTime != null && externalSource != null)
        {
            newPropertyListing.setExternalSource(externalSource);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Long time = Long.valueOf(externalListingTime);

            Date getExtTime = new Date(time);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            String currentDate = simpleDateFormat.format(new Date());
            Date current = sdf.parse(currentDate);
            long diff = current.getTime() - getExtTime.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            long timeToBeClosed = Constant.TIME_TO_BE_EXPIRE_IN_DAYS;
            if (getExtTime.getTime() > current.getTime())
            {
                getExtTime = new Date();
            }
            if (diffDays > timeToBeClosed)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Property listing has been closed");
            }

            newPropertyListing.setCreatedTime(getExtTime);
        }
        else if (propertyListingId != null && propertyListing.getExternalSource() != null)
        {
            newPropertyListing.setExternalSource(propertyListing.getExternalSource());
        }

        PropertyMarket propertyMarketValue = propertyMarket != null ? PropertyMarket.convert(propertyMarket) : null;
        newPropertyListing.setPropertyMarket(this.<PropertyMarket> returnArg1OnNull(propertyListing.getPropertyMarket(), propertyMarketValue));
        // set description
        newPropertyListing.setDescription(this.<String> returnArg1OnNull(propertyListing.getDescription(), description));
        // API 76
        if (description != null)
        {
            newPropertyListing.setDescriptionLength(description.length());
        }
        else
        {
            newPropertyListing.setDescriptionLength(propertyListing.getDescriptionLength());
        }
        // if (Utils.isEmpty(description))
        // {
        // newPropertyListing.setDescription(null);
        // }
        // set commission type
        if (commisionType != null)
        {
            CommissionType commisionTypeValue = commisionType != null ? CommissionType.convert(commisionType) : null;
            newPropertyListing.setCommissionType(this.<CommissionType> returnArg1OnNull(propertyListing.getCommissionType(), commisionTypeValue));
        }
        else if (propertyListingId != null)
        {
            newPropertyListing.setCommissionType(propertyListing.getCommissionType());
        }
        // set rooms
        if (rooms != null)
        {
            newPropertyListing.setRooms(this.<String> returnArg1OnNull(propertyListing.getRooms(), rooms));
        }
        else if (propertyListingId != null)
        {
            newPropertyListing.setRooms(propertyListing.getRooms());
        }

        long salePriceValue = salePrice != null ? new Long(salePrice) : propertyListing.getSalePrice();
        long rentPriceValue = rentPrice != null ? new Long(rentPrice) : propertyListing.getRentPrice();
        newPropertyListing.setSalePrice(this.<Long> returnArg1OnNull(propertyListing.getSalePrice(), salePriceValue));
        newPropertyListing.setRentPrice(this.<Long> returnArg1OnNull(propertyListing.getRentPrice(), rentPriceValue));
        // set area
        if (area != null)
        {
            newPropertyListing.setArea(this.<Integer> returnArg1OnNull(propertyListing.getArea(), Integer.parseInt(area)));
        }
        else if (propertyListingId != null)
        {
            newPropertyListing.setArea(propertyListing.getArea());
        }

        PropertyType propertyTypeValue = type != null ? PropertyType.convert(type) : null;
        newPropertyListing.setType(this.<PropertyType> returnArg1OnNull(propertyListing.getType(), propertyTypeValue));

        if (subType != null)
        {
            PropertySubType propertySubTypeValue = subType != null ? PropertySubType.convert(subType.toString()) : null;
            newPropertyListing.setSubType(this.<PropertySubType> returnArg1OnNull(propertyListing.getSubType(), propertySubTypeValue));
        }
        else if (propertyListingId != null)
        {
            newPropertyListing.setSubType(propertyListing.getSubType());
        }
        // set account, check if admin is requesting api , if then dont set
        // account
        if (!isAdmin)
        {
            account = accountDAO.fetchAccountById(accountId);
            newPropertyListing.setAccount(account);
        }

        if (!Utils.isEmpty(createdFor))
        {
            account = accountDAO.fetchAccountById(createdFor);
            if (account == null)
            {
                throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", false, "Account not found for " + createdFor);
            }
            newPropertyListing.setAccount(account);
        }
        // set locality
        Location locality = localityId != null ? locationDAO.fetchById(localityId) : propertyListing.getLocation();

        if (locality == null)
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "No location found for id " + localityId);
        }
        newPropertyListing.setLocation(this.<Location> returnArg1OnNull(propertyListing.getLocation(), locality));
        // set amenities assigned to property listing
        newPropertyListing.setAmenities(this.addAmenities(amenities, propertyListing));
        // check if status is not null , this indicates request is send for
        // closing a property listing
        if (status != null)
        {
            syncActionType = SyncActionType.MANUAL_CLOSE;
            closePropertyListing(propertyListing, status);
        }
        // set is hot default to false
        Boolean isHotValue = isHot != null ? Boolean.valueOf(isHot) : false;

        if (isHot != null && propertyListingId != null)
        {
            markOtherAsUnHot(accountId, propertyListingId);
        }

        if ((propertyListingId != null && isHot != null))
        {
            syncActionType = SyncActionType.EDIT_INPLACE;
        }
        if (isHot != null)
        {
            newPropertyListing.setShortReference(oldShortReference != null ? oldShortReference : shortReference);
        }
        newPropertyListing.setIsHot(isHotValue);
        // set for custom sub location
        newPropertyListing.setCustomSubLocation(this.<String> returnArg1OnNull(propertyListing.getCustomSubLocation(), customSublocation));

        // renew property if in expired state
        if (action != null)
        {
            try
            {
                newPropertyListing.setShortReference(oldShortReference);
                syncActionType = SyncActionType.RENEW;
                renewPropertyListing(propertyListing, action);
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }
        }
        // close old property listing
        if (changedRequiredFields.size() > 1 && propertyListingId != null)
        {
            syncActionType = SyncActionType.EDIT_CLOSE;
            propertyListing.setStatus(BroadcastStatus.CLOSED);
            propertyListing.setShortReference(null);
            propertyDAO.savePropertyListing(propertyListing);
            newPropertyListing.setCreatedTime(propertyListing.getCreatedTime());
            newPropertyListing.setLastRenewedTime(new Date());
            // Create replica of chat thread of old property listing for new
            // property listing
            chatThreadService.migrateChatThreadsOfBroadcast(propertyListing.getIdAsString(), newPropertyListing.getIdAsString());
            // decrement count of Active PropertyListing
            // decrementPropertyListingCount();
        }

        propertyDAO.savePropertyListing(newPropertyListing);

        // if New PropertyLisitng Added Increment Active propertyCount
        if (newPropertyListing.getStatus().equals(BroadcastStatus.ACTIVE) && propertyListingId == null)
        {
            // increment count of Active PropertyListing
            incrementPropertyListingCount();
        }

        WSPropertyListing wsPropertyListing = new WSPropertyListing(newPropertyListing);
        wsPropertyListing.setOldPropertyListingId(propertyListing.getIdAsString());
        wsPropertyListing.setSyncActionType(syncActionType);
        wsPropertyListing.setChangedRequiredFields(false);

        if (newPropertyListing.getLastRenewedTime() == null)
        {
            wsPropertyListing.setExpiryTime("" + ((newPropertyListing.getCreatedTime().getTime()) + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS));
        }
        else
        {
            wsPropertyListing.setExpiryTime(""
                    + (Math.max((newPropertyListing.getLastRenewedTime().getTime()), (newPropertyListing.getCreatedTime().getTime())) + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS));
        }

        if (changedRequiredFields.size() > 1)
        {
            wsPropertyListing.setChangedRequiredFields(true);
        }

        return wsPropertyListing;

    }

    @Profiled(tag = "PropertyListingService-appendToSyncLog")
    public WSPropertyListing appendToSyncLog(WSPropertyListing wsPropertyListing) throws Exception
    {
        // subscribe and append to sync log when new property listing created or
        // edited, since we making copy of old property listing need to
        // subscribe to newly created
        if (wsPropertyListing.getChangedRequiredFields())
        {

            if (wsPropertyListing.getSyncActionType() != null && wsPropertyListing.getSyncActionType().equals(SyncActionType.EDIT_CLOSE))
            {
                this.appendToSyncLogAsEntityReference(wsPropertyListing.getId(), wsPropertyListing.getOldPropertyListingId(), wsPropertyListing.getSyncActionType());
            }
            else
            {
                this.appendToSyncLogAsEntityReference(wsPropertyListing.getId(), wsPropertyListing.getSyncActionType());
            }

            this.subscribeAndAppendToSyncLog(wsPropertyListing.getId(), wsPropertyListing.getCreatedBy(), Constant.SUBSCRIBE_TIME * 60);
        }
        // always append to sync log as hint type fetch in case of any edit
        else if (wsPropertyListing.getId() != null && wsPropertyListing.getSyncActionType() != null)
        {
            this.appendToSyncLogAsEntityReference(wsPropertyListing.getId(), wsPropertyListing.getSyncActionType());
        }

        wsPropertyListing.setOldPropertyListingId(null);
        wsPropertyListing.setSyncActionType(null);
        wsPropertyListing.setChangedRequiredFields(null);
        wsPropertyListing.setLastRenewedTime(null);

        return wsPropertyListing;
    }

    /**
     * Function to book mark property listing
     * 
     * @throws Exception
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-bookMarkProperty")
    public void bookMarkProperty(String accountId, String propertyListingId, String action) throws Exception
    {
        PropertyListing property = propertyDAO.fetchPropertyListing(propertyListingId);
        if (property == null)
        {
            throw new PropertyTypeNotFoundException(HTTPResponseType.NOT_FOUND, "404", false, "No property found for id " + propertyListingId);
        }

        if (accountId.equals(property.getAccount().getId()))
        {
            throw new PropertyTypeNotFoundException(HTTPResponseType.NOT_FOUND, "404", false, "You cannot bookmark your own property listing " + propertyListingId);
        }
        bookMarkDAO.bookmark(accountId, propertyListingId, BookMarkType.PROPERTY_LISTING, action);
        if (action.equals(String.valueOf("true")))
        {
            subscribeAndAppendToSyncLog(propertyListingId, accountId, Constant.ONE_MONTH_IN_MINS * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
        }
        else
        {
            subscribeAndAppendToSyncLog(propertyListingId, accountId, 1 * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
        }
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING, propertyListingId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(SyncActionType.EDIT_INPLACE);
        syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
        syncLogEntry.setAssociatedEntityId(propertyListingId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    /**
     * Function to get all book marks related to property listing
     * 
     * @param authToken
     * 
     * @return {@link List WSBookMarkPropertyListings}
     * 
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getBookMarkedPropertyListings")
    public Set<WSBookMarkPropertyListings> getBookMarkedPropertyListings(String accountId, String accountDetails, String locationDetails, String matchingDetails) throws AuthenticationException,
            ClientProcessException, AccountNotFoundException
    {

        Set<WSBookMarkPropertyListings> list = new LinkedHashSet<WSBookMarkPropertyListings>();
        try
        {
            List<BookMark> bookMarks = bookMarkService.getAllBookMarksByReference(accountId, BookMarkType.PROPERTY_LISTING);
            for (int i = 0; i < bookMarks.size(); i++)
            {
                WSBookMarkPropertyListings wsBookMarkPropertyListing = new WSBookMarkPropertyListings();
                PropertyListing propertyListing = propertyDAO.fetchPropertyListing(bookMarks.get(i).getTargetEntity().toString());

                WSPropertyListing wsPropertyListing = formWSPropertyListing(propertyListing, accountId, accountDetails, locationDetails, matchingDetails, null);
                wsBookMarkPropertyListing.setPropertyListingDetails(wsPropertyListing);
                wsBookMarkPropertyListing.setPropertyListingId(wsPropertyListing.getId());
                list.add(wsBookMarkPropertyListing);
            }
            return list;
        }
        catch (Exception e)
        {
        }
        return list;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getActivePropertyListings")
    public List<PropertyListing> getActivePropertyListings()
    {
        List<PropertyListing> propertyListings = null;
        try
        {
            propertyListings = propertyDAO.getActivePropertyListings();
            return propertyListings;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return propertyListings;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getActiveAndExpiredPropertyListings")
    public List<PropertyListing> getActiveAndExpiredPropertyListings()
    {
        List<PropertyListing> propertyListings = null;
        try
        {
            propertyListings = propertyDAO.getActiveAndExpiredPropertyListings();

            return propertyListings;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return propertyListings;
    }

    /**
     * This function is used to save property search relation after new property
     * listing is added Used by Add property listing sync handler
     * 
     * @param propertyListing
     *            - Id of property listing
     * @param localityId
     *            - location id associated with that property listing
     * @throws SyncLogProcessException
     * @throws General1GroupException
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-savePropertySearchRelation")
    public List<PropertySearchRelation> savePropertySearchRelation(PropertyListing propertyListing, List<Location> locationMatches) throws SyncLogProcessException, General1GroupException
    {
        List<PropertySearchRelation> propertySearchRelationList = new ArrayList<PropertySearchRelation>();
        // save to matching client table
        List<String> amenity = new ArrayList<String>();
        String getPropertyAmenities = propertyListing.getAmenities();
        if (!Utils.isNull(getPropertyAmenities))
        {
            List<String> amenityList = Arrays.asList(getPropertyAmenities.split(","));
            for (String singleAmenity : amenityList)
            {
                amenity.add(singleAmenity);
            }
        }

        int areaSearch = String.valueOf(propertyListing.getArea()) != null ? propertyListing.getArea() : 0;
        int salePrice = (int) propertyListing.getSalePrice();
        int rentPriceSearch = (int) propertyListing.getRentPrice();
        String propertySubType = propertyListing.getSubType() != null ? propertyListing.getSubType().toString() : null;
        String rooms = propertyListing.getRooms() != null ? propertyListing.getRooms().toString() : null;
        List<Search> matchingIDs = searchDAO.getMatchingSearchIds(propertyListing.getAccount().getId(), locationMatches, "", propertyListing.getPropertyMarket().toString(), rooms, amenity,
                areaSearch, rentPriceSearch, salePrice, propertyListing.getType().toString(), propertySubType);

        for (Search ids : matchingIDs)
        {
            PropertySearchRelation propertySearchRelation = new PropertySearchRelation();
            try
            {
                // save search relations
                Account account = new Account();
                propertySearchRelation.setPropertyListing(propertyListing);
                propertySearchRelation.setAccount(account);
                propertySearchRelation.setSearchId(ids);
                propertySearchRelationService.savePropertySearchRelation(propertySearchRelation);

                // send push notification for matching clients
                String title = Constant.GCM_TITLE;
                String body = null; // String.format(Constant.NOTIFICATION_NEW_MATCH_FOUND,
                                    // ids.getRequirementName());
                WSNotification notification = new WSNotification(title, body);
                notification.setId(ids.getId());

                SyncLogEntry syncLogEntry = new SyncLogEntry();
                syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, notification);
                syncLogEntry.setType(SyncDataType.NOTIFICATION);
                List<String> targetAccountIds = new ArrayList<String>();
                targetAccountIds.add(account.getId());
                syncLogEntry.setAdditionalData(SyncEntryKey.TARGET_ACCOUNT_ID, targetAccountIds);
                syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
                syncLogEntry.setAction(SyncActionType.UNREAD);
                kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());

                // add to list
                propertySearchRelationList.add(propertySearchRelation);
            }
            catch (Exception e)
            {
                logger.error("Error while persisting property search relation property-id" + propertyListing.getId() + ". search-id " + ids + "." + e.getMessage());
            }
        }

        return propertySearchRelationList;
    }

    /**
     * Call back function called by schedulers to auto expire and close property
     * listing
     * 
     * @param propertyListingId
     *            String - Id of property listing that needs to change its
     *            status
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-autoExpirePropertyListing")
    public PropertyListing autoExpirePropertyListing(String propertyListingId) throws ParseException, Abstract1GroupException
    {
        PropertyListing propertyListing = propertyDAO.fetchPropertyListing(propertyListingId);
        SyncActionType syncActionType = null;
        Status accountStatus = propertyListing.getAccount().getStatus();
        Date getCreatedTime = propertyListing.getLastRenewedTime() != null ? propertyListing.getLastRenewedTime() : propertyListing.getCreatedTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String currentDate = sdf.format(new Date());
        Date current = sdf.parse(currentDate);
        long diff = current.getTime() - getCreatedTime.getTime();

        long diffDays = diff / (24 * 60 * 60 * 1000);
        long timeTobeEXpired = Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS;
        long timeToBeClosed = Constant.TIME_TO_BE_CLOSED_IN_MILLISECONDS;
        Boolean updated = false;
        if (diff >= timeTobeEXpired && propertyListing.getStatus().equals(BroadcastStatus.ACTIVE))
        {
            // If account status is not registered and instead is
            // then close it, else expire
            if (accountStatus == Status.SEEDED)
            {
                propertyListing.setShortReference(null);
                syncActionType = SyncActionType.AUTO_CLOSE;
                propertyListing.setStatus(BroadcastStatus.CLOSED);
                // decrement count of Active PropertyListing
                decrementPropertyListingCount();
            }
            else
            {
                syncActionType = SyncActionType.AUTO_EXPIRE;
                propertyListing.setStatus(BroadcastStatus.EXPIRED);
            }
            updated = true;
        }
        // always (active or expired)property listing should be closed, if time
        // has been passed beyond close time
        if (diff >= timeToBeClosed && propertyListing.getStatus().equals(BroadcastStatus.EXPIRED))
        {
            propertyListing.setShortReference(null);
            syncActionType = SyncActionType.AUTO_CLOSE;
            propertyListing.setStatus(BroadcastStatus.CLOSED);
            updated = true;
            // decrement count of Active PropertyListing
            decrementPropertyListingCount();
        }

        if (updated)
        {
            propertyDAO.savePropertyListing(propertyListing);
            this.appendToSyncLogAsEntityReference(propertyListing.getId(), syncActionType);
            return propertyListing;
        }
        else
        {
            return null;
        }
    }

    /**
     * This function is used to get list of active and expired property listings
     * that are required for schedulers to auto expire/close property listings
     * 
     * @return {@link List}
     */
    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getActiveAndExpiredPropertyListing")
    public List<PropertyListing> getActiveAndExpiredPropertyListing()
    {
        List<PropertyListing> propertyListings = null;
        try
        {
            propertyListings = propertyDAO.getActiveAndExpiredPropertyListings();

            return propertyListings;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return propertyListings;
    }

    @Profiled(tag = "PropertyListingService-fetchAllPropertyListingsByAccountIds")
    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIds(List<String> accountIdList, List<Location> locationList, BroadcastStatus status)
    {
        return propertyDAO.fetchAllPropertyListingsByAccountIds(accountIdList, locationList, status);
    }

    @Profiled(tag = "PropertyListingService-searchPropertyListing")
    public List<PropertyListingScoreObject> searchPropertyListing(List<Location> locationList, BroadcastStatus status)
    {
        long start = Utils.getSystemTime();
        logger.info("searchPropertyListing start: " + start);
        List<PropertyListingScoreObject> list = propertyDAO.searchPropertyListing(locationList, status);
        logger.info("searchPropertyListing end: " + (Utils.getSystemTime() - start));
        return list;
    }

    @Profiled(tag = "PropertyListingService-searchPropertyListingByLocationIds")
    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status, int maxResults)
    {
        return propertyDAO.searchPropertyListingByLocationIds(locationIdList, status, maxResults);
    }

    @Profiled(tag = "PropertyListingService-markOtherAsUnHot")
    private void markOtherAsUnHot(String accountId, String propertyListingId)
    {
        propertyDAO.markOtherPropertyListingsAsUnHot(accountId, propertyListingId);
    }

    /**
     * Private function to set Web Service Property listing object
     * 
     * @param propertyListing
     * 
     * @return {@link WSPropertyListing}
     */
    @Profiled(tag = "PropertyListingService-formWSPropertyListing")
    @Transactional(rollbackOn = Exception.class)
    private WSPropertyListing formWSPropertyListing(PropertyListing propertyListing, String accountIdFromAccessToken, String accountDetails, String locationDetails, String matchingDetails,
            String subscribe)
    {
        return this.setPropertyDetails(propertyListing, accountIdFromAccessToken, accountDetails, locationDetails, matchingDetails, subscribe);
    }

    public WSPropertyListing setPropertyDetails(PropertyListing propertyListing, String accountIdFromAccessToken, String accountDetails, String locationDetails, String matchingDetails,
            String subscribe)
    {
        WSPropertyListing wsPropertyListing = new WSPropertyListing(propertyListing);
        wsPropertyListing.setCity(propertyListing.getLocation().getCity().getName());
        String asscoiatedAmenities = propertyListing.getAmenities();
        List<String> amenitiesArray = new ArrayList<String>();
        if (!Utils.isNull(asscoiatedAmenities))
        {
            List<String> splitAmenity = Arrays.asList(asscoiatedAmenities.split(","));

            for (String amenity : splitAmenity)
            {
                amenitiesArray.add(amenity);
            }
        }

        wsPropertyListing.setAmenities(amenitiesArray);

        if (accountIdFromAccessToken != null && !accountIdFromAccessToken.equals(propertyListing.getAccount().getId()))
        {
            wsPropertyListing.setIsBookmarked(false);

            BookMark isBookMarked = bookMarkDAO.getBookMarkByReferenceAndTargetEntity(accountIdFromAccessToken, propertyListing.getIdAsString());
            if (isBookMarked != null)
            {
                wsPropertyListing.setIsBookmarked(true);
            }
        }

        /*
         * Set<WSMatchingClients> matchingClients = new
         * java.util.HashSet<WSMatchingClients>(); // if
         * (propertyListing.getAccount().getId() != null) {
         * List<PropertySearchRelation> matchingClient =
         * propertySearchRelationDAO.getMatchingClients(propertyListing.getId(),
         * accountIdFromAccessToken);
         * 
         * if (matchingClient != null) { for (PropertySearchRelation mclients :
         * matchingClient) { WSMatchingClients wsMatchingDetails = new
         * WSMatchingClients(mclients); matchingClients.add(wsMatchingDetails);
         * } } }
         */
        // wsPropertyListing.setMatchingClients(matchingClients);

        if (!Utils.isNull(accountDetails) && accountDetails.equals("true"))
        {
            Account account = accountDAO.fetchAccountById(propertyListing.getAccount().getId());
            WSAccount wsAccount = new WSAccount(account);
            if (!propertyListing.getAccount().getId().equals(accountIdFromAccessToken))
            {
                wsAccount.setStatus(null);
                // wsAccount.setLocalities(null);
                WSAccountRelations accountRelation = accountRelationService.fetchAccountRelation(accountIdFromAccessToken, propertyListing.getAccount().getId());
                if (accountRelation != null)
                {
                    wsAccount.setScore(accountRelation.getScore());
                    wsAccount.setIsBlocked(accountRelation.getIsBlocked());
                    wsAccount.setIsContact(accountRelation.getIsContact());
                }
                else
                {
                    // If relation is not set then set default values and sends
                    // all keys
                    wsAccount.setScore(Constant.DEFAULT_ACCOUNT_RELATION_SCORE);
                    wsAccount.setIsBlocked(false);
                    wsAccount.setIsContact(false);
                }

                if (!account.getStatus().equals(Status.ACTIVE))
                {
                    wsAccount.setCanChat(false);
                }
                if (subscribe != null)
                {
                    subscriptionService.subscribe(accountIdFromAccessToken, propertyListing.getAccount().getId(), EntityType.ACCOUNT, Integer.parseInt(subscribe)
                            * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
                }
            }
            wsPropertyListing.setAccountDetails(wsAccount);
        }

        if (!Utils.isNull(locationDetails) && locationDetails.equals("true"))
        {
            wsPropertyListing.setLocationDetails(propertyListing.getLocation().getLocationDisplayName());
        }
        return wsPropertyListing;
    }

    /**
     * This function is used to log property listing to kafka as sync log Also
     * an requesting account is subscribed to his own property listing
     * 
     * @param propertyListing
     */
    private void subscribeAndAppendToSyncLog(String propertyListingId, String accountId, int subcribeTime) throws SyncLogProcessException
    {
        subscriptionDAO.addSubscription(accountId, propertyListingId, EntityType.PROPERTY_LISTING, subcribeTime);
        WSSubscribeEntity subcribeEntity = new WSSubscribeEntity(Constant.SUBSCRIBE_TIME, propertyListingId, accountId);
        subcribeEntity.setType(EntityType.PROPERTY_LISTING);
        // create sync entity object of type subscribe entity
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.SUBSCRIBE);
        syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
        syncLogEntry.setAssociatedEntityId(propertyListingId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, subcribeEntity);
        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    /**
     * This function is used to append sync log entry (Kafka) after any changes
     * are made to property listing
     * 
     * @param propertyListingId
     * @param syncActionType
     * @throws SyncLogProcessException
     */
    private void appendToSyncLogAsEntityReference(String propertyListingId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING, propertyListingId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
        syncLogEntry.setAssociatedEntityId(propertyListingId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    /**
     * This function is used to append sync log entry (Kafka) after any changes
     * are made to property listing, also sets old property listing id to sync
     * data
     * 
     * @param propertyListingId
     * @param oldPropertListingId
     * @param syncActionType
     * 
     * @throws SyncLogProcessException
     */
    private void appendToSyncLogAsEntityReference(String propertyListingId, String oldPropertListingId, SyncActionType syncActionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING, propertyListingId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(syncActionType);
        syncLogEntry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
        syncLogEntry.setAssociatedEntityId(propertyListingId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);
        syncLogEntry.setAdditionalData(SyncEntryKey.CLOSED_PROPERTY_LISTING_ID, oldPropertListingId);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    // helpers
    private <T> T returnArg1OnNull(T arg1, T arg2)
    {
        return arg2 == null ? arg1 : arg2;
    }

    /**
     * Function to set amenities while adding/editing new property listing
     * 
     * @param amenities
     *            -List of requested amenities
     * @param propertyListing
     *            - Property listing object
     * 
     * @return {@link Set}
     * @throws General1GroupException
     * @see {@link Set}
     */
    private String addAmenities(List<String> amenities, PropertyListing propertyListing) throws General1GroupException
    {
        String setAmenity = null;
        // this is to avoid duplicate amenities
        Set<String> hs = new java.util.HashSet<String>();
        hs.addAll(amenities);
        List<String> newAmenity = new ArrayList<String>();
        newAmenity.addAll(hs);
        if (newAmenity.size() > 0)
        {
            for (int i = 0; i < newAmenity.size(); i++)
            {
                Amenity singleAmenity = amenityDAO.findAmenityByName(newAmenity.get(i).toString());
                if (singleAmenity == null)
                {
                    throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "No amenity found for " + amenities.get(i).toString());
                }
            }
            setAmenity = StringUtils.join(newAmenity, ",");
        }
        else
        {
            setAmenity = propertyListing.getAmenities();
        }
        return setAmenity;
    }

    /**
     * This function is used to renew property listing, checks status of
     * requesting property listing is active and time has passed beyond one
     * month from date of creation
     * 
     * @param propertyListing
     * @param action
     * 
     * @throws ParseException
     * @throws General1GroupException
     */
    private void renewPropertyListing(PropertyListing propertyListing, PropertyUpdateActionType action) throws ParseException, General1GroupException
    {
        // check if current property listing status is expired, then only renew
        // property
        Date propertyLastRenewedTime = propertyListing.getLastRenewedTime() != null ? propertyListing.getLastRenewedTime() : propertyListing.getCreatedTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String currentDate = sdf.format(new Date());
        Date current = sdf.parse(currentDate);
        long diff = current.getTime() - propertyLastRenewedTime.getTime();
        long timeToExpire = Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS;
        long timeToClose = Constant.TIME_TO_BE_CLOSED_IN_MILLISECONDS;
        if ((propertyListing.getStatus().equals(BroadcastStatus.EXPIRED) || propertyListing.getStatus().equals(BroadcastStatus.ACTIVE) || diff >= timeToExpire)
                && action.equals(PropertyUpdateActionType.RENEW))
        {
            // check logical status of property listing
            if (diff < timeToClose)
            {
                propertyListing.setLastRenewedTime(new Date());
                propertyListing.setStatus(BroadcastStatus.ACTIVE);
            }
            else
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Property has been already closed");
            }
        }
        else
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Only expired or active property listing can be renewed");
        }
    }

    /**
     * This function is used to close property listing , also checks logical
     * status of property listing and status equals to active
     * 
     * @param status
     * @param propertyListing
     * @throws General1GroupException
     * 
     */
    @Profiled(tag = "PropertyListingService-closePropertyListing")
    public void closePropertyListing(PropertyListing propertyListing, String status) throws General1GroupException
    {
        // check if current property listing status is active, then only close
        // property
        if ((propertyListing.getStatus().equals(BroadcastStatus.ACTIVE) || propertyListing.getStatus().equals(BroadcastStatus.EXPIRED)) && BroadcastStatus.parse(status).equals(BroadcastStatus.CLOSED))
        {
            propertyListing.setStatus(BroadcastStatus.CLOSED);
            propertyListing.setShortReference(null);
            // decrement count of Active PropertyListing
            decrementPropertyListingCount();
        }
        else
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Property has been already closed");
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "PropertyListingService-closePropertyListing")
    public void closePropertyListing(PropertyListing propertyListing) throws Abstract1GroupException
    {
        propertyListing.setStatus(BroadcastStatus.CLOSED);
        propertyListing.setShortReference(null);
        propertyDAO.savePropertyListing(propertyListing);
        decrementPropertyListingCount();
    }

    @Profiled(tag = "PropertyListingService-getActivePropertyListingScoreObjects")
    public List<PropertyListingScoreObject> getActivePropertyListingScoreObjects()
    {
        return propertyDAO.fetchAllPropertyListingsByStatus(BroadcastStatus.ACTIVE);
    }

    public void incrementPropertyListingCount() throws General1GroupException
    {
        // Commenting to avoid issues.
        // try
        // {
        // String propertyListingCount =
        // propertyDAO.getPropertyListingCountFromCache(EntityType.PROPERTY_LISTING);
        // // if keys Not Exist in cache
        // if (propertyListingCount == null)
        // {
        // // Fetch From db
        // Long activepropertyLisitingCount =
        // propertyDAO.fetchActivePropertyListingCount();
        // propertyDAO.updatePropertyListingCount(String.valueOf(activepropertyLisitingCount),
        // EntityType.PROPERTY_LISTING);
        // }
        // else
        // {
        // Long updateCount = Long.valueOf(propertyListingCount) + 1;
        // propertyDAO.updatePropertyListingCount(updateCount.toString(),
        // EntityType.PROPERTY_LISTING);
        // }
        //
        // }
        // catch (Exception e)
        // {
        // throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
        // true, "Exception while increment property listing count.", new
        // IllegalStateException());
        // }

    }

    public void decrementPropertyListingCount() throws General1GroupException
    {
        // Commenting to avoid issues.
        // try
        // {
        // Long propertyListingCount =
        // Long.valueOf(propertyDAO.getPropertyListingCountFromCache(EntityType.PROPERTY_LISTING));
        //
        // if (propertyListingCount > 0)
        // {
        // Long updateCount = propertyListingCount - 1;
        // propertyDAO.updatePropertyListingCount(updateCount.toString(),
        // EntityType.PROPERTY_LISTING);
        // }
        // }
        // catch (Exception e)
        // {
        // throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
        // true, "Exception while increment property listing count.", new
        // IllegalStateException());
        // }

    }

    @Profiled(tag = "PropertyListingService-getActivepropertyListingCount")
    public WSEntityCount getActivepropertyListingCount() throws General1GroupException
    {
        WSEntityCount wsEntityCount = new WSEntityCount();
        try
        {
            // String propertyListingCount =
            // propertyDAO.getPropertyListingCountFromCache(EntityType.PROPERTY_LISTING);
            //
            // if (propertyListingCount == null)
            // {
            Long activepropertyLisitingCount = propertyDAO.fetchActivePropertyListingCount();
            propertyDAO.updatePropertyListingCount(String.valueOf(activepropertyLisitingCount), EntityType.PROPERTY_LISTING);
            String propertyListingCount = String.valueOf(activepropertyLisitingCount);
            // }

            wsEntityCount.setCount(propertyListingCount);
            wsEntityCount.setNamespace(EntityType.PROPERTY_LISTING);
        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception while fetching property listing count.", new IllegalStateException());
        }
        return wsEntityCount;
    }

    @Profiled(tag = "PropertyListingService-searchPropertyListingByExactAndNearBy")
    public List<PropertyListingScoreObject> searchPropertyListingByExactAndNearBy(String locationId, BroadcastStatus status)
    {

        return propertyDAO.searchPropertyListingByExactAndNearBy(locationId, status);
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getClosedAndExpiredPropertyListings")
    public List<PropertyListing> getClosedAndExpiredPropertyListing()
    {
        List<PropertyListing> propertyListings = null;
        try
        {
            List<BroadcastStatus> propertyStatus = new ArrayList<BroadcastStatus>();
            propertyStatus.add(BroadcastStatus.CLOSED);
            propertyStatus.add(BroadcastStatus.EXPIRED);
            propertyListings = propertyDAO.fetchPropertyListingsByStatus(propertyStatus);
            return propertyListings;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.info("Error while fetching property listing of an acount");
        }
        return propertyListings;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-fetchPropertyListingIdsByStatus")
    public List<String> fetchPropertyListingIdsByStatus(List<BroadcastStatus> propertyStatus)
    {
        List<String> pIds = propertyDAO.fetchPropertyListingIdsByStatus(propertyStatus);
        return (pIds == null) ? new ArrayList<String>() : pIds;
    }

    @Profiled(tag = "PropertyListingService-fetchActivePropertyListingCountOfAccount")
    public int fetchActivePropertyListingCountOfAccount(String accountId)
    {
        List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
        statusList.add(BroadcastStatus.ACTIVE);

        long propertyListingsCount = propertyDAO.fetchPropertyListingCountByAccountId(accountId, statusList);

        return (int) propertyListingsCount;
    }

    @Profiled(tag = "PropertyListingService-getPropertyListingCountByStatus")
    public WSEntityCount getPropertyListingCountByStatus(BroadcastStatus status) throws General1GroupException
    {
        WSEntityCount wsEntityCount = new WSEntityCount();
        try
        {
            long count = propertyDAO.getPropertyListingCountByStatus(status);
            wsEntityCount.setCount(String.valueOf(count));
            wsEntityCount.setNamespace(EntityType.PROPERTY_LISTING);

        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception while fetching property listing count.", new IllegalStateException());
        }
        return wsEntityCount;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-fetchPropertyListingsByIdsAndStatus")
    public List<PropertyListing> fetchPropertyListingsByIdsAndStatus(List<String> propertyListingIds, List<BroadcastStatus> propertyStatus)
    {
        List<PropertyListing> propertyListingList = propertyDAO.fetchPropertyListingsByIdsAndStatus(propertyListingIds, propertyStatus);
        return (propertyListingList == null) ? new ArrayList<PropertyListing>() : propertyListingList;
    }

    @Profiled(tag = "PropertyListingService-getAllpropertyListingCount")
    public WSEntityCount getAllpropertyListingCount() throws General1GroupException
    {
        WSEntityCount wsEntityCount = new WSEntityCount();
        try
        {
            Long allPropertyLisitingCount = propertyDAO.fetchAllPropertyListingCount();
            propertyDAO.updatePropertyListingCount(String.valueOf(allPropertyLisitingCount), EntityType.PROPERTY_LISTING);
            String propertyListingCount = String.valueOf(allPropertyLisitingCount);

            wsEntityCount.setCount(propertyListingCount);
            wsEntityCount.setNamespace(EntityType.PROPERTY_LISTING);
        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception while fetching property listing count.", new IllegalStateException());
        }
        return wsEntityCount;
    }

    @Profiled(tag = "PropertyListingService-fetchAllActivePropertyListingsByAccountIdsAndLocationIds")
    public List<PropertyListingScoreObject> fetchAllActivePropertyListingsByAccountIdsAndLocationIds(List<String> accountIdsList, List<String> locationIdsList)
    {
        BroadcastStatus status = BroadcastStatus.ACTIVE;
        List<PropertyListingScoreObject> propertyListings = new ArrayList<PropertyListingScoreObject>();
        if (locationIdsList != null && !locationIdsList.isEmpty() && accountIdsList != null && !accountIdsList.isEmpty())
        {
            propertyListings = propertyDAO.fetchAllPropertyListingsByAccountIdsAndLocationIds(accountIdsList, locationIdsList, status);
        }
        return propertyListings;
    }

    @Profiled(tag = "PropertyListingService-fetchAllActivePropertyListingByLocationIds")
    public List<PropertyListingScoreObject> fetchAllActivePropertyListingByLocationIds(List<String> locationIdsList)
    {
        List<PropertyListingScoreObject> propertyListings = new ArrayList<PropertyListingScoreObject>();
        if (locationIdsList != null && !locationIdsList.isEmpty())
        {
            BroadcastStatus status = BroadcastStatus.ACTIVE;
            propertyListings = propertyDAO.searchPropertyListingByLocationIds(locationIdsList, status);
        }
        return propertyListings;
    }

    @Profiled(tag = "PropertyListingService-fetchPropertyListingsByPropertyListingIds")
    public List<PropertyListing> fetchPropertyListingsByPropertyListingIds(List<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");

        List<PropertyListing> propertyListingList = propertyDAO.fetchPropertyListingsByPropertyListingds(propertyListingIds);
        return (propertyListingList == null) ? new ArrayList<PropertyListing>() : propertyListingList;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getWSNewsFeedByPropertyListingIds")
    public List<WSNewsFeed> getWSNewsFeedByPropertyListingIds(List<PropertyListing> listpropertyListing, String accountId, String subscribe, String accountDetails, String locationDetails,
            String matchingDetails, Map<String, BookMark> propertyListingIdVsBookMarkMap, Map<String, AccountRelations> accountVsRelationsMap,
            HashMap<String, WSNewsFeed> propertyListingIdVsWSNewsFeedMap) throws Abstract1GroupException
    {

        Validation.isTrue(!listpropertyListing.isEmpty(), "propertyListingIds should not be null");

        List<PropertySearchRelation> allPropertySearchRelations = null;
        allPropertySearchRelations = propertySearchRelationDAO.getAllMatchingClientsByIds(listpropertyListing, accountId);

        Set<WSMatchingClients> matchingClients = null;
        HashMap<String, Set<WSMatchingClients>> propertyListingIdVsMatchClientMap = new HashMap<String, Set<WSMatchingClients>>();

        if (allPropertySearchRelations != null)
        {

            for (PropertySearchRelation psr : allPropertySearchRelations)
            {
                WSMatchingClients wsMatchingDetails = new WSMatchingClients(psr);

                String propertyListingId = psr.getPropertyListing().getId();

                if (propertyListingIdVsMatchClientMap.containsKey(propertyListingId))
                {
                    matchingClients = propertyListingIdVsMatchClientMap.get(propertyListingId);
                    matchingClients.add(wsMatchingDetails);
                }
                else
                {
                    matchingClients = new java.util.HashSet<WSMatchingClients>();
                    matchingClients.add(wsMatchingDetails);
                }
                propertyListingIdVsMatchClientMap.put(propertyListingId, matchingClients);
            }

        }

        List<WSNewsFeed> ListWSNewsFeed = new ArrayList<WSNewsFeed>();

        for (PropertyListing propertyListing : listpropertyListing)
        {
            String propertyListingId = propertyListing.getId();

            // Subscribe and append to sync log
            if (subscribe != null)
            {
                this.subscribeAndAppendToSyncLog(propertyListingId, accountId, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
            }

            WSPropertyListing wsPropertyListing = new WSPropertyListing(propertyListing);

            // Matching Clients
            Set<WSMatchingClients> setMatchingClients = new java.util.HashSet<WSMatchingClients>();

            if (!propertyListingIdVsMatchClientMap.isEmpty())
            {
                setMatchingClients = (propertyListingIdVsMatchClientMap.get(propertyListingId) != null ? propertyListingIdVsMatchClientMap.get(propertyListingId) : setMatchingClients);
                wsPropertyListing.setMatchingClients(setMatchingClients);
            }
            else
            {
                wsPropertyListing.setMatchingClients(setMatchingClients);
            }

            // city
            wsPropertyListing.setCity(propertyListing.getLocation().getCity().getName());

            // aminity
            String asscoiatedAmenities = propertyListing.getAmenities();
            List<String> amenitiesArray = new ArrayList<String>();
            if (!Utils.isNull(asscoiatedAmenities))
            {
                List<String> splitAmenity = Arrays.asList(asscoiatedAmenities.split(","));

                for (String amenity : splitAmenity)
                {
                    amenitiesArray.add(amenity);
                }
            }
            wsPropertyListing.setAmenities(amenitiesArray);

            if (!Utils.isNull(accountDetails) && accountDetails.equals("true"))
            {
                WSAccount wsAccount = new WSAccount(propertyListing.getAccount());
                String propertyListingPosterId = propertyListing.getAccount().getId();
                if (!propertyListingPosterId.equals(accountId))
                {

                    if (!wsAccount.getStatus().equals(Status.ACTIVE))
                    {
                        wsAccount.setCanChat(false);
                    }
                    wsAccount.setStatus(null);
                    // wsAccount.setLocalities(null);

                    // setting Account relation
                    AccountRelations accountRelation = accountVsRelationsMap.get(propertyListingPosterId);
                    if (accountRelation != null)
                    {
                        WSAccountRelations wsAccountRelation = new WSAccountRelations(accountRelation);

                        wsAccount.setScore(wsAccountRelation.getScore());
                        wsAccount.setIsBlocked(wsAccountRelation.getIsBlocked());
                        wsAccount.setIsContact(wsAccountRelation.getIsContact());
                    }
                    else
                    {
                        wsAccount.setScore(Constant.DEFAULT_ACCOUNT_RELATION_SCORE);
                        wsAccount.setIsBlocked(false);
                        wsAccount.setIsContact(false);
                    }

                    if (subscribe != null)
                    {
                        subscriptionService.subscribe(accountId, propertyListingPosterId, EntityType.ACCOUNT, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
                    }
                }
                wsPropertyListing.setAccountDetails(wsAccount);
            }

            if (!Utils.isNull(locationDetails) && locationDetails.equals("true"))
            {
                wsPropertyListing.setLocationDetails(propertyListing.getLocation().getLocationDisplayName());
            }

            // setting BookMark
            BookMark bookMark = propertyListingIdVsBookMarkMap.get(propertyListingId);
            if (bookMark != null)
            {
                wsPropertyListing.setIsBookmarked(true);
            }

            WSNewsFeed wsNewsFeed = propertyListingIdVsWSNewsFeedMap.get(wsPropertyListing.getId());
            wsNewsFeed.setPropertyListingDetails(wsPropertyListing);

            ListWSNewsFeed.add(wsNewsFeed);
        }
        return ListWSNewsFeed;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getWSPropertyListingByPropertyListingIds")
    public Map<String, WSPropertyListing> getWSPropertyListingByPropertyListingIds(List<PropertyListing> listpropertyListing, String accountId, String subscribe, String accountDetails,
            String locationDetails, String matchingDetails, Map<String, BookMark> propertyListingIdVsBookMarkMap, Map<String, AccountRelations> accountVsRelationsMap,
            Map<String, WSAccount> propertyListingIdVsAccountMap) throws Abstract1GroupException
    {

        Validation.isTrue(!listpropertyListing.isEmpty(), "propertyListingIds should not be null");

        List<PropertySearchRelation> allPropertySearchRelations = propertySearchRelationDAO.getAllMatchingClientsByIds(listpropertyListing, accountId);

        Set<WSMatchingClients> matchingClients = null;
        HashMap<String, Set<WSMatchingClients>> propertyListingIdVsMatchClientMap = new HashMap<String, Set<WSMatchingClients>>();

        if (allPropertySearchRelations != null)
        {
            for (PropertySearchRelation psr : allPropertySearchRelations)
            {
                WSMatchingClients wsMatchingDetails = new WSMatchingClients(psr);

                String propertyListingId = psr.getPropertyListing().getId();

                if (propertyListingIdVsMatchClientMap.containsKey(propertyListingId))
                {
                    matchingClients = propertyListingIdVsMatchClientMap.get(propertyListingId);
                    matchingClients.add(wsMatchingDetails);
                }
                else
                {
                    matchingClients = new java.util.HashSet<WSMatchingClients>();
                    matchingClients.add(wsMatchingDetails);
                }
                propertyListingIdVsMatchClientMap.put(propertyListingId, matchingClients);
            }

        }

        Map<String, WSPropertyListing> propertyListingIdVsDetailsMap = new HashMap<String, WSPropertyListing>();

        for (PropertyListing propertyListing : listpropertyListing)
        {
            String propertyListingId = propertyListing.getId();

            // Subscribe and append to sync log
            if (subscribe != null)
            {
                this.subscribeAndAppendToSyncLog(propertyListingId, accountId, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
            }

            WSPropertyListing wsPropertyListing = new WSPropertyListing(propertyListing);

            if (wsPropertyListing.getExternalSource() != null && !wsPropertyListing.getExternalSource().isEmpty())
            {
                wsPropertyListing.setExternalSource("Whatsapp");
            }

            // Matching Clients
            Set<WSMatchingClients> setMatchingClients = new java.util.HashSet<WSMatchingClients>();

            if (!propertyListingIdVsMatchClientMap.isEmpty())
            {
                setMatchingClients = (propertyListingIdVsMatchClientMap.get(propertyListingId) != null ? propertyListingIdVsMatchClientMap.get(propertyListingId) : setMatchingClients);
                wsPropertyListing.setMatchingClients(setMatchingClients);
            }
            else
            {
                wsPropertyListing.setMatchingClients(setMatchingClients);
            }

            // city
            wsPropertyListing.setCity(propertyListing.getLocation().getCity().getName());

            // aminity
            String asscoiatedAmenities = propertyListing.getAmenities();
            List<String> amenitiesArray = new ArrayList<String>();
            if (!Utils.isNull(asscoiatedAmenities))
            {
                List<String> splitAmenity = Arrays.asList(asscoiatedAmenities.split(","));

                for (String amenity : splitAmenity)
                {
                    amenitiesArray.add(amenity);
                }
            }
            wsPropertyListing.setAmenities(amenitiesArray);

            if (!Utils.isNull(accountDetails) && accountDetails.equals("true"))
            {
                WSAccount wsAccount = new WSAccount(propertyListing.getAccount());
                String propertyListingPosterId = propertyListing.getAccount().getId();
                if (!propertyListingPosterId.equals(accountId))
                {

                    if (!wsAccount.getStatus().equals(Status.ACTIVE))
                    {
                        wsAccount.setCanChat(false);
                    }
                    wsAccount.setStatus(null);
                    // wsAccount.setLocalities(null);

                    // setting Account relation
                    AccountRelations accountRelation = accountVsRelationsMap.get(propertyListingPosterId);
                    if (accountRelation != null)
                    {
                        WSAccountRelations wsAccountRelation = new WSAccountRelations(accountRelation);

                        wsAccount.setScore(wsAccountRelation.getScore());
                        wsAccount.setIsBlocked(wsAccountRelation.getIsBlocked());
                        wsAccount.setIsContact(wsAccountRelation.getIsContact());
                    }
                    else
                    {
                        wsAccount.setScore(Constant.DEFAULT_ACCOUNT_RELATION_SCORE);
                        wsAccount.setIsBlocked(false);
                        wsAccount.setIsContact(false);
                    }

                    if (subscribe != null)
                    {
                        subscriptionService.subscribe(accountId, propertyListingPosterId, EntityType.ACCOUNT, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
                    }
                }
                wsPropertyListing.setAccountDetails(wsAccount);
            }

            if (!Utils.isNull(locationDetails) && locationDetails.equals("true"))
            {
                wsPropertyListing.setLocationDetails(propertyListing.getLocation().getLocationDisplayName());
            }

            // setting BookMark
            BookMark bookMark = propertyListingIdVsBookMarkMap.get(propertyListingId);
            if (bookMark != null)
            {
                wsPropertyListing.setIsBookmarked(true);
            }

            propertyListingIdVsDetailsMap.put(propertyListingId, wsPropertyListing);
        }
        return propertyListingIdVsDetailsMap;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-getProperyListingDetailsByPropertyListingIds")
    public WSPropertyListingsResult getProperyListingDetailsByPropertyListingIds(List<String> listpropertyListingIds, String accountId, String subscribe, String accountDetails,
            String locationDetails, String matchingDetails) throws Abstract1GroupException
    {
        Validation.isTrue(!listpropertyListingIds.isEmpty(), "propertyListingIds should not be null");

        List<PropertyListing> listPropertyListing = fetchPropertyListingsByPropertyListingIds(listpropertyListingIds);

        if (listPropertyListing.isEmpty())
        {
            throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", true, "Propertylistings Not Found.");
        }
        // setting Bookmark
        Map<String, BookMark> propertyListingIdVsBookMarkMap = new HashMap<String, BookMark>();

        List<BookMark> isBookMarked = bookMarkDAO.checkIsBookMarkedForPropertyListings(accountId, listpropertyListingIds);

        if (!isBookMarked.isEmpty())
        {
            for (BookMark bookMark : isBookMarked)
            {
                String propertyListingId = bookMark.getTargetEntity();
                propertyListingIdVsBookMarkMap.put(propertyListingId, bookMark);

            }
        }
        // setting accountRelation
        Map<String, AccountRelations> accountVsRelationsMap = new HashMap<String, AccountRelations>();
        Map<String, WSAccount> propertyListingIdVsAccountMap = new HashMap<String, WSAccount>();
        if (!Utils.isNull(accountDetails) && accountDetails.equals("true"))
        {
            propertyListingIdVsAccountMap = fetchAccountsAssocitedWithPropertyListingByIds(listpropertyListingIds);
            List<String> otherAccountIds = new ArrayList<String>();
            if (propertyListingIdVsAccountMap != null)
            {
                for (WSAccount wsAccount : propertyListingIdVsAccountMap.values())
                {
                    otherAccountIds.add(wsAccount.getId());
                }
            }
            accountVsRelationsMap = accountRelationService.fetchRawAccountRelationsAsMap(accountId, otherAccountIds);
        }

        Map<String, WSPropertyListing> wsPropertyListingMap = getWSPropertyListingByPropertyListingIds(listPropertyListing, accountId, subscribe, accountDetails, locationDetails, matchingDetails,
                propertyListingIdVsBookMarkMap, accountVsRelationsMap, propertyListingIdVsAccountMap);

        WSPropertyListingsResult result = new WSPropertyListingsResult();
        if (!wsPropertyListingMap.isEmpty())
        {
            Set<WSPropertyListing> propertyListingsResult = new HashSet<WSPropertyListing>(wsPropertyListingMap.values());
            result.setPropertyListingsResult(propertyListingsResult);
        }
        return result;
    }

    @Transactional(rollbackOn = Exception.class)
    @Profiled(tag = "PropertyListingService-fetchPropertyListingIdsByPropertyListingIdsAndStatus")
    public List<String> fetchPropertyListingIdsByPropertyListingIdsAndStatus(List<String> propertyListingIds, BroadcastStatus status) throws General1GroupException
    {

        List<String> propertyListingList = propertyDAO.fetchPropertyListingIdsByPropertyListingIdsAndStatus(propertyListingIds, status);
        return (propertyListingList == null) ? new ArrayList<String>() : propertyListingList;
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status, int maxResults, String accountId)
    {
        return propertyDAO.searchPropertyListingByLocationIds(locationIdList, status, maxResults, accountId);
    }

    @Profiled(tag = "PropertyListingService-fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime")
    public List<PropertyListing> fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(Set<String> propertyListingIds) throws General1GroupException
    {

        List<PropertyListing> propertyListingList = propertyDAO.fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(propertyListingIds);
        return (propertyListingList == null) ? new ArrayList<PropertyListing>() : propertyListingList;
    }

    @Profiled(tag = "PropertyListingService-fetchAccountsAssocitedWithPropertyListingByIds")
    public Map<String, WSAccount> fetchAccountsAssocitedWithPropertyListingByIds(List<String> propertyListingIds)
    {
        Map<String, WSAccount> propertyListingIdsVsAccountMap = new HashMap<String, WSAccount>();
        if (propertyListingIds == null || propertyListingIds.isEmpty())
            return propertyListingIdsVsAccountMap;

        List<PropertyListingScoreObject> propertyListingScoreObjectList = propertyDAO.fetchAccountsByPropertyListingIds(propertyListingIds);
        for (PropertyListingScoreObject scoreObject : propertyListingScoreObjectList)
        {
            propertyListingIdsVsAccountMap.put(scoreObject.getId(), new WSAccount(scoreObject.getAccount()));
        }
        return propertyListingIdsVsAccountMap;
    }

    @Profiled(tag = "PropertyListingService-isPropertyListingsExistOfAccount")
    public Boolean isPropertyListingsExistOfAccount(String accountId)
    {
        Validation.notNull(accountId, "accountId should not be null.");
        List<PropertyListingScoreObject> propertyListingObject = propertyDAO.isPropertyListingsExistOfAccount(accountId);

        return (propertyListingObject.isEmpty()) ? false : true;
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsForOwnAccountId(String accountId, BroadcastStatus status)
    {
        return propertyDAO.fetchAllPropertyListingsForOwnAccountId(accountId, status);
    }

}
