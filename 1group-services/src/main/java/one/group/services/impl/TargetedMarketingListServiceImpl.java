package one.group.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import one.group.dao.TargetedMarketingListDAO;
import one.group.entities.jpa.TargetedMarketingList;
import one.group.services.TargetedMarketingListService;

public class TargetedMarketingListServiceImpl implements TargetedMarketingListService {

	TargetedMarketingListDAO targetedMarketingListDAO;
	
	public TargetedMarketingListDAO getTargetedMarketingListDAO() 
	{
		return targetedMarketingListDAO;
	}

	public void setTargetedMarketingListDAO(TargetedMarketingListDAO targetedMarketingListDAO) 
	{
		this.targetedMarketingListDAO = targetedMarketingListDAO;
	}


	public List<TargetedMarketingList> fetchAllTargetedList() 
	{	
		return targetedMarketingListDAO.fetchAllTargetedList();
	}

	@Transactional
	public void deleteTargetedListByPhoneNumber(String phoneNumber) 
	{
		targetedMarketingListDAO.deleteTargetedListByPhoneNumber(phoneNumber);
	}

	@Transactional
	public void deleteAllTargetedList() 
	{
		targetedMarketingListDAO.deleteAllTargetedList();
	}

}
