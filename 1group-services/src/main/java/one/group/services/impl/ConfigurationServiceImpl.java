package one.group.services.impl;

import javax.transaction.Transactional;

import one.group.dao.ConfigurationDAO;
import one.group.entities.jpa.Configuration;
import one.group.services.ConfigurationService;

import org.perf4j.aop.Profiled;

public class ConfigurationServiceImpl implements ConfigurationService
{
    private ConfigurationDAO configurationDAO;

    public ConfigurationDAO getConfigurationDAO()
    {
        return configurationDAO;
    }

    public void setConfigurationDAO(ConfigurationDAO configurationDAO)
    {
        this.configurationDAO = configurationDAO;
    }

    public Configuration getValueByKey(String key)
    {
        return configurationDAO.getConfiguationByKey(key);

    }

    @Transactional
    @Profiled(tag = "ConfigurationService-saveCOnfiguration")
    public Configuration saveCOnfiguration(Configuration config)
    {
        return configurationDAO.saveConfiguration(config);
    }

    public void remove(Configuration config)
    {
        configurationDAO.remove(config);

    }

}
