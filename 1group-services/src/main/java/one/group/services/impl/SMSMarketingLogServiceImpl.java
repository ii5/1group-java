package one.group.services.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import one.group.dao.SMSMarketingLogDAO;
import one.group.entities.jpa.SmsMarketingLog;
import one.group.services.SMSMarketingLogService;

import org.perf4j.aop.Profiled;

public class SMSMarketingLogServiceImpl implements SMSMarketingLogService
{
    private SMSMarketingLogDAO smsMarketingLogDAO;

    public SMSMarketingLogDAO getSmsMarketingLogDAO()
    {
        return smsMarketingLogDAO;
    }

    public void setSmsMarketingLogDAO(SMSMarketingLogDAO smsMarketingLogDAO)
    {
        this.smsMarketingLogDAO = smsMarketingLogDAO;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "SMSMarketingLogService - saveSMSMarketingLog")
    public void saveSMSMarketingLog(String phoneNumber, String message, String campaign)
    {

        SmsMarketingLog smsMarketingLog = new SmsMarketingLog();
        smsMarketingLog.setPhoneNumber(phoneNumber);
        smsMarketingLog.setMessage(message);
        smsMarketingLog.setCampaign(campaign);
        smsMarketingLog.setSentTime(new Date());

        smsMarketingLogDAO.saveSMSMarketingLog(smsMarketingLog);
    }

    @Profiled(tag = "SMSMarketingLogService - fetchAllLog")
    public List<SmsMarketingLog> fetchAllLog()
    {
        return smsMarketingLogDAO.fetchAllLog();
    }
}
