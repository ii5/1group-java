package one.group.services.impl;

import static one.group.core.Constant.AUTHORITIES_ROLE_ADMIN;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.AccountFieldType;
import one.group.core.enums.AccountType;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.core.enums.OAuthGrantType;
import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.Status;
import one.group.core.respone.ResponseBuilder;
import one.group.dao.AccountDAO;
import one.group.dao.ClientDAO;
import one.group.dao.GroupMembersDAO;
import one.group.dao.GroupsDAO;
import one.group.dao.MarketingListDAO;
import one.group.dao.NativeContactsDAO;
import one.group.dao.PropertyListingDAO;
import one.group.entities.api.request.WSAdminSMSRequest;
import one.group.entities.api.response.WSAdminReverseContacts;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.api.response.WSReverseContacts;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAdminGroupMetaData;
import one.group.entities.api.response.v2.WSAdminGroupMetaDataList;
import one.group.entities.api.response.v2.WSClient;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.helpers.ClientObject;
import one.group.entities.jpa.helpers.GroupMembersObject;
import one.group.entities.socket.Groups;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountService;
import one.group.services.AdminUtilityService;
import one.group.services.AuthorizationService;
import one.group.services.ClientService;
import one.group.services.PhonenumberService;
import one.group.services.PropertyListingService;
import one.group.services.SMSService;
import one.group.services.helpers.CmsConnectionFactory;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

public class AdminUtilityServiceImpl implements AdminUtilityService
{
    private static final Logger logger = LoggerFactory.getLogger(AdminUtilityServiceImpl.class);

    public static final String ACCOUNT = "account";
    public static final String CLIENT = "client";
    public static final String IS_ACCOUNT_EXISTS = "isAccountExists";

    private ClientService clientService;

    private AccountService accountService;

    private PhonenumberService phonenumberService;

    private AuthorizationService authorizationService;

    private AccountDAO accountDAO;

    private KafkaConfiguration kafkaConfiguration;

    private SimpleSyncLogProducer kafkaProducer;

    private String cdnImageURL;

    private String cdnImageFolder;

    private CmsConnectionFactory connectionFactory;

    private PropertyListingDAO propertyListingDAO;

    private PropertyListingService propertyListingService;

    private SMSService smsService;

    private SMSService smsCountryService;

    private SMSService smsNetcoreService;

    private String smsSender;

    private MarketingListDAO marketingListDAO;

    private NativeContactsDAO nativeContactsDAO;

    private ClientDAO clientDAO;

    private GroupMembersDAO groupMembersDAO;

    private GroupsDAO groupsDAO;

    public ClientDAO getClientDAO()
    {
        return clientDAO;
    }

    public void setClientDAO(ClientDAO clientDAO)
    {
        this.clientDAO = clientDAO;
    }

    public NativeContactsDAO getNativeContactsDAO()
    {
        return nativeContactsDAO;
    }

    public void setNativeContactsDAO(NativeContactsDAO nativeContactsDAO)
    {
        this.nativeContactsDAO = nativeContactsDAO;
    }

    public MarketingListDAO getMarketingListDAO()
    {
        return marketingListDAO;
    }

    public void setMarketingListDAO(MarketingListDAO marketingListDAO)
    {
        this.marketingListDAO = marketingListDAO;
    }

    public SMSService getSmsService()
    {
        return smsService;
    }

    public void setSmsService(SMSService smsService)
    {
        this.smsService = smsService;
    }

    public String getSmsSender()
    {
        return smsSender;
    }

    public void setSmsSender(String smsSender)
    {
        this.smsSender = smsSender;
    }

    public SMSService getSmsCountryService()
    {
        return smsCountryService;
    }

    public void setSmsCountryService(SMSService smsCountryService)
    {
        this.smsCountryService = smsCountryService;
    }

    public SMSService getSmsNetcoreService()
    {
        return smsNetcoreService;
    }

    public void setSmsNetcoreService(SMSService smsNetcoreService)
    {
        this.smsNetcoreService = smsNetcoreService;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public PropertyListingDAO getPropertyListingDAO()
    {
        return propertyListingDAO;
    }

    public void setPropertyListingDAO(PropertyListingDAO propertyListingDAO)
    {
        this.propertyListingDAO = propertyListingDAO;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public void setPhonenumberService(PhonenumberService phonenumberService)
    {
        this.phonenumberService = phonenumberService;
    }

    public void setAuthorizationService(AuthorizationService authorizationService)
    {
        this.authorizationService = authorizationService;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public String getCdnImageURL()
    {
        return cdnImageURL;
    }

    public void setCdnImageURL(String cdnImageURL)
    {
        this.cdnImageURL = cdnImageURL;
    }

    public String getCdnImageFolder()
    {
        return cdnImageFolder;
    }

    public void setCdnImageFolder(String cdnImageFolder)
    {
        this.cdnImageFolder = cdnImageFolder;
    }

    public CmsConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(CmsConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public GroupMembersDAO getGroupMembersDAO()
    {
        return groupMembersDAO;
    }

    public void setGroupMembersDAO(GroupMembersDAO groupMembersDAO)
    {
        this.groupMembersDAO = groupMembersDAO;
    }

    public GroupsDAO getGroupsDAO()
    {
        return groupsDAO;
    }

    public void setGroupsDAO(GroupsDAO groupsDAO)
    {
        this.groupsDAO = groupsDAO;
    }

    /**
     * Adds the mobile number and account and client.
     * 
     * @param mobileNumber
     *            the mobile number
     * @param deviceId
     *            the device id
     * @param deviceOsVersion
     *            the device os version
     * @param devicePlatform
     *            the device platform
     * @param otp
     *            the otp
     * @param grantType
     *            the grant type
     * @return the WS client
     * @throws Abstract1GroupException
     *             the abstract move in exception
     */
    @Profiled(tag = "AdminUtilityService-addMobileNumberAccountClient")
    public Map<String, Object> addMobileNumberAccountClient(final String mobileNumber, final String deviceId, final String deviceOsVersion, final String devicePlatform, final String otp,
            final String grantType, final String firstName, final String lastName, final String cityId, final Status status, final boolean isCreatingAdminRole, String cpsId)
            throws Abstract1GroupException
    {
        Map<String, Object> responseEntityMap = new HashMap<String, Object>();
        WSAccount wsAccount = accountService.fetchAccountByMobileNumber(mobileNumber);
        WSClient wsClient = null;
        String pushChannel = null;
        if (isCreatingAdminRole && wsAccount != null)
        {
            List<Client> clients = clientService.fetchAllClientsOfAccount(wsAccount.getId());
            if (clients != null && !clients.isEmpty())
            {
                wsClient = new WSClient(clients.get(0));
            }

            responseEntityMap.put(IS_ACCOUNT_EXISTS, true);
        }
        else
        {
            if (wsAccount == null)
            {
                if (mobileNumber != null)
                {
                    try
                    {
                        phonenumberService.savePhoneNumber(mobileNumber, PhoneNumberType.MOBILE, PhoneNumberSource.BPO);
                    }
                    catch (PersistenceException pe)
                    {
                        logger.error("Error while saving ADMIN phone number: " + pe.getMessage());
                    }
                }

                try
                {
                    Date registrationTime = null;
                    wsAccount = accountService.saveOrUpdateAccount(null, AccountType.AGENT, Status.SEEDED, mobileNumber, null, null, firstName, lastName, null, cityId, null, null, registrationTime);

                }
                catch (PersistenceException e)
                {
                    logger.error("Error creating ADMIN account: " + e.getMessage());
                    throw e;
                }
                responseEntityMap.put(IS_ACCOUNT_EXISTS, false);

                if (isCreatingAdminRole)
                {
                    pushChannel = getPushChannel();
                    String deviceName = "admin device";
                    wsClient = clientService.saveClient(null, Constant.APP_NAME, Constant.APP_VERSION, deviceName, deviceId, deviceOsVersion, true, pushChannel, null, null, null, DeviceStatus.ACTIVE,
                            devicePlatform, mobileNumber, cpsId);
                }
                accountService.incrementCount();
            }
            else
            {
                // Update short reference if account is unregistered
                if (wsAccount.getStatus().equals(Status.UNREGISTERED) && wsAccount.getShortReference() == null)
                {
                    accountService.updateShortReferenceForUnRegisteredAccountByAdmin(mobileNumber);
                }

                responseEntityMap.put(IS_ACCOUNT_EXISTS, true);
            }
        }
        responseEntityMap.put(CLIENT, wsClient);
        responseEntityMap.put(ACCOUNT, wsAccount);
        return responseEntityMap;
    }

    /**
     * Gets the push channel.
     * 
     * @return the push channel
     */
    public String getPushChannel()
    {
        String pushChannel = null;
        do
        {
            pushChannel = Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, Constant.PUSHER_CHANNEL_ID_LENGTH, false);
            WSClient tClient = clientService.fetchClientByPushChannel(pushChannel);
            if (tClient != null)
            {
                pushChannel = null;
            }
        }
        while (pushChannel == null);
        return pushChannel;
    }

    @Profiled(tag = "AdminUtilityService-generateAdminOauthToken")
    public String generateAdminOauthToken()
    {
        String accessToken = null;
        try
        {
            Properties adminProperties = new Properties();
            adminProperties.load(new FileInputStream("admin.properties"));
            String mobileNumber = (String) adminProperties.get("mobileNumber");
            String deviceId = (String) adminProperties.get("deviceId");
            String devicePlatform = (String) adminProperties.get("devicePlatform");
            String deviceOsVersion = (String) adminProperties.get("deviceOsVersion");
            String cpsId = (String) adminProperties.get("cpsId");
            String otp = (String) adminProperties.get("otp");
            String grantType = OAuthGrantType.CLIENT_CREDENTIALS.toString();
            boolean isAccountExists = false;
            WSClient wsClient = null;
            Map<String, Object> objectMap = addMobileNumberAccountClient(mobileNumber, deviceId, deviceOsVersion, devicePlatform, otp, grantType, null, null, null, Status.SEEDED, true, cpsId);
            wsClient = (WSClient) objectMap.get(CLIENT);
            isAccountExists = (Boolean) objectMap.get(IS_ACCOUNT_EXISTS);
            if (!isAccountExists)
            {
                authorizationService.createOauthClient(otp, grantType, wsClient, AUTHORITIES_ROLE_ADMIN);
            }
            /* generate oauth access_token */
            OAuth2AccessToken tokenDetails = authorizationService.getTokenDetails(wsClient.getId(), otp, grantType, null);
            accessToken = tokenDetails.getValue();
        }
        catch (General1GroupException e)
        {
            logger.error("error", e);
            ResponseBuilder.shootException(e);
        }
        catch (Abstract1GroupException e)
        {
            logger.error("error", e);
            ResponseBuilder.shootException(e);
        }
        catch (FileNotFoundException e)
        {
            logger.error("admin.properties File not found");
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
        catch (Exception e)
        {
            logger.error("error", e);
        }
        return accessToken;

    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AdminUtilityService-updateAccountByPhoneNumber")
    public void updateAccountByPhoneNumber(WSAccount wsaccount, AccountFieldType fieldType, String[] fieldValues) throws General1GroupException
    {
        try
        {
            Account account = accountDAO.fetchAccountById(wsaccount.getId());

            if (AccountFieldType.FULLNAME.equals(fieldType))
            {
                account.setFullName(fieldValues[0]);
            }
            else if (AccountFieldType.URL.equals(fieldType))
            {
                if (isImageUrlValid(fieldValues[0]) && isImageUrlValid(fieldValues[1]))
                {
                    if (account.getFullPhoto() == null)
                    {
                        String imageURL = fieldValues[0];
                        String imageName = imageURL.substring(imageURL.lastIndexOf("/") + 1);
                        String relativePathFull = cdnImageFolder + File.separator + connectionFactory.getImageFolder() + File.separator + imageName;
                        File fullPhotoFile = new File(relativePathFull);
                        BufferedImage fullPhoto = ImageIO.read(fullPhotoFile);

                        Media media = new Media();
                        media.setName("FULL" + account.getFullName());
                        media.setDimension(MediaDimensionType.FULL);
                        media.setType(MediaType.IMAGE);
                        media.setWidth(fullPhoto.getWidth());
                        media.setHeight(fullPhoto.getHeight());
                        media.setSize((int) fullPhotoFile.length());
                        account.setFullPhoto(media);

                    }

                    if (account.getThumbnailPhoto() == null)
                    {
                        String imageURL = fieldValues[1];
                        String imageName = imageURL.substring(imageURL.lastIndexOf("/") + 1);

                        String relativePathFull = cdnImageFolder + File.separator + connectionFactory.getThumbnailFolder() + File.separator + imageName;
                        File fullThumbnailPhotoFile = new File(relativePathFull);
                        BufferedImage fullThumbnailPhoto = ImageIO.read(fullThumbnailPhotoFile);

                        Media media = new Media();
                        media.setName("THUMBNAIL" + account.getFullName());
                        media.setDimension(MediaDimensionType.THUMBNAIL);
                        media.setType(MediaType.IMAGE);
                        media.setWidth(fullThumbnailPhoto.getWidth());
                        media.setHeight(fullThumbnailPhoto.getHeight());
                        media.setSize((int) fullThumbnailPhotoFile.length());
                        account.setThumbnailPhoto(media);
                    }

                }
                else
                {
                    throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, "Invalid format of Url for " + account.getPrimaryPhoneNumber().getNumber());
                }

                account.getFullPhoto().setUrl(fieldValues[0]);
                account.getThumbnailPhoto().setUrl(fieldValues[1]);
            }
            accountDAO.updateAccount(account);
            pushHintToSyncLog(HintType.FETCH, EntityType.ACCOUNT, account.getId(), SyncActionType.EDIT);

        }
        catch (Exception e)
        {
            logger.error("Error updating admin account.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Exception while Updating Account", e);
        }
    }

    public void pushHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType) throws SyncLogProcessException
    {
        WSEntityReference entityReference = new WSEntityReference(hintType, entityType, accountId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAction(actionType);
        syncLogEntry.setAssociatedEntityType(entityType);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, entityReference);

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
    }

    public boolean isImageUrlValid(String imageUrl)
    {
        return true;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AdminUtilityService-updatePropertyListings")
    public void updatePropertyListings(String propertyListingIds) throws BroadcastProcessException, General1GroupException, Exception
    {
        SyncActionType syncActionType;
        PropertyListing propertyListing;

        String[] propertyListingIdArray = propertyListingIds.split(",");
        Set<String> propertyListingSet = new HashSet<String>(Arrays.asList(propertyListingIdArray));

        syncActionType = SyncActionType.MANUAL_CLOSE;
        for (String propertyListingId : propertyListingSet)
        {
            propertyListingId = propertyListingId.trim();
            propertyListing = propertyListingDAO.fetchPropertyListing(propertyListingId);

            if (propertyListing == null)
            {
                continue;
            }

            propertyListingService.closePropertyListing(propertyListing);

            WSPropertyListing wsPropertyListing = new WSPropertyListing(propertyListing);

            wsPropertyListing.setSyncActionType(syncActionType);
            wsPropertyListing.setChangedRequiredFields(false);
            propertyListingService.appendToSyncLog(wsPropertyListing);
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AdminUtilityService-getAccountDetailsFromNativeContactsByPhoneNumber")
    public WSReverseContacts getAccountDetailsFromNativeContactsByPhoneNumber(String phoneNumber) throws General1GroupException
    {
        Validation.isTrue(!Utils.isNullOrEmpty(phoneNumber), "PhoneNumber passed should not be empty");

        List<String> nativeContactList = nativeContactsDAO.fetchNativeContactsByPhoneNumber(phoneNumber, Status.ACTIVE);

        // Remove own account from native contact list
        Account requestedAccount = accountDAO.fetchAccountByPhoneNumber(phoneNumber);
        if (requestedAccount != null)
        {
            nativeContactList.remove(requestedAccount.getId());
        }

        if (nativeContactList.isEmpty())
        {
            throw new General1GroupException(GeneralExceptionCode.NOT_FOUND, true, "Phone Number does not exist." + phoneNumber);
        }

        HashMap<String, WSAdminReverseContacts> adminReverseContactObjMap = new HashMap<String, WSAdminReverseContacts>();
        List<String> reverseContactphoneNumberList = new ArrayList<String>();

        // Prepare set for accountIds
        Set<String> accountIdsSet = new HashSet<String>(nativeContactList);

        // Fetch all accounts in bulk
        List<Account> accountList = accountDAO.fetchAllAccountByIds(accountIdsSet);
        Map<String, Account> accountsMap = new HashMap<String, Account>();
        for (Account account : accountList)
        {
            accountsMap.put(account.getId(), account);
        }

        // Fetch all last activity of accounts
        List<ClientObject> clientList = clientDAO.getLastActivityOfAccounts(accountIdsSet);
        Map<String, Date> clientMap = new HashMap<String, Date>();
        for (ClientObject clientObject : clientList)
        {
            clientMap.put(clientObject.getAccountId(), clientObject.getLastActivityTime());
        }

        // Fetch all property listing count of accounts
        // List<PropertyListingCountObject> propertyListingCountList =
        // propertyListingDAO.fetchPropertyListingCountOfAccountsByStatus(accountIdsSet,
        // BroadcastStatus.ACTIVE);
        // Map<String, Long> propertyListingCountMap = new HashMap<String,
        // Long>();
        // for (PropertyListingCountObject countObject :
        // propertyListingCountList)
        // {
        // propertyListingCountMap.put(countObject.getAccountId(),
        // countObject.getCount());
        // }
        //
        for (String accountId : nativeContactList)
        {
            WSAdminReverseContacts wsAdminReverseContacts = new WSAdminReverseContacts(accountsMap.get(accountId));
            wsAdminReverseContacts.setLastActivityTime("" + clientMap.get(accountId));

            String accountPhoneNumber = wsAdminReverseContacts.getPhoneNumber();
            adminReverseContactObjMap.put(accountPhoneNumber, wsAdminReverseContacts);
            reverseContactphoneNumberList.add(accountPhoneNumber);
        }

        if (adminReverseContactObjMap.isEmpty())
        {
            throw new General1GroupException(GeneralExceptionCode.NOT_FOUND, true, "Phone Number does not exist." + phoneNumber);
        }

        WSReverseContacts reverseContacts = new WSReverseContacts();

        // Ordering ReverseContacts by WhatsApp group Count
        // List<MarketingList> marketingList =
        // marketingListDAO.fetchPhoneNumbersOrderByWhatsAppGroupCount(reverseContactphoneNumberList);
        // if (!marketingList.isEmpty())
        // {
        // List<WSAdminReverseContacts> OrderedReverseContactsList = new
        // ArrayList<WSAdminReverseContacts>();
        //
        // for (MarketingList marketing : marketingList)
        // {
        // String orderedPhoneNumber = marketing.getPhoneNumber();
        //
        // if (adminReverseContactObjMap.containsKey(orderedPhoneNumber))
        // {
        // OrderedReverseContactsList.add(adminReverseContactObjMap.get(orderedPhoneNumber));
        // adminReverseContactObjMap.remove(orderedPhoneNumber);
        // }
        // }
        // // Add Unordered ReverseContact End Of List
        // if (!adminReverseContactObjMap.isEmpty())
        // {
        // OrderedReverseContactsList.addAll(adminReverseContactObjMap.values());
        // }
        // reverseContacts.setAccounts(OrderedReverseContactsList);
        // }
        // else
        // {
        reverseContacts.setAccounts(new ArrayList(adminReverseContactObjMap.values()));
        // }
        return reverseContacts;
    }

    @Profiled(tag = "AdminUtilityService-sendSMSToPhoneNumber")
    public void sendSMSToPhoneNumber(WSAdminSMSRequest wsAdminSMSRequest) throws General1GroupException
    {
        try
        {
            if (getSmsSender().equals(Constant.DEFAULT_SMS_SENDER))
            {
                smsCountryService.sendSMS(wsAdminSMSRequest.getPhoneNumber(), wsAdminSMSRequest.getContent());
            }
            else
            {
                smsNetcoreService.sendSMS(wsAdminSMSRequest.getPhoneNumber(), wsAdminSMSRequest.getContent());
            }
        }
        catch (IOException e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, "Error during Sending Sms.", e);
        }
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "AdminUtilityService-updateCityId")
    public boolean updateCityIdOfGroups(String cityId, String mobileNumber) throws General1GroupException, Exception
    {
        boolean groupsUpdated = false;
        List<GroupMembers> groupMembers = groupMembersDAO.fetchGroupMembersByMobileNo(mobileNumber);
        List<String> groupIds = new ArrayList<String>();
        for (GroupMembers groupMember : groupMembers)
        {
            String groupId = groupMember.getGroupId();
            groupIds.add(groupId);
        }
        List<Groups> groups = groupsDAO.fetchGroupsByGroupIds(groupIds);
        List<Groups> groupsToUpdate = new ArrayList<Groups>();

        for (Groups group : groups)
        {
            String groupCityId = group.getCityId() == null ? "0" : group.getCityId();
            if (Integer.parseInt(groupCityId) == 0)
            {
                group.setCityId(cityId);
                groupsToUpdate.add(group);
            }
        }
        if (!groupsToUpdate.isEmpty())
        {
            groupsDAO.saveGroupsBulk(groupsToUpdate);
            groupsUpdated = true;
        }
        return groupsUpdated;
    }

    public WSAdminGroupMetaDataList getGroupMetaDataByPhoneNumber(Set<String> phoneNumberSet, String everSync)
    {

        WSAdminGroupMetaDataList wsAdminGroupMetaDataList = new WSAdminGroupMetaDataList();
        // WSAdminGroupMetaData wsAdminGroupMetaData = new
        // WSAdminGroupMetaData();
        List<WSAdminGroupMetaData> wsAdminGroupMetaList = new ArrayList<WSAdminGroupMetaData>();

        if (!Utils.isNull(phoneNumberSet))
        {
            List<String> phoneNumberList = new ArrayList<String>(phoneNumberSet);
            List<GroupMembers> groupMembers = new ArrayList<GroupMembers>();
            List<Boolean> everSyncList = new ArrayList<Boolean>();
            List<GroupMembersObject> groupMemberObjectList = new ArrayList<GroupMembersObject>();

            if (!Utils.isNull(everSync) && everSync.equals("true"))
            {
                everSyncList.add(true);

                // fetch GroupsMembers with everSync true
                // groupMembers =
                // groupMembersDAO.fetchGroupMembersByMobileNoAndEverSync(phoneNumberList,
                // true);
                groupMemberObjectList = groupMembersDAO.fetchGroupMembersObjectByMobileNoAndEverSync(phoneNumberList, everSyncList);
            }
            else
            {
                everSyncList.add(true);
                everSyncList.add(false);
                // fetch GroupsMembers with everSync true and false
                // groupMembers =
                // groupMembersDAO.fetchGroupMembersByMobileNumbers(phoneNumberList);

                groupMemberObjectList = groupMembersDAO.fetchGroupMembersObjectByMobileNoAndEverSync(phoneNumberList, everSyncList);

            }

            Map<String, List<String>> phoneNumberVsGroupIdsMap = new HashMap<String, List<String>>();
            for (GroupMembersObject groupMembersObject : groupMemberObjectList)
            {
                String groupId = groupMembersObject.getGroupId();
                String phoneNumber = groupMembersObject.getMobileNumber();

                if (phoneNumberVsGroupIdsMap.containsKey(phoneNumber))
                {
                    List<String> groupIds = phoneNumberVsGroupIdsMap.get(phoneNumber);
                    groupIds.add(groupId);
                    phoneNumberVsGroupIdsMap.put(phoneNumber, groupIds);
                }
                else
                {
                    List<String> groupIdsList = new ArrayList<String>();
                    groupIdsList.add(groupId);
                    phoneNumberVsGroupIdsMap.put(phoneNumber, groupIdsList);
                }
            }

            // for (GroupMembers groupMember : groupMembers)
            // {
            // String phoneNumber = groupMember.getMobileNumber();
            // if (phoneNumberVsGroupIdsMap.containsKey(phoneNumber))
            // {
            // List<String> groupIds =
            // phoneNumberVsGroupIdsMap.get(phoneNumber);
            // groupIds.add(groupMember.getGroupId());
            // phoneNumberVsGroupIdsMap.put(phoneNumber, groupIds);
            // }
            // else
            // {
            // List<String> groupIdsList = new ArrayList<String>();
            // groupIdsList.add(groupMember.getGroupId());
            // phoneNumberVsGroupIdsMap.put(phoneNumber, groupIdsList);
            // }
            // }

            for (String phoneNumber : phoneNumberVsGroupIdsMap.keySet())
            {
                List<String> groupIds = phoneNumberVsGroupIdsMap.get(phoneNumber);
                if (!groupIds.isEmpty())
                {
                    WSAdminGroupMetaData metaData = groupsDAO.fetchGroupMetaDataByGroupIdsAndEverSync(groupIds, true);
                    metaData.setPhoneNumber(phoneNumber);
                    wsAdminGroupMetaList.add(metaData);
                }

            }
            wsAdminGroupMetaDataList.setGroupMetaData(wsAdminGroupMetaList);
        }
        return wsAdminGroupMetaDataList;
    }
}
