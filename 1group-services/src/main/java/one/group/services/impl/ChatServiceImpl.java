package one.group.services.impl;

import java.util.List;

import one.group.core.message.Message;
import one.group.services.ChatService;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Concrete implementation of {@link ChatService}.
 * 
 * TODO : Document correctly.
 * 
 * @author nyalfernandes
 * 
 */
public class ChatServiceImpl implements ChatService
{
    private static final Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);

    public List<String> getChatThreads(final String accountId, final int sinceIndex, final int beforeIndex, final int limit)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void sendChatMessage(final String fromAccountId, final String toAccountId, final Message message)
    {
        Validation.notNull(fromAccountId, "The originating Account identifier cannot be null.");
        Validation.notNull(toAccountId, "The target Account identifier cannot be null.");
        Validation.notNull(message, "The message passed cannot be null.");

        System.out.println("Whooohooo");
    }

}
