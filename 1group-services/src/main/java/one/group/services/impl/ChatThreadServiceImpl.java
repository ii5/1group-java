package one.group.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.MessageType;
import one.group.dao.AccountDAO;
import one.group.dao.ChatMessageDAO;
import one.group.dao.ChatThreadDAO;
import one.group.dao.PropertyListingDAO;
import one.group.dao.SubscriptionDAO;
import one.group.entities.api.response.WSMessageDeliveryStatus;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadReference;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.cache.ChatKey;
import one.group.entities.cache.SubscriptionKey;
import one.group.exceptions.movein.ChatProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.ChatMessageService;
import one.group.services.ChatThreadService;
import one.group.utils.Utils;
import one.group.utils.validation.exception.ValidationException;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ChatThreadServiceImpl implements ChatThreadService
{
    private static final Logger logger = LoggerFactory.getLogger(ChatThreadServiceImpl.class);

    private ChatThreadDAO chatThreadDAO;
    private ChatMessageDAO chatMessageDAO;
    private PropertyListingDAO propertyDAO;
    private SubscriptionDAO subscriptionDAO;
    private AccountDAO accountDAO;

    private ChatMessageService chatMessageService;

    public ChatMessageDAO getChatMessageDAO()
    {
        return chatMessageDAO;
    }

    public ChatThreadDAO getChatThreadDAO()
    {
        return chatThreadDAO;
    }

    public ChatMessageService getChatMessageService()
    {
        return chatMessageService;
    }

    public PropertyListingDAO getPropertyDAO()
    {
        return propertyDAO;
    }

    public void setChatMessageDAO(final ChatMessageDAO chatMessageDAO)
    {
        this.chatMessageDAO = chatMessageDAO;
    }

    public void setChatThreadDAO(final ChatThreadDAO chatThreadDAO)
    {
        this.chatThreadDAO = chatThreadDAO;
    }

    public SubscriptionDAO getSubscriptionDAO()
    {
        return subscriptionDAO;
    }

    public void setSubscriptionDAO(SubscriptionDAO subscriptionDAO)
    {
        this.subscriptionDAO = subscriptionDAO;
    }

    public void setChatMessageService(ChatMessageService chatMessageService)
    {
        this.chatMessageService = chatMessageService;
    }

    public void setPropertyDAO(PropertyListingDAO propertyDAO)
    {
        this.propertyDAO = propertyDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    @Profiled(tag = "ChatThreadService-fetchChatThreadOfAccount")
    public List<WSChatThreadReference> fetchChatThreadOfAccount(final String accountId, final String indexBy, final int offset, final int limit) throws ChatProcessException, JsonMappingException,
            JsonGenerationException, IOException
    {
        List<WSChatThreadReference> chatThreads = new ArrayList<WSChatThreadReference>();

        return chatThreads;
    }

    public List<WSChatThreadReference> fetchChatThreadOfAccountDummy(final String accountId, final String indexBy, final int offset, final int limit) throws ChatProcessException,
            JsonMappingException, JsonGenerationException, IOException
    {

        List<WSChatThreadReference> chatThreadList = new ArrayList<WSChatThreadReference>();
        int j = 0;
        for (int i = 1; i < 30; i++)
        {

            String chatThreadId = "chat-thread-" + i + "-" + j;
            WSChatThreadCursor cursor = new WSChatThreadCursor(-1, -1);

            List<WSMessageResponse> wsChatMessageList = new ArrayList<WSMessageResponse>();
            WSAccount createdBy = WSAccount.dummyData("Mitesh Chavda", "ii5 Technology", "+919898989898", false);
            WSAccount toAccount = WSAccount.dummyData("Mitesh Chavda", "ii5 Technology", "+919898989898", false);
            WSMessageResponse wsChatMessage = WSMessageResponse.dummyData(createdBy, toAccount, i, MessageType.TEXT, "Hello" + i, null, chatThreadId, null);
            wsChatMessage.setText("Hello [" + i + "]");
            List<WSMessageDeliveryStatus> statusList = new ArrayList<WSMessageDeliveryStatus>();
            wsChatMessage.setIndex(0);
            wsChatMessage.setId(UUID.randomUUID() + "");
            // wsChatMessage.setDeliveryStatus(statusList);

            wsChatMessageList.add(wsChatMessage);

            WSChatThread chatThread = new WSChatThread(chatThreadId, 0, wsChatMessageList);
            WSChatThreadReference chatThreadRef = new WSChatThreadReference(chatThread, i);
            chatThreadList.add(chatThreadRef);

        }
        return chatThreadList;
    }

    @Profiled(tag = "ChatThreadService-fetchChatThreadDetails")
    public WSChatThread fetchChatThreadDetails(String accountId, String chatThreadId, int offset, int limit) throws ChatProcessException, JsonMappingException, JsonGenerationException, IOException,
            General1GroupException
    {
        return new WSChatThread();
    }

    @Profiled(tag = "ChatThreadService-updateChatThreadCursor")
    public WSChatThreadCursor updateChatThreadCursor(final String chatThreadId, final String accountId, final int readIndex, int receivedIndex) throws ValidationException, JsonMappingException,
            JsonGenerationException, IOException, SyncLogProcessException
    {

        WSChatThreadCursor wsChatThreadCursor = new WSChatThreadCursor();
        wsChatThreadCursor.setReadIndex(readIndex);
        wsChatThreadCursor.setReceivedIndex(readIndex);
        return wsChatThreadCursor;
    }

    @Profiled(tag = "ChatThreadService-getMaxChatThreadIndex")
    public Integer getMaxChatThreadIndex(String accountId, String indexBy)
    {
        long chatThreadMaxIndex = 0;

        if (indexBy.equals(Constant.CHAT_THREAD_BY_ACTIVITY))
        {
            chatThreadMaxIndex = chatThreadDAO.fetchChatThreadCountByActivity(accountId) - 1;
        }
        else if (indexBy.equals(Constant.CHAT_THREAD_BY_JOINED))
        {
            chatThreadMaxIndex = chatThreadDAO.fetchChatThreadCountByJoined(accountId) - 1;
        }

        return Long.valueOf(chatThreadMaxIndex).intValue();
    }

    @Profiled(tag = "ChatThreadService-fetchChatThreadsOfAccountByProperty")
    public List<WSChatThreadReference> fetchChatThreadsOfAccountByProperty(String accountId, String broadcastId, String indexBy, long offset, long limit) throws JsonMappingException,
            JsonGenerationException, IOException
    {

        WSAccount toAccount = WSAccount.dummyData("Ruby Parker", "Real Property Eastate", "+919745612307", true);
        WSAccount createdBy = WSAccount.dummyData("Pravin dandekar", "Sherkhan Properpery", "+918974123654", true);

        List<WSChatThreadReference> wsChatThreads = new ArrayList<WSChatThreadReference>();
        WSChatThreadReference refernce1 = WSChatThreadReference.dummyData(5, 5, createdBy, toAccount, 2, 1);
        WSChatThreadReference refernce2 = WSChatThreadReference.dummyData(7, 5, createdBy, toAccount, 2, 1);
        WSChatThreadReference refernce3 = WSChatThreadReference.dummyData(8, 5, createdBy, toAccount, 2, 1);
        wsChatThreads.add(refernce1);
        wsChatThreads.add(refernce2);
        wsChatThreads.add(refernce3);
        return wsChatThreads;

    }

    @Profiled(tag = "ChatThreadService-migrateChatThreadsOfBroadcast")
    public void migrateChatThreadsOfBroadcast(String broadcastIdOld, String broadcastIdNew)
    {
        Set<String> chatThreads = chatThreadDAO.fetchChatThreadByProperty(broadcastIdOld);
        for (String chatThreadId : chatThreads)
        {
            chatThreadDAO.saveChatThreadForPropertyListing(chatThreadId, broadcastIdNew);
        }
    }

    private WSChatThreadReference formWSChatThreadReference(String accountId, String chatThreadId, String indexBy) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSChatThreadReference wsChatThreadReference = new WSChatThreadReference();
        return wsChatThreadReference;
    }

    @Profiled(tag = "ChatThreadService-removeChatThreadSubscription")
    public void removeChatThreadSubscription(String fromAccountId, String toAccountId)
    {
        String chatKey = ChatKey.CHAT_THREAD_ID.getKey();
        String chatThreadId = Utils.generateChatThreadId(fromAccountId, toAccountId, chatKey);
        // subscriptionDAO.deleteSubscription(fromAccountId, chatThreadId,
        // EntityType.CHAT);

        // API-76 Remove use of redis scan.
        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", EntityType.CHAT.toString());
        values.put("sc_entity_key", chatThreadId);
        values.put("account_id", toAccountId);
        String subscriptionKey = SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values);
        subscriptionDAO.removeSubscriptionByKey(subscriptionKey);
    }

    @Profiled(tag = "ChatThreadService-addChatThreadSubscription")
    public void addChatThreadSubscription(String fromAccountId, String toAccountId)
    {
        String chatKey = ChatKey.CHAT_THREAD_ID.getKey();
        String chatThreadId = Utils.generateChatThreadId(fromAccountId, toAccountId, chatKey);
        // subscriptionDAO.addSubscription(fromAccountId, chatThreadId,
        // EntityType.CHAT);
        subscriptionDAO.addSubscription(toAccountId, chatThreadId, EntityType.CHAT);
    }

}
