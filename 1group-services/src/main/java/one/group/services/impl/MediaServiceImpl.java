package one.group.services.impl;

import one.group.core.enums.MediaType;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.PropertyListing;
import one.group.jpa.dao.MediaJpaDAO;
import one.group.services.MediaService;

import org.perf4j.aop.Profiled;

public class MediaServiceImpl implements MediaService
{
    MediaJpaDAO mediaJpaDAO;

    /**
     * @return the mediaJpaDAO
     */
    public MediaJpaDAO getMediaJpaDAO()
    {
        return mediaJpaDAO;
    }

    /**
     * @param mediaJpaDAO
     *            the mediaJpaDAO to set
     */
    public void setMediaJpaDAO(MediaJpaDAO mediaJpaDAO)
    {
        this.mediaJpaDAO = mediaJpaDAO;
    }

    @Profiled(tag = "MediaService-addMedi")
    public void addMedia(String mediaName, MediaType type, String url, PropertyListing propertyListing)
    {
        Media media = new Media();
        media.setName(mediaName);
        media.setType(type);
        media.setUrl(url);

        mediaJpaDAO.saveOrUpdate(media);
    }

}
