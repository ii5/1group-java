package one.group.services.impl;

import java.util.List;

import one.group.dao.ParentLocationsDAO;
import one.group.entities.jpa.ParentLocations;
import one.group.services.ParentLocationService;

public class ParentLocationServiceImpl implements ParentLocationService
{
    private ParentLocationsDAO parentLocationDAO;

    public ParentLocationsDAO getParentLocationDAO()
    {
        return parentLocationDAO;
    }

    public void setParentLocationDAO(ParentLocationsDAO parentLocationDAO)
    {
        this.parentLocationDAO = parentLocationDAO;
    }

    public List<ParentLocations> getAllLocationsByCityId(String cityId)
    {
        return parentLocationDAO.getAllLocationsByCityId(cityId);
    }

}
