package one.group.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import one.group.core.ClientVersion;
import one.group.core.Constant;
import one.group.core.enums.ClientType;
import one.group.core.enums.ClientUpdateType;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.dao.ClientDAO;
import one.group.dao.OtpDAO;
import one.group.dao.SyncUpdateReadCursorsDAO;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSClient;
import one.group.entities.api.response.v2.WSClientData;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Otp;
import one.group.exceptions.codes.ClientExceptionCode;
import one.group.exceptions.codes.SigninExceptionCode;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.ClientService;
import one.group.services.helpers.EnvironmentConfiguration;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

/**
 * Concrete implementation of {@link ClientService}.
 * 
 * TODO: Document properly
 * 
 * @author miteshchavda
 * 
 */
public class ClientServiceImpl implements ClientService
{
    private static final Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);
    private ClientDAO clientDAO;

    private AccountDAO accountDAO;

    private OtpDAO otpDAO;

    private SyncUpdateReadCursorsDAO syncLogReadCursorsDAO;

    private DefaultTokenServices defaultTokenServices;

    private ClientVersion clietVersionUtil;

    private EnvironmentConfiguration environmentConfiguration;

    private long otpExpiryTime;

    public long getOtpExpiryTime()
    {
        return otpExpiryTime;
    }

    public void setOtpExpiryTime(long otpExpiryTime)
    {
        this.otpExpiryTime = otpExpiryTime;
    }

    public EnvironmentConfiguration getEnvironmentConfiguration()
    {
        return environmentConfiguration;
    }

    public void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration)
    {
        this.environmentConfiguration = environmentConfiguration;
    }

    public ClientVersion getClietVersionUtil()
    {
        return clietVersionUtil;
    }

    public void setClietVersionUtil(ClientVersion clietVersionUtil)
    {
        this.clietVersionUtil = clietVersionUtil;
    }

    public void setDefaultTokenServices(DefaultTokenServices defaultTokenServices)
    {
        this.defaultTokenServices = defaultTokenServices;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public ClientDAO getClientDAO()
    {
        return clientDAO;
    }

    public void setClientDAO(ClientDAO clientDAO)
    {
        this.clientDAO = clientDAO;
    }

    public OtpDAO getOtpDAO()
    {
        return otpDAO;
    }

    public void setOtpDAO(OtpDAO otpDAO)
    {
        this.otpDAO = otpDAO;
    }

    public SyncUpdateReadCursorsDAO getSyncLogReadCursorsDAO()
    {
        return syncLogReadCursorsDAO;
    }

    public void setSyncLogReadCursorsDAO(SyncUpdateReadCursorsDAO syncLogReadCursorsDAO)
    {
        this.syncLogReadCursorsDAO = syncLogReadCursorsDAO;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-fetchAllClientsOfAccount")
    public List<Client> fetchAllClientsOfAccount(String accountId)
    {
        return clientDAO.fetchAllClientsOfAccount(accountId);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-fetchAllActiveClientsOfAccount")
    public List<Client> fetchAllActiveClientsOfAccount(String accountId)
    {
        return clientDAO.fetchAllActiveClientsOfAccount(accountId);
    }

    @Transactional
    @Profiled(tag = "ClientService-saveClient")
    public WSClient saveClient(Account account, String appName, String appVersion, String deviceModelName, String deviceToken, String deviceVersion, boolean isOnline, String pushChannel,
            String pushAlert, String pushBadge, String pushSound, DeviceStatus status, String devicePlatform, String mobileNumber, String cpsId)
    {
        if (account == null)
        {
            account = accountDAO.fetchAccountByPhoneNumber(mobileNumber);
        }

        // Check if account signins from same client again then override client
        // information of same account
        List<Client> existingClients = clientDAO.fetchByDeviceToken(deviceToken);
        if (existingClients != null && existingClients.size() > 0)
        {
            for (Client singleClient : existingClients)
            {
                singleClient.setDeviceToken(null);
                // API-126
                singleClient.setUpdatedBy(singleClient.getAccount().getId());
                singleClient.setUpdatedTime(new Date());
                clientDAO.saveClient(singleClient);
            }
        }

        Client client = new Client();

        client.setAccount(account);
        client.setAppName(appName);
        client.setAppVersion(appVersion);
        client.setDeviceModel(deviceModelName);
        client.setDeviceToken(deviceToken);
        client.setDeviceVersion(deviceVersion);
        client.setOnline(isOnline);
        client.setPushChannel(pushChannel);
        client.setPushAlert(pushAlert);
        client.setPushBadge(pushBadge);
        client.setPushSound(pushSound);
        client.setStatus(status);
        client.setDevicePlatform(devicePlatform);
        client.setCpsId(cpsId);

        // API-126
        client.setCreatedBy(account.getId());
        clientDAO.saveClient(client);

        WSClient wsClient = new WSClient(client);
        wsClient.setAccountId(account.getId());
        return wsClient;
    }

    @Profiled(tag = "ClientService-checkIfActiveClient")
    public boolean checkIfActiveClient(String clientId)
    {
        Client clientObj = clientDAO.fetchClientById(clientId);

        return clientObj != null && clientObj.getStatus().equals(Status.ACTIVE);
    }

    @Profiled(tag = "ClientService-fetchAllPushChannelsOfClientsOfAccount")
    public List<String> fetchAllPushChannelsOfClientsOfAccount(String accountId)
    {
        return clientDAO.fetchAllPushChannelsOfClientsOfAccount(accountId);
    }

    // public List<String> fetchAllAndroidDeviceIdsOfClientOfAccount(String
    // accountId)
    // {
    // List<Client> clients = fetchAllClientsOfAccount(accountId);
    // List<String> deviceIdList = new ArrayList<String>();
    //
    // for (Client c : clients)
    // {
    // if (c.getDeviceToken() != null)
    // {
    // deviceIdList.add(c.getDeviceToken());
    // }
    // }
    //
    // return deviceIdList;
    // }
    //
    @Profiled(tag = "ClientService-fetchByAccountAndDevicePlatform")
    public List<String> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform)
    {
        List<Client> clients = clientDAO.fetchByAccountAndDevicePlatform(accountId, devicePlatform);
        List<String> deviceIdList = new ArrayList<String>();

        for (Client c : clients)
        {
            if (c.getDeviceToken() != null)
            {
                deviceIdList.add(c.getDeviceToken());
            }
        }

        return deviceIdList;
    }

    @Profiled(tag = "ClientService-fetchClientById")
    public Client fetchClientById(String clientId)
    {
        return clientDAO.fetchClientById(clientId);
    }

    @Profiled(tag = "ClientService-fetchClientByPushChannel")
    public WSClient fetchClientByPushChannel(String pushChannel)
    {
        Client client = clientDAO.fetchByPushChannel(pushChannel);
        if (client == null)
        {
            return null;
        }

        return new WSClient(client);
    }

    @Transactional
    @Profiled(tag = "ClientService-checkValidAndDeleteIfValid")
    public void checkValidOtpAndDeleteIfValid(String otp, String mobileNumber) throws General1GroupException
    {
        Otp otpO = otpDAO.fetchOtpFromOtpAndPhoneNumber(otp, mobileNumber);

        if (otpO != null)
        {
            long currentTime = System.currentTimeMillis();
            String smsMode = environmentConfiguration.getSmsMode();

            long expiryTime = otpO.getCreatedTime().getTime() + otpExpiryTime;

            if (currentTime > expiryTime && smsMode.equals(Constant.STRING_TRUE))
            {
                // delete Expired Otp
                // otpDAO.deleteOtp(otpO);
                throw new General1GroupException(SigninExceptionCode.NO_VALID_OTP, true, otp);
            }

            otpDAO.deleteOtp(otpO);
        }
        else
        {
            throw new General1GroupException(SigninExceptionCode.NO_VALID_OTP, true, otp);
        }
    }

    @Profiled(tag = "ClientService-getClientIdByAuthToken")
    public String getClientIdByAuthToken(String authToken) throws AuthenticationException
    {
        String clientId = null;
        Validation.isTrue(!Utils.isNullOrEmpty(authToken), "Auth token is null or empty");
        try
        {
            clientId = defaultTokenServices.getClientId(authToken);
        }
        catch (Exception e)
        {
            logger.error("No client authorized for the auth token '" + authToken + "'.", e);
            throw new AuthenticationException(ClientExceptionCode.CLIENT_NOT_FOUND, false, "No client authorized for the auth token '%s'.", e, authToken);
        }
        return clientId;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-getClientIdByAuthToken")
    public one.group.entities.api.request.WSClient getClientByAuthToken(String authToken) throws AuthenticationException
    {
        String clientId = null;
        Validation.isTrue(!Utils.isNullOrEmpty(authToken), "Auth token is null or empty");
        try
        {
            clientId = defaultTokenServices.getClientId(authToken);
        }
        catch (Exception e)
        {
            logger.error("No client authorized for the auth token '" + authToken + "'.", e);
            throw new AuthenticationException(ClientExceptionCode.CLIENT_NOT_FOUND, false, "No client authorized for the auth token '%s'.", e, authToken);
        }
        return new one.group.entities.api.request.WSClient(fetchClientById((clientId)));
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-getAccountFromAuthToken")
    public Account getAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        String clientId = getClientIdByAuthToken(authToken);
        Account account = null;
        if (clientId != null)
        {
            try
            {
                account = clientDAO.getAccountFromAuthToken(authToken, clientId);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return account;
    }
    
    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-getAccountFromAuthToken")
    public WSAccount getWSAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        String clientId = getClientIdByAuthToken(authToken);
        Account account = null;
        if (clientId != null)
        {
            try
            {
                account = clientDAO.getAccountFromAuthToken(authToken, clientId);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return new WSAccount(account);
    }

    @Profiled(tag = "ClientService-fetchAllActiveClients")
    public List<Client> fetchAllActiveClients()
    {
        return clientDAO.fetchAllActiveClients();
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-updateClientVersion")
    public WSClientData updateClientVersion(String authToken, one.group.entities.api.request.v2.WSClientData wsClientData) throws AuthenticationException
    {
        Validation.isTrue(!Utils.isNullOrEmpty(authToken), "Auth token is null or empty");
        String clientId = null;
        try
        {
            clientId = defaultTokenServices.getClientId(authToken);
        }
        catch (Exception e)
        {
            logger.error("No client authorized for the auth token '" + authToken + "'.", e);
            throw new AuthenticationException(ClientExceptionCode.CLIENT_NOT_FOUND, false, "No client authorized for the auth token '%s'.", e, authToken);
        }

        Client client = fetchClientById(clientId);
        client.setAppVersion(wsClientData.getVersion());
        clientDAO.saveClient(client);

        ClientType requestedClientType = ClientType.convert(client.getDevicePlatform());
        String requestedClientVersion = client.getAppVersion();
        ClientUpdateType clientUpdateType = Utils.getClientUpdateType(requestedClientType, requestedClientVersion);
        WSClientData clientData = new WSClientData(clientUpdateType, requestedClientVersion);
        return clientData;
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-findClientForSchedulers")
    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate)
    {
        return clientDAO.findClientForSchedulers(deviceStatus, beforeTimeDate);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-deactivateClient")
    public void deactivateClient(List<Client> clientList)
    {
        clientDAO.deactivateClient(clientList);
    }

    @Transactional(rollbackOn = { Exception.class })
    @Profiled(tag = "ClientService-purgeClient")
    public void purgeClient(List<Client> clientList)
    {
        clientDAO.purgeClient(clientList);
    }

    @Profiled(tag = "ClientService-getAccountIdByAuthToken")
    public String getAccountIdByAuthToken(String authToken) throws AuthenticationException
    {
        String clientId = null;
        Validation.isTrue(!Utils.isNullOrEmpty(authToken), "Auth token is null or empty");
        try
        {
            clientId = defaultTokenServices.getClientId(authToken);
        }
        catch (Exception e)
        {
            logger.error("No client authorized for the auth token '" + authToken + "'.", e);
            throw new AuthenticationException(ClientExceptionCode.CLIENT_NOT_FOUND, false, "No client authorized for the auth token '%s'.", e, authToken);
        }
        String accountId = clientDAO.getAccountIdFromClientId(clientId);
        return accountId;
    }
}