package one.group.services.impl;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.services.helpers.SMSConnectionFactory;
import one.group.services.helpers.SimpleExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SMSNetcorePushService
{
    private static final Logger logger = LoggerFactory.getLogger(SMSNetcorePushService.class);

    private SMSConnectionFactory smsNetcoreConnectionFactory;

    private static ExecutorService executorQueue;

    private static BlockingDeque<Runnable> runnableBlockingDeque = new LinkedBlockingDeque<Runnable>();

    static
    {
        executorQueue = Executors.newFixedThreadPool(20);

        for (int i = 0; i < 20; i++)
        {
            executorQueue.submit(new SimpleExecutor(runnableBlockingDeque));
        }
    }

    public SMSConnectionFactory getSmsNetcoreConnectionFactory()
    {
        return smsNetcoreConnectionFactory;
    }

    public void setSmsNetcoreConnectionFactory(SMSConnectionFactory smsNetcoreConnectionFactory)
    {
        this.smsNetcoreConnectionFactory = smsNetcoreConnectionFactory;
    }

    protected void initiateRequest(final String dataToPost)
    {
        try
        {
            if (smsNetcoreConnectionFactory.isHandle())
            {
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        smsNetcoreConnectionFactory.postData(dataToPost);
                    }
                };
                runnableBlockingDeque.addLast(t);
            }
            else
            {
                logger.info("SMS is disabled");
            }
        }
        catch (Exception e)
        {
            logger.error("Error while sending SMS. " + dataToPost, e);
        }

    }

}
