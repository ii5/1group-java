package one.group.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.EntityType;
import one.group.core.enums.MessageType;
import one.group.dao.SubscriptionDAO;
import one.group.entities.cache.SubscriptionKey;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.perf4j.aop.Profiled;

/**
 * Basic implementation for subscription service.
 * 
 * @author nyalfernandes
 * 
 */
public class SubscriptionServiceImpl implements SubscriptionService
{
    private SubscriptionDAO subscriptionDAO;

    @Profiled(tag = "SubscriptionService-retreiveAllSubscribersOf")
    public List<String> retreiveAllSubscribersOf(String subscriptionEntityId, EntityType subscriptionType)
    {
        Set<String> subscriberKeys = subscriptionDAO.fetchSubscribersOf(subscriptionEntityId, subscriptionType);
        List<String> validSubscriberAccounts = new ArrayList<String>();

        for (String subscriberKey : subscriberKeys)
        {
            Map<String, Object> keys = SubscriptionKey.SUBSCRIPTION_KEY.getParsedKey(subscriberKey);
            validSubscriberAccounts.add((String) keys.get("account_id"));
        }

        return validSubscriberAccounts;
    }

    @Profiled(tag = "SubscriptionService-retreiveAllSubscribersOf")
    public List<String> retreiveAllSubscribersOf(EntityType subscriptionType, Collection<String> subscriptionEntityIdCollection)
    {
        List<String> validSubscriberAccounts = new ArrayList<String>();
        Set<String> subscriberKeys = new HashSet<String>();

        if (subscriptionEntityIdCollection != null && subscriptionEntityIdCollection.size() > 0)
        {
            for (String entityId : subscriptionEntityIdCollection)
            {
                subscriberKeys.addAll(subscriptionDAO.fetchSubscribersOf(entityId, subscriptionType));
            }
        }

        for (String subscriberKey : subscriberKeys)
        {
            Map<String, Object> keys = SubscriptionKey.SUBSCRIPTION_KEY.getParsedKey(subscriberKey);
            validSubscriberAccounts.add((String) keys.get("account_id"));
        }

        return validSubscriberAccounts;
    }
    
    @Profiled(tag = "SubscriptionService-retrieveSubscriptionsOf")
    public List<String> retrieveSubscriptionsOf(String subscribingAccountId, EntityType subscriptionType)
    {
        Set<String> subscriptions = subscriptionDAO.fetchAllSubscriptions(subscribingAccountId);
        List<String> subscribedKeys = new ArrayList<String>();
        for (String subscriptionKey : subscriptions)
        {
            Map<String, Object> keys = SubscriptionKey.SUBSCRIPTION_KEY.getParsedKey(subscriptionKey);
            if ( ((EntityType)keys.get("sc_type")).equals(subscriptionType) )
            {
                subscribedKeys.add(keys.get("sc_entity_key").toString());
            }
        }
        
        return subscribedKeys;
    }
    
    @Profiled(tag = "SubscriptionService-subscribe")
    public void subscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType, int forTime)
    {
        subscriptionDAO.addSubscription(subscribingAccountId, subscriptionEntityId, subscriptionType, forTime);
    }

    @Profiled(tag = "SubscriptionService-fetchSubscriptions")
    public Map<MessageType, Map<String, List<String>>> fetchSubscriptions(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null.");

        Set<String> subscriptionKeySet = subscriptionDAO.fetchAllSubscriptions(accountId);
        Map<MessageType, Map<String, List<String>>> subscriptions = new HashMap<MessageType, Map<String, List<String>>>();

        for (String subscriptionKey : subscriptionKeySet)
        {
            Map<String, Object> parsedKey = SubscriptionKey.SUBSCRIPTION_KEY.getParsedKey(subscriptionKey);

            MessageType type = (MessageType) parsedKey.get("sc_type");
            String entityKey = (String) parsedKey.get("sc_entity_key");
            String subscribingAccount = (String) parsedKey.get("account_id");
            insertIfDoesNotExist(type, entityKey, subscribingAccount, subscriptions);
        }

        return subscriptions;
    }

    // private boolean isSubscriptionValid()
    // {
    // return true;
    // }

    // type/sub_id/acc_ids
    private void insertIfDoesNotExist(MessageType type, String subscribingEntityId, String subscribingAccount, Map<MessageType, Map<String, List<String>>> main)
    {
        if (!main.containsKey(type) || main.get(type) == null)
        {
            List<String> accIds = new ArrayList<String>();
            accIds.add(subscribingAccount);

            Map<String, List<String>> subIdVsAcc = new HashMap<String, List<String>>();
            subIdVsAcc.put(subscribingEntityId, accIds);

            main.put(type, subIdVsAcc);

        }
        else
        {
            if (!main.get(type).containsKey(subscribingEntityId) || main.get(type).get(subscribingEntityId) == null)
            {
                List<String> accIds = new ArrayList<String>();
                accIds.add(subscribingAccount);
                main.get(type).put(subscribingEntityId, accIds);
            }
            else
            {
                main.get(type).get(subscribingEntityId).add(subscribingAccount);
            }
        }
    }

    public void subscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        subscriptionDAO.addSubscription(subscribingAccountId, subscriptionEntityId, subscriptionType);
    }

    public boolean isSubscribed(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        String isSubscribeAccount = subscriptionDAO.isSubscribe(subscribingAccountId, subscriptionEntityId, subscriptionType);
        return isSubscribeAccount == null ? false : true;
    }
    
    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType)
    {
        subscriptionDAO.removeSubscription(subscribingAccountId, subscriptionType);
    }
    
    public void removeSubscriptionByKey(String key)
    {
        subscriptionDAO.removeSubscriptionByKey(key);
    }
    
    public SubscriptionDAO getSubscriptionDAO()
    {
        return subscriptionDAO;
    }

    public void setSubscriptionDAO(SubscriptionDAO subscriptionDAO)
    {
        this.subscriptionDAO = subscriptionDAO;
    }

}
