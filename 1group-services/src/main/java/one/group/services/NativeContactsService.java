package one.group.services;

import one.group.entities.api.response.WSInvites;
import one.group.entities.api.response.v2.WSReferralContacts;

public interface NativeContactsService
{
    public WSInvites fetchAllInvitesOfAccount(String accountId);

    public WSReferralContacts fetchReferralsOfAccount(String accountId, int currentPageNo, String pgId);

}
