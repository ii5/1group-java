package one.group.services;


public interface OtpService
{
    public void saveOTP(String otp, String mobileNumber);

    public void deleteOTP(String otp, String mobileNumber);

    public boolean checkAndSaveOtp(String otp, String mobileNumber);
}
