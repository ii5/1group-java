package one.group.services;

import java.util.List;

import one.group.entities.jpa.TargetedMarketingList;
/**
 * 
 * @author sanilshet
 *
 */
public interface TargetedMarketingListService
{
	/**
	 * 
	 * @return
	 */
	public List<TargetedMarketingList> fetchAllTargetedList();
	
	/**
	 * 
	 * @param phoneNumber
	 */
	public void deleteTargetedListByPhoneNumber(String phoneNumber);
	
	public void deleteAllTargetedList();
	
}
