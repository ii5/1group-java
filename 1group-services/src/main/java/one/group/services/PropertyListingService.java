package one.group.services;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.PropertyUpdateActionType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSBookMarkPropertyListings;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSNewsFeed;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.api.response.WSPropertyListingsResult;
import one.group.entities.jpa.AccountRelations;
import one.group.entities.jpa.BookMark;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * 
 * @author sanilshet
 *
 */
public interface PropertyListingService
{
    /**
     * Get property listing of an account
     * 
     * @param accountId
     * @param offset
     * @param limit
     * @return
     * @throws General1GroupException
     */
    public List<WSPropertyListing> getProperyListingOfAccount(String accountId, String accountIdFromToken, String accountDetails, String locationDetails, String matchingDetails, String subscribe)
            throws General1GroupException;

    /**
     * Get property listing count of an account
     * 
     * @param accountId
     */
    public int getPropertyListingCountOfAccount(String accountId, String accountIdFromToken, boolean checkStatus);

    /**
     * Get property listing details by short reference
     * 
     * @param shortReference
     * @return
     */
    public WSPropertyListing getPropertyListingByShortRef(String shortReference);

    /**
     * 
     * @param authToken
     * @param propertyData
     * @param amenities
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     * @throws LocalityNotFoundException
     * @throws General1GroupException
     * @throws ValidationException
     * @throws PropertyTypeNotFoundException
     * @throws PropertySubTypeNotFound
     * @throws CommisionTypeNotFoundException
     * @throws AmenityNotFoundException
     * @throws CityNotFoundException
     */

    /**
     * Function to book mark property listing
     * 
     * @param accountId
     * @param shortReference
     * 
     * @return
     * @throws Exception
     */
    public void bookMarkProperty(String accountId, String propertyListingId, String action) throws Exception;

    /**
     * 
     * @param propertyListingId
     * @param type
     * @return
     */
    public Set<WSBookMarkPropertyListings> getBookMarkedPropertyListings(String authToken, String accountDetails, String locationDetails, String matchingDetails) throws AuthenticationException,
            ClientProcessException, AccountNotFoundException, AccountNotFoundException;

    /**
     * 
     * @param description
     * @param type
     * @param subType
     * @param area
     * @param commisionType
     * @param rooms
     * @param localityId
     * @param amenities
     * @param salePrice
     * @param rentPrice
     * @param status
     * @param propertyMarket
     * @param isHot
     * @param action
     * @param accountId
     * @param propertyListingId
     * @param createdFor
     * @param externalListingTime
     * @param externalSource
     * @param customSublocation
     * @return
     * @throws Abstract1GroupException
     * @throws ParseException
     */
    public WSPropertyListing savePropertyListing(boolean isAdmin, String description, String type, String subType, String area, String commisionType, String rooms, String localityId,
            List<String> amenities, String salePrice, String rentPrice, String status, String propertyMarket, String isHot, PropertyUpdateActionType action, String accountId,
            String propertyListingId, String createdFor, String externalListingTime, String externalSource, String customSublocation) throws Abstract1GroupException, ParseException;

    /**
     * 
     * @param propertyListingId
     * @return
     * @throws General1GroupException
     */
    public WSPropertyListing getPropertyListingByPropertyListingId(String propertyListingId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException;

    /**
     * 
     * @return
     */
    public List<PropertyListing> getActivePropertyListings();

    /**
     * 
     * @return
     */
    public List<PropertyListingScoreObject> getActivePropertyListingScoreObjects();

    /**
     * 
     * @param propertyListingId
     * @return
     * @throws ParseException
     * @throws Abstract1GroupException
     */
    public PropertyListing autoExpirePropertyListing(String propertyListingId) throws ParseException, Abstract1GroupException;

    /**
     * 
     * @return
     */
    public List<PropertyListing> getActiveAndExpiredPropertyListing();

    /**
     * 
     * @param accountId
     * @return
     * @throws General1GroupException
     */
    public Map<String, Integer> getProperyListingOfAccount(String accountId) throws General1GroupException;

    /**
     * 
     * @param propertyListing
     * @param localityId
     * @return
     * @throws SyncLogProcessException
     * @throws General1GroupException
     */
    public List<PropertySearchRelation> savePropertySearchRelation(PropertyListing propertyListing, List<Location> locationMatches) throws SyncLogProcessException, General1GroupException;

    /**
     * 
     * @param wsPropertyListing
     * @return
     * @throws Exception
     */
    public WSPropertyListing appendToSyncLog(WSPropertyListing wsPropertyListing) throws Exception;

    /**
     * 
     * @param accountId
     * @param accountIdFromToken
     * @return
     * @throws General1GroupException
     */
    public List<String> getPropertyListingIdsOfAccount(String accountId, String accountIdFromToken) throws General1GroupException;

    /**
     * 
     * @param accountIdList
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIds(List<String> accountIdList, List<Location> locationList, BroadcastStatus status);

    /**
     * 
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> searchPropertyListing(List<Location> locationList, BroadcastStatus status);

    /**
     * 
     * @param locationIdList
     * @param status
     * @param maxResults
     * @return
     */
    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status, int maxResults);

    /**
     * 
     * @param propertyListing
     * @param accountIdFromAccessToken
     * @param accountDetails
     * @param locationDetails
     * @param matchingDetails
     * @param subscribe
     * @return
     */
    public WSPropertyListing setPropertyDetails(PropertyListing propertyListing, String accountIdFromAccessToken, String accountDetails, String locationDetails, String matchingDetails,
            String subscribe);

    /**
     * Increment property listing Count
     * 
     * @return
     * @throws General1GroupException
     */

    public void incrementPropertyListingCount() throws General1GroupException;

    /**
     * Decrement property listing Count
     * 
     * @return
     * @throws General1GroupException
     */
    public void decrementPropertyListingCount() throws General1GroupException;

    /**
     * Get Active property listing Count
     * 
     * @return
     * @throws General1GroupException
     */

    public WSEntityCount getActivepropertyListingCount() throws General1GroupException;

    /**
     * 
     * @param propertyListing
     * @throws Abstract1GroupException
     */
    public void closePropertyListing(PropertyListing propertyListing) throws Abstract1GroupException;

    public List<PropertyListingScoreObject> searchPropertyListingByExactAndNearBy(String locationId, BroadcastStatus status);

    public List<PropertyListing> getClosedAndExpiredPropertyListing();

    public List<String> fetchPropertyListingIdsByStatus(List<BroadcastStatus> propertyStatus);

    public List<PropertyListing> fetchPropertyListingsByIdsAndStatus(List<String> propertyListingIds, List<BroadcastStatus> propertyStatus);

    public int fetchActivePropertyListingCountOfAccount(String accountId);

    public WSEntityCount getPropertyListingCountByStatus(BroadcastStatus status) throws General1GroupException;

    public WSEntityCount getAllpropertyListingCount() throws General1GroupException;

    /**
     * Fetch all Active property listing of accountList and which are in
     * locationList
     * 
     * @param accountIdList
     * @param locationIdList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllActivePropertyListingsByAccountIdsAndLocationIds(List<String> accountIdList, List<String> locationIdList);

    /**
     * Fetch all active property listings of locations
     * 
     * @param locationIdsList
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllActivePropertyListingByLocationIds(List<String> locationIdsList);

    public List<PropertyListing> fetchPropertyListingsByPropertyListingIds(List<String> propertyListingIds);

    public List<WSNewsFeed> getWSNewsFeedByPropertyListingIds(List<PropertyListing> propertyListing, String accountId, String subscribe, String accountDetails, String locationDetails,
            String matchingDetails, Map<String, BookMark> propertyListingIdVsBookMarkMap, Map<String, AccountRelations> accountVsRelationsMap,
            HashMap<String, WSNewsFeed> propertyListingIdVsWSNewsFeedMap) throws Abstract1GroupException;

    public WSPropertyListingsResult getProperyListingDetailsByPropertyListingIds(List<String> listpropertyListingIds, String accountId, String subscribe, String accountDetails,
            String locationDetails, String matchingDetails) throws Abstract1GroupException;

    public List<String> fetchPropertyListingIdsByPropertyListingIdsAndStatus(List<String> propertyListingIds, BroadcastStatus status) throws General1GroupException;

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status, int maxResults, String accountId);

    public List<PropertyListing> fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(Set<String> propertyListingIds) throws General1GroupException;

    /**
     * Fetch account details associated with property listings
     * 
     * @param propertyListingIds
     * @return
     */
    public Map<String, WSAccount> fetchAccountsAssocitedWithPropertyListingByIds(List<String> propertyListingIds);

    /**
     * Fetch property listing details by ids
     * 
     * @param listpropertyListing
     * @param accountId
     * @param subscribe
     * @param accountDetails
     * @param locationDetails
     * @param matchingDetails
     * @param propertyListingIdVsBookMarkMap
     * @param accountVsRelationsMap
     * @return
     * @throws Abstract1GroupException
     */
    public Map<String, WSPropertyListing> getWSPropertyListingByPropertyListingIds(List<PropertyListing> listpropertyListing, String accountId, String subscribe, String accountDetails,
            String locationDetails, String matchingDetails, Map<String, BookMark> propertyListingIdVsBookMarkMap, Map<String, AccountRelations> accountVsRelationsMap,
            Map<String, WSAccount> propertyListingIdVsAccountMap) throws Abstract1GroupException;

    public Boolean isPropertyListingsExistOfAccount(String accountId);

    public List<PropertyListingScoreObject> fetchAllPropertyListingsForOwnAccountId(String accountId, BroadcastStatus status);

}