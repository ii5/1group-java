package one.group.services;

import java.util.Date;
import java.util.Map;

import one.group.core.enums.EntityType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSClient;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface ApplicationLogger
{
    public void log(WSAccount account, WSClient client, EntityType associatedEntityType, String associatedEntityId, String request, Date requestTime, String response, Date responseTime, String url, HTTPRequestMethodType requestType, Map<String, String> additionalData) throws SyncLogProcessException; 
}
