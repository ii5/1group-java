package one.group.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.WSCity;
import one.group.entities.jpa.City;
import one.group.exceptions.movein.General1GroupException;

public interface CityService
{
    public List<WSCity> getAllCities();

    public City getCityById(String cityId) throws General1GroupException;

    public List<WSCity> getAllCitiesByCityIds(List<String> cityids);

    public List<WSCity> getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public List<WSCity> getAllCities(List<Boolean> isPublishedList);

    public Map<String, WSCity> fetchCityMapById(Set<String> cityIdSet);
}
