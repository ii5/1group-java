package one.group.services;

import one.group.entities.jpa.PropertySearchRelation;

public interface PropertySearchRelationService {

	public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation);
}
