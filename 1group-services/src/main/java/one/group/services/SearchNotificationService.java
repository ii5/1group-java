package one.group.services;

public interface SearchNotificationService
{

    public void saveNotification(String userId);

    public void fetchAndSendNotificationToAll();

    public void schedulerExecution();

    public void sendSearchNotificationByAccountId(String accountId);

}
