package one.group.services;

import java.util.List;

import one.group.entities.jpa.WaMessagesArchive;

/**
 * 
 * @author nyalf
 *
 */
public interface WaArchiveMessageService 
{
	public void saveMessagesToDB(List<WaMessagesArchive> messagesToUpdate);

}
