package one.group.services;

import java.util.List;

import one.group.core.enums.BookMarkType;
import one.group.entities.jpa.BookMark;

public interface BookMarkService
{
    /**
     * 
     * @param referenceId
     * @param type
     * @return
     */
    public List<BookMark> getAllBookMarksByReference(String referenceId, BookMarkType type);
}
