package one.group.services;

import java.util.List;

import one.group.entities.socket.Message;

/**
 * 
 * @author nyalf
 *
 */
public interface MessageService 
{
	public void saveMessagesToDB(List<Message> messagesToUpdate);
}
