package one.group.services;

import java.util.List;

import one.group.entities.jpa.ParentLocations;

public interface ParentLocationService
{
    public List<ParentLocations> getAllLocationsByCityId(String cityId);
}
