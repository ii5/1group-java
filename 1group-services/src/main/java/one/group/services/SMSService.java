package one.group.services;

import java.io.IOException;

/**
 * The Interface SMSService.
 *
 * @author shweta.sankhe
 */
public interface SMSService
{

    /**
     * Send single sms.
     *
     * @param mobileNumber
     *            the mobile number
     * @param otp
     *            the otp
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void sendSingleSMS(final String mobileNumber, final String otp) throws IOException;

    /**
     * Send SMS to mobile number
     * 
     * @param mobileNumber
     * @param smsContent
     * @throws IOException
     */
    public void sendSMS(final String mobileNumber, final String smsContent) throws IOException;

    public void sendSingleSMSPooled(final String mobileNumber, final String otp) throws IOException;

}
