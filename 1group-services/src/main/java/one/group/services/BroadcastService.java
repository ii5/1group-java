package one.group.services;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.PropertyUpdateActionType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.api.request.v2.WSBroadcastTag;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSBroadcastListResult;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.BadRequestException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author ashishthorat
 *
 */
public interface BroadcastService
{
    /**
     * 
     * @param description
     * @param type
     * @param subType
     * @param area
     * @param commisionType
     * @param rooms
     * @param localityId
     * @param amenities
     * @param salePrice
     * @param rentPrice
     * @param status
     * @param propertyMarket
     * @param isHot
     * @param action
     * @param accountId
     * @param propertyListingId
     * @param createdFor
     * @param externalListingTime
     * @param externalSource
     * @param customSublocation
     * @return
     * @throws General1GroupException
     * @throws Abstract1GroupException
     * @throws ParseException
     */
    public WSBroadcast saveBroadcast(boolean isAdmin, String description, String type, String subType, String area, String commisionType, String rooms, String localityId, List<String> amenities,
            String salePrice, String rentPrice, String status, String propertyMarket, String isHot, PropertyUpdateActionType action, String accountId, String broadcastId, String externalListingTime,
            String externalSource, String customSublocation, String broadcastType) throws General1GroupException, ParseException;

    /**
     * 
     * @param broadcastId
     * @return
     * @throws General1GroupException
     */
    public WSBroadcast getBroadcastByBroadcastId(String broadcastId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException;

    /**
     * 
     * @param accountId
     * @return
     * @throws General1GroupException
     */
    public List<WSBroadcast> getBroadcastOfAccount(String accountId) throws General1GroupException;

    /**
     * 
     * @param broadcast
     * @throws Abstract1GroupException
     */
    public void closeBroadcast(PropertyListing propertyListing) throws Abstract1GroupException;

    public Boolean isBroadcastsExistOfAccount(String accountId);

    public WSBroadcastListResult fetchAllBroadcastOfAccount(String accountId, String paginationKey, int pageNo, String refreshPaginationKey) throws JsonMappingException, JsonGenerationException,
            IOException;

    public void starBroadcast(String accountId, String broadcastId) throws Exception;

    /**
     * 
     * @param wsPropertyListing
     * @return
     * @throws Exception
     */
    public WSBroadcast appendToSyncLog(WSBroadcast wsBroadcast) throws Exception;

    public WSBroadcast transitionStatus(String broadcastId, BroadcastStatus status, String accountId) throws General1GroupException, BadRequestException, BroadcastProcessException;

    public void unStarBroadcast(String accountId, String broadcastId) throws General1GroupException, BroadcastProcessException;

    /**
     * 
     * @param broadcastId
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws General1GroupException
     */
    public List<WSMessageResponse> getBroadcastByBroadcastIds(String accountId, List<String> listbroadcastIds) throws Abstract1GroupException, JsonMappingException, JsonGenerationException,
            IOException;

    /**
     * 
     * @param messageId
     * @param accountId
     * @param WsTag
     * @throws SyncLogProcessException
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws BroadcastProcessException
     * @throws AccountNotFoundException
     */
    public WSBroadcast saveBroadcast(String adminId, String broadcastId, String messageId, String accountId, Set<WSBroadcastTag> wsBroadcastTagSet) throws General1GroupException, ParseException,
            SyncLogProcessException, JsonMappingException, JsonGenerationException, IOException, BroadcastProcessException, AccountNotFoundException;

    public long fetchCount();

    public Map<String, Long> fetchAccountAndBroadcastCountMap(List<String> accountIds);

    public WSBroadcast fetchLastStarredBroadcastOfAccount(String accountId);

    public WSBroadcast fetchBroadcastById(String broadcastId);

    public List<WSMessageResponse> getBroadcastDetailsByAccountId(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException;

    public void removeRelationAndCloseBroadcast(String broadcastId, String messageId);

    public List<WSBroadcast> fetchAllStarredBroadcastOfAccount(String accountId, int offset, int limit);

    public void updateBroadcastViewCount(List<String> broadcastIdList);

    public List<String> searchBroadcastsAndCacheItAndUpdateViewCount(String accountId, PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, int cityId,
            Location location, Integer minArea, Integer maxArea, Long maxPrice, Long minPrice, List<Rooms> roomList, PropertySubType propertSubType, int limit, int offset);

    public List<String> fetchBroadcastofAccountFromCache(String accountId, int offset, int limit);

    public List<String> fetchStarredBroadcastoIdsOfAccount(String accountId);

    public void appendToSyncLogForEditRenewBroadcast(WSBroadcast wsBroadcast, String requestedAccountId) throws SyncLogProcessException;

    public void updateBroadcastTags(one.group.entities.api.request.v2.WSBroadcast wsBroadcast) throws Abstract1GroupException;

    public Long fetchBroadcastCountByCity(String cityId);

    public Map<String, Long> fetchAccountAndBroadcastCountMapFromSolr(List<String> accountIds);

    public Map<String, Long> fetchAccountAndBroadcastTagCountMapByStatus(Set<String> accountIds, BroadcastTagStatus broadcastTagStatus, BroadcastStatus broadcastStatus);

    public Map<String, WSBroadcast> fetchBroadcastMapById(List<String> broadcastIdList);
}
