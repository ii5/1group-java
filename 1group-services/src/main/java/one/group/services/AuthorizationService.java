package one.group.services;

import javax.ws.rs.core.UriInfo;

import one.group.entities.api.response.v2.WSClient;
import one.group.exceptions.movein.ClientProcessException;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * The Interface AuthorizationService.
 *
 * @author shweta.sankhe
 *
 */
public interface AuthorizationService
{

    /**
     * Gets the token details.
     *
     * @param clientId
     *            the client id
     * @param otp
     *            the otp
     * @param grantType
     *            the grant type
     * @param uriInfo
     *            the uri info
     * @return the token details
     */
    public OAuth2AccessToken getTokenDetails(String clientId, String otp, String grantType, UriInfo uriInfo);

    /**
     * Creates the oauth client.
     *
     * @param otp
     *            the otp
     * @param grantType
     *            the grant type
     * @param wsClient
     *            the ws client
     * @throws ClientProcessException
     *             the client process exception
     */
    public void createOauthClient(String otp, String grantType, WSClient wsClient, String authorities) throws ClientProcessException;
}
