package one.group.services;

import one.group.entities.api.response.v2.WSPaginationData;

public interface PaginationService
{
    public WSPaginationData getPaginationData(String paginationKey, int pageNo, String refreshPagiantionKey);
}
