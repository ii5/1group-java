package one.group.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.jpa.InviteLog;

/**
 * 
 * @author sanilshet
 *
 */
public interface InviteLogService
{
    /**
     * 
     * @param inviteLog
     */
    public void saveInviteLog(InviteLog inviteLog);

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<String> inviteList);

    public List<String> fetchAllPhoneNumberOfInvitedLogOfAccount(String accountId);

    public void sendInvitationAndUpdateMarketingList(List<String> phoneNumbers, WSAccount wsAccount) throws IOException;

    public void sendInvitation(List<String> phoneNumbers, String notificationTemplate) throws IOException;
}
