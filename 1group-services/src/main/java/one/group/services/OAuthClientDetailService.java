package one.group.services;


public interface OAuthClientDetailService
{
    public void saveClientDetail(String clientId, String resourceId, String clientSecret, String scope, String authorizedGrantTypes, String webServerRedirectUri, String authorities,
            int accessTokenValidity, int refreshTokenValidity, String additionalInformation, String autoApprove);

    public boolean isClientValid(String clientId);
}
