package one.group.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import one.group.core.enums.GroupSource;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.response.v2.WSAccountUtilityDataHolder;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadList;
import one.group.entities.api.response.v2.WSChatThreadListMetaDataResult;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.socket.Groups;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

import org.apache.solr.common.SolrInputDocument;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface GroupsService
{

    public Groups createGroupIfNotExistAndAddMembers(String accountId, String memberOneId, String memberTwoId, GroupSource groupSource);

    public Groups formGroupObject(String chatThreadId, String accountId, List<String> memberIds, GroupSource groupSource);

    public void updateGroups(Groups groups);

    public int updateReadAndReceivedIndex(String groupId, String participantId, int receivedIndex, int readIndex, String receivedMessageId, String readMessageId);

    public Map<String, Map<String, WSChatThreadCursor>> fetchCursorMapForParticipants(List<String> accountIds);

    public Map<List<Groups>, List<GroupMembers>> createDefaultGroupsForAccountAndReturnMembers(String accountId, String cityId, String fullname, String mobileNumber);

    public void createDefaultGroupsForAccount(String accountId, String cityId, String fullname, String mobileNumber);

    public WSChatThreadListMetaDataResult fetchMetaDataChatThread(String accountId) throws JsonMappingException, JsonGenerationException, IOException, General1GroupException;

    public WSAccountUtilityDataHolder fetchAccountMapData(String accountId, List<String> accountIdList);

    public WSMessageResponse formWSMessageResponse(WSMessageResponse wsMessageResponse, WSAccountUtilityDataHolder dataHolder);

    public WSChatThreadCursor updateChatThreadCursor(final String chatThreadId, final String accountId, final int receivedIndex, final int readIndex, final String receivedMessageId,
            final String readMessageId) throws JsonMappingException, JsonGenerationException, Abstract1GroupException;

    public Groups fetchGroupsFromParticipantsAndType(String accountId, List<String> participantsIds, GroupSource groupSource);

    public String fetchGroupsIdFromParticipantsAndType(List<String> participantsIds, GroupSource groupSource);

    public WSChatThread fetchStarredMessageOfAccount(String accountId, int currentPageNo, String pgId) throws JsonMappingException, JsonGenerationException, IOException;

    public WSChatThread fetchDirectChatThreadOfAccount(String accountId, String participantOne, String participantTwo, int pgNum, String pgId) throws JsonMappingException, JsonGenerationException,
            IOException;

    public Groups fetchGroupDetailsFromId(String groupId);

    public WSChatThread fetch1GroupdMessage(String accountId, WSBroadcastSearchQueryRequest wsBroadcastSearchRequest, int currentPageNo, String pgId) throws JsonMappingException,
            JsonGenerationException, IOException, Abstract1GroupException;

    public void deleteGroup(String accountId, String groupId) throws General1GroupException, SyncLogProcessException;

    public List<WSMessageResponse> fetchPhonecallMessage(String accountId) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException;

    public WSChatThread fetchMeMessageOfAccount(String accountId, int currentPageNo, String pgId) throws JsonMappingException, JsonGenerationException, IOException;

    public WSChatThreadList fetchChatThreadListByBroadcast(String accountId, String broadcastId) throws JsonMappingException, JsonGenerationException, IOException;

    public void updateGroupDetails(SolrInputDocument groups);

    public String fetchGroupIdFromParticipants(String participantsOne, String participantsTwo, GroupSource source);

    public List<String> fetchGroupIdListByAccountId(String accountId);

    public List<String> fetchMemberIdsOfGroup(String groupId);

    public void saveGroupMembersBulk(List<GroupMembers> groupMembers);

}
