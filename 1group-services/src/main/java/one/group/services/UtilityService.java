/**
 * 
 */
package one.group.services;

import java.io.File;
import java.io.IOException;

import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author ashishthorat
 *
 */
public interface UtilityService
{
    /**
     * 
     * @param fullPhotoFile
     * @param photoName
     * @param imageType
     * @return
     * @throws General1GroupException
     */
    public String uploadFile(File fullPhotoFile, String photoName, String imageType) throws General1GroupException;
       
}
