package one.group.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.WSCityLocations;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.WSLocation;
import one.group.entities.jpa.Location;
import one.group.exceptions.movein.General1GroupException;

public interface LocationService
{
    public WSCityLocations getLocalities(List<String> listcityIds) throws General1GroupException;

    public List<WSLocation> getLocalitiesByCityId(String cityId);

    public int compareLocations(List<String> cityIds);

    public void updateLiveLocationTable(String cityId);

    public void uploadFile();

    public List<Location> getMatchingAndNearByLocation(Location ofLocation, Boolean isExact);

    public List<Location> getMatchingAndNearByLocation(List<String> ofLocations, Boolean isExact);

    public List<Location> getMatchingLocation(Location ofLocation, Boolean isExact);

    public List<Location> getMatchingLocation(List<String> locationList, Boolean isExact);

    public List<String> getMatchingLocationIds(List<String> locationList, Boolean isExact);

    public Location fetchLocalityNameByLocationId(String locationId);

    public boolean compareParentLocations(List<String> cityIds);

    /**
     * To fetch ids of matching location & near by location of locations
     * 
     * @param ofLocations
     * @param isExact
     * @return
     */
    public List<String> getAllMatchingAndNearByLocationIds(List<String> ofLocations, Boolean isExact);

    public WSCityResult getCities() throws General1GroupException;

    public WSCityResult getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public Map<String, one.group.entities.api.response.v2.WSLocation> fetchLocationMapById(Set<String> locationIdSet);
}
