package one.group.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.AdminStatus;
import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.bpo.WSAdminUserRequest;
import one.group.entities.api.request.bpo.WSGroupMetaDataRequest;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.response.bpo.WSAuthItemResponse;
import one.group.entities.api.response.bpo.WSBroadcastConsolidatedResponse;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.bpo.WSLocationMetaData;
import one.group.entities.api.response.bpo.WSMessageResponse;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserMetaData;
import one.group.entities.api.response.v2.WSBroadcastListResult;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface BPOServices
{
    public List<WSGroupMetaData> fetchGroupMetaDataByStatus(List<GroupStatus> statusList);

    public List<WSLocationMetaData> fetchLocationMetaData();

    // 3 and 4
    public List<WSGroupMetaData> fetchDetailedGroupMetaData(List<GroupStatus> groupStatusList, List<String> groupIdList, int msgCountGreaterThan, int start, int rows, String sortByField,
            String sortByType, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    // 6
    public List<WSLocation> fetchLocationsBasedOnInput(String locationId, String cityId, boolean withCity);

    // 7
    public List<WSLocationMetaData> fetchLocationMetaData(Status status, String cityId, String locationId);

    // 8
    public List<WSMessageResponse> fetchUnreadMessages(int limit, int offset);

    public List<WSGroupMetaData> fetchGroupLabelAndIdByStatus(List<GroupStatus> statusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<WSMessageResponse> fetchReadMessages(int limit, int offset);

    public List<WSGroupMetaData> fetchGroupsByGroupIds(List<String> groupIdList);

    public List<WSLocation> fetchLocationsByLocationIds(List<String> locationIds);

    public List<WSUserMetaData> fetchUsers(String role, boolean onlyActive);

    public WSBroadcastListResult fetchBroadcasts(String cityId, String locationId);

    public List<WSMessageResponse> fetchMessages(List<MessageStatus> messageStatusList, List<GroupStatus> groupStatusList, List<String> groupIdList, List<String> locationIdList,
            List<String> cityIdList, List<BpoMessageStatus> bpoMessageStatusList, String bpoMessageUpdateTime, int start, int rows, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public Map<String, Map<String, String>> fetchLocationsByCity(List<String> cityIdList);

    public List<WSLocationMetaData> fetchLocationsMetaDataByCity(List<String> cityIdList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<WSUserMetaData> fetchUsersByRolesAndStatus(List<String> roleList, List<AdminStatus> adminStatusList, String fieldName, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public List<WSBroadcastConsolidatedResponse> fetchBroadcastsWithTagsByCityAndLocation(List<String> locationIdList, List<String> cityIdList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public List<WSUserCity> fetchByPriorityCityAndAdmin(String priority, String cityId, String adminId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public List<WSUserMetaData> fetchUserByUserNameAndStatus(String username, List<AdminStatus> adminStatusList, String email, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public List<WSGroupMetaData> fetchUnassignedGroupsBasedOnCity(String cityId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public void updateAdminUser(WSAdminUserRequest bpoUserRequest) throws Abstract1GroupException;

    public void updateUserCity(List<WSAdminUserRequest> userRequestList);

    public WSGroupMetaData saveGroup(WSGroupMetaDataRequest groupRequest, boolean createNew, boolean releaseAllGroups) throws Abstract1GroupException;

    public boolean updateMessageStatus(List<String> messageIds, MessageStatus messageStatus, BpoMessageStatus bpoMessageStatus, String updatedBy, String bpoUpdatedBy);

    public WSMessageResponse fetchMessageWithBroadcastDetails(String messageId) throws Abstract1GroupException;

    public List<WSUserCity> fetchUnassignedCityAdmin();

    public WSMessageResponse fetchMessageCountsByGroup(String groupId);
    
    public WSAuthItemResponse fetchAuthorisationForUser(String userId);
    
    public List<WSGroupMetaData> fetchGroupsOfMember(String mobileNumber);
    
    public Map<String, Set<WSMessage>> saveMessages(Map<String, Set<WSMessage>> postMessages) throws Abstract1GroupException;

	public void updateGroupsUnreadFrom(SocketEntity socketEntity) throws Exception;

	public void updateMessageMetaData(SocketEntity socketEntity) throws Abstract1GroupException;

	public void markEmptyGroups();

	public void archiveMessages(SocketEntity socketEntity) throws Abstract1GroupException;
}
