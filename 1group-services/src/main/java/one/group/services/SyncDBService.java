package one.group.services;

import java.util.List;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface SyncDBService
{
    public void deleteClient(String clientId);

    public int createClient(String clientId) throws ClientProcessException;

    public void appendUpdate(String accountId, WSSyncUpdate syncUpdate);

    public void appendUpdates(String accountId, List<ResponseEntity> syncUpdates);

    public int advanceReadCursor(String accountId, String clientId, int requestedCursorIndex) throws SyncLogProcessException;

    public WSSyncResult readUpdates(String accountId, String clientId, int maxResults) throws SyncLogProcessException;

    public void deleteEarlierUpdatesOfAccount(int index, String accountId);
}
