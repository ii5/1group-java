package one.group.services;

import java.util.Set;

import one.group.core.enums.SyncActionType;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSMatchingPropertyListing;
import one.group.entities.api.response.v2.WSBroadcastSearchQuery;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * 
 * @author miteshchavda
 *
 */
public interface SearchService
{
    /**
     * Save search
     * 
     * @param searchId
     * @param accountId
     * @param request
     * @return
     * @throws General1GroupException
     * @throws SyncLogProcessException
     */
    public WSBroadcastSearchQueryRequest saveSearch(String searchId, String accountId, WSRequest request) throws General1GroupException, SyncLogProcessException;

    /**
     * Is search with same client name exist
     * 
     * @param clientName
     * @param accountId
     */
    public boolean checkUniqueClientSearch(String clientName, String accountId);

    /**
     * Check if search belong to an account
     * 
     * @param searchId
     * @param accountId
     * @return
     */
    public boolean checkSearchOwnership(String searchId, String accountId);

    /**
     * Search property listing by search query
     * 
     * @param wsSearchQuery
     * @param accountId
     * @return
     * @throws Abstract1GroupException
     */
    public Set<WSMatchingPropertyListing> searchPropertyListingBySearchQuery(WSBroadcastSearchQueryRequest wsSearchQueryRequest, String accountId, String propertyDetails, String accountDetails,
            String locationDetails, String matchDetails, String subscribe) throws Abstract1GroupException;

    /**
     * Push search data to sync log
     * 
     * @param request
     * @param actionType
     * @throws SyncLogProcessException
     */
    public void pushSearchToSyncLog(WSRequest request, SyncActionType actionType) throws SyncLogProcessException;

    /**
     * 
     * @param searchId
     * @param accountId
     * @return
     * @throws General1GroupException
     * @throws AccountNotFoundException
     */
    public WSBroadcastSearchQueryRequest getSearch(String searchId, String accountId) throws General1GroupException, AccountNotFoundException;

    public WSBroadcastSearchQuery getSearchOfAccount(String accountId) throws General1GroupException;

    public WSBroadcastSearchQueryRequest updateSearchForAccount(String accountId, WSBroadcastSearchQueryRequest queryRequest) throws General1GroupException, SyncLogProcessException;
}
