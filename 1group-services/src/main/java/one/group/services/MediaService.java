package one.group.services;

import one.group.core.enums.MediaType;
import one.group.entities.jpa.PropertyListing;

public interface MediaService
{
    public void addMedia(String mediaName, MediaType type, String url, PropertyListing propertyListing);
}
