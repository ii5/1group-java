package one.group.services;

import java.util.Date;
import java.util.List;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSClient;
import one.group.entities.api.response.v2.WSClientData;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;

/**
 * TODO: Document properly.
 *
 * @author miteshchavda
 */

public interface ClientService
{

    /**
     * Fetch list of all clients of an account.
     *
     * @param accountId
     *            the account id
     * @return the list
     */
    public List<Client> fetchAllClientsOfAccount(String accountId);

    /**
     * Fetch all push channels of clients of account.
     *
     * @param accountId
     *            the account id
     * @return the list
     */
    public List<String> fetchAllPushChannelsOfClientsOfAccount(String accountId);

    /**
     * Fetch client from otp and mobile.
     *
     * @param otp
     *            the otp
     * @param mobileNumber
     *            the mobile number
     * @throws General1GroupException
     *             the general move in exception
     */
    public void checkValidOtpAndDeleteIfValid(String otp, String mobileNumber) throws General1GroupException;

    /**
     * Save client.
     *
     * @param account
     *            the against account id
     * @param appName
     *            the app name
     * @param appVersion
     *            the app version
     * @param deviceModelName
     *            the deviceModelName
     * @param deviceToken
     *            the device token
     * @param deviceVersion
     *            the device version
     * @param isOnline
     *            the is online
     * @param pushChannel
     *            the push channel
     * @param pushAlert
     *            the push alert
     * @param pushBadge
     *            the push badge
     * @param pushSound
     *            the push sound
     * @param status
     *            the status
     * @param devicePlatform
     *            the device platform
     * @param mobileNumber
     * @paras cpsId client push service id
     * @return the WS client
     */
    public WSClient saveClient(Account account, String appName, String appVersion, String deviceModelName, String deviceToken, String deviceVersion, boolean isOnline, String pushChannel,
            String pushAlert, String pushBadge, String pushSound, DeviceStatus status, String devicePlatform, String mobileNumber, String cpsId);

    /**
     * Check if active client.
     *
     * @param clientId
     *            the client id
     * @return true, if successful
     */
    public boolean checkIfActiveClient(String clientId);

    /**
     * Fetch client by id.
     *
     * @param clientId
     *            the client id
     * @return the client
     */
    public Client fetchClientById(String clientId);

    /**
     * Fetch client by push channel.
     *
     * @param pushChannel
     *            the push channel
     * @return the WS client
     */
    public WSClient fetchClientByPushChannel(String pushChannel);

    /**
     * Gets the client id by auth token.
     *
     * @param authToken
     *            the auth token
     * @return the client id by auth token
     * @throws AuthenticationException
     *             the authentication exception
     */
    public String getClientIdByAuthToken(String authToken) throws AuthenticationException;

    /**
     * 
     * @param authToken
     * @return
     * @throws AuthenticationException
     */
    public one.group.entities.api.request.WSClient getClientByAuthToken(String authToken) throws AuthenticationException;

    /**
     * Gets the account from auth token.
     *
     * @param authToken
     *            the auth token
     * @return the account from auth token
     * @throws AuthenticationException
     *             the authentication exception
     * @throws ClientProcessException
     *             the client process exception
     * @throws AccountNotFoundException
     *             the account not found exception
     */
    public Account getAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;
    
    public WSAccount getWSAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * Fetch by account and device platform.
     *
     * @param accountId
     *            the account id
     * @param devicePlatform
     *            the device platform
     * @return the list
     */
    public List<String> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform);

    public List<Client> fetchAllActiveClientsOfAccount(String accountId);

    public List<Client> fetchAllActiveClients();

    public WSClientData updateClientVersion(String authToken, one.group.entities.api.request.v2.WSClientData wsClientData) throws AuthenticationException;

    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate);

    public void deactivateClient(List<Client> clientList);

    public void purgeClient(List<Client> clientList);

    public String getAccountIdByAuthToken(String authToken) throws AuthenticationException;

}