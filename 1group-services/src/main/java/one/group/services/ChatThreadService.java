package one.group.services;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadReference;
import one.group.exceptions.movein.ChatProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.utils.validation.exception.ValidationException;

public interface ChatThreadService
{
    /**
     * Fetch chat thread of an account
     * 
     * @param accountId
     * @param indexBy
     * @param offset
     * @param limit
     * @return
     * @throws ChatProcessException
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    public List<WSChatThreadReference> fetchChatThreadOfAccount(String accountId, String indexBy, int offset, int limit) throws ChatProcessException, JsonMappingException, JsonGenerationException,
            IOException;

    /**
     * Fetch chat thread details
     * 
     * @param accountId
     * @param chatThreadId
     * @param offset
     * @param limit
     * @return
     * @throws ChatProcessException
     * @throws General1GroupException
     */
    public WSChatThread fetchChatThreadDetails(String accountId, String chatThreadId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException, ChatProcessException,
            General1GroupException;

    /**
     * Update chat thread cursor's values
     * 
     * @param chatThreadId
     * @param accountId
     * @param readIndex
     * @param receiveIndex
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws ValidationException
     * @throws SyncLogProcessException
     */
    public WSChatThreadCursor updateChatThreadCursor(String chatThreadId, String accountId, int readIndex, int receiveIndex) throws ValidationException, JsonMappingException, JsonGenerationException,
            IOException, SyncLogProcessException;

    /**
     * Fetch max index of chat thread list
     * 
     * @param accountId
     * @param indexBy
     * @return
     */
    public Integer getMaxChatThreadIndex(String accountId, String indexBy);

    /**
     * Fetch chat threads of an account which reference of Property listing
     * 
     * @param accountId
     * @param propertyListingId
     * @param indexBy
     * @param offset
     * @param limit
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    public List<WSChatThreadReference> fetchChatThreadsOfAccountByProperty(String accountId, String propertyListingId, String indexBy, long offset, long limit) throws JsonMappingException,
            JsonGenerationException, IOException;

    /**
     * Create copy chat thread for new property listing
     * 
     * @param propertyListingIdOld
     * @param propertyListingIdNew
     */
    public void migrateChatThreadsOfBroadcast(String propertyListingIdOld, String propertyListingIdNew);

    /**
     * Remove subscription of chat thread
     * 
     * @param fromAccountId
     * @param toAccountId
     */
    public void removeChatThreadSubscription(String fromAccountId, String toAccountId);

    /**
     * Add subscription of chat thread
     * 
     * @param fromAccountId
     * @param toAccountId
     */
    public void addChatThreadSubscription(String fromAccountId, String toAccountId);

    public List<WSChatThreadReference> fetchChatThreadOfAccountDummy(final String accountId, final String indexBy, final int offset, final int limit) throws ChatProcessException,
            JsonMappingException, JsonGenerationException, IOException;
}
