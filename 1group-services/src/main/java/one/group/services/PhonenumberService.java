package one.group.services;

import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.entities.api.response.WSPhoneNumber;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface PhonenumberService
{
    public void savePhoneNumber(String mobileNumber, PhoneNumberType type, PhoneNumberSource source);

    public WSPhoneNumber fetchPhoneNumber(String mobileNumber, boolean populateAssociatedAccount);
}
