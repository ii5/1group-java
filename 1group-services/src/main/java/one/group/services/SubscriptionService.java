package one.group.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import one.group.core.enums.EntityType;
import one.group.core.enums.MessageType;

/**
 * Subscription services.
 * 
 * @author nyalfernandes
 *
 */
public interface SubscriptionService
{
    public List<String> retreiveAllSubscribersOf(String subscriptionEntityId, EntityType subscriptionType);
    
    public List<String> retreiveAllSubscribersOf(EntityType subscriptionType, Collection<String> subscriptionEntityIdCollection);

    public void subscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType, int forTime);

    public Map<MessageType, Map<String, List<String>>> fetchSubscriptions(String accountId);

    public void subscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType);

    public boolean isSubscribed(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType);
    
    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType);
    
    public void removeSubscriptionByKey(String key);
    
    public List<String> retrieveSubscriptionsOf(String subscribingAccountId, EntityType subscriptionType);
}
