package one.group.services;

import java.security.NoSuchAlgorithmException;

import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;

public interface SyncGroupService
{

    public void saveCount(String count);

    public String fetchGroupContent(String groupkey);

    public String getInviteCode(String token) throws AuthenticationException, ClientProcessException, AccountNotFoundException, NoSuchAlgorithmException;

    public boolean isFullRelease();

}
