package one.group.services;

import java.util.List;

import one.group.core.enums.status.NotificationStatus;
import one.group.entities.api.response.v2.WSReferralTemplates;

public interface NotificationService
{
    public long fetchCountByStatus(String userId, NotificationStatus status);

    public List<WSReferralTemplates> fetchAllNotificationTemplates();

}
