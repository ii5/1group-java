package one.group.services;

import java.util.List;

import one.group.core.message.Message;

/**
 * Contracts that chat service provides.
 * 
 * @author nyalfernandes
 * 
 */
public interface ChatService
{
    public List<String> getChatThreads(String accountId, int sinceIndex, int beforeIndex, int limit);

    /**
     * Sends a chat message from one account to another. The flow of the send
     * chat message is as follows
     * 
     * <ol>
     * <li>Validate if the originatingClientId is valid.</li>
     * <li>Fetch and Validate the account to which the originatingClientId
     * belongs.</li>
     * <li>Identify the active clients of the target Account.</li>
     * <li>Build Chat message to be delivered.</li>
     * <li>Build destinations for each chat message, the destination should have
     * the targetClientId and the targetClientType.</li>
     * <li>Enqueue the message into the notification queue for the originating
     * account and the target account.</li>
     * <li>Push the message to the target client through the pusher service.</li>
     * </ol>
     * 
     * @param fromAccountId
     * @param toAccountId
     * @param message
     */
    public void sendChatMessage(String fromAccountId, String toAccountId, Message message);

}
