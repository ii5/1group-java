package one.group.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.WSMarketingList;
import one.group.entities.jpa.MarketingList;
import one.group.entities.jpa.TargetedMarketingList;

/**
 * 
 * @author sanilshet
 *
 */
public interface MarketingListService
{

    /**
     * 
     * @param phoneNumber
     * @return
     */
    public MarketingList checkPhoneNumberExists(String phoneNumber);

    public void checkAndUpsertAll(List<String> phoneNumberList) throws IOException;

    public List<MarketingList> fetchAllMarketingListData();

    public void updateMarketingList(List<TargetedMarketingList> targetedMarketingList);

    public void checkAndSaveInvitedPhoneNumbers(String phoneNumber);

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<MarketingList> marketingList);

    public List<MarketingList> fetchAllMarketingListWhichAreNativeContacts(String accountId);

    public HashSet<WSMarketingList> fetchMarketingListByPhoneNumberList(List<String> phoneNumbers);
}
