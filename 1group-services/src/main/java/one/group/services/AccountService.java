package one.group.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.AccountType;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSPhoto;
import one.group.entities.api.response.WSAccountStatusResult;
import one.group.entities.api.response.WSChatNotificationTMS;
import one.group.entities.api.response.WSHeartBeat;
import one.group.entities.api.response.WSInviteContact;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountListResultAdmin;
import one.group.entities.jpa.Account;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface AccountService
{

    /**
     * funciton that marks an account online.
     * 
     * @param accountId
     */
    public void markOnline(String accountId);

    /**
     * function that marks an account offline.
     * 
     * @param accountId
     */
    public void markOffline(String accountId);

    /**
     * 
     * @param mobileNumber
     * @return
     * @throws General1GroupException
     */
    public WSAccount fetchAccountByMobileNumber(String mobileNumber);

    /**
     * 
     * @param httpHeaders
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public WSAccount fetchActiveAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * 
     * @param authToken
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public WSAccount fetchAccountFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * Fetch account without locations
     * 
     * @param authToken
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public WSAccount fetchAccountWithOutLocationsFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * Fetch accountId from auth token
     * 
     * @param authToken
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public String fetchAccountIdFromAuthToken(String authToken) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * 
     * @return
     */
    public List<WSAccount> fetchAllAccounts();

    /**
     * To add or update account information
     * 
     * @param accountId
     * @param type
     * @param status
     * @param primaryPhoneNumber
     * @param address
     * @param email
     * @param firstname
     * @param lastname
     * @param establishmentName
     * @param cityId
     * @param fullPhoto
     * @param thumbnailPhoto
     * @param registrationTime
     * @return
     * @throws Abstract1GroupException
     */
    public WSAccount saveOrUpdateAccount(String accountId, AccountType type, Status status, String primaryPhoneNumber, String address, String email, String firstname, String lastname,
            String establishmentName, String cityId, WSPhoto fullPhoto, WSPhoto thumbnailPhoto, Date registrationTime) throws Abstract1GroupException;

    /**
     * Create account if not exist
     * 
     * @param type
     * @param status
     * @param primaryPhoneNumber
     * @param address
     * @param email
     * @param firstname
     * @param lastname
     * @param establishmentName
     * @param localityList
     * @param fullPhoto
     * @param thumbnailPhoto
     * @return
     * @throws Abstract1GroupException
     */
    public WSAccount saveIfNotExist(AccountType type, Status status, String primaryPhoneNumber, String address, String email, String firstname, String lastname, String establishmentName,
            List<String> localityList, WSPhoto fullPhoto, WSPhoto thumbnailPhoto) throws Abstract1GroupException;

    public Account saveIfNotExist(AccountType type, Status status, String mobileNumber) throws Abstract1GroupException;

    public WSAccount updateAccountProfilePhoto(String authToken, String mediaName, MediaType type, MediaDimensionType dimenstion, String url, int height, int width, int size)
            throws Abstract1GroupException;

    /**
     * Recognizes the originating account's heartbeat and returns the allianced
     * accounts online status.
     * 
     * @param authToken
     * @param requestedAccountIds
     * @return
     */
    public WSHeartBeat hearTheBeat(String authToken, List<String> requestedAccountId) throws AuthenticationException, ClientProcessException, AccountNotFoundException;

    /**
     * Fetch account details
     * 
     * @param accountId
     * @return
     */
    public WSAccount fetchAccountDetails(String accountId);

    /**
     * 
     * @param accountId
     * @return
     */
    public WSAccount fetchActiveAccountDetails(String accountId) throws AccountNotFoundException;

    /**
     * 
     * @param authToken
     * @param requestedAccountIds
     * @return
     */
    public WSAccountStatusResult fetchAccountStatus(String authToken, Collection<String> requestedAccountIds) throws Abstract1GroupException;

    public void removeProfilePic(String entityId, String entityType, List<String> url, String cdnImgFolder, String cdnImageURL);

    public WSAccount getAccountDetailsForAdminByMobileNumber(String mobileNumber);

    public WSAccount fetchAccountByShortReference(String shortReference);

    /**
     * 
     * @return
     */
    public List<String> fetchAllAccountToSendChatNotification();

    /**
     * 
     * @param accountKey
     * @return
     */
    public WSChatNotificationTMS fetchChatSmsNotificationTimestamp(String accountKey);

    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp);

    public long fetchCount();

    public void incrementCount();

    /**
     * Fetch all active accounts
     * 
     * @return
     */
    public List<WSAccount> fetchAllActiveAccounts();

    public String fetchAccountInviteCode(String accountId);

    public void saveInviteCodeForAccount(String accountId, String inviteCode);

    public List<Account> fetchAllSeededAccounts();

    public void updateSeededAccountShortReference(Account account);

    public Map<String, WSInviteContact> getMapOfPhoneNumberAndStatus(List<Account> accounts);

    public List<Account> fetchAccountOfNativeContactsOfAccount(String accountId);

    /**
     * Fetch account details for mobile numbers requested by admin
     * 
     * @param mobileNumbers
     * @return
     */
    public WSAccountListResultAdmin getAccountListByMobileNumbersForAdmin(Set<String> mobileNumbers);

    /**
     * Update registration time for an account
     * 
     * @param accountId
     * @param registrationTime
     * @return
     */
    public boolean ifNullOnlyUpdateRegistrationTimeOfAccount(String accountId, Date registrationTime);

    /**
     * Fetch all Account Ids of a location.
     * 
     * @param locationIds
     * @return
     */
    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds);

    public Set<String> fetchIdsOfAllActiveAccount();

    public void updateShortReferenceForUnRegisteredAccountByAdmin(String mobileNumber);

    public Map<String, WSAccount> fetchAccountDetailsInBulk(Set<String> accountIdSet);

    public Set<String> fetchPhoneNumbersOfNativeContactsOfAccount(String accountId);

    public WSAccount fetchAccountByAccountId(String accountId);

    public WSAccount fetchAccountWithOutLocationsFromId(String accountId);

    public Map<String, String> fetchLastActivityTimeByAccountIdInBulk(Set<String> accountIdList);
}
