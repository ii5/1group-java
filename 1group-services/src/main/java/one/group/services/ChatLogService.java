/**
 * 
 */
package one.group.services;

import one.group.entities.api.response.WSChatMessage;

/**
 * @author ashishthorat
 *
 */
public interface ChatLogService
{
    public void saveChatLog(WSChatMessage wsChatMessage, String toAccountId);
}
