package one.group.services;

import java.util.List;

import one.group.entities.api.request.v2.WSClientAnalytics;
import one.group.entities.api.response.WSClientAnalyticsResponse;

public interface ClientAnalyticsService
{
    /**
     * 
     * @param wsAnalytics
     * @param accountId
     * @param clientId
     */
    public void saveTraces(List<WSClientAnalytics> wsClientAnalytics, String accountId, String clientId);

    /**
     * 
     * @param accountId
     */
    public List<WSClientAnalyticsResponse> fetchTracesByAccountId(String accountId);
}
