package one.group.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.request.WSContactsInfo;
import one.group.entities.api.request.v2.WSContacts;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.jpa.AccountRelations;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * 
 * 
 */
public interface AccountRelationService
{

    public Set<WSAccountRelations> fetchContactsOfAccount(String accountId);

    public Set<WSAccount> fetchContactsOfAccountDetail(String accountId) throws AccountNotFoundException;

    // public WSContactsInfo syncContactsForAccount(String accountId, WSContacts
    // contacts) throws Abstract1GroupException, IOException;

    // public Set<String> syncContactsForAccount(String accountId) throws
    // Abstract1GroupException;

    public WSContactsInfo syncContactsForAccountBulk(String accountId, WSContacts contacts) throws Abstract1GroupException;

    public Set<String> syncContactsForAccountBulk(String accountId) throws Abstract1GroupException;

    public void createOrUpdateAccountRelationBlock(String requestByAccountId, String otherAccountId) throws SyncLogProcessException;

    // public void createUpdateAccountRelation(Account account, Account
    // otherAccount, Boolean isContact, Boolean isContactOf, Boolean isBlocked,
    // Boolean isBlockedBy, Integer score, Integer scoredAs);

    public void updateAccountRelationUnblock(String requestByAccountId, String otherAccountId) throws General1GroupException, SyncLogProcessException;

    public void updateAccountRelationScore(String requestByAccountId, String otherAccountId, int score) throws General1GroupException, SyncLogProcessException;

    public WSAccountRelations fetchAccountRelation(String accountId, String otherAccountId);

    public void pushHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType, String targetAccountId) throws SyncLogProcessException;

    public void pushSyncHintToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType actionType, WSContactsInfo contactsInfo) throws SyncLogProcessException;

    public void pushToSyncLog(HintType hintType, EntityType entityType, String accountId, SyncActionType syncActionType, WSContactsInfo contactsInfo) throws SyncLogProcessException;

    /**
     * function that store accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void createAccountBlockByKey(String accountId, String requestByAccountId);

    /**
     * function that delete accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void removeAccountFromBlockedBy(String accountId, String requestByAccountId);

    /**
     * 
     * @param accountId
     * @return
     */
    public Map<String, WSAccountRelations> fetchAccountRelationsAsMap(String accountId);

    /**
     * 
     * @param accountId
     * @return
     */
    public Map<String, AccountRelations> fetchRawAccountRelationsAsMap(String accountId);

    /**
     * 
     * @param accountId
     * @param otherAccountIdList
     * @return
     */
    public Map<String, WSAccountRelations> fetchAccountRelationsAsMap(String accountId, List<String> otherAccountIdList);

    /**
     * 
     * @param accountId
     * @param otherAccountIdList
     * @return
     */
    public Map<String, AccountRelations> fetchRawAccountRelationsAsMap(String accountId, List<String> otherAccountIdList);

    /**
     * To check if requestByAccountId has blocked otherAccountId or
     * otherAccountId has blocked requestByAccountId.
     * 
     * it works both way
     * 
     * @param accountId
     * @param otherAccountId
     * @return
     */
    public boolean isAccountBlocked(String requestByAccountId, String otherAccountId);

    /**
     * 
     * @param accountId
     * @param otherAccountIdList
     * @return
     */
    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList);

    /**
     * 
     * @param accountId
     * @return
     */
    public Set<WSAccountRelations> fetchAccountRelations(String accountId);

}