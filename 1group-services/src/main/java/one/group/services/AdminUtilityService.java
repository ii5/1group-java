package one.group.services;

import java.util.Map;
import java.util.Set;

import one.group.core.enums.AccountFieldType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.WSAdminSMSRequest;
import one.group.entities.api.response.WSReverseContacts;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAdminGroupMetaDataList;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.General1GroupException;

public interface AdminUtilityService
{

    /**
     * Adds the mobile number and account and client.
     * 
     * @param mobileNumber
     *            the mobile number
     * @param deviceId
     *            the device id
     * @param deviceOsVersion
     *            the device os version
     * @param devicePlatform
     *            the device platform
     * @param otp
     *            the otp
     * @param grantType
     *            the grant type
     * @return the WS client
     * @throws Abstract1GroupException
     *             the abstract move in exception
     */
    public Map<String, Object> addMobileNumberAccountClient(final String mobileNumber, final String deviceId, final String deviceOsVersion, final String devicePlatform, final String otp,
            final String grantType, final String firstName, final String lastName, final String cityId, final Status status, final boolean isCreatingAdminRole, String cpsId)
            throws Abstract1GroupException;

    public String generateAdminOauthToken();

    public void updateAccountByPhoneNumber(WSAccount account, AccountFieldType fieldType, String[] fieldValues) throws General1GroupException;

    public boolean isImageUrlValid(String imageUrl);

    public void updatePropertyListings(String propertyListingIds) throws BroadcastProcessException, General1GroupException, Exception;

    public WSReverseContacts getAccountDetailsFromNativeContactsByPhoneNumber(String phoneNumber) throws General1GroupException;

    public void sendSMSToPhoneNumber(WSAdminSMSRequest sendSms) throws General1GroupException;

    public boolean updateCityIdOfGroups(String cityId, String mobileNumber) throws General1GroupException, Exception;

    public WSAdminGroupMetaDataList getGroupMetaDataByPhoneNumber(Set<String> phoneNumberSet, String everSync);
}
