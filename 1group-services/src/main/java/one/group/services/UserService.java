package one.group.services;

import one.group.core.enums.AccountType;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.entities.jpa.User;
import one.group.exceptions.movein.Abstract1GroupException;

public interface UserService
{
    public User saveUser(String userId, String fullName, String createdBy, String shortReferece, String email, String cityId, AccountType accountType, UserSource userSource, GroupSelectionStatus groupSelectionStatus, SyncStatus syncStatus, String mobileNumber, String localStorage, boolean skipIfExist) throws Abstract1GroupException;
}
