package one.group.services;

import java.util.List;

import one.group.entities.api.response.WSLocation;

public interface LocationAdminService
{
    public List<WSLocation> getLocalitiesByCityId(String cityId);
}
