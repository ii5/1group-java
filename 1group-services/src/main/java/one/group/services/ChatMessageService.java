package one.group.services;

import java.io.IOException;
import java.util.List;

import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.v2.WSChatCursor;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSSharedItem;
import one.group.entities.socket.Message;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.SyncLogProcessException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * TODO Document properly
 * 
 * @author miteshchavda
 * 
 */
public interface ChatMessageService
{
    /**
     * Fetch chat messages of chat thread
     * 
     * @param chatThreadId
     * @param offset
     * @param limit
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    public List<WSChatMessage> fetchChatMessagesOfChatThread(String chatThreadId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException;

    /**
     * Fetch chat message content
     * 
     * @param chatThreadId
     * @param chatMessageKey
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    public WSChatMessage fetchChatMessageContent(String chatThreadId, String chatMessageKey) throws JsonMappingException, JsonGenerationException, IOException;

    /**
     * Push chat message to sync log
     * 
     * @param wsMessage
     * @param requestHash
     */
    public void pushChatMessageToSyncLog(WSMessageResponse wsMessage, String requestHash) throws SyncLogProcessException;

    /**
     * Push chat cursor to sync log
     * 
     * @param wsChatCursor
     * @param clientId
     */
    public void pushChatCursorToSyncLog(WSChatCursor wsChatCursor, String originClientId) throws SyncLogProcessException;

    /**
     * Save message
     * 
     * @param wsMessage
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws Abstract1GroupException
     */
    public WSMessageResponse saveChatMessage(one.group.entities.api.request.v2.WSMessage wsMessage) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException;

    /**
     * Fetch last chat message of chat thread
     * 
     * @param chatThreadId
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */

    public List<Message> fetchPhonecallMessages(String accountId) throws JsonMappingException, JsonGenerationException, IOException;

    public WSMessageResponse fetchLastMessageOfGroup(String groupId) throws JsonMappingException, JsonGenerationException, IOException;

    public WSMessageResponse fetchLastBroadcastMessageOfMe(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException;

    public WSMessageResponse fetchLastStarredMessageOfAccount(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException;

    public WSSharedItem fetchSharedItemsOfChatThread(String fromAccountId, String chatThreadId) throws JsonMappingException, JsonGenerationException, IOException;

    public List<Message> fetchMeMessagesOfAccount(String accountId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException;

    public WSMessageResponse editChatMessage(final String messageId, final WSMessage wsMessage) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException;

    public List<WSMessageResponse> fetchStarredBroadcastOfAccount(String accountId, int offset, int limit) throws JsonMappingException, JsonGenerationException, IOException;

    public List<WSMessageResponse> fetchMessageOfChatThreadBySourceAndType(String chatThreadId, MessageSourceType messageSoureType, List<MessageType> messageTypeList, int offset, int limit)
            throws JsonMappingException, JsonGenerationException, IOException;

    public List<String> fetchUniqueMessageByBroadcastIdandAccountId(String accountId, String broadcastId);

    public List<WSMessageResponse> fetchListOfMessages(String fromAccountId, List<String> messageIds) throws JsonMappingException, JsonGenerationException, IOException;

    public List<WSMessageResponse> fetchMessageFromBroadcastIds(List<String> broadcastIdList) throws JsonMappingException, JsonGenerationException, IOException;

    public void appendToSyncLogAsEntityReference(String accountId, String messageId, SyncActionType syncActionType) throws SyncLogProcessException;

    public WSMessageResponse fetchLastBroadcastMessageOfOneGroup(String accountId) throws General1GroupException, JsonMappingException, JsonGenerationException, IOException;

    public long getTotalMessageOfAccountOfMeGroup(String accountId);

    public List<Message> fetchMessageDetails(List<String> messageIdList);

    public WSMessageResponse fetchLastMessageOfGroupAllType(String groupId) throws JsonMappingException, JsonGenerationException, IOException;
}
