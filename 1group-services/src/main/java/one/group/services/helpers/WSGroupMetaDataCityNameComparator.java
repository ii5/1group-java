package one.group.services.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.lucene.search.Sort;

import one.group.core.enums.SortType;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.v2.WSCity;

/**
 * 
 * @author nyalf
 *
 */
public class WSGroupMetaDataCityNameComparator implements Comparator<WSGroupMetaData>
{
	private SortType type;
	
	public WSGroupMetaDataCityNameComparator(SortType type)
	{
		if (type == null)
		{
			this.type = SortType.ASCENDING;
		}
		else
		{
			this.type = type;
		}
		
	}

	public int compare(WSGroupMetaData o1, WSGroupMetaData o2) 
	{
		int val = 1;
		if (o1.getCity() == null)
		{
			if (type.equals(SortType.ASCENDING))
			{
				val = 1;
			}
			else
			{
				val = -1;
			}
		}
		
		if (o2.getCity() == null)
		{
			if (type.equals(SortType.ASCENDING))
			{
				val = -1;
			}
			else
			{
				val = 1;
			}
		}
		
		if (o1.getCity() != null && o2.getCity() != null)
		{
			String cityName1 = o1.getCity().getName();
			String cityName2 = o2.getCity().getName();
			
			if (cityName1 == null)
			{
				if (type.equals(SortType.ASCENDING))
				{
					val = 1;
				}
				else
				{
					val = -1;
				}
			}
			
			if (cityName2 == null)
			{
				if (type.equals(SortType.ASCENDING))
				{
					val = -1;
				}
				else
				{
					val = 1;
				}
			}
			
			if (cityName1.equals(cityName2))
			{
				if (type.equals(SortType.ASCENDING))
				{
					val = 1;
				}
				else
				{
					val = -1;
				}
			}
			
			if (type.equals(SortType.ASCENDING))
			{
				val = cityName1.compareTo(cityName2);
			}
			else
			{
				val = cityName2.compareTo(cityName1);
			}
		
		
		}
		
//		System.out.println(val + " : " +o1.getCity() + " : " + o2.getCity());
		return val;
	}
	
	
	public static void main(String[] args)
	{
		WSCity c1 = new WSCity();
		c1.setName("Mumbai");
		
		WSCity c2 = new WSCity();
		c2.setName("Mumbai");
		WSCity c3 = new WSCity();
		c3.setName("Mumbai");
		WSCity c4 = new WSCity();
		c4.setName("Banglore");
		WSCity c5 = new WSCity();
		c5.setName("Mumbai");
		WSCity c6 = new WSCity();
		c6.setName("Mumbai");
		WSCity c7 = new WSCity();
		c7.setName("Mumbai");
		WSCity c8 = new WSCity();
		c8.setName("Pune");
		WSCity c9 = new WSCity();
		c9.setName("Banglore");
		WSCity c10 = new WSCity();
		c10.setName("Mumbai");
		
		
		WSGroupMetaData g1 = new WSGroupMetaData();
		g1.setCity(c1);
		g1.setId("1");
		
		WSGroupMetaData g2 = new WSGroupMetaData();
		g2.setCity(c2);
		g2.setId("2");
		
		WSGroupMetaData g3 = new WSGroupMetaData();
		g3.setCity(c3);
		g3.setId("3");
		
		WSGroupMetaData g4 = new WSGroupMetaData();
		g4.setCity(c4);
		g4.setId("4");
		
		WSGroupMetaData g5 = new WSGroupMetaData();
		g5.setCity(c5);
		g5.setId("5");
		
		WSGroupMetaData g6 = new WSGroupMetaData();
		g6.setCity(c6);
		g6.setId("6");
		
		WSGroupMetaData g7 = new WSGroupMetaData();
		g7.setCity(c7);
		g7.setId("7");
		
		WSGroupMetaData g8 = new WSGroupMetaData();
		g8.setCity(c8);
		g8.setId("8");
		
		WSGroupMetaData g9 = new WSGroupMetaData();
		g9.setCity(c9);
		g9.setId("9");
		
		WSGroupMetaData g10 = new WSGroupMetaData();
		g10.setCity(c10);
		g10.setId("10");
		
		List<WSGroupMetaData> groupMetaDataList = new ArrayList<WSGroupMetaData>();
		groupMetaDataList.add(g1);
		groupMetaDataList.add(g2);
		groupMetaDataList.add(g3);
		groupMetaDataList.add(g4);
		groupMetaDataList.add(g5);
		groupMetaDataList.add(g6);
		groupMetaDataList.add(g7);
		groupMetaDataList.add(g8);
		groupMetaDataList.add(g9);
		groupMetaDataList.add(g10);
		
		Collections.sort(groupMetaDataList, new WSGroupMetaDataCityNameComparator(SortType.DESCENDING));
		
		System.out.println(groupMetaDataList);
		
		for (WSGroupMetaData m : groupMetaDataList)
		{
			System.out.println(m.getCity());
		}
		
	}

}
