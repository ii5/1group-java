package one.group.services.helpers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.pusher.rest.Pusher;
import com.pusher.rest.data.Result;

public class PushServiceAction implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(PushServiceAction.class);
    private List<String> channelsToSend = new ArrayList<String>();
    private PushServiceObject config;
    private Pusher pusher;
    private Gson gson;
    private int configuredRetryCount;

    
    public PushServiceAction(List<String> channelsToSend, PushServiceObject config, Pusher pusher, Gson gson, int configuredRetryCount)
    {
        this.channelsToSend.addAll(channelsToSend);
        this.config = config;
        this.pusher = pusher;
        this.gson = gson;
        this.configuredRetryCount = configuredRetryCount;
    }
    
    public void run()
    {
        boolean retry = false;
        int retryCount = 0;
        do
        {
            pusher.setGsonSerialiser(gson);

            Result result = pusher.trigger(channelsToSend, config.getPusherEventName(), config.getData());
            if (result.getHttpStatus() == null || result.getHttpStatus() != 200)
            {
                logger.warn("PUSHER ERROR > Result:" + config.getData() + " to (" + channelsToSend.size() + ")[" + channelsToSend + "], Result:" + result.getHttpStatus() + ":" + result.getStatus() + ":" + result.getMessage());
                if (result.getStatus().shouldRetry())
                {
                    retry = true;

                    try
                    {
                        Thread.sleep(3000);
                    }
                    catch (Exception e)
                    {
                        logger.error("", e);
                    }
                    logger.debug("Retrying [" + ++retryCount + "] push to [" + channelsToSend + "]");
                }
                else
                {
                    logger.debug("Pushed " + config.getData() + " to (" + channelsToSend.size() + ")[" + channelsToSend + "], Result:" + result.getHttpStatus() + ":" + result.getStatus() + ":" + result.getMessage());
                    retry = false;
                }
            }
            else
            {
                retry = false;
            }
        }
        while (retry && retryCount <= configuredRetryCount);
        
    }
}
