package one.group.services.helpers;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.core.Constant;
import one.group.entities.api.response.WSGCMRequest;
import one.group.entities.api.response.WSGCMResponse;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class GCMConnectionFactory
{
    private static final Logger logger = LoggerFactory.getLogger(GCMConnectionFactory.class);

    private String key;

    private String urlString;

    private int maxConnectionCount = 10;

    private int maxTimeOut = 5000;

    private String contentType = "application/json";

    private static final String ENCODING = Constant.UTF8_ENCODING;

    private BlockingDeque<HttpURLConnection> deque;

    private boolean handle;

    public <T> void postData(WSGCMResponse<T> dataObject)
    {
        String data = Utils.getJsonString(dataObject);
        logger.debug("Posting [" + data + "].");
        OutputStream os = null;
        BufferedOutputStream bos = null;

        HttpURLConnection httpUrlConnection = null;
        try
        {
            httpUrlConnection = getConnection();
            os = httpUrlConnection.getOutputStream();
            bos = new BufferedOutputStream(os);
            bos.write(data.getBytes(ENCODING));
            bos.flush();

            logger.info("Posting [" + data + "] to GCM. [code:" + httpUrlConnection.getResponseCode() + ", message:" + httpUrlConnection.getResponseMessage() + "]");
        }
        catch (Exception e)
        {
            logger.error("Error while pushing to GCM.", e);
        }
        finally
        {
            returnConnection(httpUrlConnection);
            Utils.close(os);
            Utils.close(bos);
        }
    }

    public <T> void postData(WSGCMResponse<T> data, List<String> deviceIds)
    {
        Validation.notNull(data, "The data requested to be pushed to GCM should not be null.");
        Validation.notNull(deviceIds, "The device Id list should not be null.");

        data.setRegistrationIds(deviceIds);
        postData(data);
    }

    public <T> void postData(WSSyncUpdate data, List<String> deviceIds)
    {
        Validation.notNull(data, "The data requested to be pushed to GCM should not be null.");
        Validation.notNull(deviceIds, "The device Id list should not be null.");

        WSGCMResponse<WSSyncUpdate> gcmResponse = new WSGCMResponse<WSSyncUpdate>();
        gcmResponse.setData(data);
        gcmResponse.setRegistrationIds(deviceIds);

        postData(gcmResponse);

    }

    public <T> void postData(WSGCMRequest data)
    {
        Validation.notNull(data, "The data requested to be pushed to GCM should not be null.");
        String jsonData = Utils.getJsonString(data);
        logger.debug("Posting [" + jsonData + "].");
        OutputStream os = null;
        BufferedOutputStream bos = null;

        HttpURLConnection httpUrlConnection = null;
        try
        {
            httpUrlConnection = getConnection();
            os = httpUrlConnection.getOutputStream();
            bos = new BufferedOutputStream(os);
            bos.write(jsonData.getBytes(ENCODING));
            bos.flush();

            logger.info("Posting [" + jsonData + "] to GCM. [code:" + httpUrlConnection.getResponseCode() + ", message:" + httpUrlConnection.getResponseMessage() + "]");
        }
        catch (Exception e)
        {
            logger.error("Error while pushing to GCM.", e);
        }
        finally
        {
            returnConnection(httpUrlConnection);
            Utils.close(os);
            Utils.close(bos);
        }

    }

    public void initialise() throws Exception
    {
        logger.debug("Intitializing HttpURLConnection Deque.");
        deque = new LinkedBlockingDeque<HttpURLConnection>(10);
        for (int i = 0; i < maxConnectionCount; i++)
        {
            HttpURLConnection httpURLConnection = initialiseHttpURLConnection();
            configureHttpURLConnection(httpURLConnection);
            deque.putFirst(httpURLConnection);
        }
    }

    private HttpURLConnection getConnection() throws Exception
    {
        logger.info("< HttpURLConnection Deque: [" + deque.size() + "]");
        return deque.takeFirst();
    }

    private void returnConnection(HttpURLConnection httpURLConnection)
    {
        try
        {
            // if (isConnectionValid(httpURLConnection))
            // {
            // deque.putLast(httpURLConnection);
            // }
            // else
            // {
            logger.debug("Creating new connection.");
            httpURLConnection = initialiseHttpURLConnection();
            configureHttpURLConnection(httpURLConnection);
            deque.putLast(httpURLConnection);
            // }
        }
        catch (Exception e)
        {
            logger.warn("Could not return HttpURLConnection to queue.", e);
        }

        logger.info("> HttpURLConnection Deque: [" + deque.size() + "]");
    }

    private boolean isConnectionValid(HttpURLConnection httpURLConnection)
    {
        // try
        // {
        // httpURLConnection.setRequestMethod("HEAD");
        // return httpURLConnection.getResponseCode() ==
        // HttpURLConnection.HTTP_OK;
        // }
        // catch (Exception e)
        // {
        // logger.debug("Error while checking if http url connection is active.",
        // e);
        // }

        return true;
    }

    private HttpURLConnection initialiseHttpURLConnection() throws MalformedURLException, IOException
    {
        Validation.notNull(urlString, "URL mentioned should not be null.");
        Validation.notNull(key, "Key mentioned should not be null.");

        URL url = new URL(urlString);

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        return httpURLConnection;
    }

    private void configureHttpURLConnection(HttpURLConnection httpURLConnection) throws ProtocolException
    {
        Validation.notNull(key, "Key mentioned should noe be null.");

        httpURLConnection.setConnectTimeout(getMaxTimeOut());
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", getContentType());
        httpURLConnection.setRequestProperty("Authorization", "key=" + key);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoOutput(true);
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public int getMaxTimeOut()
    {
        return maxTimeOut;
    }

    public void setMaxTimeOut(int timeOut)
    {
        this.maxTimeOut = timeOut;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getUrlString()
    {
        return urlString;
    }

    public void setUrlString(String urlString)
    {
        this.urlString = urlString;
    }

    public void setMaxConnectionCount(int maxConnectionCount)
    {
        this.maxConnectionCount = maxConnectionCount;
    }

    public int getMaxConnectionCount()
    {
        return maxConnectionCount;
    }

    public boolean isHandle()
    {
        return handle;
    }

    public void setDeque(BlockingDeque<HttpURLConnection> deque)
    {
        this.deque = deque;
    }

    public void setHandle(boolean handle)
    {
        this.handle = handle;
    }

}
