package one.group.services.helpers;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.Session;

import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

import org.apache.log4j.BasicConfigurator;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketServiceAction implements Runnable
{
    private int configuredRetryCount = 3;
    private static final Logger logger = LoggerFactory.getLogger(WebSocketServiceAction.class);
    private PushServiceObject config;

    public static class MyClientConfigurator extends ClientEndpointConfig.Configurator
    {
        static volatile boolean called = false;

        @Override
        public void beforeRequest(Map<String, List<String>> headers)
        {
            called = true;
            headers.put("authorization", Arrays.asList("bearer 9971af77-645b-478d-9d0e-92b03f0316f6"));
        }
    }

    public WebSocketServiceAction(PushServiceObject config)
    {
        this.config = config;
    }

    public void run()
    {
        boolean clientNotConnected = true;
        int retryCount = 0;
        do
        {
            String pushServiceStr = Utils.getJsonString(config);
            logger.info("WebSocketServiceAction " + pushServiceStr);
            try
            {
                clientNotConnected = runClient(pushServiceStr);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            if (clientNotConnected && retryCount <= configuredRetryCount)
            {
                try
                {
                    Thread.sleep(3000);
                }
                catch (InterruptedException ie)
                {
                    logger.info("WebSocketServiceAction " + ie);
                }
            }
            retryCount++;
        }
        while (clientNotConnected && retryCount <= configuredRetryCount);
    }

    private boolean runClient(String pushServiceStr) throws IOException
    {
        final MyClientConfigurator clientConfig = new MyClientConfigurator();
        boolean clientNotConnected = true;
        ClientManager client = ClientManager.createClient();
        client.getProperties().put(ClientProperties.SHARED_CONTAINER, true);
        Session session = null;
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().configurator(clientConfig).build();
        try
        {
            RequestEntity request = new RequestEntity();
            SocketEntity socketEntity = new SocketEntity();

            session = client.connectToServer(WebSocketServiceClientEndpoint.class, cec, new URI("ws://10.30.2.103:8026/websockets/sync"));
            clientNotConnected = session == null ? true : false;
            /*
             * Map<HttpHeaderType, String> headers = new HashMap<HttpHeaderType,
             * String>(); headers.put(HttpHeaderType.AUTHORISATION,
             * "bearer e2b34b79-273d-4b0d-a3c1-6c77da2775f9");
             * socketEntity.setEndpoint("/socket_handler");
             * socketEntity.setRequestType(HttpRequestType.POST);
             * socketEntity.setHeaders(headers);
             * session.getAsyncRemote().sendText(pushServiceStr);
             */

            /*
             * Map<HttpHeaderType, String> headers = new HashMap<HttpHeaderType,
             * String>(); WSSendChatMessage sendMessage = new
             * WSSendChatMessage(); sendMessage.setType(MessageType.TEXT);
             * sendMessage.setText("Hello7745o715");
             * sendMessage.setToAccountId("4819929371611761");
             * sendMessage.setClientSentTime(Long.valueOf("1447902956726"));
             * headers.put(HttpHeaderType.AUTHORISATION,
             * "bearer e2b34b79-273d-4b0d-a3c1-6c77da2775f9");
             * request.setChatMessage(sendMessage);
             * socketEntity.setRequestType(HttpRequestType.POST);
             * socketEntity.setRequest(request);
             * socketEntity.setHeaders(headers);
             */
            session.getBasicRemote().sendText(pushServiceStr);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (session != null && session.isOpen())
                session.close(new CloseReason(CloseReason.CloseCodes.GOING_AWAY, "Bye"));
        }
        return clientNotConnected;
    }

    public static void main(String[] args)
    {
        BasicConfigurator.configure();
        WebSocketServiceAction action = new WebSocketServiceAction(new PushServiceObject());
        action.run();
    }
}
