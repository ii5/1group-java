package one.group.services.helpers;

import java.util.concurrent.BlockingDeque;

import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SimpleExecutor implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(SimpleExecutor.class);
    
    private BlockingDeque<Runnable> runnableBlockingDeque;
    
    private final String ID = Utils.randomUUID();
    
    public SimpleExecutor(BlockingDeque<Runnable> runnableBlockingDeque)
    {
        this.runnableBlockingDeque = runnableBlockingDeque;
    }
    
    public void run()
    {
        while (true)
        {
            try
            {
                logger.debug("START [" + ID + "]: Simple Executor.");
                runnableBlockingDeque.takeFirst().run();
                logger.debug("PUSHER QUEUE: (-) [" + runnableBlockingDeque.size() + "]");
            }
            catch (Exception e)
            {
                logger.error(ID + " Error pushing, ignored and continuing!", e);
            }
            
            logger.debug("END   [" + ID + "]: Simple Executor.");
        }
    }
}
