package one.group.services.helpers;

import java.util.Comparator;

import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;

/**
 * 
 * @author nyalfernandes
 *
 */
public class PropertyListingLogicalComparator implements Comparator<PropertyListingScoreObject>
{
    public int compare(PropertyListingScoreObject o1, PropertyListingScoreObject o2)
    {
        if (o1 == o2)
        {
            return 0;
        }
       
        if (o1 == null)
        {
            return 1;
        }
        
        if (o2 == null)
        {
            return -1;
        }
        
        if (o1.equals(o2))
        {
            return 0;
        }
        
        String desc1 = o1.getAccountId();
        String desc2 = o2.getAccountId();
        
        if (!desc1.equals(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getDescription() == null ? "" : o1.getDescription().trim().toLowerCase();
        desc2 = o2.getDescription() == null ? "" : o2.getDescription().trim().toLowerCase();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getLocationId() == null ? "" : o1.getLocationId();
        desc2 = o2.getLocationId() == null ? "" : o2.getLocationId();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getType() == null ? "" : o1.getType().toString();
        desc2 = o2.getType() == null ? "" : o2.getType().toString();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getSubType() == null ? "" : o1.getSubType().toString();
        desc2 = o2.getSubType() == null ? "" : o2.getSubType().toString();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getCommissionType() == null ? "" : o1.getCommissionType().toString();
        desc2 = o2.getCommissionType() == null ? "" : o2.getCommissionType().toString();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }

        desc1 = o1.getPropertyMarket() == null ? "" : o1.getPropertyMarket().toString();
        desc2 = o2.getPropertyMarket() == null ? "" : o2.getPropertyMarket().toString();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getRentPrice() + "";
        desc2 = o2.getRentPrice() + "";
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getSalePrice() + "";
        desc2 = o2.getSalePrice() + "";
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getRooms() == null ? "" : o1.getRooms();
        desc2 = o2.getRooms() == null ? "" : o2.getRooms();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getAmenities() == null ? "" : o1.getAmenities();
        desc2 = o2.getAmenities() == null ? "" : o2.getAmenities();
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
        
        desc1 = o1.getArea() + "";
        desc2 = o2.getArea() + "";
        
        if (!desc1.equalsIgnoreCase(desc2))
        {
            return desc1.compareTo(desc2);
        }
     
        return o2.getCreationTime().compareTo(o1.getCreationTime());
    }
    
    
}
