package one.group.services.helpers;

import java.io.IOException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

public class CmsConnectionFactory
{

    private String bucket;

    private String region;

    private String accessKey;

    private String secretAccessKey;

    private String imageFolder;

    private String thumbnailFolder;

    private String masterFolder;

    private String imageFolderAPI;

    private String thumbnailFolderAPI;

    private String cdnUrl;

    private String cloudFrontUrl;

    public String getCloudFrontUrl()
    {
        return cloudFrontUrl;
    }

    public void setCloudFrontUrl(String cloudFrontUrl)
    {
        this.cloudFrontUrl = cloudFrontUrl;
    }

    public String getCdnUrl()
    {
        return cdnUrl;
    }

    public void setCdnUrl(String cdnUrl)
    {
        this.cdnUrl = cdnUrl;
    }

    public String getImageFolderAPI()
    {
        return imageFolderAPI;
    }

    public void setImageFolderAPI(String imageFolderAPI)
    {
        this.imageFolderAPI = imageFolderAPI;
    }

    public String getThumbnailFolderAPI()
    {
        return thumbnailFolderAPI;
    }

    public void setThumbnailFolderAPI(String thumbnailFolderAPI)
    {
        this.thumbnailFolderAPI = thumbnailFolderAPI;
    }

    public String getMasterFolder()
    {
        return masterFolder;
    }

    public void setMasterFolder(String masterFolder)
    {
        this.masterFolder = masterFolder;
    }

    public String getBucket()
    {
        return bucket;
    }

    public void setBucket(String bucket)
    {
        this.bucket = bucket;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public String getAccessKey()
    {
        return accessKey;
    }

    public void setAccessKey(String accessKey)
    {
        this.accessKey = accessKey;
    }

    public String getSecretAccessKey()
    {
        return secretAccessKey;
    }

    public void setSecretAccessKey(String secretAccessKey)
    {
        this.secretAccessKey = secretAccessKey;
    }

    public String getImageFolder()
    {
        return imageFolder;
    }

    public void setImageFolder(String imageFolder)
    {
        this.imageFolder = imageFolder;
    }

    public String getThumbnailFolder()
    {
        return thumbnailFolder;
    }

    public void setThumbnailFolder(String thumbnailFolder)
    {
        this.thumbnailFolder = thumbnailFolder;
    }

    public AmazonS3 getResource() throws IOException
    {
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretAccessKey);
        AmazonS3 amazons3Client = new AmazonS3Client(awsCredentials);
        Region awsregion = Region.getRegion(Regions.fromName(region));
        amazons3Client.setRegion(awsregion);
        return amazons3Client;
    }
}
