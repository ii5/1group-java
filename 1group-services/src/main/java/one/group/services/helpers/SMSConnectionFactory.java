package one.group.services.helpers;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import one.group.core.Constant;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SMSConnectionFactory
{
    private static final Logger logger = LoggerFactory.getLogger(SMSConnectionFactory.class);

    private int maxConnectionCount = 10;

    private int maxTimeOut = 5000;

    private String contentType = "application/x-www-form-urlencoded";

    private static final String ENCODING = Constant.UTF8_ENCODING;

    private BlockingDeque<HttpURLConnection> deque;

    private boolean handle;

    private String urlString;

    public <T> void postData(String smsText)
    {
        OutputStream os = null;
        BufferedOutputStream bos = null;

        HttpURLConnection httpUrlConnection = null;
        try
        {
            httpUrlConnection = getConnection();
            os = httpUrlConnection.getOutputStream();
            bos = new BufferedOutputStream(os);
            bos.write(smsText.getBytes(ENCODING));
            bos.flush();
            BufferedReader bin = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
            System.out.println("msg>>" + bin.readLine());

        }
        catch (Exception e)
        {
            logger.error("Error while sending SMS.", e);
        }
        finally
        {
            returnConnection(httpUrlConnection);
            Utils.close(os);
            Utils.close(bos);
        }
    }

    public void initialise() throws Exception
    {
        logger.debug("Intitializing HttpURLConnection Deque.");
        deque = new LinkedBlockingDeque<HttpURLConnection>(10);
        for (int i = 0; i < maxConnectionCount; i++)
        {
            HttpURLConnection httpURLConnection = initialiseHttpURLConnection();
            configureHttpURLConnection(httpURLConnection);
            deque.putFirst(httpURLConnection);
        }
    }

    private HttpURLConnection getConnection() throws Exception
    {
        logger.info("< HttpURLConnection Deque: [" + deque.size() + "]");
        return deque.takeFirst();
    }

    private void returnConnection(HttpURLConnection httpURLConnection)
    {
        try
        {
            logger.debug("Creating new connection.");
            httpURLConnection = initialiseHttpURLConnection();
            configureHttpURLConnection(httpURLConnection);
            deque.putLast(httpURLConnection);
        }
        catch (Exception e)
        {
            logger.warn("Could not return HttpURLConnection to queue.", e);
        }

        logger.info("> HttpURLConnection Deque: [" + deque.size() + "]");
    }

    private HttpURLConnection initialiseHttpURLConnection() throws MalformedURLException, IOException
    {
        Validation.notNull(urlString, "URL mentioned should not be null.");

        URL url = new URL(urlString);

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        return httpURLConnection;
    }

    private void configureHttpURLConnection(HttpURLConnection httpURLConnection) throws ProtocolException
    {

        httpURLConnection.setConnectTimeout(getMaxTimeOut());
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", getContentType());
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoOutput(true);
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public int getMaxTimeOut()
    {
        return maxTimeOut;
    }

    public void setMaxTimeOut(int timeOut)
    {
        this.maxTimeOut = timeOut;
    }

    public void setMaxConnectionCount(int maxConnectionCount)
    {
        this.maxConnectionCount = maxConnectionCount;
    }

    public int getMaxConnectionCount()
    {
        return maxConnectionCount;
    }

    public boolean isHandle()
    {
        return handle;
    }

    public void setDeque(BlockingDeque<HttpURLConnection> deque)
    {
        this.deque = deque;
    }

    public void setHandle(boolean handle)
    {
        this.handle = handle;
    }

    public String getUrlString()
    {
        return urlString;
    }

    public void setUrlString(String urlString)
    {
        this.urlString = urlString;
    }

}
