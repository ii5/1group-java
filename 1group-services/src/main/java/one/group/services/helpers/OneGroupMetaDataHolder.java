package one.group.services.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.services.LocationService;
import one.group.services.PropertyListingService;

import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Class that holds meta data of required fields
 * 
 * @author nyalfernandes
 *
 */
public class OneGroupMetaDataHolder
{
    private Map<String, PropertyListingScoreObject> duplicateIdVsOriginalPropertyObjectMap = new HashMap<String, PropertyListingScoreObject>();

    private PropertyListingService propertyListingService;

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public void initialise()
    {

    }

    private void initDuplicateVsOriginalPropertyIdMap()
    {
        List<PropertyListingScoreObject> activePropertyListingList = propertyListingService.getActivePropertyListingScoreObjects();

        // Collections.sort(activePropertyListingList, new
        // PropertyListingLogicalComparator());

    }

    public static void main(String[] args) throws Exception
    {
        DOMConfigurator.configure("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:applicationContext*.xml");
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();

        PropertyListingService propertyListingService = context.getBean("propertyListingService", PropertyListingService.class);
        LocationService localityService = context.getBean("localityService", LocationService.class);

        List<String> localities = new ArrayList<String>();
        List<Location> matchingLocations = new ArrayList<Location>();
        List<String> matchingLocationIds = new ArrayList<String>();

        localities.add("4890");
        localities.add("4923");
        localities.add("4809");
        localities.add("4816");
        localities.add("4864");

        start = System.currentTimeMillis();
        matchingLocations = localityService.getMatchingLocation(localities, true);
        end = System.currentTimeMillis() - start;
        System.out.println("fetch matching localities: [size=" + matchingLocations.size() + ", time=" + end + "]");

        start = System.currentTimeMillis();
        matchingLocationIds = localityService.getMatchingLocationIds(localities, null);
        end = System.currentTimeMillis() - start;
        System.out.println("fetch matching localities Ids: [size=" + matchingLocationIds.size() + ", time=" + end + "]");

        start = System.currentTimeMillis();
        List<PropertyListingScoreObject> propertyListingList = propertyListingService.searchPropertyListingByLocationIds(matchingLocationIds, BroadcastStatus.ACTIVE, 10000);
        end = System.currentTimeMillis() - start;
        System.out.println("fetch properties and descriptions: [size=" + propertyListingList.size() + ", time=" + end + "]");

        /*
         * propertyListingService.getActivepropertyListingCount();
         * 
         * start = System.currentTimeMillis(); matchingLocations =
         * localityService.getMatchingLocation(localities, true); end =
         * System.currentTimeMillis() - start;
         * System.out.println("fetch matching localities: [size=" +
         * matchingLocations.size() + ", time=" + end + "]");
         * 
         * start = System.currentTimeMillis(); matchingLocationIds =
         * localityService.getMatchingLocationIds(localities, null); end =
         * System.currentTimeMillis() - start;
         * System.out.println("fetch matching localities Ids: [size=" +
         * matchingLocationIds.size() + ", time=" + end + "]");
         * 
         * System.out.println(matchingLocationIds);
         * 
         * start = System.currentTimeMillis(); List<PropertyListingScoreObject>
         * propertyListingList =
         * propertyListingService.searchPropertyListing(matchingLocations,
         * PropertyStatus.ACTIVE); end = System.currentTimeMillis() - start;
         * System.out.println("fetch property listing : [size=" +
         * propertyListingList.size() + ", time=" + end + "]");
         * 
         * start = System.currentTimeMillis(); <<<<<<< HEAD propertyListingList
         * = propertyListingService.getActivePropertyListingScoreObjects();
         * //propertyListingService
         * .searchPropertyListingByLocationIds(matchingLocationIds,
         * PropertyStatus.ACTIVE); end = System.currentTimeMillis() - start;
         * System.out.println("fetch property listing by location Ids : [size="
         * + propertyListingList.size() + ", time=" + end + "]");
         * 
         * System.out.println(propertyListingList.size());
         * 
         * Collections.sort(propertyListingList, new
         * PropertyListingLogicalComparator()); List<PropertyListingScoreObject>
         * slicedList = new ArrayList<PropertyListingScoreObject>();
         * slicedList.addAll(propertyListingList); int mainListIndex = 0;
         * 
         * char[] out = {'|', '/', '-', '|', '\\', '-'}; start =
         * System.currentTimeMillis(); for (int pIndx = 0; pIndx <
         * propertyListingList.size(); pIndx++) { // System.out.println("== " +
         * pIndx + "[" + propertyListingList.get(pIndx).getCreationTime() + "] "
         * + propertyListingList.get(pIndx).getId() + " ==");
         * List<PropertyListingScoreObject> matchedProperties = new
         * ArrayList<PropertyListingScoreObject>();
         * 
         * for (int i = 0; i < slicedList.size(); i++) { if
         * (propertyListingList.get(pIndx).equalLogically(slicedList.get(i)) &&
         * !propertyListingList.get(pIndx).equals(slicedList.get(i))) {
         * matchedProperties.add(slicedList.get(i)); //
         * System.out.println(" : Matches[" + i + "=" +
         * slicedList.get(i).getId() + "(" + slicedList.get(i).getCreationTime()
         * + "]"); } else if (!matchedProperties.isEmpty()) {
         * slicedList.removeAll(matchedProperties); break; } }
         * slicedList.remove(propertyListingList.get(pIndx));
         * 
         * propertyListingList.removeAll(matchedProperties);
         * 
         * mainListIndex++; // System.out.println("Sliced List Size: " +
         * slicedList.size()); System.out.println("[" + pIndx +
         * "] mainList List Size: " + propertyListingList.size());
         * //System.out.print("\b" + out[pIndx%6]);
         * 
         * }
         * 
         * end = System.currentTimeMillis() - start;
         * System.out.println("Slicing : [size=" + slicedList.size() + ", time="
         * + end + "]");
         * 
         * System.out.println(propertyListingList.size());
         * 
         * Runtime runtime = Runtime.getRuntime();
         * 
         * NumberFormat format = NumberFormat.getInstance();
         * 
         * StringBuilder sb = new StringBuilder(); long maxMemory =
         * runtime.maxMemory(); long allocatedMemory = runtime.totalMemory();
         * long freeMemory = runtime.freeMemory();
         * 
         * sb.append("free memory: " + format.format(freeMemory / (1024*1024)) +
         * "\n"); sb.append("allocated memory: " + format.format(allocatedMemory
         * / (1024*1024)) + "\n"); sb.append("max memory: " +
         * format.format(maxMemory / (1024*1024)) + "\n");
         * sb.append("total free memory: " + format.format((freeMemory +
         * (maxMemory - allocatedMemory)) / 1024) + "<br/>");
         * 
         * System.out.println(sb.toString());
         */

        propertyListingList = propertyListingService.searchPropertyListingByLocationIds(matchingLocationIds, BroadcastStatus.ACTIVE, 10000);

        end = System.currentTimeMillis() - start;
        System.out.println("fetch property listing by location Ids : [size=" + propertyListingList.size() + ", time=" + end + "]");

        context.destroy();
        System.exit(0);
    }
}
