package one.group.services.helpers;

public class NotificationConfiguration
{
    private String specialUpdateNotificationTitle;
    private String specialUpdateNotificationBody;
    private String specialUpdateNotificationIcon;
    private String specialUpdateDataId;
    private String specialUpdateDataType;

    private String appUpdateNotificationTitle;
    private String appUpdateNotificationBody;
    private String appUpdateNotificationIcon;
    private String appUpdateDataId;
    private String appUpdateDataType;

    private String notificationMode;

    private String groupCountThreshold;
    private String invitationCodeMessage;
    private String whatsAppSyncUrl;
    private String defaultInvitationCode;
    private String notEligibleForCodeMessage;
    private String newsFeedCountThreshold;

    public NotificationConfiguration()
    {

    }

    public String getSpecialUpdateNotificationTitle()
    {
        return specialUpdateNotificationTitle;
    }

    public void setSpecialUpdateNotificationTitle(String specialUpdateNotificationTitle)
    {
        this.specialUpdateNotificationTitle = specialUpdateNotificationTitle;
    }

    public String getSpecialUpdateNotificationBody()
    {
        return specialUpdateNotificationBody;
    }

    public void setSpecialUpdateNotificationBody(String specialUpdateNotificationBody)
    {
        this.specialUpdateNotificationBody = specialUpdateNotificationBody;
    }

    public String getSpecialUpdateNotificationIcon()
    {
        return specialUpdateNotificationIcon;
    }

    public void setSpecialUpdateNotificationIcon(String specialUpdateNotificationIcon)
    {
        this.specialUpdateNotificationIcon = specialUpdateNotificationIcon;
    }

    public String getSpecialUpdateDataId()
    {
        return specialUpdateDataId;
    }

    public void setSpecialUpdateDataId(String specialUpdateDataId)
    {
        this.specialUpdateDataId = specialUpdateDataId;
    }

    public String getSpecialUpdateDataType()
    {
        return specialUpdateDataType;
    }

    public void setSpecialUpdateDataType(String specialUpdateDataType)
    {
        this.specialUpdateDataType = specialUpdateDataType;
    }

    public String getAppUpdateNotificationTitle()
    {
        return appUpdateNotificationTitle;
    }

    public void setAppUpdateNotificationTitle(String appUpdateNotificationTitle)
    {
        this.appUpdateNotificationTitle = appUpdateNotificationTitle;
    }

    public String getAppUpdateNotificationBody()
    {
        return appUpdateNotificationBody;
    }

    public void setAppUpdateNotificationBody(String appUpdateNotificationBody)
    {
        this.appUpdateNotificationBody = appUpdateNotificationBody;
    }

    public String getAppUpdateNotificationIcon()
    {
        return appUpdateNotificationIcon;
    }

    public void setAppUpdateNotificationIcon(String appUpdateNotificationIcon)
    {
        this.appUpdateNotificationIcon = appUpdateNotificationIcon;
    }

    public String getAppUpdateDataId()
    {
        return appUpdateDataId;
    }

    public void setAppUpdateDataId(String appUpdateDataId)
    {
        this.appUpdateDataId = appUpdateDataId;
    }

    public String getAppUpdateDataType()
    {
        return appUpdateDataType;
    }

    public void setAppUpdateDataType(String appUpdateDataType)
    {
        this.appUpdateDataType = appUpdateDataType;
    }

    public String getNotificationMode()
    {
        return notificationMode;
    }

    public void setNotificationMode(String notificationMode)
    {
        this.notificationMode = notificationMode;
    }

    public String getGroupCountThreshold()
    {
        return groupCountThreshold;
    }

    public void setGroupCountThreshold(String groupCountThreshold)
    {
        this.groupCountThreshold = groupCountThreshold;
    }

    public String getInvitationCodeMessage()
    {
        return invitationCodeMessage;
    }

    public void setInvitationCodeMessage(String invitationCodeMessage)
    {
        this.invitationCodeMessage = invitationCodeMessage;
    }

    public String getWhatsAppSyncUrl()
    {
        return whatsAppSyncUrl;
    }

    public void setWhatsAppSyncUrl(String whatsAppSyncUrl)
    {
        this.whatsAppSyncUrl = whatsAppSyncUrl;
    }

    public String getDefaultInvitationCode()
    {
        return defaultInvitationCode;
    }

    public void setDefaultInvitationCode(String defaultInvitationCode)
    {
        this.defaultInvitationCode = defaultInvitationCode;
    }

    public String getNotEligibleForCodeMessage()
    {
        return notEligibleForCodeMessage;
    }

    public void setNotEligibleForCodeMessage(String notEligibleForCodeMessage)
    {
        this.notEligibleForCodeMessage = notEligibleForCodeMessage;
    }

    public String getNewsFeedCountThreshold()
    {
        return newsFeedCountThreshold;
    }

    public void setNewsFeedCountThreshold(String newsFeedCountThreshold)
    {
        this.newsFeedCountThreshold = newsFeedCountThreshold;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((appUpdateDataId == null) ? 0 : appUpdateDataId.hashCode());
        result = prime * result + ((appUpdateDataType == null) ? 0 : appUpdateDataType.hashCode());
        result = prime * result + ((appUpdateNotificationBody == null) ? 0 : appUpdateNotificationBody.hashCode());
        result = prime * result + ((appUpdateNotificationIcon == null) ? 0 : appUpdateNotificationIcon.hashCode());
        result = prime * result + ((appUpdateNotificationTitle == null) ? 0 : appUpdateNotificationTitle.hashCode());
        result = prime * result + ((defaultInvitationCode == null) ? 0 : defaultInvitationCode.hashCode());
        result = prime * result + ((groupCountThreshold == null) ? 0 : groupCountThreshold.hashCode());
        result = prime * result + ((invitationCodeMessage == null) ? 0 : invitationCodeMessage.hashCode());
        result = prime * result + ((newsFeedCountThreshold == null) ? 0 : newsFeedCountThreshold.hashCode());
        result = prime * result + ((notEligibleForCodeMessage == null) ? 0 : notEligibleForCodeMessage.hashCode());
        result = prime * result + ((notificationMode == null) ? 0 : notificationMode.hashCode());
        result = prime * result + ((specialUpdateDataId == null) ? 0 : specialUpdateDataId.hashCode());
        result = prime * result + ((specialUpdateDataType == null) ? 0 : specialUpdateDataType.hashCode());
        result = prime * result + ((specialUpdateNotificationBody == null) ? 0 : specialUpdateNotificationBody.hashCode());
        result = prime * result + ((specialUpdateNotificationIcon == null) ? 0 : specialUpdateNotificationIcon.hashCode());
        result = prime * result + ((specialUpdateNotificationTitle == null) ? 0 : specialUpdateNotificationTitle.hashCode());
        result = prime * result + ((whatsAppSyncUrl == null) ? 0 : whatsAppSyncUrl.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NotificationConfiguration other = (NotificationConfiguration) obj;
        if (appUpdateDataId == null)
        {
            if (other.appUpdateDataId != null)
                return false;
        }
        else if (!appUpdateDataId.equals(other.appUpdateDataId))
            return false;
        if (appUpdateDataType == null)
        {
            if (other.appUpdateDataType != null)
                return false;
        }
        else if (!appUpdateDataType.equals(other.appUpdateDataType))
            return false;
        if (appUpdateNotificationBody == null)
        {
            if (other.appUpdateNotificationBody != null)
                return false;
        }
        else if (!appUpdateNotificationBody.equals(other.appUpdateNotificationBody))
            return false;
        if (appUpdateNotificationIcon == null)
        {
            if (other.appUpdateNotificationIcon != null)
                return false;
        }
        else if (!appUpdateNotificationIcon.equals(other.appUpdateNotificationIcon))
            return false;
        if (appUpdateNotificationTitle == null)
        {
            if (other.appUpdateNotificationTitle != null)
                return false;
        }
        else if (!appUpdateNotificationTitle.equals(other.appUpdateNotificationTitle))
            return false;
        if (defaultInvitationCode == null)
        {
            if (other.defaultInvitationCode != null)
                return false;
        }
        else if (!defaultInvitationCode.equals(other.defaultInvitationCode))
            return false;
        if (groupCountThreshold == null)
        {
            if (other.groupCountThreshold != null)
                return false;
        }
        else if (!groupCountThreshold.equals(other.groupCountThreshold))
            return false;
        if (invitationCodeMessage == null)
        {
            if (other.invitationCodeMessage != null)
                return false;
        }
        else if (!invitationCodeMessage.equals(other.invitationCodeMessage))
            return false;
        if (newsFeedCountThreshold == null)
        {
            if (other.newsFeedCountThreshold != null)
                return false;
        }
        else if (!newsFeedCountThreshold.equals(other.newsFeedCountThreshold))
            return false;
        if (notEligibleForCodeMessage == null)
        {
            if (other.notEligibleForCodeMessage != null)
                return false;
        }
        else if (!notEligibleForCodeMessage.equals(other.notEligibleForCodeMessage))
            return false;
        if (notificationMode == null)
        {
            if (other.notificationMode != null)
                return false;
        }
        else if (!notificationMode.equals(other.notificationMode))
            return false;
        if (specialUpdateDataId == null)
        {
            if (other.specialUpdateDataId != null)
                return false;
        }
        else if (!specialUpdateDataId.equals(other.specialUpdateDataId))
            return false;
        if (specialUpdateDataType == null)
        {
            if (other.specialUpdateDataType != null)
                return false;
        }
        else if (!specialUpdateDataType.equals(other.specialUpdateDataType))
            return false;
        if (specialUpdateNotificationBody == null)
        {
            if (other.specialUpdateNotificationBody != null)
                return false;
        }
        else if (!specialUpdateNotificationBody.equals(other.specialUpdateNotificationBody))
            return false;
        if (specialUpdateNotificationIcon == null)
        {
            if (other.specialUpdateNotificationIcon != null)
                return false;
        }
        else if (!specialUpdateNotificationIcon.equals(other.specialUpdateNotificationIcon))
            return false;
        if (specialUpdateNotificationTitle == null)
        {
            if (other.specialUpdateNotificationTitle != null)
                return false;
        }
        else if (!specialUpdateNotificationTitle.equals(other.specialUpdateNotificationTitle))
            return false;
        if (whatsAppSyncUrl == null)
        {
            if (other.whatsAppSyncUrl != null)
                return false;
        }
        else if (!whatsAppSyncUrl.equals(other.whatsAppSyncUrl))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "NotificationConfiguration [specialUpdateNotificationTitle=" + specialUpdateNotificationTitle + ", specialUpdateNotificationBody=" + specialUpdateNotificationBody
                + ", specialUpdateNotificationIcon=" + specialUpdateNotificationIcon + ", specialUpdateDataId=" + specialUpdateDataId + ", specialUpdateDataType=" + specialUpdateDataType
                + ", appUpdateNotificationTitle=" + appUpdateNotificationTitle + ", appUpdateNotificationBody=" + appUpdateNotificationBody + ", appUpdateNotificationIcon="
                + appUpdateNotificationIcon + ", appUpdateDataId=" + appUpdateDataId + ", appUpdateDataType=" + appUpdateDataType + ", notificationMode=" + notificationMode + ", groupCountThreshold="
                + groupCountThreshold + ", invitationCodeMessage=" + invitationCodeMessage + ", whatsAppSyncUrl=" + whatsAppSyncUrl + ", defaultInvitationCode=" + defaultInvitationCode
                + ", notEligibleForCodeMessage=" + notEligibleForCodeMessage + ", newsFeedCountThreshold=" + newsFeedCountThreshold + "]";
    }

}
