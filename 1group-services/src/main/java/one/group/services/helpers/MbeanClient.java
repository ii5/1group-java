package one.group.services.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Consumer;

import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import one.group.core.enums.StoreKey;
import one.group.entities.socket.SocketEntity;

/**
 * Exposed through an MBean.
 * @author nyalf
 *
 */
public class MbeanClient 
{
	private static final Logger logger = LoggerFactory.getLogger(MbeanClient.class);
	
	private Map<String, BlockingDeque<JMXConnector>> serviceUrlVsConnectionsMap = new ConcurrentHashMap<String, BlockingDeque<JMXConnector>>();
	private Integer poolSize = 3;
	private Long lastModifiedTime;
	
	public Integer getPoolSize() 
	{
		return poolSize;
	}
	
	public void setPoolSize(Integer poolSize) 
	{
		this.poolSize = poolSize;
	}
	
	public void broadcast(final SocketEntity socketEntity, String accountId)
	{
		socketEntity.addToStore(StoreKey.CURRENT_ACCOUNT_ID, Long.valueOf(accountId));
		serviceUrlVsConnectionsMap.keySet().parallelStream().forEach(new Consumer<String>() 
		{
			public void accept(String key) 
			{
				JMXConnector connector = null;
				BlockingDeque<JMXConnector> blockingDeque = serviceUrlVsConnectionsMap.get(key);
				try
				{
					connector = blockingDeque.takeFirst();
					logger.debug("Sending data " + socketEntity + " to " + key + ".");
					connector.getMBeanServerConnection().invoke(new ObjectName("bean:name=socketWriter"), "process", new Object[] {socketEntity}, new String[]{SocketEntity.class.getName()});
					blockingDeque.addLast(connector);
				}
				catch (IOException ioe)
				{
					logger.error("Connection error experienced for " + key + ".", ioe );
//					try{closeConnectionPool(key);initialiseConnectionPool(key);} catch (Exception e) {logger.error("Count not connect back: " + key, e);};
				}
				catch (Exception e)
				{
					logger.error("Exception while invoking write for " + key, e);
					
					if (connector != null)
					{	
						blockingDeque.addLast(connector);
					}
				}
				
			}
			 
		});
	}
	
	public void initialiseConnectionPool(String serviceUrl)
	{
		if (!serviceUrlVsConnectionsMap.containsKey(serviceUrl))
		{
			logger.info("Initialising mbean server connections for url " + serviceUrl);
			BlockingDeque<JMXConnector> q = new LinkedBlockingDeque<JMXConnector>();
			try
			{
				for (int i = 0; i < poolSize; i++)
				{
					Map<String, ?> environment = new HashMap<String, Object>();
					JMXServiceURL jmxUrl = new JMXServiceURL(serviceUrl);
					JMXConnector  jmxc = JMXConnectorFactory.connect(jmxUrl, environment);
					
					q.addLast(jmxc);
				}
				
				serviceUrlVsConnectionsMap.put(serviceUrl, q);
			}
			catch (Exception e)
			{
				logger.error("Exception while creating connection pool for " + serviceUrl, e);
			}
		}
	}
	
	public void closeConnectionPool(String serviceUrl)
	{
		if (serviceUrlVsConnectionsMap.containsKey(serviceUrl))
		{
			logger.debug("Closing connections for " + serviceUrl + ".");
			BlockingDeque<JMXConnector> connectionDeque = serviceUrlVsConnectionsMap.remove(serviceUrl);
			JMXConnector connector = null;
			while ((connector = connectionDeque.poll()) != null)
			{
				try {connector.close();} catch (Exception e) {logger.error("Error closing MBean server connection " + serviceUrl + ".", e);}
			}
		}
	}
	
	public void reinitaliseConnectionPool(String serviceUrl)
	{
		closeConnectionPool(serviceUrl);
		initialiseConnectionPool(serviceUrl);
	}
	
	public void startPropertiesScheduler()
	{
		new Thread()
		{
			@Override
			public void run() 
			{
				while (true)
				{
					try
					{
						logger.info("Polling for properties file sync.server.properties. Available : " + serviceUrlVsConnectionsMap.keySet());
						Properties p = new Properties();
						File propertiesFile = new File("sync.server.properties");
						long currentModifiedTime = propertiesFile.lastModified();
						p.load(new FileInputStream("sync.server.properties"));
						String servers = p.getProperty("serviceUrls", "");
						int serverUrlCount = servers.split(",").length;
						
						if (lastModifiedTime == null || currentModifiedTime > lastModifiedTime || serviceUrlVsConnectionsMap.size() < serverUrlCount)
						{
							if (lastModifiedTime == null)
							{
								lastModifiedTime = currentModifiedTime;
							}
							
							if (!servers.isEmpty())
							{
								for (String server : servers.split(","))
								{
									initialiseConnectionPool(server.trim());
								}
							}
							
							String servicesToAvoid = p.getProperty("avoidServices", "");
							
							if (!servicesToAvoid.isEmpty())
							{
								for (String server : servicesToAvoid.split(","))
								{
									closeConnectionPool(server.trim());
								}
							}
							
							String poolSize = p.getProperty("mbeanServicePool", "3");
							setPoolSize(Integer.parseInt(poolSize));
							
						}
						
						serviceUrlVsConnectionsMap.keySet().parallelStream().forEach(new Consumer<String>() 
						{
							public void accept(String key) 
							{
								connectionCheckAndRecreate(key);
							}
						});
						
						Thread.sleep(10000);
					}
					catch(Exception e)
					{
						logger.error("Exception while initialising mbean server connections", e);
					}
				}
			}
		}.start();
	}
	
	private void connectionCheckAndRecreate(String url)
	{
		try
		{
			logger.info("Checking validity of connection " + url);
			BlockingDeque<JMXConnector> blockingDeque = serviceUrlVsConnectionsMap.get(url);
			blockingDeque.peek().getMBeanServerConnection().getMBeanCount();
		}
		catch (IOException ioe)
		{
			logger.error("Connection issues with " + url + ", trying recreating connections.");
			try {closeConnectionPool(url);initialiseConnectionPool(url);} catch (Exception e) {};
		}
		catch (Exception e)
		{
			logger.error("Exception while checking.", e);
		}
	}
}
