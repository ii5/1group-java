package one.group.services.helpers;

import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class MoveinEnumGsonTypeAdapterFactory implements TypeAdapterFactory
{
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type)
    {
        Class<? super T> rawType = type.getRawType();
        if (rawType.isEnum())
        {
            return new MoveInEnumTypeAdapter<T>();
        }
        
        return null;
    }
    
    public class MoveInEnumTypeAdapter<T> extends TypeAdapter<T>
    {
        @Override
        public T read(JsonReader in) throws IOException
        {
            return null;
        }
        
        @Override
        public void write(JsonWriter out, T value) throws IOException
        {
            if (value == null)
            {
                out.nullValue();
                return;
            }
            
            out.value(value.toString());
            
        }
    }
}
