package one.group.services.helpers;

public class Configuration
{
    private int inviteCodeExpiryTime;

    public Configuration()
    {

    }

    public int getInviteCodeExpiryTime()
    {
        return inviteCodeExpiryTime;
    }

    public void setInviteCodeExpiryTime(int inviteCodeExpiryTime)
    {
        this.inviteCodeExpiryTime = inviteCodeExpiryTime;
    }

}
