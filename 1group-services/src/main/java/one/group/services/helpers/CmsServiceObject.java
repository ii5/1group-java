/**
 * 
 */
package one.group.services.helpers;

import java.io.File;

import one.group.core.enums.CmsType;

/**
 * @author ashishthorat
 *
 */
public class CmsServiceObject
{
    private String bucketName;

    private File awsFile;

    private String key;

    private CmsType cmsType;
    
    private String cloudFrountURL;
    
    public String getCloudFrountURL()
    {
        return cloudFrountURL;
    }
    
    public void setCloudFrountURL(String cloudFrountURL)
    {
        this.cloudFrountURL = cloudFrountURL;
    }

    public String getBucketName()
    {
        return bucketName;
    }

    public void setBucketName(String bucketName)
    {
        this.bucketName = bucketName;
    }

    public File getAwsFile()
    {
        return awsFile;
    }

    public void setAwsFile(File awsFile)
    {
        this.awsFile = awsFile;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public CmsType getCmsType()
    {
        return cmsType;
    }

    public void setCmsType(CmsType cmsType)
    {
        this.cmsType = cmsType;
    }

    @Override
    public String toString()
    {
        return "CmsServiceObject [" + (bucketName != null ? "bucketName=" + bucketName + ", " : "") + (awsFile != null ? "awsFile=" + awsFile + ", " : "") + (key != null ? "key=" + key + ", " : "")
                + (cmsType != null ? "cmsType=" + cmsType : "") + "]";
    }

}
