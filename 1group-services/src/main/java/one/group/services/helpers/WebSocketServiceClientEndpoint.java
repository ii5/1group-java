package one.group.services.helpers;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

public class WebSocketServiceClientEndpoint extends Endpoint
{
    @Override
    public void onOpen(final Session session, EndpointConfig config)
    {

        /*
         * try { session.getBasicRemote().sendText("Joining"); } catch
         * (IOException e) { e.printStackTrace(); }
         */

        session.addMessageHandler(new MessageHandler.Whole<String>()
        {
            public void onMessage(String message)
            {
                System.out.println(message);
            }
        });
    }
}