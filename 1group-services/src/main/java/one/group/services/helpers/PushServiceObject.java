package one.group.services.helpers;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.PushServerType;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PushServiceObject
{
    private List<PushServerType> serviceList = new ArrayList<PushServerType>();

    private Object data;

    private List<Object> dataList = new ArrayList<Object>();

    private List<String> pusherChannelList = new ArrayList<String>();

    private String pusherEventName;

    private List<String> gcmDeviceList = new ArrayList<String>();

    private List<String> apnsDeviceList = new ArrayList<String>();

    private String accountId;

    public List<Object> getDataList()
    {
        return dataList;
    }

    public void setDataList(List<Object> dataList)
    {
        this.dataList = dataList;
    }

    public List<PushServerType> getServiceList()
    {
        return serviceList;
    }

    public void setServiceList(List<PushServerType> serviceList)
    {
        this.serviceList = serviceList;
    }

    public void addService(PushServerType serverType)
    {
        this.serviceList.add(serverType);
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    public List<String> getPusherChannelList()
    {
        return pusherChannelList;
    }

    public void setPusherChannelList(List<String> pusherChannelList)
    {
        this.pusherChannelList = pusherChannelList;
    }

    public void addPusherChannel(String pusherChannel)
    {
        this.pusherChannelList.add(pusherChannel);
    }

    public String getPusherEventName()
    {
        return pusherEventName;
    }

    public void setPusherEventName(String pusherEventName)
    {
        this.pusherEventName = pusherEventName;
    }

    public List<String> getGcmDeviceList()
    {
        return gcmDeviceList;
    }

    public void setGcmDeviceList(List<String> gcmDeviceList)
    {
        this.gcmDeviceList = gcmDeviceList;
    }

    public List<String> getApnsDeviceList()
    {
        return apnsDeviceList;
    }

    public void setApnsDeviceList(List<String> apnsDeviceList)
    {
        this.apnsDeviceList = apnsDeviceList;
    }

    public void addGcmDevice(String gcmDevice)
    {
        this.gcmDeviceList.add(gcmDevice);
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    @Override
    public String toString()
    {
        return "PushServiceObject [serviceList=" + serviceList + ", data=" + data + ", dataList=" + dataList + ", pusherChannelList=" + pusherChannelList + ", pusherEventName=" + pusherEventName
                + ", gcmDeviceList=" + gcmDeviceList + ", apnsDeviceList=" + apnsDeviceList + ", accountId=" + accountId + "]";
    }

}
