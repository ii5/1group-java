package one.group.services.helpers;

import one.group.exceptions.ParameterNotSetException;
import one.group.services.PushService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author nyalfernandes
 * 
 */
public abstract class AbstractPushService implements PushService
{
    private PushService successor;

    private static final Logger logger = LoggerFactory.getLogger(AbstractPushService.class);

    public final void forwardRequest(PushServiceObject config)
    {
        if (basicConfigsPresent(config) && additionalConfigsPresent(config))
        {
            initiateRequest(config);
        }
        else
        {
            logger.error("Required Parameters required for push service " + this.getClass().getSimpleName() + " not set.");
            throw new ParameterNotSetException("Required Parameters required for push service " + this.getClass().getSimpleName() + " not set.");
        }
    }

    public PushService getSuccessor()
    {
        return successor;
    }

    public void setSuccessor(final PushService successor)
    {
        this.successor = successor;
    }

    private boolean basicConfigsPresent(PushServiceObject config)
    {
        if (config.getServiceList().isEmpty())
        {
            logger.error("Push Service List is empty. Atleast one needs to be present.");
            return false;
        }
        
        if (config.getData() == null)
        {
            logger.error("Push service requires data to be sent.");
            return false;
        }
        
        return true;
    }
    
    protected abstract void initiateRequest(PushServiceObject config);
    
    protected abstract boolean additionalConfigsPresent(PushServiceObject config);
}
