package one.group.services.helpers;

import java.util.Arrays;

import one.group.core.Constant;
import one.group.core.enums.PushServerType;
import one.group.services.impl.PusherPushService;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.BasicConfigurator;

import com.pusher.rest.Pusher;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PusherConnectionFactory
{
    private String key;
    private String secret;
    private String appId;
    private String host;
    private boolean secure = Constant.PUSHER_DEFAULT_SECURE;
    private int timeOut = Constant.PUSHER_DEFAULT_TIMEOUT;
    private int retryCount = 3;
    private int maxConnections = 20;
    private int maxConnectionsPerRoute = 2;
    private int batchSize = 9;
    private boolean handle;
    private int poolSize = 100;
    private Pusher p;
    
    public int getMaxConnectionsPerRoute()
    {
        return maxConnectionsPerRoute;
    }
    
    public void setMaxConnectionsPerRoute(int maxConnectionsPerRoute)
    {
        this.maxConnectionsPerRoute = maxConnectionsPerRoute;
    }

    public int getPoolSize()
    {
        return poolSize;
    }

    public void setPoolSize(int poolSize)
    {
        this.poolSize = poolSize;
    }

    public boolean isHandle()
    {
        return handle;
    }

    public void setHandle(boolean handle)
    {
        this.handle = handle;
    }

    public int getBatchSize()
    {
        return batchSize;
    }

    public void setBatchSize(int batchSize)
    {
        if (batchSize < 1 || batchSize > 10)
        {
            throw new IllegalStateException("batchsize of PUSHER configuration should be between 1 and 10 inclusive.");
        }
        this.batchSize = batchSize;
    }

    public int getRetryCount()
    {
        return retryCount;
    }

    public void setRetryCount(int retryCount)
    {
        if (retryCount < 1)
        {
            throw new IllegalStateException("retrycount of PUSHER configuration should be greater than 0.");
        }
        this.retryCount = retryCount;
    }

    public String getAppId()
    {
        return appId;
    }

    public String getHost()
    {
        return host;
    }

    public String getKey()
    {
        return key;
    }

    public int getMaxConnections()
    {
        return maxConnections;
    }

    public Pusher getResource()
    {
        if (p == null)
        {
            p = new Pusher(getAppId(), getKey(), getSecret());
            p.setHost(getHost());
            p.setRequestTimeout(getTimeOut());
            p.setEncrypted(isSecure());

            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
            connectionManager.setDefaultMaxPerRoute(getMaxConnections());
            connectionManager.setMaxTotal(getMaxConnections());
            
            p.configureHttpClient(Pusher.defaultHttpClientBuilder().setConnectionManager(connectionManager));
        }

        return p;
    }

    public String getSecret()
    {
        return secret;
    }

    public int getTimeOut()
    {
        return timeOut;
    }

    public boolean isSecure()
    {
        return secure;
    }

    public void setAppId(final String appId)
    {
        this.appId = appId;
    }

    public void setHost(final String host)
    {
        this.host = host;
    }

    public void setKey(final String key)
    {
        this.key = key;
    }

    public void setMaxConnections(final int maxConnections)
    {
        this.maxConnections = maxConnections;
    }

    public void setSecret(final String secret)
    {
        this.secret = secret;
    }

    public void setSecure(final boolean secure)
    {
        this.secure = secure;
    }

    public void setTimeOut(final int timeOut)
    {
        this.timeOut = timeOut;
    }

    public static void main(String[] args)
    {
        BasicConfigurator.configure();
        PusherConnectionFactory factory = new PusherConnectionFactory();
        factory.setAppId("135029");
        factory.setKey("8ab92fb7841df230e131");
        factory.setSecret("e1b9f4e276b02496ccb5");
        factory.setHost("api.pusherapp.com");
        factory.setSecure(true);
        factory.setTimeOut(10000);
        factory.setMaxConnections(20);
        factory.setRetryCount(5);
        factory.setBatchSize(10);

        final PusherPushService p = new PusherPushService();
        p.setConnectionFactory(factory);

        // final PushServiceObject o = new PushServiceObject();
        // o.setServiceList(Arrays.asList((new PushServerType[] {
        // PushServerType.PUSHER })));
        // o.setDataList(Arrays.asList(new Object[] { "1", "2", "3", "4", "5",
        // "6", "7" }));
        //
        // o.setPusherChannelList(Arrays.asList(new String[] { "c1", "c2", "c3",
        // "c4", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11" }));
        // o.setPusherEventName("");

        for (int i = 0; i < 10; i++)
        {
            final PushServiceObject o = new PushServiceObject();
            o.setServiceList(Arrays.asList((new PushServerType[] { PushServerType.PUSHER })));
            o.setDataList(Arrays.asList(new Object[] { "1", "2", "3", "4", "5", "6", "7" }));

            o.setPusherChannelList(Arrays.asList(new String[] { "c1", "c2", "c3", "c4", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11" }));
            o.setPusherEventName("");
            o.setData(i + 1);
            new Thread()
            {
                @Override
                public void run()
                {
                    p.forwardRequest(o);
                }
            }.run();
        }

    }
}
