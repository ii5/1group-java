package one.group.services.helpers;

public class EnvironmentConfiguration
{

    private String smsMode;
    private String testOtp;

    private String softUpdateOn;

    private String smsTextContent;

    private String smsPhoneCallContent;

    public EnvironmentConfiguration()
    {

    }

    public String getSoftUpdateOn()
    {
        return softUpdateOn;
    }

    public void setSoftUpdateOn(String softUpdateOn)
    {
        this.softUpdateOn = softUpdateOn;
    }

    public String getSmsMode()
    {
        return smsMode;
    }

    public void setSmsMode(String smsMode)
    {
        this.smsMode = smsMode;
    }

    public String getTestOtp()
    {
        return testOtp;
    }

    public void setTestOtp(String testOtp)
    {
        this.testOtp = testOtp;
    }

    public String getSmsTextContent()
    {
        return smsTextContent;
    }

    public void setSmsTextContent(String smsTextContent)
    {
        this.smsTextContent = smsTextContent;
    }

    public String getSmsPhoneCallContent()
    {
        return smsPhoneCallContent;
    }

    public void setSmsPhoneCallContent(String smsPhoneCallContent)
    {
        this.smsPhoneCallContent = smsPhoneCallContent;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((smsMode == null) ? 0 : smsMode.hashCode());
        result = prime * result + ((testOtp == null) ? 0 : testOtp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EnvironmentConfiguration other = (EnvironmentConfiguration) obj;
        if (smsMode == null)
        {
            if (other.smsMode != null)
                return false;
        }
        else if (!smsMode.equals(other.smsMode))
            return false;
        if (testOtp == null)
        {
            if (other.testOtp != null)
                return false;
        }
        else if (!testOtp.equals(other.testOtp))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "EnvironmentConfiguration [smsMode=" + smsMode + ", testOtp=" + testOtp + "]";
    }

}
