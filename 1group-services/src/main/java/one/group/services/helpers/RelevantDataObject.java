package one.group.services.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import one.group.entities.jpa.Location;
import one.group.entities.jpa.Search;

public class RelevantDataObject
{

    private int score = 0;
    private int scoredAs = 0;
    private boolean isContact;
    private boolean isContactOf;
    private boolean isBlocked;
    private boolean isBlockedBy;
    private String relevantAccountId;
    private boolean isSubscribed;
    private boolean isPostingAccount;
    private int matchingRecentSearchCount;
    private Long latestRecentSearchTime = null;
    private Long latestMatchingSavedSearchTime = null;
    private List<String> matchingSavedSearchIdList = new ArrayList<String>();
    private Set<Search> searchSet = new HashSet<Search>();
    private float relevanceScore;
    private boolean isPropertyInSameLocation;
    private List<String> locationList = new ArrayList<String>();

    public List<String> getLocationList()
    {
        return locationList;
    }

    public void setLocationList(List<String> locationList)
    {
        this.locationList = locationList;
    }

    public void setLocationWithList(Collection<Location> locationList)
    {
        if (locationList != null)
        {
            for (Location location : locationList)
            {
                if (!locationList.contains(location.getId()))
                {
                    this.locationList.add(location.getId());
                }
            }
        }
    }

    public void addLocation(String locationId)
    {
        if (!locationList.contains(locationId))
        {
            this.locationList.add(locationId);
        }
    }

    public boolean isPropertyInSameLocation()
    {
        return isPropertyInSameLocation;
    }

    public void setPropertyInSameLocation(boolean isPropertyInSameLocation)
    {
        this.isPropertyInSameLocation = isPropertyInSameLocation;
    }

    public Long getLatestMatchingSavedSearchTime()
    {
        return latestMatchingSavedSearchTime;
    }

    public void setLatestMatchingSavedSearchTime(Long latestMatchingSavedSearchTime)
    {
        this.latestMatchingSavedSearchTime = latestMatchingSavedSearchTime;
    }

    public float getRelevanceScore()
    {
        return relevanceScore;
    }

    public void setRelevanceScore(float relevanceScore)
    {
        this.relevanceScore = relevanceScore;
    }

    public Set<Search> getSearchSet()
    {
        return searchSet;
    }

    public void setSearchSet(Set<Search> searchList)
    {
        this.searchSet = searchList;
    }

    public void addToSearches(Search search)
    {
        this.searchSet.add(search);
    }

    public boolean isSubscribed()
    {
        return isSubscribed;
    }

    public void setSubscribed(boolean isSubscribed)
    {
        this.isSubscribed = isSubscribed;
    }

    public int getMatchingRecentSearchCount()
    {
        return matchingRecentSearchCount;
    }

    public void setMatchingRecentSearchCount(int recentSearchCount)
    {
        this.matchingRecentSearchCount = recentSearchCount;
    }

    public String getRelevantAccountId()
    {
        return relevantAccountId;
    }

    public void setRelevantAccountId(String accountId)
    {
        this.relevantAccountId = accountId;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Integer getScoredAs()
    {
        return scoredAs;
    }

    public void setScoredAs(Integer scoredAs)
    {
        this.scoredAs = scoredAs;
    }

    public Boolean getIsContact()
    {
        return isContact;
    }

    public void setIsContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public Boolean getIsContactOf()
    {
        return isContactOf;
    }

    public void setIsContactOf(Boolean isContactOf)
    {
        this.isContactOf = isContactOf;
    }

    public Boolean getIsBlocked()
    {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Boolean getIsBlockedBy()
    {
        return isBlockedBy;
    }

    public void setIsBlockedBy(Boolean isBlockedBy)
    {
        this.isBlockedBy = isBlockedBy;
    }

    public boolean isPostingAccount()
    {
        return isPostingAccount;
    }

    public void setPostingAccount(boolean isPostingAccount)
    {
        this.isPostingAccount = isPostingAccount;
    }

    public Long getLatestRecentSearchTime()
    {
        return latestRecentSearchTime;
    }

    public void setLatestRecentSearchTime(Long latestRecentSearchTime)
    {
        this.latestRecentSearchTime = latestRecentSearchTime;
    }

    public void addMatchingSavedSearchIdList(String id)
    {
        this.matchingSavedSearchIdList.add(id);
    }

    public void setMatchingSavedSearchList(List<String> matchingSavedSearchIdList)
    {
        this.matchingSavedSearchIdList.addAll(matchingSavedSearchIdList);
    }

    public List<String> getMatchingSavedSearchIdList()
    {
        return matchingSavedSearchIdList;
    }

    public int getSetScoredAs()
    {
        return scoredAs;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((relevantAccountId == null) ? 0 : relevantAccountId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RelevantDataObject other = (RelevantDataObject) obj;
        if (relevantAccountId == null)
        {
            if (other.relevantAccountId != null)
                return false;
        }
        else if (!relevantAccountId.equals(other.relevantAccountId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "RelevantDataObject [score=" + score + ", scoredAs=" + scoredAs + ", isContact=" + isContact + ", isContactOf=" + isContactOf + ", isBlocked=" + isBlocked + ", isBlockedBy="
                + isBlockedBy + ", relevantAccountId=" + relevantAccountId + ", isSubscribed=" + isSubscribed + ", isPostingAccount=" + isPostingAccount + ", matchingRecentSearchCount="
                + matchingRecentSearchCount + ", latestRecentSearchTime=" + latestRecentSearchTime + ", latestMatchingSavedSearchTime=" + latestMatchingSavedSearchTime
                + ", matchingSavedSearchIdList=" + matchingSavedSearchIdList + ", searchSet=" + searchSet + ", relevanceScore=" + relevanceScore + ", isPropertyInSameLocation="
                + isPropertyInSameLocation + ", locationList=" + locationList + "]";
    }

    
    
    

}
