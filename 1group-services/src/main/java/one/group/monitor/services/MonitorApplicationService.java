package one.group.monitor.services;

import java.io.IOException;
import java.util.List;

import one.group.core.enums.ApplicationActionType;
import one.group.core.enums.MonitorApplicationType;

public interface MonitorApplicationService {

	/**
	 * 
	 * @param monitorApplicationType
	 * @return
	 * @throws IOException 
	 */
	public boolean monitorApplicationType(MonitorApplicationType monitorApplicationType, List<ApplicationActionType> action) throws IOException;
	
}
