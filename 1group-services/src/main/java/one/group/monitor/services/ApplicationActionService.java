package one.group.monitor.services;

import one.group.core.enums.ApplicationActionType;
import one.group.core.enums.MonitorApplicationType;
/**
 * 
 * @author sanilshet
 *
 */
public interface ApplicationActionService 
{	
	/**
	 * 
	 * @param applicationActionType
	 * @param monitorApplicationType
	 */
	public void performApplicationAction(ApplicationActionType applicationActionType, MonitorApplicationType monitorApplicationType); 
}
