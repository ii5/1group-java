package one.group.monitor.services;

import java.io.IOException;
import java.util.List;

import one.group.core.enums.MonitorApplicationType;

public interface MonitorApplication {

	/**
	 * 
	 * @param monitorApplicationType
	 * @return
	 * @throws IOException 
	 */
	public boolean monitorApplicationType(MonitorApplicationType monitorApplicationType, List<ApplicationAction> action) throws IOException;
	
}
