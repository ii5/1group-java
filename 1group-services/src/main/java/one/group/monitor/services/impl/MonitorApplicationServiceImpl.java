package one.group.monitor.services.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import kafka.admin.AdminUtils;
import kafka.javaapi.TopicMetadata;
import kafka.javaapi.TopicMetadataRequest;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.utils.ZKStringSerializer$;
import one.group.cache.JedisConnectionFactory;
import one.group.cache.dao.impl.RedisBaseDAOImpl;
import one.group.core.enums.ApplicationActionType;
import one.group.core.enums.MonitorApplicationType;
import one.group.core.enums.TopicType;
import one.group.dao.SyncGroupDAO;
import one.group.monitor.services.ApplicationActionService;
import one.group.monitor.services.MonitorApplicationService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.Topic;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
/**
 * 
 * @author sanilshet
 *
 */

public class MonitorApplicationServiceImpl implements MonitorApplicationService 
{
	private static final Logger logger = LoggerFactory.getLogger(MonitorApplicationServiceImpl.class);
	
	private Properties configProperties = new Properties();
	
	private Properties kafkaConfigProperties = new Properties();
	
	private Properties dbProperties = new Properties();

	private ZkClient client;
	
	private RedisBaseDAOImpl cacheBaseDAO;
	
	private SyncGroupDAO syncGroupDAO;
	
	private ApplicationActionService applicationActionService;
	
	private KafkaConfiguration kafkaConfiguration; 
	
	private Properties zookeeperProperties = new Properties();
	
	public Properties getZookeeperProperties() 
	{
		return zookeeperProperties;
	}

	public void setZookeeperProperties(Properties zookeeperProperties) 
	{
		this.zookeeperProperties = zookeeperProperties;
	}

	public KafkaConfiguration getKafkaConfiguration() 
	{
		return kafkaConfiguration;
	}

	public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration) 
	{
		this.kafkaConfiguration = kafkaConfiguration;
	}

	public ApplicationActionService getApplicationActionService() 
	{
		return applicationActionService;
	}

	public void setApplicationActionService(ApplicationActionService applicationActionService) 
	{
		this.applicationActionService = applicationActionService;
	}

	public Properties getDbProperties() 
	{
		return dbProperties;
	}

	public void setDbProperties(Properties dbProperties) 
	{
		this.dbProperties = dbProperties;
	}

	public Properties getKafkaConfigProperties() 
	{
		return kafkaConfigProperties;
	}

	public void setKafkaConfigProperties(Properties kafkaConfigProperties) 
	{
		this.kafkaConfigProperties = kafkaConfigProperties;

	}	

	
	public void setConfigProperties(Properties configProperties)
	{
		this.configProperties = configProperties;
	}
	    
	public Properties getConfigProperties()
	{
		return configProperties;
	}	

	public RedisBaseDAOImpl getCacheBaseDAO() 
	{
		return cacheBaseDAO;
	}

	public void setCacheBaseDAO(RedisBaseDAOImpl cacheBaseDAO) 
	{
		this.cacheBaseDAO = cacheBaseDAO;
	}	
	
	public SyncGroupDAO getSyncGroupDAO() 
	{
		return syncGroupDAO;
	}

	public void setSyncGroupDAO(SyncGroupDAO syncGroupDAO) 
	{
		this.syncGroupDAO = syncGroupDAO;
	}	
	
	/**
	 * @throws IOException 
	 * 
	 */
	public boolean monitorApplicationType(MonitorApplicationType serverType, List<ApplicationActionType> actions) throws IOException
	{	
		boolean isApplicationRunning = false;
		
		if (serverType.equals(MonitorApplicationType.CACHE))
		{
			isApplicationRunning = checkCache();
		}
		else if (serverType.equals(MonitorApplicationType.DATABASE))
		{
			isApplicationRunning = checkDataBase();
		}
		else if (serverType.equals(MonitorApplicationType.KAKFA))
		{
			isApplicationRunning = checkKafka();
		}
		else if (serverType.equals(MonitorApplicationType.ZOOKEEPER))
		{
			isApplicationRunning = checkZookeeper();
		}
		else if (serverType.equals(MonitorApplicationType.SYNC))
		{
			isApplicationRunning = checkSync();
		}
		return isApplicationRunning;
	}
	
	/**
	 * Function to check redis is running
	 * 
	 * @return {@link Boolean}
	 */
	private boolean checkCache()
	{	
		Set<String> keys = new HashSet<String>();
		JedisConnectionFactory cf = new JedisConnectionFactory();    
		Jedis j = null;
        
		try
		{	j = cf.getResource();
			j.get("COMMAND COUNT");
			cf.returnResource(j);
			keys.add("1");
		}
		catch(Exception ex)
		{
			logger.info("Error" + ex.getMessage());
		}
		finally
		{
			
		}

		if (keys.size() > 0)
		{	
			return true;
		}
		return false;
	}
	
	/**
	 * Function to check database is running
	 * 
	 * @return {@link Boolean}
	 */
	private boolean checkDataBase()
	{
		Connection connection = null;
		ResultSet count = null;
		try 
		{
			//check if database connection is on by getting count of any table

			connection = DriverManager.getConnection(dbProperties.getProperty("url"),dbProperties.getProperty("username"),dbProperties.getProperty("password"));
			Statement statement = connection.createStatement();
			count = statement.executeQuery("Select 'ping'");
		}
		catch (Exception e) 
		{
			logger.info("Error while connecting to database "+ e.getMessage());
		}
		finally
		{
			//finally close db connection
			try 
			{	
				connection.close();
			} 
			catch (Exception e)
			{
				logger.info("Error while closing conenction - "+ e.getMessage());
			}
		}
		if (count != null)
		{
			return true;
		}
		else
		{
			applicationActionService.performApplicationAction(ApplicationActionType.SEND_EMAIL,MonitorApplicationType.DATABASE);
		}

		return false;
	}
	
	/**
	 * Function to check if kafka server is running, here we check the metadata is being 
	 * fetched for a given topic, if not then return false
	 * 
	 * @return {@link Boolean}

	 */
	private boolean checkKafka()
	{	
		SimpleConsumer simpleConsumer  = null;
		List<TopicMetadata> metaData = null;
		
		try
		{
			
	        simpleConsumer = new SimpleConsumer(zookeeperProperties.getProperty("zookeeper.host"), 9092,  10000, 1024000,"leaderLookup");	        
	        Topic topic = kafkaConfiguration.getTopic(TopicType.APP);
	        List<String> topics = Collections.singletonList(topic.getName());
            TopicMetadataRequest req = new TopicMetadataRequest(topics);
            kafka.javaapi.TopicMetadataResponse resp = simpleConsumer.send(req);
            metaData = resp.topicsMetadata();	       
		}
		catch(Exception e)
		{	
			logger.info("Error in connection = " + e.getMessage());
		}
		finally
		{			
			//close consumer connection
			simpleConsumer.close();
		}
		//check if metadata is present then return true
		if (metaData != null)
		{
			return true;
		}        

		return false;
	}
	
	/**
	 * Function to check zoo keeper server is running, here we fetch brokers topics
	 * if present then return true
	 * 
	 * @return {@link Boolean}
	 */
	private boolean checkZookeeper()
	{	
		boolean topics = false;
		try
		{	
			String HOST = "zookeeper.host";
	        String PORT = "zookeeper.port";
	        String SESSION_TIMEOUT = "zookeeper.session.timeout";
	        String CONNECTION_TIMEOUT = "zookeeper.connection.timeout";
	        final String zkConnect = configProperties.getProperty(HOST) + ":" + configProperties.getProperty(PORT) + "/kafka";
	        client = new ZkClient(zkConnect, Integer.valueOf(configProperties.getProperty(SESSION_TIMEOUT)), Integer.valueOf(configProperties
                    .getProperty(CONNECTION_TIMEOUT)), ZKStringSerializer$.MODULE$);
	        Topic topic = kafkaConfiguration.getTopic(TopicType.APP);
	        topics = AdminUtils.topicExists(client, topic.getName());			

		}
		catch(Exception ex)
		{
			logger.info("Error in connection zookeeper = " + ex.getMessage());
		}
		finally
		{

			if (client != null) {
				client.close();
			}
		}
		if (topics)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Function to check if sync server is running
	 * 
	 * @return {@link Boolean}
	 *
	 * @throws IOException
	 */
	private boolean checkSync() throws IOException
	{
		String content = "";
		File file = new File("unresponsive.properties");
		FileReader fileReader = null;
		BufferedReader bufferedreader = null;
		try
		{
			fileReader = new FileReader(file);
			bufferedreader = new BufferedReader(fileReader);			
			String line;
			while ((line = bufferedreader.readLine()) != null) 
			{
				content = line;
			}
		}
		catch(Exception e)
		{
			logger.info("Error in reading file = " + e.getMessage());
		}
		finally
		{
			if (bufferedreader != null) 
			{
				bufferedreader.close();
			}
		}
		
		if (!content.equals(""))
		{
			return false;
		}
		return true;
	}
}
