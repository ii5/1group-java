package one.group.monitor.services.impl;

import one.group.core.enums.ApplicationActionType;
import one.group.core.enums.MonitorApplicationType;
import one.group.monitor.services.ApplicationActionService;
/**
 * 
 * @author sanilshet
 *
 */
public class ApplicationActionServiceImpl implements ApplicationActionService
{
	
	public void performApplicationAction(ApplicationActionType applicationActionType, MonitorApplicationType monitorApplicationType) 
	{
		if (applicationActionType.equals(ApplicationActionType.SEND_EMAIL))
		{
			sendMail(monitorApplicationType);
		}
		else if (applicationActionType.equals(ApplicationActionType.SEND_SMS)) 
		{
			sendSMS(monitorApplicationType);
		}
	}
	
	
	private void sendMail(MonitorApplicationType type)
	{
		
	}
	
	private void sendSMS(MonitorApplicationType type)
	{
		
	}

}
