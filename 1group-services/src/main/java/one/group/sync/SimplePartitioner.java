package one.group.sync;

import kafka.producer.Partitioner;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SimplePartitioner implements Partitioner
{
    public int partition(Object key, int numPartitions)
    {
        if (key != null)
        {
            String stringKey = (String) key;
            int partition = stringKey.hashCode() % numPartitions;
            partition = partition < 0 ? ~partition : partition;
            return partition;
        }
        
        return 0;
    }
    
}
