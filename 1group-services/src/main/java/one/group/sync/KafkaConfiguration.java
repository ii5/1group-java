package one.group.sync;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import one.group.core.enums.TopicType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 *
 */
public class KafkaConfiguration
{
    private static final List<String> topicPropList = Arrays.asList("topic.name", "topic.partition.count", "topic.replication.factor", "topic.partitions.consumption");
    
    private static final Logger logger = LoggerFactory.getLogger(KafkaConfiguration.class);
    
    private Properties configProperties = new Properties();
    
    private Map<TopicType, Topic> topicMap = new HashMap<TopicType, Topic>();
    
    public void setConfigProperties(Properties configProperties)
    {
        this.configProperties = configProperties;
        initialize();
    }
    
    public Topic getTopic(TopicType type)
    {
        Topic t = topicMap.get(type);
        if (t == null)
        {
            throw new IllegalStateException("Topic Data of type " + type + " not defined.");
        }
        
        return t;
    }
    
    private void initialize()
    {
        
        
        //setPartitions(configProperties.getProperty("topic.partitions.consumption"));
        setTopics(configProperties);
    }
    
    private void setTopics(Properties p)
    {
        
        for (TopicType topicType : TopicType.values())
        {
            verifyRequiredProperties(p, topicPropList, topicType);
            if (p.containsKey(topicType.toString() + ".topic.name"))
            {
                Topic topic = new Topic();
                topic.setType(topicType);
                topic.setName(p.getProperty(topicType.toString() + ".topic.name"));
                topic.setPartitionCount(Integer.valueOf(p.getProperty(topicType.toString() + ".topic.partition.count")));
                topic.setReplicationFactor(Integer.valueOf(p.getProperty(topicType.toString() + ".topic.replication.factor")));
                // setsAllPartition and PartitionList.
                setPartitions(p.getProperty(topicType.toString() + ".topic.partitions.consumption"), topic);
                
                topicMap.put(topicType, topic);
            }
        }
    }
    
    private void setPartitions(String partitions, Topic t)
    {
        partitions = partitions.trim();
        
        if (partitions.equalsIgnoreCase("all"))
        {
            t.setAllPartitions(true);
            return;
        }
        else if (partitions.equalsIgnoreCase("none"))
        {
            t.setAllPartitions(false);
            return;
        }
        else if(!partitions.trim().isEmpty() && partitions.contains(","))
        {
            populateCommaSeparatedPartitionList(partitions, t.getPartitionList());
            if(!t.getPartitionList().isEmpty())
            {
                t.setAllPartitions(false);
            }
        }
        else if (!partitions.trim().isEmpty() && partitions.contains("-"))
        {
            populateRangeBasedPartitionList(partitions, t.getPartitionList());
            if(!t.getPartitionList().isEmpty())
            {
                t.setAllPartitions(false);
            }
        }
    }
    
    private void populateCommaSeparatedPartitionList(String partitions, List<Integer> partitionList)
    {
        String[] partitionArray = partitions.split(",");
        
        for (String partition : partitionArray)
        {
            partition = partition.trim();
            if (!partition.trim().isEmpty() && !partitionList.contains(Integer.valueOf(partition)))
            {
                addPartitionToList(Integer.parseInt(partition), partitionList);
            }
        }
    }
    
    private void populateRangeBasedPartitionList(String partitions, List<Integer> partitionList)
    {
        String[] partitionArray = partitions.split("-");
        
        if (partitionArray.length != 2)
        {
            throw new IllegalStateException("Incorrect range value specified for property '*.topic.partitions.consumption' in kafka.server.properties.");
        }
        int begin = Integer.valueOf(partitionArray[0]);
        int end = Integer.valueOf(partitionArray[1]);
        if (end < begin)
        {
            throw new IllegalStateException("Incorrect range value specified for property '*.topic.partitions.consumption' in kafka.server.properties. End range should be greater than the beginning range.");
        }
        
        for (int i = begin; i <= end; i++)
        {
            if (!partitionList.contains(Integer.valueOf(i)))
            {
                addPartitionToList(i, partitionList);
            }
        }
    }
    
    private void verifyRequiredProperties(Properties p, List<String> topicRequiredProps, TopicType type)
    {
        List<String> missingProps = new ArrayList<String>();
        for (String topicRequiredProp : topicRequiredProps)
        {
            String propName = type.toString() + "." + topicRequiredProp;
            String value = p.getProperty(propName);
            if (value == null || value.trim().isEmpty())
            {
                missingProps.add(propName);
            }
        }
        
        if (!missingProps.isEmpty())
        {
            logger.error("Properties missing or not set in kafka.server.properties: " + missingProps);
            throw new IllegalStateException("Properties missing or not set in kafka.server.properties: " + missingProps);
        }
    }
    
    private void addPartitionToList(Integer partitionNumber, List<Integer> partitionList)
    {
        partitionList.add(partitionNumber);
    }
    
    public static void main (String[] args) throws Exception
    {
        Properties p = new Properties();
        p.load(new FileInputStream("/home/nyalfernandes/Products/MoveIn/Code/movein-java/configuration/1group/kafka.server.properties"));
        
        KafkaConfiguration configuration = new KafkaConfiguration();
        configuration.setConfigProperties(p);
        
        configuration.initialize();
        
        System.out.println(configuration.getTopic(TopicType.LOG));
//        System.out.println(configuration.getConfigProperties());
    }
}
