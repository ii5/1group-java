package one.group.sync.producer;

import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import one.group.utils.validation.Validation;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kafka producer factory.
 * 
 * @author nyalfernandes
 * 
 */
public class KafkaProducerFactory<K, V>
{
    private static final Logger logger = LoggerFactory.getLogger(KafkaProducerFactory.class);
    private Properties configProperties = new Properties();
    private LinkedBlockingQueue<KafkaProducer<K, V>> producerQueue = new LinkedBlockingQueue<KafkaProducer<K, V>>();

    public void initialize()
    {
        int queueCapacity = Integer.valueOf(configProperties.getProperty("topic.producer.count"));
        producerQueue = new LinkedBlockingQueue<KafkaProducer<K, V>>(queueCapacity);
        logger.debug("Initializing producers.");
        for (int i = 0; i < queueCapacity; i++)
        {
            configProperties.setProperty("client.id", "PROD-" + i);
            producerQueue.add(new KafkaProducer<K, V>(configProperties));
        }
    }

    public KafkaProducer<K, V> getProducer() throws Exception
    {
        return producerQueue.take();
    }

    public void offerBack(KafkaProducer<K, V> producer)
    {
        producerQueue.offer(producer);
    }

    public KafkaProducer<K, V> getProducer(Properties override)
    {
        Validation.notNull(override, "Override properties should not be null.");
        Properties p = new Properties();
        p.putAll(configProperties);
        p.putAll(override);
        KafkaProducer<K, V> producer = new KafkaProducer<K, V>(p);
        return producer;
    }

    public Properties getConfigProperties()
    {
        return new Properties(configProperties);
    }

    public void setConfigProperties(Properties configProperties)
    {
        this.configProperties = configProperties;
    }
}
