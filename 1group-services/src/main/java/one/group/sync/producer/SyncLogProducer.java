package one.group.sync.producer;

import one.group.entities.socket.SocketEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.SyncLogProcessException;

/**
 * The writer of the sync log
 * 
 * @author nyalfernandes
 *
 */
public interface SyncLogProducer
{
    public void write(SyncLogEntry syncData, String topic) throws SyncLogProcessException;

    public void write(SyncLogEntry syncData, String topic, String key) throws SyncLogProcessException;

    public void write(SocketEntity socketEntity, String topic, String key) throws SyncLogProcessException;
}
