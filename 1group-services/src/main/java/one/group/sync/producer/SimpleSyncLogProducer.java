package one.group.sync.producer;

import one.group.entities.socket.SocketEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.SubscriptionService;
import one.group.sync.SimplePartitioner;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSyncLogProducer implements SyncLogProducer
{
    private static final Logger logger = LoggerFactory.getLogger(SimpleSyncLogProducer.class);

    private KafkaProducerFactory<String, String> kafkaProducerFactory;

    private SubscriptionService subscriptionService;

    public void write(final SyncLogEntry syncData, String topic) throws SyncLogProcessException
    {
        Validation.notNull(syncData, "SyncData passed should not be null.");
        Validation.notNull(topic, "Topic passed should not be null.");
        String key = syncData.getAssociatedEntityId();

        write(syncData, topic, key);
    }

    public void write(final SyncLogEntry syncData, String topic, String key) throws SyncLogProcessException
    {
        Validation.notNull(syncData, "SyncData passed should not be null.");
        Validation.notNull(topic, "Topic passed should not be null.");

        KafkaProducer<String, String> producer = null;
        try
        {
            producer = kafkaProducerFactory.getProducer();
            int partitionCount = producer.partitionsFor(topic).size();

            int partition = new SimplePartitioner().partition(key, partitionCount);
            logger.debug("type:" + syncData.getAssociatedEntityType() + ", key:" + key + ", partition:" + partition);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, partition, key, Utils.getJsonString(syncData));

            producer.send(record, new Callback()
            {
                public void onCompletion(RecordMetadata metadata, Exception exception)
                {
                    if (metadata != null)
                    {
                        logger.debug("Message sent: [" + metadata.topic() + ":" + metadata.partition() + " {" + metadata.offset() + ":" + Utils.getJsonString(syncData) + "}, exception: " + exception
                                + "]");
                    }
                }
            });

        }
        catch (Exception e)
        {
            logger.error("Error while processing sync entity: " + syncData, e);
            throw new SyncLogProcessException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        finally
        {
            if (producer != null)
            {
                kafkaProducerFactory.offerBack(producer);
            }
        }
    }

    public void write(final SocketEntity socketEntity, String topic, String key) throws SyncLogProcessException
    {
        Validation.notNull(socketEntity, "socketEntity should not be null");
        Validation.notNull(topic, "topic should not be null");

        KafkaProducer<String, String> producer = null;
        try
        {
            producer = kafkaProducerFactory.getProducer();
            int partitionCount = producer.partitionsFor(topic).size();

            int partition = new SimplePartitioner().partition(key, partitionCount);

            logger.debug("type:" + socketEntity.getEndpoint() + ", key:" + key + ", partition:" + partition);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, partition, key, Utils.getJsonString(socketEntity));

            producer.send(record, new Callback()
            {
                public void onCompletion(RecordMetadata metadata, Exception exception)
                {
                    if (metadata != null)
                    {
                        logger.debug("Message sent: [" + metadata.topic() + ":" + metadata.partition() + " {" + metadata.offset() + ":" + Utils.getJsonString(socketEntity) + "}, exception: "
                                + exception + "]");
                    }
                }
            });

        }
        catch (Exception e)
        {
            logger.error("Error while processing sync entity: " + socketEntity, e);
            throw new SyncLogProcessException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        finally
        {
            if (producer != null)
            {
                kafkaProducerFactory.offerBack(producer);
            }
        }

    }

    public KafkaProducerFactory<String, String> getKafkaProducerFactory()
    {
        return kafkaProducerFactory;
    }

    public void setKafkaProducerFactory(KafkaProducerFactory<String, String> kafkaProducerFactory)
    {
        this.kafkaProducerFactory = kafkaProducerFactory;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

}
