package one.group.sync;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.TopicType;
import one.group.utils.Utils;

public class Topic 
{
    private String name;
    private TopicType type;
    private int partitionCount;
    private List<Integer> partitionList = new ArrayList<Integer>();
    private boolean allPartitions = true;
    private int replicationFactor;
    
    public Topic () {}

    public TopicType getType()
    {
        return type;
    }
    
    public void setType(TopicType type)
    {
        this.type = type;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        Utils.isNullOrEmpty(name);
        this.name = name;
    }

    public int getPartitionCount()
    {
        return partitionCount;
    }

    public void setPartitionCount(int partitionCount)
    {
        this.partitionCount = partitionCount;
    }

    public List<Integer> getPartitionList()
    {
        return partitionList;
    }

    public void setPartitionList(List<Integer> partitionList)
    {
        this.partitionList = partitionList;
    }

    public boolean isAllPartitions()
    {
        return allPartitions;
    }

    public void setAllPartitions(boolean allPartitions)
    {
        this.allPartitions = allPartitions;
    }

    public int getReplicationFactor()
    {
        return replicationFactor;
    }

    public void setReplicationFactor(int replicationFactor)
    {
        this.replicationFactor = replicationFactor;
    }

    @Override
    public String toString()
    {
        return "Topic [name=" + name + ", type=" + type + ", partitionCount=" + partitionCount + ", partitionList=" + partitionList + ", allPartitions=" + allPartitions + ", replicationFactor="
                + replicationFactor + "]";
    }
    
    
    
}