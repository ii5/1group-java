//package one.group.utils.kafka;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * 
 * @author nyalfernandes
 *
 */
public class ConsumerChecker
{
    private Properties parentPartitionOffsets = new Properties();
    private Properties toCheckPartitionOffsets = new Properties();
    private static int ACCEPTABLE_LAG = 20;
    private static int FROM_OFFSET = 0;
    private static int TILL_OFFSET = 300;
    private static boolean VERBOSE = false;
    
    private static final String PARAM_PARENT_OFFSET_FILE_LOCATION = "parentOffsetFile";
    private static final String PARAM_TO_CHECK_OFFSET_FILE_LOCATION = "toCheckOffsetFile";
    private static final String PARAM_LAG = "lag";
    private static final String PARAM_FROM_OFFSET = "fromOffset";
    private static final String PARAM_TILL_OFFSET = "tillOffset";
    private static final String PARAM_VERBOSE = "verbose";
    
    
    
    // helpers
    private void readPropertiesFromFileInto(String fileLocation, Properties toProperties) throws Exception
    {
        toProperties.load(new FileInputStream(fileLocation));
    }
    
    /**
     * Print unacceptable offsets.
     * @param toCheckOffsetProperties
     */
    private void printUnacceptableLagOffsets(Properties toCheckOffsetProperties, Properties parentPartitionOffsets)
    {
        System.out.println("");
        System.out.println("================= LAGGING CONSUMERS =================");
        for (String key : toCheckOffsetProperties.stringPropertyNames())
        {
            String parentValue = parentPartitionOffsets.getProperty(key);
            String valueToConsider = toCheckOffsetProperties.getProperty(key);
            
            if ((parentValue != null && !parentValue.trim().isEmpty())
                    || (valueToConsider != null & !valueToConsider.trim().isEmpty()))
            {
                Integer parentValueInt = Integer.valueOf(parentValue);
                Integer valueToConsiderInt = Integer.valueOf(valueToConsider);
                
                int difference = parentValueInt - valueToConsiderInt;
                
                if (difference >= ACCEPTABLE_LAG)
                {
                    System.err.println("<< partition=" + key + ", currentOffset=" + parentValueInt + ", consumed=" + valueToConsiderInt + ", lag=" + difference);
                }
                else
                {
                    if (VERBOSE)
                    {
                        System.out.println("|| partition=" + key + ", currentOffset=" + parentValueInt + ", consumed=" + valueToConsiderInt + ", lag=" + difference);
                    }
                }
            }
            else
            {
                System.out.println("WARN : Partition '" + key + "' not definied. [parent=" + parentValue + ", local=" + valueToConsider + "]");
            }
            
        }
        System.out.println("=====================================================");
    }
    
    /**
     * Sets program arguments from the command line
     * @param args
     */
    private void loadProgramData(String[] args) throws Exception
    {
        
        String lagValue = System.getProperty(PARAM_LAG, ACCEPTABLE_LAG+"");
        String parentOffsetFile = System.getProperty(PARAM_PARENT_OFFSET_FILE_LOCATION, "");
        String toCheckOffsetFile = System.getProperty(PARAM_TO_CHECK_OFFSET_FILE_LOCATION, "");
        String fromOffset = System.getProperty(PARAM_FROM_OFFSET, FROM_OFFSET+"");
        String tillOffset = System.getProperty(PARAM_TILL_OFFSET, TILL_OFFSET+"");
        String verbose = System.getProperty(PARAM_VERBOSE, "false");
        
        ACCEPTABLE_LAG = Integer.valueOf(lagValue);
        FROM_OFFSET = Integer.valueOf(fromOffset);
        TILL_OFFSET = Integer.valueOf(tillOffset);
        VERBOSE = Boolean.valueOf(verbose);
        
        if (parentOffsetFile.trim().isEmpty() || toCheckOffsetFile.trim().isEmpty())
        {
            printUsage();
            System.exit(1);
        }
        
        readPropertiesFromFileInto(parentOffsetFile, parentPartitionOffsets);
        readPropertiesFromFileInto(toCheckOffsetFile, toCheckPartitionOffsets);
        
        for (String key : toCheckPartitionOffsets.stringPropertyNames())
        {
            int intPartition = Integer.valueOf(key);
            
            if ( !(intPartition >= FROM_OFFSET && intPartition <= TILL_OFFSET))
            {
                if (VERBOSE)
                {
                    System.out.println("Removing " + key);
                }
                toCheckPartitionOffsets.remove(key);
            }
        }
        
    }
    
    /**
     * Prints the usage information
     */
    private void printUsage()
    {
        System.out.println();System.out.println();
        System.out.println("=====================================================");
        System.out.println("Usage: ");
        System.out.println("");
        System.out.println("java ConsumerChecker -D" + PARAM_PARENT_OFFSET_FILE_LOCATION + "=[...] -D" + PARAM_TO_CHECK_OFFSET_FILE_LOCATION + "=[...] -D" + PARAM_LAG + "=[...]" +
        		" -D" + PARAM_FROM_OFFSET + "=[...] -D" + PARAM_TILL_OFFSET + "=[...]");
        System.out.println();
        System.out.println("=====================================================");
        System.out.println();System.out.println();
    }
    
    public void checkConsumersLag(String[] args) throws Exception
    {
        loadProgramData(args);
        printUnacceptableLagOffsets(toCheckPartitionOffsets, parentPartitionOffsets);
    }
    
    public static void main(String[] args) throws Exception
    {
        ConsumerChecker checker = new ConsumerChecker();
        checker.checkConsumersLag(args);
//        checker.
    }

}
