package one.group.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.MarketingList;
import one.group.services.AccountService;
import one.group.services.MarketingListService;
import one.group.utils.Utils;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVWriter;

public class MarketingListUpdater
{
    private static final Logger logger = LoggerFactory.getLogger(MarketingListUpdater.class);

    public static void main(String[] args)
    {
        DOMConfigurator.configure("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml",
                "classpath*:**applicationContext-account.xml");
        try
        {

            MarketingListUpdater updater = (MarketingListUpdater) context.getBean("marketingListUpdater");
            updater.updateMarketingListData(context);
            updater.generateCSVforMarketingList(context);
        }
        catch (Exception e)
        {
            logger.error("Error while updating marketing list", e);
        }
        context.close();

    }

    @Transactional(rollbackOn = { Exception.class })
    public void updateMarketingListData(ClassPathXmlApplicationContext context) throws IOException
    {
        MarketingListService marketingListService = (MarketingListService) context.getBean("marketingListService");
        AccountService accountService = (AccountService) context.getBean("accountService");

        List<Account> seededAccountList = accountService.fetchAllSeededAccounts();
        logger.info("Total accounts found [" + seededAccountList.size() + "]");

        List<String> seededAccontPhoneNumber = new ArrayList<String>();
        for (Account account : seededAccountList)
        {
            seededAccontPhoneNumber.add(account.getPrimaryPhoneNumber().getNumber());
        }

        if (seededAccontPhoneNumber != null && !seededAccontPhoneNumber.isEmpty())
        {
            marketingListService.checkAndUpsertAll(seededAccontPhoneNumber);
        }
        else
        {
            logger.info("No seeded accounts found");
        }

    }

    @Transactional(rollbackOn = { Exception.class })
    public void generateCSVforMarketingList(ClassPathXmlApplicationContext context) throws IOException
    {
        MarketingListService marketingListService = (MarketingListService) context.getBean("marketingListService");

        List<MarketingList> marketingList = marketingListService.fetchAllMarketingListData();
        CSVWriter writer = null;
        if (marketingList != null && !marketingList.isEmpty())
        {
            String baseDir = "";
            String csvFilePath = "phonenumber_list.csv";
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getParent();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }

            try
            {
                writer = new CSVWriter(new FileWriter(baseDir + "/" + csvFilePath));
                List<String[]> result = new ArrayList<String[]>();

                Map<String, String> urlMap = new HashMap<String, String>();

                urlMap.put("Housing - Mumbai", "http://1group.com/m/A");
                urlMap.put("99acres - Dealers List", "http://1group.com/m/B");
                urlMap.put("Housing - Delhi", "http://1group.com/m/C");
                urlMap.put("Housing - Pune", "http://1group.com/m/E");
                urlMap.put("Housing - Bangalore", "http://1group.com/m/F");
                urlMap.put("seeded", "http://1group.com/m/G");

                String smsContent = "Join the master property group. Every property from every WhatsApp group already inside. Download app from ##URL##";

                for (MarketingList record : marketingList)
                {
                    String url = urlMap.get(record.getDataSource());
                    String smsContentUpdated = smsContent.replace("##URL##", url);
                    String[] resultRow = { record.getPhoneNumber(), smsContentUpdated, "SMS 1" };
                    result.add(resultRow);
                }
                writer.writeAll(result);
                writer.flush();
            }
            catch (FileNotFoundException e)
            {
                logger.error("Error while generating csv", e);
            }
            catch (IOException e)
            {
                logger.error("Error while generating csv", e);
            }
            finally
            {
                Utils.close(writer);
            }
        }
        else
        {
            logger.info("No CSV file generate");
        }
    }
}
