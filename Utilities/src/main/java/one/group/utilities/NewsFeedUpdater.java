package one.group.utilities;

import javax.transaction.Transactional;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import one.group.exceptions.movein.General1GroupException;

public class NewsFeedUpdater
{
    private static final Logger logger = LoggerFactory.getLogger(NewsFeedUpdater.class);
    public static ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
            "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml",
            "classpath*:**applicationContext-utilities.xml");

    public static void main(String[] args)
    {
        DOMConfigurator.configure("log4j.xml");
        try
        {
            NewsFeedUpdater updater = (NewsFeedUpdater) context.getBean("newsFeedUpdater");
            logger.info("Newsfeed process started");
            updater.generateNewsFeedForAllActiveAccounts();
        }
        catch (Exception e)
        {
            logger.error("Error while newsfeed", e);
        }
        context.close();
    }

    /**
     * This function generate fresh newsfeed for all active accounts and only
     * keep 3000 newsfeed
     * 
     * @param x
     * @throws General1GroupException
     */
    @Transactional(rollbackOn = { Exception.class })
    public void generateNewsFeedForAllActiveAccounts() throws General1GroupException
    {
//        AccountService accountService = (AccountService) context.getBean("accountService");
//        NewsFeedService newsFeedService = (NewsFeedService) context.getBean("newsFeedService");
//        Set<String> accountIds = accountService.fetchIdsOfAllActiveAccount();
//        logger.info("Total accounts [" + accountIds.size() + "]");
//        //
//
//        if (accountIds != null && !accountIds.isEmpty())
//        {
//            for (String accountId : accountIds)
//            {
//                newsFeedService.generateFreshNewsFeedAndCacheIt(accountId);
//            }
//        }

    }

}
