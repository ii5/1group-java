package one.group.utilities;

import java.util.List;

import one.group.core.enums.status.Status;
import one.group.dao.AccountDAO;
import one.group.entities.jpa.Account;
import one.group.services.AccountService;
import one.group.utils.Utils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ShortReferenceUpdateInit {

    //private static final Logger logger = LoggerFactory.getLogger(ShortReferenceUpdateInit.class);	
	
    public static void main(final String[] args) 
    {	
    	try
        {
    		
        
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(    			
    			  "classpath*:**applicationContext-DAO.xml",
    			  "classpath*:**applicationContext-services.xml",
    			  "classpath*:**applicationContext-configuration-JPA.xml",
    			  "classpath*:**applicationContext-configuration-Cache.xml",
    			  "classpath*:**applicationContext-configuration-PushService.xml",
    			  "classpath*:**applicationContext-configuration-Cms.xml"
                );
    	
    	System.out.println("Updating Short reference for seeded accounts initiated");
    	AccountService accountService = (AccountService) context.getBean("accountService");
    	AccountDAO accountDAO = (AccountDAO) context.getBean("accountDAO");
    	List<Account> allSeededAccounts = accountService.fetchAllSeededAccounts();
    	System.out.println("Total Seeded Accounts " +  allSeededAccounts.size());
    	String shortReference = null;  	
    	//update short reference for each seeded accounts
    	for(Account account : allSeededAccounts)
    	{	
    		try
    		{
    			do
    	        {
    	            shortReference = Utils.generateAccountReference();
    	            Account tAccount = accountDAO.fetchAccountByShortReference(shortReference);
    	            if (tAccount != null)
    	            {
    	                shortReference = null;
    	            }
    	        }
    	        while (shortReference == null);
    			//extra check if jar is executed twice
    			if (account.getShortReference() == null && account.getStatus().equals(Status.SEEDED))
    			{
    				account.setShortReference(shortReference);
    				accountService.updateSeededAccountShortReference(account);
    				System.out.println("Account " + account.getId() + " updated with short reference " + shortReference);
    			}
    		}
    		catch(Exception ex)
    		{
    			System.out.println("Account " + account.getId() + " not updated");
    			System.out.println(ex.getMessage().toString());
    		}    		
    	}
    	context.close();
        }
    	catch(Exception ex)
    	{
    		System.out.println("" + ex.getMessage());
    	}
    }
}
