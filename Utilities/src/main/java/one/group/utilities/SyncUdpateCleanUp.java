package one.group.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import one.group.dao.AccountDAO;
import one.group.dao.SyncUpdateReadCursorsDAO;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.SyncUpdateReadCursors;
import one.group.services.ClientService;
import one.group.services.SyncDBService;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SyncUdpateCleanUp 
{	
	public static void main(String[] args)
	{	
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(    			
  			  "classpath*:**applicationContext-DAO.xml",
  			  "classpath*:**applicationContext-services.xml",
  			  "classpath*:**applicationContext-configuration-JPA.xml",
  			  "classpath*:**applicationContext-configuration-Cache.xml",
  			  "classpath*:**applicationContext-configuration-PushService.xml",
  			  "classpath*:**applicationContext-configuration-Cms.xml"
              );
		
		System.out.println("Sync update clean up initiated");		
		
		SyncDBService syncDbService = (SyncDBService) context.getBean("syncDBService");		
		ClientService clientService = (ClientService) context.getBean("clientService");
    	AccountDAO accountDAO = (AccountDAO) context.getBean("accountDAO");    	
    	SyncUpdateReadCursorsDAO syncUpdateReadCursorDAO = (SyncUpdateReadCursorsDAO) context.getBean("syncLogReadCursorsDAO");
    	
    	List<Account> allAccount = accountDAO.getAllAccounts();    	
    	
    	for (Account account : allAccount)
    	{	
    		List<Client> allClients = clientService.fetchAllClientsOfAccount(account.getId());    		
    		List<String> clientIds = new ArrayList<String>();
    		
    		for (Client client : allClients)
    		{
    			clientIds.add(client.getId());
    		}
    		List<SyncUpdateReadCursors> allUpdates = null;
    		
    		if (clientIds.size() > 0) 
    		{
    			allUpdates = syncUpdateReadCursorDAO.getUpdatesByClientIds(clientIds);
    			List<Integer> indexOfClients = new ArrayList<Integer>();
    			for (SyncUpdateReadCursors syncReadUpdate : allUpdates)
    			{
    				indexOfClients.add(syncReadUpdate.getMaxAckedUpdate());
    			}
    			
    			int minIndexOfClient = Collections.min(indexOfClients);
    			System.out.println("min index of client for account " + account.getId() + " is " + minIndexOfClient);
    			if (minIndexOfClient != -1)
    			{	
    				syncDbService.deleteEarlierUpdatesOfAccount(minIndexOfClient, account.getId());
    			}
    		}
    		
    	}
	}
}
