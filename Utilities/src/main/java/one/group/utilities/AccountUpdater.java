package one.group.utilities;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import one.group.core.Constant;
import one.group.core.enums.AccountFieldType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSAccountUpdate;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.services.UtilityService;
import one.group.utils.Utils;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class AccountUpdater
{
    private static String baseUrl = "";
    private static String imageBaseDir = "";

    private static String outputFileName = "";
    private static String usageExceptionMsg = "Usage: Requires 3 inputs 1) csv file path 2) csv file type accepting \"fullname\" or \"url\" 3) admin token";
    private static String phoneNumberHeader;
    private static String fullNameHeader;
    private static String fullPhotoUrlHeader;
    private static String thumbnailPhotoUrlHeader;

    private static String cdnImageFolder;

    private static String imageFolder;

    private static String thumbnailFolder;

    public static void main(String args[]) throws Exception
    {
        AccountUpdater updater = new AccountUpdater();

        Properties configProps = new Properties();
        configProps.load(updater.getClass().getResourceAsStream("/updaterconfig.properties"));

        baseUrl = configProps.getProperty("baseUrl");
        phoneNumberHeader = configProps.getProperty("phoneNumberHeader");
        fullNameHeader = configProps.getProperty("fullNameHeader");
        fullPhotoUrlHeader = configProps.getProperty("fullPhotoUrlHeader");
        thumbnailPhotoUrlHeader = configProps.getProperty("thumbnailPhotoUrlHeader");
        outputFileName = configProps.getProperty("outputFileName");
        imageBaseDir = configProps.getProperty("baseImageDir");
        cdnImageFolder = configProps.getProperty("prod_cdn_img_folder");
        imageFolder = configProps.getProperty("prod_imageFolder");
        thumbnailFolder = configProps.getProperty("prod_thumbnailFolder");
        ;
        String userInput = "";
        System.out.println("Enter 0 for Profile pic upload, 1 for Running Account update : ");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        userInput = bufferRead.readLine();
        int usageMode = 0;
        try
        {
            usageMode = Integer.parseInt(userInput);
            if (usageMode != 0 && usageMode != 1)
                throw new NumberFormatException();
        }
        catch (NumberFormatException ex)
        {
            System.out.println("Please enter 0 or 1");
        }
        switch (usageMode)
        {
        case 0:
            System.out.println("Enter CSV Output File path to generate:");
            bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String csvOutputFilePath = bufferRead.readLine();
            updater.uploadPicToAWSAndGenerateCSV(imageBaseDir, csvOutputFilePath);
            break;

        case 1:
            System.out.println("Enter input CSV File Path :");
            bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String csvFilePath = bufferRead.readLine();

            System.out.println("Enter input CSV File Type,valid values fullname or url :");
            bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String csvFileType = bufferRead.readLine();

            System.out.println("Enter valid auth token :");
            bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String authToken = bufferRead.readLine();

            updater.parseCSVAndUpdateAccount(csvFilePath, csvFileType, authToken);
            System.out.println("Result csv generated at " + new File(csvFilePath).getAbsoluteFile().getParent() + " with file name: " + outputFileName);
            break;

        }

    }

    private void parseCSVAndUpdateAccount(String csvFilePath, String csvFileType, String authToken) throws Exception
    {
        CSVReader reader = null;
        CSVWriter writer = null;
        Map<String, Integer> headerIndexMapper = new HashMap<String, Integer>();
        try
        {
            String baseDir = "";
            if (csvFilePath.lastIndexOf("/") == -1)
            {
                File file = new File(csvFilePath);
                baseDir = file.getAbsoluteFile().getParent();
            }
            else
            {
                baseDir = csvFilePath.substring(0, csvFilePath.lastIndexOf("/"));
            }

            reader = new CSVReader(new FileReader(csvFilePath));
            writer = new CSVWriter(new FileWriter(baseDir + "/" + outputFileName));
            String[] row;
            List<String[]> result = new ArrayList<String[]>();
            boolean isHeaderProcessed = false;
            while ((row = reader.readNext()) != null)
            {
                if (!isHeaderProcessed)
                {
                    for (int i = 0; i < row.length; i++)
                    {
                        String header = row[i];
                        if (header.equals(phoneNumberHeader))
                        {
                            headerIndexMapper.put(phoneNumberHeader, i);
                        }
                        else if (header.equals(fullNameHeader))
                        {
                            headerIndexMapper.put(fullNameHeader, i);
                        }
                        else if (header.equals(fullPhotoUrlHeader))
                        {
                            headerIndexMapper.put(fullPhotoUrlHeader, i);
                        }
                        else if (header.equals(thumbnailPhotoUrlHeader))
                        {
                            headerIndexMapper.put(thumbnailPhotoUrlHeader, i);
                        }
                    }
                    isHeaderProcessed = true;
                    continue;
                }

                String phoneNumber = row[headerIndexMapper.get(phoneNumberHeader)];
                phoneNumber = "+91" + phoneNumber;
                String[] value = new String[2];
                WSAccountUpdate wsAccountUpdate = new WSAccountUpdate();
                wsAccountUpdate.setPhoneNumber(phoneNumber);
                WSRequest request = new WSRequest();
                request.setAccountUpdate(wsAccountUpdate);
                if (AccountFieldType.FULLNAME.toString().equals(csvFileType))
                {
                    if (!headerIndexMapper.keySet().contains(phoneNumberHeader))
                        throw new Exception("Missing Header: " + phoneNumberHeader);
                    if (!headerIndexMapper.keySet().contains(fullNameHeader))
                        throw new Exception("Missing Header: " + fullNameHeader);
                    wsAccountUpdate.setNamespace(AccountFieldType.FULLNAME);
                    value[0] = row[headerIndexMapper.get(fullNameHeader)];
                    wsAccountUpdate.setValues(value);
                }
                else if (AccountFieldType.URL.toString().equals(csvFileType))
                {
                    if (!headerIndexMapper.keySet().contains(fullPhotoUrlHeader))
                        throw new Exception("Missing Header: " + fullPhotoUrlHeader);
                    if (!headerIndexMapper.keySet().contains(thumbnailPhotoUrlHeader))
                        throw new Exception("Missing Header: " + thumbnailPhotoUrlHeader);

                    wsAccountUpdate.setNamespace(AccountFieldType.URL);
                    value[0] = row[headerIndexMapper.get(fullPhotoUrlHeader)];
                    value[1] = row[headerIndexMapper.get(thumbnailPhotoUrlHeader)];
                    wsAccountUpdate.setValues(value);
                }
                String jsonRequestData = Utils.getJsonString(request);
                String url = baseUrl + "/update_accounts/" + phoneNumber;
                Response response = callWebEndPoint(url, authToken, HTTPRequestMethodType.PUT, jsonRequestData);
                String json = response.readEntity(String.class);
                WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
                String statusCode = jsonResponse.getResult().getCode();
                String statusMessage = jsonResponse.getResult().getMessage();

                if (!statusCode.equals("200"))
                {
                    String[] resultRow = { phoneNumber, value[0], String.valueOf(statusCode), statusMessage };
                    result.add(resultRow);
                }
            }
            writer.writeAll(result);
            writer.flush();
        }
        catch (FileNotFoundException e)
        {
            System.err.println(e.getMessage());
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
        finally
        {
            if (reader != null)
                reader.close();
            if (writer != null)
                writer.close();
        }
    }

    private static void copyFileUsingStream(File source, File dest) throws IOException
    {
        System.out.println(dest);
        System.out.println(dest.getAbsolutePath());
        InputStream is = null;
        OutputStream os = null;
        try
        {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        }
        finally
        {
            if (is != null)
                is.close();
            if (os != null)
                os.close();
        }
    }

    private void uploadPicToAWSAndGenerateCSV(String imagesBaseLocation, String csvOutoutFilePath) throws IOException
    {
        long startTime = System.currentTimeMillis();
        CSVWriter writer = null;
        int i = 0;
        try
        {
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
                    "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml");
            UtilityService utilityService = (UtilityService) context.getBean("utilityService");

            File imagesBaseDirectory = new File(imagesBaseLocation);
            System.out.println("base loc>>" + imagesBaseLocation);
            File[] accountProfileImages = imagesBaseDirectory.listFiles();
            System.out.println("length>>" + accountProfileImages.length);
            writer = new CSVWriter(new FileWriter(csvOutoutFilePath));
            List<String[]> dataToWrite = new ArrayList<String[]>();
            dataToWrite.add(new String[] { phoneNumberHeader, fullPhotoUrlHeader, thumbnailPhotoUrlHeader });

            for (File accountProfileImage : accountProfileImages)
            {
                if (accountProfileImage.isDirectory())
                    continue;

                if (accountProfileImage.length() < Constant.MAX_PHOTO_SIZE)
                {
                    String identifier = accountProfileImage.getName();
                    String imageName = Utils.getSystemTime() + identifier.substring(identifier.indexOf("."), identifier.length());
                    copyFileUsingStream(accountProfileImage, new File(cdnImageFolder + "/" + imageFolder + "/" + imageName));
                    String uploadedImageUrlFullPhoto = "";
                    String uploadedImageUrlThumbnail = "";
                    try
                    {
                        uploadedImageUrlFullPhoto = utilityService.uploadFile(accountProfileImage, imageName, "fullPhoto");
                        String localImagePathThumbnail = saveThumbnailPhoto(accountProfileImage, imagesBaseLocation, imageName);
                        uploadedImageUrlThumbnail = utilityService.uploadFile(new File(localImagePathThumbnail), imageName, "thumbnail");
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                    identifier = identifier.substring(0, identifier.lastIndexOf("."));
                    identifier = identifier.substring(2);
                    String[] data = { identifier, uploadedImageUrlFullPhoto, uploadedImageUrlThumbnail };
                    dataToWrite.add(data);
                }
                else
                {
                    System.out.println("File " + accountProfileImage.getName() + " Greater than 2 MB");
                }
                accountProfileImage.delete();
                System.out.println("completed " + i++ + " upload");
            }
            writer.writeAll(dataToWrite);
            writer.flush();
            long endTime = System.currentTimeMillis();
            System.out.println("Completed in " + (endTime - startTime) / 1000 + " secs");
            System.exit(0);
        }

        finally
        {
            if (writer != null)
            {
                writer.flush();
                writer.close();
            }
        }
    }

    private Response callWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {

        ClientConfig config = new ClientConfig();
        // config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }

    public String saveThumbnailPhoto(File accountProfileImage, String imagesBaseLocation, String imageName) throws Exception
    {
        String thumbnailPath = "";

        String identifier = accountProfileImage.getName();

        String extension = identifier.substring(identifier.indexOf(".") + 1, identifier.length());
        thumbnailPath = cdnImageFolder + "/" + thumbnailFolder + "/" + imageName;
        String thumbnailLocation = accountProfileImage.getAbsolutePath();

        BufferedImage fullPhoto = ImageIO.read(accountProfileImage);
        int fullPhotoWidth = fullPhoto.getWidth();
        int fullPhotoheight = fullPhoto.getHeight();

        // Create thumbnail photo
        int thumbnailWidth = Constant.MIN_PHOTO_THUMBNAIL_WIDTH;
        int thumbnailHeight = Constant.MIN_PHOTO_THUMBNAIL_HEIGHT;
        if (fullPhotoWidth / fullPhotoheight < thumbnailWidth / thumbnailHeight)
        {
            thumbnailHeight = (int) Math.floor((double) fullPhotoheight * (double) thumbnailWidth / (double) fullPhotoWidth);
        }
        else
        {
            thumbnailWidth = (int) Math.floor((double) fullPhotoWidth * (double) thumbnailHeight / (double) fullPhotoheight);
        }
        Image scaledImg = fullPhoto.getScaledInstance(thumbnailWidth, thumbnailHeight, Image.SCALE_SMOOTH);
        BufferedImage thumbnail = new BufferedImage(thumbnailWidth, thumbnailHeight, BufferedImage.TYPE_INT_RGB);
        thumbnail.createGraphics().drawImage(scaledImg, 0, 0, null);

        File thumbnailFile = new File(thumbnailPath);
        ImageIO.write(thumbnail, extension, thumbnailFile);

        if (thumbnailFile.length() > Constant.MAX_PHOTO_SIZE)
        {
            throw new IllegalStateException("File uploaded is too large: Max size is: " + Constant.MAX_PHOTO_SIZE_STR);
        }

        Map<String, Object> thumbnailPhoto = new HashMap<String, Object>();
        thumbnailPhoto.put("width", thumbnail.getWidth());
        thumbnailPhoto.put("height", thumbnail.getHeight());
        thumbnailPhoto.put("size", thumbnailFile.length());
        thumbnailPhoto.put("location", thumbnailLocation);
        thumbnailPhoto.put("path", thumbnailPath);

        System.out.println("thumbnailPath " + thumbnailPath);
        return thumbnailPath;
    }

}
