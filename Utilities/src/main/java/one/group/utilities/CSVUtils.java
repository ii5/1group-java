package one.group.utilities;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CSVUtils
{
    public static void main(String[] args)
    {
        CSVReader reader = null;
        CSVWriter writer = null;
        Map<String, List<String>> cityVsNumberMap = new HashMap<String, List<String>>();
        try
        {
            reader = new CSVReader(new FileReader("agents-numbers-nyal.csv"), ':', '"');
            writer = new CSVWriter(new FileWriter("[CITY]-unique-numbers.csv"), ':', '"');
            List<String[]> entireFileInList = reader.readAll();

            for (String[] line : entireFileInList)
            {
                String city = line[0].toUpperCase().trim();

                if (!cityVsNumberMap.containsKey(city))
                {
                    cityVsNumberMap.put(city, new ArrayList<String>());
                }
                // System.out.println(city);
                String[] numberArray = line[1].split(",");

                for (String number : numberArray)
                {
                    number = number.trim().replace(" ", "");

                    if (!cityVsNumberMap.get(city).contains(number))
                    {
                        cityVsNumberMap.get(city).add(number);
                    }
                }
                System.out.println(entireFileInList.indexOf(line));
            }

            for (String city : cityVsNumberMap.keySet())
            {
                try
                {
                    System.out.println(city + "-unique-numbers.csv");
                    new File(city + "-unique-numbers.csv").createNewFile();
                    writer = new CSVWriter(new FileWriter(city + "-unique-numbers.csv"), ':', '"');
                    for (String number : cityVsNumberMap.get(city))
                    {
                        writer.writeNext(new String[] { number });
                    }

                    writer.flush();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (Exception e)
                {
                }
                ;
            }

            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception e)
                {
                }
                ;
            }
        }
    }

}
