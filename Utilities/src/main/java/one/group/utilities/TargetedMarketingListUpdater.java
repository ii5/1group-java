package one.group.utilities;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import one.group.entities.jpa.TargetedMarketingList;
import one.group.services.MarketingListService;
import one.group.services.SMSMarketingLogService;
import one.group.services.SMSService;
import one.group.services.TargetedMarketingListService;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TargetedMarketingListUpdater
{

    private static final Logger logger = LoggerFactory.getLogger(TargetedMarketingListUpdater.class);

    public static void main(String[] args)
    {
        DOMConfigurator.configure("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-services.xml", "classpath*:**applicationContext-DAO.xml",
                "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml", "classpath*:**applicationContext-configuration-PushService.xml",
                "classpath*:**applicationContext-account.xml");

        try
        {

            TargetedMarketingListUpdater updater = (TargetedMarketingListUpdater) context.getBean("targetedMarketingListUpdater");

            // Mark all target account as white list
            List<TargetedMarketingList> targetedList = updater.markTargetListAsWhiteList(context);

            // send SMS to all targeted account
            //updater.sendSMSAndLogTargetedMarketingList(context, targetedList);

            // flush targeted marketing list
            updater.flushAllTargetedMarketingListData(context);

        }
        catch (Exception e)
        {
            logger.error("Error while updating marketing list", e);
        }
        context.close();
    }

    @Transactional(rollbackOn = { Exception.class })
    public List<TargetedMarketingList> markTargetListAsWhiteList(ClassPathXmlApplicationContext context)
    {
        logger.info("Start: Marking all targeted marketing list as White list");

        TargetedMarketingListService targetedMarketingListService = (TargetedMarketingListService) context.getBean("targetedMarketingListService");
        MarketingListService marketingListService = (MarketingListService) context.getBean("marketingListService");

        List<TargetedMarketingList> targetedMarketingList = targetedMarketingListService.fetchAllTargetedList();
        logger.info("Total targeted phonenumber [" + targetedMarketingList.size() + "]");

        marketingListService.updateMarketingList(targetedMarketingList);

        logger.info("Finished: Marking all targeted marketing list as White list");
        return targetedMarketingList;
    }

    @Transactional(rollbackOn = { Exception.class })
    public void sendSMSAndLogTargetedMarketingList(ClassPathXmlApplicationContext context, List<TargetedMarketingList> targetMarketingList) throws IOException
    {

        logger.info("Start: Sending SMS & Logging into sms marking log");
        if (targetMarketingList != null && !targetMarketingList.isEmpty())
        {
            SMSService smsNetcoreService = (SMSService) context.getBean("smsNetcoreService");
            SMSMarketingLogService smsMarketingLogService = (SMSMarketingLogService) context.getBean("smsMarketingLogService");

            int counter = 0;
            for (TargetedMarketingList targeted : targetMarketingList)
            {
                String mobileNumber = targeted.getPhoneNumber();
                String message = targeted.getMessageContent();
                String campaign = targeted.getUtmCampaign();

                smsNetcoreService.sendSMS(mobileNumber, message);
                smsMarketingLogService.saveSMSMarketingLog(mobileNumber, message, campaign);
                logger.info(counter + " - send & save log [" + mobileNumber + "]");

                counter++;
            }
        }
        else
        {
            logger.info("targeted marketing list is empty");
        }
        logger.info("Finished: Sending SMS & Logging into sms marking log");
    }

    @Transactional(rollbackOn = { Exception.class })
    public void flushAllTargetedMarketingListData(ClassPathXmlApplicationContext context)
    {
        logger.info("Start: Flushing targeted marketing list");

        TargetedMarketingListService targetedMarketingListService = (TargetedMarketingListService) context.getBean("targetedMarketingListService");
        targetedMarketingListService.deleteAllTargetedList();

        logger.info("Finished: Flushing targeted marketing list");
    }

}
