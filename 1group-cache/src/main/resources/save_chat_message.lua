local chatThreadId = ARGV[1];
local chatMessageJson = ARGV[2];
local chatMessageKey = ARGV[3];
local fromAccountId = ARGV[4];
local toAccountId = ARGV[5];
local timeStamp = ARGV[6];
local scType = ARGV[7];
local CHAT_MESSAGES_OF_CHAT_THREAD = KEYS[1];
local CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET = KEYS[2];
local FROM_CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT  = KEYS[3];
local FROM_CHAT_THREADS_BY_JOINED_OF_ACCOUNT = KEYS[4];
local TO_CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT = KEYS[5];
local TO_CHAT_THREADS_BY_JOINED_OF_ACCOUNT = KEYS[6];
local FROM_CHAT_THREAD_CURSORS = KEYS[7];
local TO_CHAT_THREAD_CURSORS = KEYS[8];
local FROM_SUBSCRIPTION_KEY  = KEYS[9];
local TO_SUBSCRIPTION_KEY  = KEYS[10];
local CHAT_THREAD_PARTICIPANTS = KEYS[11];

-- Save chat message in hash
redis.call("HSET", CHAT_MESSAGES_OF_CHAT_THREAD, chatMessageKey, chatMessageJson);
-- Get total chat message and use it as score for message
local score = redis.call("ZCARD", CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET);
-- Store sorted set of chat message
redis.call("ZADD", CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET, score, chatMessageKey);

-- Save chat threads for fromAccountId
redis.call("ZADD", FROM_CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT, timeStamp, chatThreadId);
local fromIsChatThreadCreated = redis.call("ZSCORE", FROM_CHAT_THREADS_BY_JOINED_OF_ACCOUNT, chatThreadId);
if not fromIsChatThreadCreated then 
	redis.call("ZADD", FROM_CHAT_THREADS_BY_JOINED_OF_ACCOUNT, timeStamp, chatThreadId);
end

-- Save chat threads for toAccountId
redis.call("ZADD", TO_CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT, timeStamp, chatThreadId);
local toIsChatThreadCreated = redis.call("ZSCORE", TO_CHAT_THREADS_BY_JOINED_OF_ACCOUNT, chatThreadId);
if not toIsChatThreadCreated then 
	redis.call("ZADD", TO_CHAT_THREADS_BY_JOINED_OF_ACCOUNT, timeStamp, chatThreadId);
end

-- Fetch chat message index
local index = redis.call("ZRANK", CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET, chatMessageKey);

if index == 0 then

	-- Set chat thread cursor for toAccountId to default(-1)
	redis.call("HSET", TO_CHAT_THREAD_CURSORS, "received_index", -1);
	redis.call("HSET", TO_CHAT_THREAD_CURSORS, "read_index", -1);


	-- Both accounts implicit subscribe to chat thread for a life time
	-- redis.call("SET", FROM_SUBSCRIPTION_KEY, -1);
	-- redis.call("SET", TO_SUBSCRIPTION_KEY,  -1);
	-- It has been moved to in Service layer 

	-- Store participants for chat thread 
	redis.call("HSET", CHAT_THREAD_PARTICIPANTS,"from_account_id", fromAccountId);
	redis.call("HSET", CHAT_THREAD_PARTICIPANTS,"to_account_id", toAccountId);

end

-- Update chat thread cursor for fromAccountId
redis.call("HSET", FROM_CHAT_THREAD_CURSORS, "received_index", index);
redis.call("HSET", FROM_CHAT_THREAD_CURSORS, "read_index", index);

return index;



