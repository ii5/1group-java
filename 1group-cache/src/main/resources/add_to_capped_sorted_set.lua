local SORTED_SET_KEY = tostring(KEYS[1]);

local memberValue = ARGV[1];
local memberScore = ARGV[2];
local setCap = tonumber(ARGV[3]);

redis.call("ZADD", SORTED_SET_KEY, memberScore, memberValue); --O(log(N))

local totalMembers = tonumber(redis.call("ZCOUNT", SORTED_SET_KEY, "-inf", "+inf")); --O(log(N))

if totalMembers > setCap then
	local firstSortedSetMember = redis.call("ZRANGE", SORTED_SET_KEY, "0", "0"); --O(log(N) + M); M = 1, since only one element is returned.
	redis.call("ZREM", SORTED_SET_KEY, firstSortedSetMember[1]); --O(log(N))
end