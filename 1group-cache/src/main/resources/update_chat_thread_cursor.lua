local CHAT_THREAD_CURSORS_KEY = KEYS[1];
local CHAT_THREAD_MESSAGE_SCORE = KEYS[2];
local RECEIVED_INDEX_HASHKEY = ARGV[1];
local newReceivedIndex =  tonumber(ARGV[2]);
local READ_INDEX_HASHKEY = ARGV[3];
local newReadIndex =  tonumber(ARGV[4]);

local currentReceivedIndex = tonumber(redis.call("hget", CHAT_THREAD_CURSORS_KEY, RECEIVED_INDEX_HASHKEY));
local currentReadIndex =  tonumber(redis.call("hget", CHAT_THREAD_CURSORS_KEY, READ_INDEX_HASHKEY));
local latestMessage = redis.call("zrevrangebyscore", CHAT_THREAD_MESSAGE_SCORE, "+inf", "-inf", "LIMIT", "0", "1", "WITHSCORES");
local maxIndex =  tonumber(latestMessage[2]);

local isUpdated = "false";
local indexArray = {};
indexArray[1] = currentReceivedIndex;
indexArray[2] = currentReadIndex;
indexArray[3] = isUpdated;

if newReceivedIndex - maxIndex > 0 then
	newReceivedIndex = maxIndex;
end

if newReadIndex - maxIndex > 0 then
	newReadIndex = maxIndex;
end

if currentReadIndex == -1 and newReadIndex > -1 then

	redis.call("hset", CHAT_THREAD_CURSORS_KEY, READ_INDEX_HASHKEY, newReadIndex);

	if newReadIndex > newReceivedIndex then
		newReceivedIndex = newReadIndex;
	end

	currentReadIndex = newReadIndex;
	isUpdated = "true";
elseif newReadIndex > currentReadIndex then

	redis.call("hset", CHAT_THREAD_CURSORS_KEY, READ_INDEX_HASHKEY, newReadIndex);

	if newReadIndex > newReceivedIndex then
		newReceivedIndex = newReadIndex;
	end

	currentReadIndex = newReadIndex;
	isUpdated = "true";
end

if currentReceivedIndex == -1 and newReceivedIndex > -1 then

	redis.call("hset", CHAT_THREAD_CURSORS_KEY, RECEIVED_INDEX_HASHKEY, newReceivedIndex);			

	currentReceivedIndex = newReceivedIndex;	
	isUpdated = "true";

elseif newReceivedIndex > currentReceivedIndex then

	redis.call("hset", CHAT_THREAD_CURSORS_KEY, RECEIVED_INDEX_HASHKEY, newReceivedIndex);			

	currentReceivedIndex = newReceivedIndex;	
	isUpdated = "true";
end 

indexArray[1] = "received_index=" .. currentReceivedIndex;
indexArray[2] = "read_index=" .. currentReadIndex;
indexArray[3] = "is_index_updated=" .. isUpdated;

return indexArray; 
