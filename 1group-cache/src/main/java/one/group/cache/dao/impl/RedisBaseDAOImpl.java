package one.group.cache.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.JedisConnectionFactory;
import one.group.cache.PipelineCacheEntity;
import one.group.cache.dao.CacheBaseDAO;
import one.group.core.Constant;
import one.group.exceptions.movein.runtime.CacheUnavailableException;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.Tuple;

/**
 * 
 * 
 * @author nyalfernandes
 * 
 */
public class RedisBaseDAOImpl implements CacheBaseDAO
{

    private static final Logger logger = LoggerFactory.getLogger(RedisBaseDAOImpl.class);

    private JedisConnectionFactory connectionFactory;

    private int scanCount = 1000;

    private String scanPointerEnd = String.valueOf(0);

    public String getScanPointerEnd()
    {
        return scanPointerEnd;
    }

    public void setScanPointerEnd(String scanPointerEnd)
    {
        this.scanPointerEnd = scanPointerEnd;
    }

    public int getScanCount()
    {
        return scanCount;
    }

    public void setScanCount(int scanCount)
    {
        this.scanCount = scanCount;
    }

    private Jedis getJedis()
    {
        Jedis jedis = connectionFactory.getResource();
        if (jedis == null)
        {
            throw new CacheUnavailableException("Cache server down.");
        }

        return jedis;
    }

    public JedisConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public Long addToHash(final String hashKey, final String field, final String value)
    {
        Jedis j = getJedis();
        Validation.notNull(hashKey, "Key should not be null.");
        Validation.notNull(field, "Field should not be null.");
        Validation.notNull(value, "Value should not be null.");

        try
        {
            Long v = j.hset(hashKey, field, value);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void addToList(final String listKey, final String value, final String... values)
    {
        Jedis j = getJedis();
        Validation.notNull(listKey, "Key should not be null.");
        Validation.notNull(value, "The value should not be null.");

        try
        {
            j.rpush(listKey, value);
            if (values != null && values.length > 0)
            {
                j.rpush(listKey, values);
            }
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Long getTTL(String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        try
        {
            Long value = j.ttl(key);
            return value;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void addToSet(final String setKey, final String value, final String... values)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        Validation.notNull(value, "The value should not be null.");

        try
        {
            j.sadd(setKey, value);
            if (values != null && values.length > 0)
            {
                j.sadd(setKey, values);
            }
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void addToSortedSet(final String setKey, final double itemScore, final String member)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        Validation.notNull(member, "The value should not be null.");

        try
        {
            j.zadd(setKey, itemScore, member);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void addToSortedSetBulk(Set<PipelineCacheEntity> pipelineSet)
    {
        Jedis j = getJedis();
        Validation.notNull(pipelineSet, "PipeLineSet should not be null.");

        try
        {
            Pipeline pipeline = j.pipelined();
            for (PipelineCacheEntity pce : pipelineSet)
            {
                String setKey = pce.getKey();

                Double itemScore = pce.getScore();
                String member = pce.getValue();
                pipeline.zadd(setKey, itemScore, member);
            }
            pipeline.sync();

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void pipelinedEval(Set<PipelineCacheEntity> pipelineSet, String script)
    {
        Validation.notNull(pipelineSet, "PipeLineSet should not be null.");
        Jedis j = getJedis();

        try
        {
            Pipeline pipeline = j.pipelined();
            for (PipelineCacheEntity pce : pipelineSet)
            {
                String setKey = pce.getKey();

                Double itemScore = pce.getScore();
                String member = pce.getValue();
                List<String> keys = new ArrayList<String>();
                List<String> args = new ArrayList<String>();

                keys.add(setKey);

                args.add(member);
                args.add(itemScore.toString());
                args.add(pce.getKeyCap() + "");

                pipeline.eval(script, keys, args);
            }
            pipeline.sync();

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void addToSortedSet(final String setKey, final Map<String, Double> scoredMembers)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        Validation.notNull(scoredMembers, "The setMember's holder should not be null.");

        j.zadd(setKey, scoredMembers);
        getConnectionFactory().returnResource(j);
    }

    public String getHash(final String hashKey, final String field)
    {
        Jedis j = getJedis();
        Validation.notNull(hashKey, "Key should not be null.");
        Validation.notNull(field, "Field should not be null.");
        try
        {
            String v = j.hget(hashKey, field);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> keys(final String pattern)
    {
        Validation.notNull(pattern, "Pattern should not be null.");
        Jedis j = getJedis();
        try
        {
            Set<String> v = j.keys(pattern);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> scan(final String pattern)
    {
        Validation.notNull(pattern, "Pattern should not be null.");
        Jedis j = getJedis();

        try
        {
            ScanParams params = new ScanParams();
            params.match(pattern);
            params.count(getScanCount());
            String cursor = ScanParams.SCAN_POINTER_START;
            Set<String> keySet = new HashSet<String>();

            do
            {

                ScanResult<String> scanResult = j.scan(cursor, params);
                List<String> keyList = scanResult.getResult();
                if (keyList != null && !keyList.isEmpty())
                {
                    keySet.addAll(keyList);
                }
                cursor = scanResult.getStringCursor();
            }
            while (!cursor.equals(getScanPointerEnd()));

            return keySet;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public List<String> getList(final String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        try
        {
            List<String> valueList = j.lrange(key, Constant.REDIS_FIRST_POSITION, Constant.REDIS_LAST_POSITION);
            return valueList;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<Tuple> getRevSortedSetByScoreWithScore(final String setKey, final String max, final String min, final int offset, final int count)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        try
        {
            Set<Tuple> tuple = j.zrevrangeByScoreWithScores(setKey, max, min, offset, count);
            return tuple;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Double getScoreOfSortedSet(final String setKey, final String member)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        try
        {
            Double value = j.zscore(setKey, member);
            return value;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSet(final String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        try
        {
            Set<String> valueSet = j.smembers(key);
            return valueSet;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSet(final String key)
    {
        Jedis j = getJedis();
        try
        {
            Set<String> valueSet = getSortedSet(key, Constant.REDIS_FIRST_POSITION, Constant.REDIS_LAST_POSITION);
            return valueSet;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSet(final String key, final long from, final long to)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        try
        {
            Set<String> valueSet = j.zrange(key, from, to);
            return valueSet;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getReverseSortedSet(final String key, final long from, final long to)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        try
        {
            Set<String> valueSet = j.zrevrange(key, from, to);
            return valueSet;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<Tuple> getSortedSetByScoreWithScore(final String setKey, final long max, final long min)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");

        try
        {
            Set<Tuple> tuple = j.zrangeByScoreWithScores(setKey, max, min);
            return tuple;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Long getSortedSetCount(final String setKey, final String min, final String max)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");

        try
        {
            Long v = j.zlexcount(setKey, min, max);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public String getValue(final String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");

        try
        {
            String value = j.get(key);
            return value;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public List<String> hMGet(final String hashKey, final String[] fields)
    {
        Jedis j = getJedis();
        Validation.notNull(hashKey, "Key should not be null.");
        try
        {
            List<String> valueList = j.hmget(hashKey, fields);
            return valueList;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public List<String> mGet(final String... keys)
    {
        Validation.notNull(keys, "Keys should not be null.");

        Jedis j = getJedis();

        try
        {
            List<String> valueList = new ArrayList<String>();
            if (keys.length > 0)
            {
                valueList = j.mget(keys);
            }

            return valueList;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void removeElementFromSet(final String setKey, final String element, final String... elements)
    {
        Jedis j = getJedis();
        Validation.isTrue(!Utils.isNullOrEmpty(setKey), "Set key cannot be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(element), "The element to be removed cannot be null or empty.");

        try
        {
            j.srem(setKey, element);
            if (elements != null & elements.length > 0)
            {
                j.srem(setKey, elements);
            }

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void removeElementFromSortedSet(final String setKey, final String element, final String... elements)
    {
        Jedis j = getJedis();
        Validation.isTrue(!Utils.isNullOrEmpty(setKey), "Set key cannot be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(element), "The element to be removed cannot be null or empty.");

        try
        {
            j.zrem(setKey, element);
            if (elements != null & elements.length > 0)
            {
                j.zrem(setKey, elements);
            }

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void removeElementsFromHash(final String hashKey, final String element, final String... elements)
    {
        Jedis j = getJedis();
        Validation.isTrue(!Utils.isNullOrEmpty(hashKey), "Hash key cannot be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(element), "The element to be removed cannot be null or empty.");

        try
        {
            j.hdel(hashKey, element);
            if (elements != null & elements.length > 0)
            {
                j.hdel(hashKey, elements);
            }

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Long removeKey(final String key)
    {
        Validation.notNull(key, "Key passed should not be null.");

        Jedis j = getJedis();
        try
        {
            j.del(key);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }

        return new Long(0l);
    }

    public Map<String, String> getAllHash(String hashKey)
    {
        Jedis j = getJedis();
        Validation.notNull(hashKey, "Key should not be null.");

        try
        {
            Map<String, String> v = j.hgetAll(hashKey);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public long getSortedSetMemberIndex(String setKey, String member)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null.");
        Validation.notNull(member, "Member should not be null.");

        try
        {
            long v = j.zrank(setKey, member);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSetByScoreRev(String setKey)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null");

        try
        {
            Set<String> v = getSortedSetByScoreRev(setKey, Constant.REDIS_SORTED_SET_MIN_SCORE, Constant.REDIS_SORTED_SET_MAX_SCORE, (int) Constant.REDIS_FIRST_POSITION, Constant.REDIS_MAX_RESULTS);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSetByScoreRev(String setKey, String max, String min, int offset, int limit)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null");
        Validation.notNull(min, "Min should not be null");
        Validation.notNull(max, "Max should not be null");

        try
        {
            Set<String> v = j.zrevrangeByScore(setKey, max, min, offset, limit);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSetByScore(String setKey)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null");
        try
        {
            Set<String> v = getSortedSetByScore(setKey, Constant.REDIS_SORTED_SET_MAX_SCORE, Constant.REDIS_SORTED_SET_MIN_SCORE, (int) Constant.REDIS_FIRST_POSITION, Constant.REDIS_MAX_RESULTS);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSetByScore(String setKey, String min, String max, int offset, int limit)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null");
        Validation.notNull(min, "Min should not be null");
        Validation.notNull(max, "Max should not be null");

        try
        {
            Set<String> v = j.zrangeByScore(setKey, min, max, offset, limit);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void setConnectionFactory(final JedisConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public String setValue(final String key, final String value)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        Validation.notNull(value, "Value should not be null.");

        try
        {
            String v = j.set(key, value);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Set<String> getSortedSetRev(String setKey, long from, long to)
    {
        Validation.notNull(setKey, "Key should not be null");
        Validation.notNull(from, "From should not be null");
        Validation.notNull(to, "To should not be null");

        Jedis j = getJedis();
        try
        {
            Set<String> v = j.zrevrange(setKey, from, to);
            return v;

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public long getSortedSetCount(final String setKey)
    {
        Jedis j = getJedis();
        Validation.notNull(setKey, "Key should not be null");
        try
        {
            long v = j.zcard(setKey);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public String setValue(String key, String value, int expiresInSeconds)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "Key should not be null.");
        Validation.notNull(value, "Value should not be null.");

        try
        {
            String v = j.setex(key, expiresInSeconds, value);
            return v;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Map<String, String> getAllElementsOfHash(String hashKey)
    {
        Jedis j = getJedis();
        Validation.notNull(hashKey, "Key should not be null.");

        try
        {
            Map<String, String> elements = j.hgetAll(hashKey);
            return elements;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public Object eval(String script, List<String> keys, List<String> args)
    {
        Jedis j = getJedis();
        Validation.notNull(script, "Script should not be null.");

        try
        {
            Object elements = j.eval(script, keys, args);
            return elements;
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public ScanResult<String> scan(String cursor, String pattern, int count)
    {
        Jedis j = getJedis();
        ScanParams sp = new ScanParams();
        sp.match(pattern);
        sp.count(count);
        ScanResult result = j.scan(cursor, sp);
        getConnectionFactory().returnResource(j);
        return result;

    }

    public void removeElementsFromSortedSetBulk(Set<PipelineCacheEntity> pipelineSet)
    {
        Jedis j = getJedis();
        Validation.notNull(pipelineSet, "PipeLineSet should not be null.");

        try
        {
            Pipeline pipeline = j.pipelined();
            for (PipelineCacheEntity pce : pipelineSet)
            {
                String setKey = pce.getKey();
                String member = pce.getValue();
                pipeline.zrem(setKey, member);
            }
            pipeline.sync();

        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error while reading from cache.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public void rename(String oldKey, String newKey)
    {
        Jedis j = getJedis();
        Validation.notNull(oldKey, "oldKey should not be null");
        Validation.notNull(oldKey, "newKey should not be null");

        try
        {
            j.rename(oldKey, newKey);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }

    }

    public boolean exists(String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "key should not be null");

        try
        {
            return j.exists(key);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }

    }

    public long expire(String key, int seconds)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "key should not be null");

        try
        {
            return j.expire(key, seconds);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }

    public long zcard(String key)
    {
        Jedis j = getJedis();
        Validation.notNull(key, "key should not be null");

        try
        {
            return j.zcard(key);
        }
        catch (Exception e)
        {
            throw new CacheUnavailableException("Error during cache operations.", e);
        }
        finally
        {
            getConnectionFactory().returnResource(j);
        }
    }
}
