/**
 * 
 */
package one.group.cache.dao;

import one.group.core.enums.EntityType;

/**
 * @author ashishthorat
 *
 */
public interface EntityCountCacheDAO
{
    public void updateEntityCount(String entityCount, EntityType entityType);

    public String getEntityCount(EntityType entityType);
}
