package one.group.cache.dao;

import java.util.List;

public interface ClientCacheDAO
{
    public void addClientForAccount(String accountId, String clientId);

    public List<String> fetchAllClientsOfAccount(String accountId);
}
