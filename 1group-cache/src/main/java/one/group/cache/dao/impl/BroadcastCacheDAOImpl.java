package one.group.cache.dao.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.dao.BroadcastCacheDAO;
import one.group.entities.cache.BroadcastKey;

public class BroadcastCacheDAOImpl extends RedisBaseDAOImpl implements BroadcastCacheDAO
{
    public Set<String> fetchBroadcast(String accountId, long start, long end)
    {
        if (end < 0)
        {
            end = -1;
        }

        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        String broadcastKey = BroadcastKey.SEARCHED_BROADCAST_OF_ACCOUNT.getFormedKey(values);

        return getReverseSortedSet(broadcastKey, start, end);
    }

    public void saveBroadcastBulk(Set<PipelineCacheEntity> broadcastPipelineSet)
    {
        addToSortedSetBulk(broadcastPipelineSet);
    }

    public void removeAllBroadcast(String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        String broadcastKey = BroadcastKey.SEARCHED_BROADCAST_OF_ACCOUNT.getFormedKey(values);

        removeKey(broadcastKey);

    }

}
