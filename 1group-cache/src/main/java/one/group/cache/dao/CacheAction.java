package one.group.cache.dao;

/**
 * General cache activities
 * 
 * @author nyalfernandes
 *
 */
public interface CacheAction
{
    public void invalidate(String key, String... keys);
}
