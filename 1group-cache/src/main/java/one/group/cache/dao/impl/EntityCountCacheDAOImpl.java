/**
 * 
 */
package one.group.cache.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.cache.dao.EntityCountCacheDAO;
import one.group.core.enums.EntityType;
import one.group.entities.cache.EntityCountKey;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class EntityCountCacheDAOImpl extends RedisBaseDAOImpl implements EntityCountCacheDAO
{

    public void updateEntityCount(String entityCount, EntityType entityType)
    {
        Validation.notNull(entityCount, "EntityCount passed should not be null.");
        Validation.notNull(entityType, "EntityType passed should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("entity_type", entityType.toString());

        setValue(EntityCountKey.ENTITY_COUNT_KEY.getFormedKey(values), entityCount);
    }

    public String getEntityCount(EntityType entityType)
    {
        Validation.notNull(entityType, "EntityType passed should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("entity_type", entityType.toString());

        return getValue(EntityCountKey.ENTITY_COUNT_KEY.getFormedKey(values));
    }
}
