package one.group.cache.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import one.group.cache.dao.AccountCacheDAO;
import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.entities.cache.AccountKey;
import one.group.entities.cache.ChatKey;
import one.group.entities.cache.EntityCountKey;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;
import redis.clients.jedis.ScanResult;

public class AccountCacheDAOImpl extends RedisBaseDAOImpl implements AccountCacheDAO
{
    private HashMap<String, String> key = new HashMap<String, String>();

    public Set<String> fetchOnlineAccounts()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", "*");
        Set<String> keys = new HashSet<String>();

        Map<String, String> entries = getAllHash(AccountKey.HASH_ACCOUNT_ONLINE.getKey());
        Long currentTime = new Date().getTime();

        for (String key : entries.keySet())
        {
            Long accountMilli = Long.parseLong(entries.get(key));
            if ((currentTime - accountMilli) < Constant.ONE_HOUR_IN_MS)
            {
                keys.add(key);
            }
        }

        return keys;
    }

    public Long fetchOnlineAccountTime(String accountId)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");
        String time = getHash(AccountKey.HASH_ACCOUNT_ONLINE.getKey(), accountId);
        if (time == null)
        {
            return null;
        }
        return Long.parseLong(time);
    }

    public void deleteOnlineKey(final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null.");
        removeElementsFromHash(AccountKey.HASH_ACCOUNT_ONLINE.getKey(), accountId);
    }

    public void createOnlineKey(final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null.");
        addToHash(AccountKey.HASH_ACCOUNT_ONLINE.getKey(), accountId, "" + new Date().getTime());
    }

    public void createAccountBlockByKey(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "AccountId should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(requestByAccountId), "RequestByAccount should not be null or empty");

        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CACHE_ACCOUNT_ACCOUNT_ID, accountId);
        setValue(ChatKey.SET_ACCOUNT_BLOCKED_BY.getFormedKey(values), requestByAccountId);
    }

    public void removeAccountFromBlockedBy(String accountId, String requestByAccountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "AccountId should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(requestByAccountId), "RequestByAccount should not be null or empty");

        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        removeElementFromSortedSet(ChatKey.SET_ACCOUNT_BLOCKED_BY.getFormedKey(values), requestByAccountId);
    }

    public ScanResult<String> fetchAccountsToSendSmsNotification(String cursor, String pattern, int count)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(cursor), "Cursor should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(pattern), "Pattern should not be null or empty");
        Validation.isTrue(!Utils.isNull(count), "Count should not be null or empty");

        return scan(cursor, pattern, count);
    }

    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(timestampKey), "Timestamp key should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(timestamp), "Timestamp should not be null or empty");

        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);

        addToHash(AccountKey.ACCOUNT_SMS_NOTIFICATION.getFormedKey(values), timestampKey, timestamp);

    }

    public Map<String, String> fetchChatSmsNotificationTimestamp(String accountkey)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountkey), "Accountkey should not be null or empty");

        return getAllElementsOfHash(accountkey);
    }

    public String fetchCount()
    {
        key.put("entity_type", EntityType.ACCOUNT.toString());
        return getValue(EntityCountKey.ENTITY_COUNT_KEY.getFormedKey(key));
    }

    public void updateCount(long count)
    {
        key.put("entity_type", EntityType.ACCOUNT.toString());
        setValue(EntityCountKey.ENTITY_COUNT_KEY.getFormedKey(key), String.valueOf(count));
    }

    public void saveAccountInviteCode(String accountId, String inviteCode, int expiresInSeconds)
    {
        Validation.notNull(accountId, "Account is should not be null");
        Validation.notNull(inviteCode, "Invite code should not be null");
        Validation.notNull(expiresInSeconds, "Expiry time should not be null or empty");

        key.put("account_id", accountId);
        setValue(AccountKey.ACCOUNT_INVITE_CODE.getFormedKey(key), inviteCode, expiresInSeconds);
    }

    public String fetchInviteCodeByAccount(String accountId)
    {
        Validation.notNull(accountId, "Account is should not be null");

        key.put("account_id", accountId);
        return getValue(AccountKey.ACCOUNT_INVITE_CODE.getFormedKey(key));
    }

}
