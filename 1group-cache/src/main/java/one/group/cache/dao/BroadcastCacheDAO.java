package one.group.cache.dao;

import java.util.Set;

import one.group.cache.PipelineCacheEntity;

public interface BroadcastCacheDAO extends CacheBaseDAO
{
    public Set<String> fetchBroadcast(String accountId, long start, long end);

    public void saveBroadcastBulk(Set<PipelineCacheEntity> broadcastPipelineSet);
    
    public void removeAllBroadcast(String accountId);
}
