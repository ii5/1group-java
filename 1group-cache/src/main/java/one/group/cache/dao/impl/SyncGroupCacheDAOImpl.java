package one.group.cache.dao.impl;

import one.group.cache.dao.SyncGroupCacheDAO;
import one.group.entities.cache.GroupCountKey;
import one.group.utils.validation.Validation;

public class SyncGroupCacheDAOImpl extends RedisBaseDAOImpl implements SyncGroupCacheDAO
{

    public void addCount(String count)
    {
        Validation.notNull(count, "Subscription entity key passed should not be null.");

        addToHash(GroupCountKey.GROUP_COUNT.toString(), "total_count", count);

    }

    public String fetchGroupContent(String groupkey)
    {
        return getHash(GroupCountKey.GROUP_COUNT.toString(), "total_count");
    }
}
