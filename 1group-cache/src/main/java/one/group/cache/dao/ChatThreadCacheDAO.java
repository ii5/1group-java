package one.group.cache.dao;

import java.util.Map;
import java.util.Set;

public interface ChatThreadCacheDAO extends CacheBaseDAO
{
    public String updateChatThreadCursor = "update_chat_thread_cursor.lua";

    /**
     * Fetch chat thread list of an account by activity
     * 
     * @param accountId
     * @param offset
     * @param limit
     * @return
     */
    public Set<String> fetchChatThreadListOfAccountByActivity(String accountId, int offset, int limit);

    /**
     * Fetch chat thread list of an account by joined
     * 
     * @param accountId
     * @param offset
     * @param limit
     * @return
     */
    public Set<String> fetchChatThreadListOfAccountByJoined(String accountId, int offset, int limit);

    /**
     * Get score of chat thread
     * 
     * @param chatThreadId
     * @param accountId
     * @param indexBy
     * @return
     */
    public Double fetchChatThreadScore(String chatThreadId, String accountId, String indexBy);

    /**
     * Save chat thread by activity
     * 
     * @param chatThreadId
     * @param accountId
     */
    public void saveChatThreadByActivity(String chatThreadId, String accountId);

    /**
     * Update chat thread participant
     * 
     * @param chatThreadId
     * @param accountIdKey
     * @param accountIdValue
     */
    public void updateChatThreadParticipants(String chatThreadId, String accountIdKey, String accountIdValue);

    /**
     * Get chat thread participants
     * 
     * @param chatThreadId
     * @return
     */
    public Map<String, String> getChatThreadParticipants(String chatThreadId);

    /**
     * Get chat thread index by activity
     * 
     * @param accountId
     * @param hashKey
     * @return
     */
    public long getChatThreadIndexByActivity(String accountId, String hashKey);

    /**
     * Get chat thread index by joined
     * 
     * @param accountId
     * @param hashKey
     * @return
     */
    public long getChatThreadIndexByJoined(String accountId, String hashKey);

    /**
     * Save chat thread by creation
     * 
     * @param chatThreadId
     * @param accountId
     */
    public void saveChatThreadByJoined(String chatThreadId, String accountId);

    /**
     * Update chat thread read index
     * 
     * @param chatThreadId
     * @param accountId
     * @param index
     */
    public void updateChatThreadCursorReadIndex(String chatThreadId, String accountId, String index);

    /**
     * Update chat thread received index
     * 
     * @param chatThreadId
     * @param accountId
     * @param index
     */
    public void updateChatThreadCursorReceivedIndex(String chatThreadId, String accountId, String index);

    /**
     * Fetch chat thread cursor's read index
     * 
     * @param chatThreadId
     * @param accountId
     * @return
     */
    public String fetchChatThreadCursorReadIndex(String chatThreadId, String accountId);

    /**
     * Fetch chat thread cursor's received index
     * 
     * @param chatThreadId
     * @param accountId
     * @return
     */
    public String fetchChatThreadCursorReceivedIndex(String chatThreadId, String accountId);

    /**
     * Fetch count of chat threads by activity
     * 
     * @param accountId
     * @return
     */
    public long fetchChatThreadCountByActivity(String accountId);

    /**
     * Fetch count of chat threads by joined
     * 
     * @param accountId
     * @return
     */
    public long fetchChatThreadCountByJoined(String accountId);

    /**
     * Save property listing short reference for chat thread
     * 
     * @param chatThreadId
     * @param propertyListingId
     */
    public void saveChatThreadForPropertyListing(String chatThreadId, String propertyListingId);

    /**
     * Fetch chat thread by property listing
     * 
     * @param propertyListingId
     * @return
     */
    public Set<String> fetchChatThreadByPropertyListing(String propertyListingId);

    public Object updateChatThreadCursor(String chatThreadId, String accountId, String receivedIndex, String readIndex);

    public Set<String> fetchAllChatThreads(String pattern);
}
