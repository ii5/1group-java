package one.group.cache.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.cache.dao.ClientCacheDAO;
import one.group.entities.cache.ClientKey;

public class ClientCacheDAOImpl extends RedisBaseDAOImpl implements ClientCacheDAO
{

    public void addClientForAccount(final String accountId, final String clientId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        addToList(ClientKey.CLIENTS_BY_ACCOUNT.getFormedKey(values), clientId, null);
    }

    public List<String> fetchAllClientsOfAccount(final String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        return getList(ClientKey.CLIENTS_BY_ACCOUNT.getFormedKey(values));
    }

}
