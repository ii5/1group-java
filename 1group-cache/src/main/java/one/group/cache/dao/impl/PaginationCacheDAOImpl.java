package one.group.cache.dao.impl;

import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.dao.PaginationCacheDAO;

public class PaginationCacheDAOImpl extends RedisBaseDAOImpl implements PaginationCacheDAO
{

    public void saveRecords(Set<PipelineCacheEntity> records)
    {
        addToSortedSetBulk(records);
    }

    public Set<String> fetchRecords(String paginationId, int start, int end)
    {
        return getSortedSet(paginationId, start, end);
    }

    public boolean isKeyExists(String key)
    {
        return exists(key);
    }

    public long setExpiryTimeOfKey(String key, int seconds)
    {
        return expire(key, seconds);

    }

    public long fetchTotalRecordsCount(String paginationKey)
    {
        return zcard(paginationKey);
    }

}
