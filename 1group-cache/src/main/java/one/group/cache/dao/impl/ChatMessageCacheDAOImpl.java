package one.group.cache.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.dao.ChatMessageCacheDAO;
import one.group.core.Constant;
import one.group.entities.cache.ChatKey;
import one.group.entities.cache.SubscriptionKey;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Tuple;

public class ChatMessageCacheDAOImpl extends RedisBaseDAOImpl implements ChatMessageCacheDAO
{
    private static final Logger logger = LoggerFactory.getLogger(ChatMessageCacheDAOImpl.class);

    public Set<String> fetchChatMessagesOfChatThread(final String chatThreadId, final int offset, final int limit)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        String key = ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values);
        return getSortedSetByScore(key, Constant.REDIS_SORTED_SET_MIN_SCORE, Constant.REDIS_SORTED_SET_MAX_SCORE, offset, limit);
    }

    public Set<Tuple> fetchLastChatMessageOfChatThread(final String chatThreadId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        return getRevSortedSetByScoreWithScore(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values), Constant.REDIS_SORTED_SET_MAX_SCORE, Constant.REDIS_SORTED_SET_MIN_SCORE, 0, 1);
    }

    public String saveChatMessage(final String chatThreadId, final String chatMessage)
    {
        String key = Utils.randomUUID();

        // Save chat message in hash
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        addToHash(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD.getFormedKey(values), key, chatMessage);

        // Fetch total number of chat message and use total as score for next
        // chat message
        Double score = Double.valueOf(fetchChatMessageCount(chatThreadId));

        // Save message key with score
        addToSortedSet(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values), score, key);

        return key;
    }

    public Double fetchChatMessageIndex(String chatThreadId, String chatMessageKey)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        return getScoreOfSortedSet(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values), chatMessageKey);
    }

    public String fetchChatMessageContent(String chatThreadId, String chatMessagekey)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        return getHash(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD.getFormedKey(values), chatMessagekey);
    }

    public long fetchChatMessageCount(String chatThreadId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        return getSortedSetCount(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values));
    }

    public Object saveChatMessageUsingLua(String chatThreadId, String fromAccountId, String toAccountId, String chatMessageJson)
    {
        Map<String, String> cT = new HashMap<String, String>();
        cT.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        Map<String, String> fA = new HashMap<String, String>();
        fA.put(Constant.CHAT_KEY_ACCOUNT_ID, fromAccountId);

        Map<String, String> tA = new HashMap<String, String>();
        tA.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);

        Map<String, String> cTfA = new HashMap<String, String>();
        cTfA.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        cTfA.put(Constant.CHAT_KEY_ACCOUNT_ID, fromAccountId);

        Map<String, String> cTtA = new HashMap<String, String>();
        cTtA.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        cTtA.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);

        Map<String, String> sTcTfA = new HashMap<String, String>();
        sTcTfA.put("sc_type", "chat");
        sTcTfA.put("sc_entity_key", chatThreadId);
        sTcTfA.put(Constant.CHAT_KEY_ACCOUNT_ID, fromAccountId);

        Map<String, String> sTcTtA = new HashMap<String, String>();
        sTcTtA.put("sc_type", "chat");
        sTcTtA.put("sc_entity_key", chatThreadId);
        sTcTtA.put(Constant.CHAT_KEY_ACCOUNT_ID, toAccountId);

        List<String> keys = new ArrayList<String>();
        keys.add(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD.getFormedKey(cT));
        keys.add(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(cT));
        keys.add(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(fA));
        keys.add(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(fA));
        keys.add(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(tA));
        keys.add(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(tA));
        keys.add(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(cTfA));
        keys.add(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(cTtA));
        keys.add(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(sTcTfA));
        keys.add(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(sTcTtA));
        keys.add(ChatKey.CHAT_THREAD_PARTICIPANTS.getFormedKey(cT));

        List<String> args = new ArrayList<String>();
        args.add(chatThreadId);
        args.add(chatMessageJson);
        args.add(Utils.randomUUID());
        args.add(fromAccountId);
        args.add(toAccountId);
        // TODO: move time stamp to service layer
        args.add(String.valueOf(Utils.getSystemTime()));

        String script = Utils.getResource(saveChatMessage);

        Object chatMessageindex = eval(script, keys, args);

        return chatMessageindex;
    }
}
