package one.group.cache.dao;

import java.util.Set;

import redis.clients.jedis.Tuple;

public interface ChatMessageCacheDAO extends CacheBaseDAO
{
    public String saveChatMessage = "save_chat_message.lua";

    /**
     * Fetch chat messages of chat thread
     * 
     * @param chatThreadId
     * @param offset
     * @param limit
     * @return
     */

    public Set<String> fetchChatMessagesOfChatThread(String chatThreadId, int offset, int limit);

    /**
     * Fetch last chat message of chat thread
     * 
     * @param chatThreadId
     * @return
     */
    public Set<Tuple> fetchLastChatMessageOfChatThread(String chatThreadId);

    /**
     * Save chat message
     * 
     * @param chatThreadId
     * @param chatMessage
     * @return
     */
    public String saveChatMessage(String chatThreadId, String chatMessage);

    /**
     * Fetch chat message index
     * 
     * @param chatThreadId
     * @param chatMessageKey
     * @return
     */
    public Double fetchChatMessageIndex(String chatThreadId, String chatMessageKey);

    /**
     * Fetch chat message content
     * 
     * @param chatThreadId
     * @param chatMessagekey
     * @return
     */
    public String fetchChatMessageContent(String chatThreadId, String chatMessagekey);

    /**
     * Fetch message count of chat thread
     * 
     * @param chatThreadId
     * @return
     */
    public long fetchChatMessageCount(String chatThreadId);

    /**
     * Save chat message using Lua script
     * 
     * @param chatThreadId
     * @param fromAccountId
     * @param toAccountId
     * @param chatMessageJson
     * @return
     */
    public Object saveChatMessageUsingLua(String chatThreadId, String fromAccountId, String toAccountId, String chatMessageJson);
}
