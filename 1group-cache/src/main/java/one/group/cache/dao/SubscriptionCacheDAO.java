package one.group.cache.dao;

import java.util.Set;

import one.group.core.enums.EntityType;

/**
 * Cache DAO that handles subscription
 * 
 * @author nyalfernandes
 *
 */
public interface SubscriptionCacheDAO
{
    public Set<String> fetchAllSubscriptionsOf(String accountId);

    public Set<String> fetchSubscribersOf(String subscriptionEntityId, EntityType subscriptionEntityType);

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType, int forTime);

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType);

    public void deleteSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType);

    public Long getTTLForSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType);

    public String isSubscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType);
    
    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType);
    
    public void removeSubscriptionByKey(String key);
}
