package one.group.cache.dao.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import one.group.cache.dao.SubscriptionCacheDAO;
import one.group.core.enums.EntityType;
import one.group.entities.cache.SubscriptionKey;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SubscriptionCacheDAOImpl extends RedisBaseDAOImpl implements SubscriptionCacheDAO
{

    public Set<String> fetchSubscribersOf(String subscriptionEntityKey, EntityType subscriptionEntityType)
    {
        Validation.notNull(subscriptionEntityKey, "Subscription entity id passed should not be null.");
        Validation.notNull(subscriptionEntityType, "Subscription entity type passed should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionEntityType.toString());
        values.put("sc_entity_key", subscriptionEntityKey);
        values.put("account_id", "*");

        return scan(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));
    }

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType, int forTime)
    {
        Validation.notNull(subscriptionEntityId, "Subscription entity key passed should not be null.");
        Validation.notNull(subscriptionType, "Subscription entity type passed should not be null.");
        Validation.notNull(subscribingAccountId, "Account id trying to subscribe should not be null.");
        Validation.isTrue(forTime > 1, "The subscription time should not be less that 0.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionType.toString());
        values.put("sc_entity_key", subscriptionEntityId);
        values.put("account_id", subscribingAccountId);

        setValue(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values), subscribingAccountId, forTime);

    }

    public void addSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        Validation.notNull(subscriptionEntityId, "Subscription entity key passed should not be null.");
        Validation.notNull(subscriptionType, "Subscription entity type passed should not be null.");
        Validation.notNull(subscribingAccountId, "Account id trying to subscribe should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionType.toString());
        values.put("sc_entity_key", subscriptionEntityId);
        values.put("account_id", subscribingAccountId);

        setValue(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values), subscribingAccountId);
    }

    public void deleteSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionEntityType)
    {
        Validation.notNull(subscriptionEntityId, "Subscription entity key passed should not be null.");
        Validation.notNull(subscriptionEntityType, "Subscription entity type passed should not be null.");
        Validation.notNull(subscribingAccountId, "Subscription account id passed should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionEntityType.toString());
        values.put("sc_entity_key", subscriptionEntityId);
        values.put("account_id", subscribingAccountId);

        Set<String> keySet = scan(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));
        
        if (keySet != null)
        {
            for (String key : keySet)
            {
                removeKey(key);
            }
        }
    }

    public Set<String> fetchAllSubscriptionsOf(String accountId)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");
        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", "*");
        values.put("sc_entity_key", "*");
        values.put("account_id", accountId);

        return scan(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));
    }

    public Long getTTLForSubscription(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        Validation.notNull(subscriptionEntityId, "Subscription entity key passed should not be null.");
        Validation.notNull(subscriptionType, "Subscription entity type passed should not be null.");
        Validation.notNull(subscribingAccountId, "Subscription account id passed should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionType.toString());
        values.put("sc_entity_key", subscriptionEntityId);
        values.put("account_id", subscribingAccountId);

        return getTTL(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));
    }

    public String isSubscribe(String subscribingAccountId, String subscriptionEntityId, EntityType subscriptionType)
    {
        Validation.notNull(subscriptionEntityId, "Subscription entity key passed should not be null.");
        Validation.notNull(subscriptionType, "Subscription entity type passed should not be null.");
        Validation.notNull(subscribingAccountId, "Account id trying to subscribe should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionType.toString());
        values.put("sc_entity_key", subscriptionEntityId);
        values.put("account_id", subscribingAccountId);

        return getValue(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));

    }

    public void removeSubscription(String subscribingAccountId, EntityType subscriptionType)
    {
        Validation.notNull(subscribingAccountId, "Subscribing account id should not be null.");
        Validation.notNull(subscriptionType, "Subscription Type should not be null.");
        
        Map<String, String> values = new HashMap<String, String>();
        values.put("sc_type", subscriptionType.toString());
        values.put("account_id", subscribingAccountId);
        values.put("sc_entity_key", "*");
        
        Set<String> keySet = scan(SubscriptionKey.SUBSCRIPTION_KEY.getFormedKey(values));
        
        for (String key : keySet)
        {
            removeKey(key);
        }
    }
    
    public void removeSubscriptionByKey(String key)
    {
        Validation.notNull(key, "The subscription key passed to be deleted should not be null.");
        removeKey(key);
    }
}
