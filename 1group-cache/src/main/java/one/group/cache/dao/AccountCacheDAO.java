package one.group.cache.dao;

import java.util.Map;
import java.util.Set;

import redis.clients.jedis.ScanResult;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface AccountCacheDAO extends CacheBaseDAO
{
    /**
     * Returns all accounts online.
     * 
     * @return
     */
    public Set<String> fetchOnlineAccounts();

    /**
     * 
     * @param accountId
     * @return
     */
    public Long fetchOnlineAccountTime(String accountId);

    /**
     * Mark the given account as online.
     * 
     * @param accountId
     */
    public void deleteOnlineKey(String accountId);

    /**
     * Marks the given account as offline.
     * 
     * @param accountId
     */
    public void createOnlineKey(String accountId);

    /**
     * function that store accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void createAccountBlockByKey(String accountId, String requestByAccountId);

    /**
     * function that delete accountId who requesting block .
     *
     * @param accountId
     *            the account id
     */
    public void removeAccountFromBlockedBy(String accountId, String requestByAccountId);

    public ScanResult<String> fetchAccountsToSendSmsNotification(String cursor, String pattern, int count);

    public void updateChatSmsNotificationTimestamp(String accountId, String timestampKey, String timestamp);

    public Map<String, String> fetchChatSmsNotificationTimestamp(String accountKey);

    public String fetchCount();

    public void updateCount(long count);

    /**
     * Save invite code for account for specific time duration
     * 
     */
    public void saveAccountInviteCode(String accountId, String inviteCode, int expiresInSeconds);

    /**
     * Fetch invite code for account from cache
     * 
     * @param accountId
     * @return
     */
    public String fetchInviteCodeByAccount(String accountId);
}
