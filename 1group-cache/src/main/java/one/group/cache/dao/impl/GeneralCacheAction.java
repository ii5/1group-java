package one.group.cache.dao.impl;

import one.group.cache.dao.CacheAction;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class GeneralCacheAction extends RedisBaseDAOImpl implements CacheAction
{
    private static final Logger logger = LoggerFactory.getLogger(GeneralCacheAction.class);
    
    public void invalidate(String key, String... keys)
    {
        Validation.notNull(key, "The key to be invalidated should not be null.");
        removeKey(key);

        if (keys != null)
        {
            for (String k : keys)
            {
                removeKey(k);
            }
        }
    }
    
}
