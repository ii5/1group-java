package one.group.cache.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.PipelineCacheEntity;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.Tuple;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface CacheBaseDAO
{

    public Long addToHash(String hashKey, String field, String value);

    public String getValue(String key);

    public Long getTTL(String key);

    public void addToList(String listKey, String value, String... values);

    public void addToSet(String setKey, String value, String... values);

    public void addToSortedSet(String setKey, double itemScore, String member);

    public void addToSortedSetBulk(Set<PipelineCacheEntity> pipelineSet);

    public void addToSortedSet(String setKey, Map<String, Double> scoredMembers);

    public String setValue(String key, String value, int expiresInSeconds);

    public String setValue(String key, String value);

    public String getHash(String hashKey, String field);

    public Set<String> scan(String pattern);

    public List<String> getList(String key);

    public Double getScoreOfSortedSet(String setKey, String member);

    public void removeElementFromSet(String setKey, String element, String... elements);

    public void removeElementFromSortedSet(String setKey, String element, String... elements);

    public void removeElementsFromHash(String hashKey, String element, String... elements);

    public Set<String> getSet(String key);

    public Set<String> getSortedSet(String key);

    public Set<String> getSortedSet(String key, long from, long to);

    public Set<Tuple> getSortedSetByScoreWithScore(String setKey, long min, long max);

    public Map<String, String> getAllElementsOfHash(String hashKey);

    public Long getSortedSetCount(String setKey, String min, String max);

    public Set<Tuple> getRevSortedSetByScoreWithScore(String setKey, String max, String min, int start, int limit);

    public List<String> hMGet(String hashKey, String[] fields);

    public Long removeKey(String key);

    public Map<String, String> getAllHash(String key);

    public long getSortedSetMemberIndex(String setKey, String member);

    public Set<String> getSortedSetByScoreRev(String setKey, String max, String min, int offset, int limit);

    public Set<String> getSortedSetByScoreRev(String setKey);

    public Set<String> getSortedSetByScore(String setKey, String min, String max, int offset, int limit);

    public Set<String> getSortedSetByScore(String setKey);

    public Set<String> getSortedSetRev(String setKey, long from, long to);

    public long getSortedSetCount(final String setKey);

    public Object eval(String script, List<String> keys, List<String> args);

    public ScanResult scan(String cursor, String pattern, int count);

    public void removeElementsFromSortedSetBulk(Set<PipelineCacheEntity> pipelineSet);

    public void rename(String oldKey, String newKey);

    public void pipelinedEval(Set<PipelineCacheEntity> pipelineSet, String script);

    public boolean exists(String key);

    public long expire(String key, int seconds);

    public long zcard(String key);

    public Set<String> keys(final String pattern);
}
