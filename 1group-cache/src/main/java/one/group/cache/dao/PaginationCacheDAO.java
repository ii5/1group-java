package one.group.cache.dao;

import java.util.Set;

import one.group.cache.PipelineCacheEntity;

public interface PaginationCacheDAO extends CacheBaseDAO
{
    public void saveRecords(Set<PipelineCacheEntity> records);

    /**
     * Fetch records from set of records. Start & End are inclusive in result
     * set
     * 
     * @param paginationKey
     * @param start
     * @param end
     * @return
     */
    public Set<String> fetchRecords(final String paginationKey, final int start, final int end);

    public boolean isKeyExists(final String key);

    public long setExpiryTimeOfKey(String key, int seconds);

    public long fetchTotalRecordsCount(String paginationKey);
}
