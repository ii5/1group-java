package one.group.cache.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.cache.dao.ChatThreadCacheDAO;
import one.group.core.Constant;
import one.group.core.enums.ChatThreadType;
import one.group.entities.cache.ChatKey;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatThreadCacheDAOImpl extends RedisBaseDAOImpl implements ChatThreadCacheDAO
{

    private static final Logger logger = LoggerFactory.getLogger(ChatThreadCacheDAOImpl.class);

    public Set<String> fetchChatThreadListOfAccountByActivity(final String accountId, final int offset, final int limit)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);
        String key = ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values);

        return getSortedSetByScore(key, Constant.REDIS_SORTED_SET_MIN_SCORE, Constant.REDIS_SORTED_SET_MAX_SCORE, offset, limit);
    }

    public Set<String> fetchChatThreadListOfAccountByJoined(final String accountId, final int offset, final int limit)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);
        String key = ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(values);

        return getSortedSetByScore(key, Constant.REDIS_SORTED_SET_MIN_SCORE, Constant.REDIS_SORTED_SET_MAX_SCORE, offset, limit);
    }

    public Double fetchChatThreadScore(final String chatThreadId, final String accountId, final String indexBy)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);
        if (ChatThreadType.BY_ACTIVITY.getType().equals(indexBy))
        {
            return getScoreOfSortedSet(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values), chatThreadId);
        }

        if (ChatThreadType.BY_CREATION.getType().equals(indexBy))
        {
            return getScoreOfSortedSet(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values), chatThreadId);
        }
        return null;

    }

    public void saveChatThreadByJoined(String chatThreadId, String accountId)
    {

        Double timeStamp = (double) Utils.getSystemTime();

        Map<String, String> valuesAccountId = new HashMap<String, String>();
        valuesAccountId.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        Double isChatThreadAlreadyCreated = getScoreOfSortedSet(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(valuesAccountId), chatThreadId);

        if (isChatThreadAlreadyCreated == null)
        {
            addToSortedSet(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(valuesAccountId), timeStamp, chatThreadId);
        }
    }

    public void saveChatThreadByActivity(final String chatThreadId, final String accountId)
    {

        Double timeStamp = (double) Utils.getSystemTime();

        Map<String, String> valuesAccountId = new HashMap<String, String>();
        valuesAccountId.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        addToSortedSet(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(valuesAccountId), timeStamp, chatThreadId);
    }

    public void updateChatThreadParticipants(String chatThreadId, String accountIdKey, String accountIdValue)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        addToHash(ChatKey.CHAT_THREAD_PARTICIPANTS.getFormedKey(values), accountIdKey, accountIdValue);
    }

    public Map<String, String> getChatThreadParticipants(String chatThreadId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);

        return getAllHash(ChatKey.CHAT_THREAD_PARTICIPANTS.getFormedKey(values));
    }

    public long getChatThreadIndexByActivity(String accountId, String hashKey)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getSortedSetMemberIndex(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values), hashKey);
    }

    public long getChatThreadIndexByJoined(String accountId, String hashKey)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getSortedSetMemberIndex(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(values), hashKey);
    }

    public void updateChatThreadCursorReadIndex(String chatThreadId, String accountId, String index)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        addToHash(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(values), Constant.REDIS_HASHKEY_READ_INDEX, index);
    }

    public void updateChatThreadCursorReceivedIndex(String chatThreadId, String accountId, String index)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        addToHash(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(values), Constant.REDIS_HASHKEY_RECEIVED_INDEX, index);
    }

    public String fetchChatThreadCursorReadIndex(String chatThreadId, String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getHash(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(values), Constant.REDIS_HASHKEY_READ_INDEX);
    }

    public String fetchChatThreadCursorReceivedIndex(String chatThreadId, String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getHash(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(values), Constant.REDIS_HASHKEY_RECEIVED_INDEX);
    }

    public long fetchChatThreadCountByActivity(String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getSortedSetCount(ChatKey.CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT.getFormedKey(values));
    }

    public long fetchChatThreadCountByJoined(String accountId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        return getSortedSetCount(ChatKey.CHAT_THREADS_BY_JOINED_OF_ACCOUNT.getFormedKey(values));
    }

    public void saveChatThreadForPropertyListing(String chatThreadId, String propertyListingId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_PROPERTY_LISTING_ID, propertyListingId);

        addToSet(ChatKey.CHAT_THREADS_OF_PROPERTY_LISTING.getFormedKey(values), chatThreadId);
    }

    public Set<String> fetchChatThreadByPropertyListing(String propertyListingId)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_PROPERTY_LISTING_ID, propertyListingId);

        return getSet(ChatKey.CHAT_THREADS_OF_PROPERTY_LISTING.getFormedKey(values));
    }

    public Object updateChatThreadCursor(String chatThreadId, String accountId, String receivedIndex, String readIndex)
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put(Constant.CHAT_KEY_CHAT_THREAD_ID, chatThreadId);
        values.put(Constant.CHAT_KEY_ACCOUNT_ID, accountId);

        List<String> keys = new ArrayList<String>();
        keys.add(ChatKey.CHAT_THREAD_CURSORS.getFormedKey(values));
        keys.add(ChatKey.CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET.getFormedKey(values));

        List<String> args = new ArrayList<String>();
        args.add(Constant.REDIS_HASHKEY_RECEIVED_INDEX);
        args.add(receivedIndex);
        args.add(Constant.REDIS_HASHKEY_READ_INDEX);
        args.add(readIndex);

        String script = Utils.getResource(updateChatThreadCursor);

        Object updatedChatThreadCursor = eval(script, keys, args);

        return updatedChatThreadCursor;
    }

    public Set<String> fetchAllChatThreads(String pattern)
    {
        if (pattern == null || pattern == "")
        {
            return new HashSet<String>();
        }
        return keys(pattern);
    }

}
