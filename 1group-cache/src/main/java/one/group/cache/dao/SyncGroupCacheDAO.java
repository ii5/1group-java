package one.group.cache.dao;

public interface SyncGroupCacheDAO
{
    public void addCount(String count);

    public String fetchGroupContent(String groupkey);
}
