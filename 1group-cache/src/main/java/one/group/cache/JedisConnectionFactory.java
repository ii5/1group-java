package one.group.cache;

import one.group.utils.validation.Validation;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * A factory building a thread safe {@link JedisPool} of {@link Jedis}
 * instances.
 * 
 * @author nyalfernandes
 * 
 */
public class JedisConnectionFactory
{
    private static final Logger logger = LoggerFactory.getLogger(JedisConnectionFactory.class);

    /**
     * The maximum count of Jedis instances the pool can hold.
     */
    private Integer poolSize = 100;

    /**
     * The initial Jedis instances available in the pool.
     */
    private Integer minimumConnections = 10;

    /**
     * The time to wait before retrying for a resource.
     */
    private Long waitTime = 5000L;

    private String host = Protocol.DEFAULT_HOST;

    private Integer port = Protocol.DEFAULT_PORT;
    
    private Integer jedisTimeout = 10000;
    
    private Boolean blockWhenExhausted = true;
    
    private Boolean testOnBorrow = false;
    
    private Boolean testOnCreate = false;
    
    private Boolean testOnReturn = false;
    
    private Boolean testWhileIdle = false;

    private static JedisPool pool;
    
    public Boolean getTestOnBorrow()
    {
        return testOnBorrow;
    }
    
    public Boolean getTestOnCreate()
    {
        return testOnCreate;
    }
    
    public Boolean getTestOnReturn()
    {
        return testOnReturn;
    }
    
    public Boolean getTestWhileIdle()
    {
        return testWhileIdle;
    }
    
    public void setTestOnBorrow(Boolean testOnBorrow)
    {
        this.testOnBorrow = testOnBorrow;
    }
    
    public void setTestOnCreate(Boolean testOnCreate)
    {
        this.testOnCreate = testOnCreate;
    }
    
    public void setTestOnReturn(Boolean testOnReturn)
    {
        this.testOnReturn = testOnReturn;
    }
    
    public void setTestWhileIdle(Boolean testWhileIdle)
    {
        this.testWhileIdle = testWhileIdle;
    }
    
    public Boolean getBlockWhenExhausted()
    {
        return blockWhenExhausted;
    }
    
    public void setBlockWhenExhausted(Boolean blockWhenExhausted)
    {
        this.blockWhenExhausted = blockWhenExhausted;
    }
    
    public Integer getJedisTimeout()
    {
        return jedisTimeout;
    }
    
    public void setJedisTimeout(Integer jedisTimeout)
    {
        this.jedisTimeout = jedisTimeout;
    }

    public static Logger getLogger()
    {
        return logger;
    }

    public static JedisPool getPool()
    {
        return pool;
    }

    

    public static void setPool(final JedisPool pool)
    {
        JedisConnectionFactory.pool = pool;
    }

    public String getHost()
    {
        return host;
    }

    public Integer getMinimumConnections()
    {
        return minimumConnections;
    }

    private JedisPoolConfig getPoolConfig()
    {
        JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(poolSize);
        poolConfig.setMaxWaitMillis(waitTime);
        poolConfig.setMaxIdle(poolSize);
        poolConfig.setBlockWhenExhausted(blockWhenExhausted);
        poolConfig.setTestOnBorrow(testOnBorrow);
        poolConfig.setTestOnCreate(testOnCreate);
        poolConfig.setTestOnReturn(testOnReturn);
        poolConfig.setTestWhileIdle(testWhileIdle);

        return poolConfig;
    }

    public Integer getPoolSize()
    {
        return poolSize;
    }

    public Integer getPort()
    {
        return port;
    }

    /**
     * Returns a Jedis connection from the pool.
     * 
     * @return
     */
    public Jedis getResource()
    {
        if (pool == null)
        {
            initPool();
        }

        try
        {
            return pool.getResource();
        }
        catch (JedisConnectionException e)
        {
            waitAndRetryForResource();
            logger.warn("Redis connection pool empty. No connections could be returned.", e);
        }

        return null;
    }

    public Long getWaitTime()
    {
        return waitTime;
    }

    public void initPool()
    {
        if (pool == null)
        {
            pool = new JedisPool(getPoolConfig(), host, port, jedisTimeout);
        }
        else
        {
            logger.warn("Redis Conneciton pool already initialised!");
        }
    }

    /**
     * Returns a resource to the pool.
     * 
     * @param jedis
     */
    public void returnResource(final Jedis jedis)
    {
        Validation.notNull(jedis, "The Jedis instance to be returned should not be null.");

        logger.debug("Thanks for returning " + jedis + ". Borrow again! :)");

        pool.returnResourceObject(jedis);
        logger.debug("== Return");
        logger.debug("A:" + getPool().getNumActive());
        logger.debug("I:" + getPool().getNumIdle());
        logger.debug("W:" + getPool().getNumWaiters());

    }

    public void setHost(final String host)
    {
        this.host = host;
    }

    public void setMinimumConnections(final Integer minimumConnections)
    {
        this.minimumConnections = minimumConnections;
    }

    public void setPoolSize(final Integer poolSize)
    {
        this.poolSize = poolSize;
    }

    public void setPort(final Integer port)
    {
        this.port = port;
    }

    public void setWaitTime(final Long waitTime)
    {
        this.waitTime = waitTime;
    }

    // Helper methods
    private void waitAndRetryForResource()
    {

    }
    
    public static void main(final String[] args) throws Exception
    {
        BasicConfigurator.configure();
        final JedisConnectionFactory cf = new JedisConnectionFactory();
        cf.setHost("localhost");
        cf.setMinimumConnections(100);
        cf.setPoolSize(100);
        cf.setPort(6379);
        cf.setWaitTime(10000l);
        
        cf.initPool();
        for (int i = 0; i < 100; i++)
        {
            System.out.println();
            System.out.println("> " + i);

            final Jedis j = cf.getResource();
            final Jedis j1 = cf.getResource();
            final Jedis j2 = cf.getResource();
            final Jedis j3 = cf.getResource();
            j1.get("keys *");
            System.out.println("== Fetch");
            System.out.println("A:" + JedisConnectionFactory.getPool().getNumActive());
            System.out.println("I:" + JedisConnectionFactory.getPool().getNumIdle());
            System.out.println("W:" + JedisConnectionFactory.getPool().getNumWaiters());
            cf.returnResource(j1);
            System.out.println("== Return");
            System.out.println("A:" + JedisConnectionFactory.getPool().getNumActive());
            System.out.println("I:" + JedisConnectionFactory.getPool().getNumIdle());
            System.out.println("W:" + JedisConnectionFactory.getPool().getNumWaiters());
        }
        
        
    }

}
