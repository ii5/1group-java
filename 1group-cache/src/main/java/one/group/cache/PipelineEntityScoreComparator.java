package one.group.cache;

import java.util.Comparator;

/**
 * Sort pipe line entities based on score in descending order.
 * 
 * @author nyalfernandes
 * 
 */
public class PipelineEntityScoreComparator implements Comparator<PipelineCacheEntity>
{
    public int compare(PipelineCacheEntity o1, PipelineCacheEntity o2)
    {
        if (o1 == null)
        {
            return -1;
        }

        if (o2 == null)
        {
            return 1;
        }

        Double f1 = o1.getScore();
        Double f2 = o2.getScore();

        if (f1 == null)
        {
            return -1;
        }

        if (f2 == null)
        {
            return 1;
        }

        if (f1.equals(f2))
        {
            return o2.keyValue().compareTo(o1.keyValue());
        }

        return f2.compareTo(f1);
    }
}
