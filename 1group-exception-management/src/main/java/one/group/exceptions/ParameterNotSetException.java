package one.group.exceptions;

public class ParameterNotSetException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ParameterNotSetException(final String message)
    {
        super(message);
    }

    public ParameterNotSetException(final String message, final Throwable th)
    {
        super(message, th);
    }
}
