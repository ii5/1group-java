package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum GeneralSuccessCode
{
    OTP_SUCCESSFUL("200", "OTP sent successfully", "10001", HTTPResponseType.OK, null, null), SIGNIN_SUCCESSFUL("200", "Signin successful", "10002", HTTPResponseType.OK, null, null), REGISTRATION_SUCCESSFUL(
            "200", "Registration successful", "10003", HTTPResponseType.OK, null, null), ACCOUNT_FETCH_SUCCESSFUL("200", "Account fetch successful", "10004", HTTPResponseType.OK, null, null), ACCOUNT_UPDATE_SUCCESSFUL(
            "200", "Account update successful", "10005", HTTPResponseType.OK, null, null), PHOTO_UPLOAD_SUCCESSFUL("200", "Uploaded successfully", "10006", HTTPResponseType.OK, null, null), LOCATIONS_FOUND_SUCCESSFUL(
            "200", "Location Lists found successful", "10007", HTTPResponseType.OK, null, null), CITIES_FOUND_SUCCESSFUL("200", "City Lists found successfully", "10008", HTTPResponseType.OK, null,
            null), LOCATIONS_PUBLISHED_SUCCESSFUL("200", "Locations has been published successfully", "10009", HTTPResponseType.OK, null, null), LOCATIONS_FETCH_SUCCESSFUL("200", "Localities",
            "10010", HTTPResponseType.OK, null, null), NOTIFICATION_SEND_SUCCESSFUL("200", "Notification has been sent successfully", "10011", HTTPResponseType.OK, null, null);

    private String code;
    private String successMessage;
    private String successName;
    private String description;
    private HTTPResponseType responseType;

    GeneralSuccessCode(final String successName, final String successMessage, final String code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.successMessage = successMessage;
        this.successName = successName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getSuccessMessage()
    {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage)
    {
        this.successMessage = successMessage;
    }

    public String getSuccessName()
    {
        return successName;
    }

    public void setSuccessName(String successName)
    {
        this.successName = successName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public HTTPResponseType getResponseType()
    {
        return responseType;
    }

    public void setResponseType(HTTPResponseType responseType)
    {
        this.responseType = responseType;
    }

}
