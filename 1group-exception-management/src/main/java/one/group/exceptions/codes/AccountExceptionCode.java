package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum AccountExceptionCode implements OneGroupExceptionCode
{

    ACCOUNT_NOT_FOUND("404", "Account for %s not found.", 1001, HTTPResponseType.NOT_FOUND, null, null),
    SAME_TO_AND_FROM("400", "Same Account details mentioned for to and from account. [to_account_id=%s, from_account_id=%s]", 1002, HTTPResponseType.BAD_REQUEST, null, null),
    ACCOUNT_ALREADY_REGISTERED("400", "Account already registered.", 1003, HTTPResponseType.BAD_REQUEST, null, null),
    ACCOUNT_UNREGISTERD("400", "Account %s still unregistered.", 1004, HTTPResponseType.BAD_REQUEST, null, null),
    ACCOUNT_INACTIVE("403", "Account %s, you are forbidden! Kindly contact customer support.", 1005, HTTPResponseType.FORBIDDEN, null, null),
    FORBIDDEN("403", "Account %s still unregistered.", 1006, HTTPResponseType.FORBIDDEN, null, null),
    ADMIN_ACCOUNT_NOT_SEEDED("400", "Admin Account not seeded.", 1007, HTTPResponseType.BAD_REQUEST, null, null),
    ACCOUNT_ALREADY_EXISTS_ADMIN_ROLE("400", "Account already exists with Admin role.", 1008, HTTPResponseType.BAD_REQUEST, null, null),
    ACCOUNT_IS_BLOCKED("403", "Account is blocked", 1009, HTTPResponseType.FORBIDDEN, null, null), 
    ACCOUNT_WITH_SAME_WA_NUMBER("400", "Account with same whats app number %s exists.", 1010, HTTPResponseType.BAD_REQUEST, null, null),

    ;
    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    AccountExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }

}