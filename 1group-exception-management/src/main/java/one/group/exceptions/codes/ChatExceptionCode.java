package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum ChatExceptionCode implements OneGroupExceptionCode
{

    CHAT_MESSAGE_NOT_SAVED("500", "Problem while saving chat message.", 4001, HTTPResponseType.INTERNAL_SERVER_ERROR, null, null), CHAT_MESSAGES_NOT_FOUND("500", "Chat messages not found", 4002,
            HTTPResponseType.INTERNAL_SERVER_ERROR, null, null), PARTICIPANTS_NOT_FOUND("500", "Participants messages not found", 4003, HTTPResponseType.INTERNAL_SERVER_ERROR, null, null), CHAT_THREAD_NOT_FOUND(
            "400", "Chat threads not found", 4004, HTTPResponseType.NOT_FOUND, null, null), INVALID_MESSAGE_TYPE("400", "Invalid message type", 4005, HTTPResponseType.BAD_REQUEST, null, null), CREATED_BY_NOT_MATCHED(
            "400", "Created by id should be same as account id of token", 4006, HTTPResponseType.BAD_REQUEST, null, null), CHAT_MESSAGE_SENDING_ERROR("500", "Problem while sending message.", 4007,
            HTTPResponseType.INTERNAL_SERVER_ERROR, null, null), ME_MESSAGE("400", "only text and photo type are allowed", 4008, HTTPResponseType.BAD_REQUEST, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private HTTPResponseType responseType;

    private String description;
    private String suggestion;

    ChatExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String description, final String suggestion)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
