package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum SigninExceptionCode implements OneGroupExceptionCode
{

    NO_VALID_PHONE_NUMBER("401", "Phone Number %s not found.", 3001, HTTPResponseType.UNAUTHORIZED, null, null),

    NO_VALID_OTP("401", "OTP %s not valid.", 3002, HTTPResponseType.UNAUTHORIZED, null, null), OTP_ALREADY_USED("401", "OTP %s already used.", 3003, HTTPResponseType.UNAUTHORIZED, null, null), ;

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private HTTPResponseType responseType;

    private String description;
    private String suggestion;

    SigninExceptionCode(String exceptionName, String exceptionMessage, int code, HTTPResponseType responseType, String description, String suggestion)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public int getCode()
    {
        return code;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
