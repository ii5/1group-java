package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum GeneralExceptionCode implements OneGroupExceptionCode
{
    BAD_REQUEST("400", "The request sent is incomplete. Verify if all required parameters are set!", 9001, HTTPResponseType.BAD_REQUEST, null, null), NOT_FOUND("404", "Not Found", 9004,
            HTTPResponseType.NOT_FOUND, null, null), TOO_MANY_REQUESTS("429", "Too many requests", 9005, HTTPResponseType.TOO_MANY_REQUESTS, null, null), INTERNAL_ERROR("500",
            "Internal server error.", 9006, HTTPResponseType.INTERNAL_SERVER_ERROR, null, null), SERVICE_UNAVAILABLE("503", "Service temporarily unavailable.", 9007,
            HTTPResponseType.SERVICE_UNAVAILABLE, null, null), MOVED_PERMANENTLY("301", "Requested resource has been moved permanently.", 9008, HTTPResponseType.MOVED_PERMANENTLY, null, null), UNRECOGNIZED(
            "301", "Requested resource version is not recognised.", 9009, HTTPResponseType.MOVED_PERMANENTLY, null, null), PATH_NOT_FOUND("404", "Path or http method type incorrect.", 9010,
            HTTPResponseType.NOT_FOUND, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    GeneralExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
