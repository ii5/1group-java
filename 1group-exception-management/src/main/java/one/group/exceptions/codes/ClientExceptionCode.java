package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum ClientExceptionCode implements OneGroupExceptionCode
{

    CLIENT_NOT_FOUND("404", "Client for %s not found.", 2001, HTTPResponseType.NOT_FOUND, null, null),
    CLIENT_ALREADY_EXISTS("400", "Client already exists.", 2002, HTTPResponseType.BAD_REQUEST, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    ClientExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.suggestion = suggestion;
        this.description = description;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
