package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * @author shweta.sankhe
 *
 */
public enum AuthorizationExceptionCode implements OneGroupExceptionCode
{

    USER_ACCESS_DENIED("403", "Acess Denied for user", 16001, HTTPResponseType.FORBIDDEN, null, null), INVALID_TOKEN("401", "Invalid Access Token", 16002, HTTPResponseType.UNAUTHORIZED, null, null), INVALID_REQUEST(
            "400", "Invalid Request", 16003, HTTPResponseType.BAD_REQUEST, null, null), INVALID_CLIENT("400", "Invalid Client", 16004, HTTPResponseType.BAD_REQUEST, null, null), INVALID_GRANT("400",
            "Invalid Grant", 16005, HTTPResponseType.BAD_REQUEST, null, null), UNAUTHORIZED_CLIENT("401", "Unauthorized Client", 16006, HTTPResponseType.UNAUTHORIZED, null, null), UNSUPPORTED_GRANT_TYPE(
            "400", "Unsupported Grant Type", 16007, HTTPResponseType.BAD_REQUEST, null, null), INVALID_SCOPE("400", "Invalid Scope", 16008, HTTPResponseType.BAD_REQUEST, null, null), INSUFFICIENT_SCOPE(
            "400", "Insuffiecient Scope", 16009, HTTPResponseType.BAD_REQUEST, null, null), REDIRECT_URI_MISMATCH("400", "Redirect URL mismatch", 16010, HTTPResponseType.BAD_REQUEST, null, null), UNSUPPORTED_RESPONSE_TYPE(
            "400", "Unsupported Response Type", 16011, HTTPResponseType.BAD_REQUEST, null, null), AUTHORIZATION_ERROR("400", "Authorization Exception", 16012, HTTPResponseType.BAD_REQUEST, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    AuthorizationExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }

}