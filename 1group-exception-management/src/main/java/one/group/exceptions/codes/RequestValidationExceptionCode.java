package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum RequestValidationExceptionCode implements OneGroupExceptionCode
{
    IS_NULL("400", "Value of '%s' is null.", 6001, HTTPResponseType.BAD_REQUEST, null, null),
    IS_EMPTY("400", "Value of '%s' is empty.", 6002, HTTPResponseType.BAD_REQUEST, null, null),
    NOT_NUMERIC("400", "Numeric value expected for parameter '%s'.", 6003, HTTPResponseType.BAD_REQUEST, null, null),
    NOT_INTEGER("400", "Integer value expected for parameter '%s'.", 6003, HTTPResponseType.BAD_REQUEST, null, null),
    LENGTH_EXCEEDED("400", "Value of '%s' is too long.", 6004, HTTPResponseType.BAD_REQUEST, null, null),
    SIZE_EXCEEDED("400", "File size too big.", 6005, HTTPResponseType.BAD_REQUEST, null, null),
    NOT_VALID("400", "Invalid value for parameter '%s'.", 6006, HTTPResponseType.BAD_REQUEST, null, null),
    INVALID_FORMAT("400", "Invalid charset encountered '%s'.", 6007, HTTPResponseType.BAD_REQUEST, null, null),
    OUT_OF_RANGE("400", "Value of '%s' out of range.", 6008, HTTPResponseType.BAD_REQUEST, null, null),
    BOTH_SET("400", "Value of '%s' both are not allowed.", 6009, HTTPResponseType.BAD_REQUEST, null, null),
    COUNT_EXCEEDED("400", "Tokens of '%s' extracted exceeds allowable count.", 6010, HTTPResponseType.BAD_REQUEST, null, null)
    ;
    

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    RequestValidationExceptionCode(String exceptionName, String exceptionMessage, int code, HTTPResponseType responseType, String suggestion, String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public int getCode()
    {
        return code;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
