package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum SearchExceptionCode implements OneGroupExceptionCode
{

    SEARCH_NOT_EXIST("404", "Search doesn't exist", 14001, HTTPResponseType.NOT_FOUND, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private HTTPResponseType responseType;

    private String description;
    private String suggestion;

    SearchExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String description, final String suggestion)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
