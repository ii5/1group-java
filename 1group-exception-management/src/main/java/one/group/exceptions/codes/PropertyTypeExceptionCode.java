package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum PropertyTypeExceptionCode
{
    PROPERTY_TYPE_NOT_FOUND("404", "Property type for %s not found.", 15001, HTTPResponseType.NOT_FOUND, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private HTTPResponseType responseType;

    private String description;
    private String suggestion;

    PropertyTypeExceptionCode(String exceptionName, String exceptionMessage, int code, HTTPResponseType responseType, String description, String suggestion)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public int getCode()
    {
        return code;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
