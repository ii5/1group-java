package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum SyncExceptionCode implements OneGroupExceptionCode
{
    ERROR_DURING_SYNC_LOG_PROCESS("500", "Error during processing.", 5001, HTTPResponseType.INTERNAL_SERVER_ERROR, null, null),
    NO_DATA_YET("200", "No new updates available!", 5002, HTTPResponseType.OK, null, null);
    
    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;
    
    SyncExceptionCode(String exceptionName, String exceptionMessage, int code, HTTPResponseType responseType, String suggestion, String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }
    
    public String getExceptionMessage()
    {
        return exceptionMessage;
    }
    
    public String getExceptionName()
    {
        return exceptionName;
    }
    
    public int getCode()
    {
        return code;
    }
    
    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
