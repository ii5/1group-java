package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * 
 * @author miteshchavda
 * 
 */
public enum MessageExceptionCode implements OneGroupExceptionCode
{

    MESSAGE_NOT_FOUND("404", "Message for %s not found.", 13001, HTTPResponseType.NOT_FOUND, null, null);

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    MessageExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String suggestion, final String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }

}