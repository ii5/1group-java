package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum LocationExceptionCode implements OneGroupExceptionCode
{
    LOCATION_NOT_FOUND("404", "Location %s not found in the backend system.", 12001, HTTPResponseType.NOT_FOUND, null, null), ;

    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private String description;
    private String suggestion;
    private HTTPResponseType responseType;

    LocationExceptionCode(String exceptionName, String exceptionMessage, int code, HTTPResponseType responseType, String suggestion, String description)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public int getCode()
    {
        return code;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
