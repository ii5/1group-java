package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface OneGroupExceptionCode
{
    public int getCode();

    public String getDescription();

    public String getExceptionMessage();

    public String getExceptionName();

    public HTTPResponseType getHttpResponseType();

    public String getRecoverySuggestion();
}
