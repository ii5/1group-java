package one.group.exceptions.codes;

import one.group.core.enums.HTTPResponseType;

public enum BroadcastExceptionCode implements OneGroupExceptionCode
{

    BROADCAST_NOT_FOUND("404", "Broadcast for %s not found.", 7001, HTTPResponseType.NOT_FOUND, null, null), ADMIN_ACCOUNT_NOT_FOUND("400", "Admin Account not found", 7002,
            HTTPResponseType.BAD_REQUEST, null, null), ACTIVE_BROADCAST_EDITED("400", "Only active broadcast can be edited", 7003, HTTPResponseType.BAD_REQUEST, null, null), INVALID_MESSAGE_ID_FOUND(
            "400", "Invalid messageId found", 7004, HTTPResponseType.BAD_REQUEST, null, null), BROADCAST_TAG_NOT_FOUND("400", "Invalid broadcast tag", 7005, HTTPResponseType.BAD_REQUEST, null, null), BROADCAST_ALREADY_CREATED(
            "400", "Already Broadcast Created!", 7006, HTTPResponseType.BAD_REQUEST, null, null), MESSAGE_SENDER_PHONENUMBER_NOT_FOUND("400", "message Sender phone number should not be null!", 7007,
            HTTPResponseType.BAD_REQUEST, null, null), BROADCAST_STATUS_NOT_FOUND("400", "Broadcast status should not be null!", 7008, HTTPResponseType.BAD_REQUEST, null, null);
    private int code;
    private String exceptionName;
    private String exceptionMessage;
    private HTTPResponseType responseType;

    private String description;
    private String suggestion;

    BroadcastExceptionCode(final String exceptionName, final String exceptionMessage, final int code, final HTTPResponseType responseType, final String description, final String suggestion)
    {
        this.exceptionMessage = exceptionMessage;
        this.exceptionName = exceptionName;
        this.code = code;
        this.responseType = responseType;
        this.description = description;
        this.suggestion = suggestion;
    }

    public int getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public String getExceptionName()
    {
        return exceptionName;
    }

    public HTTPResponseType getHttpResponseType()
    {
        return this.responseType;
    }

    public String getRecoverySuggestion()
    {
        return this.suggestion;
    }
}
