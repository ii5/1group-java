package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

public class UserNotAuthorizedException extends Abstract1GroupException
{

    public UserNotAuthorizedException(HTTPResponseType responseType, String code, boolean isFatal, String message)
    {
        super(responseType, code, isFatal, message);

    }

    public UserNotAuthorizedException(OneGroupExceptionCode exceptionCode, boolean isFatal, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }

    public UserNotAuthorizedException(OneGroupExceptionCode exceptionCode, boolean isFatal, String message, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }

    public UserNotAuthorizedException(OneGroupExceptionCode exceptionCode, boolean isFatal, String... params)
    {
        super(exceptionCode, isFatal, params);
    }

}
