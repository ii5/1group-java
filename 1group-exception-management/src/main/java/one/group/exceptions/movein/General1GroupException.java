package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

public class General1GroupException extends Abstract1GroupException implements OneGroupException
{
    private static final long serialVersionUID = 1L;

    public General1GroupException(final HTTPResponseType responseType, final String code, final boolean isFatal, final String message)
    {
        super(responseType, code, isFatal, message);
    }

    public General1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String... params)
    {
        super(exceptionCode, isFatal, params);
    }

    public General1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String message, final Throwable th, final String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }

    public General1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final Throwable th, final String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }
}
