package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

/**
 * 
 * @author nyalfernandes
 *
 */
public class ClientProcessException extends Abstract1GroupException
{
    public ClientProcessException(HTTPResponseType responseType, String code, boolean isFatal, String message)
    {
        super(responseType, code, isFatal, message);
    }
    
    public ClientProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }
   
    public ClientProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, String message, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }
    
    public ClientProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, String... params)
    {
        super(exceptionCode, isFatal, params);
    }
}
