package one.group.exceptions.movein.runtime;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.movein.OneGroupException;

public class CacheUnavailableException extends RuntimeException implements OneGroupException
{
    private static final long serialVersionUID = 1L;
    
    public CacheUnavailableException(String message)
    {
        super(message);
    }
    
    public CacheUnavailableException(String message, Throwable th)
    {
        super(message, th);
    }
    
    public CacheUnavailableException()
    {
        
    }

    public String getCode()
    {
        return ""+getHttpCode();
    }

    public int getHttpCode()
    {
        return HTTPResponseType.SERVICE_UNAVAILABLE.getCode();
    }

    public boolean isFatal()
    {
        return true;
    }

}
