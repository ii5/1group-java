package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */

public abstract class Abstract1GroupException extends Exception implements OneGroupException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private HTTPResponseType responseType;

    private String code;

    private boolean isFatal;

    public Abstract1GroupException(final HTTPResponseType responseType, final String code, final boolean isFatal, final String message)
    {
        super(message);
        Validation.notNull(responseType, "The http response type should not be null.");
        Validation.notNull(code, "The exception code should not be null.");
        Validation.notNull(message, "The exception message should not be null or empty.");

        this.responseType = responseType;
        this.code = code;
        this.isFatal = isFatal;
    }

    public Abstract1GroupException(final HTTPResponseType responseType, final String code, final boolean isFatal, final String message, final Throwable th)
    {
        super(message, th);
        Validation.notNull(responseType, "The http response type should not be null.");
        Validation.notNull(code, "The exception code should not be null.");
        Validation.notNull(message, "The exception message should not be null or empty.");

        this.responseType = responseType;
        this.code = code;
        this.isFatal = isFatal;
    }

    public Abstract1GroupException(final HTTPResponseType responseType, final String code, final boolean isFatal, final Throwable th)
    {
        super(th);
        Validation.notNull(responseType, "The http response type should not be null.");
        Validation.notNull(code, "The exception code should not be null.");

        this.responseType = responseType;
        this.code = code;
        this.isFatal = isFatal;
    }

    public Abstract1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String... params)
    {
        this(exceptionCode.getHttpResponseType(), String.valueOf(exceptionCode.getCode()), isFatal, String.format(exceptionCode.getExceptionMessage(), params));
    }

    public Abstract1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String message, final Throwable th, final String... params)
    {
        this(exceptionCode.getHttpResponseType(), String.valueOf(exceptionCode.getCode()), isFatal, String.format(message, params), th);
    }

    public Abstract1GroupException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final Throwable th, final String... params)
    {
        this(exceptionCode.getHttpResponseType(), String.valueOf(exceptionCode.getCode()), isFatal, String.format(exceptionCode.getExceptionMessage(), params), th);
    }

    public String getCode()
    {
        return this.code;
    }

    public int getHttpCode()
    {
        return responseType.getCode();
    }

    public boolean isFatal()
    {
        return this.isFatal;
    }

}
