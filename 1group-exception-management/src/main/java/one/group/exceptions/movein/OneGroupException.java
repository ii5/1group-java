package one.group.exceptions.movein;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface OneGroupException
{
    public String getCode();

    public int getHttpCode();

    public String getMessage();

    public boolean isFatal();
}
