package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

/**
 * 
 * @author nyalfernandes
 *
 */
public class RequestValidationException extends Abstract1GroupException
{

    private static final long serialVersionUID = 1L;

    public RequestValidationException(HTTPResponseType responseType, String code, boolean isFatal, String message)
    {
        super(responseType, code, isFatal, message);
    }

    public RequestValidationException(OneGroupExceptionCode exceptionCode, boolean isFatal, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }

    public RequestValidationException(OneGroupExceptionCode exceptionCode, boolean isFatal, String message, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }

    public RequestValidationException(OneGroupExceptionCode exceptionCode, boolean isFatal, String... params)
    {
        super(exceptionCode, isFatal, params);
    }
}
