package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class AccountNotFoundException extends Abstract1GroupException
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AccountNotFoundException(final HTTPResponseType responseType, final String code, final boolean isFatal, final String message)
    {
        super(responseType, code, isFatal, message);
    }

    public AccountNotFoundException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String... params)
    {
        super(exceptionCode, isFatal, params);
    }

    public AccountNotFoundException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final String message, final Throwable th, final String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }

    public AccountNotFoundException(final OneGroupExceptionCode exceptionCode, final boolean isFatal, final Throwable th, final String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }
}
