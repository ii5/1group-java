package one.group.exceptions.movein;

import one.group.core.enums.HTTPResponseType;
import one.group.exceptions.codes.OneGroupExceptionCode;

/**
 * 
 * @author miteshchavda
 *
 */
public class BroadcastProcessException extends Abstract1GroupException
{
    public BroadcastProcessException(HTTPResponseType responseType, String code, boolean isFatal, String message)
    {
        super(responseType, code, isFatal, message);
    }

    public BroadcastProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, th, params);
    }

    public BroadcastProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, String message, Throwable th, String... params)
    {
        super(exceptionCode, isFatal, message, th, params);
    }

    public BroadcastProcessException(OneGroupExceptionCode exceptionCode, boolean isFatal, String... params)
    {
        super(exceptionCode, isFatal, params);
    }
}
