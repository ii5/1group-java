package one.group.sync;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import one.group.services.helpers.MbeanClient;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SyncServerInitiator
{
    private static final Logger logger = LoggerFactory.getLogger(SyncServerInitiator.class);

    public static void main(final String[] args) throws Exception
    {
        DOMConfigurator.configureAndWatch("log4j.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**applicationContext-configuration-kafka.xml", "classpath*:**applicationContext-services.xml",
                "classpath*:**applicationContext-DAO.xml", "classpath*:**applicationContext-configuration-JPA.xml", "classpath*:**applicationContext-configuration-Cache.xml",
                "classpath*:**applicationContext-configuration-PushService.xml");
        
        MbeanClient c = context.getBean("mbeanClient", MbeanClient.class);
        c.startPropertiesScheduler();
        
        logger.info("Sync Server initiated");
    }

}
