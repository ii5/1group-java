package one.group.sync;

import java.util.Map;
import java.util.Set;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;

public abstract class AbstractSyncHandler<T> implements SyncEntityHandler<T>
{
    private SyncEntityHandler<T> successor;
    
    public Map<String,Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        return process(record);
    }
    
    public SyncEntityHandler<T> getSuccessor()
    {
        return successor;
    }
    
    public void setSuccessor(SyncEntityHandler<T> successor)
    {
        this.successor = successor;
    }
    
    public abstract Map<String, Set<ResponseEntity>> process(SyncLogEntry record) throws Abstract1GroupException;
}
