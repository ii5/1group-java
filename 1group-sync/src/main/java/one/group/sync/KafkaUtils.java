package one.group.sync;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import kafka.admin.AdminUtils;
import kafka.utils.ZkUtils;
import one.group.core.Constant;
import one.group.core.enums.TopicType;
import one.group.sync.consumer.KafkaConsumerFactory;
import one.group.sync.consumer.OneGroupConsumer;
import one.group.sync.producer.KafkaProducerFactory;
import one.group.utils.validation.Validation;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import scala.collection.Map;
import scala.collection.Seq;

/**
 * Set up topics and partitions on Kafka
 * 
 * @author nyalfernandes
 * 
 */
public class KafkaUtils implements ApplicationContextAware
{
    private static final Logger logger = LoggerFactory.getLogger(KafkaUtils.class);

    private ZookeeperClientFactory zkClientFactory;

    private KafkaConsumerFactory consumerFactory;

    private KafkaProducerFactory<String, String> producerFactory;

    private SyncEntityHandler syncHandler;

    private ApplicationContext applicationContext;

    private KafkaConfiguration kafkaConfiguration;

    private static ExecutorService executor;

    public SyncEntityHandler getSyncHandler()
    {
        return syncHandler;
    }

    public void setSyncHandler(SyncEntityHandler syncHandler)
    {
        this.syncHandler = syncHandler;
    }

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public KafkaConsumerFactory getConsumerFactory()
    {
        return consumerFactory;
    }

    public void setConsumerFactory(KafkaConsumerFactory consumerFactory)
    {
        this.consumerFactory = consumerFactory;
    }

    public ZookeeperClientFactory getZkClientFactory()
    {
        return zkClientFactory;
    }

    public void setZkClientFactory(ZookeeperClientFactory zkClientFactory)
    {
        this.zkClientFactory = zkClientFactory;
    }

    public KafkaProducerFactory<String, String> getProducerFactory()
    {
        return producerFactory;
    }

    public void setProducerFactory(KafkaProducerFactory<String, String> producerFactory)
    {
        this.producerFactory = producerFactory;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    public void initialize()
    {
        try
        {
            for (TopicType type : TopicType.values())
            {
                logger.info("Initializing all for topic type " + type);
                String appTopic = kafkaConfiguration.getTopic(type).getName();
                createTopicIfNotExists(appTopic, type);
                Thread.sleep(2000);
                createConsumers(appTopic, type);
            }

            // Thread.sleep(2000);
            // createProducers();
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    public void createTopicIfNotExists(String topicName, TopicType type)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        ZkClient client = zkClientFactory.getClient();
        if (!AdminUtils.topicExists(client, topicName))
        {
            logger.info("Creating topic " + topicName);
            AdminUtils.createTopic(client, topicName, kafkaConfiguration.getTopic(type).getPartitionCount(), kafkaConfiguration.getTopic(type).getReplicationFactor(), new Properties());
        }
        else
        {
            logger.warn("Topic " + topicName + " already exists.");
        }
    }

    public void deleteTopic(String topicName)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        ZkClient client = zkClientFactory.getClient();
        if (AdminUtils.topicExists(client, topicName))
        {
            logger.debug("Deleting topic " + topicName);
            AdminUtils.deleteTopic(client, topicName);
        }
        else
        {
            logger.warn("Topic " + topicName + " already deleted.");
        }
    }

    public void partitionTopicFurther(String topicName, int addOn)
    {
        Validation.notNull(topicName, "Topic name should not be null.");
        Validation.isFalse(addOn > 0, "Additional partitions should always be greater that 0.");

        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> partitionsOfTopic = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();
        int currentPartitionSize = partitionsOfTopic.size();
        AdminUtils.addPartitions(client, topicName, currentPartitionSize + addOn, "", true, new Properties());
    }

    public void createConsumers(String topicName, TopicType type) throws Exception
    {
        ZkClient client = zkClientFactory.getClient();
        Seq<String> topics = ZkUtils.getAllTopics(client);
        Map<Object, Seq<Object>> topicPartitioMap = ZkUtils.getPartitionAssignmentForTopics(client, topics).get(topicName).get();

        if (topicPartitioMap == null)
        {
            throw new IllegalStateException("No topic '" + topicName + "' present.");
        }

        List<Integer> toHandlePartitionList = getPartitionsOfTopic(topicPartitioMap, type);

        if (!toHandlePartitionList.isEmpty())
        {
            executor = Executors.newFixedThreadPool(toHandlePartitionList.size());
            Properties serverProperties = applicationContext.getBean("sync.server.properties", Properties.class);

            logger.info("Loading offsets for topic " + topicName);
            Properties syncOffsetProperties = new Properties();
            String syncOffsetFileName = getFileFromServerProperties("syncOffsetLocation", Constant.SYNC_SERVER_OFFSET_FILE_TEMPLATE, topicName, serverProperties);
            File syncOffsetFile = createFile(syncOffsetFileName);
            syncOffsetProperties.load(new FileInputStream(syncOffsetFile));

            logger.info("Loading error offsets for topic " + topicName);
            Properties errorOffsetProperties = new Properties();
            String errorOffsetFileName = getFileFromServerProperties("errorOffsetLocation", Constant.ERROR_OFFSET_FILE_TEMPLATE, topicName, serverProperties);
            File errorOffsetFile = createFile(errorOffsetFileName);
            errorOffsetProperties.load(new FileInputStream(errorOffsetFile));

            for (int partition : toHandlePartitionList)
            {
                Thread.sleep(100);
                OneGroupConsumer consumer = new OneGroupConsumer(partition, syncOffsetProperties, consumerFactory.getConfigProperties(), syncOffsetFile, topicName, type, errorOffsetFile,
                        errorOffsetProperties);
                consumer.setSyncHandler(syncHandler);
                logger.info("" + consumer);
                executor.submit(consumer);
            }
        }
    }

    public void createProducers()
    {
        producerFactory.initialize();
    }

    public int partitionFor(String key, TopicType type)
    {
        return new SimplePartitioner().partition(key, kafkaConfiguration.getTopic(type).getPartitionCount());
    }

    private List<Integer> getPartitionsOfTopic(Map<Object, Seq<Object>> topicPartitioMap, TopicType type)
    {
        List<Integer> partitionList = new ArrayList<Integer>();

        if (kafkaConfiguration.getTopic(type).isAllPartitions())
        {
            int size = topicPartitioMap.size();
            for (int i = 0; i < size; i++)
            {
                partitionList.add(i);
            }
        }
        else
        {
            partitionList.addAll(kafkaConfiguration.getTopic(type).getPartitionList());
        }

        System.out.println(partitionList);
        return partitionList;
    }

    private File createFile(String OffsetFileName) throws IOException
    {
        File OffsetFile = new File(OffsetFileName);

        if (OffsetFile.getParentFile() != null)
        {
            OffsetFile.getParentFile().mkdirs();
        }
        OffsetFile.canExecute();
        OffsetFile.canWrite();
        OffsetFile.canRead();
        OffsetFile.createNewFile();

        return OffsetFile;
    }

    private String getFileFromServerProperties(String key, String defaultTemplate, String topicName, Properties serverProperties)
    {
        String fileName = (serverProperties == null) ? defaultTemplate : serverProperties.getProperty(key, defaultTemplate);
        fileName = fileName.replace("[topic]", topicName);

        return fileName;
    }

}
