/**
 * 
 */
package one.group.sync.handler.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.BroadcastDAO;
import one.group.dao.MessageDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Broadcast;
import one.group.entities.socket.Message;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.SyncDBService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class EditBroadcastTaskHandler implements SyncTaskHandler
{
    public static final String EDIT_BROADCAST_KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(EditBroadcastTaskHandler.class);

    private BroadcastDAO broadcastDAO;

    private SyncDBService syncDBService;

    private MessageDAO messageDAO;

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public void setBroadcastDAO(BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();

        String oldBroadcastId = record.getAssociatedEntityId();
        Broadcast broadcast = broadcastDAO.fetchBroadcast(oldBroadcastId);
        if (broadcast == null)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }

        WSEntityReference wsReference = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class);
        String newBroadcastId = wsReference.getId();
        Broadcast newBroadcast = broadcastDAO.fetchBroadcast(newBroadcastId);
        if (newBroadcast == null)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }

        List<String> broadcastIdList = new ArrayList<String>();
        broadcastIdList.add(oldBroadcastId);
        List<Message> msgList = messageDAO.fetchMessageByBroadcastIds(broadcastIdList);

        for (Message message : msgList)
        {
            Message messageToUpdate = new Message();
            messageToUpdate.setId(message.getId());
            messageToUpdate.setBroadcastId(newBroadcastId);
            messageToUpdate.setUpdatedBy(newBroadcastId);
            messageToUpdate.setUpdatedTime(new Date());
            messageDAO.update(messageToUpdate);

            WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
            update.setEntity(new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, message.getId()));

            String messageCreatedBy = message.getCreatedBy();
            if (message.getMessageSource().equals(MessageSourceType.DIRECT))
            {
                addToMap(response, messageCreatedBy, update);
                addToMap(response, message.getToAccountId(), update);
            }
            else
            {
                addToMap(response, messageCreatedBy, update);
            }

            logger.info("Response: " + response);
        }
        return response;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT_CLOSE.toString());
        values.put("entityType", EntityType.MESSAGE.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
