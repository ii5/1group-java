/**
 * 
 */
package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.SyncDBService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class DeleteChatThreadTaskHandler implements SyncTaskHandler
{

    public static final String DELETE_CHAT_THREAD_KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(AddBroadcastTaskHandler.class);

    private SyncDBService syncDBService;

    private AccountDAO accountDAO;

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
        WSEntityReference entityReference = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class);

        String accountId = record.getAssociatedEntityId();

        Account account = accountDAO.fetchAccountById(accountId);
        if (Utils.isNull(account))
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }
        String groupId = entityReference.getId();

        WSSyncUpdate update = new WSSyncUpdate(SyncDataType.CHAT_CURSOR);
        update.setEntity(new WSEntityReference(HintType.DELETE, EntityType.CHAT_THREAD, groupId));
        addToMap(response, accountId, update);
        return response;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.DELETE_CHAT_THREAD.toString());
        values.put("entityType", EntityType.CHAT_THREAD.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
