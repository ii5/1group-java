/**
 * 
 */
package one.group.sync.handler.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.PipelineEntityScoreComparator;
import one.group.cache.dao.BroadcastCacheDAO;
import one.group.core.Constant;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.ClientType;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.PushServerType;
import one.group.core.enums.Rooms;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.BroadcastDAO;
import one.group.dao.BroadcastTagDAO;
import one.group.dao.MessageDAO;
import one.group.dao.SearchDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSAccountUtilityDataHolder;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.api.response.v2.WSTag;
import one.group.entities.cache.BroadcastKey;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Search;
import one.group.entities.socket.SocketEntity;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.ClientService;
import one.group.services.GroupsService;
import one.group.services.PushService;
import one.group.services.SearchNotificationService;
import one.group.services.SyncDBService;
import one.group.services.helpers.PushServiceObject;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class AddBroadcastTaskHandler implements SyncTaskHandler
{
    public static final String ADD_BROADCAST_KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(AddBroadcastTaskHandler.class);

    private BroadcastDAO broadcastDAO;

    private SyncDBService syncDBService;

    private SearchDAO searchDAO;

    private MessageDAO messageDAO;

    private BroadcastTagDAO broadcastTagDAO;

    private BroadcastCacheDAO broadcastCacheDAO;

    private GroupsService groupsService;

    private ClientService clientService;

    private PushService pushService;

    private SearchNotificationService searchNotificationService;

    public PushService getPushService()
    {
        return pushService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    public BroadcastCacheDAO getBroadcastCacheDAO()
    {
        return broadcastCacheDAO;
    }

    public void setBroadcastCacheDAO(BroadcastCacheDAO broadcastCacheDAO)
    {
        this.broadcastCacheDAO = broadcastCacheDAO;
    }

    public BroadcastTagDAO getBroadcastTagDAO()
    {
        return broadcastTagDAO;
    }

    public void setBroadcastTagDAO(BroadcastTagDAO broadcastTagDAO)
    {
        this.broadcastTagDAO = broadcastTagDAO;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public SearchDAO getSearchDAO()
    {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO)
    {
        this.searchDAO = searchDAO;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public void setBroadcastDAO(BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    public SearchNotificationService getSearchNotificationService()
    {
        return searchNotificationService;
    }

    public void setSearchNotificationService(SearchNotificationService searchNotificationService)
    {
        this.searchNotificationService = searchNotificationService;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
        WSMessageResponse WsMessageResponse = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSMessageResponse.class);

        String broadcastId = record.getAssociatedEntityId();
        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);

        if (broadcast == null)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }
        List<BroadcastTag> broadcastTagList = broadcastTagDAO.fetchBroadcastTagByBroadcastId(broadcastId, BroadcastStatus.ACTIVE);

        Set<String> matchingSearchCreatedByIDSet = new HashSet<String>();

        Boolean broadcastTagStatusFlag = false;
        for (BroadcastTag tag : broadcastTagList)
        {
            // syncUpdate send only if BroadcastTagStatus = added
            if (tag.getStatus().equals(BroadcastTagStatus.ADDED))
            {
                broadcastTagStatusFlag = true;
            }
            else
            {
                continue;
            }
            long minPrice = tag.getMinimumPrice() != null ? tag.getMinimumPrice() : 0;
            long maxPrice = tag.getMaximumPrice() != null ? tag.getMaximumPrice() : Integer.MAX_VALUE;
            long minSize = tag.getMinimumSize() != null ? tag.getMinimumSize() : 0;
            long maxSize = tag.getMaximumSize() != null ? tag.getMaximumSize() : Long.MAX_VALUE;
            long size = tag.getSize() != null ? tag.getSize() : 0;
            long price = tag.getPrice() != null ? tag.getPrice() : 0;

            String locationId = tag.getLocation().getId();
            String cityId = tag.getCityId();
            PropertyType propertyType = tag.getPropertyType();
            PropertyTransactionType transactionType = tag.getTransactionType();
            Rooms rooms = tag.getRooms() != null ? tag.getRooms() : null;
            // get matching Searches
            List<Search> searchList = searchDAO.getMatchingSearches(locationId, cityId, minPrice, maxPrice, minSize, maxSize, price, size, propertyType, transactionType, rooms,
                    tag.getBroadcastType(), tag.getPropertySubType());
            for (Search search : searchList)
            {
                matchingSearchCreatedByIDSet.add(search.getCreatedBy());
            }
        }

        if (broadcastTagStatusFlag)
        {
            Account account = broadcast.getAccount();
            String messageCreatedBy = account.getId();

            // for WhatsAPP To Account is not Present, And for ME Messages To
            // Account Is always Same
            List<String> accountIdList = new ArrayList<String>();
            accountIdList.add(messageCreatedBy);
            if (Utils.isNullOrEmpty(WsMessageResponse.getCreatedById()))
            {
                WsMessageResponse.setCreatedById(messageCreatedBy);
            }
            if (Utils.isNullOrEmpty(WsMessageResponse.getToAccountId()))
            {
                WsMessageResponse.setToAccountId(messageCreatedBy);
            }
            if (Utils.isNullOrEmpty(WsMessageResponse.getBroadcastId()))
            {
                WsMessageResponse.setBroadcastId(broadcastId);
            }
            WSAccountUtilityDataHolder dataHolder = groupsService.fetchAccountMapData(messageCreatedBy, accountIdList);
            WsMessageResponse = groupsService.formWSMessageResponse(WsMessageResponse, dataHolder);

            // Only Added tag display in WsMessageResponse
            Set<WSTag> wsTag = WsMessageResponse.getBroadcast().getTags();
            Set<WSTag> wsTagsRemoved = new HashSet<WSTag>();
            for (WSTag wsTag2 : wsTag)
            {
                if (!wsTag2.getStatus().equals(BroadcastTagStatus.ADDED))
                {
                    wsTagsRemoved.add(wsTag2);
                }
            }
            wsTag.removeAll(wsTagsRemoved);

            WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
            update.setEntity(new WSEntityReference(HintType.FETCH, EntityType.BROADCAST, WsMessageResponse.getId()));

            WSSyncUpdate update2 = new WSSyncUpdate(SyncDataType.BROADCAST);
            update2.setBroadcast(WsMessageResponse);

            // send Sync Update to Account who created broadcast
            addToMap(response, messageCreatedBy, update);
            addToMap(response, messageCreatedBy, update2);

            Set<PipelineCacheEntity> broadcastPipelinSet = new TreeSet<PipelineCacheEntity>(new PipelineEntityScoreComparator());
            for (String accountId : matchingSearchCreatedByIDSet)
            {
                Map<String, String> values = new HashMap<String, String>();
                values.put("account_id", accountId);
                String searchBroadcastKey = BroadcastKey.SEARCHED_BROADCAST_OF_ACCOUNT.getFormedKey(values);

                PipelineCacheEntity pce = new PipelineCacheEntity(searchBroadcastKey);
                if (!Utils.isNull(broadcast.getUpdatedTime()))
                {
                    pce.setScore(broadcast.getUpdatedTime().getTime());
                }
                else
                {
                    pce.setScore(broadcast.getCreatedTime().getTime());
                }
                pce.setValue(broadcastId);
                broadcastPipelinSet.add(pce);
                broadcastCacheDAO.saveBroadcastBulk(broadcastPipelinSet);
                addToMap(response, accountId, update);
                addToMap(response, accountId, update2);
                logger.info("Response: " + response);

            }

            // // mapping SyncUpdate Vs AccountId
            Map<String, List<WSSyncUpdate>> accountVsSyncUpdate = new HashMap<String, List<WSSyncUpdate>>();
            for (String accountId : response.keySet())
            {
                for (ResponseEntity syncUpdate : response.get(accountId))
                {
                    WSSyncUpdate updateOrig = (WSSyncUpdate) syncUpdate;
                    WSSyncUpdate updateCopy = new WSSyncUpdate(updateOrig);
                    syncDBService.appendUpdate(accountId, updateCopy);

                    if (accountVsSyncUpdate.containsKey(accountId))
                    {
                        List<WSSyncUpdate> syncList = accountVsSyncUpdate.get(accountId);
                        syncList.add(updateCopy);
                        accountVsSyncUpdate.put(accountId, syncList);
                    }
                    else
                    {
                        List<WSSyncUpdate> syncUpdateList = new ArrayList<WSSyncUpdate>();
                        syncUpdateList.add(updateCopy);
                        accountVsSyncUpdate.put(accountId, syncUpdateList);
                    }
                }

                List<Client> allClientsOfAccount = clientService.fetchAllActiveClientsOfAccount(accountId);
                Map<PushServerType, List<String>> channelTypeVsNames = getAllChannelsOfClients(allClientsOfAccount);
                List<String> androidDeviceIds = channelTypeVsNames.get(PushServerType.GCM);

                SocketEntity entity = new SocketEntity(accountVsSyncUpdate.get(accountId));

                // List<String> pushChannelIds =
                //
                // clientService.fetchAllPushChannelsOfClientsOfAccount(accountId);

                PushServiceObject config = new PushServiceObject();
                config.setAccountId(accountId);
                // config.addService(PushServerType.PUSHER);
                config.addService(PushServerType.WEBSOCKET);
                // config.setPusherChannelList(pushChannelIds);
                // V2-637 Notification Disabled
                // config.addService(PushServerType.GCM);
                config.setGcmDeviceList(androidDeviceIds);
                config.setAccountId(accountId);
                config.setPusherEventName("");
                config.setData(entity);

                pushService.forwardRequest(config);

                // Add account id in search notification table
                try
                {
                    searchNotificationService.saveNotification(accountId);
                }
                catch (PersistenceException pe)
                {
                    logger.info("Ignore, notification already exist for account[" + accountId + "]");
                }
            }
        }
        return null;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.ADD.toString());
        values.put("entityType", EntityType.BROADCAST.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    private Map<PushServerType, List<String>> getAllChannelsOfClients(List<Client> clientList)
    {
        List<String> pushChannelList = new ArrayList<String>();
        List<String> iosDeviceIds = new ArrayList<String>();
        List<String> androidDeviceIds = new ArrayList<String>();
        Map<PushServerType, List<String>> channelTypeVsNames = new HashMap<PushServerType, List<String>>();

        for (Client c : clientList)
        {
            /*
             * if (c.getPushChannel() != null &&
             * !pushChannelList.contains(c.getPushChannel())) {
             * pushChannelList.add(c.getPushChannel()); }
             * 
             * if (c.getDevicePlatform().equals(ClientType.IOS.toString()) &&
             * c.getCpsId() != null && !iosDeviceIds.contains(c.getCpsId())) {
             * iosDeviceIds.add(c.getDeviceToken()); }
             */
            if (c.getDevicePlatform().equals(ClientType.ANDROID.name()) && c.getCpsId() != null)
            {
                androidDeviceIds.add(c.getCpsId());
            }
        }

        channelTypeVsNames.put(PushServerType.APNS, iosDeviceIds);
        channelTypeVsNames.put(PushServerType.GCM, androidDeviceIds);
        channelTypeVsNames.put(PushServerType.PUSHER, pushChannelList);

        return channelTypeVsNames;
    }
}
