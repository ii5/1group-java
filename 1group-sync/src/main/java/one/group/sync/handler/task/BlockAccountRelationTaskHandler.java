package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.services.AccountRelationService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ashish Thorat
 * 
 */
public class BlockAccountRelationTaskHandler implements SyncTaskHandler
{
    public static final String KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(BlockAccountRelationTaskHandler.class);

    private AccountRelationService accountRelationService;

    private SyncDBService syncDBService;

    private AccountDAO accountDAO;

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> responseUpdate = new HashMap<String, Set<ResponseEntity>>();
        Set<ResponseEntity> responseEntity = new HashSet<ResponseEntity>();

        String requestByAccountId = record.getAssociatedEntityId();
        String accountId = record.getAdditionalData(SyncEntryKey.TARGET_ACCOUNT_ID);

        // accountRelationService.createAccountBlockByKey(accountId,
        // requestByAccountId);

        WSSyncUpdate syncUpdate = new WSSyncUpdate();

        syncUpdate.setEntity(record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class));
        syncUpdate.setType(record.getType());
        responseEntity.add(syncUpdate);

        Account account = accountDAO.fetchAccountByAccountId(accountId);
        if (account == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }

        WSAccount wsAccount = new WSAccount(account, true, true);

        WSSyncUpdate updateOwn2 = new WSSyncUpdate(SyncDataType.ACCOUNT_RELATION);
        WSAccountRelations accountRelation = accountRelationService.fetchAccountRelation(requestByAccountId, accountId);
        if (accountRelation != null)
        {
            wsAccount.setIsBlocked(accountRelation.getIsBlocked());
            wsAccount.setIsContact(accountRelation.getIsContact());
            if (accountRelation.getIsBlocked())
            {
                wsAccount.setCanChat(false);
            }

        }
        updateOwn2.setAccountRelation(wsAccount);
        responseEntity.add(updateOwn2);

        responseUpdate.put(requestByAccountId, responseEntity);
        return responseUpdate;
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.BLOCK.toString());
        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
