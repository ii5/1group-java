package one.group.sync.handler.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.cache.dao.CacheAction;
import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.AccountDAO;
import one.group.dao.PropertySearchRelationDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Search;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.AccountRelationService;
import one.group.services.LocationService;
import one.group.services.PropertyListingService;
import one.group.services.SearchService;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class EditLocationTaskHandler implements SyncTaskHandler
{
    public static final String KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(EditLocationTaskHandler.class);

    private AccountDAO accountDAO;

    private LocationService locationService;

    private PropertyListingService propertyListingService;

    private SearchService searchService;

    private AccountRelationService accountRelationService;

    private CacheAction cacheAction;

    private PropertySearchRelationDAO propertySearchRelationDAO;

    private SubscriptionService subscriptionService;

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public PropertySearchRelationDAO getPropertySearchRelationDAO()
    {
        return propertySearchRelationDAO;
    }

    public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO)
    {
        this.propertySearchRelationDAO = propertySearchRelationDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public SearchService getSearchService()
    {
        return searchService;
    }

    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public CacheAction getCacheAction()
    {
        return cacheAction;
    }

    public void setCacheAction(CacheAction cacheAction)
    {
        this.cacheAction = cacheAction;
    }

    @Transactional
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        String entityId = record.getAssociatedEntityId();
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();

        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", entityId);

        WSSyncUpdate update = new WSSyncUpdate();
        update.setEntity(record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class));
        update.setType(SyncDataType.INVALIDATION);

        addToMap(response, entityId, update);

        WSEntityReference ref = new WSEntityReference(HintType.DELETE, EntityType.NEWS_FEED, null);
        update = new WSSyncUpdate(SyncDataType.INVALIDATION);
        update.setEntity(ref);

        addToMap(response, entityId, update);

        Account account = accountDAO.fetchAccountById(entityId);
        List<Location> matchingLocations = new ArrayList<Location>();
        List<Location> exactMatchingLocations = new ArrayList<Location>();
        List<Search> searchList = null; // TODO
                                        // searchService.fetchSearchByAccount(account.getId());
        List<String> searchLocations = new ArrayList<String>();

        // API-76 Avoid using redis scan
        List<String> subscribedLocationsString = new ArrayList<String>();
        // Set<Location> locationSet = account.getLocations();
        // for (Location l : locationSet)
        // {
        // subscribedLocationsString.add(l.getId());
        // }

        matchingLocations.addAll(locationService.getMatchingLocation(subscribedLocationsString, true));

        // if (account.getLocalities() != null)
        // {
        // matchingLocations.addAll(account.getLocalities());
        // for (Location location : account.getLocalities())
        // {
        // matchingLocations.addAll(locationService.getMatchingLocation(location,
        // true));
        // }
        // }

        exactMatchingLocations.addAll(matchingLocations);

        for (Search search : searchList)
        {
            if (!searchLocations.contains(search.getLocationId()))
            {
                searchLocations.add(search.getLocationId());
            }
        }

        matchingLocations.addAll(locationService.getMatchingAndNearByLocation(searchLocations, true));

        // cacheAction.invalidate(NewsFeedKey.SORTED_SET_NEWS_FEED.getFormedKey(values));

        List<PropertyListingScoreObject> propertyScoreObjectList = fetchPropertyListingsBasedOnLocation(matchingLocations);

        // processNewsFeed(account, response,
        // searchService.fetchPropertyAgainstSearchMapForAccount(account.getId()),
        // exactMatchingLocations, propertyScoreObjectList, true);

        return response;
    }

    private List<PropertyListingScoreObject> fetchPropertyListingsBasedOnLocation(List<Location> locationList)
    {
        List<PropertyListingScoreObject> propertyListingList = propertyListingService.searchPropertyListing(locationList, BroadcastStatus.ACTIVE);
        return propertyListingList;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            // To preserve insertion order
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT_LOCATION.toString());
        values.put("entityType", EntityType.ACCOUNT.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
