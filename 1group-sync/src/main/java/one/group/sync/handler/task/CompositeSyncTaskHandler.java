package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class CompositeSyncTaskHandler implements SyncTaskHandler
{
    private static final Logger logger = LoggerFactory.getLogger(CompositeSyncTaskHandler.class);

    private Map<String, SyncTaskHandler> compositeHandlers = new HashMap<String, SyncTaskHandler>();

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry entity) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> accountVsUpdateMap = new HashMap<String, Set<ResponseEntity>>();
        Validation.notNull(entity, "Entity passed should not be null.");
        SyncActionType action = entity.getAction();
        EntityType entityType = entity.getAssociatedEntityType();

        Validation.notNull(action, "SyncLogEntry action should not be null.");
        Validation.notNull(entityType, "SyncLogEntry entityType should not be null.");

        Map<String, String> values = new HashMap<String, String>();
        values.put("action", action.toString());
        values.put("entityType", entityType.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);

        SyncTaskHandler asyncHandler = compositeHandlers.get(key);
        Map<String, Set<ResponseEntity>> accountVsUpdateTaskMap = null;
        if (asyncHandler != null)
        {
            logger.info("Handler found for key[" + key + "]");
            accountVsUpdateTaskMap = asyncHandler.handle(entity);
        }
        else
        {
            logger.error("No handler for key[" + key + "]. Corresponding Data: " + entity);
        }

        // Condition present since unsure of the underlying implementation.
        // Always an empty Map should be returned and never null.
        if (accountVsUpdateTaskMap != null)
        {
            accountVsUpdateMap.putAll(accountVsUpdateTaskMap);
        }

        return accountVsUpdateMap;
    }

    public Map<String, SyncTaskHandler> getCompositeHandlers()
    {
        return compositeHandlers;
    }

    public void setCompositeHandlers(Map<String, SyncTaskHandler> compositeHandlers)
    {
        this.compositeHandlers = compositeHandlers;
    }

}
