/**
 * 
 */
package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.AccountRelationService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class ScoreAccountRelationTaskHandler implements SyncTaskHandler
{
    public static final String KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(UnBlockAccountRelationTaskHandler.class);

    private AccountRelationService accountRelationService;

    private SyncDBService syncDBService;

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> responseUpdate = new HashMap<String, Set<ResponseEntity>>();
        Set<ResponseEntity> responseEntity = new HashSet<ResponseEntity>();

        String requestByAccountId = record.getAssociatedEntityId();

        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setEntity(record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class));
        syncUpdate.setType(record.getType());
        responseEntity.add(syncUpdate);
        responseUpdate.put(requestByAccountId, responseEntity);
        return responseUpdate;
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.SCORE.toString());
        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
