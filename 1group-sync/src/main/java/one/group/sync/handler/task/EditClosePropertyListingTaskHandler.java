package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ChatThreadService;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;

public class EditClosePropertyListingTaskHandler implements SyncTaskHandler
{

    public static final String KEY = formKey();

    private AddPropertyListingTaskHandler addPropertyListingTaskHandler;

    private CloseExpirePropertyListingTaskHandler closeExpirePropertyListingTaskHandler;

    private ChatThreadService chatThreadService;

    private SubscriptionService subscriptionService;

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public void setAddPropertyListingTaskHandler(AddPropertyListingTaskHandler addPropertyListingTaskHandler)
    {
        this.addPropertyListingTaskHandler = addPropertyListingTaskHandler;
    }

    public void setChatThreadService(ChatThreadService chatThreadService)
    {
        this.chatThreadService = chatThreadService;
    }

    public void setCloseExpirePropertyListingTaskHandler(CloseExpirePropertyListingTaskHandler closeExpirePropertyListingTaskHandler)
    {
        this.closeExpirePropertyListingTaskHandler = closeExpirePropertyListingTaskHandler;
    }

    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> accountVsUpdatesMap = new HashMap<String, Set<ResponseEntity>>();
        accountVsUpdatesMap = closeExpirePropertyListingTaskHandler.handle(record);
        String closedPropertyListingId = record.getAdditionalData(SyncEntryKey.CLOSED_PROPERTY_LISTING_ID);
        String newPropertyListingId = record.getAssociatedEntityId();

        WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
        update.setEntity(new WSEntityReference(HintType.DELETE, EntityType.PROPERTY_LISTING, closedPropertyListingId));

        List<String> accountIdList = subscriptionService.retreiveAllSubscribersOf(closedPropertyListingId, EntityType.PROPERTY_LISTING);

        for (String accountId : accountIdList)
        {
            addToMap(accountVsUpdatesMap, accountId, update);
        }

        chatThreadService.migrateChatThreadsOfBroadcast(closedPropertyListingId, newPropertyListingId);
        for (Entry<String, Set<ResponseEntity>> entry : addPropertyListingTaskHandler.handle(record).entrySet())
        {
            String accountId = entry.getKey();
            Set<ResponseEntity> responseEntityList = entry.getValue();

            if (accountVsUpdatesMap.containsKey(accountId))
            {
                accountVsUpdatesMap.get(accountId).addAll(responseEntityList);
            }
            else
            {
                accountVsUpdatesMap.put(accountId, responseEntityList);
            }

        }
        return accountVsUpdatesMap;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> accountVsUpdatesMap, String accountId, ResponseEntity entity)
    {
        if (accountVsUpdatesMap.containsKey(accountId))
        {
            accountVsUpdatesMap.get(accountId).add(entity);
        }
        else
        {
            Set<ResponseEntity> entitySet = new HashSet<ResponseEntity>();
            entitySet.add(entity);
            accountVsUpdatesMap.put(accountId, entitySet);
        }
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT_CLOSE.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
