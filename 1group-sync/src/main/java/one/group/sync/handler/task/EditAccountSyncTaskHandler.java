/**
 * 
 */
package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.services.AccountRelationService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class EditAccountSyncTaskHandler implements SyncTaskHandler
{
    private static final Logger logger = LoggerFactory.getLogger(EditAccountSyncTaskHandler.class);
    private SubscriptionService subscriptionService;

    private AccountDAO accountDAO;

    private SyncDBService syncDBService;

    private PushService pushService;

    private AccountRelationService accountRelationService;

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public static final String EDIT_ACCOUNTS_KEY = formEditAccountsKey();

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Validation.notNull(record, "The SyncLogEntry should not be null.");
        Map<String, Set<ResponseEntity>> accountVsUpdateMap = new HashMap<String, Set<ResponseEntity>>();

        logger.info("EditAccountSyncTaskHandler");
        long start = System.currentTimeMillis();
        List<String> subscribedAccountIds = subscriptionService.retreiveAllSubscribersOf(record.getAssociatedEntityId(), record.getAssociatedEntityType());
        logger.info("retreiveAllSubscribersOf[" + (System.currentTimeMillis() - start) + "][" + subscribedAccountIds.size() + "]");

        WSEntityReference entityReference = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class);
        WSSyncUpdate update = new WSSyncUpdate();
        update.setEntity(entityReference);
        update.setType(record.getType());

        String editAccountId = record.getAssociatedEntityId();
        Account account = accountDAO.fetchAccountByAccountId(editAccountId);
        if (account == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, editAccountId);
        }

        WSAccount wsOwnAccount = new WSAccount(account, true, true);
        WSSyncUpdate updateOwn2 = new WSSyncUpdate(SyncDataType.ACCOUNT_PROFILE);
        updateOwn2.setAccountProfile(wsOwnAccount);

        WSSyncUpdate updateOwn = new WSSyncUpdate(SyncDataType.INVALIDATION);
        updateOwn.setEntity(new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT, record.getAssociatedEntityId()));
        addToMap(accountVsUpdateMap, record.getAssociatedEntityId(), updateOwn);
        addToMap(accountVsUpdateMap, record.getAssociatedEntityId(), updateOwn2);

        for (String subscribedAccountId : subscribedAccountIds)
        {
            // set Account Relation for subscribedAccountId
            WSAccount wsAccount = new WSAccount(account, true, true);
            WSSyncUpdate subScribeupdate = new WSSyncUpdate(SyncDataType.ACCOUNT_PROFILE);

            WSAccountRelations subscribeRelation = accountRelationService.fetchAccountRelation(subscribedAccountId, editAccountId);
            if (subscribeRelation != null)
            {
                wsAccount.setIsBlocked(subscribeRelation.getIsBlocked());
                wsAccount.setIsContact(subscribeRelation.getIsContact());

                if (subscribeRelation.getIsBlocked())
                {
                    wsAccount.setCanChat(false);
                }
            }
            subScribeupdate.setAccountProfile(wsAccount);
            addToMap(accountVsUpdateMap, subscribedAccountId, update);
            addToMap(accountVsUpdateMap, subscribedAccountId, subScribeupdate);
        }

        return accountVsUpdateMap;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new HashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String formEditAccountsKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.EDIT.toString());
        values.put("entityType", EntityType.ACCOUNT.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }
}
