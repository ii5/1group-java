/**
 * 
 */
package one.group.sync.handler.task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import one.group.cache.PipelineCacheEntity;
import one.group.cache.PipelineEntityScoreComparator;
import one.group.cache.dao.BroadcastCacheDAO;
import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.BroadcastDAO;
import one.group.dao.MessageDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.cache.BroadcastKey;
import one.group.entities.jpa.Broadcast;
import one.group.entities.socket.Message;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.SubscriptionService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ashishthorat
 *
 */
public class CloseRenewBroadcastTaskHandler implements SyncTaskHandler
{
    public static final String RENEW_MESSAGE_KEY = renewFormKey();

    public static final String DELETE_MESSAGE_KEY = closeFormKey();

    private static final Logger logger = LoggerFactory.getLogger(EditBroadcastTaskHandler.class);

    private BroadcastDAO broadcastDAO;

    private MessageDAO messageDAO;

    private BroadcastCacheDAO broadcastCacheDAO;

    private SubscriptionService subscriptionService;

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public BroadcastCacheDAO getBroadcastCacheDAO()
    {
        return broadcastCacheDAO;
    }

    public void setBroadcastCacheDAO(BroadcastCacheDAO broadcastCacheDAO)
    {
        this.broadcastCacheDAO = broadcastCacheDAO;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public BroadcastDAO getBroadcastDAO()
    {
        return broadcastDAO;
    }

    public void setBroadcastDAO(BroadcastDAO broadcastDAO)
    {
        this.broadcastDAO = broadcastDAO;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();

        WSEntityReference wsReference = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSEntityReference.class);

        String broadcastId = wsReference.getId();
        Broadcast broadcast = broadcastDAO.fetchBroadcast(broadcastId);

        if (broadcast == null)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }

        long updatedTime;
        if (!Utils.isNull(broadcast.getUpdatedTime()))
        {
            updatedTime = broadcast.getUpdatedTime().getTime();
        }
        else
        {
            updatedTime = broadcast.getCreatedTime().getTime();
        }

        if (record.getAction().equals(SyncActionType.FETCH))
        {
            // if Brodacast is Renew
            List<Message> msgList = messageDAO.fetchMessagesByBroadcastIdAndMessageStatus(broadcastId, MessageStatus.ACTIVE);

            for (Message message : msgList)
            {
                WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
                update.setEntity(new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, message.getId()));

                String messageCreatedBy = message.getCreatedBy();
                this.getBroadcastPipelinSet(messageCreatedBy, broadcastId, updatedTime);
                addToMap(response, messageCreatedBy, update);

                if (message.getMessageSource().equals(MessageSourceType.DIRECT))
                {
                    String toAccountId = message.getToAccountId();
                    this.getBroadcastPipelinSet(toAccountId, broadcastId, updatedTime);
                    addToMap(response, toAccountId, update);
                }

                List<String> subscribedAccountIdList = subscriptionService.retreiveAllSubscribersOf(EntityType.MESSAGE, Arrays.asList(message.getId()));

                if (subscribedAccountIdList != null)
                {
                    for (String id : subscribedAccountIdList)
                    {
                        addToMap(response, id, update);
                    }
                }
            }

        }
        else if (record.getAction().equals(SyncActionType.DELETE))
        {
            // if Brodacast is closed
            List<Message> msgList = messageDAO.fetchMessagesByBroadcastIdAndMessageStatus(broadcastId, MessageStatus.CLOSED);

            for (Message message : msgList)
            {
                WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
                update.setEntity(new WSEntityReference(HintType.FETCH, EntityType.MESSAGE, message.getId()));
                addToMap(response, message.getCreatedBy(), update);

                if (message.getMessageSource().equals(MessageSourceType.DIRECT))
                {
                    addToMap(response, message.getToAccountId(), update);
                }

                List<String> subscribedAccountIdList = subscriptionService.retreiveAllSubscribersOf(EntityType.MESSAGE, Arrays.asList(message.getId()));

                if (subscribedAccountIdList != null)
                {
                    for (String id : subscribedAccountIdList)
                    {
                        addToMap(response, id, update);
                    }
                }
            }
        }
        return response;
    }

    private void getBroadcastPipelinSet(String accountId, String broadcastId, long updatedTime)
    {

        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        String searchBroadcastKey = BroadcastKey.SEARCHED_BROADCAST_OF_ACCOUNT.getFormedKey(values);

        Set<PipelineCacheEntity> broadcastPipelinSet = new TreeSet<PipelineCacheEntity>(new PipelineEntityScoreComparator());

        PipelineCacheEntity pce = new PipelineCacheEntity(searchBroadcastKey);
        pce.setScore(updatedTime);
        pce.setValue(broadcastId);
        broadcastPipelinSet.add(pce);
        broadcastCacheDAO.saveBroadcastBulk(broadcastPipelinSet);
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    private static String renewFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.FETCH.toString());
        values.put("entityType", EntityType.MESSAGE.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    private static String closeFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.DELETE.toString());
        values.put("entityType", EntityType.MESSAGE.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
