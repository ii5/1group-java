package one.group.sync.handler.task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.cache.dao.CacheAction;
import one.group.core.Constant;
import one.group.core.ScoreConstants;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.dao.AccountRelationDAO;
import one.group.dao.PropertyListingDAO;
import one.group.dao.PropertySearchRelationDAO;
import one.group.dao.SearchDAO;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.WSNewsFeed;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.cache.PropertyListingKey;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.entities.jpa.Search;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.LocationService;
import one.group.services.PropertyListingService;
import one.group.services.SubscriptionService;
import one.group.services.helpers.RelevantDataObject;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class AddPropertyListingTaskHandler implements SyncTaskHandler
{
    public static final String KEY = formKey();

    private static final Logger logger = LoggerFactory.getLogger(AddPropertyListingTaskHandler.class);

    private CacheAction cacheAction;

    private PropertyListingDAO propertyListingDAO;

    private SubscriptionService subscriptionService;

    private SearchDAO searchDAO;

    private AccountDAO accountDAO;

    private AccountRelationDAO accountRelationDAO;

    private PropertyListingService propertyListingService;

    private AccountRelationService accountRelationService;

    private PropertySearchRelationDAO propertySearchRelationDAO;

    private LocationService locationService;

    private AccountService accountService;

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public PropertySearchRelationDAO getPropertySearchRelationDAO()
    {
        return propertySearchRelationDAO;
    }

    public void setPropertySearchRelationDAO(PropertySearchRelationDAO propertySearchRelationDAO)
    {
        this.propertySearchRelationDAO = propertySearchRelationDAO;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public AccountRelationDAO getAccountRelationDAO()
    {
        return accountRelationDAO;
    }

    public void setAccountRelationDAO(AccountRelationDAO accountRelationDAO)
    {
        this.accountRelationDAO = accountRelationDAO;
    }

    public PropertyListingDAO getPropertyListingDAO()
    {
        return propertyListingDAO;
    }

    public void setPropertyListingDAO(PropertyListingDAO propertyListingDAO)
    {
        this.propertyListingDAO = propertyListingDAO;
    }

    public CacheAction getCacheAction()
    {
        return cacheAction;
    }

    public void setCacheAction(CacheAction cacheAction)
    {
        this.cacheAction = cacheAction;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SearchDAO getSearchDAO()
    {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO)
    {
        this.searchDAO = searchDAO;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    @Transactional(rollbackOn = { Exception.class })
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();
        List<String> exactMatchLocationIds = new ArrayList<String>();
        List<RelevantDataObject> relevanceDataList = new ArrayList<RelevantDataObject>();

        String entityId = record.getAssociatedEntityId();

        PropertyListing propertyListing = propertyListingDAO.fetchPropertyListing(entityId);
        if (propertyListing == null)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }

        Location location = propertyListing.getLocation();
        String localityId = location.getId();
        Account account = propertyListing.getAccount();

        // Invalidate Cache
        List<String> propertiesMatchingLocality = propertyListingDAO.fetchPropertiesByLocation(localityId);
        for (String pId : propertiesMatchingLocality)
        {
            Map<String, String> values = new HashMap<String, String>();
            values.put("property_listing_id", pId);
            cacheAction.invalidate(PropertyListingKey.PROPERTY_LISTING_KEY.getFormedKey(values));
        }

        // Get exact match locations
        List<Location> exactMatchLocations = locationService.getMatchingLocation(location, false);

        for (Location exactMatchLocation : exactMatchLocations)
        {
            if (!exactMatchLocationIds.contains(exactMatchLocation.getId()))
            {
                exactMatchLocationIds.add(exactMatchLocation.getId());
            }
        }

        // Get all accounts subscribed to location.
        // List<String> locationSubscribedAccountList =
        // subscriptionService.retreiveAllSubscribersOf(EntityType.LOCATION,
        // exactMatchLocationIds);
        // API-76 Avoid using scan.
        List<String> locationSubscribedAccountList = accountService.fetchAllAccountIdsSubscribedToLocations(exactMatchLocationIds);
        // Get all search relation matching the newly added property listing
        List<PropertySearchRelation> propertyMatchSearchRelationList = propertyListingService.savePropertySearchRelation(propertyListing, exactMatchLocations);
        // Get all search matching newly added property listing
        List<Search> propertyMatchingSearchList = extractSearchFromRelation(propertyMatchSearchRelationList);
        // Get all search matching location id.
        List<Search> locationMatchSearchList = new ArrayList<Search>();
        if (!exactMatchLocationIds.isEmpty())
        {
            locationMatchSearchList.addAll(searchDAO.fetchSearchByLocationList(exactMatchLocationIds));
        }
        // Get all search of locationSubscribedAccountList
        List<Search> locationSubscribedAccountsSearch = new ArrayList<Search>();
        if (!locationSubscribedAccountList.isEmpty())
        {
            locationSubscribedAccountsSearch.addAll(searchDAO.fetchSearchByAccountIds(locationSubscribedAccountList));
        }

        // form temporary RelevantDataObject
        formAlphaRelevanceDataList(propertyListing, account, locationSubscribedAccountsSearch, propertyMatchingSearchList, locationMatchSearchList, locationSubscribedAccountList, relevanceDataList,
                location);

        relevanceDataList = createPropertyListingRelevanceMap(propertyListing, account, location, exactMatchLocationIds, relevanceDataList);

        // / Add update to clients of own account.
        WSSyncUpdate update = new WSSyncUpdate(SyncDataType.INVALIDATION);
        update.setEntity(new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING, propertyListing.getId()));
        addToMap(response, account.getId(), update);

        for (RelevantDataObject relevanceData : relevanceDataList)
        {
            if (!relevanceData.getMatchingSavedSearchIdList().isEmpty())
            {
                for (String searchId : relevanceData.getMatchingSavedSearchIdList())
                {
                    WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING_SEARCH, searchId);
                    WSSyncUpdate syncupdate = new WSSyncUpdate();
                    syncupdate.setEntity(entityReference);
                    syncupdate.setType(SyncDataType.INVALIDATION);
                    addToMap(response, relevanceData.getRelevantAccountId(), syncupdate);
                }
            }

            // newsFeedService.saveNewsFeed(relevanceData.getRelevantAccountId(),
            // propertyListing.getId(), propertyListing.getAccount().getId(),
            // relevanceData.getRelevanceScore(), ""
            // + propertyListing.getCreatedTime().getTime());

            long timeInMiiliseconds = propertyListing.getCreatedTime().getTime();

            // setting Expiry Time for NewsFeed API -109
            long propertyListingExpiryTime;
            if (propertyListing.getLastRenewedTime() == null)
            {
                propertyListingExpiryTime = timeInMiiliseconds + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS;
            }
            else
            {
                propertyListingExpiryTime = (Math.max(propertyListing.getLastRenewedTime().getTime(), propertyListing.getCreatedTime().getTime()) + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS);
            }

            WSNewsFeed newsFeed = new WSNewsFeed(propertyListing.getId(), propertyListing.getAccount().getId(), relevanceData.getRelevanceScore(), "" + timeInMiiliseconds, ""
                    + propertyListingExpiryTime);
            WSSyncUpdate newsSyncUpdate = new WSSyncUpdate();
            newsSyncUpdate.setType(SyncDataType.NEWS);
            newsSyncUpdate.setNewsFeed(newsFeed);
            addToMap(response, relevanceData.getRelevantAccountId(), newsSyncUpdate);
        }

        logger.info("Response: " + response);
        return response;
    }

    private void formAlphaRelevanceDataList(PropertyListing propertyListing, Account account, List<Search> locationSubscribedAccountSearchList, List<Search> propertyMatchingSearchList,
            List<Search> locationMatchSearchList, List<String> locationSubscribedAccountList, List<RelevantDataObject> relevanceDataList, Location propertyLocation)
    {
        Set<Search> tempSearchList = new HashSet<Search>(propertyMatchingSearchList);
        tempSearchList.addAll(locationMatchSearchList);
        tempSearchList.addAll(locationSubscribedAccountSearchList);

        // add property listings owning account
        RelevantDataObject own = new RelevantDataObject();
        own.setRelevantAccountId(account.getId());
        // own.setLocationWithList(account.getLocations());
        relevanceDataList.add(own);

        for (Search tempSearch : tempSearchList)
        {
            RelevantDataObject o = new RelevantDataObject();
            o.setRelevantAccountId(tempSearch.getCreatedBy());
            // o.setLocationWithList(tempSearch.getAccount().getLocations());
            o.addToSearches(tempSearch);
            if (!relevanceDataList.contains(o))
            {
                relevanceDataList.add(o);
            }
            else
            {
                int i = relevanceDataList.indexOf(o);
                relevanceDataList.get(i).addToSearches(tempSearch);
            }
        }

        for (String accountId : locationSubscribedAccountList)
        {
            RelevantDataObject o = new RelevantDataObject();
            o.setRelevantAccountId(accountId);
            o.addLocation(propertyLocation.getId());
            if (!relevanceDataList.contains(o))
            {
                relevanceDataList.add(o);
            }
        }
    }

    private List<Search> extractSearchFromRelation(List<PropertySearchRelation> searchRelationList)
    {
        List<Search> searchList = new ArrayList<Search>();

        for (PropertySearchRelation relation : searchRelationList)
        {
            searchList.add(relation.getSearchId());
        }

        return searchList;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    public List<RelevantDataObject> createPropertyListingRelevanceMap(PropertyListing propertyListing, Account owningAccount, Location propertyLocation, List<String> exactMatchLocationIds,
            List<RelevantDataObject> relevantDataList)
    {
        Map<String, WSAccountRelations> accountVsRelationsMap = accountRelationService.fetchAccountRelationsAsMap(owningAccount.getId());

        List<RelevantDataObject> filteredRelavanceDataList = new ArrayList<RelevantDataObject>();

        for (RelevantDataObject accountRelevantData : relevantDataList)
        {
            WSAccountRelations accountRelation = accountVsRelationsMap.get(accountRelevantData.getRelevantAccountId());
            Set<Search> searchSet = accountRelevantData.getSearchSet();
            accountRelevantData.setSubscribed(subscriptionService.isSubscribed(accountRelevantData.getRelevantAccountId(), propertyListing.getId(), EntityType.PROPERTY_LISTING));
            int recentSearchCount = 0;
            Calendar c = Calendar.getInstance();
            c.set(1984, Calendar.NOVEMBER, 21);
            Date mostRecentSearch = c.getTime();
            Date latestMatchingSavedSearchTime = c.getTime();

            if (accountRelevantData.getRelevantAccountId().equals(owningAccount.getId()))
            {
                accountRelevantData.setPostingAccount(true);
            }

            if (accountRelevantData.getLocationList().contains(propertyLocation.getId()))
            {
                accountRelevantData.setPropertyInSameLocation(true);
            }

            if (accountRelation != null)
            {
                accountRelevantData.setIsBlocked(accountRelation.getIsBlockedBy());
                accountRelevantData.setIsBlockedBy(accountRelation.getIsBlocked());
                accountRelevantData.setIsContact(accountRelation.getIsContactOf());
                accountRelevantData.setIsContactOf(accountRelation.getIsContactOf());
                accountRelevantData.setScore(accountRelation.getScoredAs());
                accountRelevantData.setScoredAs(accountRelation.getScore());
            }

            for (Search search : searchSet)
            {
                if (exactMatchLocationIds.contains(search.getLocationId()))
                {
                    accountRelevantData.addMatchingSavedSearchIdList(search.getId());
                    latestMatchingSavedSearchTime = search.getSearchTime().after(latestMatchingSavedSearchTime) ? search.getSearchTime() : latestMatchingSavedSearchTime;
                }
                else if (exactMatchLocationIds.contains(search.getLocationId()))
                {
                    recentSearchCount++;
                    mostRecentSearch = search.getSearchTime().after(mostRecentSearch) ? search.getSearchTime() : mostRecentSearch;
                }
            }

            accountRelevantData.setMatchingRecentSearchCount(recentSearchCount);

            if (!mostRecentSearch.equals(c.getTime()))
            {
                accountRelevantData.setLatestRecentSearchTime(mostRecentSearch.getTime());
            }

            if (!latestMatchingSavedSearchTime.equals(c.getTime()))
            {
                accountRelevantData.setLatestMatchingSavedSearchTime(latestMatchingSavedSearchTime.getTime());
            }

            PropertyListingScoreObject scoreObject = new PropertyListingScoreObject(propertyListing, owningAccount, propertyLocation);
            float relevantScore = 0; // TODO
                                     // scoreService.getNewsFeedScore(accountRelevantData,
                                     // scoreObject);
            accountRelevantData.setRelevanceScore(relevantScore);

            if (relevantScore >= ScoreConstants.MINIMUM_NEWS_FEED_INCLUSION_SCORE)
            {
                filteredRelavanceDataList.add(accountRelevantData);
            }
        }

        return filteredRelavanceDataList;
    }

    private static String formKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.ADD.toString());
        values.put("entityType", EntityType.PROPERTY_LISTING.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
