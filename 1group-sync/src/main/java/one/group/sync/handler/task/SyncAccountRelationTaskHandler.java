package one.group.sync.handler.task;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.cache.dao.CacheAction;
import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.dao.AccountDAO;
import one.group.dao.LocationMatchDAO;
import one.group.dao.PropertyListingDAO;
import one.group.entities.api.request.WSContactsInfo;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Account;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.AccountRelationService;
import one.group.services.LocationService;
import one.group.services.PropertyListingService;
import one.group.services.SearchService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyncAccountRelationTaskHandler implements SyncTaskHandler
{
    public static final String SYNC_ACCOUNT_KEY = syncAccountFormKey();

    public static final String SYNC_ACCOUNT_RELATIONS_KEY = syncAccountRelationsFormKey();

    public static final String REGISTER_ACCOUNT_KEY = registerAccountFormKey();

    public static final String REGISTER_ACCOUNT_RELATIONS_KEY = registerAccountRelationsFormKey();

    private static final Logger logger = LoggerFactory.getLogger(SyncAccountRelationTaskHandler.class);

    private AccountDAO accountDAO;

    private SyncDBService syncDBService;

    private SubscriptionService subscriptionService;

    private LocationMatchDAO locationMatchDAO;

    private PropertyListingDAO propertyListingDAO;

    private CacheAction cacheAction;

    private AccountRelationService accountRelationService;

    private PropertyListingService propertyListingService;

    private LocationService locationService;

    private SearchService searchService;

    public SearchService getSearchService()
    {
        return searchService;
    }

    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public PropertyListingDAO getPropertyListingDAO()
    {
        return propertyListingDAO;
    }

    public void setPropertyListingDAO(PropertyListingDAO propertyListingDAO)
    {
        this.propertyListingDAO = propertyListingDAO;
    }

    public LocationMatchDAO getLocationMatchDAO()
    {
        return locationMatchDAO;
    }

    public void setLocationMatchDAO(LocationMatchDAO locationMatchDAO)
    {
        this.locationMatchDAO = locationMatchDAO;
    }

    public AccountDAO getAccountDAO()
    {
        return accountDAO;
    }

    public void setAccountDAO(AccountDAO accountDAO)
    {
        this.accountDAO = accountDAO;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public CacheAction getCacheAction()
    {
        return cacheAction;
    }

    public void setCacheAction(CacheAction cacheAction)
    {
        this.cacheAction = cacheAction;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    @Transactional
    public Map<String, Set<ResponseEntity>> handle(SyncLogEntry record) throws Abstract1GroupException
    {
        // To preserve insertion order
        Set<ResponseEntity> responseEntityList = new LinkedHashSet<ResponseEntity>();
        Map<String, Set<ResponseEntity>> response = new HashMap<String, Set<ResponseEntity>>();

        String accountId = record.getAssociatedEntityId();
        // Account account = accountDAO.fetchAccountById(accountId);
        //
        // List<Location> matchingLocations = new ArrayList<Location>();

        // if (account.getLocations() != null)
        // {
        // for (Location location : account.getLocations())
        // {
        // matchingLocations.add(location);
        // matchingLocations.addAll(locationService.getMatchingLocation(location,
        // true));
        // }
        // }

        if (record.getAssociatedEntityType().equals(EntityType.ACCOUNT_RELATION))
        {
            WSContactsInfo contactsInfo = null;
            if (SyncActionType.REGISTER.equals(record.getAction()))
            {
                Set<String> deltaContacts = accountRelationService.syncContactsForAccountBulk(accountId);
                contactsInfo = new WSContactsInfo();
                contactsInfo.setContactsSyncFlagFlipped(false);
                contactsInfo.setDeltaContacts(deltaContacts);
            }
            else
            {
                contactsInfo = record.getAdditionalDataAs(SyncEntryKey.CONTACTS, WSContactsInfo.class);
            }
            if (contactsInfo != null)
            {
                // Boolean isInitialContactsSyncFlagFlipped =
                // contactsInfo.isContactsSyncFlagFlipped();
                Set<String> otherDeltaAccountIds = contactsInfo.getDeltaContacts();
                // API-76 AND API-85 Do not re-generate or generate any news
                // feed at contacts sync.
                // if (isInitialContactsSyncFlagFlipped)
                // {// Only happens once for an account,
                // List<String> otherAccountIds = new ArrayList<String>();
                // Map<String, String> values = new HashMap<String, String>();
                // values.put("account_id", accountId);
                // cacheAction.invalidate(NewsFeedKey.SORTED_SET_NEWS_FEED.getFormedKey(values));
                // Set<WSAccountRelations> relations =
                // accountRelationService.fetchContactsOfAccount(accountId);
                //
                // WSEntityReference ref = new
                // WSEntityReference(HintType.DELETE, EntityType.NEWS_FEED,
                // null);
                // WSSyncUpdate update = new
                // WSSyncUpdate(SyncDataType.INVALIDATION);
                // update.setEntity(ref);
                //
                // addToMap(response, accountId, update);
                //
                // for (WSAccountRelations relation : relations)
                // {
                // String otherAccountId = relation.getContactId();
                // if (accountId != null && otherAccountId != null &&
                // !accountId.equals(otherAccountId))
                // {
                // otherAccountIds.add(otherAccountId);
                // }
                // }
                // response = processNewsFeed(account, otherAccountIds,
                // response, responseEntityList, matchingLocations, false,
                // true);
                // }
                // else if (account.isInitialContactsSyncComplete())
                // {
                // // API-76 - Ignoring news feed generation for additional
                // contacts established if once the client registers a finished
                // the contacts sync.
                // response = processNewsFeed(account, otherDeltaAccountIds,
                // response, responseEntityList, matchingLocations, false,
                // false);
                // }

                for (String otherAccountId : otherDeltaAccountIds)
                {
                    if (accountId != null && otherAccountId != null && !accountId.equals(otherAccountId))
                    {
                        WSAccountRelations relation = accountRelationService.fetchAccountRelation(accountId, otherAccountId);
                        response = generateSyncUpdatesForContactRelations(relation, response, responseEntityList, accountId, otherAccountId);
                        if (relation.getIsContact())
                            subscriptionService.subscribe(accountId, otherAccountId, EntityType.ACCOUNT);
                        else if (relation.getIsContactOf())
                            subscriptionService.subscribe(otherAccountId, accountId, EntityType.ACCOUNT);
                    }
                }
            }
        }
        return response;
    }

    private Map<String, Set<ResponseEntity>> generateSyncUpdatesForContactRelations(WSAccountRelations relation, Map<String, Set<ResponseEntity>> response, Set<ResponseEntity> responseEntityList,
            String accountId, String otherAccountId)
    {
        if (relation != null)
        {// UniDirectional A-->C
            if (relation.getIsContact())
            {
                WSSyncUpdate update = new WSSyncUpdate();
                WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT_RELATION, otherAccountId);
                update.setEntity(entityReference);
                update.setType(SyncDataType.INVALIDATION);
                addToMap(response, accountId, update);

                Account OtherAccount = accountDAO.fetchAccountById(otherAccountId);
                WSAccount WsOtherAccount = new WSAccount(OtherAccount, true, true);
                WsOtherAccount.setIsBlocked(relation.getIsBlocked());
                WsOtherAccount.setIsContact(relation.getIsContact());

                if (relation.getIsBlocked())
                {
                    WsOtherAccount.setCanChat(false);
                }

                WSSyncUpdate syncUpdateTargetAccount = new WSSyncUpdate(SyncDataType.ACCOUNT_RELATION);
                syncUpdateTargetAccount.setAccountRelation(WsOtherAccount);
                addToMap(response, accountId, syncUpdateTargetAccount);
            }
            else if (relation.getIsContactOf())
            {
                WSSyncUpdate update = new WSSyncUpdate();
                WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT_RELATION, accountId);
                update.setEntity(entityReference);
                update.setType(SyncDataType.INVALIDATION);
                addToMap(response, otherAccountId, update);

                Account account = accountDAO.fetchAccountById(accountId);
                WSAccount WsAccount = new WSAccount(account, true, true);
                WsAccount.setIsBlocked(relation.getIsBlockedBy());
                WsAccount.setIsContact(relation.getIsContactOf());
                if (relation.getIsBlocked())
                {
                    WsAccount.setCanChat(false);
                }

                WSSyncUpdate syncUpdate = new WSSyncUpdate(SyncDataType.ACCOUNT_RELATION);
                syncUpdate.setAccountRelation(WsAccount);
                addToMap(response, otherAccountId, syncUpdate);
            }
        }
        return response;
    }

    private void addToMap(Map<String, Set<ResponseEntity>> syncUpdateMap, String accountId, WSSyncUpdate update)
    {
        if (syncUpdateMap.containsKey(accountId))
        {
            syncUpdateMap.get(accountId).add(update);
        }
        else
        {
            // To preserve insertion order
            Set<ResponseEntity> updateSet = new LinkedHashSet<ResponseEntity>();
            updateSet.add(update);
            syncUpdateMap.put(accountId, updateSet);
        }
    }

    /**
     * Generate sync account key for sync handler
     * 
     * @return
     */
    private static String syncAccountFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.SYNC_CONTACTS.toString());
        values.put("entityType", EntityType.ACCOUNT.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate sync account relations key for sync handler
     * 
     * @return
     */
    private static String syncAccountRelationsFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.SYNC_CONTACTS.toString());
        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate register account key for sync handler
     * 
     * @return
     */
    private static String registerAccountFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.REGISTER.toString());
        values.put("entityType", EntityType.ACCOUNT.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

    /**
     * Generate register account relations key for sync handler
     * 
     * @return
     */
    private static String registerAccountRelationsFormKey()
    {
        Map<String, String> values = new HashMap<String, String>();
        values.put("action", SyncActionType.REGISTER.toString());
        values.put("entityType", EntityType.ACCOUNT_RELATION.toString());
        String key = Utils.populateKey(Constant.ASYNC_HANDLER_KEY_FORMAT, values);
        return key;
    }

}
