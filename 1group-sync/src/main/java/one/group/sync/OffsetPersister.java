package one.group.sync;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.Properties;

import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 *
 */
public class OffsetPersister implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(OffsetPersister.class);
    private Properties p;
    private String absoluteFileLocation;
    private int sleepTime = 2000;
    
    public OffsetPersister(Properties p, int sleepTime, String absoluteFileLocation)
    {
        this.p = p;
        this.sleepTime = sleepTime;
        this.absoluteFileLocation = absoluteFileLocation;
    }
    
    public void run()
    {
        if (p != null)
        {
            while (true)
            {
                FileOutputStream fos = null;
                try
                {
                    fos = new FileOutputStream(absoluteFileLocation);
                    p.store(fos, new Date().toString());
                    Thread.sleep(sleepTime);
                }
                catch (Exception e)
                {
                    logger.error("Error while persisting file: " + absoluteFileLocation + ".", e);
                }
                finally
                {
                    Utils.close(fos);
                }
            }
        }
    }
}
