package one.group.sync.consumer;

import java.util.Map;
import java.util.Set;

import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSSubscribeEntity;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.sync.AbstractSyncHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles consumed records of type {@link SyncDataType#SUBSCRIBE}.
 * 
 * @author nyalfernandes
 * 
 */
public class SubscribeSyncHandler extends AbstractSyncHandler<WSSubscribeEntity>
{
    private static final Logger logger = LoggerFactory.getLogger(SubscribeSyncHandler.class);
    private SubscriptionService subscriptionService;
    
    private ClientService clientService;
    
    private PushService pushService;

    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry record) throws Abstract1GroupException
    {
        if (record.getType().equals(SyncDataType.SUBSCRIBE))
        {
            WSSubscribeEntity data = record.getAdditionalDataAs(SyncEntryKey.SYNC_UPDATE_DATA, WSSubscribeEntity.class);
            subscriptionService.subscribe(data.getAccountId(), data.getRecordId(), data.getType(), data.getExpiresIn());
            logger.info("Subscribed to entity: " + data);
        }
        else
        {
            logger.warn("End of Sync Handler chain. No more handlers configured.");
        }

        return null;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }
    
    public ClientService getClientService()
    {
        return clientService;
    }
    
    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }
    
    public PushService getPushService()
    {
        return pushService;
    }
    
    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

}
