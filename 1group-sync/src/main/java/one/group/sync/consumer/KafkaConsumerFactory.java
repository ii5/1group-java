package one.group.sync.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.consumer.TopicFilter;
import kafka.consumer.Whitelist;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.utils.Utils;
import one.group.utils.validation.Validation;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;

/**
 * Factory object to create Kafka consumers.
 * 
 * @author nyalfernandes
 * 
 */
public class KafkaConsumerFactory
{
    private Properties configProperties = new Properties();

    public ConsumerConnector getConsumerConnector(String consumerGroupId, String consumerId)
    {
        // Validation.notNull(consumerGroupId,
        // "Consumer group should not be null.");
        // Validation.notNull(consumerId, "Consumer Id should not be null.");

        Properties p = new Properties();
        p.putAll(configProperties);
        if (!Utils.nullOrEmpty(consumerGroupId))
        {
            p.setProperty(GROUP_ID_CONFIG, consumerGroupId);
        }
        
        if(!Utils.nullOrEmpty(consumerId))
        {
            p.setProperty("consumer.id", consumerId);
        }
        
        ConsumerConfig config = new ConsumerConfig(p);
        ConsumerConnector consumer = Consumer.createJavaConsumerConnector(config);

        return consumer;
    }

    public List<KafkaStream<byte[], byte[]>> getKafkaStreams(String consumerGroupId, String consumerPreFix, String topic, int numStreams, int consumerCount)
    {
        return createKafkaStreams(consumerGroupId, consumerPreFix, topic, numStreams, consumerCount);
    }

    public List<KafkaStream<byte[], byte[]>> getKafkaStreams(String consumerGroupId, String topic, int numStreams, int consumerCount)
    {
        return createKafkaStreams(consumerGroupId, null, topic, numStreams, consumerCount);
    }

    private List<KafkaStream<byte[], byte[]>> createKafkaStreams(String consumerGroupId, String consumerPreFix, String topic, int numStreams, int consumerCount)
    {
        Validation.notNull(topic, "Topic should not be null.");
//        Validation.isTrue(consumerCount > 0, "Consumer Count should be greater than 0.");
        String consumerId = null;
        
        if (consumerGroupId == null)
        {
            consumerGroupId = "MIG_" + topic;
        }
        
        if(numStreams == 0)
        {
            numStreams = Integer.parseInt(configProperties.getProperty("number.of.streams"));
        }
        
        List<KafkaStream<byte[], byte[]>> kafkaStreams = new ArrayList<KafkaStream<byte[], byte[]>>();
//        consumerCount = 1;
        int i = 1;
//        for (int i = 0; i < consumerCount; i++)
//        {
            if (consumerPreFix != null)
            {
                consumerId = consumerPreFix + consumerId + ":" + i;
            }
            else
            {
                consumerId = "MIGC" + ":" + i;
            }
            
            ConsumerConnector consumer = getConsumerConnector(consumerGroupId + i, consumerId);
            Map<String, Integer> topicCount = new HashMap<String, Integer>();
            topicCount.put(topic, numStreams);
            TopicFilter filter = new Whitelist(topic);
            
            Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCount);
            kafkaStreams.addAll(consumerMap.get(topic));
//        }

        return kafkaStreams;
    }

    public Properties getConfigProperties()
    {
        return configProperties;
    }

    public void setConfigProperties(Properties configProperties)
    {
        this.configProperties = configProperties;
    }
}
