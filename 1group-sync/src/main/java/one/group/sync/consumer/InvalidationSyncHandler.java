package one.group.sync.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.PushServerType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSEntityReference;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.socket.SocketEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.ClientService;
import one.group.services.PushService;
import one.group.services.SubscriptionService;
import one.group.services.SyncDBService;
import one.group.services.helpers.PushServiceObject;
import one.group.sync.AbstractSyncHandler;
import one.group.sync.handler.task.SyncTaskHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles consumed records of type {@link SyncDataType#INVALIDATION}.
 * 
 * @author nyalfernandes
 * 
 */
public class InvalidationSyncHandler extends AbstractSyncHandler<WSEntityReference>
{
    private static final Logger logger = LoggerFactory.getLogger(InvalidationSyncHandler.class);

    private ClientService clientService;

    private PushService pushService;

    private SubscriptionService subscriptionService;

    private SyncDBService syncDBService;

    private SyncTaskHandler syncTaskHandler;

    @Override
    public Map<String, Set<ResponseEntity>> process(SyncLogEntry syncLogEntry) throws Abstract1GroupException
    {

        if (syncLogEntry.getType().equals(SyncDataType.INVALIDATION))
        {
            Map<String, Set<ResponseEntity>> accountVsSyncUpdate = syncTaskHandler.handle(syncLogEntry);

            // mapping SyncUpdate Vs AccountId
            Map<String, List<WSSyncUpdate>> accountVsSyncUpdateList = new HashMap<String, List<WSSyncUpdate>>();
            for (String accountId : accountVsSyncUpdate.keySet())
            {
                for (ResponseEntity syncUpdate : accountVsSyncUpdate.get(accountId))
                {
                    WSSyncUpdate updateOrig = (WSSyncUpdate) syncUpdate;
                    WSSyncUpdate updateCopy = new WSSyncUpdate(updateOrig);
                    syncDBService.appendUpdate(accountId, updateCopy);

                    if (accountVsSyncUpdateList.containsKey(accountId))
                    {
                        List<WSSyncUpdate> syncList = accountVsSyncUpdateList.get(accountId);
                        syncList.add(updateCopy);
                        accountVsSyncUpdateList.put(accountId, syncList);
                    }
                    else
                    {
                        List<WSSyncUpdate> syncUpdateList = new ArrayList<WSSyncUpdate>();
                        syncUpdateList.add(updateCopy);
                        accountVsSyncUpdateList.put(accountId, syncUpdateList);
                    }
                }

                SocketEntity entity = new SocketEntity(accountVsSyncUpdateList.get(accountId));

                PushServiceObject config = new PushServiceObject();
                config.addService(PushServerType.WEBSOCKET);
                config.setAccountId(accountId);
                config.setPusherEventName("");
                config.setData(entity);
                // V2-637 : disable GCM notification

                // if (SyncActionType.REGISTER.equals(syncLogEntry.getAction()))
                // {
                // String requesterAccountId =
                // syncLogEntry.getAssociatedEntityId();
                // List<Client> allClientsOfAccount =
                // clientService.fetchAllActiveClientsOfAccount(accountId);
                // List<String> androidDeviceIds = new ArrayList<String>();
                // for (Client client : allClientsOfAccount)
                // {
                // androidDeviceIds.add(client.getCpsId());
                // }
                // config.addService(PushServerType.GCM);
                // config.setGcmDeviceList(androidDeviceIds);
                // }

                pushService.forwardRequest(config);
            }

        }
        else
        {
            getSuccessor().handle(syncLogEntry);
        }
        return null;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public PushService getPushService()
    {
        return pushService;
    }

    public void setPushService(PushService pushService)
    {
        this.pushService = pushService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public SyncTaskHandler getSyncTaskHandler()
    {
        return syncTaskHandler;
    }

    public void setSyncTaskHandler(SyncTaskHandler syncTaskHandler)
    {
        this.syncTaskHandler = syncTaskHandler;
    }

}
