package one.group.sync.consumer;

import kafka.consumer.KafkaStream;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.movein.SyncLogProcessException;


/**
 * The reader of the sync log
 * 
 * @author nyalfernandes
 *
 */
public interface SyncLogConsumer
{
    public SyncLogEntry read(KafkaStream<byte[], byte[]> stream) throws SyncLogProcessException;
}
