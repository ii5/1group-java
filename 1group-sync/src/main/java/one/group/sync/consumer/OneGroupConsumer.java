package one.group.sync.consumer;

import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import kafka.api.FetchRequest;
import kafka.api.FetchRequestBuilder;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.common.ErrorMapping;
import kafka.common.OffsetAndMetadata;
import kafka.common.TopicAndPartition;
import kafka.javaapi.FetchResponse;
import kafka.javaapi.OffsetCommitRequest;
import kafka.javaapi.OffsetRequest;
import kafka.javaapi.OffsetResponse;
import kafka.javaapi.PartitionMetadata;
import kafka.javaapi.TopicMetadata;
import kafka.javaapi.TopicMetadataRequest;
import kafka.javaapi.TopicMetadataResponse;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.message.MessageAndOffset;
import one.group.core.Constant;
import one.group.core.enums.TopicType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.sync.SyncLogEntry;
import one.group.sync.SyncHandler;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * https://cwiki.apache.org/confluence/display/KAFKA/0.8.0+SimpleConsumer+
 * Example
 * 
 * @author nyalfernandes
 * 
 */
public class OneGroupConsumer implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(OneGroupConsumer.class);

    private SyncHandler syncHandler;

    private Properties offsetProperties;

    private int partition;

    private String clientPreFix;

    private String groupId;

    private List<Broker> brokerList = new ArrayList<Broker>();;

    private int timeout;

    private int bufferSize;

    private String topic;

    private TopicType topicType;

    private String clientId;

    private File syncOffsetFile;

    private File errorOffsetFile;

    private Properties errorOffsetProperties;

    public OneGroupConsumer(int partition, Properties offsetProperties, Properties consumerProperties, File syncOffsetFile, String topicName, TopicType topicType, File errorOffsetFile,
            Properties errorOffsetProperties)
    {
        Validation.notNull(offsetProperties, "OffsetProperties file should not be null.");
        Validation.notNull(consumerProperties, "Consumer Properties file should not be null.");
        Validation.isTrue(!consumerProperties.isEmpty(), "Consumer Properties should not be empty.");
        Validation.isTrue(!(partition < 0), "Partition number should not be less than 0.");
        Validation.notNull(syncOffsetFile, "Sync Offset File should not be null.");
        Validation.notNull(errorOffsetFile, "Error Offset File should not be null.");
        Validation.notNull(errorOffsetProperties, "ErrorOffsetProperties file should not be null.");
        validateConsumerProperties(consumerProperties, topicType);

        this.offsetProperties = offsetProperties;
        this.partition = partition;
        this.syncOffsetFile = syncOffsetFile;
        this.topic = topicName;
        this.topicType = topicType;
        this.clientId = clientPreFix + "-" + topic + "-" + partition;
        this.errorOffsetFile = errorOffsetFile;
        this.errorOffsetProperties = errorOffsetProperties;
    }

    public void run()
    {
        readData();
    }

    public void readData()
    {
        SimpleConsumer consumer = null;
        try
        {
            PartitionMetadata metaData = findLeader(brokerList, topic, partition);
            if (metaData == null)
            {
                logger.error("Cannot find metadata for Topic and Parition. Exiting!");
                return;
            }

            if (metaData.leader() == null)
            {
                logger.error("Cannot find leader for Topic or Partition. Exiting!");
                return;
            }

            String leader = InetAddress.getByName(metaData.leader().host()).getHostAddress();
            int leaderPort = metaData.leader().port();

            consumer = new SimpleConsumer(leader, leaderPort, timeout, bufferSize, clientId);

            long whichTime = System.currentTimeMillis();
            // long readOffset = getLastOffset(consumer, topic, partition,
            // whichTime, clientId);
            long readOffset = Long.parseLong(offsetProperties.getProperty("" + partition, "0"));
            int numErrors = 0;

            logger.info(clientId + " initiated.");
            while (true)
            {
                if (consumer == null)
                {
                    consumer = new SimpleConsumer(leader, leaderPort, timeout, bufferSize, clientId);
                }

                FetchRequest request = new FetchRequestBuilder().clientId(clientId).addFetch(topic, partition, readOffset, bufferSize).maxWait(2000).build();
                FetchResponse response = consumer.fetch(request);

                if (response.hasError())
                {
                    numErrors++;
                    int code = response.errorCode(topic, partition);
                    logger.error("Error(" + numErrors + ") in consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + " errorCode=" + code + "]");
                    if (numErrors > 5)
                    {
                        break;
                    }

                    if (code == ErrorMapping.OffsetOutOfRangeCode())
                    {
                    	logger.info("Fetching last offset: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + " errorCode=" + code + "]");
                        readOffset = getLastOffset(consumer, topic, partition, whichTime, clientId);
                    }
                    consumer.close();
                    consumer = null;

                    leader = findNewLeader(brokerList, leader, topic, partition);
                    continue;
                }
                numErrors = 0;

                ByteBufferMessageSet byteBuffermessageSet = response.messageSet(topic, partition);
                Iterator<MessageAndOffset> msgAndOffset = byteBuffermessageSet.iterator();

                if (!msgAndOffset.hasNext() && byteBuffermessageSet.sizeInBytes() >= bufferSize)
                {
                    Properties commitErroroffsetProperties = commitErrorOffset(partition, readOffset, errorOffsetProperties, errorOffsetFile);
                    commitOffsets(commitErroroffsetProperties, errorOffsetFile);

                    throw new Exception("Unable to consume large message !!!");
                }

                for (MessageAndOffset messageAndOffset : response.messageSet(topic, partition))
                {

                    long currentOffset = messageAndOffset.offset();
                    if (currentOffset < readOffset)
                    {
                        logger.warn("[" + topic + ":" + partition + "] Current offset < Read Offset, [CurrOffset: " + currentOffset + ", ReadOffset:" + readOffset + "]");
                        continue;
                    }
                    readOffset = messageAndOffset.nextOffset();
                    ByteBuffer payload = messageAndOffset.message().payload();

                    byte[] bytes = new byte[payload.limit()];
                    payload.get(bytes);

                    String rawData = new String(bytes, Constant.UTF8_ENCODING);
                    logger.info(clientId + " [" + currentOffset + ": " + rawData + "]");

                    try
                    {
                        SyncLogEntry syncLogEntry = (SyncLogEntry) Utils.getInstanceFromJson(rawData, SyncLogEntry.class);
                        Map<String, Set<ResponseEntity>> accountSyncUpdateList = syncHandler.handle(syncLogEntry);
                    }
                    catch (Exception e)
                    {
                        Properties commitErroroffsetProperties = commitErrorOffset(partition, readOffset, errorOffsetProperties, errorOffsetFile);
                        commitOffsets(commitErroroffsetProperties, errorOffsetFile);

                        logger.error("Error while parsing data into SyncLogEntry. Data: " + rawData, e);
                    }

                    offsetProperties.setProperty("" + partition, "" + readOffset);
                    commitOffsets(offsetProperties, syncOffsetFile);

                    // OffsetCommitResponse commitResponse =
                    // consumer.commitOffsets(offSetCommitRequest(clientId,
                    // groupId, currentOffset, topic, partition));
                    // if (commitResponse.hasError())
                    // {
                    // logger.info(commitResponse.toString());
                    // }
                }

            }

            logger.info("====================== " + clientId);

        }
        catch (Exception e)
        {
            logger.error("Shutting down consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + "]", e);
        }
        finally
        {
            if (consumer != null)
            {
                consumer.close();
            }

            logger.error("Shutting down consumer: [topic=" + topic + ", partition:" + partition + ", brokers:" + brokerList + "]");
        }
    }

    public long getLastOffset(SimpleConsumer consumer, String topic, int partition, long whichTime, String clientId)
    {
        TopicAndPartition topicAndPartition = new TopicAndPartition(topic, partition);
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo = new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(whichTime, 1));

        OffsetRequest request = new OffsetRequest(requestInfo, kafka.api.OffsetRequest.CurrentVersion(), clientId);
        OffsetResponse response = consumer.getOffsetsBefore(request);

        if (response.hasError())
        {
            logger.error("Error fetching data offset. " + ErrorMapping.exceptionFor(response.errorCode(topic, partition)));
            return 0;
        }

        long[] offsets = response.offsets(topic, partition);
        logger.info("Rechecking setting : " + offsets[0] );
        return offsets[0];

    }

    private String findNewLeader(List<Broker> brokers, String oldLeader, String topic, int partition) throws Exception
    {
        for (int i = 0; i < 3; i++)
        {
            boolean goToSleep = false;
            PartitionMetadata metadata = findLeader(brokers, topic, partition);
            if (metadata == null)
            {
                goToSleep = true;
            }
            else if (metadata.leader() == null)
            {
                goToSleep = true;
            }
            else if (oldLeader.equalsIgnoreCase(metadata.leader().host()) && i == 0)
            {
                // first time through if the leader hasn't changed give
                // ZooKeeper a second to recover
                // second time, assume the broker did recover before failover,
                // or it was a non-Broker issue
                //
                goToSleep = true;
            }
            else
            {
                return metadata.leader().host();
            }
            if (goToSleep)
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ie)
                {
                }
            }
        }
        logger.error("Unable to find new leader after Broker failure. Exiting");
        throw new Exception("Unable to find new leader after Broker failure. Exiting");
    }

    private PartitionMetadata findLeader(List<Broker> brokers, String topic, int partition)
    {
        PartitionMetadata metadata = null;

        loop:
        for (Broker broker : brokers)
        {
            SimpleConsumer consumer = null;

            try
            {
                consumer = new SimpleConsumer(broker.getHost(), Integer.parseInt(broker.getPort()), timeout, bufferSize, "leaderLookup");
                List<String> topics = Collections.singletonList(topic);
                TopicMetadataRequest topicMetadataRequest = new TopicMetadataRequest(topics);
                TopicMetadataResponse topicMetadataResponse = consumer.send(topicMetadataRequest);

                List<TopicMetadata> topicMetaDataList = topicMetadataResponse.topicsMetadata();

                for (TopicMetadata topicMetadata : topicMetaDataList)
                {
                    for (PartitionMetadata partitionMetadata : topicMetadata.partitionsMetadata())
                    {
                        if (partitionMetadata.partitionId() == partition)
                        {
                            metadata = partitionMetadata;
                            break loop;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.error("Error communicating with broker " + broker + " of [" + brokers + "].", e);
            }
            finally
            {
                if (consumer != null)
                {
                    consumer.close();
                }
            }
        }

        return metadata;
    }

    private OffsetCommitRequest offSetCommitRequest(String clientId, String groupId, long currentOffset, String topic, int partition)
    {
        TopicAndPartition topicAndPartition = new TopicAndPartition(topic, partition);
        Map<TopicAndPartition, OffsetAndMetadata> offsetAndMetaData = new HashMap<TopicAndPartition, OffsetAndMetadata>();
        offsetAndMetaData.put(topicAndPartition, new OffsetAndMetadata(currentOffset, "Consumed", System.currentTimeMillis()));
        OffsetCommitRequest offsetCommitRequest = new OffsetCommitRequest(groupId, offsetAndMetaData, 0, clientId, kafka.api.OffsetRequest.CurrentVersion());

        return offsetCommitRequest;
    }

    private boolean validateConsumerProperties(Properties p, TopicType type)
    {
        Validation.notNull(p, "Consumer properties should not be null.");
        StringBuilder errorMessage = new StringBuilder();
        String template = "Required property '%s' not set.";

        if (Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "brokers")))
        {
            errorMessage.append(String.format(template, "brokers")).append("\n");
        }
        this.brokerList = getBrokers(p.getProperty(type.toString() + "." + "brokers"));

        if (Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "groupId")))
        {
            errorMessage.append(String.format(template, "groupId")).append("\n");
        }
        this.groupId = p.getProperty(type.toString() + "." + "groupId");

        setDefaults();
        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "timeout")))
        {
            this.timeout = Integer.parseInt(p.getProperty(type.toString() + "." + "timeout"));
        }

        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "bufferSize")))
        {
            this.bufferSize = Integer.parseInt(p.getProperty(type.toString() + "." + "bufferSize"));
        }

        if (!Utils.isNullOrEmpty(p.getProperty(type.toString() + "." + "clientIdPrefix")))
        {
            this.clientPreFix = p.getProperty(type.toString() + "." + "clientIdPrefix");
        }

        return true;
    }

    private void setDefaults()
    {
        this.timeout = Constant.CONSUMER_CLIENT_TIMEOUT;
        this.bufferSize = Constant.CONSUMER_BUFFER_SIZE;
        this.clientPreFix = Constant.CONSUMER_CLIENT_PREFIX;
    }

    private List<Broker> getBrokers(String unParsed)
    {
        Set<Broker> brokerList = new HashSet<Broker>();

        String[] brokersRaw = unParsed.split(",");

        if (brokersRaw.length == 0)
        {
            throw new IllegalArgumentException("Required parameter 'brokers' not set correctly. Format: 'localhost:2181,10.40.23.21:2998'");
        }

        for (String broker : brokersRaw)
        {
            String[] brokerDetails = broker.split(":");

            if (brokerDetails.length < 2 || brokerDetails.length > 2)
            {
                throw new IllegalArgumentException("Required parameter 'brokers' not set correctly. Format: 'localhost:2181,10.40.23.21:2998'");
            }

            brokerList.add(new Broker(brokerDetails[0], brokerDetails[1]));
        }

        return new ArrayList<Broker>(brokerList);
    }

    private void commitOffsets(Properties offsetProperties, File offsetFile) throws Exception
    {
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(offsetFile);
            offsetProperties.store(fos, new Date().toString());
        }
        catch (Exception e)
        {
            logger.error("Error while persisting offsets [" + offsetProperties + "]: " + e.getMessage());
        }
        finally
        {
            Utils.close(fos);
        }
    }

    private Properties commitErrorOffset(int partition, Long readOffset, Properties errorOffsetProperties, File errorOffsetFile)
    {
        String value = null;
        String errorOffsetPartition = String.valueOf(partition);
        String errorOffsetreadOffset = String.valueOf(readOffset);

        if (errorOffsetProperties.containsKey(errorOffsetPartition))
        {
            value = errorOffsetProperties.getProperty(errorOffsetPartition);
            errorOffsetProperties.setProperty(errorOffsetPartition, value + "," + errorOffsetreadOffset);
        }
        else
        {
            errorOffsetProperties.setProperty(errorOffsetPartition, errorOffsetreadOffset);
        }
        return errorOffsetProperties;
    }

    public long getBufferSize()
    {
        return bufferSize;
    }

    public String getClientId()
    {
        return clientId;
    }

    public String getClientPreFix()
    {
        return clientPreFix;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public Properties getOffsetProperties()
    {
        return offsetProperties;
    }

    public int getPartition()
    {
        return partition;
    }

    public long getTimeout()
    {
        return timeout;
    }

    public String getTopic()
    {
        return topic;
    }

    public List<Broker> getBrokerList()
    {
        return this.brokerList;
    }

    public SyncHandler getSyncHandler()
    {
        return syncHandler;
    }

    public void setSyncHandler(SyncHandler syncHandler)
    {
        this.syncHandler = syncHandler;
    }

    public Properties getErrorOffsetProperties()
    {
        return errorOffsetProperties;
    }

    public void setErrorOffsetProperties(Properties errorOffsetProperties)
    {
        this.errorOffsetProperties = errorOffsetProperties;
    }

    class Broker
    {
        private String host;

        private String port;

        public Broker(String host, String port)
        {
            Validation.isTrue(!Utils.isNullOrEmpty(host), "broker host should not be null or empty.");
            this.host = host;
            this.port = port;
        }

        public String getHost()
        {
            return host;
        }

        public String getPort()
        {
            return port;
        }

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((host == null) ? 0 : host.hashCode());
            result = prime * result + ((port == null) ? 0 : port.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Broker other = (Broker) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (host == null)
            {
                if (other.host != null)
                    return false;
            }
            else if (!host.equals(other.host))
                return false;
            if (port == null)
            {
                if (other.port != null)
                    return false;
            }
            else if (!port.equals(other.port))
                return false;
            return true;
        }

        private OneGroupConsumer getOuterType()
        {
            return OneGroupConsumer.this;
        }

    }

    @Override
    public String toString()
    {
        return "OneGroupConsumer [syncHandler=" + syncHandler + ", offsetProperties=" + offsetProperties + ", partition=" + partition + ", clientPreFix=" + clientPreFix + ", groupId=" + groupId
                + ", brokerList=" + brokerList + ", timeout=" + timeout + ", bufferSize=" + bufferSize + ", topic=" + topic + ", topicType=" + topicType + ", clientId=" + clientId
                + ", syncOffsetFile=" + syncOffsetFile + "]";
    }

    public static void main(String[] args) throws Exception
    {
        // BasicConfigurator.configure();
        // int timeout = 100000;
        // int bufferSize = 64 * 1024;
        // String broker = "localhost";
        // List<String> brokerList = new ArrayList<String>();
        // brokerList.add(broker);
        // String topic = "MoveIn-Topic";
        // int port = 9092;
        // int partition = 0;
        // String clientName = "Client_" + topic + "_" + partition;
        //
        // MoveInConsumer consumer = new MoveInConsumer();
        // SimpleConsumer simpleConsumer = consumer.getInitialConsumer(broker,
        // port, timeout, bufferSize, clientName);
        // PartitionMetadata partitionMetaData = consumer.findLeader(brokerList,
        // topic, partition, port);
        // System.out.println(partitionMetaData);
        // long lastOffset = consumer.getLastOffset(simpleConsumer, topic,
        // partition, System.currentTimeMillis(), clientName);
        // // System.out.println(consumer.findNewLeader(brokerList, broker,
        // topic,
        // // partition, port));
        //
        // ZKGroupDirs groupDirs = new ZKGroupDirs("Group");
        // System.out.println(groupDirs.consumerDir());
        // System.out.println(groupDirs.consumerGroupDir());
        // System.out.println(groupDirs.consumerRegistryDir());
        // System.out.println(groupDirs.group());

        // Properties p = new Properties();
        // p.setProperty("zookeeper.connect", "localhost:2181");
        // p.setProperty("group.id", "Group");
        // ZookeeperConsumerConnector c = new ZookeeperConsumerConnector(new
        // ConsumerConfig(p));
        // Map<String, Object> topicCountMap = new HashMap<String, Object>();
        // topicCountMap.put(topic, 0);
        // TopicCount topicCount = new StaticTopicCount(clientName,
        // JavaConversions.mapAsScalaMap(topicCountMap));
        // c.kafka$consumer$ZookeeperConsumerConnector$$registerConsumerInZK(groupDirs,
        // clientName, topicCount);

        // consumer.readData(brokerList, topic, broker, port, partition,
        // timeout, bufferSize, System.currentTimeMillis(), clientName);

        // c.kafka$consumer$ZookeeperConsumerConnector$$reinitializeConsumer(topicCount,
        // queuesAndStreams)

        // Properties consumerProperties = new Properties();
        // consumerProperties.load(new
        // FileInputStream("../configuration/movein/kafka.consumer.properties"));
        // System.out.println(consumerProperties);
        //
        // Properties offsetProperties = new Properties();
        // offsetProperties.setProperty("key", "value");
        //
        // BasicConfigurator.configure();
        // MoveInConsumer consumer = new MoveInConsumer(0, offsetProperties,
        // consumerProperties);
        // consumer.readData();

    }
}
