package one.group.sync;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author nyalfernandes
 * 
 */
public enum SyncProperty
{
    SYNC_DATA_FILE("syncDataLocation", true),
    CLEANUP("cleanUp", false),

    ;

    private String name;
    private boolean required;

    private static List<String> requiredProperties = new ArrayList<String>();

    static
    {
        for (SyncProperty s : values())
        {
            if (s.required)
            {
                requiredProperties.add(s.getName());
            }
        }
    }

    private SyncProperty(String name, boolean required)
    {
        this.name = name;
        this.required = required;
    }

    public String getName()
    {
        return this.name;
    }

    public boolean isRequired()
    {
        return this.required;
    }

    public static List<String> getRequiredProperties()
    {
        return requiredProperties;
    }

}
