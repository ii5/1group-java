package one.group.solr;

import one.group.core.enums.MessageType;
import one.group.entities.socket.AbstractSolrEntity;

import org.apache.solr.client.solrj.beans.Field;

public class TestEntity extends AbstractSolrEntity
{
    @Field
    private String description;
    
    
    private String type_s;
    
    private MessageType type;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getType_s()
    {
        return type_s;
    }
    
    @Field
    public void setType_s(String type)
    {
        this.type_s = type;
        this.type = MessageType.parse(type);
    }
    
    public void setType(MessageType type)
    {
        this.type = type;
        this.type_s = type.toString();
    }
    
    public MessageType getType()
    {
        return type;
    }

    @Override
    public String toString()
    {
        return "TestEntity [description=" + description + ", type_s=" + type_s + ", type=" + type + ", getEntityType()=" + getEntityType() + ", getId()=" + getId() + "]";
    }

    
    
    
    
}
