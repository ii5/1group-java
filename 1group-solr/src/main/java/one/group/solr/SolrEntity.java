package one.group.solr;


/**
 * 
 * @author nyalfernandes
 *
 */
public interface SolrEntity
{   
    public String getId();
}
