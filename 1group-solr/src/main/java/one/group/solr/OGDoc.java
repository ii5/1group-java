package one.group.solr;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;

/**
 * 
 * @author nyalfernandes
 *
 */
public class OGDoc
{
    @Field
    private Long id;
    
    @Field
    private String description;
    
    @Field
    private Date createdTime;
    
    public OGDoc()
    {
        
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }
    
    
    
}
