package one.group.solr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import one.group.core.enums.SolrCollectionType;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SolrServerFactory
{
    private Map<SolrCollectionType, SolrClient> solrCollectionVsSolrClientMap = new ConcurrentHashMap<SolrCollectionType, SolrClient>();
    private Map<SolrCollectionType, SolrClient> solrCollectionVsSolrQueryClientMap = new ConcurrentHashMap<SolrCollectionType, SolrClient>();
    private static final SolrServerFactory INSTANCE = new SolrServerFactory();
    private static final String SOLR_BASE_URL = getSolrUrl();
    private static int queueSize = 100;
    private static int threadCount = 1;
    
    

    public static int getQueueSize() 
    {
		return queueSize;
	}

	public static void setQueueSize(int queueSize) 
	{
		SolrServerFactory.queueSize = queueSize;
	}

	public static int getThreadCount() 
	{
		return threadCount;
	}

	public static void setThreadCount(int threadCount) 
	{
		SolrServerFactory.threadCount = threadCount;
	}
	
	public void reinitialiseSolrFactoryMap()
	{
		solrCollectionVsSolrClientMap = new ConcurrentHashMap<SolrCollectionType, SolrClient>();
	}

	private static String getSolrUrl()
    {
        Properties properties = new Properties();
        String SOLR_BASE_URL = "";
        try
        {
            properties.load(new FileInputStream(new File("cache.properties")));
            SOLR_BASE_URL = properties.get("solrBaseUrl").toString();
            queueSize = Integer.parseInt(properties.getProperty("queueSize", "100"));
            threadCount = Integer.parseInt(properties.getProperty("threadCount", "1"));
            
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
        return SOLR_BASE_URL;
    }

    private SolrServerFactory()
    {

    }

    public static SolrServerFactory getInstance()
    {
        System.out.println("SOLR_BASE_URL>>" + SOLR_BASE_URL);
        return INSTANCE;
    }

    public SolrClient getUpdateClient(SolrCollectionType collectionType)
    {
        if (solrCollectionVsSolrClientMap.containsKey(collectionType))
        {
            return solrCollectionVsSolrClientMap.get(collectionType);
        }

        // HttpSolrClient solrClient = new HttpSolrClient(SOLR_BASE_URL +
        // collectionType.toString());
        ConcurrentUpdateSolrClient solrClient = new ConcurrentUpdateSolrClient(SOLR_BASE_URL + collectionType.toString(), queueSize, threadCount);
        solrCollectionVsSolrClientMap.put(collectionType, solrClient);
        configureSolr(solrClient);
        return solrClient;
    }
    
    public SolrClient getQueryClient(SolrCollectionType collectionType)
    {
        if (solrCollectionVsSolrQueryClientMap.containsKey(collectionType))
        {
            return solrCollectionVsSolrQueryClientMap.get(collectionType);
        }

        HttpSolrClient solrClient = new HttpSolrClient(SOLR_BASE_URL + collectionType.toString());
        solrCollectionVsSolrQueryClientMap.put(collectionType, solrClient);
        configureSolr(solrClient);
        return solrClient;
    }

    private void configureSolr(HttpSolrClient client)
    {
        client.setConnectionTimeout(20000);
        client.setSoTimeout(10000);
        client.setDefaultMaxConnectionsPerHost(200);
        client.setMaxTotalConnections(500);
        client.setFollowRedirects(false);
        client.setAllowCompression(true);
        // client.setRequestWriter(requestWriter)setParser(processor)
    }

    private void configureSolr(ConcurrentUpdateSolrClient client)
    {
        client.setConnectionTimeout(20000);
        client.setSoTimeout(20000);
        // client.s
        // client.setDefaultMaxConnectionsPerHost(500);
        // client.setMaxTotalConnections(500);
        // client.setFollowRedirects(false);
        // client.setAllowCompression(false);
        // client.setRequestWriter(requestWriter)setParser(processor)
    }

}
