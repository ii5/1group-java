package one.group.solr;

import org.apache.solr.client.solrj.beans.Field;
import org.apache.solr.common.SolrInputDocument;

/**
 * 
 * @author nyalfernandes
 *
 */
public abstract class AbstractSolrEntity implements SolrEntity
{
    @Field
    private String id;
    
    @Field
    private String entityType;
    
    public AbstractSolrEntity()
    {
        this.entityType = this.getClass().getSimpleName().toLowerCase();
    }
    
    public String getEntityType()
    {
        return entityType;
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
}
