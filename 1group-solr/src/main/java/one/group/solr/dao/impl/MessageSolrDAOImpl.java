package one.group.solr.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.core.enums.SolrCollectionType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.socket.Message;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.solr.dao.MessageSolrDAO;
import one.group.utils.Utils;

import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.MapSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageSolrDAOImpl extends AbstractSolrDaoImpl<Message> implements MessageSolrDAO
{

    private static final Logger logger = LoggerFactory.getLogger(MessageSolrDAOImpl.class);

    @Override
    public SolrCollectionType getSolrCollectionType()
    {
        return SolrCollectionType.ONEGROUP;
    }

    public void saveMessage(Message message)
    {
        persist(message);
    }

    public List<Message> fetchMessagesByStatusAndGroupIds(List<MessageStatus> messageStatusList, List<GroupStatus> groupStatusList, List<String> groupIdList,
            List<BpoMessageStatus> bpoMessageStatusList, String bpoMessageStatusUpdateTime, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows,
            List<String> fields)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = "entityType:message AND ";

        if (groupStatusList != null && !groupStatusList.isEmpty())
        {
            String groupStatusListString = "status_s:(" + Utils.getCollectionAsStringForEnum(groupStatusList, " ") + ")";
            q = q + groupStatusListString + " AND ";
        }

        if (groupIdList != null && !groupIdList.isEmpty())
        {
            String groupIdListString = "groupId:(" + Utils.getCollectionAsString(groupIdList, " ") + ")";
            q = q + groupIdListString + " AND ";
        }

        if (messageStatusList != null && !messageStatusList.isEmpty())
        {
            String messageStatusListString = "messageStatus_s:(" + Utils.getCollectionAsStringForEnum(messageStatusList, " ") + ")";
            q = q + messageStatusListString + "  AND ";
        }

        if (bpoMessageStatusList != null && !bpoMessageStatusList.isEmpty())
        {
            String bpoMessageStatusListString = "bpoMessageStatus_s:(" + Utils.getCollectionAsStringForEnum(bpoMessageStatusList, " ") + ")";
            q = q + bpoMessageStatusListString + "  AND ";
        }

        if (bpoMessageStatusUpdateTime != null && !bpoMessageStatusUpdateTime.isEmpty())
        {
            Date date = new Date(Long.valueOf(bpoMessageStatusUpdateTime));
            SimpleDateFormat out = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            String dateString = out.format(date);
            String bpoMessageUpdateTimeString = " ( (-bpoStatusUpdateTime:[* TO *]) OR bpoStatusUpdateTime:[ * TO " + dateString + "] )";
            q = q + bpoMessageUpdateTimeString + " AND ";
        }

        q = q + "isDuplicate:" + false;

        params.put("q", q);
        params.put("start", start + "");
        params.put("rows", rows + "");

        if (fields != null && !fields.isEmpty())
        {
            params.put("fl", Utils.getCollectionAsString(fields, ","));
        }

        if (sort != null && !sort.isEmpty())
        {
            params.put("sort", Utils.sortMapToSolrString(sort));
        }

        if (filter != null && !filter.isEmpty())
        {
            params.put("fq", Utils.filterMapToSolrString(filter));
        }

        logger.info(params + "");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params), METHOD.POST);
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return messageList;
    }

    public Map<String, Map<String, Object>> fetchLatestMessageBasedOnGroupQuery(Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, boolean useGroup, SortField groupSortField,
            SortType groupSortType, String groupField, List<String> fields, int start, int rows)
    {
        Map<String, String> params = new HashMap<String, String>();
        Map<String, Map<String, Object>> idVsFieldMap = new HashMap<String, Map<String, Object>>();

        String q = "entityType:message";

        if (sort != null && !sort.isEmpty())
        {
            params.put("sort", Utils.sortMapToSolrString(sort));
        }

        if (filter != null && !filter.isEmpty())
        {
            params.put("fq", Utils.filterMapToSolrString(filter));
        }

        if (fields != null && !fields.isEmpty())
        {
            params.put("fl", Utils.getCollectionAsString(fields, ","));
        }

        if (useGroup)
        {
            params.put("group", "true");
            params.put("group.field", groupField);
            params.put("group.sort", groupSortField.solrField() + " " + groupSortType.toString());
            params.put("group.limit", "1");
        }

        params.put("q", q);
        params.put("start", start + "");
        params.put("rows", rows + "");
        // logger.info(params + "");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params), METHOD.POST);
            GroupResponse groupResponse = response.getGroupResponse();

            for (GroupCommand command : groupResponse.getValues())
            {
                for (Group group : command.getValues())
                {
                    for (SolrDocument solrDoc : group.getResult())
                    {
                        solrDoc.addField("numFound", group.getResult().getNumFound());
                        idVsFieldMap.put(solrDoc.get("groupId").toString(), solrDoc.getFieldValueMap());
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error while fetchLatestMessageBasedOnGroupQuery, " + params, e);
        }

        return idVsFieldMap;
    }

    public List<String> fetchBroadcastIdsByMessageCreator(Set<String> mobileNumber)
    {
        List<String> broadcastIdList = new ArrayList<String>();
        Map<String, String> params = new HashMap<String, String>();
        String q = "entityType:message AND broadcastId:[* TO *]";
        if (mobileNumber == null || mobileNumber.isEmpty())
        {
            return broadcastIdList;
        }

        if (mobileNumber != null && !mobileNumber.isEmpty())
        {
            q = q + " AND senderPhoneNumber:(" + Utils.getCollectionAsString(mobileNumber, " ") + ")";
        }

        params.put("q", q);
        params.put("fl", "broadcastId");
        params.put("rows", Integer.MAX_VALUE + "");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            SolrDocumentList docList = response.getResults();

            if (docList != null)
            {
                for (SolrDocument d : docList)
                {
                    String broadcastId = d.get("broadcastId").toString();

                    if (!broadcastIdList.contains(broadcastId))
                    {
                        broadcastIdList.add(broadcastId);
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return broadcastIdList;
    }

    public List<Message> fetchPhonecallTypeMessagesOfAccount(String accountId)

    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = "entityType:message AND messageType_s:" + MessageType.PHONE_CALL.name() + " AND (createdBy:" + accountId + " OR toAccountId:" + accountId + ")";
        params.put("q", q);

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return messageList;
    }

    public List<Message> fetchLastMessageOfGroup(String groupId)
    {

        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = "entityType:message AND groupId:" + groupId + "";
        params.put("q", q);
        params.put("sort", "createdTime desc");
        params.put("rows", "1");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchMessagesBySearchCriteria()
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = "entityType:message";
        List<String> messageSourceList = new ArrayList<String>();
        messageSourceList.add(MessageSourceType.DIRECT.name());

        if (messageSourceList != null && !messageSourceList.isEmpty())
        {
            String messageSourceQuery = "messageSource_s:(" + Utils.getCollectionAsString(messageSourceList, " ") + ")";
            q = messageSourceQuery;
        }

        params.put("q", q);

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return messageList;
    }

    public Message fetchMessageFromId(String messageId)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();
        Message message = new Message(); // TODO
        String q = "id:" + messageId;

        params.put("q", q);
        params.put("rows", "1");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
            if (!messageList.isEmpty())
            {
                message = messageList.get(0);
                return message;
            }
            return null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return message;
    }

    public List<Message> fetchMessageByBroadcastIds(List<String> broadcastIdsList)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        List<Message> MessageListById = new ArrayList<Message>();
        for (String broadcastId : broadcastIdsList)
        {
            String q = "entityType:message AND messageStatus_s:" + MessageStatus.ACTIVE.name() + " AND broadcastId:" + broadcastId + "";
            params.put("q", q);
            params.put("sort", "updatedTime desc");
            params.put("rows", "1");
            try
            {
                QueryResponse response = getClient().query(new MapSolrParams(params));
                messageList = response.getBeans(Message.class);

                if (!messageList.isEmpty())
                {
                    MessageListById.add(messageList.get(0));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return MessageListById;
    }

    public List<Message> fetchMessagesOfBroadcast(String accountId, String broadcastId)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();
        String q = "entityType:message AND messageType_s:" + MessageType.BROADCAST.name() + " AND (createdBy:" + accountId + ") AND text:" + broadcastId + " AND messageSource_s:"
                + MessageSourceType.DIRECT.name();
        params.put("q", q);
        params.put("fq", "{!collapse field=messageSource_s}");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return messageList;
    }

    public List<Message> fetchSharedMessagesByType(String accountId, String groupId)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();
        List<MessageType> messageTypeList = new ArrayList<MessageType>();
        messageTypeList.add(MessageType.BROADCAST);
        messageTypeList.add(MessageType.PHOTO);
        String q = "entityType:message AND (messageType_s: " + MessageType.BROADCAST.name() + " OR messageType_s: " + MessageType.PHOTO.name() + ") AND (createdBy:" + accountId + ") AND groupId:"
                + groupId;
        params.put("q", q);
        // params.put("fq", "{!collapse field=groupId}");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND createdBy:" + accountId + " AND messageSource_s:" + messageSourceType.name();

        q = messageSourceQuery;
        params.put("q", q);
        params.put("sort", "updatedTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchMessagesByGroupIdAndSourceAndType(String groupId, MessageSourceType messageSourceType, List<MessageType> messageTypeList, int offset, int limit)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND groupId:" + groupId + " AND messageSource_s:" + messageSourceType.name();
        q = messageSourceQuery;
        params.put("q", q);
        params.put("start", offset + "");
        params.put("rows", limit + "");
        params.put("sort", "createdTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchMessageByBroadcastIdsAndStatusAndSource(List<String> broadcastIdsList, List<MessageStatus> messageStatusList, List<MessageSourceType> messageSourceTypeList,
            MessageType messageType)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        if (broadcastIdsList == null || broadcastIdsList.isEmpty())
        {
            return messageList;
        }

        String q = "entityType:message";

        String broadcastIdsListString = " broadcastId:(" + Utils.getCollectionAsString(broadcastIdsList, " OR ") + ") AND messageStatus_s:("
                + Utils.getCollectionAsStringForEnum(messageStatusList, " OR ") + " ) " + " AND messageSource_s:(" + Utils.getCollectionAsStringForEnum(messageSourceTypeList, " OR ") + " ) ";
        q += " AND messageType_s:" + messageType.name();
        q += " AND " + broadcastIdsListString;

        // q = q + "isDuplicate:" + false;
        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");
        params.put("sort", "updatedTime asc, createdTime asc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<String> fetchMessageByBroadcastIdsAndSourceAndAccount(List<String> accountIdList, List<String> broadcastIdsList, MessageSourceType messageSourceType, MessageStatus messageStatus)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<String> messageList = new ArrayList<String>();

        String q = " entityType:message AND broadcastId:(" + Utils.getCollectionAsString(broadcastIdsList, " OR ") + ") AND createdBy:(" + Utils.getCollectionAsString(accountIdList, " OR ") + " ) "
                + " AND messageSource_s:(" + messageSourceType.name() + ") AND messageStatus_s:(" + messageStatus.name() + ")";
        params.put("q", q);
        params.put("group", "true");
        params.put("group.field", "groupId");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            GroupResponse gResponse = response.getGroupResponse();
            for (GroupCommand c : gResponse.getValues())
            {
                for (Group solrGroup : c.getValues())
                {
                    for (SolrDocument solrDoc : solrGroup.getResult())
                    {
                        messageList.add(solrDoc.get("groupId").toString());
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchMessagesFromIds(List<String> messageIds, List<String> fields)
    {
        if (messageIds == null || messageIds.isEmpty())
        {
            return new ArrayList<Message>();
        }

        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();
        String messageIdsListString = "id:(" + Utils.getCollectionAsString(messageIds, " ") + ")";
        String q = "entityType:message AND " + messageIdsListString;

        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");

        if (fields != null && !fields.isEmpty())
        {
            params.put("fl", Utils.getCollectionAsString(fields, ","));
        }

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params), METHOD.POST);
            messageList = response.getBeans(Message.class);

        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
        return messageList;
    }
    
	public List<Message> fetchMessagesFromIdsWithException(List<String> messageIds, List<String> fields) throws Abstract1GroupException
    {
        if (messageIds == null || messageIds.isEmpty())
        {
            return new ArrayList<Message>();
        }

        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();
        String messageIdsListString = "id:(" + Utils.getCollectionAsString(messageIds, " ") + ")";
        String q = "entityType:message AND " + messageIdsListString;

        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");

        if (fields != null && !fields.isEmpty())
        {
            params.put("fl", Utils.getCollectionAsString(fields, ","));
        }

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params), METHOD.POST);
            messageList = response.getBeans(Message.class);

        }
        catch (Exception e)
        {
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return messageList;
    }

    public List<Message> fetchMessagesByAccountIdAndMessageSourceType(String accountId, MessageSourceType messageSourceType, int offset, int limit)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND createdBy:" + accountId + " AND messageSource_s:" + messageSourceType.name();

        q = messageSourceQuery;
        params.put("q", q);
        params.put("start", offset + "");
        params.put("rows", limit + "");
        params.put("sort", "updatedTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public boolean updateMessageStatus(List<String> messageIds, MessageStatus messageStatus, BpoMessageStatus bpoMessageStatus, String updatedBy, String bpoUpdatedBy)
    {
        boolean statusUpdated = false;

        if (messageIds != null && messageIds.isEmpty())
        {
            return true;
        }

        try
        {
            List<Message> messages = new ArrayList<Message>();

            for (String messageId : messageIds)
            {
                Message message = new Message();
                message.setId(messageId);

                if (messageStatus != null)
                {
                    message.setMessageStatus(messageStatus);
                    message.setUpdatedBy(updatedBy);
                    message.setUpdatedTime(new Date());
                }
                if (bpoMessageStatus != null)
                {
                    message.setBpoMessageStatus(bpoMessageStatus);
                    message.setBpoStatusUpdateTime(new Date());
                    message.setBpoStatusUpdateBy(bpoUpdatedBy);
                }

                if (messageStatus != null || bpoMessageStatus != null)
                {
                    messages.add(message);
                    statusUpdated = true;
                }
            }

            if (!messages.isEmpty())
                update(messages);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return statusUpdated;
    }

    public List<Message> fetchAllMessagesByAccountIdAndMessageSourceTypeList(String accountId, List<MessageType> messageTypeList, List<MessageSourceType> messageSourceTypeList, boolean latestToOldest)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND createdBy:" + accountId + " AND messageSource_s:(" + Utils.getCollectionAsStringForEnum(messageSourceTypeList, " OR ") + ")"
                + " AND messageType_s:(" + Utils.getCollectionAsStringForEnum(messageTypeList, " OR ") + ")";
        ;
        q = messageSourceQuery;
        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");
        System.out.println("query=" + q);
        if (latestToOldest)
        {
            params.put("sort", "createdTime desc");
        }
        else
        {
            params.put("sort", "createdTime asc");
        }
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public void updateMessageStatusBybroadcastId(String broadcastId, BroadcastStatus broadcastStatus, String accountId)
    {

        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND broadcastId:" + broadcastId;
        q = messageSourceQuery;
        params.put("q", q);
        params.put("fl", "id");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
            List<Message> messageUpdateList = new ArrayList<Message>();
            for (Message message : messageList)
            {
                if (broadcastStatus.equals(BroadcastStatus.CLOSED))
                {
                    message.setMessageStatus(MessageStatus.CLOSED);
                    message.setUpdatedTime(new Date());
                    message.setUpdatedBy(accountId);
                }
                else if (broadcastStatus.equals(BroadcastStatus.RENEW))
                {
                    message.setUpdatedTime(new Date());
                    message.setUpdatedBy(accountId);
                }

                messageUpdateList.add(message);

            }

            update(messageUpdateList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public long fetchUnreadMessageCount(String groupId)
    {
        Map<String, String> params = new HashMap<String, String>();

        String q = "entityType:message AND bpoMessageStatus_s:UNREAD AND groupId:" + groupId + " and facet=true";
        params.put("q", q);
        params.put("rows", "0");
        long totalUnreadMessageCount = 0;
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            totalUnreadMessageCount = response.getResults().getNumFound();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return totalUnreadMessageCount;
    }

    public long fetchTotalMessageCount(String groupId)
    {
        Map<String, String> params = new HashMap<String, String>();

        String q = "entityType:message AND groupId:" + groupId + " and facet=true";
        params.put("q", q);
        params.put("rows", "0");
        long totalMessageCount = 0;
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            totalMessageCount = response.getResults().getNumFound();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return totalMessageCount;
    }

    public List<Message> fetchMessagesByBroadcastIdAndMessageStatus(String broadcastId, MessageStatus messageStatus)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND broadcastId:" + broadcastId + " AND messageStatus_s:" + messageStatus.name();
        q = messageSourceQuery;
        params.put("q", q);
        params.put("sort", "createdTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public void saveMessages(Collection<Message> messageList)
    {

        if (messageList != null && !messageList.isEmpty())
        {
            persistAllWithoutCommit(messageList);
        }
    }

    public static void main(String[] args)
    {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat out = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String dateString = out.format(date);
        System.out.println(dateString);

    }

    public List<Message> fetchMessagesWithEmptyGroupId()
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND groupId:\"\"";

        q = messageSourceQuery;
        params.put("q", q);
        params.put("rows", "20000");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public long countAllMessagesByAccountIdForMeAndWhatsApp(String accountId)
    {

        Map<String, String> params = new HashMap<String, String>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND createdBy:" + accountId + " AND (messageSource_s:" + MessageSourceType.ME.name() + " OR (messageSource_s:"
                + MessageSourceType.WHATSAPP.name() + " AND broadcastId:['' TO *]))";

        q = messageSourceQuery;

        params.put("q", q);
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            return response.getResults().getNumFound();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0l;
    }

    public List<Message> fetchMessagesByForMeAndWhatsappByAccountId(String accountId, int offset, int limit)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " ";
        String messageSourceQuery = "entityType:message AND createdBy:" + accountId + " AND (messageSource_s:" + MessageSourceType.ME.name() + " OR (messageSource_s:"
                + MessageSourceType.WHATSAPP.name() + " AND broadcastId:['' TO *]))";

        q = messageSourceQuery;
        params.put("q", q);
        params.put("start", offset + "");
        params.put("rows", limit + "");
        params.put("sort", "updatedTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public List<Message> fetchLastMessageOfGroupNotTypePhonecall(String groupId)
    {

        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " entityType:message AND groupId:" + groupId + " AND -(messageType_s:" + MessageType.PHONE_CALL.name() + ") ";
        params.put("q", q);
        params.put("sort", "createdTime desc");
        params.put("rows", "1");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }

    public Map<String, Long> fetchBroadcastMessageCountByAccountId(List<String> accountIdList)
    {
        Map<String, Long> response = new HashMap<String, Long>();

        if (accountIdList == null || accountIdList.isEmpty())
        {
            return response;
        }

        Map<String, String> params = new HashMap<String, String>();
        String q = "entityType:message AND messageType_s:" + MessageType.BROADCAST.name() + " AND messageStatus_s:" + MessageStatus.ACTIVE.name() + " AND createdBy:("
                + Utils.getCollectionAsString(accountIdList, " OR ") + ")";
        params.put("q", q);
        params.put("group", "true");
        params.put("group.field", "createdBy");

        try
        {
            QueryResponse queryResponse = getClient().query(new MapSolrParams(params));
            GroupResponse gResponse = queryResponse.getGroupResponse();

            for (GroupCommand gc : gResponse.getValues())
            {
                for (Group g : gc.getValues())
                {
                    response.put(g.getGroupValue(), g.getResult().getNumFound());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return response;
    }

    public List<Message> fetchMessageByBroadcastIdAndSourceAndStatus(Collection<String> broadcastIdList, List<MessageSourceType> messageSourceList, List<MessageStatus> messageStatusList)
    {
        Map<String, String> params = new HashMap<String, String>();
        List<Message> messageList = new ArrayList<Message>();

        String q = " entityType:message AND broadcastId:(" + Utils.getCollectionAsString(broadcastIdList, " OR ") + ") AND messageSource_s:("
                + Utils.getCollectionAsStringForEnum(messageSourceList, " OR ") + " ) AND messageStatus_s:(" + Utils.getCollectionAsStringForEnum(messageStatusList, " OR ") + ")";
        params.put("q", q);
        params.put("sort", "updatedTime desc");
        params.put("rows", Integer.MAX_VALUE + "");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            messageList = response.getBeans(Message.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return messageList;
    }
}
