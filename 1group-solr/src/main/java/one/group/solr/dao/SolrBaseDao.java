package one.group.solr.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.common.SolrInputDocument;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface SolrBaseDao<K, E>
{
    public Collection<E> findAll();

    public Collection<E> findAllFromTo(E entity, int offset, int rows);

    public E findById(String id);

    public List<E> findByIds(List<String> ids);

    public void persist(E entity);

    public void persistAll(Collection<E> entities);

    public void remove(E entity);

    public void removeById(String id);

    public E findBeanByQuery(String query);

    public List<E> findBeansByQuery(String query);

    public E findBeanByQuery(Map<String, String> params);

    public List<E> findBeansByQuery(Map<String, String> params);

    public SolrClient getClient();

    public void partialUpdate(SolrInputDocument solrInputDocument);
    
    public void partialUpdate(List<SolrInputDocument> solrInputDocument);
    
    public void update(List<E> solrBeans);
}
