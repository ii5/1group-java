package one.group.solr.dao.impl;

import one.group.core.enums.SolrCollectionType;
import one.group.solr.TestEntity;

/**
 * 
 * @author nyalfernandes
 *
 */
public class TestDAOImpl extends AbstractSolrDaoImpl<TestEntity>
{

    @Override
    public SolrCollectionType getSolrCollectionType()
    {
        return SolrCollectionType.GETTINGSTARTED;
    }
    
}
