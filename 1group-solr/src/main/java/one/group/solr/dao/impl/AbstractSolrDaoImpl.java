package one.group.solr.dao.impl;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import one.group.core.enums.SolrCollectionType;
import one.group.entities.socket.ISolrEntity;
import one.group.solr.SolrServerFactory;
import one.group.solr.dao.SolrBaseDao;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.SolrParams;

public abstract class AbstractSolrDaoImpl<E extends ISolrEntity> implements SolrBaseDao<String, E>
{

    private Class<E> entityClass;
    private SolrClient updateClient;
    private SolrClient queryClient;

    public AbstractSolrDaoImpl()
    {
        ParameterizedType genericSuperClass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperClass.getActualTypeArguments()[0];
        this.updateClient = SolrServerFactory.getInstance().getUpdateClient(getSolrCollectionType());
        this.queryClient = SolrServerFactory.getInstance().getQueryClient(getSolrCollectionType());
    }

    public Collection<E> findAll()
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("q", "entityType:" + entityClass.getSimpleName().toLowerCase());
        map.put("rows", Integer.MAX_VALUE + "");
        SolrParams params = new MapSolrParams(map);

        return getBeans(params);
    }

    public Collection<E> findAllFromTo(E entity, int offset, int rows)
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("q", "entityType:" + entityClass.getSimpleName().toLowerCase());
        map.put("start", offset + "");
        map.put("rows", rows + "");
        SolrParams params = new MapSolrParams(map);
        return getBeans(params);
    }

    public E findById(String id)
    {
        try
        {
            SolrDocument document = updateClient.getById("id:" + id);
            return queryClient.getBinder().getBean(entityClass, document);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public List<E> findByIds(List<String> ids)
    {
        try
        {
            SolrDocumentList documentList = updateClient.getById(ids);
            return queryClient.getBinder().getBeans(entityClass, documentList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void persist(E entity)
    {
        try
        {

            updateClient.addBean(entity, 100);
            // updateClient.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void persistAll(Collection<E> entities)
    {

        try
        {

            Iterator<E> iterator = entities.iterator();
            int cursor = 0;

            while (iterator.hasNext())
            {
                E entity = iterator.next();
                if (entity != null)
                {
                    updateClient.addBean(entity, 100);

                    if (cursor % 100 == 0 || !iterator.hasNext())
                    {
                        System.out.println(cursor);
                        updateClient.commit(false, false, false);
                    }
                    cursor++;
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void persistAllWithoutCommit(Collection<E> entities)
    {

        try
        {

            Iterator<E> iterator = entities.iterator();
            int cursor = 0;

            while (iterator.hasNext())
            {
                E entity = iterator.next();
                if (entity != null)
                {
                    updateClient.addBean(entity, 100);

                    if (cursor % 100 == 0 || !iterator.hasNext())
                    {
                        // System.out.println(cursor);
                        // updateClient.commit(false, false, false);
                    }
                    cursor++;
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void remove(E entity)
    {
        removeById(entity.getId());
    }

    public void removeById(String id)
    {
        try
        {
            updateClient.deleteById("" + id);
            // updateClient.commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public E findBeanByQuery(Map<String, String> params)
    {
        params = injectEntityType(params);
        SolrParams solrParams = new MapSolrParams(params);

        return getBean(solrParams);
    }

    public E findBeanByQuery(String query)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", query);
        params = injectEntityType(params);

        SolrParams solrParams = new MapSolrParams(params);

        return getBean(solrParams);
    }

    public List<E> findBeansByQuery(Map<String, String> params)
    {
        params = injectEntityType(params);
        SolrParams solrParams = new MapSolrParams(params);

        return getBeans(solrParams);
    }

    public List<E> findBeansByQuery(String query)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", query);
        params = injectEntityType(params);

        SolrParams solrParams = new MapSolrParams(params);

        return getBeans(solrParams);
    }

    public abstract SolrCollectionType getSolrCollectionType();

    private Map<String, String> injectEntityType(Map<String, String> params)
    {
        String value = params.get("q");
        value = value + " AND entityType:" + entityClass.getSimpleName().toLowerCase();
        params.put("q", value);

        return params;
    }

    private List<E> getBeans(SolrParams params)
    {
        try
        {
            QueryResponse response = queryClient.query(params);
            response.getResults();
            // System.out.println(response);
            return response.getBeans(entityClass);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    private E getBean(SolrParams params)
    {
        try
        {
            QueryResponse response = queryClient.query(params);
            System.out.println(response);
            List<E> beans = response.getBeans(entityClass);

            if (beans != null && beans.size() > 1)
            {
                throw new IllegalStateException("Non unique entity found.");
            }

            return beans.get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public SolrClient getClient()
    {
        return updateClient;
    }

    public void partialUpdate(SolrInputDocument solrInputDocument)
    {
        try
        {
            updateClient.add(solrInputDocument);
            // updateClient.commit();
        }
        catch (SolrServerException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void partialUpdate(List<SolrInputDocument> solrInputDocument)
    {
        try
        {
            updateClient.add(solrInputDocument);
            // updateClient.commit();
        }
        catch (SolrServerException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void update(List<E> solrBeans)
    {
        try
        {
            if (solrBeans == null)
                return;
            List<SolrInputDocument> inputDocumentList = new ArrayList<SolrInputDocument>();
            for (E entity : solrBeans)
            {
                if (entity instanceof ISolrEntity)
                {
                    SolrInputDocument inputDocument = formSolrInputDocument(entity);
                    if (!inputDocument.getFieldNames().isEmpty())
                        inputDocumentList.add(inputDocument);
                }
            }
            if (!inputDocumentList.isEmpty())
            {
                updateClient.add(inputDocumentList);
                // updateClient.commit();
            }

        }
        catch (SolrServerException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public SolrInputDocument formSolrInputDocument(E bean)
    {
        SolrInputDocument tempDocument = updateClient.getBinder().toSolrInputDocument(bean);
        SolrInputDocument inputDocument = new SolrInputDocument();

        for (String field : tempDocument.getFieldNames())
        {
            if (!(field.equals("id") || field.equals("entityType") || tempDocument.getFieldValue(field) == null))
            {
                Map<String, Object> fieldValue = new HashMap<String, Object>();
                if (tempDocument.getField(field).getValueCount() > 1)
                {
                    fieldValue.put("set", tempDocument.getFieldValues(field));
                }
                else
                {
                    fieldValue.put("set", tempDocument.getFieldValue(field));
                }
                inputDocument.addField(field, fieldValue);
            }
            else if (field.equals("id"))
            {
                if (tempDocument.getFieldValue("id") == null || tempDocument.getFieldValue("id").toString().trim().isEmpty())
                {
                    throw new IllegalStateException("id not present while trying to update.");
                }
                else
                {
                    inputDocument.addField("id", tempDocument.getFieldValue("id"));
                }
            }
        }

        return inputDocument;
    }

    public SolrInputDocument getInputDoc(E entity)
    {
        return updateClient.getBinder().toSolrInputDocument(entity);
    }
}
