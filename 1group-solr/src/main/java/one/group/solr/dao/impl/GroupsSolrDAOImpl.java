package one.group.solr.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.SolrCollectionType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.v2.WSAdminGroupMetaData;
import one.group.entities.socket.Groups;
import one.group.solr.dao.GroupsSolrDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.apache.solr.common.params.MapSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class GroupsSolrDAOImpl extends AbstractSolrDaoImpl<Groups> implements GroupsSolrDAO
{

    private static final Logger logger = LoggerFactory.getLogger(GroupsSolrDAOImpl.class);

    @Override
    public SolrCollectionType getSolrCollectionType()
    {
        return SolrCollectionType.ONEGROUP;
    }

    public Map<GroupStatus, Set<Groups>> fetchGroupsByGroupStatusAndEverSync(List<GroupStatus> groupStatus, boolean everSync)
    {
        Map<String, String> params = new HashMap<String, String>();
        Map<GroupStatus, Set<Groups>> groupStatusVsMetaData = new HashMap<GroupStatus, Set<Groups>>();

        String q = "";

        if (groupStatus != null && !groupStatus.isEmpty())
        {
            String groupStatusListString = "status_s:(" + Utils.getCollectionAsString(groupStatus, " ") + ")";
            q = groupStatusListString + "  AND ";
        }

        q = q + "everSync:" + everSync;
        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");
        params.put("fl", "id,status_s");
        params.put("group", "true");
        params.put("group.field", "status_s");
        params.put("group.limit", "" + Integer.MAX_VALUE);

        System.out.println(q);

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            GroupResponse gResponse = response.getGroupResponse();
            for (GroupCommand c : gResponse.getValues())
            {
                // System.out.println(c.getName() + " : " + c.getMatches());
                for (Group solrGroup : c.getValues())
                {
                    GroupStatus status = GroupStatus.parse(solrGroup.getGroupValue());
                    groupStatusVsMetaData.put(status, new HashSet<Groups>());
                    for (SolrDocument solrDoc : solrGroup.getResult())
                    {
                        if (solrDoc.get("status_s") != null)
                        {
                            Groups g = new Groups();
                            g.setId("" + solrDoc.get("id"));
                            g.setStatus_s("" + solrDoc.get("status_s"));
                            groupStatusVsMetaData.get(status).add(g);
                        }
                    }

                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupStatusVsMetaData;
    }

    public List<WSGroupMetaData> fetchGroupCountMetaData(List<GroupStatus> groupStatus, boolean everSync, boolean isDuplicate, List<BpoMessageStatus> bpoMessageStatusList,
            List<MessageStatus> messageStatusList)
    {
        List<WSGroupMetaData> groupMetaDataList = new ArrayList<WSGroupMetaData>();
        Map<String, String> params = new HashMap<String, String>();

        String q = "";
        String groupStatusListString = "*";

        if (groupStatus != null && !groupStatus.isEmpty())
        {
            groupStatusListString = "status_s:(" + Utils.getCollectionAsString(groupStatus, " ") + ")";
            q = groupStatusListString + "  AND ";
        }

        if (bpoMessageStatusList != null && !bpoMessageStatusList.isEmpty())
        {
            String bpoMessageStatusListString = "bpoMessageStatus_s:(" + Utils.getCollectionAsString(bpoMessageStatusList, " ") + ")";
            q = q + " AND " + bpoMessageStatusListString;
        }

        if (messageStatusList != null && !messageStatusList.isEmpty())
        {
            String messageStatusListString = "messageStatus_s:(" + Utils.getCollectionAsString(messageStatusList, " ") + ")";
            q = q + " AND " + messageStatusListString + "  AND ";
        }

        q = "{!join from=id to=id}(entityType:groups AND " + groupStatusListString + " AND everSync:" + everSync + ")";

        q = q + "isDuplicate:" + isDuplicate;

        q = q + "everSync:" + everSync;
        params.put("q", q);
        params.put("rows", Integer.MAX_VALUE + "");
        params.put("facet", "on");
        params.put("fl", "id");
        params.put("facet.field", "groupId");
        params.put("rows", "0");

        try
        {

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupMetaDataList;
    }

    public int fetchLastMessageIndexOfGroup(String groupId)
    {
        return -1;
    }

    public List<Groups> fetchGroupLabelAndIdByStatus(List<GroupStatus> groupStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String q = "entityType:groups";
        String groupStatusListString = "*";

        if (groupStatusList != null && !groupStatusList.isEmpty())
        {
            groupStatusListString = " AND status_s:(" + Utils.getCollectionAsStringForEnum(groupStatusList, " ") + ")";
            q = q + groupStatusListString;
        }

        params.put("q", q);
        params.put("fl", "id,groupName");
        params.put("start", 0 + "");
        params.put("rows", Integer.MAX_VALUE + "");

        if (sort != null && !sort.isEmpty())
        {
            params.put("sort", Utils.sortMapToSolrString(sort));
        }

        if (filter != null && !filter.isEmpty())
        {
            params.put("fq", Utils.filterMapToSolrString(filter));
        }

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public List<Groups> fetchGroupsByGroupIds(List<String> groupIdList)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String q = " entityType:groups ";
        String groupIdListString = "*";

        if (groupIdList != null && !groupIdList.isEmpty())
        {
            groupIdListString = " AND id:(" + Utils.getCollectionAsString(groupIdList, " ") + ")";
            q = q + groupIdListString;
        }

        params.put("q", q);
        params.put("start", 0 + "");
        params.put("rows", 100 + "");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public List<Groups> fetchGroupIdFromMembers(List<String> participantIds, GroupSource groupSource)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String queryString = " entityType:groups AND source_s:" + groupSource.name();

        if (participantIds != null && !participantIds.isEmpty())
        {
            queryString += " AND participants:(" + Utils.getCollectionAsString(participantIds, " AND ") + ")";
        }

        params.put("q", queryString);

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public void saveGroups(Groups groups)
    {
        persist(groups);
    }

    public List<Groups> fetchUnassignedGroupsBasedOnCityAndStatusList(List<GroupStatus> groupStatusList, List<String> cityIdList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String q = "entityType:groups ";
        String groupStatusListString = "*";
        String cityIdListString = "*";

        if (groupStatusList != null && !groupStatusList.isEmpty())
        {
            groupStatusListString = " AND status_s:(" + Utils.getCollectionAsStringForEnum(groupStatusList, " ") + ")";
            q = q + groupStatusListString;
        }

        if (cityIdList != null && !cityIdList.isEmpty())
        {
            cityIdListString = " AND cityId:(" + Utils.getCollectionAsString(cityIdList, " ") + ")";
            q = q + cityIdListString;
        }

        q = q + " AND -assignedTo:[\"\" TO *]";
        q = q + " AND unreadmsgCount:[1 TO *]";

        params.put("q", q);
        params.put("rows", "1");

        if (sort != null && !sort.isEmpty())
        {
            params.put("sort", Utils.sortMapToSolrString(sort));
        }

        if (filter != null && !filter.isEmpty())
        {
            params.put("fq", Utils.filterMapToSolrString(filter));
        }

        logger.info("UNASSIGNED:" + params);

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public List<Groups> fetchAllGroupsByAccountId(String accountId, List<GroupSource> groupSourceList)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String queryString = " entityType:groups ";

        queryString += " AND (participants:(" + accountId + ")) ";
        queryString += " AND source_s:(" + Utils.getCollectionAsStringForEnum(groupSourceList, " OR ") + ")";

        params.put("q", queryString);
        params.put("sort", "updatedTime DESC");
        params.put("rows", Integer.MAX_VALUE + "");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public static void main(String[] args) throws Exception
    {
        GroupsSolrDAOImpl dao = new GroupsSolrDAOImpl();
        SolrInputDocument toPersist = new SolrInputDocument();
//        toPersist.addField("id", "test");
        Groups g = new Groups();
//        g.setId("1");
//        g.setStatus(GroupStatus.ACTIVE);
//        dao.formSolrInputDocument(g);
        System.out.println(dao.formSolrInputDocument(g).getFieldNames().size());
        System.out.println(dao.formSolrInputDocument(g).getFieldNames());
    }

    public List<Groups> fetchDefaultGroupsByAccountId(String accountId)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String queryString = " entityType:groups AND createdBy:" + accountId + " AND source_s:(" + GroupSource.ME.name() + " OR " + GroupSource.ONEGROUP.name() + " OR " + GroupSource.STARRED.name()
                + ")";

        params.put("q", queryString);
        params.put("sort", "createdTime asc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public List<Groups> fetchGroupsByInputs(List<GroupStatus> groupStatusList, List<String> groupIds, int msgCountGreaterThan, int start, int rows, String sortByField, String sortByType,
            Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String q = "entityType:groups ";
        String groupStatusListString = "*";
        String groupIdListString = "*";

        if (groupStatusList != null && !groupStatusList.isEmpty())
        {
            groupStatusListString = " AND status_s:(" + Utils.getCollectionAsStringForEnum(groupStatusList, " ") + ")";
            q = q + groupStatusListString;
        }

        if (groupIds != null && !groupIds.isEmpty())
        {
            groupIdListString = " AND id:(" + Utils.getCollectionAsString(groupIds, " ") + ")";
            q = q + groupIdListString;
        }

        if (rows == 0)
        {
            rows = 100;
        }

        // q = q + " AND everSync:true";
        // q = q + " AND unreadmsgCount:{" + msgCountGreaterThan + " TO *}";

        params.put("q", q);
        params.put("rows", rows + "");
        params.put("start", start + "");

        if (sort != null && !sort.isEmpty())
        {
            params.put("sort", Utils.sortMapToSolrString(sort));
        }

        if (filter != null && !filter.isEmpty())
        {
            params.put("fq", Utils.filterMapToSolrString(filter));
        }

        logger.info(params + "");

        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
            List<String> groupIdList = new ArrayList<String>();
            if (groupsList != null && !groupsList.isEmpty())
            {
                for (Groups g : groupsList)
                {
                    groupIdList.add(g.getId());
                }

                // List<String> groupIdSubList = new ArrayList<String>();
                int subListSize = 20;
                List<List<String>> groupSubIdList = new ArrayList<List<String>>();
                for (int i = 0; i < (groupIdList.size() / subListSize); i++)
                {
                    int fromIndx = i * subListSize;
                    int toIndx = fromIndx + subListSize;
                    groupSubIdList.add(groupIdList.subList(fromIndx, toIndx));
                }
                List<String> remaining = new ArrayList<String>();

                for (int i = 0; i < (groupIdList.size() % subListSize); i++)
                {
                    remaining.add(groupIdList.get(groupIdList.size() - (i + 1)));
                }
                groupSubIdList.add(remaining);

                for (int i = 0; i < groupSubIdList.size(); i++)
                {
                    // if (i != 0 && (i % 20 == 0 || i == (groupIdList.size() -
                    // 1)))
                    // {
                    q = "entityType:message AND groupId:(" + Utils.getCollectionAsString(groupSubIdList.get(i), " ") + ")";
                    params = new HashMap<String, String>();
                    params.put(
                            "fq",
                            "bpoMessageStatus_s:("
                                    + Utils.getCollectionAsStringForEnum(Arrays.asList(BpoMessageStatus.UNREAD, BpoMessageStatus.BULK_READ, BpoMessageStatus.READ, BpoMessageStatus.UNQUALIFIED), " ")
                                    + ")");
                    params.put("fl", "id,groupId");
                    params.put("group", "true");
                    params.put("group.field", "bpoMessageStatus_s");
                    params.put("group.limit", "" + Integer.MAX_VALUE);
                    params.put("q", q);

                    logger.info("message query: " + q);
                    response = getClient().query(new MapSolrParams(params), METHOD.POST);
                    Set<Integer> visitedIndexSet = new HashSet<Integer>();

                    // response = getClient().query(new
                    // MapSolrParams(params));
                    GroupResponse gResponse = response.getGroupResponse();
                    for (GroupCommand c : gResponse.getValues())
                    {
                        // System.out.println(c.getName() + " : " +
                        // c.getMatches());
                        for (Group solrGroup : c.getValues())
                        {
                            String bpoMessageStatusString = solrGroup.getGroupValue();
                            BpoMessageStatus bpoMessageStatus = BpoMessageStatus.valueOf(bpoMessageStatusString);

                            for (SolrDocument solrDoc : solrGroup.getResult())
                            {
                                String groupId = "" + solrDoc.get("groupId");
                                Groups dummyGroup = new Groups();
                                dummyGroup.setId(groupId);

                                int index = groupsList.indexOf(dummyGroup);
                                if (index == -1)
                                {
                                    continue;
                                }

                                if (!visitedIndexSet.contains(new Integer(index)))
                                {
                                    groupsList.get(index).setReadmsgCount(0);
                                    groupsList.get(index).setUnqualifiedmsgCount(0);
                                    groupsList.get(index).setUnreadmsgCount(0);
                                    visitedIndexSet.add(index);
                                }

                                if (BpoMessageStatus.READ.equals(bpoMessageStatus) || BpoMessageStatus.BULK_READ.equals(bpoMessageStatus))
                                {
                                    groupsList.get(index).incrementReadmsgCount();
                                }
                                else if (BpoMessageStatus.UNQUALIFIED.equals(bpoMessageStatus))
                                {
                                    groupsList.get(index).incrementUnqualifiedmshCount();
                                }
                                else if (BpoMessageStatus.UNREAD.equals(bpoMessageStatus))
                                {
                                    groupsList.get(index).incrementUnReadmsgCount();
                                }
                            }

                        }
                    }

                    // groupIdSubList.clear();
                    // }
                    // else
                    // {
                    // groupIdSubList.add(groupIdList.get(i));
                    // }
                }

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public void releaseAllGroups()
    {
        String q = "entityType:groups";
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", q);
        params.put("start", "0");
        params.put("rows", "1000");
        boolean loop = true;
        int fetched = 0;
        long numFound = -1;

        try
        {
            while (loop)
            {
                QueryResponse response = getClient().query(new MapSolrParams(params));
                if (numFound == -1)
                {
                    numFound = response.getResults().getNumFound();
                }

                List<Groups> groupList = response.getBeans(Groups.class);
                fetched = fetched + groupList.size();

                for (Groups g : groupList)
                {
                    g.setAssignedTo(null);
                }
                getClient().addBeans(groupList);

                if (fetched >= numFound)
                {
                    loop = false;
                }
                else
                {
                    params.put("start", "" + fetched);
                    params.put("rows", "1000");
                }
                System.out.println(params);
            }

            getClient().commit();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public List<Groups> fetchAllGroupsByAccountIdAndGroupId(String accountId, String groupId)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String queryString = " entityType:groups AND createdBy:" + accountId + " AND source_s:(" + GroupSource.ME.name() + " OR " + GroupSource.ONEGROUP.name() + " OR " + GroupSource.STARRED.name()
                + ") AND participants:(" + accountId + " AND " + accountId + ") AND id:" + groupId;

        params.put("q", queryString);
        params.put("sort", "createdTime asc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public QueryResponse fetchGroupsToUpdateUnreadFrom(int start, int rows, boolean fullEntity, Collection<String> groupList)
    {
        Map<String, String> params = new HashMap<String, String>();

        if (groupList != null && !groupList.isEmpty())
        {
            params.put("q", "entityType:message AND groupId:(" + Utils.getCollectionAsString(groupList, " ") + ")");
        }
        else
        {
            params.put("q", "entityType:message");
        }

        params.put("start", start + "");
        params.put("rows", rows + "");
        params.put("group", "true");
        params.put("group.field", "groupId");
        params.put("group.sort", "bpoStatusUpdateTime desc");
        params.put("group.limit", "1");
        // params.put("group.main", "true");

        if (!fullEntity)
        {
            params.put("fl", "id,groupId,bpoStatusUpdateTime");
        }

        try
        {
            System.out.println(params);
            return getClient().query(new MapSolrParams(params), METHOD.POST);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public Map<String, Groups> fetchGroups(int start, int rows)
    {
        Map<String, String> params = new HashMap<String, String>();

        params.put("q", "entityType:groups AND everSync:true");
        params.put("start", start + "");
        params.put("rows", rows + "");
        params.put("fl", "id,createdTime");

        Map<String, Groups> response = new HashMap<String, Groups>(rows);
        try
        {
            System.out.println(params);
            QueryResponse queryResponse = getClient().query(new MapSolrParams(params));
            SolrDocumentList resultList = queryResponse.getResults();
            if (resultList != null)
            {
                for (SolrDocument sDoc : resultList)
                {
                    String groupId = sDoc.get("id").toString();
                    Groups g = new Groups();
                    g.setId(groupId);
                    g.setCreatedTime((Date) sDoc.get("createdTime"));
                    response.put(groupId, g);
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    public void saveGroupsBulk(List<Groups> groupsList)
    {
        persistAll(groupsList);
    }

    public void update(SolrInputDocument groups)
    {
        SolrInputField id = groups.getField("id");
        Validation.isTrue(!Utils.isNull(id), "Id must not be null");
        Validation.isTrue(!id.getValue().equals(""), "Id must not be empty");

        partialUpdate(groups);
    }

    public List<Groups> fetchGroupsByGroupIdsAndSource(List<String> groupIdList, List<GroupSource> groupSourceList)
    {
        List<Groups> groupsList = new ArrayList<Groups>();
        Map<String, String> params = new HashMap<String, String>();

        String q = " entityType:groups ";
        String groupIdListString = "*";

        if (groupIdList != null && !groupIdList.isEmpty())
        {
            groupIdListString = " AND id:(" + Utils.getCollectionAsString(groupIdList, " OR ") + ")";
            q = q + groupIdListString;
        }

        if (groupSourceList != null && !groupSourceList.isEmpty())
        {
            groupIdListString = " AND source_s:(" + Utils.getCollectionAsStringForEnum(groupSourceList, " OR ") + ")";
            q = q + groupIdListString;
        }

        params.put("q", q);
        params.put("start", 0 + "");
        params.put("rows", Integer.MAX_VALUE + "");
        params.put("sort", "updatedTime desc");
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            groupsList = response.getBeans(Groups.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return groupsList;
    }

    public int fetchTotalGroupCount()
    {
        Map<String, String> params = new HashMap<String, String>();
        String q = "entityType:groups AND source_s:WHATSAPP";
        params.put("q", q);
        params.put("rows", "0");
        int totalGroupCount = 0;
        try
        {
            QueryResponse response = getClient().query(new MapSolrParams(params));
            totalGroupCount = (int) response.getResults().getNumFound();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return totalGroupCount;
    }

    public WSAdminGroupMetaData fetchGroupMetaDataByGroupIdsAndEverSync(List<String> groupIdList, boolean everSync)
    {
        List<List<String>> subGroupIds = new ArrayList<List<String>>();
        int size = groupIdList.size();
        int maxsize = 20;
        for (int i = 0; i < size; i += maxsize)
        {
            subGroupIds.add(new ArrayList<String>(groupIdList.subList(i, Math.min(size, i + maxsize))));
        }

        Map<String, Long> statusVsCount = new HashMap<String, Long>();
        int totalGroups = 0;
        for (List<String> groupIdsList : subGroupIds)
        {
            String q = " entityType:groups ";
            String groupIdListString = "*";

            groupIdListString = " AND id:(" + Utils.getCollectionAsString(groupIdsList, " OR ") + ")" + " and facet=on";

            q = q + groupIdListString;

            SolrQuery solrQuery = new SolrQuery(q);
            solrQuery.setFacet(true);
            solrQuery.addFacetField("status_s");
            solrQuery.setRows(1);

            try
            {
                QueryResponse response = getClient().query(solrQuery);
                List<Count> values = response.getFacetField("status_s").getValues();
                long groupCount = response.getResults().getNumFound();
                totalGroups += (int) groupCount;
                for (Count count : values)
                {
                    String status = count.getName();
                    long countValMap = count.getCount();
                    if (statusVsCount.containsKey(status))
                    {
                        long countval = statusVsCount.get(status);
                        countval = countval + countValMap;
                        statusVsCount.put(status, countval);
                    }
                    else
                    {
                        statusVsCount.put(status, countValMap);
                    }

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        WSAdminGroupMetaData groupMetaData = new WSAdminGroupMetaData();
        for (String status_s : statusVsCount.keySet())
        {
            long groupCount = statusVsCount.get(status_s);
            int statusGroupCount = (int) groupCount;
            String status = status_s.toLowerCase();

            if (status.equals(GroupStatus.ACTIVE.toString()))
            {
                groupMetaData.setApprovedGroupCount(statusGroupCount);

            }
            else if ((status.equals(GroupStatus.NEW.toString())))
            {
                long newCount = statusVsCount.get(GroupStatus.NEW.toString().toUpperCase());
                if (newCount > 0)
                {
                    statusGroupCount += newCount;
                }
                groupMetaData.setNewGroupCount(statusGroupCount);
            }
            else if (status.equals(GroupStatus.IRRELEVANT.toString()))
            {
                groupMetaData.setRejectedGroupCount(statusGroupCount);
            }

            else if (status.equals(GroupStatus.SYNC_DISABLED.toString()))
            {
                groupMetaData.setSyncDisabledGroupCount(statusGroupCount);
            }
            else if (status.equals(GroupStatus.INSUFFICIENT_MESSAGES.toString()))
            {
                long newCount = statusVsCount.get(GroupStatus.NEW.toString().toUpperCase());
                if (newCount > 0)
                {
                    statusGroupCount += newCount;
                }
                groupMetaData.setNewGroupCount(statusGroupCount);
            }
        }
        groupMetaData.setTotalGroups(totalGroups);
        return groupMetaData;
    }

}
