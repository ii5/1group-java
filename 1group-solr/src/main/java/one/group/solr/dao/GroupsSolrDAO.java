package one.group.solr.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.v2.WSAdminGroupMetaData;
import one.group.entities.socket.Groups;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface GroupsSolrDAO extends SolrBaseDao<String, Groups>
{
    public Map<GroupStatus, Set<Groups>> fetchGroupsByGroupStatusAndEverSync(List<GroupStatus> groupStatus, boolean everSync);

    public int fetchLastMessageIndexOfGroup(String groupId);

    public List<Groups> fetchGroupLabelAndIdByStatus(List<GroupStatus> groupStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<Groups> fetchGroupsByGroupIds(List<String> groupIdList);

    public List<Groups> fetchGroupIdFromMembers(List<String> participantIds, GroupSource groupSource);

    public void saveGroups(Groups groups);

    public List<Groups> fetchAllGroupsByAccountId(String accountId, List<GroupSource> groupSourceList);

    public List<Groups> fetchUnassignedGroupsBasedOnCityAndStatusList(List<GroupStatus> groupStatusList, List<String> cityIdList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<Groups> fetchGroupsByInputs(List<GroupStatus> groupStatusList, List<String> groupIds, int msgCountGreaterThan, int rows, int limit, String sortByField, String sortByType,
            Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter);

    public List<Groups> fetchDefaultGroupsByAccountId(String accountId);

    public void releaseAllGroups();

    public List<Groups> fetchAllGroupsByAccountIdAndGroupId(String accountId, String groupId);

    public void saveGroupsBulk(List<Groups> groupsList);

    public void update(SolrInputDocument groups);

    public QueryResponse fetchGroupsToUpdateUnreadFrom(int start, int rows, boolean fullEntity, Collection<String> groupList);

    public Map<String, Groups> fetchGroups(int start, int rows);

    public List<Groups> fetchGroupsByGroupIdsAndSource(List<String> groupIdList, List<GroupSource> groupSourceList);

    public int fetchTotalGroupCount();

    public WSAdminGroupMetaData fetchGroupMetaDataByGroupIdsAndEverSync(List<String> groupIdList, boolean everSync);

}
