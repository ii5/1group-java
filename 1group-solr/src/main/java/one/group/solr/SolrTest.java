package one.group.solr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.StreamingResponseCallback;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.SolrCollectionType;
import one.group.entities.socket.Groups;
import one.group.entities.socket.Message;
import one.group.solr.dao.impl.GroupsSolrDAOImpl;


public class SolrTest
{
    public static void main(String[] args) throws Exception
    {
//    	updateMessagesSenderPhoneNumber();
//    	streamResponseTest();
    	long start = System.currentTimeMillis();
    	archiveMessages();
    	long end = System.currentTimeMillis();
    	
    	System.out.println((end - start));
    }
    
    private static void updateGroupStatus() throws Exception
    {
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
    	Map<String, String> params = new HashMap<String, String>();
    	SolrDocumentList documentList = null;
    	int start = 0;
    	int rows = 5000;
    	do
    	{
	    	List<SolrInputDocument> documentsToUpdate = new ArrayList<SolrInputDocument>();
	    	params.put("q", "entityType:groups AND status_s:Sync_Disabled");
	    	params.put("fl", "id");
	    	params.put("start", start+"");
	    	params.put("rows", rows+"");
	    	
	    	QueryResponse response = c.query(new MapSolrParams(params));
	    	documentList = response.getResults();
	    	
	    	System.out.println("Remaining: " + documentList.getNumFound() + " : " + params);
	    	
	    	if (documentList != null)
	    	{
	    		for (SolrDocument document : documentList)
	    		{
	    			String id = document.get("id").toString();
	    			
	    			Map<String, String> updatedValues = new HashMap<String, String>();
	    			updatedValues.put("set", GroupStatus.SYNC_DISABLED.name());
	    			
	    			SolrInputDocument inputDoc = new SolrInputDocument();
	    			inputDoc.addField("id", id);
	    			inputDoc.addField("status_s", updatedValues);
	    			
	    			documentsToUpdate.add(inputDoc);
	    		}
	    	}
	    	
	    	if (!documentsToUpdate.isEmpty())
	    	{
	    		c.add(documentsToUpdate);
	    	}
	    	
	    	c.commit();
	    	start = start + rows;
    	} while (documentList != null && !documentList.isEmpty());
    	
    	c.close();
    }
    
    private static void updateBPOMessageStatusOfMessage() throws Exception
    {
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
    	Map<String, String> params = new HashMap<String, String>();
    	SolrDocumentList documentList = null;
    	int start = 0;
    	int rows = 5000;
    	do
    	{
	    	List<SolrInputDocument> documentsToUpdate = new ArrayList<SolrInputDocument>();
	    	params.put("q", "entityType:message AND bpoMessageStatus_s:" + BpoMessageStatus.READ.name());
	    	params.put("fl", "id");
	    	params.put("start", start+"");
	    	params.put("rows", rows+"");
	    	
	    	QueryResponse response = c.query(new MapSolrParams(params));
	    	documentList = response.getResults();
	    	
	    	System.out.println("Remaining: " + documentList.getNumFound() + " : " + params);
	    	
	    	if (documentList != null)
	    	{
	    		for (SolrDocument document : documentList)
	    		{
	    			String id = document.get("id").toString();
	    			
	    			Map<String, String> updatedValues = new HashMap<String, String>();
	    			updatedValues.put("set", BpoMessageStatus.UNREAD.name());
	    			
	    			SolrInputDocument inputDoc = new SolrInputDocument();
	    			inputDoc.addField("id", id);
	    			inputDoc.addField("bpoMessageStatus_s", updatedValues);
	    			
	    			documentsToUpdate.add(inputDoc);
	    		}
	    	}
	    	
	    	if (!documentsToUpdate.isEmpty())
	    	{
	    		c.add(documentsToUpdate);
	    	}
	    	
	    	c.commit();
	    	start = start + rows;
    	} while (documentList != null && !documentList.isEmpty());
    	
    	c.close();
    }
    
    private static void archiveMessages() throws Exception
    {
//    	ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:**.xml");
//    	context.getBean("waMessagesArchiveJpaDAO");
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
    	Map<String, String> params = new HashMap<String, String>();
    	List<String> messageIds = new ArrayList<String>();
    	int start = 0;
    	int rows = 100;
    	do
    	{
    		messageIds.clear();
	    	List<SolrInputDocument> documentsToUpdate = new ArrayList<SolrInputDocument>();
	    	params.put("q", "entityType:message AND messageSource_s:WHATSAPP AND messageType_s:TEXT AND createdTime:[NOW-15DAYS TO *]");
//	    	params.put("fl", "id");
	    	params.put("start", start+"");
	    	params.put("rows", rows+"");
	    	
	    	QueryResponse response = c.query(new MapSolrParams(params));
	    	List<Message> messageList = response.getBeans(Message.class);
	    	
	    	for (Message m : messageList)
	    	{
	    		messageIds.add(m.getId());
	    	}
	    	
	    	
	    	
//	    	System.out.println(messageList);
//	    	start = start + rows;
    	} while (messageIds != null && !messageIds.isEmpty());
    	
    	c.close();
    }
    
    private static void testAtomicUpdates() throws Exception
    {
    	GroupsSolrDAOImpl dao = new GroupsSolrDAOImpl();
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
    	Map<String, Groups> idVsGroupMap = dao.fetchGroups(0, 1000);
    	System.out.println(idVsGroupMap.size());
    	QueryResponse response = dao.fetchGroupsToUpdateUnreadFrom(0, 1000, false, new ArrayList<String>(idVsGroupMap.keySet()));
    	GroupResponse gResponse = response.getGroupResponse();
    	Map<String, SolrInputDocument> idVsDocList = new HashMap<String, SolrInputDocument>(1000);
    	
    	for (GroupCommand command : gResponse.getValues())
    	{		
    		for (Group g : command.getValues())
    		{
    			for (SolrDocument doc : g.getResult())
    			{
    				Date bpoMessageTime = doc.get("bpoStatusUpdateTime") == null ? null : (Date) doc.get("bpoStatusUpdateTime");
    				Groups group = idVsGroupMap.get(doc.get("groupId").toString());
    				Date groupCreatedTime = group.getCreatedTime();
    				Date unreadFrom = groupCreatedTime;
    				
    				if (bpoMessageTime != null)
    				{
    					if (bpoMessageTime.compareTo(groupCreatedTime) > 0)
    					{
    						unreadFrom = bpoMessageTime;
    					}
    				}
    				
    				Map<String, Object> updatedFields = new HashMap<String, Object>();
    				updatedFields.put("set", unreadFrom);
    				SolrInputDocument inputDoc = new SolrInputDocument();
    				inputDoc.addField("id", doc.get("groupId"));
    				inputDoc.addField("unreadFrom", updatedFields);
    				idVsDocList.put(doc.get("groupId").toString(), inputDoc);
    			}
    		}
    		
    	}
    	
    	c.add(idVsDocList.values());
    	c.commit();
    	
    }
    
    private static void streamResponseTest() throws Exception
    {
    	SolrTest t = new SolrTest();
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        
        Map<String, String> params = new HashMap<String, String>();
        Map<String, Map<String, Integer>> idVsCountMap = new HashMap<String, Map<String, Integer>>();
//        Map<String, Integer> idVsCountMapNew = new HashMap<String, Integer>();
        String findGroupsQuery = "entityType:message AND senderPhoneNumber:[ \"\" TO * ] AND -senderPhoneNumber:\\+*";

        params.put("q", findGroupsQuery);
//        params.put("start", "0");
        params.put("rows", "" + Integer.MAX_VALUE);

        QueryResponse response = c.queryAndStreamResponse(new MapSolrParams(params), t.new MyStreamingResponseCallback(c, Message.class));
        
        List<Message> messageList = response.getBeans(Message.class);
        
        for (Message m : messageList)
        {
        	
        }
        
    }
    
    private static void updateMessagesSenderPhoneNumber() throws Exception
    {
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        
        Map<String, String> params = new HashMap<String, String>();
        Map<String, Map<String, Integer>> idVsCountMap = new HashMap<String, Map<String, Integer>>();
//        Map<String, Integer> idVsCountMapNew = new HashMap<String, Integer>();
        String findGroupsQuery = "entityType:message AND senderPhoneNumber:[ \"\" TO * ] AND -senderPhoneNumber:\\+*";
        
        int start = 0;
        int rows = 100;
        params.put("q", findGroupsQuery);
        params.put("start", "0");
        params.put("rows", "100");
        params.put("fl", "id,senderPhoneNumber");
        
        boolean loop = true;
        int fetched = 0;
        long numFound = -1;
        
        try
        {
            while (loop)
            {
                QueryResponse response = c.query(new MapSolrParams(params));
                
                if (numFound == -1)
                {
                    numFound = response.getResults().getNumFound();
                }
                else
                {
                    System.out.println("New remaining: " + response.getResults().getNumFound());
                }

                List<Message> list = response.getBeans(Message.class);
                fetched = fetched + list.size();

                List<SolrInputDocument> updatedList = new ArrayList<SolrInputDocument>();
                for (Message o : list)
                {
                	String senderPhoneNumber = o.getSenderPhoneNumber();
                	if (senderPhoneNumber != null && !senderPhoneNumber.trim().isEmpty())
                	{
                		if (!senderPhoneNumber.startsWith("+"))
                		{
                			Map<String, Object> updatedFields = new HashMap<String, Object>();
                			
                			senderPhoneNumber = "+" + senderPhoneNumber;
                			
                			updatedFields.put("set", senderPhoneNumber);
                			
                			SolrInputDocument solrInputDocument = new SolrInputDocument();
                			solrInputDocument.addField("id", o.getId());
                			solrInputDocument.addField("senderPhoneNumber", updatedFields);
                			
                			updatedList.add(solrInputDocument);
                		}
                	}
                	
                	o.setSenderPhoneNumber(senderPhoneNumber);
                }
                
                System.out.println("Updated List: " + updatedList.size());
                
                if (!updatedList.isEmpty())
                	c.add(updatedList);
                
                System.out.println(numFound);

                if (fetched >= numFound)
                {
                    loop = false;
                }
                else
                {
                    params.put("start", "" + fetched);
                    params.put("rows", "1000");
                }
                System.out.println(params);
                c.commit(false, false, true);
                
//                System.out.println(response);
//                loop = true;
            }
            
            
            

//            System.out.println(idVsCountMapNew);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        c.commit();
    }
    
    private static void updateTotalMessageGroupCount() throws Exception
    {
    	SolrClient c = SolrServerFactory.getInstance().getUpdateClient(SolrCollectionType.ONEGROUP);
        
        Map<String, String> params = new HashMap<String, String>();
        Map<String, Map<String, Integer>> idVsCountMap = new HashMap<String, Map<String, Integer>>();
        Map<String, Integer> idVsCountMapNew = new HashMap<String, Integer>();
        String findGroupsQuery = "entityType:groups AND source_s:(WHATSAPP ME)";
        
        int start = 0;
        int rows = 100;
        params.put("q", findGroupsQuery);
        params.put("start", "0");
        params.put("rows", "100");
//        params.put("fl", "id");
        
        boolean loop = true;
        int fetched = 0;
        long numFound = -1;
        
        try
        {
            while (loop)
            {
                QueryResponse response = c.query(new MapSolrParams(params));
                
                if (numFound == -1)
                {
                    numFound = response.getResults().getNumFound();
                }
                else
                {
                    System.out.println("New remaining: " + response.getResults().getNumFound());
                }

                List<Groups> list = response.getBeans(Groups.class);
                fetched = fetched + list.size();

                List<Groups> updatedList = new ArrayList<Groups>();
                for (Groups o : list)
                {
                	SolrQuery q = new SolrQuery("entityType:message AND groupId:" + o.getId());
                    
                    q.setRows(0);
                    
                    QueryResponse gqResponse = c.query(q);
                    long totalMessages = gqResponse.getResults().getNumFound();
                    o.setTotalmsgCount(Integer.parseInt(totalMessages+""));
                    updatedList.add(o);
                	
                }
                
                if (!updatedList.isEmpty())
                	c.addBeans(updatedList);
                
                System.out.println(numFound);

                if (fetched >= numFound)
                {
                    loop = false;
                }
                else
                {
                    params.put("start", "" + fetched);
                    params.put("rows", "100");
                }
                System.out.println(params);
                c.commit();
                
//                System.out.println(response);
//                loop = true;
            }
            
            
            

            System.out.println(idVsCountMapNew);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        c.commit();
    }
    
    public class MyStreamingResponseCallback extends StreamingResponseCallback
    {
    	private SolrClient c;
    	private Class<?> type;
    	private int count = 0;
    	
    	
    	public MyStreamingResponseCallback(SolrClient c, Class<?> type) 
    	{
    		this.c = c;
    		this.type = type;
		}
    	
    	@Override
    	public void streamDocListInfo(long numFound, long start, Float maxScore) 
    	{
    		System.out.println(numFound + " : " + start + " : " + maxScore);
    	}
    	
    	@Override
    	public void streamSolrDocument(SolrDocument doc) 
    	{
    		System.out.println(count++ + " : " + c.getBinder().getBean(Message.class, doc).getId());
    		
    	}
    	
    	
    }
}
