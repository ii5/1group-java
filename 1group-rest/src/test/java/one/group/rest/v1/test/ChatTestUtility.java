package one.group.rest.v1.test;

import java.io.IOException;

import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageType;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.request.WSSendChatMessage;
import one.group.entities.api.response.WSChatThreadCursor;
import one.group.entities.api.response.WSPhotoReference;
import one.group.entities.api.response.WSResponse;
import one.group.utils.Utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ChatTestUtility
{

    public WSResponse sendTextChatMessage(String fromAccountId, String toAccountId, String text, String baseURL, String bearer) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSRequest request = new WSRequest();
        WSSendChatMessage chatMessage = new WSSendChatMessage();

        chatMessage.setFromAccountId(fromAccountId);
        chatMessage.setToAccountId(toAccountId);
        chatMessage.setType(MessageType.TEXT);
        chatMessage.setClientSentTime(System.currentTimeMillis());
        // Utils.randomString(Constant.CHARSET_ALPHABETS, 3, false) + " " +
        // Utils.randomString(Constant.CHARSET_ALPHABETS, 3, false)
        // + Utils.randomString(Constant.CHARSET_ALPHABETS, 3, false)
        chatMessage.setText(text);
        request.setChatMessage(chatMessage);

        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "chat_messages", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    public WSResponse sendPhonecallChatMessage(String fromAccountId, String toAccountId, String phoneCallText, String baseURL, String bearer) throws JsonMappingException, JsonGenerationException,
            IOException
    {
        WSRequest request = new WSRequest();
        WSSendChatMessage chatMessage = new WSSendChatMessage();

        chatMessage.setFromAccountId(fromAccountId);
        chatMessage.setToAccountId(toAccountId);
        chatMessage.setType(MessageType.PHONE_CALL);
        chatMessage.setText(phoneCallText);

        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "chat_messages", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    public WSResponse sendPropertyListingChatMessage(String fromAccountId, String toAccountId, String propertyListingId, String propertyListingShortReference, String baseURL, String bearer)
            throws JsonMappingException, JsonGenerationException, IOException
    {
        WSRequest request = new WSRequest();
        WSSendChatMessage chatMessage = new WSSendChatMessage();

        chatMessage.setFromAccountId(fromAccountId);
        chatMessage.setToAccountId(toAccountId);
        chatMessage.setType(MessageType.PROPERTY_LISTING);
        chatMessage.setPropertyListingId(propertyListingId);
        chatMessage.setPropertyListingShortReference(propertyListingShortReference);

        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "chat_messages", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    public WSResponse sendPhotoChatMessage(String fromAccountId, String toAccountId, WSPhotoReference photo, String baseURL, String bearer) throws JsonMappingException, JsonGenerationException,
            IOException
    {
        WSRequest request = new WSRequest();
        WSSendChatMessage chatMessage = new WSSendChatMessage();

        chatMessage.setFromAccountId(fromAccountId);
        chatMessage.setToAccountId(toAccountId);
        chatMessage.setType(MessageType.PHOTO);
        chatMessage.setPhoto(photo);

        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "chat_messages", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    public WSResponse updateChatThreadCursor(String chatThreadId, Integer receivedIndex, Integer readIndex, String baseURL, String bearer) throws JsonMappingException, JsonGenerationException,
            IOException
    {
        WSRequest request = new WSRequest();
        WSChatThreadCursor chatThreadCursor = new WSChatThreadCursor();
        chatThreadCursor.setReadIndex(readIndex);
        chatThreadCursor.setReceivedIndex(receivedIndex);
        request.setChatThreadCursor(chatThreadCursor);

        System.out.println("Cursor Server Request: " + request);

        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "chat_threads/" + chatThreadId + "/cursors", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }
}
