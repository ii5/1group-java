package one.group.rest.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import one.group.core.enums.Amenity;
import one.group.core.enums.CommissionType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.api.request.WSAddPropertyListing;
import one.group.entities.api.request.WSAdminPropertyListing;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSPropertyListingEditResult;
import one.group.entities.api.response.WSResponse;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/*
 * TODO: Document in depth
 */

@ContextConfiguration("/applicationContext-configuration.xml")
public class PropertyWebServiceTest
{
    @Autowired
    private PropertyListingJpaDAO propertyListingJpaDAO;

    CommonTestUtility commonTestUtility = new CommonTestUtility();

    public PropertyListingJpaDAO getPropertyListingJpaDAO()
    {
        return propertyListingJpaDAO;
    }

    public void setPropertyListingJpaDAO(PropertyListingJpaDAO propertyListingJpaDAO)
    {
        this.propertyListingJpaDAO = propertyListingJpaDAO;
    }

    private static String url = "http://10.50.249.127:8080/movein-rest/v1/";
    private static String mobileNumber = "+919975671304";
    private static String otherMobileNumber = "+919975671305";

    @Test
    public void testNoTokenAccountPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Account Property >> no token, accounts/<id>/property_listing");
        Response response = commonTestUtility.callWebEndPoint(url + "accounts/" + siginResponse.get(1) + "/property_listings", "", HTTPRequestMethodType.GET, null); // jsonResponseMap

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
    }

    @Test
    public void testEmptyTokenAccountPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Account Property >> no token, accounts/<id>/property_listing");
        Response response = commonTestUtility.callWebEndPoint(url + "accounts/" + siginResponse.get(1) + "/property_listings", "bearer ", HTTPRequestMethodType.GET, null); // jsonResponseMap

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
    }

    @Test
    public void testInvalidTokenAccountPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Account Property >> no token, accounts/<id>/property_listing");
        Response response = commonTestUtility.callWebEndPoint(url + "accounts/" + siginResponse.get(1) + "/property_listings", "bearer adc", HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
    }

    @Test
    public void testInvalidAccountIdAccountPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Account Property >> no token, accounts/<id>/property_listing");
        Response response = commonTestUtility.callWebEndPoint(url + "accounts/1234/property_listings", siginResponse.get(0), HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "404");
        assertFalse(isFatal);
    }

    @Test
    public void testValidAccountPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Account Property >> no token, accounts/<id>/property_listing");
        Response response = commonTestUtility.callWebEndPoint(url + "accounts/" + siginResponse.get(1) + "/property_listings", siginResponse.get(0), HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testNoTokenPropertyListingBookmarks() throws Exception
    {

        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Get Bookmarek Property Listings >> no token, property_listing/bookmarks");
        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/bookmarks", "", HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);

    }

    @Test
    public void testEmptyTokenPropertyListingBookmarks() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Get Bookmarek Property Listings >> no token, property_listing/bookmarks");
        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/bookmarks", "bearer ", HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidTokenPropertyListingBookmarks() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Get Bookmarek Property Listings >> no token, property_listing/bookmarks");
        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/bookmarks", "bearer abxcxs", HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);
    }

    @Test
    public void testValidPropertyListingBookmarks() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Get Bookmarek Property Listings >> no token, property_listing/bookmarks");
        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/bookmarks", siginResponse.get(0), HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testNoTokenPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> no token, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test Description");
        wsAddPropertyListing.setLocationId("3592");
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.GYM.toString());
        amenities.add(Amenity.PARKING.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyTokenPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Access token, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test Description");
        wsAddPropertyListing.setLocationId("3592");
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.GYM.toString());
        amenities.add(Amenity.PARKING.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", "", HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidTokenPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid access token, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test Description");
        wsAddPropertyListing.setLocationId("3592");
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.GYM.toString());
        amenities.add(Amenity.PARKING.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", "bearer ", HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();

        assertEquals(code, "401");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyDescriptionPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Description, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("");
        wsAddPropertyListing.setLocationId("3592");
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.GYM.toString());
        amenities.add(Amenity.PARKING.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamAmenitiesPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Add without amenities, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyAmenitiesPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Add without amenities, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();

        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testInvalidAmenitiesPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Add invalid amenities, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add("test");
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertFalse(isFatal);
    }

    @Test
    public void testInvalidValueAmenitiesPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Add invalid wild characters amenities, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add("&%^%^%^%^%%%");
        amenities.add("<script>alert('hii')</script>");
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertFalse(isFatal);
    }

    @Test
    public void testNoParamPropertyTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Property type defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Property type defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setPropertyType("");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid Property type defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setPropertyType("#$%^^&%%");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamSubTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> NO Property Sub type defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testInvalidSubTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> NO Property Sub type defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setArea("845");
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType("invalid Sub type");
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamAreaPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Area defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyAreaPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidAreaPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("ewe344");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNonIntegerAreaPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("23.56");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamCommisionTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRooms(Rooms.ONE_BHK.toString());
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyCommisionTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType("");
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRooms(Rooms.ONE_BHK.toString());
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidCommisionTypePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Area, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);

        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setRooms(Rooms.ONE_BHK.toString());
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType("directorvia");
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamRoomsPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Rooms Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyRoomsPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Rooms Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms("");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidRoomsPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid Rooms Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms("1.5 bhk");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoParamLocationIdPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Location ID defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyLocalityIdPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty Location ID Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidLocalityIdPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid Location ID Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("700000");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("sasy$#@$%$$");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoSalePriceDefinedPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Sale Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidSellPriceNumericValueExpectedPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No Sale Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("$%^#@!");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNegativeValueSellPricePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Negative Sale Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("-56");
        wsAddPropertyListing.setRentPrice("0");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyRentPricePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty rent Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("5600000");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidRentPricePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid Rent Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("5600000");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRentPrice("45025.25");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNegativeValueRentPricePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Negative rent Price Defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setSalePrice("5600000");
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRentPrice("-45025");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testNoSellRentPricePropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> No sale and rent price defined, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyIsHotPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Empty is hot value, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        wsAddPropertyListing.setIsHot("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidIsHotPropertyListing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Invalid is hot value, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        wsAddPropertyListing.setIsHot("aaaaa");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        System.out.println(jsonResponse);
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testAddPropertyListing() throws Exception
    {
        WSResponse response = this.addPropertyListing();

        String code = response.getResult().getCode();
        Boolean isFatal = response.getResult().getIsFatal();

        assertEquals(code, "200");
        assertFalse(isFatal);

        Map data = (Map) response.getData();

        assertNotNull(data.get("property_listing_id").toString());
        assertNotNull(data.get("short_reference").toString());
    }

    private WSResponse addPropertyListings() throws Exception
    {
        WSResponse response = this.addPropertyListing();
        return response;
    }

    @Test
    public void testEmptyTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();

        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setPropertyType("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();

        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setPropertyType("ssss");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptySubTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("EDit Property with empty sub type");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setPropertySubType("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidSubTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid sub type");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setPropertySubType("sssss");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyAreaPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty area");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidAreaPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid area");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("90oio89");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyCommisionTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty commission type");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setCommissionType("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidCommisionTypePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid commission type");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setCommissionType("Direct OR VIA");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyRoomsPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty rooms");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setRooms("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidRoomsPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid rooms");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setRooms("sssss");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyLocalityIdPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty location id");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setLocationId("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidLocalityIdPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid location id");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setLocationId("ssaa");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptySellPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty sale price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setSalePrice("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidSellPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid sale price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setSalePrice("sss.256");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testZeroSellPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with zero sale price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setSalePrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyRentPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty rent price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setRentPrice("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidRentPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid rent price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setRentPrice("@#$@@");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testZeroRentPricePropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with zero rent price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setRentPrice("0");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "200");
        assertFalse(isFatal);
    }

    @Test
    public void testEmptyStatusPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty status");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setStatus("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidStatusPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with zero sale price");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setStatus("$%#^&*()_");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyIsHotPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty is hot value");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setIsHot("");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidIsHotPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with invalid is hot value");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setIsHot("test hot");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testEmptyActionPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty action - renew property listing");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setAction(" ");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidActionPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Edit property listing with empty action - renew property listing");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setAction("renewed");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
        assertTrue(isFatal);
    }

    @Test
    public void testInvalidPropertyIdBookmarkProperty() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Bookmark property listing with invalid property listing id");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId() + "test";

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId + "/bookmark?action=true", siginResponse.get(0), HTTPRequestMethodType.PUT, "");

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "404");
        assertFalse(jsonResponse.getResult().getIsFatal());

    }

    @Test
    public void testInvalidActionBookmarkProperty() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        System.out.println("Bookmark property listing with invalid property listing id");
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId + "/bookmark?action=tsrue", siginResponse.get(0), HTTPRequestMethodType.PUT, "");

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testEditClosedPropertyListingEdit() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setStatus("closed");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        System.out.println("Edit Property >> edit closed property, /property_listings_edit");

        WSRequest requestNew = new WSRequest();
        WSAddPropertyListing wsAddPropertyListingEdit = new WSAddPropertyListing();
        wsAddPropertyListingEdit.setArea("456");
        requestNew.setPropertyListing(wsAddPropertyListingEdit);

        Response responseNew = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(requestNew));

        String json = responseNew.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertFalse(jsonResponse.getResult().getIsFatal());

    }

    @Test
    public void testNoHeaderListPropertyListing() throws Exception
    {
        System.out.println("List Property >> No header, /property_listings/<property_list_id>");
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        Response responseNew = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, "", HTTPRequestMethodType.GET, null);

        String json = responseNew.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
        assertTrue(jsonResponse.getResult().getIsFatal());

    }

    @Test
    public void testEmptyHeaderListPropertyListing() throws Exception
    {
        System.out.println("List Property >> No header, /property_listings/<property_list_id>");
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        Response responseNew = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, "bearer ", HTTPRequestMethodType.GET, null);

        String json = responseNew.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testInvalidIdListPropertyListing() throws Exception
    {

        System.out.println("List Property >> No header, /property_listings/<property_list_id>");
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId() + "test";

        Response responseNew = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.GET, null);

        String json = responseNew.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testListPropertyListing() throws Exception
    {
        System.out.println("List Property >> No header, /property_listings/<property_list_id>");
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        Response responseNew = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.GET, null);

        String json = responseNew.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "200");
        assertFalse(jsonResponse.getResult().getIsFatal());
    }

    @Rollback(true)
    public WSResponse addPropertyListing() throws Exception
    {

        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Adding property Listing, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        return jsonResponse;

    }

    /**
     * Test case to verify one account cannot edit other property listing
     * 
     * @throws Exception
     */
    @Test
    public void testCannotEditOtherAccountsPropertyListing() throws Exception
    {

        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();
        // other account sigin

        List<String> siginResponseOtherAccount = commonTestUtility.signin(otherMobileNumber, url);
        String statusOtherAccount = siginResponseOtherAccount.get(2);

        if (statusOtherAccount.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, otherMobileNumber, siginResponseOtherAccount.get(0), url);
        }

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setArea("234");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility
                .callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponseOtherAccount.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        System.out.println("Edit Property >> edit closed property, /property_listings_edit");

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "403");
        assertFalse(jsonResponse.getResult().getIsFatal());

    }

    @Test
    public void testCannotRenewActivePropertyListing() throws Exception
    {
        System.out.println("List Property >> /property_listings/<property_list_id>");

        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }

        WSResponse addPropertyListingResponse = addPropertyListings();
        WSPropertyListingEditResult propertyresult = (WSPropertyListingEditResult) Utils.getInstanceFromJson(Utils.getJsonString(addPropertyListingResponse.getData()),
                WSPropertyListingEditResult.class);
        String propertyListingId = propertyresult.getPropertyListingId();

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        wsAddPropertyListing.setAction("renew");
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings/" + propertyListingId, siginResponse.get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertFalse(jsonResponse.getResult().getIsFatal());

    }

    @Test
    public void testAdminAddPropertyListingParametersMissing() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();

        wsAddPropertyListing.setAdmin(adminRequest);
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testAdminAddPropertyListingCreatedForEmpty() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();
        adminRequest.setCreatedFor("");
        wsAddPropertyListing.setAdmin(adminRequest);
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testAdminAddPropertyListingCreatedForInvalid() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();
        adminRequest.setCreatedFor("123456");
        wsAddPropertyListing.setAdmin(adminRequest);
        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "404");
        assertFalse(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testAdminAddPropertyEmptyExternalSource() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();
        adminRequest.setCreatedFor("123456");
        adminRequest.setExternalSource("");
        wsAddPropertyListing.setAdmin(adminRequest);

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testAdminAddPropertyEmptyExternalTime() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();
        adminRequest.setCreatedFor("123456");
        adminRequest.setExternalListingTime("");
        wsAddPropertyListing.setAdmin(adminRequest);

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertTrue(jsonResponse.getResult().getIsFatal());
    }

    @Test
    public void testAdminAddPropertyInvalidExternalTime() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);
        String status = siginResponse.get(2);

        if (status.equals("STATUS_UNREGISTERED"))
        {
            List<String> locations = new ArrayList<String>();
            locations.add("3592");
            commonTestUtility.register("Sanil", "Shet", locations, mobileNumber, siginResponse.get(0), url);
        }
        System.out.println("Add Property Listings >> Admin, property_listing/");

        WSRequest request = new WSRequest();
        WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
        List<String> amenities = new ArrayList<String>();
        amenities.add(Amenity.INTERCOM.toString());
        wsAddPropertyListing.setAmenities(amenities);
        wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
        wsAddPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        wsAddPropertyListing.setDescription("Test description");
        wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
        wsAddPropertyListing.setCommissionType(CommissionType.DIRECT.toString());
        wsAddPropertyListing.setArea("526");
        wsAddPropertyListing.setRooms(Rooms.FIVE_PLUS.toString());
        wsAddPropertyListing.setLocationId("3592");
        wsAddPropertyListing.setSalePrice("7000000");
        wsAddPropertyListing.setRentPrice("10000");
        WSAdminPropertyListing adminRequest = new WSAdminPropertyListing();
        adminRequest.setCreatedFor(siginResponse.get(1));
        adminRequest.setExternalListingTime("12345");
        adminRequest.setExternalSource("99acres");
        wsAddPropertyListing.setAdmin(adminRequest);

        request.setPropertyListing(wsAddPropertyListing);

        Response response = commonTestUtility.callWebEndPoint(url + "property_listings", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "400");
        assertFalse(jsonResponse.getResult().getIsFatal());
    }
}
