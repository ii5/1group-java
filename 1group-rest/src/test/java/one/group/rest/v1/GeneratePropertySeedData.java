package one.group.rest.v1;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.ws.rs.core.Response;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.entities.api.request.WSAccountRelations;
import one.group.entities.api.request.WSAddPropertyListing;
import one.group.entities.api.request.WSContacts;
import one.group.entities.api.request.WSNativeContact;
import one.group.entities.api.request.WSPropertyListingSearchQueryRequest;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@ContextConfiguration("/applicationContext-configuration.xml")
public class GeneratePropertySeedData
{

    public enum CommissionType
    {
        DIRECT, VIA;

        private static final List<CommissionType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomCommissionType()
        {
            return VALUES.get(RANDOM.nextInt(SIZE)).toString();
        }
    };

    public enum Rooms
    {
        ONE_RK("1rk"), ONE_BHK("1bhk"), ONE_HALF("1hbhk"), TWO("2bhk"), TWO_HALF("2hbhk"), THREE("3bhk"), FOUR("4bhk"), FIVE_PLUS("5pbhk");
        private String name;

        Rooms(String name)
        {
            this.name = name;
        }

        private static final List<Rooms> VALUES = Collections.unmodifiableList(Arrays.asList(Rooms.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomRooms()
        {
            return Rooms.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum PropertySubType
    {
        APARTMENT("apartment"), INDEPENDENT_HOUSE("independent_house"), ROW_HOUSE("row_house"), VILLA("villa");

        private String name;

        PropertySubType(String name)
        {
            this.name = name;
        }

        private static final List<PropertySubType> VALUES = Collections.unmodifiableList(Arrays.asList(PropertySubType.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomSubType()
        {
            return PropertySubType.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum Amenity
    {
        GARDEN("garden"), PARKING("parking"), POOL("pool"), INTERCOM("intercom"), SECURITY("security"), CLUBHOUSE("clubhouse"), GYM("gym"), LIFT("lift"), PLAY_AREA("play_area"), MAINTENANCE_STAFF(
                "maintenance_staff"), RAIN_HARVESTING("rain_harvesting");

        private String name;

        Amenity(String name)
        {
            this.name = name;
        }

        private static final List<Amenity> VALUES = Collections.unmodifiableList(Arrays.asList(Amenity.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomAmenity()
        {
            return Amenity.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }

    }

    CommonTestUtility commonTestUtility = new CommonTestUtility();

    // private static String url = "http://10.50.249.127:8080/movein-rest/v1/";
    private static String url = "http://10.50.249.80:8080/movein-rest/v1/";

    Random randomLocation = new Random();
    List<String> locations1 = new ArrayList<String>(Arrays.asList("1560", "1561", "1562", "1563", "1564", "1565", "1566", "1567", "1568", "1569", "1570", "1571", "1572", "1573", "1574", "1576",
            "1578", "2663", "13587", "1579", "1580", "1581", "1582"));

    @Test
    public void testGetAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        System.out.println("Started: " + new Date(System.currentTimeMillis()));
        String bearer = null;

        List<String> newNativeRegistrationList = new ArrayList<String>();

        List<String> accountList = new ArrayList<String>();
        String phonePrefix = "+912";
        String nativePhonePrefix = "+918";
        int totalAccounts = 1;
        int totalClientsPerAccount = 1;
        int totalPropertyPerAccount = 1;
        int totalSearchPerAccount = 1;
        int totalContactsPerAccount = 1;
        int totalPhoneNumberPerContact = 1;
        int totalRegisterNativeContactsPerContact = 1;
        for (int i = 0; i < totalAccounts; i++)
        {
            String mobileNumber = phonePrefix + this.generateRandomMobileNumber();
            int size = locations1.size();

            System.out.println("===================" + mobileNumber + "========================");
            List<String> account = commonTestUtility.signin(mobileNumber, url);
            bearer = account.get(0);
            accountList.add(account.get(1));

            String firstName = "Account - " + i;
            String lastName = "Company = " + i;
            List<String> locations = new ArrayList<String>();
            locations.add(locations1.get(randomLocation.nextInt(size)));

            // Account Registration
            commonTestUtility.register(firstName, lastName, locations, mobileNumber, bearer, url);

            // Clients of account
            for (int c = 0; c < totalClientsPerAccount; c++)
            {
                System.out.println("=====" + commonTestUtility.signin(mobileNumber, url) + "=====");
            }
            System.out.println("account: " + account.get(0));

            // Properties
            for (int p = 0; p < totalPropertyPerAccount; p++)
            {

                WSRequest request = new WSRequest();
                WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
                wsAddPropertyListing.setArea(Utils.randomString(Constant.CHARSET_NUMERIC, 3, true));
                wsAddPropertyListing.setCommissionType(CommissionType.randomCommissionType().toString().toLowerCase());
                wsAddPropertyListing.setDescription("Test Description");
                wsAddPropertyListing.setLocationId(locations1.get(randomLocation.nextInt(size)));
                List<String> amenities = new ArrayList<String>();
                amenities.add(Amenity.randomAmenity().toString());
                amenities.add(Amenity.randomAmenity().toString());
                amenities.add(Amenity.randomAmenity().toString());
                amenities.add(Amenity.randomAmenity().toString());

                wsAddPropertyListing.setAmenities(amenities);
                wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
                wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
                wsAddPropertyListing.setPropertySubType(PropertySubType.randomSubType().toString());
                wsAddPropertyListing.setSalePrice(Utils.randomString(Constant.CHARSET_NUMERIC, 8, true));
                wsAddPropertyListing.setRentPrice("0");
                wsAddPropertyListing.setRooms(Rooms.randomRooms().toString());
                System.out.println(wsAddPropertyListing);
                request.setPropertyListing(wsAddPropertyListing);
                Response response = commonTestUtility.callWebEndPoint(url + "property_listings", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

                String json = response.readEntity(String.class);
                WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

                if (!jsonResponse.getResult().getCode().equals("200"))
                {
                    System.out.println(jsonResponse);
                    System.exit(1);
                }
            }

            // Search
            for (int p = 0; p < totalSearchPerAccount; p++)
            {
                SecureRandom random = new SecureRandom();
                WSPropertyListingSearchQueryRequest searchRequest = new WSPropertyListingSearchQueryRequest();
                searchRequest.setPropertyType(Arrays.asList(new String[] { "residential" }));
                searchRequest.setLocationId(Integer.valueOf(locations1.get(random.nextInt(locations1.size()))));
                searchRequest.setPropertyMarket(Arrays.asList(new String[] { PropertyMarket.SECONDARY.toString() }));
                searchRequest.setTransactionType(Arrays.asList(new String[] { PropertyTransactionType.values()[random.nextInt(2)].toString() }));

                if (random.nextBoolean())
                {
                    List<String> amenities = new ArrayList<String>();
                    int limit = random.nextInt(9);
                    for (int k = 0; k < limit; k++)
                    {
                        amenities.add(Amenity.randomAmenity().toString());
                    }

                    if (!amenities.isEmpty())
                        searchRequest.setAmenities(new ArrayList<String>(amenities));
                }

                if (random.nextBoolean())
                {
                    Set<String> subTypes = new HashSet<String>();
                    int limit = random.nextInt(one.group.core.enums.PropertySubType.values().length);
                    for (int k = 0; k < limit; k++)
                    {
                        subTypes.add(PropertySubType.randomSubType().toString());
                    }

                    if (!subTypes.isEmpty())
                        searchRequest.setPropertySubType(new ArrayList<String>(subTypes));
                }

                searchRequest.setMaxArea(random.nextInt(Integer.MAX_VALUE));
                searchRequest.setMinArea(random.nextInt(searchRequest.getMaxArea()));
                searchRequest.setMaxRentPrice(random.nextInt(Integer.MAX_VALUE) + "");
                searchRequest.setMinRentPrice(random.nextInt(Integer.valueOf(searchRequest.getMaxRentPrice())) + "");
                searchRequest.setMaxSalePrice(random.nextInt(Integer.MAX_VALUE) + "");
                searchRequest.setMinSalePrice(random.nextInt(Integer.valueOf(searchRequest.getMaxSalePrice())) + "");

                if (random.nextBoolean())
                {
                    Set<String> rooms = new HashSet<String>();
                    int limit = random.nextInt(Rooms.values().length);
                    for (int k = 0; k < limit; k++)
                    {
                        rooms.add(Rooms.randomRooms());
                    }

                    if (!rooms.isEmpty())
                        searchRequest.setRooms(new ArrayList<String>(rooms));
                }

                if (random.nextBoolean())
                {
                    searchRequest.setRequirementName("REQ-" + random.nextDouble());
                }

                WSRequest request = new WSRequest();
                request.setSearch(searchRequest);
                Response response = commonTestUtility.callWebEndPoint(url + "search/property_listings", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));
                String stringResponse = response.readEntity(String.class);
                WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(stringResponse, WSResponse.class);

                if (!jsonResponse.getResult().getCode().equals("200"))
                {
                    System.out.println(Utils.getJsonString(request));
                    System.err.println(jsonResponse);
                }
                else
                {
                    System.out.println(jsonResponse);
                }
            }

            // Add native contacts
            for (int c = 0; c < totalContactsPerAccount; c++)
            {
                SecureRandom random = new SecureRandom();
                List<WSNativeContact> nativeContacts = new ArrayList<WSNativeContact>();
                WSNativeContact wsNativeContacts = new WSNativeContact();
                wsNativeContacts.setFullName("FULLNAME- " + random.nextDouble());
                wsNativeContacts.setCompanyName("COMPANY- " + random.nextDouble());
                List<String> phoneNumberList = new ArrayList<String>();

                int count = 0;
                for (int p = 0; p < totalPhoneNumberPerContact; p++)
                {
                    String phoneNumber = nativePhonePrefix + this.generateRandomMobileNumber();

                    phoneNumberList.add(phoneNumber);

                    if (count < totalRegisterNativeContactsPerContact)
                    {
                        newNativeRegistrationList.add(phoneNumber);
                        count++;
                    }
                }
                wsNativeContacts.setNumbers(phoneNumberList);
                nativeContacts.add(wsNativeContacts);

                WSContacts contacts = new WSContacts();
                contacts.setNativeContacts(nativeContacts);

                WSRequest request = new WSRequest();
                request.setContacts(contacts);

                Response response = commonTestUtility.callWebEndPoint(url + "contacts/sync", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

                String json = response.readEntity(String.class);
                WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

                if (!jsonResponse.getResult().getCode().equals("200"))
                {
                    System.out.println(Utils.getJsonString(request));
                    System.err.println(jsonResponse);
                }
                else
                {
                    System.out.println(jsonResponse);
                }

            }

        }

        for (int p = 0; p < newNativeRegistrationList.size(); p++)
        {
            String mobileNumber1 = newNativeRegistrationList.get(p);
            List<String> account1 = commonTestUtility.signin(mobileNumber1, url);
            String bearer1 = account1.get(0);
            String firstName1 = "NC Account - " + p;
            String lastName1 = "NC Company = " + p;
            List<String> locations2 = new ArrayList<String>();
            locations2.add(locations1.get(randomLocation.nextInt(locations1.size())));
            WSResponse jsonResponse = commonTestUtility.register(firstName1, lastName1, locations2, mobileNumber1, bearer1, url);
            // for (int c1 = 0; c1 < 2; c1++)
            // {
            // commonTestUtility.signin(mobileNumber, url);
            // }

            // Block Account
            String accountId = accountList.get(randomLocation.nextInt(accountList.size()));
            commonTestUtility.callWebEndPoint(url + "accounts/" + accountId + "/block", bearer1, HTTPRequestMethodType.PUT, " ");

            // Unblock Account
            commonTestUtility.callWebEndPoint(url + "accounts/" + accountId + "/unblock", bearer1, HTTPRequestMethodType.PUT, " ");

            // Score Account
            WSRequest scoreRequest = getScoreRequest("5");
            commonTestUtility.callWebEndPoint(url + "accounts/" + accountId + "/score", bearer1, HTTPRequestMethodType.POST, Utils.getJsonString(scoreRequest));

            if (!jsonResponse.getResult().getCode().equals("200"))
            {
                System.err.println(jsonResponse);
            }
            else
            {
                System.out.println(jsonResponse);
            }
        }

        System.out.println("End: " + new Date(System.currentTimeMillis()));

        commonTestUtility.averageRegisteredTime();
    }

    private WSRequest getScoreRequest(String score)
    {
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore(score);

        request.setAccountRelations(wsAccountRelations);
        return request;
    }

    public static long generateRandomMobileNumber()
    {
        int length = 9;
        SecureRandom random = new SecureRandom();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++)
        {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

}
