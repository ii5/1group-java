package one.group.rest.v1.test;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MeasurementUnitType;
import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.core.enums.PhoneNumberType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.ApprovalStatus;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.request.WSRequestOtp;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.Association;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Media;
import one.group.entities.jpa.Otp;
import one.group.entities.jpa.PhoneNumber;
import one.group.entities.jpa.PropertyListing;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.utils.Utils;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:**applicationContext-test.xml", "classpath*:**/applicationContext-DAO-jpa-test.xml" })
public class TestUtilityWebService
{

    private static final Logger logger = LoggerFactory.getLogger(TestUtilityWebService.class);

    @Autowired
    private AccountJpaDAO accountJpaDAO;

    private WSRequest request = new WSRequest();
    private WSRequestOtp otpRequest = new WSRequestOtp("+919812312345");

    @Before
    public void setUpData()
    {

    }

    @Test
    @Transactional
    @Rollback(true)
    public void testAccountStatus() throws Exception
    {
        request.setRequestOtp(otpRequest);

        Response response = callWebEndPoint("http://localhost:8080/movein-rest/v1/request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        logger.info(response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);

        otpRequest.setMobileNumber(null);
        response = callWebEndPoint("http://localhost:8080/movein-rest/v1/request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        logger.info(response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 400);

        otpRequest.setMobileNumber("");
        response = callWebEndPoint("http://localhost:8080/movein-rest/v1/request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        logger.info(response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 400);
    }

    private Response callWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {
        ClientConfig config = new ClientConfig();
        // config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }

    private void setUpTestData()
    {
        Media[] mediaArray = new Media[20];
        PhoneNumber[] numberArray = new PhoneNumber[1500];
        Association[] associationArray = new Association[20];
        PropertyListing[] propertiesArray = new PropertyListing[1000];
        Client[] clientArray = new Client[200];
        String[] deviceType = { "Android", "iOS", "Web" };
        Set<Amenity> amenities = new HashSet<Amenity>();

        for (int i = 0; i < mediaArray.length; i++)
        {
            Media media = createMediaSeedData("Media-" + i, MediaType.IMAGE, "http://10.50.249.13/cdn/accounts/" + i + ".jpg");
            // mediaJpaDAO.persist(media);
            mediaArray[i] = media;
        }

        for (int i = 0; i < numberArray.length; i++)
        {
            PhoneNumber number = (createPhoneNumberSeedData("+91" + new Integer(1833243230 + i), PhoneNumberType.MOBILE));
            // phoneNumberJpaDAO.persist(number);
            numberArray[i] = number;
        }

        for (int i = 0; i < associationArray.length; i++)
        {
            Association association = createAssociationSeedData("Association-" + i, ApprovalStatus.ACCEPTED, numberArray[numberArray.length - (i + 1)], Status.ACTIVE);
            // associationJpaDAO.persist(association);
            associationArray[i] = association;
        }

        for (int i = 0; i < clientArray.length; i++)
        {
            Random random = new SecureRandom();
            int r = random.nextInt(3);
            Client c = createClientSeedData(deviceType[r] + "-CLI-" + i, "1", "production", deviceType[r], UUID.randomUUID().toString(), new Integer(i).toString(), "PUSH-ALERT", "PUSH-BADGE",
                    "PUSH-SOUND", DeviceStatus.ACTIVE);
            // clientJpaDAO.persist(c);
            clientArray[i] = c;
        }

        for (int i = 0; i < 30; i++)
        {
            Amenity amenity = createAmenitiesSeedData("Amenity-" + i, "Amenity-Type-" + i);
            // amenityJpaDAO.persist(amenity);
            amenities.add(amenity);
        }

        for (int i = 0; i < propertiesArray.length; i++)
        {
            PropertyListing property = createPropertySeedData(i, amenities, new Date(), "2hbhk", CommissionType.DIRECT, Currency.getInstance(Locale.getDefault()), "Description of Property " + i,
                    true,

                    false, new Location("Locality of Property " + i), MeasurementUnitType.SQFT, numberArray[numberArray.length - (i + 1)],
                    Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, 6, false), new Long("231233123"), new Long("56345453"), 328, BroadcastStatus.ACTIVE, PropertySubType.APARTMENT,
                    PropertyTransactionType.SALE, PropertyType.RESIDENTIAL);

            propertiesArray[i] = property;
        }

        Agent account1 = createAgentSeedData("Mitesh", "Chavda", "mchavda@ii5.com", AccountType.AGENT, "Always at ii5", "The Great Gujrat Establishment", numberArray[0], new Date(),
                associationArray[0], Arrays.copyOfRange(clientArray, 0, 10), Status.ACTIVE, mediaArray[0], Arrays.copyOfRange(propertiesArray, 0, 10));

        Agent account2 = createAgentSeedData("Sanil", "Shet", "sshet@ii5.com", AccountType.AGENT, "Always at ii5", "Badlapur Establishment", numberArray[1], new Date(), associationArray[1],
                Arrays.copyOfRange(clientArray, 11, 20), Status.ACTIVE, mediaArray[1], Arrays.copyOfRange(propertiesArray, 11, 20));

        Agent account3 = createAgentSeedData("Bhumika", "Bhatt", "bbhatt@ii5.com", AccountType.AGENT, "Always at ii5", "Place of Chips Establishment", numberArray[2], new Date(), associationArray[2],
                Arrays.copyOfRange(clientArray, 21, 30), Status.ACTIVE, mediaArray[2], Arrays.copyOfRange(propertiesArray, 21, 30));

        Agent account4 = createAgentSeedData("Vipul", "Vora", "vvora@ii5.com", AccountType.AGENT, "Always at ii5", "Governing Dev", numberArray[3], new Date(), associationArray[3],
                Arrays.copyOfRange(clientArray, 31, 40), Status.ACTIVE, mediaArray[3], Arrays.copyOfRange(propertiesArray, 31, 40));

        Agent account5 = createAgentSeedData("Shivraj", "Pawar", "spawar@ii5.com", AccountType.AGENT, "Always at ii5", "The Mobile Zone", numberArray[4], new Date(), associationArray[4],
                Arrays.copyOfRange(clientArray, 41, 50), Status.ACTIVE, mediaArray[4], Arrays.copyOfRange(propertiesArray, 41, 50));

        Agent account6 = createAgentSeedData("Josiah", "Adams", "jadams@ii5.com", AccountType.AGENT, "Always at ii5", "The Roots Establishment", numberArray[5], new Date(), associationArray[5],
                Arrays.copyOfRange(clientArray, 51, 60), Status.ACTIVE, mediaArray[5], Arrays.copyOfRange(propertiesArray, 51, 60));

        Agent account7 = createAgentSeedData("Ruta", "Kulkarni", "rkulkarni@ii5.com", AccountType.AGENT, "Always at ii5", "Place of Chips Establishment", numberArray[6], new Date(),
                associationArray[6], Arrays.copyOfRange(clientArray, 61, 70), Status.ACTIVE, mediaArray[6], Arrays.copyOfRange(propertiesArray, 61, 70));

        Agent account8 = createAgentSeedData("Deb", "Nayak", "bnayak@ii5.com", AccountType.AGENT, "Always at ii5", "Governing Dev", numberArray[7], new Date(), associationArray[7],
                Arrays.copyOfRange(clientArray, 71, 80), Status.ACTIVE, mediaArray[7], Arrays.copyOfRange(propertiesArray, 71, 80));

        Agent account9 = createAgentSeedData("Mahesh", "Raut", "mraut@ii5.com", AccountType.AGENT, "Always at ii5", "The Roots Establishment", numberArray[8], new Date(), associationArray[8],
                Arrays.copyOfRange(clientArray, 81, 90), Status.ACTIVE, mediaArray[8], Arrays.copyOfRange(propertiesArray, 81, 90));

        Agent account10 = createAgentSeedData("Mahesh", "Raut", "mraut123@ii5.com", AccountType.AGENT, "Always at ii5", "The Roots Establishment", numberArray[9], new Date(), associationArray[8],
                Arrays.copyOfRange(clientArray, 91, 100), Status.ACTIVE, mediaArray[9], Arrays.copyOfRange(propertiesArray, 91, 100));

        Agent[] accountArray = { account1, account2, account3, account4, account5, account6, account7, account8, account9, account10 };

        for (Agent account : accountArray)
        {
            account.addContact(account1);
            account.addContact(account2);
            account.addContact(account3);
            account.addContact(account4);
            account.addContact(account5);
            account.addContact(account6);
            account.addContact(account7);
            account.addContact(account8);
            account.addContact(account9);

        }

        accountJpaDAO.persist(account1);
    }

    private Agent createAgentSeedData(String firstname, String lastname, String email, AccountType accountType, String address, String establishmentName, PhoneNumber primaryPhoneNumber,
            Date workingSince, Association association, Client[] clients, Status status, Media accountPicture, PropertyListing[] properties)
    {
        Agent account = new Agent();

        account.setFullName(firstname);
        account.setCompanyName(lastname);
        account.setEmail(email);
        account.setType(accountType);
        account.setAddress(address);
        account.setEstablishmentName(establishmentName);
        account.setPrimaryPhoneNumber(primaryPhoneNumber);
        account.setWorkingSince(workingSince);
        account.addAssociation(association);
        account.setStatus(status);
        account.setFullPhoto(accountPicture);

        for (Client c : clients)
        {
            Otp otp = new Otp();
            // otp.setClient(c);
            otp.setOtp("1234");
            otp.setPhoneNumber(account.getPrimaryPhoneNumber());
            // otp.setExpiryTime(new Date());

            account.addClient(c);
            c.setAccount(account);
            // c.setOtp(otp);
        }

        for (PropertyListing p : properties)
        {
            account.addProperty(p);
            p.setAccount(account);
        }

        association.addAgent(account);

        return account;
    }

    private static PhoneNumber createPhoneNumberSeedData(String number, PhoneNumberType type)
    {
        PhoneNumber pNumber = new PhoneNumber();
        pNumber.setNumber(number);

        return pNumber;
    }

    private Association createAssociationSeedData(String name, ApprovalStatus approvalStatus, PhoneNumber phoneNumber, Status status)
    {
        Association association = new Association();

        association.setApprovalStatus(approvalStatus);
        association.setName(name);
        association.setPhoneNumber(phoneNumber);
        association.setStatus(status);

        return association;
    }

    private Client createClientSeedData(String appName, String appVersion, String development, String deviceModel, String deviceToken, String deviceVersion, String pushAlert, String pushBadge,
            String pushSound, DeviceStatus status)
    {
        Client client = new Client();

        client.setAppName(appName);
        client.setAppVersion(appVersion);
        client.setDevelopment(development);
        client.setDeviceModel(deviceModel);
        client.setDeviceToken(deviceToken);
        client.setDeviceVersion(deviceVersion);
        client.setPushAlert(pushAlert);
        client.setPushBadge(pushBadge);
        client.setPushSound(pushSound);
        client.setPushChannel(client.getIdAsString());
        client.setStatus(status);

        return client;
    }

    private Media createMediaSeedData(String name, MediaType type, String url)
    {
        Media media = new Media();
        media.setName(name);
        media.setDimension(MediaDimensionType.FULL);
        media.setType(type);
        media.setUrl(url);
        return media;
    }

    private PropertyListing createPropertySeedData(int age, Set<Amenity> amenities, Date availableDate, String bhk, CommissionType commisionSplit, Currency currencyUnit, String description,
            boolean isAvailable, boolean isHot, Location locality, MeasurementUnitType measurementUnit, PhoneNumber phoneNumber, String referenceId, long rentPrice, long salePrice, int size,
            BroadcastStatus status, PropertySubType subType, PropertyTransactionType transactionType, PropertyType type)
    {
        PropertyListing p = new PropertyListing();

        // p.setAmenities(amenities);
        p.setRooms(bhk);
        p.setCommissionType(commisionSplit);
        p.setDescription(description);
        p.setIsHot(isHot);
        p.setLocation(locality);

        p.setShortReference(referenceId);
        p.setRentPrice(rentPrice);
        p.setSalePrice(salePrice);
        p.setArea(size);
        p.setStatus(status);
        p.setSubType(subType);
        p.setType(type);

        // for (Amenity a : amenities)
        // {
        // a.addProperty(p);
        // }
        return p;
    }

    private Amenity createAmenitiesSeedData(String name, String type)
    {
        Amenity a = new Amenity();
        a.setName(name);
        a.setType(type);

        return a;
    }

}
