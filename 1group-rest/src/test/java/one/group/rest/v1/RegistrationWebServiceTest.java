package one.group.rest.v1;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

@ContextConfiguration("/applicationContext-configuration.xml")
public class RegistrationWebServiceTest
{
    private static String url = "http://10.50.249.127:8080/movein-rest/v1/";
    private static String mobileNumber = "+919610427694";
    private static String otherMobileNumber = "+919608427614";
    CommonTestUtility commonTestUtility = new CommonTestUtility();

    @Test
    public void testNoAccessTokenProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");

        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);

        Response response = commonTestUtility.callWebEndPoint(url + "registration/", "", HTTPRequestMethodType.POST, Utils.getJsonString(account));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
    }

    @Test
    public void testInvalidAccessTokenRegistration() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration/", "bearer ssss", HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        assertEquals(code, "401");
    }

    @Test
    public void testValueOfAccountNullRegistration() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();

        Response response = commonTestUtility.callWebEndPoint(url + "registration/", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");

    }

    @Test
    public void testNoFirstNameProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    @Test
    public void testNoLastNameProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    @Test
    public void testNoLocationsProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();

        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    @Test
    public void testInvalidLocationsProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592S");
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    @Test
    public void testExecedsLocationsProvided() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");
        locations.add("394");
        locations.add("3272");
        locations.add("877");
        locations.add("2276");
        locations.add("3621");
        locations.add("2663");
        locations.add("5379");
        locations.add("2541");
        locations.add("2481");
        locations.add("1637");

        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    @Test
    public void testRegistratingAlreadyRegisteredAccount() throws Exception
    {
        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");

        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);
        Response response1 = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        Response response = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        System.out.println(jsonResponse);
        String code = jsonResponse.getResult().getCode();
        Boolean isFatal = jsonResponse.getResult().getIsFatal();
        assertEquals(code, "400");
    }

    private void testUploadProfileImage() throws Exception
    {

        List<String> siginResponse = commonTestUtility.signin(mobileNumber, url);

        List<String> locations = new ArrayList<String>();
        locations.add("3592");

        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Sanil");
        account.setCompanyName("Sheth");
        account.setLocations(locations);
        request.setAccount(account);

        Response response1 = commonTestUtility.callWebEndPoint(url + "registration", siginResponse.get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));

    }

    private void generateRandom10DigitNumber(long range)
    {
        if (range > 10)
        {
            throw new IllegalStateException("Too many digits");
        }

        long Leng = (long) Math.pow(10, range - 1) * 9;

    }

}
