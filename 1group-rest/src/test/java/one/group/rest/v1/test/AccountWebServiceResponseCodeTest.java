package one.group.rest.v1.test;

import static one.group.rest.v1.test.TestUtils.getCommaSeparatedString;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import javax.ws.rs.core.Response;

import one.group.cache.dao.AccountCacheDAO;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSAccountRelations;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.utils.Utils;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-DAO-test.xml", "classpath:applicationContext-DAO-cache-test.xml", "classpath:applicationContext-configuration-Cache-test.xml" })
@ComponentScan
public class AccountWebServiceResponseCodeTest
{
    private static CommonTestUtility commonTestUtility = new CommonTestUtility();
    private static String BASE_URL = "http://localhost:8081/1group/v2/";

    private static Map<String, List<String>> signinInfoMap = new HashMap<String, List<String>>();
    private static List<String> locations = new ArrayList<String>();
    private static String[] phoneNumbers = { "+919000000001", "+919000000002", "+919000000003", "+919000000004", "+919000000005" };

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    private AccountCacheDAO accountCacheDAO;

    @Autowired
    private AccountJpaDAO accountJpaDAO;

    public DataSource getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    @BeforeClass
    public static void setTestData() throws JsonMappingException, JsonGenerationException, IOException
    {
        locations.clear();
        locations.add("1");
        // Unregistered
        signinInfoMap.put(phoneNumbers[0], commonTestUtility.signin(phoneNumbers[0], BASE_URL));
        signinInfoMap.put(phoneNumbers[1], commonTestUtility.signin(phoneNumbers[1], BASE_URL));

        // Registered
        signinInfoMap.put(phoneNumbers[2], commonTestUtility.signin(phoneNumbers[2], BASE_URL));
        signinInfoMap.put(phoneNumbers[3], commonTestUtility.signin(phoneNumbers[3], BASE_URL));
        signinInfoMap.put(phoneNumbers[4], commonTestUtility.signin(phoneNumbers[4], BASE_URL));

        commonTestUtility.register("second", "", locations, phoneNumbers[2], signinInfoMap.get(phoneNumbers[2]).get(0), BASE_URL);
        commonTestUtility.register("third", "", locations, phoneNumbers[3], signinInfoMap.get(phoneNumbers[3]).get(0), BASE_URL);
        commonTestUtility.register("fourth", "", locations, phoneNumbers[4], signinInfoMap.get(phoneNumbers[4]).get(0), BASE_URL);

    }

    public void tearDown()
    {
        for (String phoneNumber : phoneNumbers)
        {
            String accountId = signinInfoMap.get(phoneNumber).get(1);
            deleteAccount(accountId);
        }
    }

    public void deleteAccount(String accountId)
    {
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        jdbcTemplateObject.execute("delete from oauth_authorization where client_id in(select id from client where account_id='" + accountId + "')");
        jdbcTemplateObject.execute("delete from oauth_access_token where client_id in(select id from client where account_id='" + accountId + "')");
        jdbcTemplateObject.execute("delete from client where account_id='" + accountId + "'");
        jdbcTemplateObject.execute("delete from account_location where account_id='" + accountId + "'");
        jdbcTemplateObject.execute("delete from account_relations where account_id='" + accountId + "'");
        jdbcTemplateObject.execute("delete  from account where id='" + accountId + "'");
        accountCacheDAO.updateCount(Long.parseLong(accountCacheDAO.fetchCount()) - 1);
    }

    // START FETCH ACCOUNT API /account/fetch GET REQUEST TESTCASES

    @Test
    public void testResponseForFetchAccountsForRegistered() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String[] accountIds = { signinInfoMap.get(phoneNumbers[3]).get(1), signinInfoMap.get(phoneNumbers[4]).get(1) };
        String accountIdsStringSeparated = getCommaSeparatedString(accountIds);
        String url = "accounts/" + accountIdsStringSeparated;
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("20003", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForFetchAccountsForUnRegistered() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String[] accountIds = { signinInfoMap.get(phoneNumbers[0]).get(1), signinInfoMap.get(phoneNumbers[1]).get(1) };
        String accountIdsStringSeparated = getCommaSeparatedString(accountIds);
        String url = "accounts/" + accountIdsStringSeparated;
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[0]).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("1004", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForFetchAccountsForInvalidAccountId() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountIdsStringSeparated = "abcd,efgh";
        String url = "accounts/" + accountIdsStringSeparated;
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("6006", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForFetchAccountsForUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String[] accountIds = { signinInfoMap.get(phoneNumbers[3]).get(1) };
        String accountIdsStringSeparated = getCommaSeparatedString(accountIds);
        String url = "accounts/" + accountIdsStringSeparated;
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("9013", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForFetchAccountsForAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String[] accountIds = { signinInfoMap.get(phoneNumbers[3]).get(1) };
        String accountIdsStringSeparated = getCommaSeparatedString(accountIds);
        String url = "accounts/" + accountIdsStringSeparated;
        Response response = commonTestUtility.callWebEndPoint(BASE_URL.replace("v2", "v3") + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("9009", jsonResponse.getResult().getCode());
        tearDown();
    }

    // END FETCH ACCOUNT API /account/fetch GET REQUEST TESTCASES

    // START REGISTRATION API /registration POST REQUEST TESTCASES

    @Test
    public void testResponseForRegisteration() throws JsonMappingException, JsonGenerationException, IOException
    {
        locations.add("1");
        List<String> signinInfo = commonTestUtility.signin("+919000000010", BASE_URL);
        WSResponse registrationResponse = commonTestUtility.register("Tester", "Dev", locations, "+919000000010", signinInfo.get(0), BASE_URL);
        assertEquals("20002", registrationResponse.getResult().getCode());
        deleteAccount(signinInfo.get(1));
    }

    @Test
    public void testResponseForRegisterationForUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        locations.add("1");
        List<String> signinInfo = commonTestUtility.signin("+919000000010", BASE_URL);
        WSResponse registrationResponse = commonTestUtility.register("Tester", "Dev", locations, "+919000000010", null, BASE_URL);
        assertEquals("9013", registrationResponse.getResult().getCode());
        deleteAccount(signinInfo.get(1));
    }

    @Test
    public void testResponseForRegisterationForBadRequest() throws JsonMappingException, JsonGenerationException, IOException
    {
        locations.add("100000000000000");
        List<String> signinInfo = commonTestUtility.signin("+919000000010", BASE_URL);
        WSResponse registrationResponse = commonTestUtility.register("Tester", "Dev", locations, "+919000000010", signinInfo.get(0), BASE_URL);
        assertEquals("8001", registrationResponse.getResult().getCode());
        // deleteAccount(signinInfo.get(1));
    }

    @Test
    public void testResponseForRegisterationForAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        locations.add("1");
        List<String> signinInfo = commonTestUtility.signin("+919000000010", BASE_URL);
        WSResponse registrationResponse = commonTestUtility.register("Tester", "Dev", locations, "+919000000010", signinInfo.get(0), BASE_URL.replace("v2", "v3"));
        assertEquals("9009", registrationResponse.getResult().getCode());
        deleteAccount(signinInfo.get(1));
    }

    // END REGISTRATION API /registration POST REQUEST TESTCASES

    // START EDIT Account API /accounts/1234 POST REQUEST TESTCASES

    @Test
    public void testResponseForEditAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId;
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Tester");
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("20004", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForEditAccountForUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId;
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Tester");
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("9013", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForEditAccountForAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId;
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Tester");
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL.replace("v2", "v3") + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("9009", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForEditAccountForBadRequest() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId;
        WSRequest request = new WSRequest();
        WSAccount account = null;
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("6001", jsonResponse.getResult().getCode());
        tearDown();
    }

    @Test
    public void testResponseForEditAccountForInvalidAccountId() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String url = "accounts/wrongformatid";
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName("Tester");
        request.setAccount(account);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("1001", jsonResponse.getResult().getCode());
        tearDown();
    }

    // END EDIT ACCOUNT API /accounts/{account_id} POST REQUEST TESTCASES

    // START BLOCK Account API /accounts/{account_id}/block PUT REQUEST
    // TESTCASES

    // @Test
    public void testResponseForBlockAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[3]).get(1);
        String url = "accounts/" + accountId + "/block";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("200", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForBlockSameAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/block";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("400", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForBlockUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/block";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("401", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForBlockBadRequest() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = "1234";
        String url = "accounts/" + accountId + "/block";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("404", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForBlockAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[3]).get(1);
        String url = "accounts/" + accountId + "/block";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL.replace("v1", "v2") + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("301", jsonResponse.getResult().getCode());
        tearDown();
    }

    // END BLOCK Account API /accounts/{account_id}/block PUT REQUEST
    // TESTCASES

    // START UNBLOCK Account API /accounts/{account_id}/unblock PUT REQUEST
    // TESTCASES

    // @Test
    public void testResponseForUnBlockAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        // start block action
        String accountId = signinInfoMap.get(phoneNumbers[3]).get(1);
        commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + accountId + "/block", signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        // end block action
        String url = "accounts/" + accountId + "/unblock";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("200", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForUnBlockSameAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/unblock";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("400", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForUnBlockUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/unblock";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("401", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForUnBlockBadRequest() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = "1234";
        String url = "accounts/" + accountId + "/unblock";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("404", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForUnBlockAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[3]).get(1);
        String url = "accounts/" + accountId + "/unblock";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL.replace("v1", "v2") + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.PUT, "");
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("301", jsonResponse.getResult().getCode());
        tearDown();
    }

    // END UNBLOCK Account API /accounts/{account_id}/unblock PUT REQUEST
    // TESTCASES

    // START SCORE Account API /accounts/{account_id}/score POST REQUEST
    // TESTCASES

    // @Test
    public void testResponseForScoreAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/score";
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore("1");
        request.setAccountRelations(wsAccountRelations);
        String jsonRequest = Utils.getJsonString(request);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[3]).get(0), HTTPRequestMethodType.POST, jsonRequest);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("200", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForScoreSameAccount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/score";
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore("1");
        request.setAccountRelations(wsAccountRelations);
        String jsonRequest = Utils.getJsonString(request);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.POST, jsonRequest);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("400", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForScoreUnauthorized() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[2]).get(1);
        String url = "accounts/" + accountId + "/score";
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore("1");
        request.setAccountRelations(wsAccountRelations);
        String jsonRequest = Utils.getJsonString(request);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.POST, jsonRequest);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("401", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForScoreBadRequest() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = "1234";
        String url = "accounts/" + accountId + "/score";
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore("1");
        request.setAccountRelations(wsAccountRelations);
        String jsonRequest = Utils.getJsonString(request);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.POST, jsonRequest);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("404", jsonResponse.getResult().getCode());
        tearDown();
    }

    // @Test
    public void testResponseForScoreAdvancedVersion() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String accountId = signinInfoMap.get(phoneNumbers[3]).get(1);
        String url = "accounts/" + accountId + "/score";
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore("1");
        request.setAccountRelations(wsAccountRelations);
        String jsonRequest = Utils.getJsonString(request);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL.replace("v1", "v2") + url, signinInfoMap.get(phoneNumbers[2]).get(0), HTTPRequestMethodType.POST, jsonRequest);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("301", jsonResponse.getResult().getCode());
        tearDown();
    }

    // END SCORE Account API /accounts/{account_id}/score POST REQUEST
    // TESTCASES

    // @Test
    public void testResponseForFetchAccountsCount() throws JsonMappingException, JsonGenerationException, IOException
    {
        setTestData();
        String url = "accounts/count";
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + url, null, HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        assertEquals("200", jsonResponse.getResult().getCode());
        tearDown();
    }

}
