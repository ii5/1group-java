package one.group.rest.v1.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import one.group.core.enums.EntityType;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.dao.SyncUpdateDAO;
import one.group.entities.api.request.WSAccountRelations;
import one.group.entities.api.request.WSAddPropertyListing;
import one.group.entities.api.request.WSContacts;
import one.group.entities.api.request.WSNativeContact;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSAccountsResult;
import one.group.entities.api.response.WSNewsFeed;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSSyncUpdate;
import one.group.entities.jpa.SyncUpdate;
import one.group.utils.Utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-DAO.xml", "classpath:applicationContext-DAO-jpa.xml", "classpath:applicationContext-configuration-Cache.xml" })
public class AccountRelationsWebServiceTest
{
    // String BASE_URL = "http://localhost:8079/movein-rest/v1/";
    String BASE_URL = "http://localhost:8081/movein-rest/v1/";
    // String BASE_URL = "http://10.50.249.127:8080/movein-rest/v1/";

    public final long baseNumberConstant = 919870000000l;
    long baseNumber = 919870000000l;
    final int syncBatchSize = 7;
    String STATUS_REGISTERED = "STATUS_REGISTERED";
    String STATUS_UNREGISTERED = "STATUS_UNREGISTERED";

    CommonTestUtility commonTestUtility = new CommonTestUtility();

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Autowired
    private SyncUpdateDAO syncUpdateDao;

    public DataSource getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    private List<String> getSyncUpdate(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        String accountId = CommonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(1);
        List<String> receiverAccountIds = new ArrayList<String>();
        List<SyncUpdate> syncUpdates = syncUpdateDao.fetchByAccount(accountId);
        for (SyncUpdate syncUpdate : syncUpdates)
        {
            Map syncUpdatesMap = (Map) Utils.getInstanceFromJson(Utils.getJsonString(syncUpdate), Map.class);
            if (syncUpdatesMap.get("update") != null)
            {
                WSSyncUpdate syncUpdatesMap2 = (WSSyncUpdate) Utils.getInstanceFromJson(syncUpdatesMap.get("update").toString(), WSSyncUpdate.class);
                if (syncUpdatesMap2.getEntity() != null && syncUpdatesMap2.getEntity().getNamespace().equals(EntityType.ACCOUNT_RELATION))
                {
                    receiverAccountIds.add(syncUpdatesMap2.getEntity().getId());
                }
            }

        }
        return receiverAccountIds;
    }

    private List<String> getSyncUpdateByAccountId(String accountId) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> receiverAccountIds = new ArrayList<String>();
        List<SyncUpdate> syncUpdates = syncUpdateDao.fetchByAccount(accountId);
        for (SyncUpdate syncUpdate : syncUpdates)
        {
            Map syncUpdatesMap = (Map) Utils.getInstanceFromJson(Utils.getJsonString(syncUpdate), Map.class);
            if (syncUpdatesMap.get("update") != null)
            {
                WSSyncUpdate syncUpdatesMap2 = (WSSyncUpdate) Utils.getInstanceFromJson(syncUpdatesMap.get("update").toString(), WSSyncUpdate.class);
                if (syncUpdatesMap2.getEntity() != null && syncUpdatesMap2.getEntity().getNamespace().equals(EntityType.ACCOUNT_RELATION))
                {
                    receiverAccountIds.add(syncUpdatesMap2.getEntity().getId());
                }
            }

        }
        return receiverAccountIds;
    }

    public String getAccountDetails(String accountId, String token, boolean blockUnblock) throws JsonMappingException, JsonGenerationException, IOException
    {
        String returnData = "";
        // System.out.println(">>" + BASE_URL + "accounts/" + accountId);
        // System.out.println(">>" + token);
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + accountId, token, HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);

        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        WSAccountsResult accountResult = (WSAccountsResult) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSAccountsResult.class);
        if (accountResult != null)
        {
            Set<WSAccount> wsAccounts = accountResult.getAccountsResult();
            WSAccount account = wsAccounts.iterator().next();
            if (account != null && account.getIsBlocked() != null)
            {
                if (blockUnblock && account.getIsBlocked())
                    returnData = account.getId();
                else if (blockUnblock && !account.getIsBlocked())
                    returnData = account.getId();
            }

            if (!blockUnblock)
                returnData = String.valueOf(account.getScore());
        }
        return returnData;
    }

    public List<String> fetchSyncContacts(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> contacts = new ArrayList<String>();
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + "contacts", CommonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        List<Map> dataList = (List<Map>) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), List.class);
        for (Map data : dataList)
        {
            if ((Boolean) data.get("is_contact"))
                contacts.add((String) data.get("contact_id"));
        }
        return contacts;
    }

    @Transactional
    public void postPropertyListing(List<String> phoneNumbers)
    {
        WSRequest request = new WSRequest();
        WSAddPropertyListing requestPropertyListing = new WSAddPropertyListing();

        requestPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString().toLowerCase());
        requestPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        requestPropertyListing.setArea("111");
        requestPropertyListing.setCommissionType("direct");
        requestPropertyListing.setDescription("description");
        requestPropertyListing.setRooms("4bhk");
        requestPropertyListing.setSalePrice("2222222");
        requestPropertyListing.setRentPrice("4444");
        requestPropertyListing.setLocationId("394");
        requestPropertyListing.setPropertyMarket("secondary");

        request.setPropertyListing(requestPropertyListing);

        for (String phoneNumber : phoneNumbers)
        {
            commonTestUtility.callWebEndPoint(BASE_URL + "property_listings", commonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(0), HTTPRequestMethodType.POST,
                    Utils.getJsonString(request));
        }
    }

    public List<String> getNewsFeedAccountList(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> receivingAccountIds = new ArrayList<String>();
        Response response = commonTestUtility.callWebEndPoint(BASE_URL + "news_feed", CommonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(0), HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        Map result = (Map) jsonResponse.getData();
        Object resultObj = result.get("news_feed");
        String jsonString = Utils.getJsonString(resultObj);
        List newsFeedList = (List) Utils.getInstanceFromJson(jsonString, List.class);
        for (Object newsFeed : newsFeedList)
        {
            WSNewsFeed wsNewsFeed = (WSNewsFeed) Utils.getInstanceFromJson(Utils.getJsonString(newsFeed), WSNewsFeed.class);
            receivingAccountIds.add(wsNewsFeed.getAccountId());

        }
        return receivingAccountIds;
    }

    public void init(int size) throws JsonMappingException, JsonGenerationException, IOException
    {
        baseNumber = 919870000000l;
        CommonTestUtility.getPhoneBearerMapper().clear();
        for (int indx = 0; indx <= size; indx++)
        {
            String phoneNumber = "+" + baseNumber;
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber, commonTestUtility.signin(phoneNumber, BASE_URL));
            baseNumber++;
        }
        register("+" + String.valueOf(baseNumberConstant));

    }

    public void register(List<String> phoneNumbers) throws JsonMappingException, JsonGenerationException, IOException
    {
        for (String phoneNumber : phoneNumbers)
        {
            List<String> property = new ArrayList<String>();
            property.add("3592");
            commonTestUtility.register("fname", "lname", property, phoneNumber, CommonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(0), BASE_URL);
        }

    }

    public void register(String phoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> property = new ArrayList<String>();
        property.add("3592");
        commonTestUtility.register("fname", "lname", property, phoneNumber, CommonTestUtility.getPhoneBearerMapper().get(phoneNumber).get(0), BASE_URL);
    }

    private WSNativeContact getNativeContact(String firstName, String lastName, List<String> phoneNumbers)
    {

        WSNativeContact nativeContact = new WSNativeContact();
        nativeContact.setFullName(firstName);
        nativeContact.setCompanyName(lastName);
        List<String> numbers = new ArrayList<String>();
        for (int i = 0; i < phoneNumbers.size(); i++)
        {
            numbers.add(phoneNumbers.get(i));
        }
        nativeContact.setNumbers(numbers);
        return nativeContact;

    }

    private void verifyBlock(Map<String, List<String>> blockedData) throws JsonMappingException, JsonGenerationException, IOException
    {
        if (blockedData.size() > 0)
        {
            String blockingKey = blockedData.keySet().iterator().next();
            List<String> blockedPhoneNumbers = blockedData.get(blockingKey);
            List<String> blockedActualResults = new ArrayList<String>();
            for (String blockedPhoneNumber : blockedPhoneNumbers)
            {
                String actualAccountId = getAccountDetails(CommonTestUtility.getPhoneBearerMapper().get(blockedPhoneNumber).get(1), CommonTestUtility.getPhoneBearerMapper().get(blockingKey).get(0),
                        true);
                if (!actualAccountId.isEmpty())
                    blockedActualResults.add(actualAccountId);
            }

            List<String> expectedBlockList = getExpectedResultsBlockList(blockedPhoneNumbers);
            Collections.sort(expectedBlockList);
            Collections.sort(blockedActualResults);
            Assert.assertArrayEquals(expectedBlockList.toArray(), blockedActualResults.toArray());
            assertEquals(expectedBlockList.size(), blockedActualResults.size());

            String blockerPhoneNumber = "+" + baseNumberConstant;
            List<String> actualSyncUpdateBlockList = getSyncUpdate(blockerPhoneNumber);

            if (!expectedBlockList.isEmpty())
                Assert.assertTrue(actualSyncUpdateBlockList.contains(CommonTestUtility.getPhoneBearerMapper().get(blockerPhoneNumber).get(1)));

        }

    }

    private String getUnBlockDetails(String unblockedPhoneNumber) throws JsonMappingException, JsonGenerationException, IOException
    {
        String unBlocked = "";
        List<String> contacts = fetchSyncContacts("+" + baseNumberConstant);
        String unBlockedAccountId = CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(1);
        String token = CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(0);
        if (contacts.contains(unBlockedAccountId))
        {
            unBlocked = getAccountDetails(unBlockedAccountId, token, true);
        }

        return unBlocked;
    }

    private void verifyUnBlock(Map<String, List<String>> unblockedData) throws JsonMappingException, JsonGenerationException, IOException
    {
        if (unblockedData.size() > 0)
        {
            String unblockingKey = unblockedData.keySet().iterator().next();
            List<String> unblockedPhoneNumbers = unblockedData.get(unblockingKey);
            List<String> unblockedActualResults = new ArrayList<String>();
            for (String unblockedPhoneNumber : unblockedPhoneNumbers)
            {
                String actualAccountId = getUnBlockDetails(unblockedPhoneNumber);
                if (!actualAccountId.isEmpty())
                    unblockedActualResults.add(actualAccountId);
            }

            List<String> expectedUnblockList = getExpectedResultsUnBlockList(unblockedPhoneNumbers);
            Collections.sort(expectedUnblockList);
            Collections.sort(unblockedActualResults);
            Assert.assertArrayEquals(expectedUnblockList.toArray(), unblockedActualResults.toArray());
            assertEquals(expectedUnblockList.size(), unblockedActualResults.size());

            String unblockerPhoneNumber = "+" + baseNumberConstant;
            List<String> actualSyncUpdateUnBlockList = getSyncUpdate(unblockerPhoneNumber);
            if (!expectedUnblockList.isEmpty())
                Assert.assertTrue(actualSyncUpdateUnBlockList.contains(CommonTestUtility.getPhoneBearerMapper().get(unblockerPhoneNumber).get(1)));

        }

    }

    private void verifyScore(Map<String, List<String>> scoredData) throws JsonMappingException, JsonGenerationException, IOException
    {
        if (scoredData.size() > 0)
        {
            String scoringKey = scoredData.keySet().iterator().next();
            List<String> scoredPhoneNumbers = scoredData.get(scoringKey);
            for (String scoredPhoneNumber : scoredPhoneNumbers)
            {
                if (STATUS_REGISTERED.equals(CommonTestUtility.getPhoneBearerMapper().get(scoredPhoneNumber).get(2)))
                {
                    String score = getAccountDetails(CommonTestUtility.getPhoneBearerMapper().get(scoredPhoneNumber).get(1), CommonTestUtility.getPhoneBearerMapper().get(scoringKey).get(0), false);
                    assertEquals("5", score);
                    String scorerPhoneNumber = "+" + baseNumberConstant;
                    List<String> actualSyncUpdateScoreList = getSyncUpdate(scorerPhoneNumber);
                    Assert.assertTrue(actualSyncUpdateScoreList.contains(CommonTestUtility.getPhoneBearerMapper().get(scorerPhoneNumber).get(1)));
                }

            }
        }

    }

    private void verifyBlockUnBlockScore(Map<String, List<String>> blockedData, Map<String, List<String>> unblockedData, Map<String, List<String>> scoredData, boolean verifyBlock,
            boolean verifyUnblock, boolean verifyScore) throws JsonMappingException, JsonGenerationException, IOException
    {
        if (verifyBlock)
            verifyBlock(blockedData);
        if (verifyUnblock)
            verifyUnBlock(unblockedData);
        if (verifyScore)
            verifyScore(scoredData);

    }

    private void verifySyncResults(Map<String, Set<String>> syncData, List<String> propertyPosterPhoneNumbers) throws JsonMappingException, JsonGenerationException, IOException
    {
        try
        {
            Thread.sleep(20000);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        Set<String> phoneNumbers = syncData.keySet();
        List<String> expectedContacts = getExpectedResultsContactList(syncData);
        List<String> expectedNewsFeeds = getExpectedResultsNewsFeedList(syncData, propertyPosterPhoneNumbers);
        List<String> contacts = new ArrayList<String>();
        List<String> syncUpdates = new ArrayList<String>();
        List<String> newsFeeds = new ArrayList<String>();

        System.out.println("phoneNumbers>>" + phoneNumbers);
        for (String phoneNumber : phoneNumbers)
        {
            List<String> fetchContacts = fetchSyncContacts(phoneNumber);
            List<String> fetchNewsFeeds = getNewsFeedAccountList(phoneNumber);
            List<String> fetchSyncUpdates = getSyncUpdate(phoneNumber);

            contacts.addAll(fetchContacts);
            newsFeeds.addAll(fetchNewsFeeds);
            syncUpdates.addAll(fetchSyncUpdates);
        }
        Collections.sort(expectedContacts);
        Collections.sort(contacts);
        Assert.assertArrayEquals(expectedContacts.toArray(), contacts.toArray());
        assertEquals(expectedContacts.size(), contacts.size());

        Collections.sort(expectedNewsFeeds);
        Collections.sort(newsFeeds);
        System.out.println("expectedNewsFeeds " + expectedNewsFeeds);
        System.out.println("newsFeeds " + newsFeeds);
        System.out.println(CommonTestUtility.getPhoneBearerMapper());
        Assert.assertArrayEquals(expectedNewsFeeds.toArray(), newsFeeds.toArray());
        assertEquals(expectedNewsFeeds.size(), newsFeeds.size());

        Collections.sort(expectedContacts);
        Collections.sort(syncUpdates);
        Assert.assertArrayEquals(expectedContacts.toArray(), syncUpdates.toArray());
        assertEquals(expectedContacts.size(), syncUpdates.size());

    }

    public List<String> getExpectedResultsContactList(Map<String, Set<String>> syncData)
    {
        List<String> expectedResults = new ArrayList<String>();
        Set<String> phoneNumbers = syncData.keySet();
        for (String syncingPhoneNumber : phoneNumbers)
        {
            Set<String> syncedPhoneNumbers = syncData.get(syncingPhoneNumber);
            String syncingAccountId = CommonTestUtility.getPhoneBearerMapper().get(syncingPhoneNumber).get(1);
            String syncingStatus = CommonTestUtility.getPhoneBearerMapper().get(syncingPhoneNumber).get(2);
            for (String syncedPhoneNumber : syncedPhoneNumbers)
            {
                String syncedAccountId = CommonTestUtility.getPhoneBearerMapper().get(syncedPhoneNumber).get(1);
                String syncedStatus = CommonTestUtility.getPhoneBearerMapper().get(syncedPhoneNumber).get(2);
                if (syncingStatus.equals(STATUS_REGISTERED) && syncedStatus.equals(STATUS_REGISTERED))
                {
                    if (!syncingAccountId.equals(syncedAccountId))
                        expectedResults.add(syncedAccountId);
                }
            }
        }
        return expectedResults;
    }

    public List<String> getExpectedResultsBlockList(List<String> blockedList)
    {
        List<String> expectedResults = new ArrayList<String>();
        for (String blockedPhoneNumber : blockedList)
        {
            if (CommonTestUtility.getPhoneBearerMapper().get(blockedPhoneNumber).get(2).equals(STATUS_REGISTERED))
                expectedResults.add(CommonTestUtility.getPhoneBearerMapper().get(blockedPhoneNumber).get(1));

        }
        return expectedResults;
    }

    public List<String> getExpectedResultsUnBlockList(List<String> unblockedList) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> contacts = fetchSyncContacts("+" + baseNumberConstant);
        List<String> expectedResults = new ArrayList<String>();
        for (String unblockedPhoneNumber : unblockedList)
        {
            if (CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(2).equals(STATUS_REGISTERED)
                    && contacts.contains(CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(1)))
                expectedResults.add(CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(1));

        }
        return expectedResults;
    }

    public List<String> getExpectedResultsNewsFeedList(Map<String, Set<String>> syncData, List<String> propertyPosterPhoneNumbers)
    {
        List<String> propertyPosterAccountIds = new ArrayList<String>();
        for (String propertyPosterPhoneNumber : propertyPosterPhoneNumbers)
        {
            String propertyPosterAccountId = CommonTestUtility.getPhoneBearerMapper().get(propertyPosterPhoneNumber).get(1);
            propertyPosterAccountIds.add(propertyPosterAccountId);
        }

        List<String> expectedResults = new ArrayList<String>();
        Set<String> phoneNumbers = syncData.keySet();
        for (String syncingPhoneNumber : phoneNumbers)
        {
            Set<String> syncedPhoneNumbers = syncData.get(syncingPhoneNumber);
            String syncingAccountId = CommonTestUtility.getPhoneBearerMapper().get(syncingPhoneNumber).get(1);
            String syncingStatus = CommonTestUtility.getPhoneBearerMapper().get(syncingPhoneNumber).get(2);
            for (String syncedPhoneNumber : syncedPhoneNumbers)
            {
                String syncedAccountId = CommonTestUtility.getPhoneBearerMapper().get(syncedPhoneNumber).get(1);
                String syncedStatus = CommonTestUtility.getPhoneBearerMapper().get(syncedPhoneNumber).get(2);
                if (syncingStatus.equals(STATUS_REGISTERED) && syncedStatus.equals(STATUS_REGISTERED))
                {
                    if (!syncingAccountId.equals(syncedAccountId) && propertyPosterAccountIds.contains(syncedAccountId))
                        expectedResults.add(syncedAccountId);
                }
            }
        }
        return expectedResults;
    }

    // Sync contacts with following sequence
    // 1) Register account 1,2,3
    // 2) Account 1 syncing contact of 1 to 10
    // Sequence register then sync
    public Map<String, List<String>> preSetIt(int initCount, int registerRange[], int propertyPosterRange[], int syncerRange[], int blockedRange[], int unblockedRange[], int scoredRange[])
            throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> syncerPhoneNumbers = new ArrayList<String>();
        List<String> propertyPosterPhoneNumbers = new ArrayList<String>();
        List<String> blockedPhoneNumbers = new ArrayList<String>();
        List<String> unblockedPhoneNumbers = new ArrayList<String>();
        List<String> scoredPhoneNumbers = new ArrayList<String>();
        init(initCount);
        int registerFrom = registerRange[0];
        int registerTo = registerRange[1];
        Map<String, List<String>> syncersPostersBlockUnblockedScored = new HashMap<String, List<String>>();

        for (int indx = registerFrom; indx <= registerTo; indx++)
        {
            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            register(baseNumberStr);
        }

        int propertyPosterFrom = propertyPosterRange[0];
        int propertyPosterTo = propertyPosterRange[1];
        for (int indx = propertyPosterFrom; indx <= propertyPosterTo; indx++)
        {
            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            propertyPosterPhoneNumbers.add(baseNumberStr);
        }
        postPropertyListing(propertyPosterPhoneNumbers);
        syncersPostersBlockUnblockedScored.put("posters", propertyPosterPhoneNumbers);

        int syncerFrom = syncerRange[0];
        int syncerTo = syncerRange[1];
        for (int indx = syncerFrom; indx <= syncerTo; indx++)
        {

            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            syncerPhoneNumbers.add(baseNumberStr);
        }
        syncersPostersBlockUnblockedScored.put("syncers", syncerPhoneNumbers);

        int blockedFrom = blockedRange[0];
        int blockedTo = blockedRange[1];
        for (int indx = blockedFrom; indx <= blockedTo; indx++)
        {
            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            blockedPhoneNumbers.add(baseNumberStr);
        }
        syncersPostersBlockUnblockedScored.put("blocked", blockedPhoneNumbers);

        int unblockedFrom = unblockedRange[0];
        int unblockedTo = unblockedRange[1];
        for (int indx = unblockedFrom; indx <= unblockedTo; indx++)
        {
            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            unblockedPhoneNumbers.add(baseNumberStr);
        }
        syncersPostersBlockUnblockedScored.put("unblocked", unblockedPhoneNumbers);

        int scoredFrom = scoredRange[0];
        int scoredTo = scoredRange[1];
        for (int indx = scoredFrom; indx <= scoredTo; indx++)
        {
            String baseNumberStr = String.valueOf(baseNumberConstant);
            baseNumberStr = "+" + Long.parseLong(baseNumberStr.substring(0, baseNumberStr.length() - String.valueOf(indx).length())) + indx;
            scoredPhoneNumbers.add(baseNumberStr);
        }
        syncersPostersBlockUnblockedScored.put("scored", scoredPhoneNumbers);

        return syncersPostersBlockUnblockedScored;

    }

    private WSRequest getSyncRequest(List<String> phoneNumberBatch, boolean isLast)
    {
        WSRequest request = new WSRequest();
        List<WSNativeContact> wsNativeContacts = new ArrayList<WSNativeContact>();
        WSContacts contacts = new WSContacts();
        wsNativeContacts.add(getNativeContact("fname", "lname", phoneNumberBatch));
        contacts.setInitialContactsSyncComplete(isLast);
        contacts.setNativeContacts(wsNativeContacts);
        request.setContacts(contacts);
        return request;
    }

    private WSRequest getScoreRequest(String score)
    {
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore(score);

        request.setAccountRelations(wsAccountRelations);
        return request;
    }

    @Transactional
    @Rollback(true)
    private void execute(Map<String, List<String>> posterSyncerPhoneNumbers, boolean isSyncEnabled, boolean isBlockEnabled, boolean isUnblockEnabled, boolean isScoreEnabled, boolean verifySync,
            boolean verifyBlock, boolean verifyUnblock, boolean verifyScore) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> posterPhoneNumbers = posterSyncerPhoneNumbers.get("posters");
        List<String> syncerPhoneNumbers = posterSyncerPhoneNumbers.get("syncers");
        List<String> blockedPhoneNumbers = posterSyncerPhoneNumbers.get("blocked");
        List<String> unblockedPhoneNumbers = posterSyncerPhoneNumbers.get("unblocked");
        List<String> scoredPhoneNumbers = posterSyncerPhoneNumbers.get("scored");

        WSRequest scoreRequest = getScoreRequest("5");
        Map<String, Set<String>> syncData = new HashMap<String, Set<String>>();
        Map<String, List<String>> blockedData = new HashMap<String, List<String>>();
        Map<String, List<String>> unblockedData = new HashMap<String, List<String>>();
        Map<String, List<String>> scoredData = new HashMap<String, List<String>>();

        Set<String> phoneNumbers = CommonTestUtility.getPhoneBearerMapper().keySet();
        String firstPhoneNumber = "+" + String.valueOf(baseNumberConstant);
        if (isSyncEnabled)
        {
            System.out.println("Sync started..");
            long startTime = System.currentTimeMillis();
            for (String syncerPhoneNumber : syncerPhoneNumbers)
            {
                syncData.put(syncerPhoneNumber, phoneNumbers);
                List<List<String>> phoneNumberBatches = CommonTestUtility.split(phoneNumbers, syncBatchSize);
                int batchSize = phoneNumberBatches.size();
                int indx = 1;
                boolean initSyncCompleteFlag = false;
                for (List<String> phoneNumberBatch : phoneNumberBatches)
                {
                    initSyncCompleteFlag = (batchSize == indx);
                    WSRequest syncRequest = getSyncRequest(phoneNumberBatch, initSyncCompleteFlag);
                    commonTestUtility.callWebEndPoint(BASE_URL + "contacts/sync", CommonTestUtility.getPhoneBearerMapper().get(syncerPhoneNumber).get(0), HTTPRequestMethodType.POST,
                            Utils.getJsonString(syncRequest));
                    indx++;
                }
            }
            System.out.println("Sync Complete in : " + (System.currentTimeMillis() - startTime) + " ms");
        }
        if (isBlockEnabled)
        {

            for (String blockedPhoneNumber : blockedPhoneNumbers)
            {

                commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(blockedPhoneNumber).get(1) + "/block", CommonTestUtility.getPhoneBearerMapper()
                        .get(firstPhoneNumber).get(0), HTTPRequestMethodType.PUT, "");
                blockedData.put(firstPhoneNumber, blockedPhoneNumbers);
            }
        }
        if (isUnblockEnabled)
        {

            for (String unblockedPhoneNumber : unblockedPhoneNumbers)
            {
                commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(unblockedPhoneNumber).get(1) + "/unblock", CommonTestUtility
                        .getPhoneBearerMapper().get(firstPhoneNumber).get(0), HTTPRequestMethodType.PUT, "");
                unblockedData.put(firstPhoneNumber, unblockedPhoneNumbers);
            }
        }
        if (isScoreEnabled)
            for (String scoredPhoneNumber : scoredPhoneNumbers)
            {
                commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(scoredPhoneNumber).get(1) + "/score", CommonTestUtility.getPhoneBearerMapper()
                        .get(firstPhoneNumber).get(0), HTTPRequestMethodType.POST, Utils.getJsonString(scoreRequest));
                scoredData.put(firstPhoneNumber, scoredPhoneNumbers);
            }

        if (verifySync)
        {
            verifySyncResults(syncData, posterPhoneNumbers);
        }
        verifyBlockUnBlockScore(blockedData, unblockedData, scoredData, verifyBlock, verifyUnblock, verifyScore);
        cleanUp();
    }

    private void cleanUp()
    {
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        jdbcTemplateObject.execute("delete from sync_update");
        Map<String, List<String>> phoneMapper = CommonTestUtility.getPhoneBearerMapper();
        Set<String> phoneNumbers = phoneMapper.keySet();
        for (String phoneNumber : phoneNumbers)
        {
            String accountId = phoneMapper.get(phoneNumber).get(1);

            jdbcTemplateObject.execute("delete from account_location where account_id ='" + accountId + "'");
            jdbcTemplateObject.execute("delete from account_relations where account_id ='" + accountId + "'");
            jdbcTemplateObject.execute("delete from native_contacts where account_id ='" + accountId + "'");
            jdbcTemplateObject.execute("truncate oauth_access_token cascade");
            jdbcTemplateObject.execute("truncate oauth_authorization cascade");

            jdbcTemplateObject.execute("delete from client where account_id ='" + accountId + "'");
            jdbcTemplateObject.execute("delete from property_listing where account_id ='" + accountId + "'");
            jdbcTemplateObject.execute("delete from account where id ='" + accountId + "'");
            jdbcTemplateObject.execute("delete from phonenumber where number='" + phoneNumber + "'");
        }
    }

    public void syncContacts(int totalAccountCount, int registerStartRange, int registerEndRange, int syncerStartRange, int syncerEndRange, int posterStartRange, int posterEndRange,
            int blockedStartRange, int blockedEndRange, int unblockedStartRange, int unblockedEndRange, int scoredStartRange, int scoredEndRange, boolean isSyncEnabled, boolean isBlockEnabled,
            boolean isUnblockEnabled, boolean isScoreEnabled, boolean verifySync, boolean verifyBlock, boolean verifyUnBlock, boolean verifyScore) throws JsonMappingException,
            JsonGenerationException, IOException
    {
        int[] registerRange = new int[] { registerStartRange, registerEndRange };
        int[] propertyPosterRange = new int[] { posterStartRange, posterEndRange };
        int[] syncerRange = new int[] { syncerStartRange, syncerEndRange };
        int[] blockedRange = new int[] { blockedStartRange, blockedEndRange };
        int[] unblockedRange = new int[] { unblockedStartRange, unblockedEndRange };
        int[] scoredRange = new int[] { scoredStartRange, scoredEndRange };
        Map<String, List<String>> posterSyncerPhoneNumbers = preSetIt(totalAccountCount, registerRange, propertyPosterRange, syncerRange, blockedRange, unblockedRange, scoredRange);
        execute(posterSyncerPhoneNumbers, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnBlock, verifyScore);

    }

    // @Test
    public void testAccountRelations1a() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 000
        boolean isSyncEnabled = false;
        boolean isBlockEnabled = false;
        boolean isUnblockEnabled = false;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1b() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 001
        boolean isSyncEnabled = false;
        boolean isBlockEnabled = false;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1c() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 010
        boolean isSyncEnabled = false;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = false;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1d() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 011
        boolean isSyncEnabled = false;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1e() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 100
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = false;
        boolean isUnblockEnabled = false;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1f() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 101
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = false;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1g() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 110
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = false;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1h() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 111
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 10;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 1;
        int syncerEndIndx = 2;

        int posterStartIndx = 1;
        int posterEndIndx = 2;

        int blockedStartIndx = 1;
        int blockedEndIndx = 2;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 2;

        int scoredStartIndx = 1;
        int scoredEndIndx = 2;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1i() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 111
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 20;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 2;
        int syncerEndIndx = 3;

        int posterStartIndx = 3;
        int posterEndIndx = 4;

        int blockedStartIndx = 4;
        int blockedEndIndx = 5;

        int unblockedStartIndx = 5;
        int unblockedEndIndx = 6;

        int scoredStartIndx = 6;
        int scoredEndIndx = 7;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1j() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 111
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 20;
        int registerStartIndx = 1;
        int registerEndIndx = 2;

        int syncerStartIndx = 3;
        int syncerEndIndx = 4;

        int posterStartIndx = 5;
        int posterEndIndx = 6;

        int blockedStartIndx = 7;
        int blockedEndIndx = 8;

        int unblockedStartIndx = 9;
        int unblockedEndIndx = 10;

        int scoredStartIndx = 11;
        int scoredEndIndx = 12;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelations1k() throws JsonMappingException, JsonGenerationException, IOException
    {
        // 111
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 15;
        int registerStartIndx = 1;
        int registerEndIndx = 9;

        int syncerStartIndx = 1;
        int syncerEndIndx = 9;

        int posterStartIndx = 1;
        int posterEndIndx = 9;

        int blockedStartIndx = 1;
        int blockedEndIndx = 9;

        int unblockedStartIndx = 1;
        int unblockedEndIndx = 9;

        int scoredStartIndx = 1;
        int scoredEndIndx = 9;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    @Test
    public void testAccountRelations1l() throws JsonMappingException, JsonGenerationException, IOException
    {
        boolean isSyncEnabled = true;
        boolean isBlockEnabled = true;
        boolean isUnblockEnabled = true;
        boolean isScoreEnabled = true;

        boolean verifySync = true;
        boolean verifyBlock = true;
        boolean verifyUnblockScore = true;
        boolean verifyScore = true;

        int initCount = 40;
        int registerStartIndx = 1;
        int registerEndIndx = 40;

        int syncerStartIndx = 5;
        int syncerEndIndx = 27;

        int posterStartIndx = 7;
        int posterEndIndx = 30;

        int blockedStartIndx = 1;
        int blockedEndIndx = 38;

        int unblockedStartIndx = 7;
        int unblockedEndIndx = 27;

        int scoredStartIndx = 9;
        int scoredEndIndx = 29;

        syncContacts(initCount, registerStartIndx, registerEndIndx, syncerStartIndx, syncerEndIndx, posterStartIndx, posterEndIndx, blockedStartIndx, blockedEndIndx, unblockedStartIndx,
                unblockedEndIndx, scoredStartIndx, scoredEndIndx, isSyncEnabled, isBlockEnabled, isUnblockEnabled, isScoreEnabled, verifySync, verifyBlock, verifyUnblockScore, verifyScore);
    }

    // @Test
    public void testAccountRelationsMultipleClients() throws JsonMappingException, JsonGenerationException, IOException
    {
        long basePhoneNumber = baseNumberConstant;
        int noOfClients = 3;
        int phoneNumbersPerClient = 50;
        CommonTestUtility commonTestUtility = new CommonTestUtility();
        List<List<String>> clientInfo = new ArrayList<List<String>>();
        List<List<WSRequest>> clientRequests = new ArrayList<List<WSRequest>>();
        String accountPhoneNumber = "+" + baseNumberConstant;
        List<String> accountInfo = commonTestUtility.signin(accountPhoneNumber, BASE_URL);
        System.out.println("accountInfo>>" + accountInfo);
        List<String> property = new ArrayList<String>();
        property.add("3592");
        commonTestUtility.register("fname", "lname", property, accountPhoneNumber, accountInfo.get(0), BASE_URL);

        for (int i = 1; i <= noOfClients * phoneNumbersPerClient; i++)
        {
            basePhoneNumber = basePhoneNumber + 1;
            String phoneNumber = "+" + basePhoneNumber;
            List<String> registeredUserInfo = commonTestUtility.signin(phoneNumber, BASE_URL);
            List<String> registeredUserProperty = new ArrayList<String>();
            registeredUserProperty.add("5379");
            commonTestUtility.register("fname", "lname", registeredUserProperty, phoneNumber, registeredUserInfo.get(0), BASE_URL);
            commonTestUtility.postPropertyListing(BASE_URL, registeredUserInfo.get(0));
        }

        List<List<String>> numbersList = new ArrayList<List<String>>();
        List<List<List<String>>> batchesForClient = new ArrayList<List<List<String>>>();

        for (int i = 0; i < noOfClients; i++)
        {
            clientInfo.add(i, commonTestUtility.signin(accountPhoneNumber, BASE_URL));
            List<String> numbers = new ArrayList<String>();
            for (int j = 1; j <= phoneNumbersPerClient; j++)
            {
                String phoneNumber = "+" + baseNumber++;
                numbers.add(phoneNumber);
            }
            numbersList.add(i, numbers);
            batchesForClient.add(i, CommonTestUtility.split(numbers, syncBatchSize));
        }

        int indx = 1;

        for (int i = 0; i < noOfClients; i++)
        {
            List<WSRequest> clientRequest = new ArrayList<WSRequest>();
            for (List<String> batchForClient : batchesForClient.get(i))
            {
                WSRequest request = new WSRequest();
                WSContacts contacts = new WSContacts();
                contacts.setInitialContactsSyncComplete(false);
                List<WSNativeContact> nativeContactsList = new ArrayList<WSNativeContact>();
                for (String number : batchForClient)
                {
                    WSNativeContact nativeContact = new WSNativeContact();
                    nativeContact.setFullName("fname" + i);
                    nativeContact.setCompanyName("lname" + i);
                    List<String> numbers = new ArrayList<String>();
                    numbers.add(number);
                    nativeContact.setNumbers(numbers);
                    nativeContactsList.add(nativeContact);
                }
                if (batchesForClient.get(i).size() == indx)
                {
                    contacts.setInitialContactsSyncComplete(true);
                    indx = 0;
                }
                contacts.setNativeContacts(nativeContactsList);
                indx++;
                request.setContacts(contacts);
                clientRequest.add(request);
            }
            clientRequests.add(i, clientRequest);

        }

        for (int i = 0; i < clientRequests.get(0).size(); i++)
        {
            for (int j = 0; j < noOfClients; j++)
            {
                System.out.println(Utils.getJsonString(clientRequests.get(j).get(i)));
                commonTestUtility.asyncCallWebEndPoint(BASE_URL + "contacts/sync", clientInfo.get(j).get(0), HTTPRequestMethodType.POST, Utils.getJsonString(clientRequests.get(j).get(i)));
            }
        }
        // verify results
        try
        {
            Thread.sleep(20000);
            int newsFeedsCount = (noOfClients * phoneNumbersPerClient) - 1;
            List<String> newsFeedAccounts = commonTestUtility.getNewsFeedAccountList(BASE_URL, accountInfo.get(0));
            List<String> syncUpdateAccounts = getSyncUpdateByAccountId(accountInfo.get(1));
            assertEquals(newsFeedsCount, newsFeedAccounts.size());
            assertEquals(newsFeedsCount, syncUpdateAccounts.size());

            Collections.sort(newsFeedAccounts);
            Collections.sort(syncUpdateAccounts);
            Assert.assertArrayEquals(newsFeedAccounts.toArray(), syncUpdateAccounts.toArray());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // @Test
    @Transactional
    public void testBlockWithUnRegisteredAccounts()
    {
        // 1st account blocking 2nd
        long phoneNumber1 = baseNumberConstant;
        long phoneNumber2 = baseNumberConstant + 1;
        String phoneNumber1Str = "+" + phoneNumber1;
        String phoneNumber2Str = "+" + phoneNumber2;

        try
        {
            List<String> account1Info = commonTestUtility.signin(phoneNumber1Str, BASE_URL);
            List<String> account2Info = commonTestUtility.signin(phoneNumber2Str, BASE_URL);
            // System.out.println(BASE_URL + "accounts/" + account2Info.get(1) +
            // "/block");
            Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + account2Info.get(1) + "/block", account1Info.get(0), HTTPRequestMethodType.PUT, "");
            cleanUp();
            assertEquals(403, response.getStatus());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

        }
    }

    // @Test
    @Transactional
    public void testBlockWithBlockerAsUnRegisteredAccount()
    {
        // 1st account blocking 2nd
        long phoneNumber1 = baseNumberConstant;
        long phoneNumber2 = baseNumberConstant + 1;
        String phoneNumber1Str = "+" + phoneNumber1;
        String phoneNumber2Str = "+" + phoneNumber2;

        try
        {
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber1Str, commonTestUtility.signin(phoneNumber1Str, BASE_URL));
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber2Str, commonTestUtility.signin(phoneNumber2Str, BASE_URL));
            register(phoneNumber2Str);
            Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(phoneNumber2Str).get(1) + "/block", CommonTestUtility
                    .getPhoneBearerMapper().get(phoneNumber1Str).get(0), HTTPRequestMethodType.PUT, "");
            cleanUp();
            assertEquals(403, response.getStatus());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // @Test
    @Transactional
    public void testBlockWithBlockedAsUnRegisteredAccount()
    {
        // 1st account blocking 2nd
        long phoneNumber1 = baseNumberConstant;
        long phoneNumber2 = baseNumberConstant + 1;
        String phoneNumber1Str = "+" + phoneNumber1;
        String phoneNumber2Str = "+" + phoneNumber2;

        try
        {
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber1Str, commonTestUtility.signin(phoneNumber1Str, BASE_URL));
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber2Str, commonTestUtility.signin(phoneNumber2Str, BASE_URL));
            register(phoneNumber1Str);
            Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(phoneNumber2Str).get(1) + "/block", CommonTestUtility
                    .getPhoneBearerMapper().get(phoneNumber1Str).get(0), HTTPRequestMethodType.PUT, "");
            cleanUp();
            CommonTestUtility.getPhoneBearerMapper().clear();
            assertEquals(400, response.getStatus());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // @Test
    @Transactional
    public void testBlockWithInvalidBlockerAccount()
    {
        // 1st account blocking 2nd
        long phoneNumber2 = baseNumberConstant + 1;
        String phoneNumber2Str = "+" + phoneNumber2;

        try
        {
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber2Str, commonTestUtility.signin(phoneNumber2Str, BASE_URL));
            Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/" + CommonTestUtility.getPhoneBearerMapper().get(phoneNumber2Str).get(1) + "/block", "beared invalid",
                    HTTPRequestMethodType.PUT, "");
            cleanUp();
            CommonTestUtility.getPhoneBearerMapper().clear();
            assertEquals(401, response.getStatus());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // @Test
    @Transactional
    public void testBlockWithInvalidBlockedAccount()
    {
        // 1st account blocking 2nd
        long phoneNumber1 = baseNumberConstant;
        String phoneNumber1Str = "+" + phoneNumber1;

        try
        {
            CommonTestUtility.getPhoneBearerMapper().put(phoneNumber1Str, commonTestUtility.signin(phoneNumber1Str, BASE_URL));
            register(phoneNumber1Str);
            Response response = commonTestUtility.callWebEndPoint(BASE_URL + "accounts/invalidaccount/block", CommonTestUtility.getPhoneBearerMapper().get(phoneNumber1Str).get(0),
                    HTTPRequestMethodType.PUT, "");
            cleanUp();
            CommonTestUtility.getPhoneBearerMapper().clear();
            assertEquals(400, response.getStatus());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // @Test
    public void testSplit()
    {
        Set<String> testSet = new LinkedHashSet<String>();
        for (int i = 1; i <= 1; i++)
        {
            testSet.add("ss" + i);
        }
    }

    // @Test
    public void testSyncContactsDuplicates() throws JsonMappingException, JsonGenerationException, IOException
    {
        init(10);
        WSRequest request = new WSRequest();
        List<WSNativeContact> wsNativeContacts = new ArrayList<WSNativeContact>();
        WSContacts contacts = new WSContacts();
        Iterator<String> phoneNumberIterator = (Iterator<String>) CommonTestUtility.getPhoneBearerMapper();
        List<String> phoneNumbers = new ArrayList<String>();
        register("+919870000001");
        while (phoneNumberIterator.hasNext())
        {
            String number = phoneNumberIterator.next();
            phoneNumbers.add(number);
            phoneNumbers.add(number);
        }
        wsNativeContacts.add(getNativeContact("fname1", "lname1", phoneNumbers));
        contacts.setInitialContactsSyncComplete(true);
        contacts.setNativeContacts(wsNativeContacts);
        request.setContacts(contacts);
        commonTestUtility.callWebEndPoint(BASE_URL + "contacts/sync", CommonTestUtility.getPhoneBearerMapper().get("+919870000001").get(0), HTTPRequestMethodType.POST, Utils.getJsonString(request));
        cleanUp();
        CommonTestUtility.getPhoneBearerMapper().clear();
    }
}