package one.group.rest.v1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSAccountRelations;
import one.group.entities.api.request.WSAddPropertyListing;
import one.group.entities.api.request.WSContacts;
import one.group.entities.api.request.WSNativeContact;
import one.group.entities.api.request.WSPhotoReference;
import one.group.entities.api.request.WSPropertyListingSearchQueryRequest;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@ContextConfiguration("/applicationContext-configuration.xml")
public class GenerateDemoSeedData
{

    public enum CommissionType
    {
        DIRECT, VIA;

        private static final List<CommissionType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomCommissionType()
        {
            return VALUES.get(RANDOM.nextInt(SIZE)).toString();
        }
    };

    public enum Rooms
    {
        ONE_RK("1rk"), ONE_BHK("1bhk"), ONE_HALF("1hbhk"), TWO("2bhk"), TWO_HALF("2hbhk"), THREE("3bhk"), FOUR("4bhk"), FIVE_PLUS("5pbhk");
        private String name;

        Rooms(String name)
        {
            this.name = name;
        }

        private static final List<Rooms> VALUES = Collections.unmodifiableList(Arrays.asList(Rooms.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomRooms()
        {
            return Rooms.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum PropertySubType
    {
        APARTMENT("apartment"), INDEPENDENT_HOUSE("independent_house"), ROW_HOUSE("row_house"), VILLA("villa");

        private String name;

        PropertySubType(String name)
        {
            this.name = name;
        }

        private static final List<PropertySubType> VALUES = Collections.unmodifiableList(Arrays.asList(PropertySubType.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomSubType()
        {
            return PropertySubType.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum Amenity
    {
        GARDEN("garden"), PARKING("parking"), POOL("pool"), INTERCOM("intercom"), SECURITY("security"), CLUBHOUSE("clubhouse"), GYM("gym"), LIFT("lift"), PLAY_AREA("play_area"), MAINTENANCE_STAFF(
                "maintenance_staff"), RAIN_HARVESTING("rain_harvesting");

        private String name;

        Amenity(String name)
        {
            this.name = name;
        }

        private static final List<Amenity> VALUES = Collections.unmodifiableList(Arrays.asList(Amenity.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomAmenity()
        {
            return Amenity.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;
        }
    }

    CommonTestUtility commonTestUtility = new CommonTestUtility();
    Random randomLocation = new Random();
    List<String> topBusinessLocations = new ArrayList<String>(Arrays.asList("1560", "1561", "1562", "1563", "1564", "1565", "1566", "1567", "1568", "1569", "1570", "1571"));

    List<String> savedRequirementsLocations = new ArrayList<String>(Arrays.asList("1560", "1561", "1562", "1563", "1564", "1565", "1566", "1567", "1568", "1569", "1570", "1571"));

    List<String> savedRequirementsNames = new ArrayList<String>(Arrays.asList("Mukesh Verma", "Prashant", "Zaffer Abbas", "Virendra Pandey", "Deepak Bansal"));

    List<String> descriptions = new ArrayList<String>(
            Arrays.asList(
                    "A well presented semi detached house on the outskirts of city",
                    "Spacious Apartment in peaceful location with quiet surroundings. Large, airy and spacious rooms with ample sunlight and good views.",
                    "Cool, calm and sophisticated with a youthful edge, this functional home is enveloped in light and comfort. Crisp white walls, timber floors and high ceilings create a style as timeless as the sparkling ocean view",
                    "This property lies convenient for a variety of local amenities including shops, post office and regular public transport into the city centre. The property is also within walking distance of John Jones Secondary School.",
                    "If you would like to view the property, we can be very flexible with times but would prefer evenings or weekends. Please call Sue on +919820165456 for more information",
                    "The property is beautifully designed internally and externally by the existing house owner. Gas pipeline for kitchen and bathroom. Wardrobes for bedroom. Modular Kitchen. Beautiful Safety Door. Video door phone for safety. Property visit available on Sunday. Property is in a big complex"));

    public static final char[] CHARSET_NUMERIC = "23456789".toCharArray();

    public static final char[] CHARSET_NUMERIC_AREA = "568947".toCharArray();

    public static final char[] CHARSET_NUMERIC_AREA_SEARCH = "354".toCharArray();

    // private static String url = "http://10.50.249.127:8080/movein-rest/v1/";

    // private static String url = "http://10.50.249.15:8081/movein-rest/v1/";
    private static String url = "http://10.50.249.111:8081/movein-rest/v1/";

    @Test
    public void testGenerateDemoData() throws JsonMappingException, JsonGenerationException, IOException
    {
        int totalPropertyPerAccount = 15;
        int totalSearchForAccountOne = 3;
        List<Map> accountList = new ArrayList<Map>();
        HashMap<String, List<String>> multiMap = new HashMap<String, List<String>>();
        multiMap.put("7506054762", Arrays.asList("Mukesh Pandey", "Hedge Realty Private Limited")); //
        multiMap.put("8655359531", Arrays.asList("Ajay Sakpal", "PropHorizon Real Estate"));//
        multiMap.put("9920472205", Arrays.asList("Archana", "Khushi Groups")); //
        multiMap.put("9819974715", Arrays.asList("Ramesh  Soni", "Soonee Homes"));
        multiMap.put("9820168785", Arrays.asList("Virendra Pandey", "V. R. Pandey Group Of Real Estate"));

        multiMap.put("9820028883", Arrays.asList("Kailash Rajani", "Rajani Estate Agents"));
        multiMap.put("9821447122", Arrays.asList("Mitesh", "Skyscrapper Realtors"));
        multiMap.put("8898534837", Arrays.asList("Shivananda   ", "3 Villaz"));
        multiMap.put("8655094922", Arrays.asList("Mehul  Rathod", "Mangalyam Meadows"));
        multiMap.put("9930168147", Arrays.asList("Suphip Patel", "Ekta Developers"));

        multiMap.put("7208643006", Arrays.asList("Mukesh Verma", "Mukesh Real Estate Private Limited"));
        multiMap.put("9322359333", Arrays.asList("Prakash Rao", "Samarth Group Of Companies"));
        multiMap.put("9324566804", Arrays.asList("Deepa", "Reliable Group"));
        multiMap.put("9768162133", Arrays.asList("Divya", "Kalpataru"));
        multiMap.put("8007611711", Arrays.asList("Amar Agrawal", "Shiv Sai Associates"));

        multiMap.put("9820163867", Arrays.asList("Rohit Batus", "Yellow Key"));
        multiMap.put("9920201749", Arrays.asList("Prashant", "DHFL Property Services Limited"));
        multiMap.put("9320791080", Arrays.asList("Srikant", "Sohum Habitat Private Limited"));
        multiMap.put("9282511012", Arrays.asList("Sindhu", "L And T Realty Limited"));
        multiMap.put("9867892102", Arrays.asList("Ravi", "RK Mumbai Realtors"));

        multiMap.put("9820326875", Arrays.asList("Zaffer Abbas", "Angel Homes"));
        multiMap.put("9867288455", Arrays.asList("Sandeep Sawant", "Instant Office Space"));
        multiMap.put("9321177022", Arrays.asList("Jatin Ambani", "Advice Consultants"));
        multiMap.put("9819890078", Arrays.asList("dhirendra", "DHM Estates"));
        multiMap.put("9820120671", Arrays.asList("Deepak Bansal", "Home N Homes"));

        multiMap.put("9503561189", Arrays.asList("Nirmiti Tawde", "99 Windows"));
        multiMap.put("9820308733", Arrays.asList("Roshal", "Abhaas Landscapes"));
        multiMap.put("9920830147", Arrays.asList("Manish Shah", "Snackers Accommodation"));
        multiMap.put("9594433333", Arrays.asList("Ramprasad Padhi Padhi", "Pinnacle  Realty"));
        multiMap.put("9820433018", Arrays.asList("Ashok Jaiswal", "Hanu Media"));

        multiMap.put("9820332945", Arrays.asList("Siddharth Pai", "Bad Studio"));
        multiMap.put("9820720888", Arrays.asList("Sachin N Aswani", "Ek-Om Real Estate Consultants"));
        multiMap.put("7208653327", Arrays.asList("Shailendra Nachankar", "Omkar & Royal Realtors"));
        multiMap.put("9768804499", Arrays.asList("Dheeraj Kumar", "Dream Homes Realty Group"));
        multiMap.put("8652288515", Arrays.asList("Azmat Shah", "Realty Merchant"));

        multiMap.put("9820017468", Arrays.asList("Vineet Jain", "Fin Street Financial Service Pvt Limited"));
        multiMap.put("8655095701", Arrays.asList("Angeli", "Shreeji Vrund Realty Private Limited"));
        multiMap.put("9833620620", Arrays.asList("Ramakrishna P", "Landbell Realty"));
        multiMap.put("9702458988", Arrays.asList("Karan Damodar Naik", "Veena Estate Consultants"));
        multiMap.put("9699959969", Arrays.asList("Vishal Nayak", "Vitrag Estate Agency"));

        multiMap.put("8879505146", Arrays.asList("Shilpa ji", "Nisarg Creation"));
        multiMap.put("9923403102", Arrays.asList("S A Malik", "Novaspace Realty Private Limited"));
        multiMap.put("9222149046", Arrays.asList("Vipin Joshi", "Varasiddhi Private Limited"));
        multiMap.put("9702451967", Arrays.asList("Atul Thakker", "Jai Ganesh Enterprise"));
        multiMap.put("9326473825", Arrays.asList("Pravin Dayaram Prajapati", "Purvi Pest Control"));

        multiMap.put("9664353735", Arrays.asList("Julie  Dedhia", "Soundlines Group"));
        multiMap.put("9769300230", Arrays.asList("Sohil Noorani", "Preleased Properties"));
        multiMap.put("9004011113", Arrays.asList("Deepak Surve", "Om Associates, Mumbai"));
        multiMap.put("9699107818", Arrays.asList("Amit S.", "Hanumant Group"));
        multiMap.put("9867540163", Arrays.asList("Santosh Rajeshirke", "Drushti Group"));

        String bearer = null;
        String accountOneAccessToken = null;
        Set<Entry<String, List<String>>> setMap = multiMap.entrySet();

        // get all Image To Upload
        File photoFolder = new File("/home/ashishthorat/Desktop/Agent");
        File[] photoFiles = photoFolder.listFiles();

        Iterator<Entry<String, List<String>>> iteratorMap = setMap.iterator();
        int accountFlag = 0;
        while (iteratorMap.hasNext())
        {

            int size = topBusinessLocations.size();
            int descSize = descriptions.size();

            Map.Entry<String, List<String>> entry = (Map.Entry<String, List<String>>) iteratorMap.next();

            String mobileNumber = "+91" + entry.getKey();
            List<String> values = entry.getValue();

            List<String> account = commonTestUtility.signin(mobileNumber, url);
            bearer = account.get(0);
            String accountId = account.get(1);

            // Store Register Account Information
            Map<String, String> accountMap = new HashMap<String, String>();
            accountMap.put("bearer", account.get(0));
            accountMap.put("accountId", account.get(1));
            accountMap.put("mobileNumber", mobileNumber);
            accountList.add(accountMap);

            if (accountFlag == 0)
            {
                accountOneAccessToken = account.get(0);
            }
            List<String> locations = new ArrayList<String>();
            for (int k = 0; k < topBusinessLocations.size(); k++)
            {
                locations.add(topBusinessLocations.get(k));
            }

            List<String> descs = new ArrayList<String>();
            for (int desc = 0; desc < descriptions.size(); desc++)
            {
                descs.add(descriptions.get(desc));
            }

            String fullName = values.get(0);
            String companyName = values.get(1);
            // Account Registration
            commonTestUtility.register(fullName, companyName, locations, mobileNumber, bearer, url);

            // Upload Photo
            getFileUpload(bearer, accountId, fullName, companyName, photoFiles[accountFlag]);

            for (int p = 0; p < totalPropertyPerAccount; p++)
            {
                WSRequest request = new WSRequest();
                WSAddPropertyListing wsAddPropertyListing = new WSAddPropertyListing();
                wsAddPropertyListing.setArea(Utils.randomString(CHARSET_NUMERIC_AREA, 3, true));
                wsAddPropertyListing.setCommissionType(CommissionType.randomCommissionType().toString().toLowerCase());
                wsAddPropertyListing.setDescription(descs.get(randomLocation.nextInt(descSize)));
                wsAddPropertyListing.setLocationId(locations.get(randomLocation.nextInt(size)));
                List<String> amenities = new ArrayList<String>();
                for (int a = 0; a < 10; a++)
                {
                    amenities.add(Amenity.randomAmenity().toString());
                }

                wsAddPropertyListing.setAmenities(amenities);
                wsAddPropertyListing.setPropertyMarket(PropertyMarket.SECONDARY.toString());
                wsAddPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString());
                wsAddPropertyListing.setPropertySubType(PropertySubType.randomSubType().toString());
                wsAddPropertyListing.setSalePrice(Utils.randomString(CHARSET_NUMERIC, 7, true));
                wsAddPropertyListing.setRentPrice("0");
                wsAddPropertyListing.setRooms(Rooms.randomRooms().toString());
                // System.out.println(wsAddPropertyListing);
                request.setPropertyListing(wsAddPropertyListing);

                Response response = commonTestUtility.callWebEndPoint(url + "property_listings", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

                String json = response.readEntity(String.class);
                WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
                // System.out.println(jsonResponse);
            }
            accountFlag++;
        }

        for (int p = 0; p < totalSearchForAccountOne; p++)
        {
            List<String> locations = new ArrayList<String>();
            for (int k = 0; k < savedRequirementsLocations.size(); k++)
            {
                locations.add(savedRequirementsLocations.get(k));
            }

            List<String> reqname = new ArrayList<String>();
            for (int savedSearch = 0; savedSearch < savedRequirementsNames.size(); savedSearch++)
            {
                reqname.add(savedRequirementsNames.get(savedSearch));
            }
            SecureRandom random = new SecureRandom();
            WSPropertyListingSearchQueryRequest searchRequest = new WSPropertyListingSearchQueryRequest();
            searchRequest.setPropertyType(Arrays.asList(new String[] { "residential" }));
            searchRequest.setLocationId(Integer.valueOf(locations.get(random.nextInt(savedRequirementsLocations.size()))));
            searchRequest.setPropertyMarket(Arrays.asList(new String[] { PropertyMarket.SECONDARY.toString() }));
            searchRequest.setTransactionType(Arrays.asList(new String[] { PropertyTransactionType.SALE.toString() }));
            if (random.nextBoolean())
            {
                List<String> amenities = new ArrayList<String>();
                int limit = random.nextInt(9);
                for (int k = 0; k < 2; k++)
                {
                    amenities.add(Amenity.randomAmenity().toString());
                }

                if (!amenities.isEmpty())
                    searchRequest.setAmenities(new ArrayList<String>(amenities));
            }

            if (random.nextBoolean())
            {
                Set<String> subTypes = new HashSet<String>();
                int limit = random.nextInt(one.group.core.enums.PropertySubType.values().length);
                for (int k = 0; k < limit; k++)
                {
                    subTypes.add(PropertySubType.randomSubType().toString());
                }

                if (!subTypes.isEmpty())
                    searchRequest.setPropertySubType(new ArrayList<String>(subTypes));
            }

            searchRequest.setMaxArea(1200);
            searchRequest.setMinArea(Integer.parseInt(Utils.randomString(CHARSET_NUMERIC_AREA_SEARCH, 3, true)));
            searchRequest.setMaxRentPrice(null);
            searchRequest.setMinRentPrice(null);
            searchRequest.setMaxSalePrice("" + 8000000);
            searchRequest.setMinSalePrice(Utils.randomString(CHARSET_NUMERIC_AREA_SEARCH, 7, true));

            if (random.nextBoolean())
            {
                Set<String> rooms = new HashSet<String>();
                int limit = random.nextInt(Rooms.values().length);
                for (int k = 0; k < limit; k++)
                {
                    rooms.add(Rooms.randomRooms());
                }

                if (!rooms.isEmpty())
                    searchRequest.setRooms(new ArrayList<String>(rooms));
            }

            searchRequest.setRequirementName(reqname.get(random.nextInt(savedRequirementsNames.size())));

            WSRequest request = new WSRequest();
            request.setSearch(searchRequest);
            Response response = commonTestUtility.callWebEndPoint(url + "search/property_listings", accountOneAccessToken, HTTPRequestMethodType.POST, Utils.getJsonString(request));
            String stringResponse = response.readEntity(String.class);

        }

        // Account Contacts Sync And Score Functionality

        SecureRandom random = new SecureRandom();
        List<String> phoneNumberList = new ArrayList<String>();
        List<WSNativeContact> nativeContacts = new ArrayList<WSNativeContact>();
        WSNativeContact wsNativeContacts = new WSNativeContact();
        wsNativeContacts.setFullName("FULLNAME- " + random.nextDouble());
        wsNativeContacts.setCompanyName("COMPANY- " + random.nextDouble());

        int toAccount;
        int NegativeScoreAccountCount = 0;
        for (int fromAccount = 0; fromAccount < accountList.size(); fromAccount++)
        {
            System.out.println("FromAccount======" + fromAccount + "====");
            Map<String, String> fromAccountMap = new HashMap<String, String>();
            fromAccountMap = accountList.get(fromAccount);

            String accountBearer = fromAccountMap.get("bearer");
            String fromAccountId = fromAccountMap.get("accountId");
            String accountmobileNumber = fromAccountMap.get("mobileNumber");

            System.out.println("AccountDetails===" + "ID=" + fromAccountId + "====" + "bearer=" + accountBearer + "==mobileNumber=" + accountmobileNumber);
            toAccount = fromAccount + 1;
            int contactSyncTotal = 0;
            while (contactSyncTotal < 20)
            {

                if (toAccount == accountList.size())
                {
                    toAccount = 0;
                }

                Map<String, String> mobileNumberMap = new HashMap<String, String>();
                mobileNumberMap = accountList.get(toAccount);
                String accountId = mobileNumberMap.get("accountId");
                String mobileNumber = mobileNumberMap.get("mobileNumber");
                phoneNumberList.add(mobileNumber);

                // System.out.println("toAccountCount======" + toAccount +
                // "====");
                // System.out.println("toAccountID======" + accountId + "====");
                // To assign Postive Score
                if (contactSyncTotal < 10)
                {
                    // Score Account
                    WSRequest scoreRequest = getScoreRequest("10");
                    commonTestUtility.callWebEndPoint(url + "accounts/" + accountId + "/score", accountBearer, HTTPRequestMethodType.POST, Utils.getJsonString(scoreRequest));

                    // System.out.println("Positive Score==" + "FromID==" +
                    // fromAccountId + "ToAccount=" + accountId);
                    // System.out.println("PositiveScoreAccountID======" +
                    // accountId + "====");
                }

                contactSyncTotal++;
                toAccount++;
            }

            // For Negative Score
            int NegativeScoreAssignToAccount = 0;
            NegativeScoreAssignToAccount = fromAccount + 15;

            // System.out.println("ValueOf scoreAccount==" +
            // NegativeScoreAssignToAccount);
            if (NegativeScoreAssignToAccount >= accountList.size())
            {
                NegativeScoreAssignToAccount = NegativeScoreAccountCount;
                // System.out.println("ValueOf S" +
                // NegativeScoreAssignToAccount);
                NegativeScoreAccountCount++;
            }
            int scoreCount = 0;
            while (scoreCount < 10)
            {
                Map<String, String> ScoreMap = new HashMap<String, String>();
                ScoreMap = accountList.get(NegativeScoreAssignToAccount);
                String accountId = ScoreMap.get("accountId");

                // System.out.println("NegativeScoretoAccountCount======" +
                // NegativeScoreAssignToAccount + "====");
                // System.out.println("NegativeScortoAccountID======" +
                // accountId + "====");
                // Score Account
                WSRequest scoreRequest = getScoreRequest("-10");
                commonTestUtility.callWebEndPoint(url + "accounts/" + accountId + "/score", accountBearer, HTTPRequestMethodType.POST, Utils.getJsonString(scoreRequest));

                // System.out.println("Negative Score==" + "FromID==" +
                // fromAccountId + "ToAccount=" + accountId);
                NegativeScoreAssignToAccount++;
                if (NegativeScoreAssignToAccount == accountList.size())
                {
                    NegativeScoreAssignToAccount = 0;
                }

                scoreCount++;
            }

            wsNativeContacts.setNumbers(phoneNumberList);
            nativeContacts.add(wsNativeContacts);

            WSContacts contacts = new WSContacts();
            contacts.setNativeContacts(nativeContacts);

            WSRequest request = new WSRequest();
            request.setContacts(contacts);

            Response response = commonTestUtility.callWebEndPoint(url + "contacts/sync", accountBearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));

            phoneNumberList.clear();
            String json = response.readEntity(String.class);
            WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

            if (!jsonResponse.getResult().getCode().equals("200"))
            {

                // System.out.println(Utils.getJsonString(request));
                // System.err.println(jsonResponse);
            }
            else
            {
                // System.out.println(jsonResponse);
            }
        }

    }

    public void getFileUpload(String bearer, String accountId, String fullName, String companyName, File file) throws JsonMappingException, JsonGenerationException, IOException
    {

        FormDataMultiPart multiPart = new FormDataMultiPart();
        multiPart.field("photo", new FileInputStream(file.getAbsolutePath()), new MediaType("image", "jpg"));

        Response response = commonTestUtility.callWebEndPointMultiPart(url + "upload", bearer, HTTPRequestMethodType.POST, multiPart);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        if (!jsonResponse.getResult().getCode().equals("200"))
        {
            System.out.println("file" + file.getName());
            System.err.println(jsonResponse);
        }
        Object jsonObj = jsonResponse.getData();
        String jsonData = Utils.getJsonString(jsonObj);
        WSPhotoReference wsPhoto = (WSPhotoReference) Utils.getInstanceFromJson(jsonData, WSPhotoReference.class);

        WSAccount wsaccount = new WSAccount();
        wsaccount.setFullName(fullName);
        wsaccount.setCompanyName(companyName);
        wsaccount.setPhoto(wsPhoto);
        WSRequest request = new WSRequest();
        request.setAccount(wsaccount);

        Response response1 = commonTestUtility.callWebEndPoint(url + "accounts/" + accountId, bearer, HTTPRequestMethodType.PUT, Utils.getJsonString(request));
        String json1 = response1.readEntity(String.class);
        WSResponse jsonResponse1 = (WSResponse) Utils.getInstanceFromJson(json1, WSResponse.class);
        if (!jsonResponse1.getResult().getCode().equals("200"))
        {

            System.err.println(jsonResponse1);
        }
        else
        {
            // System.out.println(jsonResponse1);

        }
    }

    private WSRequest getScoreRequest(String score)
    {
        WSRequest request = new WSRequest();
        WSAccountRelations wsAccountRelations = new WSAccountRelations();
        wsAccountRelations.setScore(score);

        request.setAccountRelations(wsAccountRelations);
        return request;
    }

}
