package one.group.rest.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.WSAccount;
import one.group.entities.api.response.WSAccountsResult;
import one.group.entities.api.response.WSClient;
import one.group.entities.api.response.WSResponse;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@ContextConfiguration("/applicationContext-configuration.xml")
public class GenerateChatSeedData
{
    public static CommonTestUtility commonTestUtility = new CommonTestUtility();

    private static String urlOld = "http://10.50.249.127:8080/movein-rest/v1/";
    private static String url = "http://10.30.2.102:8080/movein-rest/v1/";
    private static List<String> AWSAccountList = new ArrayList<String>();
    private static List<Object> AWSClientList = new ArrayList<Object>();

    // @Test
    public static void main(String[] args) throws JsonMappingException, JsonGenerationException, IOException
    {

        System.out.println(AWSAccountList.size());
        for (int i = 0; i < 5; i++)
        {

            System.out.println((WSClient) AWSClientList.get(i));

        }
    }

    public void fetchAccountDetails() throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> accountList = AWSAccountList;
        String bearer = "bc8a523b-a209-40e9-a8b1-18b02a20bb1c";
        String clientString = "";
        String phoneString = "";
        for (int i = 400; i < 500; i++)
        {
            String accountId = accountList.get(i);
            // System.out.println(accountId);
            Response response = commonTestUtility.callWebEndPoint(url + "accounts/" + accountId, bearer, HTTPRequestMethodType.GET, null);
            String json = response.readEntity(String.class);
            WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

            WSAccountsResult accountResult = (WSAccountsResult) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSAccountsResult.class);
            Set<WSAccount> wsAccounts = accountResult.getAccountsResult();
            WSAccount account = wsAccounts.iterator().next();
            String mobileNumber = account.getPrimaryMobile();

            WSClient accountDetails = commonTestUtility.signin1(mobileNumber, url);

            System.out.println(",\"" + accountDetails + "\"");
        }
    }
}
