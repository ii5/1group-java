package one.group.rest.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import one.group.core.Constant;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ContactsWebServiceTest
{
    public enum CommissionType
    {
        DIRECT,
        VIA;

        private static final List<CommissionType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomCommissionType()
        {
            return VALUES.get(RANDOM.nextInt(SIZE)).toString();
        }
    };

    public enum Rooms
    {
        ONE_RK("1rk"),
        ONE_BHK("1bhk"),
        ONE_HALF("1hbhk"),
        TWO("2bhk"),
        TWO_HALF("2hbhk"),
        THREE("3bhk"),
        FOUR("4bhk"),
        FIVE_PLUS("5pbhk");
        private String name;

        Rooms(String name)
        {
            this.name = name;
        }

        private static final List<Rooms> VALUES = Collections.unmodifiableList(Arrays.asList(Rooms.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomRooms()
        {
            return Rooms.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum PropertySubType
    {
        APARTMENT("apartment"),
        INDEPENDENT_HOUSE("independent_house"),
        ROW_HOUSE("row_house"),
        VILLA("villa");

        private String name;

        PropertySubType(String name)
        {
            this.name = name;
        }

        private static final List<PropertySubType> VALUES = Collections.unmodifiableList(Arrays.asList(PropertySubType.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomSubType()
        {
            return PropertySubType.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;

        }
    }

    public enum Amenity
    {
        GARDEN("garden"),
        PARKING("parking"),
        POOL("pool"),
        INTERCOM("intercom"),
        SECURITY("security"),
        CLUBHOUSE("clubhouse"),
        GYM("gym"),
        LIFT("lift"),
        PLAY_AREA("play_area"),
        MAINTENANCE_STAFF("maintenance_staff"),
        RAIN_HARVESTING("rain_harvesting");

        private String name;

        Amenity(String name)
        {
            this.name = name;
        }

        private static final List<Amenity> VALUES = Collections.unmodifiableList(Arrays.asList(Amenity.values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static String randomAmenity()
        {
            return Amenity.valueOf(VALUES.get(RANDOM.nextInt(SIZE)).toString()).name;
        }
    }

    Random randomLocation = new Random();
    List<String> topBusinessLocations = new ArrayList<String>(Arrays.asList("637", "572", "1025", "13584", "1639"));

    List<String> savedRequirementsLocations = new ArrayList<String>(Arrays.asList("572", "1025", "13584"));

    List<String> savedRequirementsNames = new ArrayList<String>(Arrays.asList("Mukesh Verma", "Prashant", "Zaffer Abbas", "Virendra Pandey", "Deepak Bansal"));

    List<String> descriptions = new ArrayList<String>(
            Arrays.asList(
                    "A well presented semi detached house on the outskirts of city",
                    "Spacious Apartment in peaceful location with quiet surroundings. Large, airy and spacious rooms with ample sunlight and good views.",
                    "Cool, calm and sophisticated with a youthful edge, this functional home is enveloped in light and comfort. Crisp white walls, timber floors and high ceilings create a style as timeless as the sparkling ocean view",
                    "This property lies convenient for a variety of local amenities including shops, post office and regular public transport into the city centre. The property is also within walking distance of John Jones Secondary School.",
                    "If you would like to view the property, we can be very flexible with times but would prefer evenings or weekends. Please call Sue on +919820165456 for more information",
                    "The property is beautifully designed internally and externally by the existing house owner. Gas pipeline for kitchen and bathroom. Wardrobes for bedroom. Modular Kitchen. Beautiful Safety Door. Video door phone for safety. Property visit available on Sunday. Property is in a big complex"));

    public static final char[] CHARSET_NUMERIC = "23456789".toCharArray();

    public static final char[] CHARSET_NUMERIC_AREA = "568947".toCharArray();

    public static final char[] CHARSET_NUMERIC_AREA_SEARCH = "354".toCharArray();

    // private static String url = "http://10.50.249.127:8080/movein-rest/v1/";

    // private static String url = "http://10.50.249.15:8081/movein-rest/v1/";
    private static String url = "http://10.50.249.94:8080/movein-rest/v1/";

    @Test
    public void testGenerateContactsData() throws JsonMappingException, JsonGenerationException, IOException
    {

        List<String> phonenumber = new ArrayList<String>();

        CommonTestUtility commonTestUtility = new CommonTestUtility();

        for (int i = 2; i < 4/* phonenumber.size() */; i++)
        {
            List<String> account = commonTestUtility.signin(phonenumber.get(i), url);
            String bearer = account.get(0);
            String accountId = account.get(1);

            // Store Register Account Information
            Map<String, String> accountMap = new HashMap<String, String>();
            accountMap.put("bearer", account.get(0));
            accountMap.put("accountId", account.get(1));
            accountMap.put("mobileNumber", phonenumber.get(i));

            List<String> locations = new ArrayList<String>();
            for (int k = 0; k < topBusinessLocations.size(); k++)
            {
                locations.add(topBusinessLocations.get(k));
            }

            List<String> descs = new ArrayList<String>();
            for (int desc = 0; desc < descriptions.size(); desc++)
            {
                descs.add(descriptions.get(desc));
            }

            String fullName = "Full Name " + Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, Constant.PUSHER_CHANNEL_ID_LENGTH, false);
            String companyName = "Company Name " + Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, Constant.PUSHER_CHANNEL_ID_LENGTH, false);
            // Account Registration
            commonTestUtility.register(fullName, companyName, locations, phonenumber.get(i), bearer, url);
        }
    }
}
