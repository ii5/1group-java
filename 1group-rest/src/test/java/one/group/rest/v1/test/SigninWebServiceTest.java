package one.group.rest.v1.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;
import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.request.WSRequestOtp;
import one.group.entities.api.request.WSSignIn;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.utils.Utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = {
// "classpath:applicationContext-configuration-PushService.xml",
// "classpath:applicationContext-configuration-Cms.xml",
// "classpath:applicationContext-services.xml",
// "classpath:applicationContext-DAO.xml",
// "classpath:applicationContext-DAO-jpa.xml",
// "classpath:applicationContext-configuration-Cache.xml", })
@ContextConfiguration(locations = { "classpath:applicationContext-DAO-test.xml", })
public class SigninWebServiceTest
{
    String BASE_URL = "http://localhost:8081/1group/v2/";
    CommonTestUtility commonTestUtility = new CommonTestUtility();

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    // @Autowired
    // private SyncUpdateDAO syncUpdateDao;

    public DataSource getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    private WSResponse getOtpResponse(String mobileNumber, String baseURL) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSRequest request = new WSRequest();
        WSRequestOtp requestOut = new WSRequestOtp(mobileNumber);
        request.setRequestOtp(requestOut);
        Response response = commonTestUtility.callWebEndPoint(baseURL + "request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    private WSResponse getSigninResponse(String baseURL, String mobileNumber, String otp, String grantType, String clientType, String deviceId, String deviceUserName, String deviceModelName,
            String devicePlatform, String deviceOsVersion) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSRequest request = new WSRequest();
        WSSignIn requestSignin = new WSSignIn();
        requestSignin.setMobileNumber(mobileNumber);
        requestSignin.setOtp(otp);
        requestSignin.setGrantType(grantType);
        requestSignin.setClientType(clientType);
        requestSignin.setDeviceId(deviceId);
        requestSignin.setDeviceUserName(deviceUserName);
        requestSignin.setDeviceModelName(deviceModelName);
        requestSignin.setDevicePlatform(devicePlatform);
        requestSignin.setDeviceOsVersion(deviceOsVersion);
        request.setSignIn(requestSignin);
        Response response = commonTestUtility.callWebEndPoint(baseURL + "sign_in", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }

    private void cleanUp(String phoneNumber)
    {
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        jdbcTemplateObject.execute("truncate account cascade");
        jdbcTemplateObject.execute("delete from otp where phonenumber_id=(select id from phonenumber where number='" + phoneNumber + "')");
        jdbcTemplateObject.execute("delete from phonenumber where number='" + phoneNumber + "'");
    }

    @Test
    public void testRequestOtpNormal() throws JsonMappingException, JsonGenerationException, IOException
    {
        String mobileNumber = "+919870000001";
        WSResponse response = getOtpResponse(mobileNumber, BASE_URL);
        WSResult result = response.getResult();
        assertEquals("20000", result.getCode());
        cleanUp(mobileNumber);
    }

    @Test
    public void testMultipleRequestOtp() throws JsonMappingException, JsonGenerationException, IOException
    {
        long baseMobileNumber = 919870000001l;
        for (int i = 0; i < 50; i++)
        {
            String mobileNumber = "+" + baseMobileNumber++;
            WSResponse response = getOtpResponse(mobileNumber, BASE_URL);
            WSResult result = response.getResult();
            assertEquals("20000", result.getCode());
            cleanUp(mobileNumber);
        }
    }

    @Test
    public void testRequestOtpMultipleWithSameNumber() throws JsonMappingException, JsonGenerationException, IOException
    {
        String mobileNumber1 = "+919870000001";
        String mobileNumber2 = "+919870000001";
        String mobileNumber3 = "+919870000001";
        WSResponse response1 = getOtpResponse(mobileNumber1, BASE_URL);
        WSResponse response2 = getOtpResponse(mobileNumber2, BASE_URL);
        WSResponse response3 = getOtpResponse(mobileNumber3, BASE_URL);
        WSResult result1 = response1.getResult();
        WSResult result2 = response2.getResult();
        WSResult result3 = response3.getResult();
        assertEquals("20000", result1.getCode());
        assertEquals("20000", result2.getCode());
        assertEquals("20000", result3.getCode());
        cleanUp(mobileNumber1);
        cleanUp(mobileNumber2);
        cleanUp(mobileNumber3);
    }

    @Test
    public void testRequestOtpWithInvalidNumbers() throws JsonMappingException, JsonGenerationException, IOException
    {
        String[] mobileNumbers = { "+91987000000", "+9198700000.0", "-919870000001", "+91+987000000", "+91a987000000", "+91011111110x", "+910x11111111" };

        for (String mobileNumber : mobileNumbers)
        {
            WSResponse response = getOtpResponse(mobileNumber, BASE_URL);
            WSResult result = response.getResult();
            List<String> expectedCode = Arrays.asList(new String[] { "6006", "6008" });
            assertEquals(expectedCode.contains(result.getCode()), true);
            cleanUp(mobileNumber);
        }
    }

    @Test
    public void testSigninNormal() throws JsonMappingException, JsonGenerationException, IOException
    {
        String mobileNumber = "+919870000001";
        String otp = commonTestUtility.getOtp(mobileNumber, BASE_URL);
        String grantType = "client_credentials";
        String clientType = "mobile";
        String deviceId = "123";
        String deviceUserName = "MD";
        String deviceModelName = "iphone6";
        String devicePlatform = "ios";
        String deviceOsVersion = "ios-8";

        WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
        WSResult result = response.getResult();
        assertEquals("20001", result.getCode());
        cleanUp(mobileNumber);
    }

    @Test
    public void testSigninWithNullParams() throws JsonMappingException, JsonGenerationException, IOException
    {

        for (int i = 0; i < 9; i++)
        {
            String mobileNumber = "+919870000002";
            String otp = commonTestUtility.getOtp(mobileNumber, BASE_URL);
            String grantType = "client_credentials";
            String clientType = "mobile";
            String deviceId = "123";
            String deviceUserName = "MD";
            String deviceModelName = "iphone6";
            String devicePlatform = "ios";
            String deviceOsVersion = "ios-8";

            if (i == 0)
                mobileNumber = null;
            if (i == 1)
                otp = null;
            if (i == 2)
                grantType = null;
            if (i == 3)
                clientType = null;
            if (i == 4)
                deviceId = null;
            if (i == 5)
                deviceUserName = null;
            if (i == 6)
                deviceModelName = null;
            if (i == 7)
                devicePlatform = null;
            if (i == 8)
                deviceOsVersion = null;

            WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
            WSResult result = response.getResult();
            assertEquals("6001", result.getCode());
            cleanUp(mobileNumber);
        }
    }

    @Test
    public void testSigninWithInvalidParams() throws JsonMappingException, JsonGenerationException, IOException
    {

        for (int i = 0; i < 7; i++)
        {
            String mobileNumber = "+919870000002";
            String otp = commonTestUtility.getOtp(mobileNumber, BASE_URL);
            String grantType = "client_credentials";
            String clientType = "mobile";
            String deviceId = "123";
            String deviceUserName = "MD";
            String deviceModelName = "iphone6";
            String devicePlatform = "ios";
            String deviceOsVersion = "ios-8";
            String invalidText = "xxxxxxxxxxxxxxxxxxxxx";

            if (i == 0)
            {
                grantType = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("6006", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 1)
            {
                clientType = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("6006", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 2)
            {
                deviceId = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("20001", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 3)
            {
                deviceUserName = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("20001", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 4)
            {
                deviceModelName = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("20001", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 5)
            {
                devicePlatform = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("6006", result.getCode());
                cleanUp(mobileNumber);

            }
            if (i == 6)
            {
                deviceOsVersion = invalidText;
                WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
                WSResult result = response.getResult();
                assertEquals("20001", result.getCode());
                cleanUp(mobileNumber);

            }

            // System.out.println(grantType + "*" + clientType + "*" + deviceId
            // + "*" + deviceUserName + "*" + deviceModelName + "*" +
            // devicePlatform + "*" + deviceOsVersion + "*" + otp);
        }
    }

    @Test
    public void testSigninWithSameNumber() throws JsonMappingException, JsonGenerationException, IOException
    {
        String mobileNumber = "+919870000001";
        String otp = commonTestUtility.getOtp(mobileNumber, BASE_URL);
        String otp2 = commonTestUtility.getOtp(mobileNumber, BASE_URL);
        String grantType = "client_credentials";
        String clientType = "mobile";
        String deviceId = "123";
        String deviceUserName = "MD";
        String deviceModelName = "iphone6";
        String devicePlatform = "ios";
        String deviceOsVersion = "ios-8";

        WSResponse response = getSigninResponse(BASE_URL, mobileNumber, otp, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
        WSResult result = response.getResult();
        assertEquals("20001", result.getCode());

        WSResponse response2 = getSigninResponse(BASE_URL, mobileNumber, otp2, grantType, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion);
        WSResult result2 = response2.getResult();
        assertEquals("20001", result2.getCode());

        cleanUp(mobileNumber);
    }

    // Bulk test for checking multiple parallel connection openinf to DB (data
    // source max connection test)
    @Test
    public void testRequestOtpMultipleWithSameNumberAsync() throws JsonMappingException, JsonGenerationException, IOException
    {
        String mobileNumberStr = null;
        long mobileNumber = 9870000300l;
        for (int i = 0; i < 50; i++)
        {
            mobileNumber++;
            mobileNumberStr = "+91" + mobileNumber;
            asyncOtp(mobileNumberStr, BASE_URL);
        }

    }

    private void asyncOtp(String mobileNumber, String baseURL) throws JsonMappingException, JsonGenerationException, IOException
    {
        WSRequest request = new WSRequest();
        WSRequestOtp requestOut = new WSRequestOtp(mobileNumber);
        request.setRequestOtp(requestOut);
        commonTestUtility.asyncCallWebEndPoint(baseURL + "request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));

    }

}
