package one.group.rest.v1.test;

import java.io.IOException;

import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.WSResponse;
import one.group.utils.Utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ContactsTestUtility
{
    public WSResponse getContacts(String baseURL, String bearer) throws JsonMappingException, JsonGenerationException, IOException
    {
        CommonTestUtility ctu = new CommonTestUtility();
        Response response = ctu.callWebEndPoint(baseURL + "contacts", bearer, HTTPRequestMethodType.GET, null);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        return jsonResponse;
    }
}
