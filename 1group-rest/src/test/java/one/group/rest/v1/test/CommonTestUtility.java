package one.group.rest.v1.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import javax.transaction.Transactional;
import javax.ws.rs.client.AsyncInvoker;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSAddPropertyListing;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.request.WSRequestOtp;
import one.group.entities.api.request.WSSignIn;
import one.group.entities.api.response.WSAccountsResult;
import one.group.entities.api.response.WSClient;
import one.group.entities.api.response.WSNewsFeed;
import one.group.entities.api.response.WSOtp;
import one.group.entities.api.response.WSResponse;
import one.group.utils.Utils;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CommonTestUtility
{
    static Map<String, List<String>> phoneBearerMapper = new HashMap<String, List<String>>();
    static String STATUS_REGISTERED = "STATUS_REGISTERED";
    static String STATUS_UNREGISTERED = "STATUS_UNREGISTERED";
    public static List<Long> registerTime = new ArrayList<Long>();

    public static Map<String, List<String>> getPhoneBearerMapper()
    {
        return phoneBearerMapper;
    }

    public String getOtp(String mobileNumber, String baseURL) throws JsonMappingException, JsonGenerationException, IOException
    {
        CommonTestUtility commonTestUtility = new CommonTestUtility();
        WSRequest request = new WSRequest();
        WSRequestOtp requestOut = new WSRequestOtp(mobileNumber);
        request.setRequestOtp(requestOut);
        Response response = callWebEndPoint(baseURL + "request_otp", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSOtp result = (WSOtp) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSOtp.class);
        String otp = "0000";
        return otp;
    }

    public void getFile(FormDataMultiPart formData, String baseURL, String bearer)
    {
        Response response = callWebEndPointMultiPart(baseURL + "upload", bearer, HTTPRequestMethodType.POST, formData);
    }

    public void postPropertyListing(String baseURL, String token)
    {
        WSRequest request = new WSRequest();
        WSAddPropertyListing requestPropertyListing = new WSAddPropertyListing();

        requestPropertyListing.setPropertyType(PropertyType.RESIDENTIAL.toString().toLowerCase());
        requestPropertyListing.setPropertySubType(PropertySubType.APARTMENT.toString());
        requestPropertyListing.setArea("111");
        requestPropertyListing.setCommissionType("direct");
        requestPropertyListing.setDescription("description");
        requestPropertyListing.setRooms("4bhk");
        requestPropertyListing.setSalePrice("2222222");
        requestPropertyListing.setRentPrice("4444");
        requestPropertyListing.setLocationId("394");
        requestPropertyListing.setPropertyMarket("secondary");

        request.setPropertyListing(requestPropertyListing);

        callWebEndPoint(baseURL + "property_listings", token, HTTPRequestMethodType.POST, Utils.getJsonString(request));
    }

    public List<String> getNewsFeedAccountList(String baseUrl, String token) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> receivingAccountIds = new ArrayList<String>();
        Response response = callWebEndPoint(baseUrl + "news_feed", token, HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        Map result = (Map) jsonResponse.getData();
        Object resultObj = result.get("news_feed");
        String jsonString = Utils.getJsonString(resultObj);
        List newsFeedList = (List) Utils.getInstanceFromJson(jsonString, List.class);
        for (Object newsFeed : newsFeedList)
        {
            WSNewsFeed wsNewsFeed = (WSNewsFeed) Utils.getInstanceFromJson(Utils.getJsonString(newsFeed), WSNewsFeed.class);
            receivingAccountIds.add(wsNewsFeed.getAccountId());

        }
        return receivingAccountIds;
    }

    public static List<List<String>> split(Collection<String> set, int syncBatchSize)
    {
        List<List<String>> splitList = new ArrayList<List<String>>();
        List<String> mainList = new ArrayList<String>();
        mainList.addAll(set);
        int mainListSize = mainList.size();
        int parts = mainListSize / syncBatchSize;
        int modElementSize = mainListSize % syncBatchSize;
        System.out.println(parts + " " + modElementSize);
        int[][] partIndex = new int[parts][2];

        for (int i = 0; i < parts; i++)
        {
            partIndex[i][0] = i * syncBatchSize;
            partIndex[i][1] = (i + 1) * syncBatchSize;
        }

        for (int x = 0; x < parts; x++)
        {
            List<String> partList = new ArrayList<String>();

            for (int i = partIndex[x][0]; i < partIndex[x][1]; i++)
            {
                partList.add(mainList.get(i));
            }
            splitList.add(partList);
        }

        if (modElementSize > 0)
        {
            List<String> modPartList = new ArrayList<String>();
            for (int i = 0; i < modElementSize; i++)
            {
                modPartList.add(mainList.get(syncBatchSize * parts + i));
            }
            splitList.add(modPartList);
        }

        return splitList;
    }

    @Transactional
    public List<String> signin(String mobileNumber, String baseURL) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> bearerAccount = new ArrayList<String>();
        WSRequest request = new WSRequest();
        WSSignIn requestSignin = new WSSignIn();
        requestSignin.setMobileNumber(mobileNumber);
        getOtp(mobileNumber, baseURL);

        requestSignin.setOtp("0000");
        requestSignin.setGrantType("client_credentials");
        requestSignin.setClientType("mobile");
        requestSignin.setDeviceId("123");
        requestSignin.setDeviceUserName("MD");
        requestSignin.setDeviceModelName("iphone6");
        requestSignin.setDevicePlatform("ios");
        requestSignin.setDeviceOsVersion("1os-8");
        requestSignin.setCpsId("aaa");
        requestSignin.setClientVersion("1.1.3");
        request.setSignIn(requestSignin);
        Response response = callWebEndPoint(baseURL + "sign_in", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSClient result = (WSClient) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSClient.class);
        bearerAccount.add(0, result.getAccessToken());
        bearerAccount.add(1, result.getAccountId());
        String accountStatus = getAccountStatus(result.getAccountId(), baseURL, result.getAccessToken());
        if (accountStatus.equals("active"))
        {
            bearerAccount.add(2, STATUS_REGISTERED);
        }
        else
        {
            bearerAccount.add(2, STATUS_UNREGISTERED);
        }
        return bearerAccount;
    }

    @Transactional
    public WSClient signin1(String mobileNumber, String baseURL) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> bearerAccount = new ArrayList<String>();
        WSRequest request = new WSRequest();
        WSSignIn requestSignin = new WSSignIn();
        requestSignin.setMobileNumber(mobileNumber);
        requestSignin.setOtp(getOtp(mobileNumber, baseURL));
        requestSignin.setGrantType("client_credentials");
        requestSignin.setClientType("mobile");
        requestSignin.setDeviceId("123");
        requestSignin.setDeviceUserName("MD");
        requestSignin.setDeviceModelName("iphone6");
        requestSignin.setDevicePlatform("ios");
        requestSignin.setDeviceOsVersion("1os-8");
        request.setSignIn(requestSignin);
        Response response = callWebEndPoint(baseURL + "sign_in", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSClient result = (WSClient) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSClient.class);
        return result;
    }

    @Transactional
    public List<String> signin(String mobileNumber, String baseURL, String otp) throws JsonMappingException, JsonGenerationException, IOException
    {
        List<String> bearerAccount = new ArrayList<String>();
        WSRequest request = new WSRequest();
        WSSignIn requestSignin = new WSSignIn();
        requestSignin.setMobileNumber(mobileNumber);
        requestSignin.setOtp(otp);
        requestSignin.setGrantType("client_credentials");
        requestSignin.setClientType("mobile");
        requestSignin.setDeviceId("123");
        requestSignin.setDeviceUserName("MD");
        requestSignin.setDeviceModelName("iphone6");
        requestSignin.setDevicePlatform("ios");
        requestSignin.setDeviceOsVersion("1os-8");
        request.setSignIn(requestSignin);
        Response response = callWebEndPoint(baseURL + "sign_in", null, HTTPRequestMethodType.POST, Utils.getJsonString(request));

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        WSClient result = (WSClient) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSClient.class);
        bearerAccount.add(0, result.getAccessToken());
        bearerAccount.add(1, result.getAccountId());
        String accountStatus = getAccountStatus(result.getAccountId(), baseURL, result.getAccessToken());
        if (accountStatus.equals("active"))
        {
            bearerAccount.add(2, STATUS_REGISTERED);
        }
        else
        {
            bearerAccount.add(2, STATUS_UNREGISTERED);
        }
        return bearerAccount;
    }

    @Transactional
    public WSResponse register(String firstName, String lastName, List<String> locations, String mobileNumber, String bearer, String baseURL) throws JsonMappingException, JsonGenerationException,
            IOException
    {
        WSRequest request = new WSRequest();
        WSAccount account = new WSAccount();
        account.setFullName(firstName);
        account.setCompanyName(lastName);

        account.setLocations(locations);
        request.setAccount(account);

        long startTime = System.currentTimeMillis();
        Response response = callWebEndPoint(baseURL + "registration", bearer, HTTPRequestMethodType.POST, Utils.getJsonString(request));
        long endTime = System.currentTimeMillis();

        registerTime.add(endTime - startTime);

        String json = response.readEntity(String.class);
        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);
        // System.out.println(jsonResponse);
        if (phoneBearerMapper.get(mobileNumber) != null)
            phoneBearerMapper.get(mobileNumber).add(2, STATUS_REGISTERED);

        return jsonResponse;

    }

    public Response callWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {

        ClientConfig config = new ClientConfig();
        // config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }

    public Response callWebEndPointMultiPart(String url, String token, HTTPRequestMethodType httpMethod, FormDataMultiPart data)
    {

        ClientConfig config = new ClientConfig();
        config.register(MultiPartFeature.class);

        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);

        Invocation.Builder invocationBuilder = target.request();
        Invocation invocation = null;

        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<FormDataMultiPart> entity = Entity.entity(data, javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE);
            invocation = invocationBuilder.build(httpMethod.toString(), entity);
        }
        else
        {
            invocation = invocationBuilder.build(httpMethod.toString());
        }

        invocationBuilder.header("accept", "*/*");

        Response response = invocation.invoke();
        return response;
    }

    public Future<Response> asyncCallWebEndPoint(String url, String token, HTTPRequestMethodType httpMethod, String data)
    {
        ClientConfig config = new ClientConfig();
        javax.ws.rs.client.Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(url);
        Future<Response> futureResponse = null;
        Invocation.Builder invocationBuilder = target.request();
        AsyncInvoker asycInvoker = invocationBuilder.async();
        if (token != null)
        {
            invocationBuilder.header("authorization", "bearer " + token);
        }

        if (data != null)
        {
            Entity<String> entity = Entity.entity(data, javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
            futureResponse = asycInvoker.post(entity);
        }
        invocationBuilder.header("accept", "*/*");
        return futureResponse;
    }

    public String getAccountStatus(String accountId, String url, String token) throws JsonMappingException, JsonGenerationException, IOException
    {
        // System.out.println(url + "accounts/" + accountId);
        Response response = callWebEndPoint(url + "accounts/" + accountId, token, HTTPRequestMethodType.GET, null);
        String json = response.readEntity(String.class);

        WSResponse jsonResponse = (WSResponse) Utils.getInstanceFromJson(json, WSResponse.class);

        WSAccountsResult accountResult = (WSAccountsResult) Utils.getInstanceFromJson(Utils.getJsonString(jsonResponse.getData()), WSAccountsResult.class);

        Set<one.group.entities.api.response.WSAccount> wsAccount = accountResult.getAccountsResult();
        String status = null;

        for (one.group.entities.api.response.WSAccount account : wsAccount)
        {

            status = account.getStatus().toString();
        }

        return status;
    }

    public void averageRegisteredTime()
    {
        Long total = 0l;
        for (Long value : registerTime)
        {
            total = total + value;
        }

        total = total / registerTime.size();
        System.out.println("Registration Times: " + registerTime);
        System.out.println("Average registerd time: " + total);
    }
}