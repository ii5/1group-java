import java.util.ArrayList;
import java.util.List;

import one.group.entities.api.response.WSChatMessage;
import one.group.entities.api.response.WSParticipants;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatThreadTwo
{

    private String chatThreadId;

    private Integer maxChatMessageIndex;

    private List<WSChatMessage> chatMessages;

    private List<WSParticipants> participants;

    public WSChatThreadTwo()
    {
    }

    public WSChatThreadTwo(final String chatThreadId, final Integer maxChatMessageIndex)

    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "The Chat thread identifier should not be null or empty.");
        Validation.isTrue(maxChatMessageIndex > -2, "Invalid value for max_chat_message_index.");

        this.chatThreadId = chatThreadId;
        this.maxChatMessageIndex = maxChatMessageIndex;
        this.chatMessages = new ArrayList<WSChatMessage>();
        this.participants = new ArrayList<WSParticipants>();
    }

    public WSChatThreadTwo(String chatThreadId, Integer maxChatMessageIndex, List<WSChatMessage> wsChatMessages, List<WSParticipants> participants)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "The Chat thread identifier should not be null or empty.");
        Validation.isTrue(maxChatMessageIndex > -2, "Invalid value for max_chat_message_index.");
        Validation.isTrue(!Utils.isNull(wsChatMessages), "Chat message object should not be null or empty");
        Validation.isTrue(!Utils.isNull(participants), "Participants object should not be null or empty");

        this.chatThreadId = chatThreadId;
        this.maxChatMessageIndex = maxChatMessageIndex;
        this.chatMessages = wsChatMessages;
        this.participants = participants;

    }

    public List<WSChatMessage> getChatMessages()
    {
        return chatMessages;
    }

    public void setChatMessages(final List<WSChatMessage> chatMessages)
    {
        this.chatMessages = chatMessages;
    }

    public void addChatMessage(final WSChatMessage chatMessage)
    {
        this.chatMessages.add(chatMessage);
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void addParticipant(final WSParticipants participant)
    {
        this.participants.add(participant);
    }

    public Integer getMaxChatMessageIndex()
    {
        return maxChatMessageIndex;
    }

    public List<WSParticipants> getParticipants()
    {
        return participants;
    }

    public void setParticipants(final List<WSParticipants> participants)
    {
        this.participants = participants;
    }

    @Override
    public String toString()
    {
        return "WSChatThread [chat_thread_id=" + chatThreadId + ", max_chat_message_index=" + maxChatMessageIndex + ", chat_messages=" + chatMessages + ", participants=" + participants + "]";
    }
}
