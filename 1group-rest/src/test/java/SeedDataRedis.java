import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import one.group.cache.dao.ClientCacheDAO;
import one.group.cache.dao.impl.RedisBaseDAOImpl;
import one.group.core.enums.AccountType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.cache.AccountKey;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.PropertyListing;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.utils.Utils;

public class SeedDataRedis
{
    private AccountJpaDAO accountJpaDAO;
    private ClientJpaDAO clientJpaDAO;
    private ClientCacheDAO clientCacheDAO;
    private PropertyListingJpaDAO propertyJpaDAO;
    private RedisBaseDAOImpl redisBaseDAOImpl;

    @Transactional
    public static void main(String[] args)
    {

        SeedDataRedis redisSeed = new SeedDataRedis();
        redisSeed.initiate();

        // seedData.buildAccountAssociation(accountJpaDAO, redisBaseDAOImpl);
        // seedData.buildOnlineAccounts(accountJpaDAO, redisBaseDAOImpl);
        // seedData.buildClients(clientJpaDAO, accountJpaDAO, clientCacheDAO);
        //
        // for (Account a : accountJpaDAO.findAll())
        // {
        // System.out.println();System.out.println();
        // System.out.println(String.format("Account [id=%s firstname=%s, lastname=%s, profilePicUrl=%s, primaryPhoneNumber=%s]",
        // a.getId(), a.getFirstname(), a.getLastname(),
        // a.getContactPicture().getUrl(),
        // a.getPrimaryPhoneNumber().getNumber()));
        // System.out.println("= Start Clients =");
        // for (Client c : clientJpaDAO.getAllClientsOfAccount(a.getId()))
        // {
        // System.out.println(String.format("\t Client [id=%s]", c.getId()));
        // }
        // System.out.println("= End Clients =");
        // System.out.println();
        // System.out.println("= Start Properties =");
        // for (Property p : propertyJpaDAO.findPropertiesForAgent(a.getId(), 0,
        // 100))
        // {
        // System.out.println(String.format("\t Property [id=%s, referenceid=%s]",
        // p.getId(), p.getReferenceId()));
        // }
        // System.out.println("= End Properties =");
        //
        // }
        //
        // System.out.println("Building Redis data done.");
    }

    @Transactional
    public void initiate()
    {
        buildAccountAssociation(accountJpaDAO, redisBaseDAOImpl);
        buildOnlineAccounts(accountJpaDAO, redisBaseDAOImpl);
        buildClients(clientJpaDAO, accountJpaDAO, clientCacheDAO);

        for (Account a : accountJpaDAO.findAll())
        {
            System.out.println();
            System.out.println();
            System.out.println(String.format("Account [id=%s firstname=%s, lastname=%s, profilePicUrl=%s, primaryPhoneNumber=%s]", a.getIdAsString(), a.getFullName(), a.getCompanyName(), a
                    .getFullPhoto().getUrl(), a.getPrimaryPhoneNumber().getNumber()));
            System.out.println("= Start Clients =");
            for (Client c : clientJpaDAO.getAllClientsOfAccount(a.getIdAsString()))
            {
                System.out.println(String.format("\t Client [id=%s, type=%s]", c.getIdAsString(), c.getDeviceModel()));
            }
            System.out.println("= End Clients =");
            System.out.println();
            System.out.println("= Start Properties =");

            List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
            statusList.add(BroadcastStatus.ACTIVE);
            statusList.add(BroadcastStatus.EXPIRED);

            for (PropertyListing p : propertyJpaDAO.fetchAllPropertyListingByAccountId(a.getIdAsString(), statusList))
            {
                System.out.println(String.format("\t Property [id=%s, referenceid=%s]", p.getIdAsString(), p.getShortReference()));
            }
            System.out.println("= End Properties =");

        }

        System.out.println("Seeding Redis done.");
    }

    @Transactional
    private void buildAccountAssociation(AccountJpaDAO accountJpaDAO, RedisBaseDAOImpl redisBaseDAOImpl)
    {

        // build online data
        for (Account a : accountJpaDAO.findAll())
        {
            if (a.getType().equals(AccountType.AGENT))
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("account_id", a.getIdAsString());
                Set<Account> associatedAccounts = ((Agent) a).getContacts();

                for (Account x : associatedAccounts)
                {
                    redisBaseDAOImpl.addToSet(AccountKey.SET_ACCOUNT_ALLIANCE.getFormedKey(params), x.getIdAsString());
                }
            }
        }
    }

    private void buildOnlineAccounts(AccountJpaDAO accountJpaDAO, RedisBaseDAOImpl redisBaseDAOImpl)
    {

        for (Account a : accountJpaDAO.findAll())
        {
            Map<String, String> values = new HashMap<String, String>();
            values.put("account_id", a.getIdAsString());
            redisBaseDAOImpl.setValue(AccountKey.HASH_ACCOUNT_ONLINE.getFormedKey(values), a.getIdAsString());
        }
    }

    private void buildClients(ClientJpaDAO clientJpaDAO, AccountJpaDAO accountJpaDAO, ClientCacheDAO clientCacheDAO)
    {
        Collection<Account> accounts = accountJpaDAO.findAll();
        for (Account a : accounts)
        {

            if (!Utils.isNullOrEmpty(a.getIdAsString()))
            {

                List<Client> clients = clientJpaDAO.getAllClientsOfAccount(a.getIdAsString());
                for (int i = 0; i < clients.size(); i++)
                {
                    // System.out.println(">" + i);
                    clientCacheDAO.addClientForAccount(a.getIdAsString(), clients.get(i).getIdAsString());
                }

            }

        }
    }

    public AccountJpaDAO getAccountJpaDAO()
    {
        return accountJpaDAO;
    }

    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
    {
        this.accountJpaDAO = accountJpaDAO;
    }

    public ClientJpaDAO getClientJpaDAO()
    {
        return clientJpaDAO;
    }

    public void setClientJpaDAO(ClientJpaDAO clientJpaDAO)
    {
        this.clientJpaDAO = clientJpaDAO;
    }

    public ClientCacheDAO getClientCacheDAO()
    {
        return clientCacheDAO;
    }

    public void setClientCacheDAO(ClientCacheDAO clientCacheDAO)
    {
        this.clientCacheDAO = clientCacheDAO;
    }

    public PropertyListingJpaDAO getPropertyJpaDAO()
    {
        return propertyJpaDAO;
    }

    public void setPropertyJpaDAO(PropertyListingJpaDAO propertyJpaDAO)
    {
        this.propertyJpaDAO = propertyJpaDAO;
    }

    public RedisBaseDAOImpl getRedisBaseDAOImpl()
    {
        return redisBaseDAOImpl;
    }

    public void setRedisBaseDAOImpl(RedisBaseDAOImpl redisBaseDAOImpl)
    {
        this.redisBaseDAOImpl = redisBaseDAOImpl;
    }

}