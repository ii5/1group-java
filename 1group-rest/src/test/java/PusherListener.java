import com.pusher.client.Pusher;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;


public class PusherListener implements ConnectionEventListener, ChannelEventListener, SubscriptionEventListener
{
    
    public static void main(String[] args)
    {
        PusherListener p = new PusherListener();
        Pusher pusher = new Pusher("df6f4b06a4bbdd47e6ee");
        pusher.connect(p, ConnectionState.ALL);
    }
    
    @Override
    public void onEvent(String channel, String event, String data)
    {

        if (data.contains("chat_message"))
        {
            System.out.println(data);
        }
    }

    @Override
    public void onSubscriptionSucceeded(String arg0)
    {
        System.out.println("Subscribed: " + arg0);
    }

    @Override
    public void onConnectionStateChange(ConnectionStateChange change)
    {
        System.out.println("State change: [" + change.getPreviousState() + " > " + change.getCurrentState());
    }

    @Override
    public void onError(String message, String code, Exception exception)
    {
        System.err.print("Exception while connecting : [message=" + message + ", code=" + code + ", exception=" + exception.getMessage() + "].");

    }
}
