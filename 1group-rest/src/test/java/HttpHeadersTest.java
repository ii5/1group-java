import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * 
 * @author nyalfernandes
 *
 */
public class HttpHeadersTest implements HttpHeaders
{
    private static MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<String, String>();
    private MediaType mediaType;
    
    public HttpHeadersTest(String authToken)
    {
        requestHeaders.get("authorization").add(authToken);
        this.mediaType = MediaType.APPLICATION_JSON_TYPE;
    }
    
    public HttpHeadersTest(String authToken, MediaType mediaType)
    {
        this(authToken);
        this.mediaType = mediaType;
    }
    
    static 
    {
        requestHeaders.put("authorization", new ArrayList<String>());
    }

    @Override
    public List<Locale> getAcceptableLanguages()
    {
        return Arrays.asList(Locale.getAvailableLocales());
    }

    @Override
    public List<MediaType> getAcceptableMediaTypes()
    {
        return null;
    }

    @Override
    public Map<String, Cookie> getCookies()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Date getDate()
    {
        return new Date();
    }

    @Override
    public String getHeaderString(String arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Locale getLanguage()
    {
        return Locale.getDefault();
    }

    @Override
    public int getLength()
    {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public MediaType getMediaType()
    {
        return mediaType;
    }

    @Override
    public List<String> getRequestHeader(String key)
    {
        return requestHeaders.get(key);
    }

    @Override
    public MultivaluedMap<String, String> getRequestHeaders()
    {
        return requestHeaders;
    }

}
