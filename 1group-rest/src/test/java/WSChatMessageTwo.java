import one.group.entities.api.response.WSChatMessage;

public class WSChatMessageTwo
{
    private WSChatMessage chatMessage;

    public WSChatMessage getChatMessage()
    {
        return chatMessage;
    }

    public void setChatMessage(WSChatMessage chatMessage)
    {
        this.chatMessage = chatMessage;
    }

    public WSChatMessageTwo()
    {
        // TODO Auto-generated constructor stub
    }

    @Override
    public String toString()
    {
        return "WSChatMessageTwo [chatMessage=" + chatMessage + "]";
    }

}
