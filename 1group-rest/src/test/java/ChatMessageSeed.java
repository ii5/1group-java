import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import one.group.entities.api.response.WSClient;
import one.group.entities.api.response.WSResponse;
import one.group.rest.v1.test.ChatTestUtility;
import one.group.rest.v1.test.CommonTestUtility;
import one.group.utils.Utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ChatMessageSeed implements ConnectionEventListener, ChannelEventListener, SubscriptionEventListener
{

    private static long start;
    private static long end;
    private static int cursor = 0;
    private static long[] delta = new long[1000];
    private static Map<String, WSClient> wsClientMap = new HashMap<String, WSClient>();
    private static String url = "http://localhost:8080/movein-rest/v1/";

    public static void main(String[] args) throws Exception
    {

        ChatTestUtility chatUtility = new ChatTestUtility();
        CommonTestUtility utility = new CommonTestUtility();
        String mobileNumber1 = "+917506497079";
        String mobileNumber2 = "+917506497080";

        WSClient client1 = (WSClient) Utils
                .getInstanceFromJson(
                        "{\"initial_sync_index\":8,\"push_channel\":\"H9B46XS5DGG0\",\"push_key\":\"a6df7ff50be87b2bf2fd\",\"access_token\":\"aa9f2e62-39ab-48d1-959e-93762660ae80\",\"account_id\":\"3210413885421695\",\"scope\":\"standard_client\",\"token_type\":\"bearer\"}",
                        WSClient.class); // utility.signin1(mobileNumber1, url);
        WSClient client2 = (WSClient) Utils
                .getInstanceFromJson(
                        "{\"initial_sync_index\":436,\"push_channel\":\"N57TC70W9BVT\",\"push_key\":\"a6df7ff50be87b2bf2fd\",\"access_token\":\"08d17da9-bac2-487f-a74e-ffaaffea45f1\",\"account_id\":\"8092124115119069\",\"scope\":\"standard_client\",\"token_type\":\"bearer\"}",
                        WSClient.class); // utility.signin1(mobileNumber2, url);

        wsClientMap.put(client1.getPushChannel(), client1);
        wsClientMap.put(client2.getPushChannel(), client2);

        System.out.println("Client1 : " + client1);
        System.out.println("Client2 : " + client2);

        System.out.println();
        System.out.println("Client 1 : " + Utils.getJsonString(client1));
        System.out.println("Client 2 : " + Utils.getJsonString(client2));

        ChatMessageSeed seedOne = new ChatMessageSeed();
        Pusher pusherOne = new Pusher(client2.getPushKey());
        pusherOne.connect(seedOne, ConnectionState.ALL);
        Channel channelOne = pusherOne.subscribe(client1.getPushChannel(), seedOne);
        channelOne.bind("", seedOne);

        ChatMessageSeed seedTwo = new ChatMessageSeed();
        Pusher pusherTwo = new Pusher(client2.getPushKey());
        pusherTwo.connect(seedTwo, ConnectionState.ALL);
        Channel channelTwo = pusherTwo.subscribe(client2.getPushChannel(), seedTwo);
        channelTwo.bind("", seedTwo);

        // Thread.sleep(5000);

        for (int i = 0; i < delta.length; i++)
        {
            System.out.println();

            start = System.currentTimeMillis();
            WSResponse response = chatUtility.sendTextChatMessage(client1.getAccountId(), client2.getAccountId(), "[" + i + "] Hi There! " + client2.getAccountId(), url, client1.getAccessToken());
            System.out.println("Message Server Response: " + response);
            Thread.sleep(500);

            cursor++;
        }

        String chatThreadId = "chat-thread-3210413885421695-8092124115119069";
        String token = "08d17da9-bac2-487f-a74e-ffaaffea45f1";
        System.out.println("");
        for (int i = 0; i < delta.length; i++)
        {

            Integer receivedIndex = i;
            Integer readIndex = null;
            WSResponse response1 = chatUtility.updateChatThreadCursor(chatThreadId, receivedIndex, readIndex, url, token);

            Thread.sleep(500);
            receivedIndex = null;
            readIndex = i;
            WSResponse response2 = chatUtility.updateChatThreadCursor(chatThreadId, receivedIndex, readIndex, url, token);

            System.out.println(i + " :" + response1);
            System.out.println(i + " :" + response2);
            System.out.println("");
            Thread.sleep(500);
        }

        // System.out.println("Cursor Server Response: " + response);
    }

    @Override
    public void onEvent(String channel, String event, String data)
    {

        if (data.contains("chat_message"))
        {

            WSChatMessageTwo chatMessage;
            try
            {
                Thread.sleep(2000);
                chatMessage = (WSChatMessageTwo) Utils.getInstanceFromJson(data, WSChatMessageTwo.class);
                WSClient clientDetails = wsClientMap.get(channel);

                ChatTestUtility chatUtility = new ChatTestUtility();
                Integer readIndex = 0;// chatMessage.getChatMessage().getIndex();
                Integer receivedIndex = chatMessage.getChatMessage().getIndex();
                // WSResponse response =
                // chatUtility.updateChatThreadCursor(chatMessage.getChatMessage().getChatThreadId(),
                // receivedIndex, readIndex, url,
                // clientDetails.getAccessToken());
                // System.out.println("Cursor Server Response: " + response);

            }
            catch (JsonMappingException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (JsonGenerationException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        else if (data.contains("chat_cursor"))
        {
            // System.out.println(">>>>>" + data + "<<<<<");
            WSChatThreadTwo chatThread = new WSChatThreadTwo();
            try
            {
                chatThread = (WSChatThreadTwo) Utils.getInstanceFromJson(data, WSChatThreadTwo.class);
            }
            catch (JsonMappingException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (JsonGenerationException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            WSClient clientDetails = wsClientMap.get(channel);

        }
    }

    @Override
    public void onSubscriptionSucceeded(String arg0)
    {
        System.out.println("Subscribed: " + arg0);
    }

    @Override
    public void onConnectionStateChange(ConnectionStateChange change)
    {
        System.out.println("State change: [" + change.getPreviousState() + " > " + change.getCurrentState());
    }

    @Override
    public void onError(String message, String code, Exception exception)
    {
        System.err.print("Exception while connecting : [message=" + message + ", code=" + code + ", exception=" + exception.getMessage() + "].");

    }
}
