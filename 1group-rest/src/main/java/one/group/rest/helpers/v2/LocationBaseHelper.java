package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.MAX_TOKEN_COUNT;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import one.group.core.Constant;
import one.group.core.enums.CmsType;
import one.group.core.enums.HTTPResponseType;
import one.group.entities.api.request.WSLocationDB;
import one.group.entities.api.request.WSLocationMasterCity;
import one.group.entities.api.request.WSLocationMasterData;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.WSCityLocations;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.WSLocation;
import one.group.entities.api.response.WSLocationResponse;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Configuration;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.ParentLocations;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.CityService;
import one.group.services.CmsService;
import one.group.services.ConfigurationService;
import one.group.services.LocationAdminService;
import one.group.services.LocationService;
import one.group.services.ParentLocationService;
import one.group.services.helpers.CmsConnectionFactory;
import one.group.services.helpers.CmsServiceObject;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * 
 * @author sanilshet
 *
 */
public class LocationBaseHelper
{
    private static Logger logger = LoggerFactory.getLogger(LocationBaseHelper.class);

    private String cdnImageURL;

    private String cdnImageFolder;

    private LocationService locationService;

    private LocationAdminService locationAdminService;

    private CityService cityService;

    private CmsService cmsService;

    private ParentLocationService parentLocationService;

    private CmsConnectionFactory connectionFactory;

    private ConfigurationService configurationService;

    public ParentLocationService getParentLocationService()
    {
        return parentLocationService;
    }

    public void setParentLocationService(ParentLocationService parentLocationService)
    {
        this.parentLocationService = parentLocationService;
    }

    public String getCdnImageURL()
    {
        return cdnImageURL;
    }

    public String getCdnImageFolder()
    {
        return cdnImageFolder;
    }

    public void setCdnImageURL(String cdnImageURL)
    {
        this.cdnImageURL = cdnImageURL;
    }

    public void setCdnImageFolder(String cdnImageFolder)
    {
        this.cdnImageFolder = cdnImageFolder;
    }

    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    public LocationAdminService getLocationAdminService()
    {
        return locationAdminService;
    }

    public void setLocationAdminService(LocationAdminService locationAdminService)
    {
        this.locationAdminService = locationAdminService;
    }

    public CityService getCityService()
    {
        return cityService;
    }

    public void setCityService(CityService cityService)
    {
        this.cityService = cityService;
    }

    public CmsConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(CmsConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public CmsService getCmsService()
    {
        return cmsService;
    }

    public void setCmsService(CmsService cmsService)
    {
        this.cmsService = cmsService;
    }

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public WSCityLocations getAllLocalities(String cityIds) throws Abstract1GroupException, Exception
    {
        List<String> ListCityIds = new ArrayList<String>();
        if (cityIds != null)
        {
            validate("cityIds", cityIds, NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));
            String[] cityIdArray = cityIds.split(",");
            ListCityIds.addAll(Arrays.asList(cityIdArray));
        }

        WSCityLocations wsCityLocation = locationService.getLocalities(ListCityIds);
        return wsCityLocation;
    }

    public WSLocationResponse fetchLocalityNameByLocationId(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader) throws Abstract1GroupException, Exception
    {
        String locationIds = Utils.fetchValueFromPathData(uriInfo, "location_ids");
        // validations
        validate("location_ids", locationIds, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));
        String[] locationIdArray = locationIds.split(",");
        Set<String> locationIdSet = new HashSet<String>(Arrays.asList(locationIdArray));
        WSLocationResponse wsResponse = new WSLocationResponse();

        Set<WSLocation> localitySet = new HashSet<WSLocation>();
        for (String locationId : locationIdSet)
        {
            locationId = locationId.trim();
            Location location = locationService.fetchLocalityNameByLocationId(locationId);
            WSLocation wsLocality = new WSLocation();
            if (location.getParentId() != null)
            {
                wsLocality.setSubLocality(location.getLocalityName());
                Location parentLocation = locationService.fetchLocalityNameByLocationId(location.getParentId());
                wsLocality.setName(parentLocation.getLocalityName());
                wsLocality.setId(parentLocation.getId());
            }
            else
            {
                wsLocality.setId(location.getId());
                wsLocality.setName(location.getLocalityName());
            }

            localitySet.add(wsLocality);
        }
        wsResponse.setLocations(localitySet);
        return wsResponse;
    }

    public int compareLocations(UriInfo uriInfo, HttpHeaders httpHeaders, String json2) throws JsonMappingException, JsonGenerationException, IOException, RequestValidationException,
            General1GroupException
    {
        WSRequest wsRequest = null;
        String publishLocations = null;
        String publishLocationsCities = null;
        if (json2 != null)
        {
            wsRequest = (WSRequest) Utils.getInstanceFromJson(json2, WSRequest.class);
            json2 = wsRequest.getLocationMaster() == null ? null : json2;
            publishLocations = (!Utils.isNull(wsRequest.getLocationMaster())) ? wsRequest.getLocationMaster().toString() : null;

        }
        validate("publish_locations", publishLocations, NOT_NULL());
        if (publishLocations != null)
        {
            publishLocationsCities = (!Utils.isNull(wsRequest.getLocationMaster().getCities())) ? wsRequest.getLocationMaster().getCities().toString() : null;
            validate("cities", publishLocationsCities, NOT_NULL());
        }

        if (wsRequest.getLocationMaster().getCities().size() <= 0)
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Cities cannot be blank");
        }

        List<String> cityIds = new ArrayList<String>();
        for (WSLocationMasterCity locationMasterCity : wsRequest.getLocationMaster().getCities())
        {
            if (locationMasterCity.getId() == null)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "City ids cannot be blank");
            }

            cityIds.add(locationMasterCity.getId());
        }
        return locationService.compareLocations(cityIds);
    }

    public boolean compareWithParentLocations(UriInfo uriInfo, HttpHeaders httpHeaders, String json2) throws JsonMappingException, JsonGenerationException, IOException, RequestValidationException,
            General1GroupException
    {
        WSRequest wsRequest = null;
        String publishLocations = null;
        if (json2 != null)
        {
            wsRequest = (WSRequest) Utils.getInstanceFromJson(json2, WSRequest.class);
            json2 = wsRequest.getLocationMaster() == null ? null : json2;
            publishLocations = (!Utils.isNull(wsRequest.getLocationMaster())) ? wsRequest.getLocationMaster().toString() : null;
        }
        validate("publish_locations", publishLocations, NOT_NULL());
        if (wsRequest.getLocationMaster().getCities().size() <= 0)
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "City ids cannot be blank");
        }

        List<String> cityIds = new ArrayList<String>();
        for (WSLocationMasterCity locationMasterCity : wsRequest.getLocationMaster().getCities())
        {
            cityIds.add(locationMasterCity.getId());
        }
        return locationService.compareParentLocations(cityIds);
    }

    /**
     * Function to update live location tables and generate json versioned files
     * 
     * @param json2
     * @param httpHeaders
     * @param uriInfo
     * 
     * @throws IOException
     * @throws General1GroupException
     * @throws RequestValidationException
     */
    public void updateLiveLocationTable(UriInfo uriInfo, HttpHeaders httpHeaders, String json2) throws IOException, General1GroupException, RequestValidationException
    {
        WSRequest wsRequest = null;
        String publishLocations = null;

        if (json2 != null)
        {
            wsRequest = (WSRequest) Utils.getInstanceFromJson(json2, WSRequest.class);
            json2 = wsRequest.getLocationMaster() == null ? null : json2;
            publishLocations = (!Utils.isNull(wsRequest.getLocationMaster())) ? wsRequest.getLocationMaster().toString() : null;
        }
        validate("publish_locations", publishLocations, NOT_NULL());

        // throw error if city ids are blank in request
        if (wsRequest.getLocationMaster().getCities().size() <= 0)
        {
            throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "City ids cannot be blank");
        }

        long currentTime = System.currentTimeMillis();

        HashMap<String, String> locationsMap = new HashMap<String, String>();
        List<WSLocationMasterData> masterDataList = new ArrayList<WSLocationMasterData>();
        List<String> cityIds = new ArrayList<String>();

        // iterate through each city and check for changes
        for (WSLocationMasterCity locationMasterCity : wsRequest.getLocationMaster().getCities())
        {
            if (cityIds.contains(locationMasterCity.getId()))
            {
                continue;
            }

            City city = cityService.getCityById(locationMasterCity.getId());
            if (city == null)
            {
                throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", true, "City id " + locationMasterCity.getId() + " not found");
            }
            Boolean isUpdated = false;
            List<WSLocation> list = locationService.getLocalitiesByCityId(city.getId());
            List<WSLocation> listAdmin = locationAdminService.getLocalitiesByCityId(city.getId());

            List<String> locationIds = new ArrayList<String>();
            for (WSLocation locId : list)
            {
                locationIds.add(locId.getId());
            }

            for (WSLocation locationAdmin : listAdmin)
            {
                if (!list.contains(locationAdmin))
                {
                    isUpdated = true;
                    break;
                }
            }
            // here we check for change in city id, if one location having city
            // id (1) is changed to city id(10),then there should be two updates
            // for city id 1 and 10, because one location will add for city id
            // 10 and same location will be deleted ( not actually) for city id
            // 1
            if (list.size() > listAdmin.size())
            {
                isUpdated = true;
            }
            // if any difference is found between admin and live location
            // tables, process it and upload the latest version of city wise
            // location json file to s3 server
            logger.debug("City: " + city.getName() + ", isUpdated: " + isUpdated + ", local: " + list.size() + ", admin: " + listAdmin.size());

            if (isUpdated)
            {
                BufferedWriter bw = null;
                try
                {
                    // here we get all top parent locations which are needed to
                    // push in json file
                    List<ParentLocations> pl = parentLocationService.getAllLocationsByCityId(city.getId());
                    String json = Utils.getJsonString(pl);
                    // check if file exists in system, else create new one

                    logger.debug("Locations of city from parent locations: " + pl.size() + ", json: " + json);
                    File file = new File(cdnImageFolder + "/" + connectionFactory.getMasterFolder() + "/locations-" + city.getName() + "-" + currentTime + ".json");
                    logger.info("Location File:" + file.getAbsolutePath());
                    if (!file.exists())
                    {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file.getAbsolutePath());
                    bw = new BufferedWriter(fw);

                    bw.write(json);
                    bw.flush();

                    // upload json to s3 server -> city wise locations
                    String key = connectionFactory.getMasterFolder() + File.separator + "locations-" + city.getName() + "-" + currentTime + ".json";
                    String bucket = connectionFactory.getBucket();
                    CmsServiceObject config = new CmsServiceObject();
                    config.setBucketName(bucket);
                    config.setAwsFile(file);
                    config.setKey(key);
                    config.setCmsType(CmsType.AWS);
                    // upload to s3 now
                    String awsFileUploadedUrl = cmsService.UploadFile(config);
                    locationsMap.put(city.getName(), awsFileUploadedUrl);
                    WSLocationMasterData masterData = new WSLocationMasterData();
                    masterData.setCityName(city.getName());
                    masterData.setCityId(city.getId());
                    masterData.setUrl(awsFileUploadedUrl);
                    masterDataList.add(masterData);
                }
                catch (Exception e)
                {
                    logger.error("Error while publishing city location files to AWS.", e);
                    throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
                }
                finally
                {
                    Utils.close(bw);
                }
                // here we copy new location data from admin table to live
                // location table city wise
                locationService.updateLiveLocationTable(city.getId());
            }
            cityIds.add(locationMasterCity.getId());
        }

        // also we need to create a json file which contains mapping of city
        // name and respective s3 location url
        if (masterDataList.size() > 0)
        {
            WSLocationDB locationDB = new WSLocationDB();
            locationDB.setLocationsMasterData(masterDataList);
            uploadLocationMapFile(locationDB);
        }
    }

    public String generateCityJsonData() throws General1GroupException, IOException
    {
        return null;
    }

    /**
     * Function to upload location mapped file to amazon
     * 
     * @param locationMap
     * @throws IOException
     * @throws General1GroupException
     */
    private void uploadLocationMapFile(WSLocationDB locationMap) throws IOException, General1GroupException
    {
        long currentTime = System.currentTimeMillis();
        String bucket = connectionFactory.getBucket();
        Configuration getLocationsUrl = configurationService.getValueByKey(Constant.LOCATIONS_DB_KEY);
        WSLocationDB locationDB2 = new WSLocationDB();
        BufferedWriter bufferedWriter = null;
        List<WSCity> wsCities = cityService.getAllCities();
        Map<String, String> wsCityIdNameMap = new HashMap<String, String>();
        for (WSCity wsCity : wsCities)
        {
            wsCityIdNameMap.put(wsCity.getName(), wsCity.getId());
        }

        if (getLocationsUrl != null)
        {
            String url = getLocationsUrl.getValue();
            String result = java.net.URLDecoder.decode(url, Constant.UTF8_ENCODING);
            String fileName = result.substring(result.lastIndexOf('/') + 1, result.length());
            String key = connectionFactory.getMasterFolder() + "/" + fileName;
            CmsServiceObject urlConfig = new CmsServiceObject();
            urlConfig.setBucketName(bucket);
            urlConfig.setKey(key);
            urlConfig.setCmsType(CmsType.AWS);
            urlConfig.setCloudFrountURL(connectionFactory.getCloudFrontUrl());
            String jsons = cmsService.getFile(urlConfig);

            // here we merge previous location mapped file with changed location
            // url
            WSLocationDB locationDB = (WSLocationDB) Utils.getInstanceFromJson(jsons, WSLocationDB.class);
            List<WSLocationMasterData> masterDataList = new ArrayList<WSLocationMasterData>();
            // for each city and its location url, if any changes update its
            // corresponding location url
            for (WSLocationMasterData previousLocationMapping : locationDB.getLocationsMasterData())
            {
                WSLocationMasterData master = new WSLocationMasterData();
                master.setCityName(previousLocationMapping.getCityName());
                String previousCityId = previousLocationMapping.getCityId();
                if (previousCityId == null)
                    previousCityId = wsCityIdNameMap.get(master.getCityName());

                if (previousCityId == null)
                    throw new General1GroupException(HTTPResponseType.NOT_FOUND, "404", true, "No city id corresponding to city name in existing json file");

                master.setCityId(previousCityId);
                master.setUrl(previousLocationMapping.getUrl());

                for (WSLocationMasterData updatedMapping : locationMap.getLocationsMasterData())
                {
                    if (previousLocationMapping.getCityName().equals(updatedMapping.getCityName()))
                    {
                        master.setUrl(updatedMapping.getUrl());
                        master.setCityId(updatedMapping.getCityId());
                    }
                    else
                    {
                        // always add new cities
                        WSLocationMasterData newCityMaster = new WSLocationMasterData();
                        newCityMaster.setCityName(updatedMapping.getCityName());
                        newCityMaster.setCityId(updatedMapping.getCityId());
                        newCityMaster.setUrl(updatedMapping.getUrl());
                        if (!masterDataList.contains(newCityMaster))
                        {
                            masterDataList.add(newCityMaster);
                        }
                    }
                }

                if (!masterDataList.contains(master))
                {
                    masterDataList.add(master);
                }
            }
            locationDB2.setLocationsMasterData(masterDataList);
        }
        else
        {
            locationDB2 = locationMap;
        }

        File file = new File(cdnImageFolder + "/" + connectionFactory.getMasterFolder() + "/locations-" + currentTime + ".json");
        if (!file.exists())
        {
            file.createNewFile();
        }

        try
        {
            FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
            bufferedWriter = new BufferedWriter(fileWriter);
            String json = Utils.getJsonString(locationDB2);
            bufferedWriter.write(json);
            bufferedWriter.flush();
        }
        catch (Exception e)
        {
            logger.error("Error writing location files to AWS.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        finally
        {
            Utils.close(bufferedWriter);
        }
        String saveKey = connectionFactory.getMasterFolder() + File.separator + "locations-" + currentTime + ".json";
        CmsServiceObject config = new CmsServiceObject();
        config.setBucketName(bucket);
        config.setAwsFile(file);
        config.setKey(saveKey);
        config.setCmsType(CmsType.AWS);
        String awsFileUploadedUrl = cmsService.UploadFile(config);
        // update existing location db settings
        Configuration configuration = null;
        if (getLocationsUrl != null)
        {
            configuration = getLocationsUrl;
            configuration.setValue(awsFileUploadedUrl);
        }
        else
        {
            configuration = new Configuration();
            configuration.setKey("locations_db");
            configuration.setValue(awsFileUploadedUrl);
        }
        configurationService.saveCOnfiguration(configuration);
    }

    public WSCityResult getAllCities() throws General1GroupException
    {
        WSCityResult wsCities = locationService.getCities();
        return wsCities;
    }
}
