package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_GREATER_OR_EQUAL;
import static one.group.validate.operations.Operations.IS_GREATER_THAN;
import static one.group.validate.operations.Operations.IS_LESS_OR_EQUAL;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF_WITH_SPLIT;
import static one.group.validate.operations.Operations.validate;
import one.group.core.enums.PathParamType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.StoreKey;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSBroadcastSearchQuery;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountService;
import one.group.services.SearchService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author miteshchavda
 * 
 */

public class SearchBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(SearchBaseHelper.class);

    private SearchService searchService;
    private AccountService accountService;

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public SearchService getSearchService()
    {
        return searchService;
    }

    public void setSearchService(SearchService searchService)
    {
        this.searchService = searchService;
    }

    public WSBroadcastSearchQuery getSearchOfAccount(final SocketEntity socketEntity) throws AuthenticationException, ClientProcessException, AccountNotFoundException, RequestValidationException,
            General1GroupException
    {

        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("accountId", accountId, NOT_NULL());
        WSAccount wsAccount = accountService.fetchAccountDetails(accountId);

        if (wsAccount.getStatus().equals(Status.UNREGISTERED))
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_UNREGISTERD, false, accountId);

        WSBroadcastSearchQuery searchQuery = searchService.getSearchOfAccount(accountId);
        return searchQuery;
    }

    public WSBroadcastSearchQuery createSearch(final SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException,
            General1GroupException, SyncLogProcessException
    {

        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("accountId", accountId, NOT_NULL());

        // Validate search key
        WSRequest request = socketEntity.getRequest();

        String wsSearchQueryStr = (!Utils.isNull(request.getSearch())) ? request.getSearch().toString() : null;
        validate("search", wsSearchQueryStr, NOT_NULL(), NOT_EMPTY());

        WSBroadcastSearchQueryRequest wsSearchQueryRequest = request.getSearch();
        wsSearchQueryRequest.setAccountId(accountId);

        // Compulsory fields.
        String propertyType = wsSearchQueryRequest.getPropertyType();
        String locationId = wsSearchQueryRequest.getLocationId();

        validate("property_type", propertyType, NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", PropertyType.propertyTypeNames()));
        validate("location_id", String.valueOf(locationId), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        // Optional fields. But if value is present then it must be valid.
        Integer minArea = wsSearchQueryRequest.getMinArea();
        Integer maxArea = wsSearchQueryRequest.getMaxArea();
        String minPrice = wsSearchQueryRequest.getMinPrice();
        String maxPrice = wsSearchQueryRequest.getMaxPrice();

        validate("min_area", String.valueOf(minArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        validate("max_area", String.valueOf(maxArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minArea)), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        validate("min_price", minPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        validate("max_price", maxPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minPrice)), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));

        validate("rooms", wsSearchQueryRequest.getRooms().toString(), ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",", Rooms.roomsNames()));

        String searchId = null;
        WSBroadcastSearchQuery query = searchService.saveSearch(searchId, accountId, request);
        return query;
    }

    public WSBroadcastSearchQuery updateSearch(final SocketEntity socketEntity) throws AuthenticationException, ClientProcessException, AccountNotFoundException, RequestValidationException,
            General1GroupException, SyncLogProcessException
    {
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("accountId", accountId, NOT_NULL());

        // Validate search key
        WSRequest request = socketEntity.getRequest();

        String wsSearchQueryStr = (!Utils.isNull(request.getSearch())) ? request.getSearch().toString() : null;
        validate("search", wsSearchQueryStr, NOT_NULL(), NOT_EMPTY());

        WSBroadcastSearchQueryRequest wsSearchQueryRequest = request.getSearch();
        wsSearchQueryRequest.setAccountId(accountId);

        // Compulsory fields.
        String propertyType = wsSearchQueryRequest.getPropertyType();
        String locationId = wsSearchQueryRequest.getLocationId();

        validate("property_type", propertyType, NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", PropertyType.propertyTypeNames()));
        validate("location_id", String.valueOf(locationId), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        // Optional fields. But if value is present then it must be valid.
        Integer minArea = wsSearchQueryRequest.getMinArea();
        Integer maxArea = wsSearchQueryRequest.getMaxArea();
        String minPrice = wsSearchQueryRequest.getMinPrice();
        String maxPrice = wsSearchQueryRequest.getMaxPrice();

        validate("min_area", String.valueOf(minArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        validate("max_area", String.valueOf(maxArea), ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minArea)), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        validate("min_price", minPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        validate("max_price", maxPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minPrice)), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));

        validate("rooms", wsSearchQueryRequest.getRooms().toString(), ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",", Rooms.roomsNames()));

        String searchId = socketEntity.getPathParam(PathParamType.SEARCH_IDS);
        WSBroadcastSearchQuery query = searchService.saveSearch(searchId, accountId, request);

        return query;
    }

    public WSBroadcastSearchQuery searchBroadcast(final WSBroadcastSearchQuery query)
    {
        WSBroadcastSearchQuery searchResult = new WSBroadcastSearchQuery();

        return searchResult;
    }
}
