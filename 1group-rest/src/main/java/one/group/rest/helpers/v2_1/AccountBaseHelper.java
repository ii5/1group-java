package one.group.rest.helpers.v2_1;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountsResult;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.ClientService;
import one.group.services.GroupsService;
import one.group.services.SubscriptionService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;

public class AccountBaseHelper extends one.group.rest.helpers.v2.AccountBaseHelper 
{

	@Override
	public KafkaConfiguration getKafkaConfiguration() {
		// TODO Auto-generated method stub
		return super.getKafkaConfiguration();
	}

	@Override
	public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration) {
		// TODO Auto-generated method stub
		super.setKafkaConfiguration(kafkaConfiguration);
	}

	@Override
	public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer) {
		// TODO Auto-generated method stub
		super.setKafkaProducer(kafkaProducer);
	}

	@Override
	public SimpleSyncLogProducer getKafkaProducer() {
		// TODO Auto-generated method stub
		return super.getKafkaProducer();
	}

	@Override
	public AccountRelationService getAccountRelationService() {
		// TODO Auto-generated method stub
		return super.getAccountRelationService();
	}

	@Override
	public void setAccountRelationService(AccountRelationService accountRelationService) {
		// TODO Auto-generated method stub
		super.setAccountRelationService(accountRelationService);
	}

	@Override
	public ClientService getClientService() {
		// TODO Auto-generated method stub
		return super.getClientService();
	}

	@Override
	public void setClientService(ClientService clientService) {
		// TODO Auto-generated method stub
		super.setClientService(clientService);
	}

	@Override
	public AccountService getAccountService() {
		// TODO Auto-generated method stub
		return super.getAccountService();
	}

	@Override
	public void setAccountService(AccountService accountService) {
		// TODO Auto-generated method stub
		super.setAccountService(accountService);
	}

	@Override
	public ChatMessageService getChatMessageService() {
		// TODO Auto-generated method stub
		return super.getChatMessageService();
	}

	@Override
	public void setChatMessageService(ChatMessageService chatMessageService) {
		// TODO Auto-generated method stub
		super.setChatMessageService(chatMessageService);
	}

	@Override
	public SubscriptionService getSubscriptionService() {
		// TODO Auto-generated method stub
		return super.getSubscriptionService();
	}

	@Override
	public void setSubscriptionService(SubscriptionService subscriptionService) {
		// TODO Auto-generated method stub
		super.setSubscriptionService(subscriptionService);
	}

	@Override
	public GroupsService getGroupsService() {
		// TODO Auto-generated method stub
		return super.getGroupsService();
	}

	@Override
	public void setGroupsService(GroupsService groupsService) {
		// TODO Auto-generated method stub
		super.setGroupsService(groupsService);
	}

	@Override
	public WSAccountsResult fetchAccount(SocketEntity socketEntity)
			throws Abstract1GroupException, JsonProcessingException, Exception {
		// TODO Auto-generated method stub
		return super.fetchAccount(socketEntity);
	}

	@Override
	public WSEntityCount fetchAccountsCount(SocketEntity socketEntity) throws Abstract1GroupException {
		// TODO Auto-generated method stub
		return super.fetchAccountsCount(socketEntity);
	}

	@Override
	public WSAccount editAccount(SocketEntity socketEntity)
			throws Abstract1GroupException, JsonProcessingException, Exception {
		// TODO Auto-generated method stub
		return super.editAccount(socketEntity);
	}

	@Override
	public void blockAccount(SocketEntity socketEntity) throws Abstract1GroupException, Exception {
		// TODO Auto-generated method stub
		super.blockAccount(socketEntity);
	}

	@Override
	public void unBlockAccount(SocketEntity socketEntity) throws Abstract1GroupException, Exception {
		// TODO Auto-generated method stub
		super.unBlockAccount(socketEntity);
	}

	@Override
	public void updateScore(SocketEntity socketEntity)
			throws Abstract1GroupException, JsonProcessingException, Exception {
		// TODO Auto-generated method stub
		super.updateScore(socketEntity);
	}

	@Override
	public WSAccount getAccountbyShortReference(SocketEntity socketEntity) throws RequestValidationException,
			AuthenticationException, ClientProcessException, AccountNotFoundException, General1GroupException {
		// TODO Auto-generated method stub
		return super.getAccountbyShortReference(socketEntity);
	}

	@Override
	public WSChatThread getStarredBroadcastOfAccount(SocketEntity entity) throws RequestValidationException,
			AccountNotFoundException, JsonMappingException, JsonGenerationException, IOException {
		// TODO Auto-generated method stub
		return super.getStarredBroadcastOfAccount(entity);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
	
}
