package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_INTEGER;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF_WITH_SPLIT;
import static one.group.validate.operations.Operations.validate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.AdminStatus;
import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.PathParamType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.request.bpo.WSAdminUserRequest;
import one.group.entities.api.request.bpo.WSGroupMetaDataRequest;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.bpo.WSAdminDetailsResponse;
import one.group.entities.api.response.bpo.WSAuthItemResponse;
import one.group.entities.api.response.bpo.WSAutoCompleteResponse;
import one.group.entities.api.response.bpo.WSBroadcastConsolidatedResponse;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.bpo.WSLocationMetaData;
import one.group.entities.api.response.bpo.WSMessageResponse;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserMetaData;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.BPOServices;
import one.group.services.LocationService;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class BPOBaseHelper
{
    private BPOServices bpoServices;

    private LocationService locationService;

    public LocationService getLocationService()
    {
        return locationService;
    }

    public void setLocationService(LocationService locationService)
    {
        this.locationService = locationService;
    }

    public BPOServices getBpoServices()
    {
        return bpoServices;
    }

    public void setBpoServices(BPOServices bpoServices)
    {
        this.bpoServices = bpoServices;
    }

    public List<WSGroupMetaData> fetchGroupMetaData(SocketEntity entity) throws Abstract1GroupException
    {
        String groupStatusArrayString = entity.getQueryParam(QueryParamType.GROUP_STATUS);
        List<GroupStatus> groupStatusList = new ArrayList<GroupStatus>();

        if (groupStatusArrayString != null)
        {
            validate(QueryParamType.GROUP_STATUS.toString(), groupStatusArrayString, NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", GroupStatus.stringValues()));
            for (String s : groupStatusArrayString.split(","))
            {
                groupStatusList.add(GroupStatus.parse(s.trim()));
            }

        }

        return bpoServices.fetchGroupMetaDataByStatus(groupStatusList);
    }

    public List<WSGroupMetaData> fetchGroupLabelMetaData(SocketEntity socketEntity)
    {
        // List<WSGroupMetaData> metaDataList = new
        // ArrayList<WSGroupMetaData>();
        //
        // for (int i = 0; i < 100; i++)
        // {
        // metaDataList.add(WSGroupMetaData.dummyGroupLabelMetaData());
        // }
        //
        // return metaDataList;

        List<GroupStatus> groupStatusList = new ArrayList<GroupStatus>();
        String groupStatusListString = socketEntity.getQueryParam(QueryParamType.GROUP_STATUS);
        Map<SortField, SortType> sort = socketEntity.getSort();
        Map<FilterField, Set<String>> filter = socketEntity.getFilter();

        if (groupStatusListString == null)
        {
            groupStatusList = Arrays.asList(GroupStatus.values());
        }
        else
        {
            for (String s : groupStatusListString.split(","))
            {
                groupStatusList.add(GroupStatus.parse(s));
            }
        }

        return bpoServices.fetchGroupLabelAndIdByStatus(groupStatusList, sort, filter);
    }

    public List<WSGroupMetaData> fetchGroupDetailedMetaData(SocketEntity entity) throws Abstract1GroupException
    {
        List<WSMessageResponse> messages = new ArrayList<WSMessageResponse>();
        List<String> groupIdList = null;
        List<GroupStatus> groupStatusList = new ArrayList<GroupStatus>();

        String groupIdsString = entity.getQueryParam(QueryParamType.GROUP_IDS);
        String groupStatusString = entity.getQueryParam(QueryParamType.GROUP_STATUS);
        String sortByField = entity.getQueryParam(QueryParamType.SORTY_KEY);
        String sortByType = entity.getQueryParam(QueryParamType.SORT_TYPE);
        String start = entity.getQueryParam(QueryParamType.OFFSET);
        String rows = entity.getQueryParam(QueryParamType.LIMIT);
        Map<FilterField, Set<String>> filter = entity.getFilter();
        Map<SortField, SortType> sort = entity.getSort();
        // String msgCountGreaterThan = "0";

        validate(QueryParamType.GROUP_IDS.toString(), groupIdsString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.GROUP_STATUS.toString(), groupStatusString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", GroupStatus.stringValues()));
        validate(QueryParamType.OFFSET.toString(), start, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rows, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        if (groupStatusString != null)
        {
            for (String s : groupStatusString.split(","))
            {
                GroupStatus status = GroupStatus.parse(s);
                groupStatusList.add(status);
            }
        }

        groupIdList = (groupIdsString == null) ? null : Arrays.asList(groupIdsString.split(","));

        int msgCount = 0;
        int startInt = (start == null) ? 0 : Integer.parseInt(start);
        int rowsInt = (rows == null) ? 0 : Integer.parseInt(rows);

        if (filter == null)
        {
            filter = new HashMap<FilterField, Set<String>>();
            filter.put(FilterField.EVER_SYNC, new HashSet<String>(Arrays.asList("true")));
        }
        else
        {
            if (filter.get(FilterField.EVER_SYNC) != null)
            {
                filter.put(FilterField.EVER_SYNC, new HashSet<String>(Arrays.asList("true")));
            }
        }

        return bpoServices.fetchDetailedGroupMetaData(groupStatusList, groupIdList, msgCount, startInt, rowsInt, sortByField, sortByType, sort, filter);
    }

    public List<WSGroupMetaData> fetchGroupsOfMember(SocketEntity entity) throws Abstract1GroupException
    {
        String mobileNumber = entity.getPathParam(PathParamType.PHONE_NUMBERS);

        validate(PathParamType.PHONE_NUMBERS.toString(), mobileNumber, NOT_EMPTY());

        return bpoServices.fetchGroupsOfMember(mobileNumber);
    }

    public List<WSLocationMetaData> fetchLocationMetaData(SocketEntity entity) throws Abstract1GroupException
    {
        List<String> cityIdList = null;

        Map<FilterField, Set<String>> filter = entity.getFilter();
        Map<SortField, SortType> sort = entity.getSort();
        String cityIdListString = entity.getQueryParam(QueryParamType.CITY_IDS);

        validate(QueryParamType.CITY_IDS.toString(), cityIdListString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        if (cityIdListString != null && !cityIdListString.trim().isEmpty())
        {
            cityIdList = Arrays.asList(cityIdListString.split(","));
        }

        return bpoServices.fetchLocationsMetaDataByCity(cityIdList, sort, filter);
    }

    public List<WSLocation> fetchLocationsByLocationIds(SocketEntity entity) throws Abstract1GroupException
    {
        List<String> locationIdList = null;

        String locationIdListString = entity.getQueryParam(QueryParamType.LOCATION_IDS);

        validate(QueryParamType.LOCATION_IDS.toString(), locationIdListString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        if (locationIdListString != null)
        {
            locationIdList = Arrays.asList(locationIdListString.split(","));
        }

        return bpoServices.fetchLocationsByLocationIds(locationIdList);
    }

    public Map<String, Map<String, String>> fetchLocationsByCity(SocketEntity entity) throws Abstract1GroupException
    {

        List<String> cityIdList = null;

        String cityIdListString = entity.getQueryParam(QueryParamType.CITY_IDS);

        validate(QueryParamType.CITY_IDS.toString(), cityIdListString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        if (cityIdListString != null)
        {
            cityIdList = Arrays.asList(cityIdListString.split(","));
        }

        return bpoServices.fetchLocationsByCity(cityIdList);
    }

    public List<WSMessageResponse> fetchMessages(SocketEntity entity) throws Abstract1GroupException
    {
        List<WSMessageResponse> messages = new ArrayList<WSMessageResponse>();
        List<String> groupIdList = null;
        List<GroupStatus> groupStatusList = new ArrayList<GroupStatus>();
        List<MessageStatus> messageStatusList = new ArrayList<MessageStatus>();
        List<BpoMessageStatus> bpoMessageStatusList = new ArrayList<BpoMessageStatus>();
        List<String> locationIdList = new ArrayList<String>();
        List<String> cityIdList = new ArrayList<String>();
        SortField messageSentTimeSortField = SortField.MESSAGE_SENT_TIME;
        SortType messageSentTimeSortType = SortType.ASCENDING;
        int start = 0;
        int rows = 100;

        String groupIdsString = entity.getQueryParam(QueryParamType.GROUP_IDS);
        String groupStatusString = entity.getQueryParam(QueryParamType.GROUP_STATUS);
        String messageStatusString = entity.getQueryParam(QueryParamType.MESSAGE_STATUS);
        String locationIdsString = entity.getQueryParam(QueryParamType.LOCATION_IDS);
        String cityIdsString = entity.getQueryParam(QueryParamType.CITY_IDS);
        String bpoMessageStatusString = entity.getQueryParam(QueryParamType.BPO_MESSAGE_STATUS);
        String bpoMessageStatusUpdateTimeLessThan = entity.getQueryParam(QueryParamType.BPO_MESSAGE_STATUS_UPDATE_TIME_LESS_THAN);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.LOCATION_IDS.toString(), locationIdsString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.CITY_IDS.toString(), cityIdsString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.MESSAGE_STATUS.toString(), messageStatusString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", MessageStatus.stringValues()));
        validate(QueryParamType.GROUP_IDS.toString(), groupIdsString, NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.BPO_MESSAGE_STATUS.toString(), bpoMessageStatusString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", BpoMessageStatus.stringValues()));
        validate(QueryParamType.GROUP_STATUS.toString(), groupStatusString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", GroupStatus.stringValues()));
        validate(QueryParamType.OFFSET.toString(), startString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rowsString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.BPO_MESSAGE_STATUS_UPDATE_TIME_LESS_THAN.toString(), bpoMessageStatusUpdateTimeLessThan, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        if (groupStatusString != null)
        {
            validate(QueryParamType.GROUP_STATUS.toString(), groupStatusString, NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", GroupStatus.stringValues()));
            for (String s : groupStatusString.split(","))
            {
                GroupStatus status = GroupStatus.parse(s);
                groupStatusList.add(status);
            }
        }

        if (locationIdsString != null)
        {
            locationIdList = Arrays.asList(locationIdsString.split(","));
        }

        if (cityIdsString != null)
        {
            cityIdList = Arrays.asList(cityIdsString.split(","));
        }

        if (bpoMessageStatusString != null)
        {
            for (String s : bpoMessageStatusString.split(","))
            {
                bpoMessageStatusList.add(BpoMessageStatus.parse(s.trim()));
            }
        }

        if (messageStatusString != null)
        {
            for (String s : messageStatusString.split(","))
            {
                MessageStatus status = MessageStatus.parse(s);
                messageStatusList.add(status);
            }
        }

        groupIdList = Arrays.asList(groupIdsString.split(","));

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        if (bpoMessageStatusUpdateTimeLessThan != null && !bpoMessageStatusUpdateTimeLessThan.trim().isEmpty())
        {
            messageSentTimeSortType = SortType.DESCENDING;
        }

//        if (sort.get(SortField.MESSAGE_SENT_TIME) == null)
//        {
//            sort.put(messageSentTimeSortField, messageSentTimeSortType);
//        }

        messages = bpoServices.fetchMessages(messageStatusList, groupStatusList, groupIdList, locationIdList, cityIdList, bpoMessageStatusList, bpoMessageStatusUpdateTimeLessThan, start, rows, sort,
                filter);

        return messages;
    }

    public void addUserInAdmin(SocketEntity entity)
    {

    }

    public void updatePassword(SocketEntity entity)
    {

    }

    public void deleteUserCity(SocketEntity entity)
    {

    }

    public void addUserCity(SocketEntity entity)
    {

    }

    public void updatedBroadcastStatus(SocketEntity entity)
    {

    }

    public void updateMessageDetails(SocketEntity entity)
    {

    }

    public void updateGroupDetails(SocketEntity entity)
    {

    }

    public List<WSUserCity> fetchUserCityAssignment(SocketEntity entity)
    {
        return null;
    }

    public List<WSAutoCompleteResponse> fetchAutoCompleteUserNameOrEmail()
    {
        return null;
    }

    public List<WSUserMetaData> fetchAllActiveUsersWithNoCity(SocketEntity entity)
    {
        return null;
    }

    public List<WSAdminDetailsResponse> fetchDetailedUserCityAssignment(SocketEntity entity)
    {
        return null;
    }

    public List<WSGroupMetaData> fetchGroupsByGroupIds(SocketEntity entity) throws Abstract1GroupException
    {
        List<WSGroupMetaData> groupMetaDataList = new ArrayList<WSGroupMetaData>();
        List<String> groupIdList = null;

        String groupIdsString = entity.getQueryParam(QueryParamType.GROUP_IDS);

        validate(QueryParamType.GROUP_IDS.toString(), groupIdsString, NOT_NULL(), NOT_EMPTY());

        groupIdList = Arrays.asList(groupIdsString.split(","));

        groupMetaDataList = bpoServices.fetchGroupsByGroupIds(groupIdList);

        return groupMetaDataList;
    }

    public WSCityResult getCitiesByIds(SocketEntity entity) throws Abstract1GroupException
    {
        List<String> cityIdList = new ArrayList<String>();
        List<Boolean> isPublishedList = Arrays.asList(Boolean.TRUE);
        int start = 0;
        int rows = Integer.MAX_VALUE;

        String cityIdListString = entity.getQueryParam(QueryParamType.CITY_IDS);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.GROUP_IDS.toString(), cityIdListString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        if (cityIdListString != null && !cityIdListString.trim().isEmpty())
        {
            cityIdList = Arrays.asList(cityIdListString.split(","));
        }

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        return locationService.getAllCities(cityIdList, isPublishedList, sort, filter, start, rows);
    }

    public List<WSUserMetaData> fetchUsersByRoles(SocketEntity entity) throws Abstract1GroupException
    {

        List<String> roleList = null;
        List<AdminStatus> adminStatusList = new ArrayList<AdminStatus>();
        int start = 0;
        int rows = Integer.MAX_VALUE;

        String roleListString = entity.getQueryParam(QueryParamType.ROLES);
        String adminStatusListString = entity.getQueryParam(QueryParamType.STATUS);
        String fieldName = entity.getQueryParam(QueryParamType.FIELD);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        if (adminStatusListString == null || adminStatusListString.trim().isEmpty())
        {
            adminStatusListString = AdminStatus.ACTIVE.toString();
        }

        validate(QueryParamType.ROLES.toString(), roleListString, NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.STATUS.toString(), adminStatusListString, NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.OFFSET.toString(), startString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rowsString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        roleList = Arrays.asList(roleListString.split(","));
        for (String s : adminStatusListString.split(","))
        {
            AdminStatus status = AdminStatus.parse(s);
            adminStatusList.add(status);
        }

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        return bpoServices.fetchUsersByRolesAndStatus(roleList, adminStatusList, fieldName, sort, filter, start, rows);
    }

    public List<WSUserMetaData> fetchUserBasedOnInput(SocketEntity entity) throws Abstract1GroupException
    {
        List<AdminStatus> adminStatusList = new ArrayList<AdminStatus>();
        int start = 0;
        int rows = 100;

        String username = entity.getQueryParam(QueryParamType.USERNAME);
        String adminStatusListString = entity.getQueryParam(QueryParamType.STATUS);
        String email = entity.getQueryParam(QueryParamType.EMAIL);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.USERNAME.toString(), username, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.STATUS.toString(), adminStatusListString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.EMAIL.toString(), email, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.OFFSET.toString(), startString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rowsString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        if (adminStatusListString != null && !adminStatusListString.isEmpty())
        {
            for (String s : adminStatusListString.split(","))
            {
                AdminStatus status = AdminStatus.parse(s);
                adminStatusList.add(status);
            }
        }

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        return bpoServices.fetchUserByUserNameAndStatus(username, adminStatusList, email, sort, filter, start, rows);
    }

    public List<WSBroadcastConsolidatedResponse> fetchBroadcastsWithTagsByCityAndLocation(SocketEntity entity) throws Abstract1GroupException
    {
        List<String> locationIdList = new ArrayList<String>();
        List<String> cityIdList = new ArrayList<String>();
        int start = 0;
        int rows = 100;

        String locationIdsString = entity.getQueryParam(QueryParamType.LOCATION_IDS);
        String cityIdsString = entity.getQueryParam(QueryParamType.CITY_IDS);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.LOCATION_IDS.toString(), locationIdsString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.CITY_IDS.toString(), cityIdsString, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.OFFSET.toString(), startString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rowsString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        if (locationIdsString != null)
        {
            locationIdList = Arrays.asList(locationIdsString.split(","));
        }

        if (cityIdsString != null)
        {
            cityIdList = Arrays.asList(cityIdsString.split(","));
        }

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        return bpoServices.fetchBroadcastsWithTagsByCityAndLocation(cityIdList, locationIdList, sort, filter, start, rows);
    }

    public List<WSUserCity> fetchAdminAccountsBasedOnRoleCityAndId(SocketEntity entity) throws Abstract1GroupException
    {
        int start = 0;
        int rows = 100;
        String cityId = entity.getQueryParam(QueryParamType.CITY_IDS);
        String adminId = entity.getQueryParam(QueryParamType.ADMIN_IDS);
        String priority = entity.getQueryParam(QueryParamType.PRIORITY);
        String startString = entity.getQueryParam(QueryParamType.OFFSET);
        String rowsString = entity.getQueryParam(QueryParamType.LIMIT);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.CITY_IDS.toString(), cityId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.ADMIN_IDS.toString(), adminId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.PRIORITY.toString(), priority, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(QueryParamType.OFFSET.toString(), startString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        validate(QueryParamType.LIMIT.toString(), rowsString, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_INTEGER());

        if (startString != null && !startString.trim().isEmpty())
        {
            start = Integer.parseInt(startString);
        }

        if (rowsString != null && !rowsString.trim().isEmpty())
        {
            rows = Integer.parseInt(rowsString);
        }

        return bpoServices.fetchByPriorityCityAndAdmin(priority, cityId, adminId, sort, filter, start, rows);
    }

    public List<WSGroupMetaData> fetchUnassignedGroupsBasedOnCity(SocketEntity entity) throws Abstract1GroupException
    {
        String cityId = entity.getQueryParam(QueryParamType.CITY_IDS);
        Map<SortField, SortType> sort = entity.getSort();
        Map<FilterField, Set<String>> filter = entity.getFilter();

        validate(QueryParamType.CITY_IDS.toString(), cityId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        return bpoServices.fetchUnassignedGroupsBasedOnCity(cityId, sort, filter);
    }

    public void updateAdminUser(SocketEntity entity) throws Abstract1GroupException
    {
        WSRequest request = entity.getRequest();

        WSAdminUserRequest bpoUserRequest = request.getAdminUser();

        String reqStr = (!Utils.isNull(request.getAdminUser())) ? request.getAdminUser().toString() : null;
        validate("admin_user", reqStr, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        String userId = bpoUserRequest.getId();

        validate("id", userId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        bpoServices.updateAdminUser(bpoUserRequest);
    }

    public void updateUserCity(SocketEntity entity) throws Abstract1GroupException
    {
        WSRequest request = entity.getRequest();

        List<WSAdminUserRequest> userRequestList = request.getUserCityList();

        String reqStr = (!Utils.isNull(request.getUserCityList())) ? request.getUserCityList().toString() : null;

        validate("user_city_list", reqStr, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        bpoServices.updateUserCity(userRequestList);
    }

    public WSGroupMetaData saveGroups(SocketEntity socketEntity, boolean createNew) throws Abstract1GroupException
    {
        WSRequest request = socketEntity.getRequest();

        String requestString = (request == null) ? null : request.toString();
        String idString = null;
        String releaseAllGroups = socketEntity.getQueryParam(QueryParamType.RELEASE_ALL_GROUPS);

        validate(QueryParamType.RELEASE_ALL_GROUPS.toString(), releaseAllGroups, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", "true", "false"));

        WSGroupMetaDataRequest groupRequest = request.getBpoGroupMetaData();
        if (releaseAllGroups == null || !releaseAllGroups.equals("true"))
        {
            validate("request", requestString, NOT_NULL(), NOT_EMPTY());

            String groupRequestString = (groupRequest == null) ? null : groupRequest.toString();

            validate("bpoGroup", groupRequestString, NOT_NULL(), NOT_EMPTY());

            idString = groupRequest.getId();
            validate("id", idString, NOT_NULL(), NOT_EMPTY());
        }
        Boolean realeasAllGroupsBoolean = Boolean.parseBoolean(releaseAllGroups);

        return bpoServices.saveGroup(groupRequest, createNew, realeasAllGroupsBoolean);
    }

    public boolean updateMessageStatus(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String messageStatusStr = socketEntity.getQueryParam(QueryParamType.MESSAGE_STATUS);
        MessageStatus messageStatus = messageStatusStr == null ? null : MessageStatus.parse(messageStatusStr);

        String bpoMessageStatusStr = socketEntity.getQueryParam(QueryParamType.BPO_MESSAGE_STATUS);
        BpoMessageStatus bpoMessageStatus = bpoMessageStatusStr == null ? null : BpoMessageStatus.parse(bpoMessageStatusStr);

        String updatedBy = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);

        String messageIds = socketEntity.getQueryParam(QueryParamType.MESSAGE_IDS);
        validate(QueryParamType.MESSAGE_IDS.toString(), messageIds, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate(PathParamType.ACCOUNT_IDS.toString(), updatedBy, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        List<String> messageIdList = null;

        if (messageIds != null)
        {
            messageIdList = Arrays.asList(messageIds.split(","));
        }

        return bpoServices.updateMessageStatus(messageIdList, messageStatus, bpoMessageStatus, updatedBy, updatedBy);
    }

    public WSMessageResponse fetchMessage(SocketEntity entity) throws Abstract1GroupException
    {
        WSMessageResponse messageResponse = new WSMessageResponse();

        String messageIdString = entity.getQueryParam(QueryParamType.MESSAGE_IDS);
        validate(QueryParamType.MESSAGE_IDS.toString(), messageIdString, NOT_NULL(), NOT_EMPTY());

        messageResponse = bpoServices.fetchMessageWithBroadcastDetails(messageIdString);

        return messageResponse;
    }

    public List<WSUserCity> fetchUnassignedCityAdmin(SocketEntity entity) throws Abstract1GroupException
    {
        return bpoServices.fetchUnassignedCityAdmin();
    }

    public WSMessageResponse fetchMessageCountsByGroup(SocketEntity entity) throws Abstract1GroupException
    {
        String groupId = entity.getQueryParam(QueryParamType.GROUP_IDS);
        validate(QueryParamType.GROUP_IDS.toString(), groupId, NOT_NULL(), NOT_EMPTY());
        return bpoServices.fetchMessageCountsByGroup(groupId);
    }

    public WSAuthItemResponse fetchAuthorisationForUser(SocketEntity entity) throws Abstract1GroupException
    {
        String userId = entity.getPathParam(PathParamType.ACCOUNT_IDS);

        validate(PathParamType.ACCOUNT_IDS.toString(), userId, NOT_EMPTY());

        return bpoServices.fetchAuthorisationForUser(userId);
    }

    public Map<String, Set<WSMessage>> saveMessages(SocketEntity entity) throws Abstract1GroupException
    {
    	WSRequest request = entity.getRequest();
    	String requestString = (request == null) ? null : request.toString();
    	validate("request", requestString, NOT_NULL(), NOT_EMPTY());
    	
        Map<String, Set<WSMessage>> postMessages = request.getPostMessages();

    	return bpoServices.saveMessages(postMessages);
    }

	public void updateGroupsUnreadFrom(SocketEntity socketEntity) throws Exception
	{
		bpoServices.updateGroupsUnreadFrom(socketEntity);
	}

	public void updateMessageMetaData(SocketEntity socketEntity) throws Abstract1GroupException
	{
		bpoServices.updateMessageMetaData(socketEntity);
	}

	
	public void updateGroupStatus(SocketEntity socketEntity) throws Exception
	{
		bpoServices.markEmptyGroups();
	}
	
	public void archiveMessages(SocketEntity socketEntity) throws Exception
	{
		bpoServices.archiveMessages(socketEntity);
	}
}
