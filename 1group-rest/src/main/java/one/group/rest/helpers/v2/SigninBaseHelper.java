package one.group.rest.helpers.v2;

import static one.group.core.Constant.AUTHORITIES_ROLE_USER;
import static one.group.core.Constant.MAX_OTP_LENGTH;
import static one.group.core.Constant.MIN_OTP_LENGTH;
import static one.group.core.Constant.MOBILE_NUMBER_LENGTH;
import static one.group.core.Constant.VALID_PHONE_NUMBER_REGEX;
import static one.group.validate.operations.Operations.IS_EQUAL;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.LENGTH_BETWEEN;
import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.validate;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import one.group.core.ClientVersion;
import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.ClientType;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.OAuthGrantType;
import one.group.core.enums.OAuthScope;
import one.group.core.enums.OAuthTokenType;
import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.WSContactsInfo;
import one.group.entities.api.request.v2.WSPhoto;
import one.group.entities.api.request.v2.WSPhotoReference;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.request.v2.WSRequestOtp;
import one.group.entities.api.request.v2.WSSignIn;
import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSClient;
import one.group.entities.api.response.v2.WSOtp;
import one.group.exceptions.codes.AuthorizationExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.exceptions.movein.UserNotAuthorizedException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.AuthorizationService;
import one.group.services.ClientService;
import one.group.services.GroupsService;
import one.group.services.OtpService;
import one.group.services.PhonenumberService;
import one.group.services.SMSService;
import one.group.services.SubscriptionService;
import one.group.services.helpers.EnvironmentConfiguration;
import one.group.services.helpers.PusherConnectionFactory;
import one.group.utils.Utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * @author bhumikabhatt
 * 
 */

public class SigninBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(SigninBaseHelper.class);

    private AccountService accountService;

    private ClientService clientService;

    private OtpService otpService;

    private PhonenumberService phonenumberService;

    private SubscriptionService subscriptionService;

    private PusherConnectionFactory pusherConnectionFactory;

    private AuthorizationService authorizationService;

    private SMSService smsService;

    private SMSService smsCountryService;

    private SMSService smsNetcoreService;

    private EnvironmentConfiguration environmentConfiguration;

    private AccountRelationService accountRelationService;

    private GroupsService groupsService;

    public void setPusherConnectionFactory(PusherConnectionFactory pusherConnectionFactory)
    {
        this.pusherConnectionFactory = pusherConnectionFactory;
    }

    public void setOtpService(OtpService otpService)
    {
        this.otpService = otpService;
    }

    public void setPhonenumberService(PhonenumberService phonenumberService)
    {
        this.phonenumberService = phonenumberService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public void setAuthorizationService(AuthorizationService authorizationService)
    {
        this.authorizationService = authorizationService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public void setSmsService(SMSService smsService)
    {
        this.smsService = smsService;
    }

    public void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration)
    {
        this.environmentConfiguration = environmentConfiguration;
    }

    public SMSService getSmsCountryService()
    {
        return smsCountryService;
    }

    public void setSmsCountryService(SMSService smsCountryService)
    {
        this.smsCountryService = smsCountryService;
    }

    public SMSService getSmsNetcoreService()
    {
        return smsNetcoreService;
    }

    public void setSmsNetcoreService(SMSService smsNetcoreService)
    {
        this.smsNetcoreService = smsNetcoreService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    /**
     * 
     * Method adds new mobile number and generates otp
     * 
     * @param mobileNumber
     * @return Response Json String
     */
    public WSOtp requestOtp(String json) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        WSRequest request = null;
        String mobileNumber = null;
        if (json != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = request.getRequestOtp() == null ? null : json;
        }

        validate("request_otp", json, NOT_NULL(), NOT_EMPTY());
        mobileNumber = request.getRequestOtp().getMobileNumber();
        validate("mobile_number", mobileNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(Constant.MOBILE_NUMBER_LENGTH, Constant.MOBILE_NUMBER_LENGTH), MATCHES(VALID_PHONE_NUMBER_REGEX));

        WSRequestOtp optRequest = request.getRequestOtp();
        // Validate phone number entry, create if not in db table
        try
        {
            phonenumberService.savePhoneNumber(mobileNumber, PhoneNumberType.MOBILE, PhoneNumberSource.APP);
        }
        catch (PersistenceException e)
        {
            logger.debug("", e);
            e.printStackTrace();
        }

        // If smsMode is true then save 0000 as OTP value
        // If smsMode is false then generate random otp and save it and send
        // SMS
        String smsMode = environmentConfiguration.getSmsMode();
        String randomOtp = environmentConfiguration.getTestOtp();
        if (smsMode.equals(Constant.STRING_TRUE))
        {
            randomOtp = getAndSaveRandomOtp(mobileNumber);
            // smsService.sendSingleSMS(mobileNumber, randomOtp);
            smsCountryService.sendSingleSMSPooled(mobileNumber, randomOtp);
            smsNetcoreService.sendSingleSMSPooled(mobileNumber, randomOtp);
        }
        else
        {
            randomOtp = saveTestModeOTP(mobileNumber, randomOtp);
        }

        WSOtp wsOtp = new WSOtp();

        return wsOtp;
    }

    /**
     * Gets the random otp. Generate unique random OTP. A safety net if a
     * duplicate OTP is generated.
     * 
     * @return the random otp
     */
    private String getAndSaveRandomOtp(String mobileNumber)

    {
        String randomOtp = null;
        do
        {
            randomOtp = Utils.randomString(Constant.CHARSET_NUMERIC, 4, false);

            if (otpService.checkAndSaveOtp(randomOtp, mobileNumber))
            {
                randomOtp = null;
            }

        }
        while (randomOtp == null);
        return randomOtp;
    }

    private String saveTestModeOTP(String mobileNumber, String otp)
    {
        otpService.saveOTP(otp, mobileNumber);
        return otp;
    }

    /**
     * 
     * Method creates new account, client, oauth client, attaches otp with
     * client and generates oauth access token
     * 
     * @param mobileNumber
     * @param grantType
     * @param otp
     * @param clientType
     * @param deviceId
     * @param deviceUserName
     * @param deviceModelName
     * @param devicePlatform
     * @param deviceOsVersion
     * @return Response Json String
     */
    public WSClient signin(String json, @Context UriInfo uriInfo) throws Abstract1GroupException, JsonProcessingException, Exception
    {

        String mobileNumber, grantType, otp, clientType, deviceId, deviceUserName, deviceModelName, devicePlatform, deviceOsVersion, roleType, roleAuthrities, cpsId, appVersion;
        WSRequest request = null;

        if (json != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = request.getSignIn() == null ? null : json;
        }
        validate("sign_in", json, NOT_NULL(), NOT_EMPTY());
        mobileNumber = request.getSignIn().getMobileNumber();
        grantType = request.getSignIn().getGrantType();
        clientType = request.getSignIn().getClientType();
        deviceId = request.getSignIn().getDeviceId();
        deviceUserName = request.getSignIn().getDeviceUserName();
        deviceModelName = request.getSignIn().getDeviceModelName();
        devicePlatform = request.getSignIn().getDevicePlatform();
        deviceOsVersion = request.getSignIn().getDeviceOsVersion();
        otp = request.getSignIn().getOtp();
        cpsId = request.getSignIn().getCpsId();

        validate("device_platform", devicePlatform, NOT_NULL(), NOT_EMPTY(), ONE_OF(ClientType.mobileNames()));

        ClientType clientTypeEnum = ClientType.convert(devicePlatform);
        List<String> supportedVersion = ClientVersion.getSupportedVersion(clientTypeEnum);
        String clientVersion = request.getSignIn().getClientVersion();

        WSSignIn signInRequest = request.getSignIn();

        validate("client_version", clientVersion, NOT_NULL(), NOT_EMPTY(), ONE_OF(supportedVersion.toArray(new String[supportedVersion.size()])));
        validate("mobile_number", mobileNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_LENGTH), MATCHES(Constant.VALID_PHONE_NUMBER_REGEX));
        validate("grant_type", grantType, NOT_NULL(), NOT_EMPTY(), IS_EQUAL(OAuthGrantType.CLIENT_CREDENTIALS.toString())); // ONE_OF(OAuthGrantType.names())
        validate("otp", otp, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), LENGTH_BETWEEN(MIN_OTP_LENGTH, MAX_OTP_LENGTH));
        validate("client_type", clientType, NOT_NULL(), NOT_EMPTY(), ONE_OF(ClientType.platforms()));
        // validate("cps_id", cpsId, NOT_NULL(), NOT_EMPTY());

        if (clientType.equals(ClientType.ANDROID.getPlatform()))
        {
            validate("device_id", deviceId, NOT_NULL(), NOT_EMPTY());
            validate("device_user_name", deviceUserName, NOT_NULL(), NOT_EMPTY());
            validate("device_model_name", deviceModelName, NOT_NULL(), NOT_EMPTY());
            validate("device_os_version", deviceOsVersion, NOT_NULL(), NOT_EMPTY());
        }

        WSClient wsClient = null;
        String pushChannel = null;
        String accessToken = null;
        String tokenType = null;
        String scope = null;

        clientService.checkValidOtpAndDeleteIfValid(otp, mobileNumber);

        WSAccount account = accountService.fetchAccountByMobileNumber(mobileNumber);

        try
        {
            signInRequest.setAccount(accountService.saveIfNotExist(AccountType.AGENT, Status.UNREGISTERED, signInRequest.getMobileNumber()));
        }
        catch (PersistenceException pe)
        {
            logger.debug("", pe);
        }
        ;

        if (signInRequest.getAccount() != null)
        {
            subscriptionService.subscribe(signInRequest.getAccount().getId(), signInRequest.getAccount().getId(), EntityType.ACCOUNT);
        }

        // get unique push channel
        do
        {
            pushChannel = Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHANUMERIC, Constant.PUSHER_CHANNEL_ID_LENGTH, false);
            WSClient tClient = clientService.fetchClientByPushChannel(pushChannel);
            if (tClient != null)
            {
                pushChannel = null;
            }
        }
        while (pushChannel == null);

        wsClient = clientService.saveClient(signInRequest.getAccount(), Constant.APP_NAME, clientVersion, deviceModelName, deviceId, deviceOsVersion, true, pushChannel, null, null, null,
                DeviceStatus.ACTIVE, ClientType.convert(devicePlatform).name(), mobileNumber, cpsId);

        authorizationService.createOauthClient(otp, grantType, wsClient, AUTHORITIES_ROLE_USER);

        OAuth2AccessToken tokenDetails = null;
        try
        {
            /* generate oauth access_token */
            tokenDetails = authorizationService.getTokenDetails(wsClient.getId(), otp, grantType, uriInfo);
        }
        catch (Exception e)
        {
            throw new UserNotAuthorizedException(AuthorizationExceptionCode.UNAUTHORIZED_CLIENT, true, "User Unauthorized, %s", e);
        }

        accessToken = tokenDetails.getValue();
        tokenType = tokenDetails.getTokenType();
        scope = StringUtils.join(tokenDetails.getScope(), ",");

        wsClient.setAccountId(wsClient.getAccountId());
        wsClient.setTokenType(OAuthTokenType.getType(tokenType));
        wsClient.setScope(OAuthScope.getType(scope));
        wsClient.setAccessToken(accessToken);
        wsClient.setPushKey(pusherConnectionFactory.getKey());
        wsClient.setAccountStatus(Status.UNREGISTERED);

        if (account == null)
        {
            accountService.incrementCount();
        }
        else
        {
            Status accountStatus = account.getStatus();
            wsClient.setAccountStatus(accountStatus);
            if (Status.ACTIVE.equals(accountStatus))
            {

                WSCity city = account.getCity();
                if (city != null)
                {
                    // IF account is already ACTIVE and if default groups are
                    // not
                    // exist then create it.
                    String accountId = account.getId();
                    String cityId = account.getCity().getId();
                    String fullName = account.getFullName();
                    String phoneNumber = account.getPrimaryMobile();
                    groupsService.createDefaultGroupsForAccount(accountId, cityId, fullName, phoneNumber);

                    wsClient.setCityId(city.getId());
                    wsClient.setCityName(city.getName());
                }
            }
        }
        return wsClient;
    }

    /**
     * API to register account
     * 
     * @param uriInfo
     * @param httpHeader
     * @param firstName
     * @param lastName
     * @param localities
     * @param photoStream
     * @param photoDetails
     * @param photoBodyPart
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public WSAccount register(@Context HttpHeaders httpHeaders, String json) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        long start = Utils.getSystemTime();
        logger.debug("Registration start:" + Utils.getSystemTime());
        WSRequest request = null;
        WSPhoto fullPhoto = null;
        WSPhoto thumbNail = null;

        if (json != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = request.getAccount() == null ? null : json;

        }
        validate("account", json, NOT_NULL(), NOT_EMPTY());

        String fullName = request.getAccount().getFullName();
        String companyName = request.getAccount().getCompanyName();
        String cityId = request.getAccount().getCityId();
        WSPhotoReference photo = request.getAccount().getPhoto();

        if (photo != null)
        {
            fullPhoto = photo.getFull();
            thumbNail = photo.getThumbnail();
        }

        if (Utils.isNull(json))
        {
            throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, true);
        }
        validate("full_name", fullName, NOT_NULL(), NOT_EMPTY());
        validate("city_id", cityId, NOT_NULL(), NOT_EMPTY());

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchAccountFromAuthToken(authToken);

        // call register account service
        Date registrationTime = new Date();
        wsAccount = accountService.saveOrUpdateAccount(wsAccount.getId(), null, Status.ACTIVE, null, null, null, fullName, companyName, null, cityId, fullPhoto, thumbNail, registrationTime);
        String accountId = wsAccount.getId();
        // sync contacts

        logger.info("Calling sync contacts for registered user..");
        // Set<String> deltaContacts =
        // accountRelationService.syncContactsForAccount(accountId);
        // Set<String> deltaContacts =
        // accountRelationService.syncContactsForAccountBulk(accountId);
        WSContactsInfo contactsInfo = new WSContactsInfo();
        // contactsInfo.setContactsSyncFlagFlipped(false);
        // contactsInfo.setDeltaContacts(deltaContacts);

        accountRelationService.pushToSyncLog(HintType.FETCH, EntityType.ACCOUNT_RELATION, accountId, SyncActionType.REGISTER, contactsInfo);

        // saving default groups for account
        groupsService.createDefaultGroupsForAccount(accountId, cityId, fullName, wsAccount.getPrimaryMobile());

        // accountRelationService.pushToSyncLog(HintType.FETCH,
        // EntityType.ACCOUNT, accountId, SyncActionType.REGISTER,
        // contactsInfo);
        // accountRelationService.pushToSyncLog(HintType.FETCH,
        // EntityType.ACCOUNT_RELATION, accountId, SyncActionType.REGISTER,
        // contactsInfo);

        logger.info("Registration end: " + (Utils.getSystemTime() - start));
        return wsAccount;
    }
}