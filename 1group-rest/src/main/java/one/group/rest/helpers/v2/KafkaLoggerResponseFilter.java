package one.group.rest.helpers.v2;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.response.WSResponse;
import one.group.services.ApplicationLogger;
import one.group.services.ClientService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author nyalfernandes
 * 
 */
@Provider
public class KafkaLoggerResponseFilter implements ContainerResponseFilter, ContainerRequestFilter
{
    private static final String REQUEST_INPUT = "REQ_KAFKA_INPUT";
    private static final String REQUEST_TIME = "REQ_TIME";

    private static final Logger logger = LoggerFactory.getLogger(KafkaLoggerResponseFilter.class);

    public static final String BEARER_TYPE = "Bearer";

    @Autowired
    private ClientService clientService;

    @Autowired
    private ApplicationLogger applicationLogger;

    @Override
    public void filter(ContainerRequestContext request) throws IOException
    {
        request.setProperty(REQUEST_TIME, new Date());
        if (request.getMediaType() != null && !request.getMediaType().getType().equals("multipart"))
        {
            InputStream is = null;
            String requestJson = null;
            try
            {
                is = request.getEntityStream();
                if (is != null)
                {
                    requestJson = readFromStream(is, true);
                }
            }
            catch (Exception e)
            {
                requestJson = "{}";
                logger.warn("Error reading request IS.", e);
            }
            finally
            {
                Utils.close(is);
                if (requestJson != null)
                {
                    request.setProperty(REQUEST_INPUT, requestJson);
                    request.setEntityStream(new ByteArrayInputStream(requestJson.getBytes()));
                }
            }
        }

    }

    @Override
    public void filter(ContainerRequestContext containerRequest, ContainerResponseContext containerResponse) throws IOException
    {
        try
        {
            String authToken = getAuthTokenFromHeader(containerRequest.getHeaders());
            WSAccount account = null;
            WSClient client = null;
            String url = containerRequest.getUriInfo().getRequestUri().getPath();
            String request = null;
            String response = null;
            Map<String, String> additionalData = new HashMap<String, String>();
            Date responseTime = new Date();
            Date requestTime = null;

            addElementsToMap(containerRequest.getHeaders(), additionalData, containerRequest);
            addElementsToMap(containerRequest.getUriInfo().getPathParameters(), additionalData, containerRequest);
            addElementsToMap(containerRequest.getUriInfo().getQueryParameters(), additionalData, containerRequest);

            if (authToken != null)
            {
                client = clientService.getClientByAuthToken(authToken);
                account = client.getAccount();
            }

            request = (String) containerRequest.getProperty(REQUEST_INPUT);
            requestTime = (Date) containerRequest.getProperty(REQUEST_TIME);

            if (containerResponse.hasEntity())
            {
                response = (String) containerResponse.getEntity().toString();
            }
            
            if (request != null && request.trim().isEmpty())
            {
                request = null;
            }
            
            if (response != null && response.trim().isEmpty())
            {
                response = null;
            }

            applicationLogger.log(account, client, null, null, request, requestTime, response, responseTime, url, HTTPRequestMethodType.valueOf(containerRequest.getMethod()), additionalData);
        }
        catch (Exception e)
        {
            logger.warn("Error while logging event.", e);
        }
    }

    private void addElementsToMap(MultivaluedMap<String, String> receivedData, Map<String, String> additionalData, ContainerRequestContext containerRequest)
    {
        if (receivedData != null)
        {
            for (String key : receivedData.keySet())
            {
                additionalData.put(key, containerRequest.getHeaderString(key));
            }
        }
    }

    private static String getAuthTokenFromHeader(MultivaluedMap<String, String> httpHeaders)
    {
        if (httpHeaders == null)
        {
            return null;
        }

        List<String> headers = httpHeaders.get("authorization");

        if (headers == null)
        {
            return null;
        }

        String token = null;
        for (String authHeader : headers)
        {
            if ((authHeader.toLowerCase().startsWith(BEARER_TYPE.toLowerCase())))
            {
                token = authHeader.substring(BEARER_TYPE.length()).trim();
            }
        }
        return token;
    }

    private String readFromStream(InputStream is, boolean closeStream)
    {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try
        {
            reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                builder.append(line);
            }

            return builder.toString();

        }
        catch (Exception e)
        {
            logger.warn("Exception while reading from request stream while logging.", e);
        }
        finally
        {
            if (closeStream)
            {
                Utils.close(reader);
                Utils.close(is);
            }
        }

        return null;
    }
    
    public static void main(String []args) throws Exception
    {
        System.out.println(Utils.getInstanceFromJson(null, WSResponse.class));
    }
}
