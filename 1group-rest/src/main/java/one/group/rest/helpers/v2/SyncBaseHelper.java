package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.IS_GREATER_THAN;
import static one.group.validate.operations.Operations.IS_LESS_THAN;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;

import one.group.core.ClientVersion;
import one.group.core.Constant;
import one.group.core.enums.APIVersion;
import one.group.core.enums.ClientType;
import one.group.core.enums.ClientUpdateType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.StoreKey;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.request.v2.WSSyncRequest;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.Configuration;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.SyncExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountService;
import one.group.services.ClientService;
import one.group.services.ConfigurationService;
import one.group.services.SyncDBService;
import one.group.services.helpers.EnvironmentConfiguration;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SyncBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(SyncBaseHelper.class);

    private ClientService clientService;

    private AccountService accountService;

    private SyncDBService syncDBService;

    private ConfigurationService configurationService;

    private EnvironmentConfiguration environmentConfiguration;

    public EnvironmentConfiguration getEnvironmentConfiguration()
    {
        return environmentConfiguration;
    }

    public void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration)
    {
        this.environmentConfiguration = environmentConfiguration;
    }

    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    public SyncDBService getSyncDBService()
    {
        return syncDBService;
    }

    public void setSyncDBService(SyncDBService syncDBService)
    {
        this.syncDBService = syncDBService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    /**
     * 
     * @param formData
     * @param httpHeaders
     * 
     * @param ack_sync_index
     *            Optional. If provided, informs the server that all sync
     *            updates at an index > the client's current sync_index and <=
     *            to the provided value have been completely processed by the
     *            client. Must be in the range [-1, MAX_INT]. If this value is
     *            provided and it is <= the client's current sync_index then it
     *            is ignored. If this value is higher than the index of the last
     *            sync update in the client's sync update list, it is equivilant
     *            to setting this value equal to the index of the last sync
     *            update in the client's sync update list. If not provided then
     *            up to max_results sync updates from the client's current sync
     *            index will be returned.
     * 
     * @param max_results
     *            Optional. If provided, specifies the maximum number of sync
     *            updates that might be returned. Must be in the range of [0,
     *            100]. If not provided defaults to 50.
     * 
     * @return
     */
    public WSSyncResult sync(SocketEntity socketEntity) throws Abstract1GroupException, JsonParseException, Exception
    {
//        String authToken = Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        WSClient wsClient = (WSClient) socketEntity.getFromStore(StoreKey.CURRENT_WS_CLIENT);
        String clientId = wsClient.getId();
        WSAccount wsAccount = (WSAccount) socketEntity.getFromStore(StoreKey.CURRENT_WS_ACCOUNT);

        WSRequest request = socketEntity.getRequest();
        WSSyncRequest syncRequest = request.getSync();

        validate("ack_sync_index", "" + syncRequest.getAckSyncIndex(), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_LESS_THAN(Integer.MAX_VALUE), IS_GREATER_THAN(-2));
        validate("max_results", "" + syncRequest.getMaxResults(), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_LESS_THAN(101), IS_GREATER_THAN(-1));

        int requestedCursorIndex = syncRequest.getAckSyncIndex();
        int maxResults = syncRequest.getMaxResults();

        // Marking account as online
        accountService.markOnline(wsAccount.getId());

        WSSyncResult result = new WSSyncResult();
        int actualAckedIndex = -1;
        try
        {
            actualAckedIndex = syncDBService.advanceReadCursor(wsAccount.getId(), clientId, requestedCursorIndex);
            result = syncDBService.readUpdates(wsAccount.getId(), clientId, maxResults);
        }
        catch (SyncLogProcessException e)
        {
            if (e.getCode() != null && !e.getCode().equals(SyncExceptionCode.NO_DATA_YET.getExceptionName()))
            {
                throw e;
            }
            logger.warn("No data to read for account " + wsAccount);
        }

        result.setSyncIndex(actualAckedIndex);
        Configuration config = configurationService.getValueByKey(Constant.LOCATIONS_DB_KEY);

        if (config != null)
        {
            result.setLocationsDb(config.getValue());
        }
        String softUpdateOn = environmentConfiguration.getSoftUpdateOn();
        
        ClientType requestedClientType = wsClient.getClientPlatform() == null ? null : ClientType.valueOf(wsClient.getClientPlatform());
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        if (Constant.STRING_FALSE.equals(softUpdateOn))
        {
        	
        	if (version != null)
        	{
        		result.setLatestClientVersion(version.replace("v", ""));
        		result.setCurrentClientVersion(version.replace("v", ""));
        	}
        	
        	String requestedClientVersion = wsClient.getAppVersion();
            ClientUpdateType clientUpdateType = Utils.getClientUpdateType(requestedClientType, requestedClientVersion);
            if (clientUpdateType != null)
            {
            	result.setClientUpdateType(clientUpdateType);
            }
        }
        else
        {
        
	        // Temporary disabled code for 16 Dec, 2015 release
	         result.setCurrentClientVersion(ClientVersion.getCurrentVersion(requestedClientType));
	         
	         result.setLatestClientVersion(ClientVersion.getCurrentVersion(requestedClientType));
	//        result.setCurrentClientVersion(null);
	
	         String requestedClientVersion = wsClient.getAppVersion();
	         ClientUpdateType clientUpdateType = Utils.getClientUpdateType(requestedClientType, requestedClientVersion);
	         if (clientUpdateType != null)
	         {
	        	 result.setClientUpdateType(clientUpdateType);
	         }
        }

        return result;
    }

    public WSSyncResult getDummySync(SocketEntity socketEntity) throws RequestValidationException
    {
        WSRequest request = socketEntity.getRequest();
        WSSyncRequest syncRequest = request.getSync();

        validate("ack_sync_index", "" + syncRequest.getAckSyncIndex(), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_LESS_THAN(Integer.MAX_VALUE), IS_GREATER_THAN(-2));
        validate("max_results", "" + syncRequest.getMaxResults(), NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_LESS_THAN(101), IS_GREATER_THAN(-1));

        WSSyncResult result = new WSSyncResult();
        result.setSyncIndex(Long.valueOf("5"));
        result.setCurrentApiVersion(APIVersion.VERSION2);

        WSSyncUpdate syncUpdate = new WSSyncUpdate();

        result.addUpdate(syncUpdate);

        return result;
    }
}
