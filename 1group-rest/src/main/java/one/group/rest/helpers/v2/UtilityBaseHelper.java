package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_INTEGER;
import static one.group.validate.operations.Operations.LENGTH;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.validate;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import one.group.core.ClientVersion;
import one.group.core.enums.ApplicationActionType;
import one.group.core.enums.ClientType;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.MediaExtensionType;
import one.group.core.enums.MonitorApplicationType;
import one.group.core.enums.StoreKey;
import one.group.core.enums.status.NotificationStatus;
import one.group.core.enums.status.Status;
import one.group.dao.GroupsDAO;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.request.v2.WSClientAnalytics;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSAccountStatusResult;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSMonitor;
import one.group.entities.api.response.WSPhoto;
import one.group.entities.api.response.WSPhotoReference;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSApplicationMetaData;
import one.group.entities.socket.Groups;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GroupExceptionCode;
import one.group.exceptions.codes.RequestValidationExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.monitor.services.MonitorApplicationService;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ClientAnalyticsService;
import one.group.services.ClientService;
import one.group.services.ConfigurationService;
import one.group.services.NotificationService;
import one.group.services.PropertyListingService;
import one.group.services.SyncGroupService;
import one.group.services.UtilityService;
import one.group.services.helpers.CmsConnectionFactory;
import one.group.utils.ManagePhotos;
import one.group.utils.Utils;

import org.apache.commons.codec.binary.Base64;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * Utility services.
 * 
 * @author nyalfernandes
 * 
 */
public class UtilityBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(UtilityBaseHelper.class);

    private AccountService accountService;

    private PropertyListingService propertyListingService;

    private ClientAnalyticsService clientAnalyticsService;

    private ClientService clientService;

    private ConfigurationService configurationService;

    private String cdnImageURL;

    private String cdnImageFolder;

    private UtilityService utilityService;

    private String awsImageFolder;

    private String awsThumbnailFolder;

    private SyncGroupService syncGroupService;

    private CmsConnectionFactory connectionFactory;

    private MonitorApplicationService monitorApplicationServerService;

    private BroadcastService broadcastService;

    private NotificationService notificationService;

    private GroupsDAO groupsDAO;

    public GroupsDAO getGroupsDAO()
    {
        return groupsDAO;
    }

    public void setGroupsDAO(GroupsDAO groupsDAO)
    {
        this.groupsDAO = groupsDAO;
    }

    public MonitorApplicationService getMonitorApplicationServerService()
    {
        return monitorApplicationServerService;
    }

    public void setMonitorApplicationServerService(MonitorApplicationService monitorApplicationServerService)
    {
        this.monitorApplicationServerService = monitorApplicationServerService;
    }

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public SyncGroupService getSyncGroupService()
    {
        return syncGroupService;
    }

    public void setSyncGroupService(SyncGroupService syncGroupService)
    {
        this.syncGroupService = syncGroupService;
    }

    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    public String getAwsImageFolder()
    {
        return awsImageFolder;
    }

    public void setAwsImageFolder(String awsImageFolder)
    {
        this.awsImageFolder = awsImageFolder;
    }

    public String getAwsThumbnailFolder()
    {
        return awsThumbnailFolder;
    }

    public void setAwsThumbnailFolder(String awsThumbnailFolder)
    {
        this.awsThumbnailFolder = awsThumbnailFolder;
    }

    public UtilityService getUtilityService()
    {
        return utilityService;
    }

    public void setUtilityService(UtilityService utilityService)
    {
        this.utilityService = utilityService;
    }

    public String getCdnImageURL()
    {
        return cdnImageURL;
    }

    public void setCdnImageURL(String cdnImageURL)
    {
        this.cdnImageURL = cdnImageURL;
    }

    public String getCdnImageFolder()
    {
        return cdnImageFolder;
    }

    public void setCdnImageFolder(String cdnImageFolder)
    {
        this.cdnImageFolder = cdnImageFolder;
    }

    public void setAccountService(final AccountService accountService)
    {
        this.accountService = accountService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public ClientAnalyticsService getClientAnalyticsService()
    {
        return clientAnalyticsService;
    }

    public void setClientAnalyticsService(ClientAnalyticsService clientAnalyticsService)
    {
        this.clientAnalyticsService = clientAnalyticsService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public CmsConnectionFactory getConnectionFactory()
    {
        return connectionFactory;
    }

    public void setConnectionFactory(CmsConnectionFactory connectionFactory)
    {
        this.connectionFactory = connectionFactory;
    }

    public BroadcastService getBroadcastService()
    {
        return broadcastService;
    }

    public void setBroadcastService(BroadcastService broadcastService)
    {
        this.broadcastService = broadcastService;
    }

    public NotificationService getNotificationService()
    {
        return notificationService;
    }

    public void setNotificationService(NotificationService notificationService)
    {
        this.notificationService = notificationService;
    }

    public WSAccountStatusResult fetchAccountStatus(String jsonData, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String token = Utils.getAuthTokenFromHeader(httpHeaders);

        WSRequest request = null;

        if (jsonData != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(jsonData, WSRequest.class);
            jsonData = request.getFetch() == null ? null : jsonData;
        }

        validate("fetch", jsonData, NOT_NULL(), NOT_EMPTY());
        validate("account_ids", request.getFetch().toString(), NOT_NULL(), NOT_EMPTY());

        WSAccountStatusResult accountStatus = accountService.fetchAccountStatus(token, request.getFetch().getAccountIds());
        return accountStatus;
    }

    public WSPhotoReference uploadProfilePic(FormDataMultiPart formData, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, Exception
    {

        WSPhotoReference wsPhotoReference = null;
        String imageType = "";

        if (Utils.isNull(formData))
        {
            throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, true);
        }

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchAccountFromAuthToken(authToken);

        // if (wsAccount.getStatus().equals(Status.UNREGISTERED))
        // {
        // throw new
        // RequestValidationException(AccountExceptionCode.ACCOUNT_UNREGISTERD,
        // true, wsAccount.getId());
        // }
        InputStream photoStream = null;
        FormDataContentDisposition photoDetails = null;
        FormDataBodyPart photoBodyPart = null;
        WSPhoto wsPhotoFull = null;
        WSPhoto wsPhotoThumbnail = null;
        photoBodyPart = formData.getField("photo");
        if (photoBodyPart != null)
        {
            try
            {
                photoStream = photoBodyPart.getEntityAs(InputStream.class);
                photoDetails = photoBodyPart.getFormDataContentDisposition();
                String photoName = null;
                try
                {
                    photoName = ManagePhotos.getPhotoName(photoBodyPart);
                }
                catch (IllegalArgumentException ipe)
                {
                    throw new RequestValidationException(RequestValidationExceptionCode.INVALID_FORMAT, true);
                }

                String relativePathFull = cdnImageFolder + File.separator + connectionFactory.getImageFolder() + File.separator;
                String absolutePathFull = cdnImageURL + File.separator + connectionFactory.getImageFolder() + File.separator;
                // save File Locally
                Map<String, Object> photoFullMap = null;
                try
                {
                    photoFullMap = ManagePhotos.saveFullPhoto(photoName, photoStream, photoDetails, relativePathFull, absolutePathFull);
                }
                catch (IllegalStateException ise)
                {
                    throw new RequestValidationException(RequestValidationExceptionCode.SIZE_EXCEEDED, true);
                }

                File fullPhotoFile = new File(photoFullMap.get("path").toString());
                if (fullPhotoFile.exists())
                {
                    // Upload Image to AWS
                    imageType = "fullPhoto";
                    String photoFullLocation = utilityService.uploadFile(fullPhotoFile, photoName, imageType);
                    photoFullMap.put("location", photoFullLocation);
                }

                wsPhotoFull = new WSPhoto(Integer.parseInt(photoFullMap.get("height").toString()), Integer.parseInt(photoFullMap.get("width").toString()), photoFullMap.get("location").toString(),
                        Long.valueOf(photoFullMap.get("size").toString()));

                String relativePathThumbnail = relativePathFull + "thumbnail" + File.separator;
                String absolutePathThumbnail = absolutePathFull + "thumbnail" + File.separator;
                // Upload Thumbnail to local
                Map<String, Object> photoThumbnailMap = null;
                try
                {
                    photoThumbnailMap = ManagePhotos.saveThumbnailPhoto(photoName, photoFullMap.get("path").toString(), photoBodyPart, relativePathThumbnail, absolutePathThumbnail);
                }
                catch (IllegalArgumentException iae)
                {
                    throw new RequestValidationException(RequestValidationExceptionCode.INVALID_FORMAT, true);
                }

                File thumbnailFile = new File(photoThumbnailMap.get("path").toString());
                if (thumbnailFile.exists())
                {
                    // Upload Image to AWS
                    imageType = "thumbnail";
                    String ThumbnaiLocation = utilityService.uploadFile(thumbnailFile, photoName, imageType);
                    photoThumbnailMap.put("location", ThumbnaiLocation);
                    if (!Utils.isNullOrEmpty(ThumbnaiLocation))
                    {
                        // delete files Store On Local
                        fullPhotoFile.delete();
                        thumbnailFile.delete();
                    }
                }

                wsPhotoThumbnail = new WSPhoto(Integer.parseInt(photoThumbnailMap.get("height").toString()), Integer.parseInt(photoThumbnailMap.get("width").toString()), photoThumbnailMap.get(
                        "location").toString(), Long.valueOf(photoThumbnailMap.get("size").toString()));

                wsPhotoReference = new WSPhotoReference(wsPhotoFull, wsPhotoThumbnail);

            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, e.getMessage(), e);
            }
        }
        else
        {
            // throw new
            // General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
            throw new RequestValidationException(RequestValidationExceptionCode.IS_NULL, true);
        }
        return wsPhotoReference;
    }

    public Map<String, Object> removeMedia(String jsonData, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String entityType = null, entityId = null;
        WSRequest request = null;
        List<String> images = null;
        List<String> imageList = new ArrayList<String>();

        if (jsonData != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(jsonData, WSRequest.class);
            jsonData = request.getRemoveMedia() == null ? null : jsonData;
        }

        entityId = request.getRemoveMedia().getEntityId();
        entityType = request.getRemoveMedia().getEntityType();
        images = request.getRemoveMedia().getImages();
        validate("entity_id", entityId, NOT_NULL(), NOT_EMPTY());
        validate("entity_type", entityType, NOT_NULL(), NOT_EMPTY());

        if (images != null)
        {
            for (int i = 0; i < images.size(); i++)
            {
                imageList.add(images.get(i));
            }
        }
        if (imageList.size() <= 0)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true);
        }
        accountService.removeProfilePic(entityId, entityType, imageList, cdnImageFolder, cdnImageURL);

        HashMap<String, Object> finalMap = new HashMap<String, Object>();

        return finalMap;
    }

    public void traceIt(String jsonInput, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        String accountId = accountService.fetchAccountIdFromAuthToken(authToken);

        WSRequest request = null;
        String clientAnalytics = null;
        if (jsonInput != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(jsonInput, WSRequest.class);
            clientAnalytics = (request.getClientAnalytics() != null) ? request.getClientAnalytics().toString() : null;
        }

        validate("client_analytics", clientAnalytics, NOT_NULL(), NOT_EMPTY());

        List<WSClientAnalytics> wsClientAnalyticsList = request.getClientAnalytics();

        if (wsClientAnalyticsList != null && !wsClientAnalyticsList.isEmpty())
        {
            for (WSClientAnalytics wsClientAnalytics : wsClientAnalyticsList)
            {
                validate("context", wsClientAnalytics.getContext(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(255));
                validate("entity_id", wsClientAnalytics.getEntityId(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(255));
                validate("entity_type", wsClientAnalytics.getEntityType(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(255));
                validate("client_event_time", wsClientAnalytics.getClientEventTime(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(255));
                validate("action", wsClientAnalytics.getAction(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(255));
            }

            String clientId = clientService.getClientIdByAuthToken(authToken);
            clientAnalyticsService.saveTraces(wsClientAnalyticsList, accountId, clientId);
        }
    }

    public String getPropertiesCount() throws Abstract1GroupException
    {

        WSEntityCount wsEntityCount = propertyListingService.getAllpropertyListingCount();

        return (Utils.isNullOrEmpty(wsEntityCount.getCount())) ? "0" : wsEntityCount.getCount();

    }

    public String getAccountsCount()
    {
        return accountService.fetchCount() + "";
    }

    public String getInviteCode(@Context HttpHeaders httpHeaders) throws AuthenticationException, ClientProcessException, AccountNotFoundException, NoSuchAlgorithmException
    {
        String inviteCode = "";
        String token = Utils.getAuthTokenFromHeader(httpHeaders);
        inviteCode = syncGroupService.getInviteCode(token);
        return inviteCode;
    }

    public boolean isFullRelease()
    {
        boolean isFullRelease = false;
        isFullRelease = syncGroupService.isFullRelease();
        return isFullRelease;
    }

    public void updateCount(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws RequestValidationException
    {
        MultivaluedMap<String, String> pathParam = uriInfo.getPathParameters();
        String count = pathParam.getFirst("total_group_count").toString();
        // action should be either true or false
        validate("count", count, NOT_NULL(), NOT_EMPTY(), IS_INTEGER());
        syncGroupService.saveCount(count);
    }

    public void configuration(String jsonInput, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, JsonProcessingException, Exception
    {

    }

    /**
     * Function to check if any systems are running
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws IOException
     * @throws RequestValidationException
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     * 
     * @return {@link WSMonitor}
     */
    public WSMonitor monitor(UriInfo uriInfo, HttpHeaders httpHeaders) throws IOException, RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);

        if (!adminWsAccount.getStatus().equals(Status.SEEDED))
        {
            throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
        }

        List<ApplicationActionType> actions = new ArrayList<ApplicationActionType>();
        boolean isCacheRunning = monitorApplicationServerService.monitorApplicationType(MonitorApplicationType.CACHE, actions);
        boolean isDataBaseRunning = monitorApplicationServerService.monitorApplicationType(MonitorApplicationType.DATABASE, actions);
        boolean isZoopkeeperRunning = monitorApplicationServerService.monitorApplicationType(MonitorApplicationType.ZOOKEEPER, actions);
        boolean isKafkaRunning = monitorApplicationServerService.monitorApplicationType(MonitorApplicationType.KAKFA, actions);

        WSMonitor wsMonitor = new WSMonitor();
        wsMonitor.setCache(isCacheRunning);
        wsMonitor.setDatabase(isDataBaseRunning);
        wsMonitor.setKafka(isKafkaRunning);
        wsMonitor.setZookeeper(isZoopkeeperRunning);
        wsMonitor.setOverallStatus(true);

        if (!isCacheRunning || !isDataBaseRunning || !isKafkaRunning || !isZoopkeeperRunning)
        {
            wsMonitor.setOverallStatus(false);
        }

        return wsMonitor;
    }

    public one.group.entities.api.response.v2.WSClientData updateClientVersion(String jsonInput, @Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws RequestValidationException,
            AuthenticationException, ClientProcessException, AccountNotFoundException, JsonMappingException, JsonGenerationException, IOException
    {

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);

        WSRequest request = null;
        String clientData = null;
        if (jsonInput != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(jsonInput, WSRequest.class);
            clientData = (request.getClientData() != null) ? request.getClientData().toString() : null;
        }

        validate("client_data", clientData, NOT_NULL(), NOT_EMPTY());

        WSClient wsClient = clientService.getClientByAuthToken(authToken);

        String clientType = wsClient.getClientPlatform();
        ClientType clientTypeEnum = ClientType.convert(clientType);

        one.group.entities.api.request.v2.WSClientData wsClientData = request.getClientData();
        String clientVersion = wsClientData.getVersion();

        List<String> supportedVersion = ClientVersion.getSupportedVersion(clientTypeEnum);
        validate("version", clientVersion, NOT_NULL(), NOT_EMPTY(), ONE_OF(supportedVersion.toArray(new String[supportedVersion.size()])));

        one.group.entities.api.response.v2.WSClientData wsClientDataResponse = clientService.updateClientVersion(authToken, wsClientData);

        return wsClientDataResponse;
    }

    public WSApplicationMetaData fetchApplicationMetadata(final SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId =
        // accountService.fetchAccountIdFromAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        if (accountId == null)
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, accountId);

        WSAccount wsAccount = accountService.fetchAccountDetails(accountId);
        if (wsAccount.getStatus().equals(Status.UNREGISTERED))
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_UNREGISTERD, false, accountId);

        WSApplicationMetaData applicationMetadata = new WSApplicationMetaData();
        applicationMetadata.setTotalAccountCount((int) accountService.fetchCount());
        applicationMetadata.setTotalBroadcastCount((int) broadcastService.fetchCount());
        applicationMetadata.setUnreadNotificationCount((int) notificationService.fetchCountByStatus(accountId, NotificationStatus.UNREAD));
        applicationMetadata.setTotalGroupCount(groupsDAO.fetchTotalGroupCount());
        applicationMetadata.setReferralTemplates(notificationService.fetchAllNotificationTemplates());
        return applicationMetadata;
    }

    public WSGroupMetaData UploadBase64Image(SocketEntity socketEntity) throws Exception
    {

        String imageType = "";
        WSRequest request = socketEntity.getRequest();

        if (request == null)
            throw new RequestValidationException(RequestValidationExceptionCode.IS_NULL, false);

        String jsonStr = (!Utils.isNull(request.getBpoGroupMetaData())) ? request.getBpoGroupMetaData().toString() : null;
        validate("jsonStr", jsonStr, NOT_NULL());

        String groupId = request.getBpoGroupMetaData().getId();
        String imageBase64 = request.getBpoGroupMetaData().getImageUrl();

        String relativePathFull = cdnImageFolder + File.separator + connectionFactory.getImageFolder() + File.separator;
        String absolutePathFull = cdnImageURL + File.separator + connectionFactory.getImageFolder() + File.separator;

        String fileName = Utils.getSystemTime() + "." + MediaExtensionType.jpg.toString();

        String imageFormats[] = { "data:image/jpeg;base64,", "data:image/png;base64,", "data:image/jpg;base64," };

        WSGroupMetaData groupMetaData = null;
        if (!Utils.isNullOrEmpty(imageBase64))
        {
            String newString[] = null;

            for (String imageFormat : imageFormats)
            {
                if (imageBase64.indexOf(imageFormat) > -1)
                {
                    newString = imageBase64.split(imageFormat);
                    break;
                }
            }
            if (newString == null)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Invalid image formate passed !!");
            }

            List<String> groupIdList = new ArrayList<String>();
            groupIdList.add(groupId);
            List<Groups> group = groupsDAO.fetchGroupsByGroupIds(groupIdList);
            if (group.isEmpty())
            {
                throw new General1GroupException(GroupExceptionCode.GROUP_NOT_FOUND, true, groupId);
            }
            else
            {
                for (Groups groups : group)
                {
                    byte[] decodedBytes = Base64.decodeBase64(newString[1]);
                    InputStream inputStream = new ByteArrayInputStream(decodedBytes);

                    // save File Locally
                    Map<String, Object> photoFullMap = null;
                    try
                    {
                        photoFullMap = ManagePhotos.saveFullPhoto(fileName, inputStream, null, relativePathFull, absolutePathFull);
                    }
                    catch (IllegalStateException ise)
                    {
                        throw new RequestValidationException(RequestValidationExceptionCode.SIZE_EXCEEDED, true);
                    }

                    File fullPhotoFile = new File(photoFullMap.get("path").toString());
                    if (fullPhotoFile.exists())
                    {
                        // Upload Image to AWS
                        imageType = "fullPhoto";
                        String photoFullLocation = utilityService.uploadFile(fullPhotoFile, fileName, imageType);
                        photoFullMap.put("location", photoFullLocation);

                        if (!Utils.isNullOrEmpty(photoFullLocation))
                        {
                            // delete files Store On Local
                            fullPhotoFile.delete();
                            groups.setMediaId(photoFullLocation);
                            groupsDAO.saveGroup(groups);
                        }
                    }
                    groupMetaData = new WSGroupMetaData(groups);
                }
            }
        }
        return groupMetaData;
    }

}
