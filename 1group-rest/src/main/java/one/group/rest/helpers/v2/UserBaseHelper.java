package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.validate;

import java.util.List;

import one.group.core.enums.AccountType;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.request.v2.WSUserRequest;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserResponse;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.services.UserService;

public class UserBaseHelper
{

    private UserService userService;

    public UserService getUserService()
    {
        return userService;
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    public WSUserResponse saveUser(SocketEntity entity, SyncStatus defaultSyncStatus, UserSource defaultUserSource, boolean isNew) throws Abstract1GroupException
    {
        WSRequest request = entity.getRequest();
        
        String requestString = null;
        
        if (request != null && request.getUser() != null)
        {
            requestString = request.getUser().toString();
        }
        
        validate("user", requestString, NOT_EMPTY());
        
        WSUserRequest userRequest = request.getUser();
        
        validate("user.mobile_number", userRequest.getMobileNumber(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY());
        validate("user.group_selection_status", userRequest.getGroupSelectionStatus(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(GroupSelectionStatus.stringNames()));
        validate("user.source", userRequest.getSource(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(UserSource.stringNames()));
        validate("user.sync_status", userRequest.getSyncStatus(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(SyncStatus.stringNames()));
        
        if (!isNew)
        {
            validate("user.id", userRequest.getId(), NOT_EMPTY());
        }
        
        SyncStatus syncStatus = (userRequest.getSyncStatus() == null) ? defaultSyncStatus : SyncStatus.parse(userRequest.getSyncStatus());
        UserSource userSource = (userRequest.getSource() == null) ? defaultUserSource : UserSource.parse(userRequest.getSource());
        GroupSelectionStatus groupSelectionStatus = (userRequest.getGroupSelectionStatus() == null) ? GroupSelectionStatus.NOT_SELECTED : GroupSelectionStatus.parse(userRequest.getGroupSelectionStatus());
        
        
        return new WSUserResponse(userService.saveUser(userRequest.getId(), userRequest.getFullName(), userRequest.getCreatedBy(), userRequest.getShortReference(), userRequest.getEmail(), userRequest.getCityId(), AccountType.LEAD, userSource, groupSelectionStatus, syncStatus, userRequest.getMobileNumber(), userRequest.getLocalStorage(), false)); 
    }

    public List<WSUserCity> fetchUsers(SocketEntity entity)
    {
        return null;
    }
}
