package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_GREATER_THAN;
import static one.group.validate.operations.Operations.IS_LESS_THAN;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.MAX_TOKEN_COUNT;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.StoreKey;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.core.enums.TopicType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSPhoto;
import one.group.entities.api.request.v2.WSPhotoReference;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSEntityReference;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountRelations;
import one.group.entities.api.response.v2.WSAccountsResult;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.socket.SocketEntity;
import one.group.entities.sync.SyncEntryKey;
import one.group.entities.sync.SyncLogEntry;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.BadRequestException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.ClientService;
import one.group.services.GroupsService;
import one.group.services.SubscriptionService;
import one.group.sync.KafkaConfiguration;
import one.group.sync.producer.SimpleSyncLogProducer;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * 
 * @author sanilshet
 * 
 */
public class AccountBaseHelper
{

    private static final Logger logger = LoggerFactory.getLogger(AccountBaseHelper.class);

    private AccountService accountService;

    private ClientService clientService;

    private AccountRelationService accountRelationService;

    private ChatMessageService chatMessageService;

    private SubscriptionService subscriptionService;

    private SimpleSyncLogProducer kafkaProducer;

    private KafkaConfiguration kafkaConfiguration;

    private GroupsService groupsService;

    public KafkaConfiguration getKafkaConfiguration()
    {
        return kafkaConfiguration;
    }

    public void setKafkaConfiguration(KafkaConfiguration kafkaConfiguration)
    {
        this.kafkaConfiguration = kafkaConfiguration;
    }

    public void setKafkaProducer(SimpleSyncLogProducer kafkaProducer)
    {
        this.kafkaProducer = kafkaProducer;
    }

    public SimpleSyncLogProducer getKafkaProducer()
    {
        return kafkaProducer;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(final AccountService accountService)
    {
        this.accountService = accountService;
    }

    public ChatMessageService getChatMessageService()
    {
        return chatMessageService;
    }

    public void setChatMessageService(ChatMessageService chatMessageService)
    {
        this.chatMessageService = chatMessageService;
    }

    public SubscriptionService getSubscriptionService()
    {
        return subscriptionService;
    }

    public void setSubscriptionService(SubscriptionService subscriptionService)
    {
        this.subscriptionService = subscriptionService;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    public WSAccountsResult fetchAccount(final SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String accountIds = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        validate("account_ids", accountIds, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));

        String subscribe = socketEntity.getQueryParam(QueryParamType.SUBSCRIBE);
        validate("subscribe", subscribe, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(Constant.MIN_SUBSCRIBE_TIME_IN_MINUTES),
                IS_LESS_THAN(Constant.MAX_SUBSCRIBE_TIME_IN_MINUTES));

        // If requestingAccount does not exist, the
        // fetchAccountFromAuthToken throws an error.
        // If requestedAccount does not exist, data in the response is set
        // to null.
        WSAccount requestedWSAccount = null;
        String token = Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        WSAccount requestingWSAccount = accountService.fetchAccountFromAuthToken(token);

        String[] accountIdArray = accountIds.split(",");
        Set<String> accountIdSet = new HashSet<String>(Arrays.asList(accountIdArray));

        WSAccountsResult result = new WSAccountsResult();

        for (String accountId : accountIdSet)
        {
            requestedWSAccount = accountService.fetchAccountDetails(accountId);

            // 1. if requesting account is null, the system throws an
            // exception
            // 2. if requested account is null, the system returns no data
            //
            // 3. if requested account is the same as the requesting
            // account,
            // then
            // a. send localities and status data
            // 4. if requested account is not the same as the requesting
            // account,
            // then
            // a. if the requested account is not active
            // then
            // system throws an account not found exception.
            // b. if requested account is active
            // then do not send status, localities; send score, is_blocked,
            // is_contact.
            //
            // generate response.

            if (requestedWSAccount != null)
            {
                // v2-469 allow chat to seeded accounts
                if ((requestedWSAccount.getStatus().equals(Status.ACTIVE)) || (requestedWSAccount.getStatus().equals(Status.SEEDED)))
                {
                    requestedWSAccount.setCanChat(true);
                }
                else
                {
                    requestedWSAccount.setCanChat(false);
                }

                if (requestingWSAccount.equals(requestedWSAccount))
                {
                    requestingWSAccount.setPrimaryMobile(null);
                }
                else if (!requestingWSAccount.getStatus().equals(Status.ACTIVE))
                {
                    throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_UNREGISTERD, false, requestingWSAccount.getId());
                }
                else if (requestingWSAccount.getStatus().equals(Status.ACTIVE))
                {
                    WSAccountRelations accountRelation = accountRelationService.fetchAccountRelation(requestingWSAccount.getId(), requestedWSAccount.getId());

                    requestedWSAccount.setStatus(null);
                    // requestedWSAccount.setLocalities(null);
                    // requestedWSAccount.setType(null);
                    if (accountRelation != null)
                    {
                        requestedWSAccount.setScore(accountRelation.getScore());
                        requestedWSAccount.setIsBlocked(accountRelation.getIsBlocked());
                        requestedWSAccount.setIsContact(accountRelation.getIsContact());
                    }
                    else
                    {
                        // Subscribe to an account which doesn't have
                        // account_relation with requesting account
                        if (subscribe != null)
                        {
                            subscriptionService.subscribe(requestingWSAccount.getId(), accountId, EntityType.ACCOUNT, Integer.parseInt(subscribe) * Constant.SUSBCRIBTION_CONVERSION_IN_SECONDS);
                        }
                    }
                }

                result.addAccountResult(requestedWSAccount);
            }
        }

        return result;
    }

    public WSEntityCount fetchAccountsCount(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        WSEntityCount wsEntityCount = new WSEntityCount();
        // String token = Utils.getAuthTokenFromHeader(httpHeaders);
        // accountService.fetchAccountFromAuthToken(token);
        wsEntityCount.setNamespace(EntityType.ACCOUNT);
        wsEntityCount.setCount(String.valueOf(accountService.fetchCount()));
        return wsEntityCount;
    }

    /**
     * Account edit his account
     * 
     * @param socketEntity
     * @return
     * @throws Abstract1GroupException
     * @throws JsonProcessingException
     * @throws Exception
     */
    public WSAccount editAccount(final SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        String ownerAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY());

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // WSAccount wsAccount =
        // accountService.fetchActiveAccountFromAuthToken(authToken);

        // Not allowed to edit other accounts details
        if (!ownerAccountId.equals(accountId))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }

        WSRequest request = socketEntity.getRequest();
        String accountStr = request.getAccount() == null ? null : request.getAccount().toString();
        validate("account", accountStr, NOT_NULL(), NOT_EMPTY());

        String fullName = request.getAccount().getFullName();
        validate("full_name", fullName, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY());

        String companyName = request.getAccount().getCompanyName();
        String cityId = request.getAccount().getCityId();
        validate("city_id", cityId, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY());

        WSPhotoReference photo = request.getAccount().getPhoto();

        WSPhoto full = null;
        WSPhoto thumbnail = null;
        if (photo != null)
        {
            full = photo.getFull();
            thumbnail = photo.getThumbnail();
        }
        // setting unwanted parameters to null, as we don't required for edit
        // account request
        AccountType accountType = null;
        Status status = null;
        String primaryMobileNumber = null;
        String address = null;
        String email = null;
        String establishmentName = null;

        Date registrationTime = null;
        WSAccount wsAccount = accountService.saveOrUpdateAccount(accountId, accountType, status, primaryMobileNumber, address, email, fullName, companyName, establishmentName, cityId, full,
                thumbnail, registrationTime);

        // save to sync log(kafka)
        WSEntityReference entityReference = new WSEntityReference(HintType.FETCH, EntityType.ACCOUNT, accountId);
        SyncLogEntry syncLogEntry = new SyncLogEntry();
        syncLogEntry.setType(SyncDataType.INVALIDATION);
        syncLogEntry.setAssociatedEntityType(EntityType.ACCOUNT);
        syncLogEntry.setAssociatedEntityId(accountId);
        syncLogEntry.setAction(SyncActionType.EDIT);
        syncLogEntry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, Utils.getJsonString(entityReference));

        kafkaProducer.write(syncLogEntry, kafkaConfiguration.getTopic(TopicType.APP).getName());
        return wsAccount;
    }

    /**
     * Account block another account. Account can not block his own account
     * 
     * @param socketEntity
     * @throws Abstract1GroupException
     * @throws Exception
     */
    public void blockAccount(final SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {
        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        String ownerAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        // String token =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));

        // WSAccount requestingWSAccount =
        // accountService.fetchActiveAccountFromAuthToken(token);
        WSAccount requestedWSAccount = accountService.fetchAccountDetails(accountId);

        if (requestedWSAccount == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, accountId);
        }
        else if (ownerAccountId.equals(requestedWSAccount.getId()))
        {
            // Avoid Self Referencing Account
            throw new BadRequestException(AccountExceptionCode.SAME_TO_AND_FROM, false, requestedWSAccount.getId(), ownerAccountId);
        }

        accountRelationService.createOrUpdateAccountRelationBlock(ownerAccountId, requestedWSAccount.getId());

    }

    /**
     * Account unblock another account. Account can not unblock his own account
     * 
     * @param socketEntity
     * @throws Abstract1GroupException
     * @throws Exception
     */
    public void unBlockAccount(final SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {

        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        String ownerAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        // String token =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));

        // WSAccount requestingWSAccount =
        // accountService.fetchActiveAccountFromAuthToken(token);
        WSAccount requestedWSAccount = accountService.fetchAccountDetails(accountId);

        if (requestedWSAccount == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, accountId);
        }
        else if (ownerAccountId.equals(requestedWSAccount.getId()))
        {
            // Avoid Self Referencing Account
            throw new AccountNotFoundException(AccountExceptionCode.SAME_TO_AND_FROM, false, requestedWSAccount.getId(), ownerAccountId);
        }

        // Unblock account
        accountRelationService.updateAccountRelationUnblock(ownerAccountId, requestedWSAccount.getId());
    }

    public void updateScore(final SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String score = null;

        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        String ownerAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        WSRequest request = socketEntity.getRequest();

        String accountRelationStr = request.getAccountRelations() == null ? null : request.getAccountRelations().toString();

        validate("account_relations", accountRelationStr, NOT_NULL(), NOT_EMPTY());
        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        score = request.getAccountRelations().getScore();

        validate("score", score, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_LESS_THAN(11), IS_GREATER_THAN(-11));

        //String token = socketEntity.getHeader(HttpHeaderType.AUTHORISATION);

        WSAccount requestedWSAccount, requestingWSAccount = null;
       // requestingWSAccount = accountService.fetchActiveAccountFromAuthToken(token);
        requestedWSAccount = accountService.fetchAccountDetails(accountId);

        // block account
        if (requestedWSAccount == null)
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, accountId);
        }
        else if (ownerAccountId.equals(requestedWSAccount.getId()))
        {
            // Avoid Self Referencing Account
            throw new AccountNotFoundException(AccountExceptionCode.SAME_TO_AND_FROM, false, requestedWSAccount.getId(), ownerAccountId);
        }

        accountRelationService.updateAccountRelationScore(ownerAccountId, requestedWSAccount.getId(), Integer.parseInt(score));

    }

    public WSAccount getAccountbyShortReference(final SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException,
            General1GroupException
    {
        String shortReference = socketEntity.getPathParam(PathParamType.SHORT_REFERENCE);
        //String token = socketEntity.getHeader(HttpHeaderType.AUTHORISATION);

        validate("short_reference", shortReference, NOT_NULL(), NOT_EMPTY());

        // If requestingAccount does not exist, the
        // fetchAccountFromAuthToken throws an error.
        // If requestedAccount does not exist, data in the response is set
        // to null.
        WSAccount requestedWSAccount = null;
       // WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(token);

        requestedWSAccount = accountService.fetchAccountByShortReference(shortReference);

        if (requestedWSAccount == null)
        {
            // throw new General1GroupException(HTTPResponseType.BAD_REQUEST,
            // "400", true, "No Account found for id " + shortReference);
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, shortReference);
        }
        requestedWSAccount.setStatus(null);
        return requestedWSAccount;
    }

    public WSChatThread getStarredBroadcastOfAccount(SocketEntity entity) throws RequestValidationException, AccountNotFoundException, JsonMappingException, JsonGenerationException, IOException
    {

        String offsetStr = null;
        String limitStr = null;

        String accountId = entity.getPathParam(PathParamType.ACCOUNT_IDS);

        if (accountId == null)
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, false, accountId);

        WSAccount wsAccount = accountService.fetchAccountDetails(accountId);
        if (wsAccount.getStatus().equals(Status.UNREGISTERED))
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_UNREGISTERD, false, accountId);

        String pgNum = !Utils.isNull(entity.getQueryParam(QueryParamType.PAGE_NO)) ? entity.getQueryParam(QueryParamType.PAGE_NO) : "0";
        String pgId = !Utils.isNull(entity.getQueryParam(QueryParamType.PAGE_ID)) ? entity.getQueryParam(QueryParamType.PAGE_ID) : UUID.randomUUID().toString();

        if (!Utils.isNull(pgNum))
        {
            validate("pgnum", pgNum + "", NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        }

        int pgNumValue = Integer.valueOf(pgNum);

        WSChatThread wsChatThread = groupsService.fetchStarredMessageOfAccount(accountId, pgNumValue, pgId);

        return wsChatThread;
    }
}
