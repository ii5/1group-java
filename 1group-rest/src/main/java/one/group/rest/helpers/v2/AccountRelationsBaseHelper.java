package one.group.rest.helpers.v2;

import static one.group.core.Constant.VALID_PHONE_NUMBER_REGEX;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.LENGTH_BETWEEN;
import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.StoreKey;
import one.group.core.enums.SyncActionType;
import one.group.entities.api.request.WSContactsInfo;
import one.group.entities.api.request.v2.WSContacts;
import one.group.entities.api.request.v2.WSInvitation;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSInvites;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSReferralContacts;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.ClientService;
import one.group.services.InviteLogService;
import one.group.services.NativeContactsService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class AccountRelationsBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(AccountRelationsBaseHelper.class);
    private AccountRelationService accountRelationService;

    private AccountService accountService;

    private ClientService clientService;

    private NativeContactsService nativeContactsService;

    private InviteLogService inviteLogService;

    public InviteLogService getInviteLogService()
    {
        return inviteLogService;
    }

    public void setInviteLogService(InviteLogService inviteLogService)
    {
        this.inviteLogService = inviteLogService;
    }

    public NativeContactsService getNativeContactsService()
    {
        return nativeContactsService;
    }

    public void setNativeContactsService(NativeContactsService nativeContactsService)
    {
        this.nativeContactsService = nativeContactsService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    /**
     * To fetch contacts of an account
     * 
     * @param socketEntity
     * @return
     * @throws Abstract1GroupException
     * @throws Exception
     */
    // public Set<WSAccountRelations> fetchContacts(final SocketEntity
    // socketEntity) throws Abstract1GroupException, Exception
    // {
    //
    // // String authToken =
    // // socketEntity.getHeader(HttpHeaderType.AUTHORISATION);
    // String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
    // // accountService.fetchAccountIdFromAuthToken(authToken);
    //
    // Set<WSAccountRelations> wsAccountRelation =
    // accountRelationService.fetchContactsOfAccount(accountId);
    //
    // return wsAccountRelation;
    // }

    public Set<WSAccount> fetchContacts(final SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId =
        // accountService.fetchAccountIdFromAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        Set<WSAccount> wsAccount = accountRelationService.fetchContactsOfAccountDetail(accountId);

        return wsAccount;
    }

    /**
     * To save native contacts of an account
     * 
     * @param socketEntity
     * @throws JsonProcessingException
     * @throws Abstract1GroupException
     * @throws Exception
     */
    public void syncContacts(final SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, Exception
    {

        WSRequest contactsRequest = socketEntity.getRequest();

        String contactsJson = contactsRequest.getContacts() == null ? null : contactsRequest.toString();

        validate("contacts", contactsJson, NOT_NULL(), NOT_EMPTY());
        WSContacts contacts = contactsRequest.getContacts();

        String nativeContactsJson = Utils.getJsonString(contacts.getNativeContacts());
        nativeContactsJson = contacts.getNativeContacts() == null ? null : nativeContactsJson;
        validate("native_contacts", nativeContactsJson, NOT_NULL(), NOT_EMPTY());

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId =
        // accountService.fetchAccountIdFromAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        WSContactsInfo contactsInfo = new WSContactsInfo();

        contactsInfo = accountRelationService.syncContactsForAccountBulk(accountId, contacts);

        if (contactsInfo != null && !(contactsInfo.getDeltaContacts().isEmpty()))
        {
            // accountRelationService.pushToSyncLog(HintType.FETCH,
            // EntityType.ACCOUNT, accountId, SyncActionType.SYNC_CONTACTS,
            // null);
            accountRelationService.pushSyncHintToSyncLog(HintType.FETCH, EntityType.ACCOUNT_RELATION, accountId, SyncActionType.SYNC_CONTACTS, contactsInfo);
        }
    }

    /**
     * To fetch invite list of account
     * 
     * @param socketEntity
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    public WSInvites fetchInviteListOfAccount(final SocketEntity socketEntity) throws AuthenticationException, ClientProcessException, AccountNotFoundException
    {

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // WSAccount wsAccount =
        // accountService.fetchActiveAccountFromAuthToken(authToken);
        //
        // String accountId = wsAccount.getId();
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        WSInvites wsInvites = nativeContactsService.fetchAllInvitesOfAccount(accountId);

        return wsInvites;
    }

    /**
     * To send invitation to contacts to join 1group
     * 
     * @param socketEntity
     * @throws RequestValidationException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws IOException
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */

    public void inviteContacts(final SocketEntity socketEntity) throws RequestValidationException, JsonMappingException, JsonGenerationException, IOException, AuthenticationException,
            ClientProcessException, AccountNotFoundException
    {

        String authToken = Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));

        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);

        WSRequest request = socketEntity.getRequest();

        String inviteestring = (!Utils.isNull(request.getInvitees())) ? request.getInvitees().toString() : null;
        validate("invitees", inviteestring, NOT_NULL(), NOT_EMPTY());

        List<String> inviteesList = request.getInvitees();

        // validate phone numbers
        for (String phoneNumber : inviteesList)
        {
            validate("number", phoneNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(Constant.MOBILE_NUMBER_LENGTH, Constant.MOBILE_NUMBER_LENGTH), MATCHES(VALID_PHONE_NUMBER_REGEX));
        }
        inviteLogService.sendInvitationAndUpdateMarketingList(inviteesList, wsAccount);
    }

    public void inviteReferralContacts(final SocketEntity socketEntity) throws RequestValidationException, JsonMappingException, JsonGenerationException, IOException, AuthenticationException,
            ClientProcessException, AccountNotFoundException
    {

        WSRequest request = socketEntity.getRequest();

        String invitationString = (!Utils.isNull(request.getInvitation())) ? request.getInvitation().toString() : null;
        validate("invitation", invitationString, NOT_NULL(), NOT_EMPTY());

        WSInvitation wsInvitation = request.getInvitation();

        List<WSNativeContact> wsNativeContacts = wsInvitation.getContacts();
        String mobileNumber = wsInvitation.getMobileNumber();
        String message = wsInvitation.getMessage();

        List<String> nativeNumbers = new ArrayList<String>();

        for (WSNativeContact wsNativeContact : wsNativeContacts)
        {
            List<String> nativeContacts = wsNativeContact.getNumbers();
            if (nativeContacts != null && !nativeContacts.isEmpty())
            {
                String nativeContact = nativeContacts.get(0);
                validate("number", nativeContact, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(Constant.MOBILE_NUMBER_LENGTH, Constant.MOBILE_NUMBER_LENGTH), MATCHES(VALID_PHONE_NUMBER_REGEX));
                nativeNumbers.add(nativeContact);
            }
        }
        inviteLogService.sendInvitation(nativeNumbers, message);
    }

    public WSReferralContacts fetchReferralsOfAccount(final SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException
    {
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId =
        // accountService.fetchAccountIdFromAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        String pgNum = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_NO)) ? socketEntity.getQueryParam(QueryParamType.PAGE_NO) : "0";
        String pgId = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_ID)) ? socketEntity.getQueryParam(QueryParamType.PAGE_ID) : UUID.randomUUID().toString();

        if (!Utils.isNull(pgNum))
        {
            validate("pgnum", pgNum + "", NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        }

        int pgNumValue = Integer.valueOf(pgNum);

        // String accountId =
        // socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        WSReferralContacts wsReferralContacts = nativeContactsService.fetchReferralsOfAccount(accountId, pgNumValue, pgId);

        return wsReferralContacts;
    }
}
