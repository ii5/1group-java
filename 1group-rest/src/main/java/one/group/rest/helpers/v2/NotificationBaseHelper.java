package one.group.rest.helpers.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.entities.api.response.v2.WSNotification;
import one.group.entities.api.response.v2.WSNotificationListResult;
import one.group.entities.socket.SocketEntity;
import one.group.services.SearchNotificationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NotificationBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(NotificationBaseHelper.class);
    private SearchNotificationService searchNotificationService;

    public SearchNotificationService getSearchNotificationService()
    {
        return searchNotificationService;
    }

    public void setSearchNotificationService(SearchNotificationService searchNotificationService)
    {
        this.searchNotificationService = searchNotificationService;
    }

    public WSNotificationListResult fechNotificationOfAccount(final SocketEntity socketEntity)
    {
        WSNotificationListResult notificationListResult = new WSNotificationListResult();

        List<WSNotification> wsNotificationList = new ArrayList<WSNotification>();
        wsNotificationList.add(WSNotification.dummyData());
        wsNotificationList.add(WSNotification.dummyData());
        wsNotificationList.add(WSNotification.dummyData());
        wsNotificationList.add(WSNotification.dummyData());
        wsNotificationList.add(WSNotification.dummyData());

        notificationListResult.setNotifications(wsNotificationList);

        return notificationListResult;
    }

    /**
     * To remove notifications. Account can only remove his notification
     * 
     * @param socketEntity
     */
    public void deleteNotificationOfAccount(final SocketEntity socketEntity)
    {

    }

    /**
     * To mark notification as read. Account can only mark his notification
     * 
     * @param socketEntity
     */
    public void markAsReadNotification(final SocketEntity socketEntity)
    {

    }

    public void sendSearchNotificationToAll()
    {
        searchNotificationService.fetchAndSendNotificationToAll();
    }
}
