package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.MAX_TOKEN_COUNT;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.StoreKey;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.core.enums.status.BroadcastStatus;
import one.group.dao.MessageDAO;
import one.group.dao.UserDAO;
import one.group.entities.api.pagination.Pagination;
import one.group.entities.api.request.v2.WSBroadcastTag;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.request.v2.WSUserRequest;
import one.group.entities.api.response.WSBroadcastResult;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSBroadcastListResult;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSMessageResponseResult;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.User;
import one.group.entities.socket.Message;
import one.group.entities.socket.RequestEntity;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.BroadcastExceptionCode;
import one.group.exceptions.codes.MessageExceptionCode;
import one.group.exceptions.codes.RequestValidationExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.BroadcastProcessException;
import one.group.exceptions.movein.MessageProcessException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountService;
import one.group.services.BroadcastService;
import one.group.services.ClientService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author ashishthorat
 *
 */
public class BroadcastBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(BroadcastBaseHelper.class);

    private BroadcastService broadcastService;

    private AccountService accountService;

    private ClientService clientService;

    private MessageDAO messageDAO;

    private UserBaseHelper userBaseHelper;

    private UserDAO userDao;

    public UserDAO getUserDao()
    {
        return userDao;
    }

    public void setUserDao(UserDAO userDao)
    {
        this.userDao = userDao;
    }

    public UserBaseHelper getUserBaseHelper()
    {
        return userBaseHelper;
    }

    public void setUserBaseHelper(UserBaseHelper userBaseHelper)
    {
        this.userBaseHelper = userBaseHelper;
    }

    public MessageDAO getMessageDAO()
    {
        return messageDAO;
    }

    public void setMessageDAO(MessageDAO messageDAO)
    {
        this.messageDAO = messageDAO;
    }

    public BroadcastService getBroadcastService()
    {
        return broadcastService;
    }

    public void setBroadcastService(BroadcastService broadcastService)
    {
        this.broadcastService = broadcastService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public ClientService getClientService()
    {
        return clientService;
    }

    public void setClientService(ClientService clientService)
    {
        this.clientService = clientService;
    }

    /**
     * API callback to save new property listing
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return String json response
     */

    public WSBroadcastResult addBroadcast(SocketEntity socketEntity) throws JsonParseException, Exception, Abstract1GroupException, RequestValidationException
    {
        String broadcastId, broadcastType, cityId, comissionType, createdBy, propertyMarket, propertyType, title, transactionType = null;
        long area;
        WSBroadcast wsbroadcast = null;

        WSRequest request = socketEntity.getRequest();

        if (request == null)
            throw new RequestValidationException(RequestValidationExceptionCode.IS_NULL, false);

        String broadcastStr = (!Utils.isNull(request.getBroadcast())) ? request.getBroadcast().toString() : null;
        validate("broadcast", broadcastStr, NOT_NULL());

        String adminId = request.getAdminUser().getId();
        validate("adminId", adminId, NOT_NULL());

        String messageId = request.getBroadcast().getMessageId();
        broadcastId = request.getBroadcast().getBroadcastId();

        List<WSBroadcastTag> wsTagList = request.getBroadcast().getTags();

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String adminAccountId =
        // clientService.getAccountIdByAuthToken(authToken);
        String adminAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("adminAccountId", adminAccountId, NOT_NULL());

        Set<WSBroadcastTag> wsbroadcastTagSet = new HashSet<WSBroadcastTag>();
        for (WSBroadcastTag wsTag : wsTagList)
        {
            // validation
            wsbroadcastTagSet.add(wsTag);
        }

        validate("messageId", messageId, NOT_NULL());
        Message message = messageDAO.fetchMessageFromId(messageId);

        if (Utils.isNull(message))
        {
            throw new MessageProcessException(BroadcastExceptionCode.INVALID_MESSAGE_ID_FOUND, false, messageId);
        }
        else if (message.getBroadcastId() != null && broadcastId == null)
        {
            throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_ALREADY_CREATED, false, broadcastId);
        }
        else if (message.getMessageSource() != null && message.getMessageSource().equals(MessageSourceType.DIRECT))
        {
            throw new MessageProcessException(MessageExceptionCode.MESSAGE_NOT_FOUND, false, messageId);
        }
        String accountId = message.getCreatedBy();
        if (Utils.isNullOrEmpty(accountId))
        {
            String senderPhonenumber = message.getSenderPhoneNumber();

            if (Utils.isNullOrEmpty(senderPhonenumber))
            {
                throw new BroadcastProcessException(BroadcastExceptionCode.MESSAGE_SENDER_PHONENUMBER_NOT_FOUND, false);
            }
            Account account = userDao.fetchUserByNumber(senderPhonenumber);
            if (Utils.isNull(account))
            {
                RequestEntity requestEntity = new RequestEntity();
                WSUserRequest userRequest = new WSUserRequest();
                userRequest.setMobileNumber(senderPhonenumber);
                requestEntity.setUser(userRequest);

                SocketEntity entity = new SocketEntity();
                entity.setRequest(requestEntity);
                userBaseHelper.saveUser(entity, SyncStatus.UNDEFINED, UserSource.SYNC, true);

                account = userDao.fetchUserByNumber(senderPhonenumber);
                accountId = account.getId();
            }
            else
            {
                accountId = account.getId();
            }
        }

        wsbroadcast = broadcastService.saveBroadcast(adminId, broadcastId, messageId, accountId, wsbroadcastTagSet);
        wsbroadcast = broadcastService.appendToSyncLog(wsbroadcast);
        WSBroadcastResult wsResult = new WSBroadcastResult();
        wsResult.setBroadcastId(wsbroadcast.getId());
        return wsResult;
    }

    public void starBroadcast(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId = clientService.getAccountIdByAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        // String accountId = "5972138471255927";
        String broadcastId = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);
        validate("broadcastId", broadcastId, NOT_NULL());

        broadcastService.starBroadcast(accountId, broadcastId);

    }

    public WSBroadcastListResult getBroadcasts(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {
        // String authToken =
        // socketEntity.getHeader(HttpHeaderType.AUTHORISATION);
        // String accountId = clientService.getAccountIdByAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        List<WSBroadcast> wsbroadcastList = broadcastService.getBroadcastOfAccount(accountId);
        WSBroadcastListResult result = new WSBroadcastListResult();
        result.setBroadcasts(wsbroadcastList);
        return result;
    }

    public WSBroadcastListResult fethcBroadcastListOfAccount(SocketEntity socketEntity) throws JsonMappingException, JsonGenerationException, IOException, NoSuchAlgorithmException
    {
        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);

        // TODO Updated pagination
        UriInfo uriInfo = null;
        HttpHeaders httpHeaders = null;
        Pagination pagination = new Pagination(uriInfo, httpHeaders, EntityType.BROADCAST, accountId);

        String refreshPaginationKey = pagination.getRefreshPaginationKey();
        String paginationKey = pagination.getPaginationKey();
        int pageNo = pagination.getPageNo();

        return broadcastService.fetchAllBroadcastOfAccount(accountId, paginationKey, pageNo, refreshPaginationKey);
    }

    public void unStarredBroadcast(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId = clientService.getAccountIdByAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        // String accountId = "5972138471255927";
        String broadcastId = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);
        validate("broadcastId", broadcastId, NOT_NULL());
        broadcastService.unStarBroadcast(accountId, broadcastId);
    }

    /**
     * Function to update updateBroadcast, close updateBroadcast, mark as
     * hot/not hot, renew updateBroadcast
     * 
     * @param uriInfo
     * @param httpHeaders
     * @param formParams
     * @param amenities
     * 
     * @return {@link String}
     * @throws Exception
     */
    public WSBroadcastResult updateBroadcast(SocketEntity socketEntity) throws Exception
    {
        String broadcastId, broadcastStr = null;
        WSBroadcast wsbroadcast = null;
        Account account = null;
        WSRequest request = socketEntity.getRequest();
        broadcastId = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);
        validate("broadcastId", broadcastId, NOT_NULL());

        String broadcastStatusStr = socketEntity.getQueryParam(QueryParamType.STATUS);

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String requestedAccountId =
        // clientService.getAccountIdByAuthToken(authToken);
        String requestedAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("requestedAccountId", requestedAccountId, NOT_NULL());
        WSBroadcastResult wsResult = new WSBroadcastResult();
        // update Broadcast
        if (Utils.isNullOrEmpty(broadcastStatusStr) && !Utils.isNull(request))
        {
            String adminId = request.getAdminUser().getId();
            validate("adminId", adminId, NOT_NULL());

            broadcastStr = (!Utils.isNull(request.getBroadcast())) ? request.getBroadcast().toString() : null;

            validate("broadcastStr", broadcastStr, NOT_NULL());
            String messageId = request.getBroadcast().getMessageId();
            validate("messageId", messageId, NOT_NULL());

            Message message = messageDAO.fetchMessageFromId(messageId);

            if (Utils.isNull(message))
            {
                throw new MessageProcessException(MessageExceptionCode.MESSAGE_NOT_FOUND, false, messageId);
            }
            else if (message.getBroadcastId() != null && broadcastId == null)
            {
                throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_ALREADY_CREATED, false, broadcastId);
            }
            else if (message.getMessageSource().equals(MessageSourceType.DIRECT))
            {
                throw new MessageProcessException(MessageExceptionCode.MESSAGE_NOT_FOUND, false, messageId);
            }

            String accountId = message.getCreatedBy();
            if (Utils.isNullOrEmpty(accountId))
            {
                String senderPhonenumber = message.getSenderPhoneNumber();

                if (Utils.isNullOrEmpty(senderPhonenumber))
                {
                    throw new BroadcastProcessException(BroadcastExceptionCode.MESSAGE_SENDER_PHONENUMBER_NOT_FOUND, false);
                }
                account = userDao.fetchUserByNumber(senderPhonenumber);
                if (Utils.isNull(account))
                {
                    RequestEntity requestEntity = new RequestEntity();
                    WSUserRequest userRequest = new WSUserRequest();
                    userRequest.setMobileNumber(senderPhonenumber);
                    requestEntity.setUser(userRequest);

                    SocketEntity entity = new SocketEntity();
                    entity.setRequest(requestEntity);
                    userBaseHelper.saveUser(entity, SyncStatus.UNDEFINED, UserSource.SYNC, true);

                    account = userDao.fetchUserByNumber(senderPhonenumber);
                    accountId = account.getId();
                }
                else
                {
                    accountId = account.getId();
                }
            }

            List<WSBroadcastTag> wsTagList = request.getBroadcast().getTags();
            Set<WSBroadcastTag> wsbroadcastTagSet = new HashSet<WSBroadcastTag>();
            for (WSBroadcastTag wsTag : wsTagList)
            {
                // validation
                wsbroadcastTagSet.add(wsTag);
            }
            wsbroadcast = broadcastService.saveBroadcast(adminId, broadcastId, messageId, accountId, wsbroadcastTagSet);
            wsbroadcast = broadcastService.appendToSyncLog(wsbroadcast);
            wsResult.setBroadcastId(wsbroadcast.getId());
        }
        else
        {
            // update status of broadcast
            BroadcastStatus broadcastStatus = BroadcastStatus.parse(broadcastStatusStr);
            if (broadcastStatus == null)
            {
                throw new BroadcastProcessException(BroadcastExceptionCode.BROADCAST_STATUS_NOT_FOUND, true, broadcastStatusStr);
            }

            WSBroadcast wsBroadcast = broadcastService.transitionStatus(broadcastId, broadcastStatus, requestedAccountId);
            wsResult.setCreatedTime(wsBroadcast.getUpdatedTime());
            broadcastService.appendToSyncLogForEditRenewBroadcast(wsBroadcast, requestedAccountId);
        }
        return wsResult;
    }

    public WSMessageResponseResult getBroadcastByBroadcastId(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {

        String broadcastIds = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);

        validate("broadcast_ids", broadcastIds, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));
        String[] broadcastIdArray = broadcastIds.split(",");
        List<String> listbroadcastIds = new ArrayList<String>(Arrays.asList(broadcastIdArray));

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId = clientService.getAccountIdByAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();
        validate("accountId", accountId, NOT_NULL());
        User user = userDao.fetchUserByUserId(accountId);
        if (Utils.isNull(user))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }
        // Call is available for admin and client also
        List<WSMessageResponse> messages = broadcastService.getBroadcastByBroadcastIds(accountId, listbroadcastIds);
        WSMessageResponseResult messageResult = new WSMessageResponseResult();
        messageResult.setMessages(messages);
        return messageResult;
    }

    public void renewBroadcast(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {

        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String accountId = clientService.getAccountIdByAuthToken(authToken);
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String broadcastId = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);
        broadcastService.starBroadcast(accountId, broadcastId);

    }

    public WSMessageResponseResult getBroadcastsOfAccount(SocketEntity socketEntity) throws Abstract1GroupException, Exception
    {
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String requestingAccount =
        // clientService.getAccountIdByAuthToken(authToken);

        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);
        validate("account_id", accountId, NOT_EMPTY(), NOT_NULL());

        WSAccount wsAccount = accountService.fetchActiveAccountDetails(accountId);

        if (Utils.isNull(wsAccount))
        {
            throw new AccountNotFoundException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, accountId);
        }
        List<WSMessageResponse> messages = broadcastService.getBroadcastDetailsByAccountId(accountId);
        WSMessageResponseResult messageResult = new WSMessageResponseResult();
        messageResult.setMessages(messages);
        return messageResult;
    }

    public void updateBroadcastTag(SocketEntity socketEntity) throws Abstract1GroupException
    {
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        // String requestingAccountId =
        // clientService.getAccountIdByAuthToken(authToken);

        WSRequest request = socketEntity.getRequest();
        String requestString = (request == null) ? null : (request.getBroadcast() == null ? null : (request.getBroadcast().getTags() == null ? null : request.getBroadcast().toString()));

        validate("broadcast", requestString, NOT_EMPTY());

        String broadcastId = request.getBroadcast().getBroadcastId();

        validate("broadcast.id", broadcastId, NOT_EMPTY());

        broadcastService.updateBroadcastTags(request.getBroadcast());
    }
}
