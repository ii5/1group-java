package one.group.rest.helpers.v2;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.utils.Utils;

/**
 * 
 * @author sanilshet
 * 
 *         This class implements Exception Mapper Interface, and it use to
 *         provide proper json error when tomcat throws its default web based
 *         html error.
 *
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable>
{

    public GenericExceptionMapper()
    {
        // default constructor
    }

    /**
     * 
     */
    public Response toResponse(Throwable exception)
    {
        WSResponse wsResponse = new WSResponse();
        WSResult wsResult = new WSResult();
        wsResult.setCode(String.valueOf(getStatusCode(exception)));
        wsResult.setMessage(exception.getMessage());
        wsResponse.setResult(wsResult);

        return Response.status(getStatusCode(exception)).entity(Utils.getJsonString(wsResponse)).type(MediaType.APPLICATION_JSON).build();
    }

    /**
     * 
     * @param exception
     * @return
     */
    private int getStatusCode(Throwable exception)
    {
        if (exception instanceof WebApplicationException)
        {
            return ((WebApplicationException) exception).getResponse().getStatus();
        }
        return Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
    }
}
