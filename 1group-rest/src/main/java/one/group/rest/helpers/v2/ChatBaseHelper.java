package one.group.rest.helpers.v2;

import static one.group.core.Constant.DEFAULT_CHAT_THREAD_INDEX_BY;
import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_BETWEEN;
import static one.group.validate.operations.Operations.IS_GREATER_OR_EQUAL;
import static one.group.validate.operations.Operations.IS_GREATER_THAN;
import static one.group.validate.operations.Operations.IS_LESS_OR_EQUAL;
import static one.group.validate.operations.Operations.IS_LESS_THAN;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.LENGTH;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_EQUAL;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.ONE_OF_WITH_SPLIT;
import static one.group.validate.operations.Operations.ONE_SET;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import one.group.core.Constant;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.Rooms;
import one.group.core.enums.StoreKey;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSBroadcastSearchQueryRequest;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.request.v2.WSPhotoReference;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadList;
import one.group.entities.api.response.v2.WSChatThreadListMetaDataResult;
import one.group.entities.api.response.v2.WSChatThreadReference;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSSharedItem;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.ChatExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.exceptions.movein.SyncLogProcessException;
import one.group.services.AccountRelationService;
import one.group.services.AccountService;
import one.group.services.ChatMessageService;
import one.group.services.ChatThreadService;
import one.group.services.ClientService;
import one.group.services.GroupsService;
import one.group.services.SMSService;
import one.group.services.helpers.EnvironmentConfiguration;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * 
 * @author miteshchavda
 * 
 */
public class ChatBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(ChatBaseHelper.class);

    private ClientService clientService;
    private ChatMessageService chatMessageService;
    private ChatThreadService chatThreadService;
    private AccountService accountService;
    private AccountRelationService accountRelationService;
    private GroupsService groupsService;
    private SMSService smsCountryService;
    private SMSService smsNetcoreService;
    private EnvironmentConfiguration environmentConfiguration;

    public ClientService getClientService()
    {
        return clientService;
    }

    public ChatMessageService getChatMessageService()
    {
        return chatMessageService;
    }

    public ChatThreadService getChatThreadService()
    {
        return chatThreadService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setClientService(final ClientService clientService)
    {
        this.clientService = clientService;
    }

    public void setChatMessageService(final ChatMessageService chatMessageService)
    {
        this.chatMessageService = chatMessageService;
    }

    public void setChatThreadService(final ChatThreadService chatThreadService)
    {
        this.chatThreadService = chatThreadService;
    }

    public void setAccountService(final AccountService accountService)
    {
        this.accountService = accountService;
    }

    public AccountRelationService getAccountRelationService()
    {
        return accountRelationService;
    }

    public void setAccountRelationService(AccountRelationService accountRelationService)
    {
        this.accountRelationService = accountRelationService;
    }

    public GroupsService getGroupsService()
    {
        return groupsService;
    }

    public void setGroupsService(GroupsService groupsService)
    {
        this.groupsService = groupsService;
    }

    public SMSService getSmsCountryService()
    {
        return smsCountryService;
    }

    public void setSmsCountryService(SMSService smsCountryService)
    {
        this.smsCountryService = smsCountryService;
    }

    public SMSService getSmsNetcoreService()
    {
        return smsNetcoreService;
    }

    public void setSmsNetcoreService(SMSService smsNetcoreService)
    {
        this.smsNetcoreService = smsNetcoreService;
    }

    public EnvironmentConfiguration getEnvironmentConfiguration()
    {
        return environmentConfiguration;
    }

    public void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration)
    {
        this.environmentConfiguration = environmentConfiguration;
    }

    /**
     * Get a chat thread's messages.
     * 
     * @param limit
     *            Optional.If provided, must be an integer in the range [-100,
     *            100] inclusive. Specifies the maximum number of results to
     *            return. A positive value indicates that up to limit results
     *            will be returned with an offset equal to or greater than the
     *            provided offset. A negative value indicates that up to -limit
     *            results will be returned with an offset equal to or less than
     *            the provided offset. If not provided, defaults to -20.
     * 
     * @param offset
     *            Optional. If provided, must be an integer in the range [0,
     *            MAX_INT] inclusive, where MAX_INT is the max value for a
     *            signed 32 bit integer (2^31-1). Specifies the index into the
     *            list to begin scanning for results. Providing an offset that
     *            is beyond the end of the list and a negative limit will return
     *            up to limit results from the end of the list. Providing an
     *            offset that is beyond the end of the list and a non-negative
     *            limit is a valid request but will return no results. If not
     *            provided, defaults to MAX_INT.
     * 
     * @param chat_thread_id
     *            The chat thread id to request information about.
     * 
     * @return
     */
    public WSChatThread getChatThreadMessages(SocketEntity entity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String authToken = Utils.getAuthTokenFromHeaderString(entity.getHeader(HttpHeaderType.AUTHORISATION));
        String accountId = accountService.fetchAccountIdFromAuthToken(authToken);

        String chatThreadId = null;
        String offsetStr = null;
        String limitStr = null;

        chatThreadId = entity.getPathParam(PathParamType.CHAT_THREAD_IDS);
        limitStr = entity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = entity.getQueryParam(QueryParamType.OFFSET);

        validate("chat_thread_id", chatThreadId, NOT_NULL(), NOT_EMPTY());

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }

        // Get chat thread
        WSChatThread chatThread = new WSChatThread();

        return chatThread;
    }

    /**
     * Get chat threads for the currently authenticated account.
     * 
     * @param index_by
     *            If provided, must be either "joined" or "activity". If
     *            "joined" then chat threads are returned according to the order
     *            they were joined by the account. The indexes of chat threads
     *            in the "joined" list are generally not re-ordered or removed,
     *            although this is not strictly guaranteed. If "activity" than
     *            chat threads are returned in an order that generally
     *            represents the most recent chat message (although from a
     *            strict timing perspective, this also is not strictly
     *            guaranteed). The indexes of chat threads in the "activity"
     *            list are generally re-ordered over time. Therefore, if
     *            attempting to page through all of an account's chat threads,
     *            it is recommended to use "joined". If not provided, defaults
     *            to "activity".
     * @param limit
     *            Optional. If provided, must be an integer in the range [-100,
     *            100] inclusive. Specifies the maximum number of results to
     *            return. A positive value indicates that up to limit results
     *            will be returned with an offset equal to or greater than the
     *            provided offset. A negative value indicates that up to -limit
     *            results will be returned with an offset equal to or less than
     *            the provided offset. If not provided, defaults to -20.
     * @param offset
     *            Optional. If provided, must be an integer in the range [0,
     *            MAX_INT] inclusive, where MAX_INT is the max value for a
     *            signed 32 bit integer (2^31-1). Specifies the index into the
     *            list to begin scanning for results. Providing an offset that
     *            is beyond the end of the list and a negative limit will return
     *            up to limit results from the end of the list. Providing an
     *            offset that is beyond the end of the list and a non-negative
     *            limit is a valid request but will return no results. If not
     *            provided, defaults to MAX_INT.
     * @return
     */
    public WSChatThreadList getChatThreadsForAccount(SocketEntity entity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        // Get fromAccountId
        // String authToken =
        // Utils.getAuthTokenFromHeaderString(entity.getHeader(HttpHeaderType.AUTHORISATION));
        // WSAccount wsAccount =
        // accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountId = "accountId"; // wsAccount.getId();

        String offsetStr = null;
        String limitStr = null;
        String indexBy = DEFAULT_CHAT_THREAD_INDEX_BY;

        limitStr = entity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = entity.getQueryParam(QueryParamType.OFFSET);
        indexBy = entity.getQueryParam(QueryParamType.INDEX_BY);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
            // IS_LESS_THAN(totalChatThreads)
        }
        if (!Utils.isNull(indexBy))
        {
            // TODO: Needs to change logic
            String[] ChatThreadType = { Constant.CHAT_THREAD_BY_ACTIVITY, Constant.CHAT_THREAD_BY_JOINED };
            validate("index_by", indexBy, NOT_NULL(), NOT_EMPTY(), ONE_OF(ChatThreadType));
        }
        else
        {
            indexBy = Constant.DEFAULT_CHAT_THREAD_INDEX_BY;
        }

        // Fetch max chat thread index
        Integer maxChatThreadIndex = 29;// chatThreadService.getMaxChatThreadIndex(
        // accountId, indexBy);
        Integer totalChatThreads = maxChatThreadIndex;

        // If chat thread index by is "joined" and no limit has been set
        // then fetch records from 0 to 20
        // If chat thread index by is "activity" and no limit has been set
        // then fetch records from totalRecords-maxResult to maxResult
        if (indexBy.equals(Constant.CHAT_THREAD_BY_JOINED) && Utils.isNullOrEmpty(limitStr))
        {
            totalChatThreads = Constant.REDIS_MAX_RESULTS - 1;
        }

        Map<String, Integer> limitAndOffset = Utils.getOffsetAndLimit(offsetStr, limitStr, totalChatThreads);
        int limit = limitAndOffset.get("limit");
        int offset = limitAndOffset.get("offset");

        // Fetch chat thread list of an account
        List<WSChatThreadReference> chatThreads = chatThreadService.fetchChatThreadOfAccountDummy(accountId, indexBy, offset, limit);

        WSChatThreadList wsChatThreadList = new WSChatThreadList(chatThreads, maxChatThreadIndex);

        return wsChatThreadList;

    }

    /**
     * Send a chat message to another account.
     * 
     * @param to_account_id
     *            account id of the account to send to.
     * @param type
     *            Identifies the type of chat message to be sent. Must be either
     *            "text", "call", "property_listing", or "photo".
     * @param text
     *            Text content of text based chat message. This field is
     *            required if and only if type is "text". Max length of 4000
     *            characters.
     * @param property_listing_id
     *            Property Listing Id of the property listing to send. Exactly
     *            one of either this field, or property_listing_short_reference
     *            is required if type is "property_listing". If type is "call"
     *            then this field can be optionally used to indicate if a call
     *            was made in reference to a specific property listing. If type
     *            is neither "property_listing" or "call", then this field must
     *            not be provided.
     * @param property_listing_short_reference
     *            Property listing short reference of the property listing to
     *            send. Exactly one of either this field, or property_listing_id
     *            is required if and only if type is "property_listing".
     * @param photo
     *            .jpg or .png file with a max size of 2MB. This field is
     *            required if and only if the type is "photo". Uploaded photos
     *            will be validated and potentially resized before they are
     *            distributed through the chat service.
     * @return
     */
    public WSMessageResponse sendChatMessage(final SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String fromAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        WSRequest request = socketEntity.getRequest();

        String messageStr = (!Utils.isNull(request.getMessage())) ? request.getMessage().toString() : null;
        validate("message", messageStr, NOT_NULL(), NOT_EMPTY());

        WSMessage wsMessage = request.getMessage();
        String createdById = wsMessage.getCreatedById();
        String toAccountId = wsMessage.getToAccountId();
        String type = !Utils.isNull(wsMessage.getType()) ? wsMessage.getType().toString() : null;
        String clientSentTime = !Utils.isNull(wsMessage.getClientSentTime()) ? wsMessage.getClientSentTime().toString() : null;

        if (!createdById.equals(fromAccountId))
        {
            throw new General1GroupException(ChatExceptionCode.CREATED_BY_NOT_MATCHED, true, createdById);
        }

        if (createdById.equals(toAccountId))
        {
            if (!(type.equals(MessageType.TEXT.toString()) || type.equals(MessageType.PHOTO.toString())))
            {
                throw new General1GroupException(ChatExceptionCode.ME_MESSAGE, true);
            }
        }
        validate("to_account_id", toAccountId, NOT_NULL(), NOT_EMPTY());
        validate("type", type, NOT_NULL(), NOT_EMPTY(), ONE_OF(MessageType.MessageTypes()));
        validate("client_sent_time", clientSentTime, NOT_NULL(), NOT_EMPTY());

        // V2-469 allow chat to seeded and active accounts
        WSAccount wsToAccount = accountService.fetchAccountWithOutLocationsFromId(toAccountId);
        if (wsToAccount == null)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, toAccountId);
        }
        else if ((!wsToAccount.getStatus().equals(Status.ACTIVE)) && (!wsToAccount.getStatus().equals(Status.SEEDED)))
        {
            throw new General1GroupException(AccountExceptionCode.FORBIDDEN, true, toAccountId);
        }

        // check if requesting account is blocked by target account
        boolean isBlocked = accountRelationService.isAccountBlocked(fromAccountId, toAccountId);
        if (isBlocked)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_IS_BLOCKED, true, toAccountId);
        }

        wsMessage.setCreatedById(fromAccountId);
        if (type.equals(MessageType.TEXT.toString()))
        {
            String text = wsMessage.getText();
            validate("text", text, NOT_NULL(), NOT_EMPTY(), LENGTH(Constant.CHAT_MESSAGE_CHAR_LIMIT));
        }
        else if (type.equals(MessageType.PHOTO.toString()))
        {
            if (Utils.isNull(wsMessage.getPhoto()))
            {
                validate("photo", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getFull()))
            {
                validate("full", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getThumbnail()))
            {
                validate("thumbnail", null, NOT_NULL());
            }

            WSPhotoReference wsPhoto = wsMessage.getPhoto();
            String fullLocation = wsPhoto.getFull().getLocation();
            String thumbnailLocation = wsPhoto.getThumbnail().getLocation();
            validate("full_location", fullLocation, NOT_NULL(), NOT_EMPTY());
            validate("thumbnail_location", thumbnailLocation, NOT_NULL(), NOT_EMPTY());
        }
        else if (type.equals(MessageType.BROADCAST.toString()))
        {
            String broadcastId = wsMessage.getBroadcastId();

            if (Utils.isNullOrEmpty(broadcastId) == true)
            {
                validate("broadcast_id", null, NOT_NULL(), NOT_EMPTY());
            }
        }
        else if (type.equals(MessageType.PHONE_CALL.toString()))
        {
            // Dont do anything here
        }
        else
        {
            throw new General1GroupException(ChatExceptionCode.INVALID_MESSAGE_TYPE, true, type);
        }
        WSMessageResponse wsMessageResponse = chatMessageService.saveChatMessage(wsMessage);

        // Push chat message to sync log
        if (!wsMessageResponse.isMessageRepeated())
        {
            String requestHash = socketEntity.getRequestHash();
            chatMessageService.pushChatMessageToSyncLog(wsMessageResponse, requestHash);
        }

        // send sms for first chatMessage to Seeded Account
        if ((wsToAccount.getStatus().equals(Status.SEEDED)) && (wsMessageResponse.getMessageSource().equals(MessageSourceType.DIRECT.toString())) && (wsMessageResponse.getIndex() == 0))
        {
            String smsContent = null;

            WSAccount senderAccount = accountService.fetchAccountDetails(createdById);
            String userName = senderAccount.getFullName();

            if (wsMessageResponse.getType().equals(MessageType.TEXT))
            {
                // smsContent = userName + " " +
                // environmentConfiguration.getSmsTextContent();
                smsContent = environmentConfiguration.getSmsTextContent();
            }
            else if (wsMessageResponse.getType().equals(MessageType.PHONE_CALL))
            {
                smsContent = userName + " " + environmentConfiguration.getSmsPhoneCallContent();
            }

            String mobileNumber = wsToAccount.getPrimaryMobile();

            smsCountryService.sendSMS(mobileNumber, smsContent);
            smsNetcoreService.sendSMS(mobileNumber, smsContent);
        }
        return wsMessageResponse;
    }

    public WSMessageResponse sendChatMessageDummy(final SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        // Check if chatTread is is passed

        // if not then
        // create group & generate chat thread id & save participant for this
        // group

        // save chat message under this chat thread id
        String authToken = Utils.getAuthTokenFromHeaderString(socketEntity.getHeader(HttpHeaderType.AUTHORISATION));
        String fromAccountId = accountService.fetchAccountIdFromAuthToken(authToken);

        WSRequest request = socketEntity.getRequest();

        String messageStr = (!Utils.isNull(request.getMessage())) ? request.getMessage().toString() : null;
        validate("message", messageStr, NOT_NULL(), NOT_EMPTY());

        WSMessage wsMessage = request.getMessage();
        String toAccountId = wsMessage.getToAccountId();
        String type = !Utils.isNull(wsMessage.getType()) ? wsMessage.getType().toString() : null;
        String clientSentTime = !Utils.isNull(wsMessage.getClientSentTime()) ? wsMessage.getClientSentTime().toString() : null;

        validate("to_account_id", toAccountId, NOT_NULL(), NOT_EMPTY(), NOT_EQUAL(fromAccountId));
        validate("type", type, ONE_OF(MessageType.MessageTypes()));
        validate("client_sent_time", clientSentTime, NOT_NULL(), NOT_EMPTY());

        // MOV-2685: Temporarily block chat with seeded accounts, open for only
        // active accounts.
        // WSAccount wsToAccount =
        // accountService.fetchActiveAccountDetails(toAccountId);
        // if (wsToAccount == null)
        // {
        // throw new
        // General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true,
        // toAccountId);
        // }

        // check if requesting account is blocked by target account
        // boolean isBlocked =
        // accountRelationService.isAccountBlocked(fromAccountId, toAccountId);
        // if (isBlocked)
        // {
        // throw new
        // General1GroupException(AccountExceptionCode.ACCOUNT_IS_BLOCKED, true,
        // toAccountId);
        // }

        wsMessage.setCreatedById(fromAccountId);
        if (type.equals(MessageType.TEXT.toString()))
        {
            String text = wsMessage.getText();
            validate("text", text, NOT_NULL(), NOT_EMPTY(), LENGTH(Constant.CHAT_MESSAGE_CHAR_LIMIT));
        }
        else if (type.equals(MessageType.PHOTO.toString()))
        {
            if (Utils.isNull(wsMessage.getPhoto()))
            {
                validate("photo", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getFull()))
            {
                validate("full", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getThumbnail()))
            {
                validate("thumbnail", null, NOT_NULL());
            }

            WSPhotoReference wsPhoto = wsMessage.getPhoto();
            String fullLocation = wsPhoto.getFull().getLocation();
            String thumbnailLocation = wsPhoto.getThumbnail().getLocation();
            validate("full_location", fullLocation, NOT_NULL(), NOT_EMPTY());
            validate("thumbnail_location", thumbnailLocation, NOT_NULL(), NOT_EMPTY());
        }
        else if (type.equals(MessageType.BROADCAST.toString()))
        {
            String shortReference = wsMessage.getBroadcastShortReference();
            String broadcastId = wsMessage.getBroadcastId();

            if (Utils.isNullOrEmpty(shortReference) == true && Utils.isNullOrEmpty(broadcastId) == true)
            {
                validate("broadcast_short_reference or broadcast_id", null, NOT_NULL(), NOT_EMPTY());
            }
            if (!Utils.isNullOrEmpty(shortReference) && !Utils.isNullOrEmpty(broadcastId))
            {
                validate("broadcast_short_reference and broadcast_id", shortReference, ONE_SET(broadcastId));
            }
        }
        else if (type.equals(MessageType.PHONE_CALL.toString()))
        {
            String text = wsMessage.getText();
            validate("text", text, NOT_NULL(), NOT_EMPTY());
        }
        else
        {
            throw new General1GroupException(ChatExceptionCode.INVALID_MESSAGE_TYPE, true, type);
        }
        WSMessageResponse wsMessageResponse = chatMessageService.saveChatMessage(wsMessage);

        // Push chat message to sync log
        if (!wsMessageResponse.isMessageRepeated())
        {
            // String clientId =
            // clientService.getClientIdByAuthToken(authToken);
            // chatMessageService.pushChatMessageToSyncLog(wsMessageResponse,
            // clientId);
        }

        return wsMessageResponse;

    }

    /**
     * Request to advance cursors in a chat thread.
     * 
     * @param read_index
     *            Optional. If provided, updates the server's state for
     *            read_index to min(max_chat_message_index,
     *            max(server_read_index, requested_read_index)).
     * 
     * @param received_index
     *            Optional. If provided, updates the server's state for
     *            received_index to min(max_chat_message_index,
     *            max(server_received_index, requested_received_index,
     *            requested_read_index (if also provided in request))).
     * 
     * @param chat_thread_id
     *            The chat thread id of the cursor.
     * 
     * @return
     */
    public WSChatThreadCursor updateCursor(SocketEntity entity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String jsonString = "";

        String accountId = entity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String chatThreadId = null;

        chatThreadId = entity.getPathParam(PathParamType.CHAT_THREAD_IDS);

        chatThreadId = Utils.isNullOrEmpty(chatThreadId) ? null : chatThreadId;
        validate("chat_thread_id", chatThreadId, NOT_NULL(), NOT_EMPTY());

        /*
         * if (wsParticipants == null || wsParticipants.size() == 0) { throw new
         * General1GroupException(GeneralExceptionCode.NOT_FOUND, true); } else
         * { List<String> participants = new ArrayList<String>(); for
         * (WSParticipants wsParticipant : wsParticipants) {
         * participants.add(wsParticipant.getAccountId()); } if
         * (!participants.contains(accountId)) { throw new
         * General1GroupException(GeneralExceptionCode.FORBIDDEN, true); } }
         */

        String chatThreadCursor = null;
        String readIndex = null;
        String receivedIndex = null;
        String receivedMessageId = null;
        String readMessageId = null;
        WSRequest request = entity.getRequest();

        chatThreadCursor = (!Utils.isNull(request.getChatThreadCursor())) ? String.valueOf(request.getChatThreadCursor()) : null;

        validate("chat_thread_cursor", chatThreadCursor, NOT_NULL(), NOT_EMPTY());

        receivedIndex = (!Utils.isNull(request.getChatThreadCursor().getReceivedIndex())) ? String.valueOf(request.getChatThreadCursor().getReceivedIndex()) : receivedIndex;
        readIndex = (!Utils.isNull(request.getChatThreadCursor().getReadIndex())) ? String.valueOf(request.getChatThreadCursor().getReadIndex()) : readIndex;
        receivedMessageId = request.getChatThreadCursor().getReceivedMessageId();
        readMessageId = request.getChatThreadCursor().getReadMessageId();

        validate("read_message_id", readMessageId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        validate("received_message_id", receivedMessageId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());
        // validate("read_index", readIndex, NOT_NULL(), NOT_EMPTY(),
        // IS_NUMERIC(), IS_GREATER_THAN(-1));
        // validate("received_index", receivedIndex, NOT_NULL(), NOT_EMPTY(),
        // IS_NUMERIC(), IS_GREATER_THAN(-1));

        readIndex = (!Utils.isNull(readIndex)) ? readIndex : Constant.DEFAULT_CHAT_THREAD_CURSOR;
        receivedIndex = (!Utils.isNull(receivedIndex)) ? receivedIndex : Constant.DEFAULT_CHAT_THREAD_CURSOR;

        int readIndexValue = Integer.valueOf(readIndex);
        int receivedIndexValue = Integer.parseInt(receivedIndex);
        WSChatThreadCursor wsChatThreadCursor = groupsService.updateChatThreadCursor(chatThreadId, accountId, receivedIndexValue, readIndexValue, receivedMessageId, readMessageId);

        return wsChatThreadCursor;
    }

    public WSChatThreadList getChatThreadsByBroadcast(SocketEntity socketEntity) throws Abstract1GroupException, JsonProcessingException, Exception
    {
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String broadcastId = socketEntity.getPathParam(PathParamType.BROADCAST_IDS);

        validate("broadcast_id", broadcastId, NOT_NULL(), NOT_EMPTY());

        String limitStr = socketEntity.getQueryParam(QueryParamType.LIMIT);
        String offsetStr = socketEntity.getQueryParam(QueryParamType.OFFSET);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }

        WSChatThreadList wsChatThreadList = groupsService.fetchChatThreadListByBroadcast(accountId, broadcastId);
        return wsChatThreadList;
    }

    public WSChatThread getDirectChatThreadMessages(SocketEntity entity) throws RequestValidationException, JsonMappingException, JsonGenerationException, IOException
    {

        String accountId = entity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        WSRequest request = entity.getRequest();

        String participantOne = (!Utils.isNull(request.getChatThread().getParticipants())) ? String.valueOf(request.getChatThread().getParticipants().get(0)) : null;
        String participantTwo = (!Utils.isNull(request.getChatThread().getParticipants())) ? String.valueOf(request.getChatThread().getParticipants().get(1)) : null;

        String pgNum = !Utils.isNull(entity.getQueryParam(QueryParamType.PAGE_NO)) ? entity.getQueryParam(QueryParamType.PAGE_NO) : "0";
        String pgId = !Utils.isNull(entity.getQueryParam(QueryParamType.PAGE_ID)) ? entity.getQueryParam(QueryParamType.PAGE_ID) : UUID.randomUUID().toString();

        if (!Utils.isNull(pgNum))
        {
            validate("pgnum", pgNum + "", NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        }

        int pgNumValue = Integer.valueOf(pgNum);
        WSChatThread wsChatThread = groupsService.fetchDirectChatThreadOfAccount(accountId, participantOne, participantTwo, pgNumValue, pgId);

        return wsChatThread;

    }

    public WSChatThreadListMetaDataResult getMetaDataChatThread(SocketEntity socketEntity) throws RequestValidationException, JsonMappingException, JsonGenerationException, General1GroupException,
            IOException
    {

        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        WSChatThreadListMetaDataResult wsChatThreadListMetaDataResult = groupsService.fetchMetaDataChatThread(accountId);
        return wsChatThreadListMetaDataResult;

    }

    public void deleteChatThreadMessages(SocketEntity socketEntity) throws RequestValidationException, General1GroupException, AuthenticationException, ClientProcessException,
            AccountNotFoundException, SyncLogProcessException
    {
        String chatThreadId = null;

        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        chatThreadId = socketEntity.getPathParam(PathParamType.CHAT_THREAD_IDS);

        validate("chat_thread_id", chatThreadId, NOT_NULL(), NOT_EMPTY());

        groupsService.deleteGroup(accountId, chatThreadId);
    }

    /*
     * public WSChatThread getDummyStarredChatThreadMessages(SocketEntity
     * entity) throws RequestValidationException {
     * 
     * WSChatThread thread = new WSChatThread();
     * thread.setChatThreadId("chat-thread-5666263985569411-5947948258647571");
     * thread.setMaxChatMessageIndex(2);
     * 
     * List<WSChatMessage> chatlist = new ArrayList<WSChatMessage>();
     * 
     * WSChatMessage chatMessage = new WSChatMessage();
     * chatMessage.setFromAccountId("1");
     * chatMessage.setServerTime(Utils.getSystemTime() + "");
     * chatMessage.setId(UUID.randomUUID() + "");
     * 
     * chatMessage.setText(
     * "Property in delhi residential 1BHK apartement, sale price 6000000" + ""
     * +
     * "I have one more property in Delhi residential 2BHK apartment, sale price 5000000"
     * ); chatMessage.setIsStarred(true);
     * chatMessage.setType(MessageType.BROADCAST); WSBroadcast wsBroadcast = new
     * WSBroadcast(); wsBroadcast.setId("broadcast_1"); wsBroadcast.setTags(
     * "Delhi-Res-Apartment-1BHK-6000000, Delhi-Res-Apartment-2BHK-5000000");
     * wsBroadcast.setType(BroadcastType.PROPERTYLISTING);
     * chatMessage.setBroadcast(wsBroadcast);
     * 
     * WSChatMessage chatMessage1 = new WSChatMessage();
     * chatMessage1.setFromAccountId("2"); chatMessage1.setId(UUID.randomUUID()
     * + ""); chatMessage1.setServerTime(Utils.getSystemTime() + "");
     * 
     * chatMessage1.setText(
     * "Property in mumbai residential 1BHK apartement, sale price 35000000" +
     * "" +
     * "I have one more property in Thane residential 2BHK apartment, sale price 450000000"
     * );
     * 
     * chatMessage1.setType(MessageType.BROADCAST); wsBroadcast = new
     * WSBroadcast(); wsBroadcast.setId("broadcast_2");
     * wsBroadcast.setType(BroadcastType.PROPERTYLISTING); wsBroadcast.setTags(
     * "Mumbai-Res-Apartment-1BHK-35000000, Thane-Res-Apartment-2BHK-45000000");
     * chatMessage1.setBroadcast(wsBroadcast); chatMessage1.setIsStarred(true);
     * 
     * WSChatMessage chatMessage2 = new WSChatMessage();
     * chatMessage2.setFromAccountId("3"); chatMessage2.setId(UUID.randomUUID()
     * + ""); chatMessage2.setServerTime(Utils.getSystemTime() + "");
     * 
     * chatMessage2.setText("Looking to buy for 1BHK in Ahmedabad, price is 2300000"
     * );
     * 
     * chatMessage2.setType(MessageType.BROADCAST); wsBroadcast = new
     * WSBroadcast(); wsBroadcast.setId("broadcast_3");
     * wsBroadcast.setType(BroadcastType.REQUIREMENT);
     * wsBroadcast.setTags("Ahmedabad-Res-Apartment-1BHK-2300000");
     * chatMessage2.setBroadcast(wsBroadcast); chatMessage2.setIsStarred(true);
     * 
     * chatlist.add(chatMessage); chatlist.add(chatMessage1);
     * chatlist.add(chatMessage2);
     * 
     * thread.setChatMessages(chatlist);
     * 
     * return thread;
     * 
     * }
     */

    public WSMessageResponse editChatMessage(final SocketEntity socketEntity) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {
        // Check if chatTread is is passed

        // save chat message under this chat thread id
        String messageId = socketEntity.getPathParam(PathParamType.MESSAGE_IDS);
        validate("message_id", messageId, NOT_NULL(), NOT_EMPTY());

        String fromAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        WSRequest request = socketEntity.getRequest();

        String messageStr = (!Utils.isNull(request.getMessage())) ? request.getMessage().toString() : null;
        validate("message", messageStr, NOT_NULL(), NOT_EMPTY());

        WSMessage wsMessage = request.getMessage();
        String toAccountId = wsMessage.getToAccountId();
        String type = !Utils.isNull(wsMessage.getType()) ? wsMessage.getType().toString() : null;
        String clientSentTime = !Utils.isNull(wsMessage.getClientSentTime()) ? wsMessage.getClientSentTime().toString() : null;

        validate("type", type, ONE_OF(MessageType.MessageTypes()));
        validate("client_sent_time", clientSentTime, NOT_NULL(), NOT_EMPTY());

        // MOV-2685: Temporarily block chat with seeded accounts, open for only
        // active accounts.
        WSAccount wsToAccount = accountService.fetchActiveAccountDetails(toAccountId);
        if (wsToAccount == null)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_NOT_FOUND, true, toAccountId);
        }

        // check if requesting account is blocked by target account
        boolean isBlocked = accountRelationService.isAccountBlocked(fromAccountId, toAccountId);
        if (isBlocked)
        {
            throw new General1GroupException(AccountExceptionCode.ACCOUNT_IS_BLOCKED, true, toAccountId);
        }

        wsMessage.setCreatedById(fromAccountId);
        if (type.equals(MessageType.TEXT.toString()))
        {
            String text = wsMessage.getText();
            validate("text", text, NOT_NULL(), NOT_EMPTY(), LENGTH(Constant.CHAT_MESSAGE_CHAR_LIMIT));
        }
        else if (type.equals(MessageType.BROADCAST.toString()))
        {
            String shortReference = wsMessage.getBroadcastShortReference();
            String broadcastId = wsMessage.getBroadcastId();

            if (Utils.isNullOrEmpty(shortReference) == true && Utils.isNullOrEmpty(broadcastId) == true)
            {
                validate("broadcast_short_reference or broadcast_id", null, NOT_NULL(), NOT_EMPTY());
            }
            if (!Utils.isNullOrEmpty(shortReference) && !Utils.isNullOrEmpty(broadcastId))
            {
                validate("broadcast_short_reference and broadcast_id", shortReference, ONE_SET(broadcastId));
            }
        }
        else if (type.equals(MessageType.PHOTO.toString()))
        {
            if (Utils.isNull(wsMessage.getPhoto()))
            {
                validate("photo", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getFull()))
            {
                validate("full", null, NOT_NULL());
            }
            if (Utils.isNull(wsMessage.getPhoto().getThumbnail()))
            {
                validate("thumbnail", null, NOT_NULL());
            }

            WSPhotoReference wsPhoto = wsMessage.getPhoto();
            String fullLocation = wsPhoto.getFull().getLocation();
            String thumbnailLocation = wsPhoto.getThumbnail().getLocation();
            validate("full_location", fullLocation, NOT_NULL(), NOT_EMPTY());
            validate("thumbnail_location", thumbnailLocation, NOT_NULL(), NOT_EMPTY());
        }
        else
        {
            throw new General1GroupException(ChatExceptionCode.INVALID_MESSAGE_TYPE, true, type);
        }
        WSMessageResponse wsMessageResponse = chatMessageService.editChatMessage(messageId, wsMessage);
        chatMessageService.appendToSyncLogAsEntityReference(fromAccountId, messageId, SyncActionType.EDIT);
        // Push chat message to sync log
        // if (!wsMessageResponse.isMessageRepeated())
        // {
        // String clientId = clientService.getClientIdByAuthToken(authToken);
        // chatMessageService.pushChatMessageToSyncLog(wsMessageResponse,
        // clientId);
        // }

        return wsMessageResponse;
    }

    public one.group.entities.api.response.v2.WSChatThread get1GroupChatThreadMessages(SocketEntity socketEntity) throws JsonMappingException, JsonGenerationException, IOException,
            Abstract1GroupException
    {

        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String offsetStr = null;
        String limitStr = null;

        limitStr = socketEntity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = socketEntity.getQueryParam(QueryParamType.OFFSET);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }

        WSRequest request = socketEntity.getRequest();

        String searchStr = (!Utils.isNull(request.getSearch())) ? request.getSearch().toString() : null;
        validate("search", searchStr, NOT_NULL(), NOT_EMPTY());

        WSBroadcastSearchQueryRequest wsSearchQueryRequest = request.getSearch();

        // Compulsory fields.
        String cityId = wsSearchQueryRequest.getCityId();

        validate("city_id", cityId, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());

        // Optional fields. But if value is present then it must be valid.
        Integer minArea = wsSearchQueryRequest.getMinArea();
        Integer maxArea = wsSearchQueryRequest.getMaxArea();
        String minPrice = wsSearchQueryRequest.getMinPrice();
        String maxPrice = wsSearchQueryRequest.getMaxPrice();
        String propertyType = wsSearchQueryRequest.getPropertyType();
        String locationId = wsSearchQueryRequest.getLocationId();
        String transactionType = wsSearchQueryRequest.getTransactionType();
        String broadcastType = wsSearchQueryRequest.getBroadcastType();
        List<String> rooms = wsSearchQueryRequest.getRooms();
        // String property_sub_type = wsSearchQueryRequest.getPropertySubType();

        if (!Utils.isNull(minArea))
        {
            validate("min_area", minArea + "", ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        }

        if (!Utils.isNull(maxArea))
        {
            validate("max_area", maxArea + "", ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_LESS_OR_EQUAL(String.valueOf(Integer.MAX_VALUE)));
        }
        if (!Utils.isNull(maxArea) && !Utils.isNull(minArea))
        {
            validate("max_area", maxArea + "", ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minArea)));
        }

        validate("min_price", minPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        validate("max_price", maxPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        if (!Utils.isNull(minPrice) && !Utils.isNull(maxPrice))
        {
            validate("max_price", maxPrice, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC(), IS_GREATER_OR_EQUAL(String.valueOf(minPrice)), IS_LESS_OR_EQUAL(String.valueOf(Long.MAX_VALUE)));
        }
        validate("property_type", propertyType, ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",", PropertyType.propertyTypeNames()));
        validate("location_id", locationId, ADVANCE_ONLY_NOT_NULL(), IS_NUMERIC());
        validate("transaction_type", transactionType, ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",", PropertyTransactionType.propertyTransactionTypeNames()));
        validate("broadcast_type", broadcastType, ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",", BroadcastType.broadcastTypeNames()));
        // validate("property_sub_type", property_sub_type,
        // ADVANCE_ONLY_NOT_NULL(), ONE_OF_WITH_SPLIT(",",
        // PropertySubType.subTypeNames()));
        if (rooms != null && !rooms.isEmpty())
        {
            for (String room : rooms)
            {
                validate("rooms", room, NOT_NULL(), NOT_EMPTY(), ONE_OF_WITH_SPLIT(",", Rooms.roomsNames()));
            }
        }

        String pgNum = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_NO)) ? socketEntity.getQueryParam(QueryParamType.PAGE_NO) : "0";
        String pgId = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_ID)) ? socketEntity.getQueryParam(QueryParamType.PAGE_ID) : null;

        if (!Utils.isNull(pgNum))
        {
            validate("pgnum", pgNum + "", NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        }

        int pgNumValue = Integer.valueOf(pgNum);
        WSChatThread chatThread = groupsService.fetch1GroupdMessage(accountId, wsSearchQueryRequest, pgNumValue, pgId);

        return chatThread;
    }

    public one.group.entities.api.response.v2.WSChatThread getStarredChatThreadMessages(SocketEntity entity) throws RequestValidationException
    {
        String offsetStr = null;
        String limitStr = null;

        limitStr = entity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = entity.getQueryParam(QueryParamType.OFFSET);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }
        MessageType[] messageTypeArray = { MessageType.BROADCAST };
        WSAccount toAccount = WSAccount.dummyData("Ruby Parker", "Real Property Eastate", "+919745612307", true);
        WSAccount createdBy = WSAccount.dummyData("Pravin dandekar", "Sherkhan Properpery", "+918974123654", true);
        WSChatThread chatThread = WSChatThread.dummyDataWithType(20, 19, toAccount, createdBy, 5, 3, "145726262698256", messageTypeArray);
        return chatThread;
    }

    public List<WSMessageResponse> getPhonecallMessages(SocketEntity socketEntity) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException
    {
        String accountId = socketEntity.getPathParam(PathParamType.ACCOUNT_IDS);

        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY());

        String offsetStr = null;
        String limitStr = null;

        limitStr = socketEntity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = socketEntity.getQueryParam(QueryParamType.OFFSET);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }

        List<WSMessageResponse> wsMessageResponseList = groupsService.fetchPhonecallMessage(accountId);
        return wsMessageResponseList;
    }

    public WSSharedItem fetchSharedItemsOfChatThread(SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException,
            JsonMappingException, JsonGenerationException, IOException
    {

        String fromAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String offsetStr = null;
        String limitStr = null;

        limitStr = socketEntity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = socketEntity.getQueryParam(QueryParamType.OFFSET);

        String chatThreadId = socketEntity.getPathParam(PathParamType.CHAT_THREAD_IDS);
        validate("chat_thread_id", chatThreadId, NOT_NULL(), NOT_EMPTY());

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }
        WSSharedItem wsSharedItem = chatMessageService.fetchSharedItemsOfChatThread(fromAccountId, chatThreadId);
        return wsSharedItem;

    }

    public WSChatThread fetchMessagesByOneGroupMe(SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException,
            JsonMappingException, JsonGenerationException, IOException
    {
        String accountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        validate("account_id", accountId, NOT_NULL(), NOT_EMPTY());

        String pgNum = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_NO)) ? socketEntity.getQueryParam(QueryParamType.PAGE_NO) : "0";
        String pgId = !Utils.isNull(socketEntity.getQueryParam(QueryParamType.PAGE_ID)) ? socketEntity.getQueryParam(QueryParamType.PAGE_ID) : UUID.randomUUID().toString();

        if (!Utils.isNull(pgNum))
        {
            validate("pgnum", pgNum + "", NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
        }

        int pgNumValue = Integer.valueOf(pgNum);

        WSChatThread wsChatThread = groupsService.fetchMeMessageOfAccount(accountId, pgNumValue, pgId);
        return wsChatThread;
    }

    public List<WSMessageResponse> fetchMessagesByIds(SocketEntity socketEntity) throws RequestValidationException, AuthenticationException, ClientProcessException, AccountNotFoundException,
            JsonMappingException, JsonGenerationException, IOException
    {
        String fromAccountId = socketEntity.getStore().get(StoreKey.CURRENT_ACCOUNT_ID).toString();

        String messageIdsStr = socketEntity.getPathParam(PathParamType.MESSAGE_IDS);
        if (!Utils.isNull(messageIdsStr))
        {
            validate("MessageIds", messageIdsStr, NOT_NULL(), NOT_EMPTY());
        }

        List<String> messageIds = Utils.getCollectionFromString(messageIdsStr, ",");
        String offsetStr = null;
        String limitStr = null;

        limitStr = socketEntity.getQueryParam(QueryParamType.LIMIT);
        offsetStr = socketEntity.getQueryParam(QueryParamType.OFFSET);

        if (!Utils.isNull(limitStr))
        {
            validate("limit", limitStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_BETWEEN(-Constant.PAGE_DEFAULT_MAX_LIMIT, Constant.PAGE_DEFAULT_MAX_LIMIT));
        }
        if (!Utils.isNull(offsetStr))
        {
            validate("offset", offsetStr, NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-1), IS_LESS_THAN(Integer.MAX_VALUE));
        }
        List<WSMessageResponse> wsMessageResponses = chatMessageService.fetchListOfMessages(fromAccountId, messageIds);
        return wsMessageResponses;

    }

}