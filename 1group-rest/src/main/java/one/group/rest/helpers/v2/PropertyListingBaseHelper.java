package one.group.rest.helpers.v2;

import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.IS_EQUAL;
import static one.group.validate.operations.Operations.IS_GREATER_THAN;
import static one.group.validate.operations.Operations.IS_INTEGER;
import static one.group.validate.operations.Operations.IS_LESS_THAN;
import static one.group.validate.operations.Operations.IS_NUMERIC;
import static one.group.validate.operations.Operations.LENGTH;
import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.MAX_TOKEN_COUNT;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.validate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import one.group.core.Constant;
import one.group.core.enums.CommissionType;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.PropertyUpdateActionType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;
import one.group.core.enums.status.Status;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.request.WSAdminPropertyListing;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSBookMarkPropertyListings;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSPropertyListing;
import one.group.entities.api.response.WSPropertyListingEditResult;
import one.group.entities.api.response.WSPropertyListingsListResultDehydrated;
import one.group.entities.api.response.WSPropertyListingsListResultHydrated;
import one.group.entities.api.response.WSPropertyListingsResult;
import one.group.entities.api.response.v2.WSAccount;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountService;
import one.group.services.PropertyListingService;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;

/**
 * PropertyListingHelper.java
 * 
 * Purpose: API's related to property listing
 * 
 * @version 1.0
 * 
 * @author sanilshet
 * 
 */
public class PropertyListingBaseHelper
{
    private static final Logger logger = LoggerFactory.getLogger(PropertyListingBaseHelper.class);

    private PropertyListingService propertyListingService;

    private AccountService accountService;

    public PropertyListingService getPropertyListingService()
    {
        return propertyListingService;
    }

    public void setPropertyListingService(PropertyListingService propertyListingService)
    {
        this.propertyListingService = propertyListingService;
    }

    public AccountService getAccountService()
    {
        return accountService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    /**
     * Get property listings of an account
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     */
    public WSPropertyListingsListResultHydrated getListingHydrated(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, Exception
    {

        HashMap<String, Object> data = new HashMap<String, Object>();
        MultivaluedMap<String, String> pathParam = uriInfo.getPathParameters();
        String accountId = pathParam.getFirst("account_id").toString();
        String subscribe = Utils.fetchValueFromQueryParam(uriInfo, "subscribe");
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String dehydrated = "true";
        if (queryParams.containsKey("dehydrated"))
        {
            dehydrated = queryParams.getFirst("dehydrated").toString();
        }

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountIdFromToken = wsAccount.getId();

        String accountDetails = "false";
        if (queryParams.containsKey("account_details"))
        {
            accountDetails = queryParams.getFirst("account_details").toString();
        }

        String locationDetails = "false";
        if (queryParams.containsKey("location_details"))
        {
            locationDetails = queryParams.getFirst("location_details").toString();
        }

        String matchingDetails = "false";
        if (queryParams.containsKey("match_details"))
        {
            matchingDetails = queryParams.getFirst("match_details").toString();
        }

        validate("account_details", accountDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("location_details", locationDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("match_details", matchingDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("dehydrated", dehydrated, ADVANCE_ONLY_NOT_NULL(), ONE_OF("true", "false"));
        validate("subscribe", subscribe, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(Constant.MIN_SUBSCRIBE_TIME_IN_MINUTES),
                IS_LESS_THAN(Constant.MAX_SUBSCRIBE_TIME_IN_MINUTES));

        WSPropertyListingsListResultHydrated propertyListingListResult = new WSPropertyListingsListResultHydrated();
        int totalPropertyListing = propertyListingService.getPropertyListingCountOfAccount(accountId, accountIdFromToken, false);

        List<WSPropertyListing> propertyListings = propertyListingService.getProperyListingOfAccount(accountId, accountIdFromToken, accountDetails, locationDetails, matchingDetails, subscribe);
        propertyListingListResult.setPropertyListings(propertyListings);
        propertyListingListResult.setMaxPropertyListingIndex(totalPropertyListing);

        return propertyListingListResult;

    }

    public WSPropertyListingsListResultDehydrated getListingDehydrated(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, Exception
    {

        HashMap<String, Object> data = new HashMap<String, Object>();
        MultivaluedMap<String, String> pathParam = uriInfo.getPathParameters();
        String accountId = pathParam.getFirst("account_id").toString();
        String subscribe = Utils.fetchValueFromQueryParam(uriInfo, "subscribe");
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String dehydrated = "true";
        if (queryParams.containsKey("dehydrated"))
        {
            dehydrated = queryParams.getFirst("dehydrated").toString();
        }

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountIdFromToken = wsAccount.getId();

        String accountDetails = "false";
        if (queryParams.containsKey("account_details"))
        {
            accountDetails = queryParams.getFirst("account_details").toString();
        }

        String locationDetails = "false";
        if (queryParams.containsKey("location_details"))
        {
            locationDetails = queryParams.getFirst("location_details").toString();
        }

        String matchingDetails = "false";
        if (queryParams.containsKey("match_details"))
        {
            matchingDetails = queryParams.getFirst("match_details").toString();
        }

        validate("account_details", accountDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("location_details", locationDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("match_details", matchingDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("dehydrated", dehydrated, ADVANCE_ONLY_NOT_NULL(), ONE_OF("true", "false"));
        validate("subscribe", subscribe, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(Constant.MIN_SUBSCRIBE_TIME_IN_MINUTES),
                IS_LESS_THAN(Constant.MAX_SUBSCRIBE_TIME_IN_MINUTES));

        WSPropertyListingsListResultDehydrated propertyListingListResult = new WSPropertyListingsListResultDehydrated();
        int totalPropertyListing = propertyListingService.getPropertyListingCountOfAccount(accountId, accountIdFromToken, false);

        List<String> ids = propertyListingService.getPropertyListingIdsOfAccount(accountId, accountIdFromToken);
        propertyListingListResult.setPropertyListings(ids);
        propertyListingListResult.setMaxPropertyListingIndex(totalPropertyListing);

        return propertyListingListResult;

    }

    /**
     * Get property listing details by property listing id
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return
     */

    public WSPropertyListingsResult getProperyListingDetails(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader) throws Abstract1GroupException, Exception
    {

        String authToken = Utils.getAuthTokenFromHeader(httpHeader);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountId = wsAccount.getId();

        String propertyListingIds = Utils.fetchValueFromPathData(uriInfo, "property_listing_ids");
        String subscribe = Utils.fetchValueFromQueryParam(uriInfo, "subscribe");
        String accountDetails = Utils.fetchValueFromQueryParam(uriInfo, "account_details");
        String locationDetails = Utils.fetchValueFromQueryParam(uriInfo, "location_details");
        String matchingDetails = Utils.fetchValueFromQueryParam(uriInfo, "matching_details");

        // validations
        validate("property_listing_ids", propertyListingIds, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));
        validate("subscribe", subscribe, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(Constant.MIN_SUBSCRIBE_TIME_IN_MINUTES),
                IS_LESS_THAN(Constant.MAX_SUBSCRIBE_TIME_IN_MINUTES));

        validate("account_details", accountDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("location_details", locationDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("match_details", matchingDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));

        String[] propertyListingIdArray = propertyListingIds.split(",");

        List<String> listpropertyListingIds = new ArrayList<String>(Arrays.asList(propertyListingIdArray));

        return propertyListingService.getProperyListingDetailsByPropertyListingIds(listpropertyListingIds, accountId, subscribe, accountDetails, locationDetails, matchingDetails);

    }

    /**
     * API callback to save new property listing
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return String json response
     */
    public WSPropertyListingEditResult addPropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, String json) throws Abstract1GroupException, JsonParseException, Exception
    {
        List<String> amenitiesList = new ArrayList<String>();
        String shortReference = null;
        String propertyMarket = null, actionStr = null, customSublocation = null, status = null, isHot, description, propertyType, propertySubType = null, area = null, commisionType = null, rooms = null, locationId, salePrice, rentPrice = null;
        List<String> amenities = null;
        String createdFor = "", externalSource = null, externalListingTime = null;
        WSPropertyListing property;
        WSAdminPropertyListing adminValues = null;
        String propertyListingStr = null;

        WSRequest request = null;
        if (json != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = request.getPropertyListing() == null ? null : json;
            propertyListingStr = (!Utils.isNull(request.getPropertyListing())) ? request.getPropertyListing().toString() : null;
        }
        validate("property_listing", propertyListingStr, NOT_NULL());

        propertyType = request.getPropertyListing().getPropertyType();
        propertySubType = request.getPropertyListing().getPropertySubType();
        propertyMarket = request.getPropertyListing().getPropertyMarket();
        description = request.getPropertyListing().getDescription();
        area = request.getPropertyListing().getArea();
        commisionType = request.getPropertyListing().getCommissionType();
        salePrice = request.getPropertyListing().getSalePrice();
        rentPrice = request.getPropertyListing().getRentPrice();
        rooms = request.getPropertyListing().getRooms();
        locationId = request.getPropertyListing().getLocationId();
        isHot = request.getPropertyListing().getIsHot();
        amenities = request.getPropertyListing().getAmenities();
        adminValues = request.getPropertyListing().getAdmin();
        customSublocation = request.getPropertyListing().getCustomSublocation();
        // validations
        validate("property_type", propertyType, NOT_NULL(), ONE_OF(PropertyType.propertyTypeNames()));
        validate("location", locationId, NOT_NULL(), NOT_EMPTY());
        validate("sale_price", salePrice, NOT_NULL(), NOT_EMPTY());
        validate("rent_price", rentPrice, NOT_NULL(), NOT_EMPTY());
        validate("property_market", propertyMarket, NOT_NULL(), NOT_EMPTY(), ONE_OF(PropertyMarket.propertyMarketNames()));
        validate("property_sub_type", request.getPropertyListing().getPropertySubType(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(PropertySubType.subTypeNames()));
        validate("commision_type", request.getPropertyListing().getCommissionType(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(CommissionType.commisionTypeNames()));
        validate("area", request.getPropertyListing().getArea(), ADVANCE_ONLY_NOT_NULL(), IS_INTEGER(), IS_LESS_THAN(Integer.MAX_VALUE), IS_GREATER_THAN(0));
        validate("rooms", request.getPropertyListing().getRooms(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(Rooms.roomsNames()));
        validate("description", request.getPropertyListing().getDescription(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(4000));
        validate("sell_price", salePrice, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-2), IS_LESS_THAN(Long.MAX_VALUE));
        validate("rent_price", rentPrice, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-2), IS_LESS_THAN(Long.MAX_VALUE));
        String[] possibleNames = new String[] { "true", "false" };
        validate("is_hot", isHot, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF(possibleNames));
        validate("custom_sublocation", customSublocation, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(500));
        // for administrative clients
        if (adminValues != null)
        {
            createdFor = adminValues.getCreatedFor();
            validate("created_for", createdFor, NOT_NULL(), NOT_EMPTY());
            externalListingTime = adminValues.getExternalListingTime();
            externalSource = adminValues.getExternalSource();
            validate("external_listing_time", externalListingTime, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC());
            validate("external_source", externalSource, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY());

            if ((!Utils.isNull(externalListingTime) && Utils.isNull(externalSource)) || (Utils.isNull(externalListingTime) && !Utils.isNull(externalSource)))
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", false, "Either both or none of external_source and external_listing_time must be provided");
            }
        }

        if (amenities != null)
        {
            for (int i = 0; i < amenities.size(); i++)
            {
                amenitiesList.add(amenities.get(i));
            }
        }

        if (salePrice != null && rentPrice != null)
        {
            // if sale and rent price both are zero then throw error
            if (Long.valueOf(request.getPropertyListing().getRentPrice()) == 0 && Long.valueOf(request.getPropertyListing().getSalePrice()) == 0)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Rent price / Sale Price cannot be zero");
            }
            // check rent price is not greater than sale price
            if (Long.valueOf(request.getPropertyListing().getSalePrice()) > 0
                    && (Long.valueOf(request.getPropertyListing().getRentPrice()) > Long.valueOf(request.getPropertyListing().getSalePrice())))
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Rent price cannot be greater than sale price");
            }
        }
        // get account id from authorization token
        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountId = wsAccount.getId();
        PropertyUpdateActionType action = PropertyUpdateActionType.parse(actionStr);
        // save property
        property = propertyListingService.savePropertyListing(false, description, propertyType, propertySubType, area, commisionType, rooms, locationId, amenitiesList, salePrice, rentPrice, status,
                propertyMarket, isHot, action, accountId, shortReference, createdFor, externalListingTime, externalSource, customSublocation);

        property = propertyListingService.appendToSyncLog(property);

        WSPropertyListingEditResult result = new WSPropertyListingEditResult();
        result.setPropertyListingId(property.getId());
        result.setShortReference(property.getShortReference());
        result.setExpiryTime(property.getExpiryTime());

        return result;
    }

    /**
     * Function to book mark property listing
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     *             *
     * 
     * 
     */
    public Map<String, Object> bookMarkPropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, Exception
    {

        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String action = null;
        if (queryParams.containsKey("action"))
        {
            action = queryParams.getFirst("action").toString();
        }

        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountId = wsAccount.getId();

        MultivaluedMap<String, String> pathParam = uriInfo.getPathParameters();
        String propertyListingId = pathParam.getFirst("property_listing_id").toString();
        // action should be either true or false
        validate("action", action, NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));

        propertyListingService.bookMarkProperty(accountId, propertyListingId, action);
        HashMap<String, Object> finalMap = new HashMap<String, Object>();

        return finalMap;
    }

    /**
     * Function to get property listing book marks for a account
     * 
     * @param httpHeader
     * 
     * @return {@link String}
     */
    public Map<String, Object> getBookMarkedPropertyListings(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws Abstract1GroupException, Exception
    {
        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        String accountId = wsAccount.getId();
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String accountDetails = "false";
        if (queryParams.containsKey("account_details"))
        {
            accountDetails = queryParams.getFirst("account_details").toString();
        }

        String locationDetails = "false";
        if (queryParams.containsKey("location_details"))
        {
            locationDetails = queryParams.getFirst("location_details").toString();
        }

        String matchingDetails = "false";
        if (queryParams.containsKey("match_details"))
        {
            matchingDetails = queryParams.getFirst("match_details").toString();
        }

        validate("account_details", accountDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("location_details", locationDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));
        validate("match_details", matchingDetails, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("true", "false"));

        Set<WSBookMarkPropertyListings> bookMarks = propertyListingService.getBookMarkedPropertyListings(accountId, accountDetails, locationDetails, matchingDetails);
        HashMap<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put("book_marks", bookMarks);

        return finalMap;
    }

    /**
     * Function to update property listing, close property listing, mark as
     * hot/not hot, renew property listing
     * 
     * @param uriInfo
     * @param httpHeaders
     * @param formParams
     * @param amenities
     * 
     * @return {@link String}
     */
    public WSPropertyListingEditResult updatePropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, String json) throws Abstract1GroupException, JsonParseException, Exception
    {
        String jsonString = "";
        List<String> amenitiesList = new ArrayList<String>();
        String propertyMarket = null, customSublocation = null, actionStr, status, isHot, description, propertyType, propertySubType = null, area = null, commisionType = null, rooms = null, locationId, salePrice, rentPrice = null;
        List<String> amenities = null;
        String createdFor = "", externalSource = null, externalListingTime = null;
        WSPropertyListing property;
        WSAdminPropertyListing adminValues = null;
        String propertyListingStr = null;

        WSRequest request = null;
        MultivaluedMap<String, String> pathParam = uriInfo.getPathParameters();
        String propertyListingId = pathParam.getFirst("property_listing_id").toString();

        if (json != null)
        {
            request = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = request.getPropertyListing() == null ? null : json;
            propertyListingStr = (!Utils.isNull(request.getPropertyListing())) ? request.getPropertyListing().toString() : null;
        }
        validate("property_listing", propertyListingStr, NOT_NULL());

        propertyType = request.getPropertyListing().getPropertyType();
        propertySubType = request.getPropertyListing().getPropertySubType();
        propertyMarket = request.getPropertyListing().getPropertyMarket();
        description = request.getPropertyListing().getDescription();
        area = request.getPropertyListing().getArea();
        commisionType = request.getPropertyListing().getCommissionType();
        salePrice = request.getPropertyListing().getSalePrice();
        rentPrice = request.getPropertyListing().getRentPrice();
        rooms = request.getPropertyListing().getRooms();
        locationId = request.getPropertyListing().getLocationId();
        isHot = request.getPropertyListing().getIsHot();
        amenities = request.getPropertyListing().getAmenities();
        adminValues = request.getPropertyListing().getAdmin();
        actionStr = request.getPropertyListing().getAction();
        status = request.getPropertyListing().getStatus();
        customSublocation = request.getPropertyListing().getCustomSublocation();
        // validations
        validate("property_type", propertyType, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_EQUAL(PropertyType.RESIDENTIAL.toString()));
        validate("property_sub_type", request.getPropertyListing().getPropertySubType(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(PropertySubType.subTypeNames()));
        validate("commision_type", request.getPropertyListing().getCommissionType(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(CommissionType.commisionTypeNames()));
        validate("area", request.getPropertyListing().getArea(), ADVANCE_ONLY_NOT_NULL(), IS_INTEGER(), IS_LESS_THAN(Integer.MAX_VALUE), IS_GREATER_THAN(0));
        validate("rooms", request.getPropertyListing().getRooms(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(Rooms.roomsNames()));
        validate("description", request.getPropertyListing().getDescription(), ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), LENGTH(4000));
        validate("sell_price", salePrice, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-2), IS_LESS_THAN(Long.MAX_VALUE));
        validate("rent_price", rentPrice, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_NUMERIC(), IS_GREATER_THAN(-2), IS_LESS_THAN(Long.MAX_VALUE));
        String[] possibleNames = new String[] { "true", "false" };
        validate("is_hot", isHot, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY(), ONE_OF(possibleNames));
        validate("custom_sublocation", customSublocation, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY());
        validate("action", actionStr, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF(PropertyUpdateActionType.names()));
        validate("status", status, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), IS_EQUAL("closed"));
        validate("location", locationId, ADVANCE_ONLY_NOT_NULL(), NOT_NULL(), NOT_EMPTY());

        if (amenities != null)
        {
            for (int i = 0; i < amenities.size(); i++)
            {
                amenitiesList.add(amenities.get(i));
            }
        }

        if (salePrice != null && rentPrice != null)
        {
            // check for sale and rent price both are not zero, if provided
            if (Long.valueOf(request.getPropertyListing().getRentPrice()) == 0 && Long.valueOf(request.getPropertyListing().getSalePrice()) == 0)
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Rent price / Sale Price cannot be zero");
            }
            // if provided, rent price should be always less than sale price
            if (Long.valueOf(request.getPropertyListing().getSalePrice()) > 0
                    && (Long.valueOf(request.getPropertyListing().getRentPrice()) > Long.valueOf(request.getPropertyListing().getSalePrice())))
            {
                throw new General1GroupException(HTTPResponseType.BAD_REQUEST, "400", true, "Rent price cannot be greater than sale price");
            }
        }

        // get account id from authorization token
        String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
        WSAccount wsAccount = accountService.fetchActiveAccountFromAuthToken(authToken);
        PropertyUpdateActionType action = PropertyUpdateActionType.parse(actionStr);
        boolean isAdmin = false;
        if (wsAccount.getStatus().equals(Status.ADMIN))
        {
            isAdmin = true;
        }
        String accountId = wsAccount.getId();
        // update/save property
        property = propertyListingService.savePropertyListing(isAdmin, description, propertyType, propertySubType, area, commisionType, rooms, locationId, amenitiesList, salePrice, rentPrice, status,
                propertyMarket, isHot, action, accountId, propertyListingId, createdFor, externalListingTime, externalSource, customSublocation);
        property = propertyListingService.appendToSyncLog(property);

        WSPropertyListingEditResult editResult = new WSPropertyListingEditResult();
        editResult.setPropertyListingId(property.getId());
        editResult.setShortReference(property.getShortReference());
        editResult.setExpiryTime(property.getExpiryTime());
        jsonString = ResponseBuilder.buildOkResponse(editResult, "Property updated successfully");

        return editResult;
    }

    /**
     * Function to get property listing Count
     * 
     * @param httpHeader
     * 
     * @return {@link String}
     * @throws AccountNotFoundException
     * @throws ClientProcessException
     * @throws AuthenticationException
     * @throws General1GroupException
     * @throws RequestValidationException
     */
    public WSEntityCount getActivePropertyListingCount(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders) throws AuthenticationException, ClientProcessException, AccountNotFoundException,
            General1GroupException, RequestValidationException
    {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String status = null;
        WSEntityCount wsEntityCount = null;

        if (queryParams.containsKey("status"))
        {
            status = queryParams.getFirst("status").toString();
            validate("status", status, ADVANCE_ONLY_NOT_NULL(), NOT_EMPTY(), ONE_OF("closed", "expired"));
            if (status.equals("closed"))
            {
                wsEntityCount = propertyListingService.getPropertyListingCountByStatus(BroadcastStatus.CLOSED);
            }
            else if (status.equals("expired"))
            {
                wsEntityCount = propertyListingService.getPropertyListingCountByStatus(BroadcastStatus.EXPIRED);
            }
        }
        else
        {
            wsEntityCount = propertyListingService.getActivepropertyListingCount();
        }

        return wsEntityCount;
    }
}