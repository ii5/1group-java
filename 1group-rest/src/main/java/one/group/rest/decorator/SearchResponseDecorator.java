package one.group.rest.decorator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import one.group.entities.api.response.WSCity;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSPhoto;
import one.group.entities.api.response.v2.WSPhotoReference;
import one.group.entities.api.response.v2.WSTag;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SearchResponseDecorator implements OneGroupResponseDecorator<WSChatThread>
{
	@Override
	public WSChatThread decorate(WSChatThread input) 
	{
		if (input == null)
		{
			return null;
		}
		
		input.setMaxChatMessageIndex(null);
		List<WSMessageResponse> messageResponseList = input.getMessages();
		
		if (messageResponseList == null)
		{
			return input;
		}
		
		for (WSMessageResponse messageResponse : messageResponseList)
		{
			messageResponse.setMessageRepeated(null);
			messageResponse.setVersion(null);
			
			messageResponse.setCreatedBy(trimWSAccount(messageResponse.getCreatedBy()));
//			messageResponse.setToAccount(trimWSAccount(messageResponse.getToAccount()));
			messageResponse.setToAccount(null);
			
			messageResponse.setBroadcast(trimWSBroadcast(messageResponse.getBroadcast()));
			
		}
		
		return input;
	}

	
	private WSAccount trimWSAccount(WSAccount input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setType(null);
		input.setShortReference(null);
		input.setScore(null);
		input.setIsContact(null);
		input.setCreatedTime(null);
		input.setBroadcastCount(null);
		input.setRegistrationTime(null);
		input.setType(null);
		input.setScore(null);
		input.setCreatedTime(null);
		input.setBroadcastCount(null);
		input.setRegistrationTime(null);
		input.setInitialSyncComplete(null);
		
		input.setCursors(trimWSChatCursor(input.getCursors()));
		input.setPhoto(trimWSPhotoReference(input.getPhoto()));
		input.setCity(trimWSCity(input.getCity()));
		
		return input;
	}
	
	private WSChatThreadCursor trimWSChatCursor(WSChatThreadCursor input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setReadIndex(null);
		input.setReceivedIndex(null);
		input.setReadMessageId(null);
		input.setReceivedMessageId(null);
		
		return input;
	}
	
	private WSPhotoReference trimWSPhotoReference(WSPhotoReference input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setFull(trimWSPhoto(input.getFull()));
		input.setThumbnail(trimWSPhoto(input.getThumbnail()));
		
		return input;
	}
	
	private WSPhoto trimWSPhoto(WSPhoto input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setHeight(null);
		input.setWidth(null);
		input.setSize(null);
		
		return input;
	}
	
	private WSBroadcast trimWSBroadcast(WSBroadcast input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setOldBroadcastId(null);
		input.setSyncActionType(null);
		input.setUpdatedTime(null);
		
		Set<WSTag> tagSet = input.getTags();
		
		if (tagSet != null && !tagSet.isEmpty())
		{
			Set<WSTag> trimmedTagSet = new HashSet<WSTag>();
			for (WSTag tag : tagSet)
			{
				trimmedTagSet.add(trimWSTag(tag));
				break;
			}
			
			input.setTags(trimmedTagSet);
		}
		
		
		return input;
	}
	
	private WSTag trimWSTag(WSTag input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setLocation(trimWSLocation(input.getLocation()));
		
		return input;
	}
	
	private WSLocation trimWSLocation(WSLocation input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setParentId(null);
		input.setLatitude(null);
		input.setLongitude(null);
		input.setLocationDisplayName(null);
		
		return input;
	}
	
	private WSCity trimWSCity(WSCity input)
	{
		if (input == null)
		{
			return null;
		}
		
		input.setValue(null);
		input.setLabel(null);
		
		return input;
	}
}
