package one.group.rest.decorator;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface OneGroupResponseDecorator<T>
{
	public abstract T decorate(T input);
}
