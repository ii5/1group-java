package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSInvites;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSReferralContacts;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.AccountRelationsBaseHelper;
import one.group.utils.Utils;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

@Component
@Path("/{version}/")
public class AccountRelationsWebService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountRelationsWebService.class);
    private Map<APIVersion, AccountRelationsBaseHelper> versionHelperMap = new HashMap<APIVersion, AccountRelationsBaseHelper>();

    public Map<APIVersion, AccountRelationsBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, AccountRelationsBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * API to fetch contacts
     * 
     * @param socketEntity
     * @return
     */
    @Path("/accounts/{account_ids}/contacts")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Profiled(tag = "/accounts/{account_ids}/contacts")
    public String fetchContacts(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            Set<WSAccount> wsAccountRelation = helper.fetchContacts(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsAccountRelation, "Contacts for account");

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching contacts.", ame);
            throw ame;
            // ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Error while fetching contacts.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }

    /**
     * API to sync native contacts
     * 
     * @param socketEntity
     * @return
     */
    @Path("/contacts/sync")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Profiled(tag = "/contacts/sync")
    public String syncContacts(final SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.syncContacts(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse("", "Contacts sync completed");

        }

        catch (JsonProcessingException jpe)
        {
            logger.error("Error while syncing contacts.", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while syncing contacts.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while syncing contacts.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }

        return jsonString;
    }

    /**
     * API to fetch invites list to send them invitation to join 1group
     * 
     * @param socketEntity
     * @return
     */
    @GET
    @Path("/invites")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "AccountRelationsWebService - fetchInvites")
    public String fetchInvites(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            boolean isTrue = Utils.checkVersionAndFunctionality(AccountRelationsBaseHelper.class, helper);
            if (helper == null || !isTrue)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            AccountRelationsBaseHelper accountRelationhelper = (AccountRelationsBaseHelper) helper;
            WSInvites invites = accountRelationhelper.fetchInviteListOfAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(invites, "Invites list found");

        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while fetching invites list.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching invites list.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }

        return jsonString;
    }

    /**
     * API to send invitation to phone number to join 1group
     * 
     * @param socketEntity
     * @return
     */
    @POST
    @Path("/invites")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "AccountRelationsWebService - sendInvite")
    public String sendInvite(final SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = (AccountRelationsBaseHelper) versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            helper.inviteContacts(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse("", "Invitation send successfully");

        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while sending invitation to invitees", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while sending invitation to invitees", e);
            // ResponseBuilder.shootException(e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        return jsonString;
    }

    @POST
    @Path("/invites/referral")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "AccountRelationsWebService - sendInvite")
    public String sendReferralInvite(final SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = (AccountRelationsBaseHelper) versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            helper.inviteReferralContacts(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse("", "Invitation send successfully");

        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while sending invitation to invitees", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while sending invitation to invitees", e);
            // ResponseBuilder.shootException(e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        return jsonString;
    }

    @GET
    @Path("/accounts/{account_ids}/contacts/referral")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "AccountRelationsWebService - fetchReferralsOfAccount")
    public String fetchReferralsOfAccount(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountRelationsBaseHelper helper = (AccountRelationsBaseHelper) versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSReferralContacts wsReferralContacts = helper.fetchReferralsOfAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsReferralContacts, "Referral Contacts");
        }

        catch (Abstract1GroupException e)
        {
            logger.error("Error while sending invitation to invitees", e);
            // ResponseBuilder.shootException(e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }
        return jsonString;
    }

}
