package one.group.rest.services.http;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSCityLocations;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.WSLocationResponse;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.LocationBaseHelper;
import one.group.rest.services.socket.ChatWebService;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Path("/{version}/")
public class LocationWebService
{
    private static final Logger logger = LoggerFactory.getLogger(ChatWebService.class);

    private Map<APIVersion, LocationBaseHelper> versionHelperMap = new HashMap<APIVersion, LocationBaseHelper>();

    public Map<APIVersion, LocationBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, LocationBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    @Path("/locations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/locations")
    public String getAllLocalities(@QueryParam("city_ids") String city_ids, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            LocationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSCityLocations wsCityLocations = helper.getAllLocalities(city_ids);

            jsonString = ResponseBuilder.buildOkResponse(wsCityLocations, GeneralSuccessCode.LOCATIONS_FOUND_SUCCESSFUL);

        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }

    @Path("/localities/{location_ids}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/localities/{location_ids}")
    public String getAllLocalitiesByLocationId(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            LocationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSLocationResponse wsLocationResponse = helper.fetchLocalityNameByLocationId(uriInfo, httpHeader);
            jsonString = ResponseBuilder.buildOkResponse(wsLocationResponse, GeneralSuccessCode.LOCATIONS_FETCH_SUCCESSFUL);

        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }

    /**
     * 
     * @param formData
     * @param httpHeaders
     * @return
     * @throws General1GroupException
     */
    @Path("/locations")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/locations")
    public String addLocations(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, @PathParam("version") String version, String json) throws General1GroupException
    {
        String jsonString = "";
        try
        {
            LocationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            int diffLocations = helper.compareLocations(uriInfo, httpHeaders, json);
            if (diffLocations > 0)
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, "Data mismatch. location and location admin data are not synced", new IllegalStateException());
            }

            boolean diffParentLocations = helper.compareWithParentLocations(uriInfo, httpHeaders, json);
            if (!diffParentLocations)
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, "Data mismatch, parent locations has not been synced with location admin.", new IllegalStateException());
            }
            helper.updateLiveLocationTable(uriInfo, httpHeaders, json);
            // helper.generateCityJsonData();

            HashMap<String, String> map = new HashMap<String, String>();
            jsonString = ResponseBuilder.buildOkResponse(map, GeneralSuccessCode.LOCATIONS_PUBLISHED_SUCCESSFUL);
        }
        catch (Abstract1GroupException ae)
        {
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }

    @Path("/cities")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/cities")
    public String getAllCities(@PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            LocationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSCityResult wsCityLocations = helper.getAllCities();

            jsonString = ResponseBuilder.buildOkResponse(wsCityLocations, GeneralSuccessCode.CITIES_FOUND_SUCCESSFUL);
        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonString;
    }
}
