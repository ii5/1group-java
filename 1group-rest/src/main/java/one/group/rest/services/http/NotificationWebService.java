package one.group.rest.services.http;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.NotificationBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Path("/{version}/")
public class NotificationWebService
{
    private static final Logger logger = LoggerFactory.getLogger(NotificationWebService.class);

    private Map<APIVersion, NotificationBaseHelper> versionHelperMap = new HashMap<APIVersion, NotificationBaseHelper>();

    public Map<APIVersion, NotificationBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, NotificationBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    @Path("/send_search_notification")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/send_search_notification")
    public String sendSearchNotification(@PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            NotificationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            helper.sendSearchNotificationToAll();

            jsonString = ResponseBuilder.buildOkResponse(null, GeneralSuccessCode.NOTIFICATION_SEND_SUCCESSFUL);

        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }
}
