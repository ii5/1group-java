/**
 * 
 */
package one.group.rest.services.http;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSPhotoReference;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.UtilityBaseHelper;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author ashishthorat
 *
 */
@Component
@Path("/{version}/")
public class MediaWebService
{
    private static final Logger logger = LoggerFactory.getLogger(MediaWebService.class);

    private Map<APIVersion, UtilityBaseHelper> versionHelperMap = new HashMap<APIVersion, UtilityBaseHelper>();

    public Map<APIVersion, UtilityBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, UtilityBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * This API call is used to upload media for account ( profile pic) Returns
     * photo object that includes full and thumb image path that has been
     * uploaded
     * 
     * @param formData
     * @param httpHeaders
     * @param version
     * @return
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/upload")
    @Profiled(tag = "/uploadProfilePic")
    public String uploadProfilePic(FormDataMultiPart formData, @Context HttpHeaders httpHeaders, @PathParam("version") String version)
    {
        String jsonString = "";
        WSPhotoReference wsPhotoReference = null;
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            wsPhotoReference = helper.uploadProfilePic(formData, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(wsPhotoReference, GeneralSuccessCode.PHOTO_UPLOAD_SUCCESSFUL);
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while uploading image.", ae);
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            logger.error("Error while uploading image.", e);
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonString;
    }

}
