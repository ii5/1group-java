package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountsResult;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.AccountBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * @author nyal fernandes
 * 
 */
@Component
@Path("/{version}/")
public class AccountWebService
{

    private static final Logger logger = LoggerFactory.getLogger(AccountRelationsWebService.class);
    private Map<APIVersion, AccountBaseHelper> versionHelperMap = new HashMap<APIVersion, AccountBaseHelper>();

    public Map<APIVersion, AccountBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, AccountBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * API to get account details
     * 
     * @param accountId
     * @param uriInfo
     * @param httpHeader
     * @return json string
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    @Path("/accounts/{account_ids}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Profiled(tag = "/accounts/{account_ids}")
    public String fetchAccount(@PathParam("dummy") SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAccountsResult result = helper.fetchAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(result, GeneralSuccessCode.ACCOUNT_FETCH_SUCCESSFUL);

        }

        catch (JsonProcessingException jpe)
        {
            logger.error("Error while fetching contacts.", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while fetching contacts.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching contacts.", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    @Path("/accounts/count")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    @Profiled(tag = "/accounts/count")
    public String fetchCount(@PathParam("dummy") SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSEntityCount result = helper.fetchAccountsCount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(result, "Total Account Entities.");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while fetching account count.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching account count.", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    /**
     * API to edit an account. Only account owner can edit his own account but
     * not allowed to edit other account.
     * 
     * @param socketEntity
     * @return
     */
    @PUT
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/accounts/{account_id}")
    @Profiled(tag = "/editAccount")
    public String editAccount(SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAccount wsAccount = helper.editAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsAccount, GeneralSuccessCode.ACCOUNT_UPDATE_SUCCESSFUL);

        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while editing account.", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while editing account.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while editing account.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    /**
     * API to block an account
     * 
     * @param socketEntity
     * @return
     */
    @Path("/accounts/{account_id}/block")
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    @Profiled(tag = "/accounts/{account_id}/block")
    public String blockAccount(@PathParam("dummy") SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.blockAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse("", "Contact Blocked");

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while blocking account.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while blocking account.", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to unblock an account
     * 
     * @param socketEntity
     * @return
     */
    @Path("/accounts/{account_id}/unblock")
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    @Profiled(tag = "/accounts/{account_id}/unblock")
    public String unBlockAccount(@PathParam("dummy") SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.unBlockAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse("", "Contact UnBlocked");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while un-blocking account.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while un-blocking account.", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    /**
     * API to give score to another account
     * 
     * @param socketEntity
     * @return
     */
    @Path("/accounts/{account_id}/score")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Profiled(tag = "/accounts/{account_id}/score")
    public String updateScore(final @PathParam("dummy") SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";

        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateScore(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse("", "Score Updated");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while scoring account.", jpe);
            // ResponseBuilder.shootFatalException(jpe,
            // GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST,
            // "Error parsing request.");
            throw jpe;

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while scoring account.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while scoring account.", e);
            // ResponseBuilder.shootException(new
            // General1GroupException(GeneralExceptionCode.INTERNAL_ERROR,
            // true));
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/account/reference/{short_reference}")
    @Profiled(tag = "/account/reference/{short_reference}")
    public String getAccountDetailsByShortReference(SocketEntity socketEntity) throws JsonProcessingException, Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAccount result = helper.getAccountbyShortReference(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(result, "Account Details.");
        }

        catch (Abstract1GroupException ae)
        {
            logger.error("Error while fetching account by short reference", ae);
            throw ae;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching account by short reference", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/accounts/{account_id}/broadcasts/starred")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /accounts/{account_id}/broadcasts/starred")
    public String getStarredChatThreadMessages(SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            AccountBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSChatThread chatThread = helper.getStarredBroadcastOfAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(chatThread, "Starred broadcast found");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }
}