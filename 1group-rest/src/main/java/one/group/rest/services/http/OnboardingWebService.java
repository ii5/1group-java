package one.group.rest.services.http;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.request.v2.WSRequestOtp;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSClient;
import one.group.entities.api.response.v2.WSOtp;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.SigninBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * Consists of all onboarding related web services.
 * 
 * @author nyalfernandes
 *
 */

@Component
@Path("/{version}/")
public class OnboardingWebService
{
    private static final Logger logger = LoggerFactory.getLogger(OnboardingWebService.class);

    private Map<APIVersion, SigninBaseHelper> versionHelperMap = new HashMap<APIVersion, SigninBaseHelper>();

    public Map<APIVersion, SigninBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, SigninBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * Creates a new unique random four digit OTP and sends SMS on the
     * requesting mobile number. The OTP is uniquely generated each time a
     * client of an account request for an OTP and is valid for only 2 minutes.
     * After the backend system identifies that the otp has been consumed for
     * the account for which it was generated, the otp cannot be used again.
     * Within the validity time and if the otp has not been consumed by the
     * account, the OTP can be used by the account only once.
     * 
     * @param request_otp
     *            {@link WSRequestOtp}
     * @return
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/request_otp")
    @Profiled(tag = "/request_otp")
    public String requestOtp(String json, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            SigninBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSOtp wsOtp = helper.requestOtp(json);

            jsonString = ResponseBuilder.buildOkResponse(wsOtp, GeneralSuccessCode.OTP_SUCCESSFUL);
        }
        catch (JsonProcessingException jpe)
        {
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException me)
        {
            ResponseBuilder.shootException(me);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    /**
     * 
     * Method creates new account, client, oauth client, attaches otp with
     * client and generates oauth access token
     * 
     * @param mobileNumber
     * @param grantType
     * @param otp
     * @param clientType
     * @param deviceId
     * @param deviceUserName
     * @param deviceModelName
     * @param devicePlatform
     * @param deviceOsVersion
     * @return Response Json String
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/sign_in")
    @Profiled(tag = "/sign_in")
    public String signin(String json, @Context UriInfo uriInfo, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {

            SigninBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSClient wsClient = helper.signin(json, uriInfo);
            jsonString = ResponseBuilder.buildOkResponse(wsClient, GeneralSuccessCode.SIGNIN_SUCCESSFUL);
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    /**
     * API to register account
     * 
     * @param uriInfo
     * @param httpHeader
     * @param firstName
     * @param lastName
     * @param localities
     * @param photoStream
     * @param photoDetails
     * @param photoBodyPart
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/registration")
    @Profiled(tag = "/registration")
    public String register(@Context HttpHeaders httpHeaders, String json, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            SigninBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAccount account = helper.register(httpHeaders, json);
            jsonString = ResponseBuilder.buildOkResponse(account, GeneralSuccessCode.REGISTRATION_SUCCESSFUL);

        }

        catch (JsonProcessingException jpe)
        {
            logger.error("Error while registering.", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while registering.", ae);
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            logger.error("Error while registering.", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;
    }

}