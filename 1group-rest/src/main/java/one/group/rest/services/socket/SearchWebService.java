package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.v2.WSBroadcastSearchQuery;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.SearchBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author miteshchavda
 *
 */
@Component
@Path("/{version}/")
public class SearchWebService
{
    private static final Logger logger = LoggerFactory.getLogger(SearchWebService.class);

    private Map<APIVersion, SearchBaseHelper> versionHelperMap = new HashMap<APIVersion, SearchBaseHelper>();

    public Map<APIVersion, SearchBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, SearchBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * Fetch search & search broadcast if result=true
     * 
     * @param uriInfo
     * @param httpHeader
     * @return
     */
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /search")
    public String getSearchResults(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            SearchBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSBroadcastSearchQuery wsSearchResult = helper.getSearchOfAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(wsSearchResult, "Search fetched successfully!!!");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
            // ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonString;
    }

    /**
     * Create search & search broadcasts
     * 
     * @param socketEntity
     * @return
     */
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    @Path("/searches")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /searches")
    public String saveSearchAndFetchBroadcasts(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonOutput = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            SearchBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSBroadcastSearchQuery wsQuery = helper.createSearch(socketEntity);
            // WSSearchResult wsSearchResult = helper.searchBroadcast(wsQuery);

            jsonOutput = ResponseBuilder.buildOkResponse(wsQuery, "Broadcasts found");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
            // ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonOutput;
    }

    /**
     * Update search & fetch broadcasts
     * 
     * @param uriInfo
     * @param httpHeader
     * @return
     */
    @PUT
    @Path("/searches")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "PUT /searches")
    public String updateMetadataAndSearchBroadcast(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            SearchBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSBroadcastSearchQuery wsQuery = helper.updateSearch(socketEntity);
            // WSSearchResult wsSearchResult = helper.searchBroadcast(wsQuery);

            jsonString = ResponseBuilder.buildOkResponse(wsQuery, "Broadcasts found.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }
}
