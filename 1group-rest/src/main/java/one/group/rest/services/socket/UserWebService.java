package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserResponse;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.UserBaseHelper;

@Component
@Path("{version}")
public class UserWebService
{
    private static final Logger logger = LoggerFactory.getLogger(UserWebService.class);

    private Map<APIVersion, UserBaseHelper> versionHelperMap = new HashMap<APIVersion, UserBaseHelper>();

    public Map<APIVersion, UserBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, UserBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    @POST
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public String saveUser(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            UserBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSUserResponse response = helper.saveUser(socketEntity, SyncStatus.UNDEFINED, UserSource.SYNC, true);
            jsonString = ResponseBuilder.buildOkResponse(response, "User saved");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while saving user.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while saving user.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @PUT
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateUser(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            UserBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSUserResponse response = helper.saveUser(socketEntity, null, null, false);
            jsonString = ResponseBuilder.buildOkResponse(response, "User updated");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while saving user.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while saving user.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchUser(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            UserBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserCity> responseList = helper.fetchUsers(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "User info");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }
}
