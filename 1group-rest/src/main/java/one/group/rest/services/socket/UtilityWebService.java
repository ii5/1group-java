package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSAccountStatusResult;
import one.group.entities.api.response.WSMonitor;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.v2.WSApplicationMetaData;
import one.group.entities.api.response.v2.WSClientData;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.codes.GeneralSuccessCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.UtilityBaseHelper;
import one.group.utils.Utils;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Utility services.
 * 
 * @author nyalfernandes
 * 
 */
@Component
@Path("/{version}/")
public class UtilityWebService
{
    private static final Logger logger = LoggerFactory.getLogger(UtilityWebService.class);

    private Map<APIVersion, UtilityBaseHelper> versionHelperMap = new HashMap<APIVersion, UtilityBaseHelper>();

    public Map<APIVersion, UtilityBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, UtilityBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * 
     * @param jsonData
     * @param httpHeaders
     * @param version
     * @return
     * @throws Abstract1GroupException
     */
    @POST
    @Path("/accounts_status")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/accounts_status")
    public String fetchAccountStatus(String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAccountStatusResult accountStatus = helper.fetchAccountStatus(jsonData, httpHeaders);
            jsonString = ResponseBuilder.buildOkResponse(accountStatus, "All online accounts.");

        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while requesting account_status.", jpe);
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, jpe);
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while requesting account_status.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while requesting account_status.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * This API Call is used to delete media associated with entity, Entity can
     * be account , property listings etc etc
     * 
     * @param jsonData
     * @param httpHeaders
     * @param version
     * @return
     * @throws Abstract1GroupException
     */
    @Path("/media")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/media")
    public String removeMedia(String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";

        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            Map<String, Object> finalMap = helper.removeMedia(jsonData, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(finalMap, "Media removed successfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while removing media.", jpe);
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, jpe);
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while removing media.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while removing media.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * 
     * @param jsonData
     * @param httpHeaders
     * @param version
     * @return
     * @throws Abstract1GroupException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/trace_it")
    @Path("/trace_it")
    public String trace(String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.traceIt(jsonData, httpHeaders);
            jsonString = ResponseBuilder.buildOkResponse(null, "Client analytics added successfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while tracing requests.", jpe);
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, jpe);
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while tracing requests.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while tracing requests.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * This API call is called by the admin team whenever sync happens to there
     * end, here in backend we store group count in cache
     * 
     * @param uriInfo
     * @param jsonData
     * @param httpHeaders
     * @param version
     * @return
     * @throws Abstract1GroupException
     */
    @PUT
    @Path("/group_update/{total_group_count}")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/group_update/{total_group_count}")
    public String updateGroupCount(@Context UriInfo uriInfo, String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateCount(uriInfo, httpHeaders);
            jsonString = ResponseBuilder.buildOkResponse(null, "Count Added successfully");
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while updating group count.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while updating group count.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @PUT
    @Path("/update_client_version")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/update_client_version")
    public String updateClientVersion(@Context UriInfo uriInfo, String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            boolean isTrue = Utils.checkVersionAndFunctionality(UtilityBaseHelper.class, helper);
            if (helper == null || !isTrue)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            UtilityBaseHelper utilHelper = (UtilityBaseHelper) helper;
            WSClientData wsClientData = utilHelper.updateClientVersion(jsonData, uriInfo, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(wsClientData, "Client version updated successfully");
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while updating client version.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while updating client version.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @POST
    @Path("/configuration")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/configurations")
    public String configurations(String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {
        String jsonString = "";

        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.traceIt(jsonData, httpHeaders);
            jsonString = ResponseBuilder.buildOkResponse(null, "Client analytics added successfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while tracing requests.", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while tracing requests.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while tracing requests.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * 
     * @param uriInfo
     * @param jsonData
     * @param httpHeaders
     * @param version
     * @return
     * @throws Abstract1GroupException
     */
    @GET
    @Path("/monitoring")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/monitorSystems")
    public String monitorSystems(@Context UriInfo uriInfo, String jsonData, @Context HttpHeaders httpHeaders, @PathParam("version") String version) throws Abstract1GroupException
    {

        String jsonString = "";
        try
        {
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSMonitor isRunning = helper.monitor(uriInfo, httpHeaders);
            jsonString = ResponseBuilder.buildOkResponse(isRunning, "Monitered Succussfully");
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while updating group count.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while updating group count.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/meta_data/application")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/meta_data/application")
    public String fetchApplicationMetadata(final SocketEntity socketEntity) throws Abstract1GroupException
    {

        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSApplicationMetaData appMetadata = helper.fetchApplicationMetadata(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(appMetadata, "Application meta data fetched.");
            System.out.println("jsonString>>" + jsonString);
        }
        catch (Abstract1GroupException e)
        {
            logger.error("Error while fetching application metadata.", e);
            throw e;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching application metadata.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * This API call is used to upload Base64 media ( profile pic) Returns photo
     * object that includes full and thumb image path that has been uploaded
     * 
     * @param formData
     * @param httpHeaders
     * @param version
     * @return
     * @throws n
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/UploadBase64")
    @Profiled(tag = "/UploadBase64")
    public String UploadBase64(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        WSGroupMetaData wsGroupMetaData = null;
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            UtilityBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            wsGroupMetaData = helper.UploadBase64Image(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(wsGroupMetaData, GeneralSuccessCode.PHOTO_UPLOAD_SUCCESSFUL);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }
}
