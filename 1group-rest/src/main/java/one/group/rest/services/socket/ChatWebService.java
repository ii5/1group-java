package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSChatThreadList;
import one.group.entities.api.response.v2.WSChatThreadListMetaDataResult;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.entities.api.response.v2.WSSharedItem;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.decorator.OneGroupResponseDecorator;
import one.group.rest.helpers.v2.ChatBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * @author miteshchavda
 * 
 */
@Component
@Path("/{version}/")
public class ChatWebService
{
    private static final Logger logger = LoggerFactory.getLogger(ChatWebService.class);

    private Map<APIVersion, ChatBaseHelper> versionHelperMap = new HashMap<APIVersion, ChatBaseHelper>();
    
    private OneGroupResponseDecorator<WSChatThread> searchResponseDecorator;
    
    public OneGroupResponseDecorator<WSChatThread> getSearchResponseDecorator() 
    {
		return searchResponseDecorator;
	}

	public void setSearchResponseDecorator(OneGroupResponseDecorator<WSChatThread> searchResponseDecorator) 
	{
		this.searchResponseDecorator = searchResponseDecorator;
	}

	public Map<APIVersion, ChatBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, ChatBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * Get a chat thread's messages.
     * 
     * @param limit
     *            Optional.If provided, must be an integer in the range [-100,
     *            100] inclusive. Specifies the maximum number of results to
     *            return. A positive value indicates that up to limit results
     *            will be returned with an offset equal to or greater than the
     *            provided offset. A negative value indicates that up to -limit
     *            results will be returned with an offset equal to or less than
     *            the provided offset. If not provided, defaults to -20.
     * 
     * @param offset
     *            Optional. If provided, must be an integer in the range [0,
     *            MAX_INT] inclusive, where MAX_INT is the max value for a
     *            signed 32 bit integer (2^31-1). Specifies the index into the
     *            list to begin scanning for results. Providing an offset that
     *            is beyond the end of the list and a negative limit will return
     *            up to limit results from the end of the list. Providing an
     *            offset that is beyond the end of the list and a non-negative
     *            limit is a valid request but will return no results. If not
     *            provided, defaults to MAX_INT.
     * 
     * @param chat_thread_id
     *            The chat thread id to request information about.
     * 
     * @return
     * @throws Abstract1GroupException
     */
    @POST
    @Path("/chat_threads")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /chat_threads")
    public String getDirectChatThreadMessage(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            // WSChatThread chatThread =
            // helper.getChatThreadMessages(socketEntity);
            WSChatThread chatThread = helper.getDirectChatThreadMessages(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(chatThread, "Chat thread details");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching messages of desired chat thread.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    /**
     * Get chat threads for the currently authenticated account.
     * 
     * @param index_by
     *            If provided, must be either "joined" or "activity". If
     *            "joined" then chat threads are returned according to the order
     *            they were joined by the account. The indexes of chat threads
     *            in the "joined" list are generally not re-ordered or removed,
     *            although this is not strictly guaranteed. If "activity" than
     *            chat threads are returned in an order that generally
     *            represents the most recent chat message (although from a
     *            strict timing perspective, this also is not strictly
     *            guaranteed). The indexes of chat threads in the "activity"
     *            list are generally re-ordered over time. Therefore, if
     *            attempting to page through all of an account's chat threads,
     *            it is recommended to use "joined". If not provided, defaults
     *            to "activity".
     * @param limit
     *            Optional. If provided, must be an integer in the range [-100,
     *            100] inclusive. Specifies the maximum number of results to
     *            return. A positive value indicates that up to limit results
     *            will be returned with an offset equal to or greater than the
     *            provided offset. A negative value indicates that up to -limit
     *            results will be returned with an offset equal to or less than
     *            the provided offset. If not provided, defaults to -20.
     * @param offset
     *            Optional. If provided, must be an integer in the range [0,
     *            MAX_INT] inclusive, where MAX_INT is the max value for a
     *            signed 32 bit integer (2^31-1). Specifies the index into the
     *            list to begin scanning for results. Providing an offset that
     *            is beyond the end of the list and a negative limit will return
     *            up to limit results from the end of the list. Providing an
     *            offset that is beyond the end of the list and a non-negative
     *            limit is a valid request but will return no results. If not
     *            provided, defaults to MAX_INT.
     * @return
     * @throws Abstract1GroupException
     */
    @GET
    @Path("/chat_threads")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /chat_threads")
    public String getChatThreadsForAccount(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            // Prepare data
            WSChatThreadList wsChatThreadList = helper.getChatThreadsForAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(wsChatThreadList, "Chat thread listing");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching chat threads of an account.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching chat threads of an account.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * Send a chat message to another account.
     * 
     * @param to_account_id
     *            account id of the account to send to.
     * @param type
     *            Identifies the type of chat message to be sent. Must be either
     *            "text", "call", "property_listing", or "photo".
     * @param text
     *            Text content of text based chat message. This field is
     *            required if and only if type is "text". Max length of 4000
     *            characters.
     * @param property_listing_id
     *            Property Listing Id of the property listing to send. Exactly
     *            one of either this field, or property_listing_short_reference
     *            is required if type is "property_listing". If type is "call"
     *            then this field can be optionally used to indicate if a call
     *            was made in reference to a specific property listing. If type
     *            is neither "property_listing" or "call", then this field must
     *            not be provided.
     * @param property_listing_short_reference
     *            Property listing short reference of the property listing to
     *            send. Exactly one of either this field, or property_listing_id
     *            is required if and only if type is "property_listing".
     * @param photo
     *            .jpg or .png file with a max size of 2MB. This field is
     *            required if and only if the type is "photo". Uploaded photos
     *            will be validated and potentially resized before they are
     *            distributed through the chat service.
     * @return
     * @throws Abstract1GroupException
     */

    @POST
    @Path("/messages")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /messages")
    public String sendChatMessage(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonOutput = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSMessageResponse wsMessage = helper.sendChatMessage(socketEntity);

            jsonOutput = ResponseBuilder.buildOkResponse(wsMessage, "Message sent successfully");
        }
        catch (JsonProcessingException jpe)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, jpe);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonOutput;
    }

    /**
     * Request to advance cursors in a chat thread.
     * 
     * @param read_index
     *            Optional. If provided, updates the server's state for
     *            read_index to min(max_chat_message_index,
     *            max(server_read_index, requested_read_index)).
     * 
     * @param received_index
     *            Optional. If provided, updates the server's state for
     *            received_index to min(max_chat_message_index,
     *            max(server_received_index, requested_received_index,
     *            requested_read_index (if also provided in request))).
     * 
     * @param chat_thread_id
     *            The chat thread id of the cursor.
     * 
     * @return
     * @throws Abstract1GroupException
     */
    @PUT
    @Path("/chat_threads/{chat_thread_ids}")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /chat_threads/{chat_thread_ids}")
    public String updateCursor(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSChatThreadCursor wsChatThreadCursor = helper.updateCursor(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsChatThreadCursor, "Cursor updated successfully.");
        }
        catch (JsonProcessingException jpe)
        {
            throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, jpe);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * Get list of chat threads associated with broadcast
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws Abstract1GroupException
     * 
     */
    @GET
    @Path("/broadcasts/{broadcast_ids}/chat_threads")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/broadcasts/{broadcast_ids}/chat_threads")
    public String getChatThreadsByBroadcast(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSChatThreadList wsChatThreadList = helper.getChatThreadsByBroadcast(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsChatThreadList, "List of chat thread list");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @DELETE
    @Path("/chat_threads/{chat_thread_id}/")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /chat_threads/{chat_thread_id}")
    public String deleteChatThreadMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            // WSChatThread chatThread =
            // helper.getChatThreadMessages(socketEntity);
            helper.deleteChatThreadMessages(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Chat thread delete successfully");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching messages of desired chat thread.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @PUT
    @Path("/messages/{message_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String editChatMessage(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSMessageResponse chatMessage = helper.editChatMessage(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(chatMessage, "Chat message updated successfully");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating chat message.", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating chat message.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    @GET
    @Path("/meta_data/chat_threads")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /meta_data/chat_threads")
    public String getMetadataChatThreads(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSChatThreadListMetaDataResult wsChatThreadListMetaDataResult = helper.getMetaDataChatThread(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(wsChatThreadListMetaDataResult, "Chat message found");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @POST
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /search")
    public String get1GroupChatThreadMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            long start = System.currentTimeMillis();
            WSChatThread chatThread = helper.get1GroupChatThreadMessages(socketEntity);
            chatThread = searchResponseDecorator.decorate(chatThread);
            jsonString = ResponseBuilder.buildOkResponse(chatThread, "Chat message found");
            System.out.println(System.currentTimeMillis() - start);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/messages/{account_ids}/phone_call")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /messages/{account_ids}/phone_call")
    public String getPhonecallMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSMessageResponse> wsMessageResponseList = helper.getPhonecallMessages(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsMessageResponseList, "Phone call messages found");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/chat_threads/{chat_thread_id}/shared/items")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "POST /chat_threads/shared/items")
    public String fetchSharedItemsOfChatThread(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSSharedItem sharedItems = helper.fetchSharedItemsOfChatThread(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(sharedItems, "Shared items of your account.");
            System.out.println("jsonString>>" + jsonString);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/chat_threads/me")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /chat_threads/me")
    public String getChatThreadsByOneGroupMe(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSChatThread chatThread = helper.fetchMessagesByOneGroupMe(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(chatThread, "Chat details of me.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    @GET
    @Path("/messages/{message_ids}")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /messages/{message_ids}")
    public String getMessagesByIds(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            ChatBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSMessageResponse> wsMessageResponses = helper.fetchMessagesByIds(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsMessageResponses, "Messages by Message Ids");
            System.out.println("getMessagesByIds>>" + jsonString);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

}