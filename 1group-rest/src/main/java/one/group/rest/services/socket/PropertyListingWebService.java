package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import one.group.core.enums.APIVersion;
import one.group.core.enums.HTTPResponseType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSEntityCount;
import one.group.entities.api.response.WSPropertyListingEditResult;
import one.group.entities.api.response.WSPropertyListingsListResultDehydrated;
import one.group.entities.api.response.WSPropertyListingsListResultHydrated;
import one.group.entities.api.response.WSPropertyListingsResult;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.PropertyListingBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * PropertyListingHelper.java
 * 
 * Purpose: API's related to property listing
 * 
 * @version 1.0
 * 
 * @author sanilshet
 * 
 */
@Component
@Path("/{version}/")
public class PropertyListingWebService
{
    private static final Logger logger = LoggerFactory.getLogger(PropertyListingWebService.class);

    private Map<APIVersion, PropertyListingBaseHelper> versionHelperMap = new HashMap<APIVersion, PropertyListingBaseHelper>();

    public Map<APIVersion, PropertyListingBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, PropertyListingBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * API to get property listings of an account
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/accounts/{account_id}/property_listings")
    @Profiled(tag = "/accounts/{account_id}/property_listings")
    public String getListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
            String dehydrated = "true";
            if (queryParams.containsKey("dehydrated"))
            {
                dehydrated = queryParams.getFirst("dehydrated").toString();
            }

            if (Boolean.valueOf(dehydrated) == false)
            {
                WSPropertyListingsListResultDehydrated data = helper.getListingDehydrated(uriInfo, httpHeaders);
                jsonString = ResponseBuilder.buildOkResponse(data, "Property listing of an account");
            }
            else
            {
                WSPropertyListingsListResultHydrated data = helper.getListingHydrated(uriInfo, httpHeaders);
                jsonString = ResponseBuilder.buildOkResponse(data, "Property listing of an account");
            }
        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;
    }

    /**
     * API to get property listing details by property listing id
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return
     */
    @GET
    @Path("/property_listings/{property_listing_ids}")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /property_listings/{property_listing_ids}")
    public String getProperyListingDetails(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader, @PathParam("version") String version)
    {
        String jsonString = "";

        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSPropertyListingsResult result = helper.getProperyListingDetails(uriInfo, httpHeader);

            jsonString = ResponseBuilder.buildOkResponse(result, "Property listings result.");
        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;
    }

    /**
     * API callback to save new property listing
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return String json response
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/property_listings/")
    @Profiled(tag = "POST /property_listings/")
    public String addPropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, String json, @PathParam("version") String version)
    {
        String jsonString = "";

        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSPropertyListingEditResult result = helper.addPropertyListing(uriInfo, httpHeaders, json);
            jsonString = ResponseBuilder.buildOkResponse(result, "Property added successfully");
        }
        catch (JsonProcessingException jpe)
        {
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException e)
        {
            ResponseBuilder.shootException(e);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    /**
     * API to book mark property listing
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     * 
     * 
     */
    @PUT
    @Path("/property_listings/{property_listing_id}/bookmark")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "PUT /property_listings/{property_listing_id}/bookmark")
    public String bookMarkPropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            Map<String, Object> finalMap = helper.bookMarkPropertyListing(uriInfo, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(finalMap, "Action on property successfull.");
        }
        catch (Abstract1GroupException e)
        {
            ResponseBuilder.shootException(e);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;
    }

    /**
     * API call to get property listing book marks for a account
     * 
     * @param httpHeader
     * 
     * @return {@link String}
     */
    @Path("/property_listings/bookmarks")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /property_listings/bookmarks")
    public String getBookMarkedPropertyListings(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            Map<String, Object> finalMap = helper.getBookMarkedPropertyListings(uriInfo, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(finalMap, "bookmarks found");
        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    /**
     * API to update property listing, close property listing, mark as hot/not
     * hot, renew property listing
     * 
     * @param uriInfo
     * @param httpHeaders
     * @param formParams
     * @param amenities
     * 
     * @return {@link String}
     */
    @PUT
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/property_listings/{property_listing_id}")
    @Profiled(tag = "/property_listings/{property_listing_id}")
    public String updatePropertyListing(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, String json, @PathParam("version") String version)
    {
        String jsonString = "";

        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSPropertyListingEditResult editResult = helper.updatePropertyListing(uriInfo, httpHeaders, json);

            jsonString = ResponseBuilder.buildOkResponse(editResult, "Property updated successfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    /**
     * API call to get property listing Count
     * 
     * @param httpHeader
     * 
     * @return {@link String}
     */
    @Path("/property_listings/count")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/property_listings/count")
    public String getPropertyListingCount(@Context UriInfo uriInfo, @Context HttpHeaders httpHeaders, @PathParam("version") String version)
    {
        String jsonString = "";
        try
        {
            PropertyListingBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSEntityCount wsEntityCount = helper.getActivePropertyListingCount(uriInfo, httpHeaders);

            jsonString = ResponseBuilder.buildOkResponse(wsEntityCount, "PropertyListing Count");
        }
        catch (Abstract1GroupException ame)
        {
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

}