package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.v2.WSNotificationListResult;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.NotificationBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Path("/{version}/")
public class NotificationWebService
{

    private static final Logger logger = LoggerFactory.getLogger(NotificationWebService.class);

    private Map<APIVersion, NotificationBaseHelper> versionHelperMap = new HashMap<APIVersion, NotificationBaseHelper>();

    public Map<APIVersion, NotificationBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, NotificationBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * To fetch all notifications of account
     * 
     * @param socketEntity
     * @return
     */
    @GET
    @Path("/notifications")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET /notifications")
    public String fetchAllNotification(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            NotificationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSNotificationListResult result = helper.fechNotificationOfAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(result, "Notifications fetched successfully");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while fetching all Notifications.", ae);
            // ResponseBuilder.shootException(ae);
            throw ae;

        }
        catch (Exception e)
        {
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            logger.error("Error while fetching all Notifications.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true);
        }

        return jsonString;
    }

    /**
     * API to delete notifications
     * 
     * @param socketEntity
     * @return
     */
    @DELETE
    @Path("/notifications/{notification_ids}")
    @Profiled(tag = "DELETE /notifications/{notification_ids}")
    public String deleteNotification(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            NotificationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.deleteNotificationOfAccount(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(null, "Notification removed successfully");
        }
        catch (Abstract1GroupException ae)
        {
            // ResponseBuilder.shootException(ae);
            logger.error("Error while deleting Notifications.", ae);
            throw ae;
        }
        catch (Exception e)
        {
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            logger.error("Error while deleting Notifications.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    /**
     * API to mark notification as read
     * 
     * @param socketEntity
     * @return
     */
    @PUT
    @Path("/notifications/{notification_ids}")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "PUT /notifications/{notification_ids}")
    public String readNotification(final SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            NotificationBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.markAsReadNotification(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(null, "Notification marked as read successfully");
        }
        catch (Abstract1GroupException ae)
        {
            // ResponseBuilder.shootException(ae);
            logger.error("Error while reading Notifications.", ae);
            throw ae;
        }
        catch (Exception e)
        {
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            logger.error("Error while deleting Notifications.", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }
}
