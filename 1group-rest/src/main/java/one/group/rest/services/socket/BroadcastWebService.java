/**
 * 
 */
package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.enums.StoreKey;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSBroadcastResult;
import one.group.entities.api.response.v2.WSMessageResponseResult;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.AuthenticationException;
import one.group.exceptions.movein.ClientProcessException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.BroadcastBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author ashishthorat
 *
 */
@Component
@Path("/{version}/")
public class BroadcastWebService
{
    private static final Logger logger = LoggerFactory.getLogger(BroadcastWebService.class);
    private Map<APIVersion, BroadcastBaseHelper> versionHelperMap = new HashMap<APIVersion, BroadcastBaseHelper>();

    public Map<APIVersion, BroadcastBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, BroadcastBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * API callback to save new broadcast
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return String json response
     * @throws Abstract1GroupException
     */
    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/broadcasts")
    @Profiled(tag = "POST /broadcasts")
    public String addBroadcast(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";

        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSBroadcastResult result = helper.addBroadcast(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(result, "broadcast added successfully.");

        }
        catch (Abstract1GroupException age)
        {
            logger.error("", age);
            throw age;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to update broadcast, close broadcast, mark as hot/not hot, renew
     * broadcast
     * 
     * @param uriInfo
     * @param httpHeaders
     * @param formParams
     * @param amenities
     * 
     * @return {@link String}
     * @throws Abstract1GroupException
     */
    @PUT
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/broadcasts/{broadcast_id}")
    @Profiled(tag = "PUT /broadcasts/{broadcast_id}}")
    public String updateBroadcast(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";

        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSBroadcastResult result = helper.updateBroadcast(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(result, "broadcast updated successfully.");

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to get broadcasts of an account
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws Abstract1GroupException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/accounts/{account_ids}/broadcasts")
    @Profiled(tag = "GET accounts/{account_id}/broadcasts")
    public String getListing(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSMessageResponseResult wsMessageResponseList = helper.getBroadcastsOfAccount(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsMessageResponseList, "Broadcast list found.");

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to starred broadcast
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws Abstract1GroupException
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     * 
     * 
     */
    @Path("/broadcasts/{broadcast_id}/star")
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    @Profiled(tag = "PUT /broadcasts/{broadcast_id}/star")
    public String starredBroadcast(final SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.starBroadcast(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(null, "Broadcast has been starred successfully.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to UnStarred broadcast
     * 
     * @param uriInfo
     * @param httpHeaders
     * @return
     * @throws Abstract1GroupException
     * @throws AuthenticationException
     * @throws ClientProcessException
     * @throws AccountNotFoundException
     * 
     * 
     */
    @DELETE
    @Path("/broadcasts/{broadcast_ids}/star")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "DELETE /broadcasts/{broadcast_ids}/star")
    public String unStarredBroadcast(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";
        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.unStarredBroadcast(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Broadcast unstarred successfull.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }
    
    @POST
    @Path("/broadcasts/{broadcast_ids}/tags/")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET broadcasts/{broadcast_ids}/tags")
    public String updateBroadcastTags(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";

        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateBroadcastTag(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Broadcast tags updated", true);

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Exception while updating broadcast tags. ", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Exception while updating broadcast tags. ", e);
            e.printStackTrace();
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }

    /**
     * API to get broadcasts details by broadcasts ids
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return
     * @throws Abstract1GroupException
     */
    @GET
    @Path("/broadcasts/{broadcast_ids}")
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "GET broadcasts/{broadcast_ids}")
    public String getBroadcastsDetails(SocketEntity socketEntity) throws Abstract1GroupException
    {
        String jsonString = "";

        try
        {
            String version = socketEntity.getPathParam(PathParamType.VERSION);
            BroadcastBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSMessageResponseResult messagesList = helper.getBroadcastByBroadcastId(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(messagesList, "Broadcast result.");

        }
        catch (Abstract1GroupException ame)
        {
            logger.error("", ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }
        return jsonString;
    }
    
    
    /**
     * API to update broadcast tags
     * 
     * @param uriInfo
     * @param httpHeader
     * 
     * @return
     * @throws Abstract1GroupException
     */
}
