package one.group.rest.services.socket;

import one.group.core.enums.PathParamType;

/**
 * 
 * @author nyalfernandes
 *
 */
public class Endpoint
{
    public static final String FETCH_CHATS_WITH_IDS = "/chat_threads/" + PathParamType.CHAT_THREAD_IDS.toString();
}
