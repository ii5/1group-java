package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.request.v2.WSMessage;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.SyncBaseHelper;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author nyalfernandes
 *
 */
@Component
@Path("/{version}/")
public class SyncWebService
{
    private static final Logger logger = LoggerFactory.getLogger(SyncWebService.class);

    private Map<APIVersion, SyncBaseHelper> versionHelperMap = new HashMap<APIVersion, SyncBaseHelper>();

    public Map<APIVersion, SyncBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, SyncBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    /**
     * 
     * @param formData
     * @param httpHeaders
     * 
     * @param ack_sync_index
     *            Optional. If provided, informs the server that all sync
     *            updates at an index > the client's current sync_index and <=
     *            to the provided value have been completely processed by the
     *            client. Must be in the range [-1, MAX_INT]. If this value is
     *            provided and it is <= the client's current sync_index then it
     *            is ignored. If this value is higher than the index of the last
     *            sync update in the client's sync update list, it is equivilant
     *            to setting this value equal to the index of the last sync
     *            update in the client's sync update list. If not provided then
     *            up to max_results sync updates from the client's current sync
     *            index will be returned.
     * 
     * @param max_results
     *            Optional. If provided, specifies the maximum number of sync
     *            updates that might be returned. Must be in the range of [0,
     *            100]. If not provided defaults to 50.
     * 
     * @return
     */
    @POST
    @Path("/sync")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/sync")
    public String sync(SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            SyncBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSSyncResult result = helper.sync(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(result, "Sync Good!");

        }
        catch (Abstract1GroupException mie)
        {
            logger.error("", mie);
            throw mie;
            // ResponseBuilder.shootException(mie);
        }
        catch (Exception e)
        {
            logger.error(GeneralExceptionCode.INTERNAL_ERROR.getExceptionMessage(), e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }

        return jsonString;
    }

    @POST
    @Path("sync_handler/")
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/sync_handler")
    public String syncHandler(SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            SyncBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            System.out.println("socketEntity>>>>>" + socketEntity);
            WSRequest request = socketEntity.getRequest();
            WSMessage chatMessage = request.getMessage();
            // String fromAccountId = chatMessage.getFromAccountId();
            // String toAccountId = chatMessage.getToAccountId();
            // String text = chatMessage.getText();

        }
        catch (Abstract1GroupException mie)
        {
            logger.error("", mie);
            // ResponseBuilder.shootException(mie);
            throw mie;
        }
        catch (Exception e)
        {
            logger.error(GeneralExceptionCode.INTERNAL_ERROR.getExceptionMessage(), e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

}
