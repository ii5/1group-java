package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.enums.StoreKey;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.WSCityResult;
import one.group.entities.api.response.bpo.WSAuthItemResponse;
import one.group.entities.api.response.bpo.WSBroadcastConsolidatedResponse;
import one.group.entities.api.response.bpo.WSGroupMetaData;
import one.group.entities.api.response.bpo.WSLocationMetaData;
import one.group.entities.api.response.bpo.WSMessageResponse;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.api.response.bpo.WSUserMetaData;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.BPOBaseHelper;

/**
 * 
 * @author nyalfernandes
 *
 */
@Component
@Path("{version}")
public class BPOWebService
{
    private static final Logger logger = LoggerFactory.getLogger(BPOWebService.class);

    private Map<APIVersion, BPOBaseHelper> versionHelperMap = new HashMap<APIVersion, BPOBaseHelper>();
    
    private Lock bpoPostMessageLock = new ReentrantLock();
    private Lock bpoArchiveMessageLock = new ReentrantLock();

    public Map<APIVersion, BPOBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, BPOBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    @GET
    @Path("/bpo/meta_data/groups/count")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchGroupCountMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchGroupMetaData(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/groups/unassigned")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchUnassignedGroups(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchUnassignedGroupsBasedOnCity(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/meta_data/groups/label")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchGroupLabelMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchGroupLabelMetaData(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/meta_data/groups/detailed")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchGroupDetailedMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchGroupDetailedMetaData(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    @GET
    @Path("/bpo/group_members/{mobile_numbers}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchGroupsByMobileNumber(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchGroupsOfMember(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group details.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/meta_data/locations")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchLocationMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSLocationMetaData> responseList = helper.fetchLocationMetaData(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/locations")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchLocationsByLocationIds(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSLocation> responseList = helper.fetchLocationsByLocationIds(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/cities/locations")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchLocationsByCity(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            Map<String, Map<String, String>> responseList = helper.fetchLocationsByCity(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Group meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @Path("/bpo/cities")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Profiled(tag = "/cities")
    public String getAllCities(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSCityResult wsCityLocations = helper.getCitiesByIds(socketEntity);

            jsonString = ResponseBuilder.buildOkResponse(wsCityLocations, "City Lists found successfully", true);
        }
        catch (Abstract1GroupException ame)
        {
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            logger.error("", e);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }
        return jsonString;
    }

    @GET
    @Path("/bpo/messages")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSMessageResponse> responseList = helper.fetchMessages(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched messages.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    
    @POST
    @Path("/bpo/messages")
    @Produces(MediaType.APPLICATION_JSON)
    public String postMessages(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        bpoPostMessageLock.lock();
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true); 
            }

            helper.saveMessages(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Messages saved.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while saving messages.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while saving messages.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }
        finally
        {
        	bpoPostMessageLock.unlock();
        }

        return jsonString;
    }
    

    @GET
    @Path("/bpo/notes")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchNotes(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSGroupMetaData> responseList = helper.fetchGroupsByGroupIds(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched group data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching group meta data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/users")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchUsersByRoles(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserMetaData> responseList = helper.fetchUsersByRoles(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched group data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching group meta data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/broadcasts")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchBroadcastsWithTagsByCityAndLocation(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSBroadcastConsolidatedResponse> responseList = helper.fetchBroadcastsWithTagsByCityAndLocation(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched broadcasts data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching group meta data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/user_city")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchAdminBasedOnInput(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserCity> responseList = helper.fetchAdminAccountsBasedOnRoleCityAndId(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched account details.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @POST
    @Path("/bpo/user_city")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateUserCityAssignment(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateUserCity(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "All entries updated.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @POST
    @Path("/bpo/users")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateAdminUser(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateAdminUser(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "All entries updated.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/users/ids")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchAdminBasedOnUsernameAndStatus(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserMetaData> responseList = helper.fetchUserBasedOnInput(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched account details.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @PUT
    @Path("/bpo/groups")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateGroups(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSGroupMetaData response = helper.saveGroups(socketEntity, false);
            jsonString = ResponseBuilder.buildOkResponse(response, "Group updated.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @POST
    @Path("/bpo/groups")
    @Produces(MediaType.APPLICATION_JSON)
    public String createGroup(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSGroupMetaData response = helper.saveGroups(socketEntity, true);
            jsonString = ResponseBuilder.buildOkResponse(response, "Group created.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @POST
    @Path("/bpo/messages/{updated_by}/updatestatus")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateMessageStatus(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            boolean statusUpdated = helper.updateMessageStatus(socketEntity);
            jsonString = statusUpdated == true ? ResponseBuilder.buildOkResponse("", "Message status updated successfully.", true) : ResponseBuilder.buildOkResponse("", "Message status was not updated.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/message")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchMessage(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));
            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }
            WSMessageResponse messageResponse = helper.fetchMessage(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(messageResponse, "Fetched message.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/user_city/unassigned")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchUnassignedCityAdmin(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserCity> responseList = helper.fetchUnassignedCityAdmin(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "Fetched account details.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }

    @GET
    @Path("/bpo/group/message_counts")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchMessageCountsByGroup(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSMessageResponse wsMessageResponse = helper.fetchMessageCountsByGroup(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsMessageResponse, "Fetched message counts by group id.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    @GET
    @Path("/bpo/users/{user_ids}/auth")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchAuthByUser(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            WSAuthItemResponse wsAuthItemResponse = helper.fetchAuthorisationForUser(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(wsAuthItemResponse, "Fetched auth by user id.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    
    @PUT
    @Path("/bpo/groups/counts")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateGroupsUnreadFrom(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateGroupsUnreadFrom(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Updated group unread from count.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating group unread from count.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating group unread from count.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    
    @PUT
    @Path("/bpo/messages")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateMessageMetaData(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateMessageMetaData(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Updated message meta data.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating message meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating message meta data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
    
    @PUT
    @Path("/bpo/groups/status")
    @Produces(MediaType.APPLICATION_JSON)
    public String updateGroupStatus(SocketEntity socketEntity) throws Abstract1GroupException
    {
        socketEntity.addToStore(StoreKey.FROM_BPO, "true");
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            BPOBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.updateGroupStatus(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(null, "Updated groups status.", true);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while updating group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while updating group meta data.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, false, "Internal Server Error.");
        }

        return jsonString;
    }
}
