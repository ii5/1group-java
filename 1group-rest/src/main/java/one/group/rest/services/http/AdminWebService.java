package one.group.rest.services.http;

import static one.group.core.Constant.MOBILE_NUMBER_LENGTH;
import static one.group.core.Constant.VALID_PHONE_NUMBER_REGEX;
import static one.group.validate.operations.Operations.ADVANCE_ONLY_NOT_NULL;
import static one.group.validate.operations.Operations.LENGTH_BETWEEN;
import static one.group.validate.operations.Operations.MATCHES;
import static one.group.validate.operations.Operations.MAX_TOKEN_COUNT;
import static one.group.validate.operations.Operations.NOT_EMPTY;
import static one.group.validate.operations.Operations.NOT_NULL;
import static one.group.validate.operations.Operations.ONE_OF;
import static one.group.validate.operations.Operations.validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import one.group.core.Constant;
import one.group.core.enums.AccountFieldType;
import one.group.core.enums.HTTPResponseType;
import one.group.core.enums.OAuthGrantType;
import one.group.core.enums.status.Status;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.request.WSAccountUpdate;
import one.group.entities.api.request.WSAdminSMSRequest;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.request.v2.AdminRequest;
import one.group.entities.api.response.WSClientAnalyticsResponse;
import one.group.entities.api.response.WSReverseContacts;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSAccountListResultAdmin;
import one.group.entities.api.response.v2.WSAdminGroupMetaDataList;
import one.group.exceptions.codes.AccountExceptionCode;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.AccountNotFoundException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.RequestValidationException;
import one.group.services.AccountService;
import one.group.services.AdminUtilityService;
import one.group.services.ClientAnalyticsService;
import one.group.utils.Utils;

import org.perf4j.aop.Profiled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author shweta.sankhe
 * 
 */
@Component
@Path("{version}")
public class AdminWebService
{

    private static final Logger logger = LoggerFactory.getLogger(AdminWebService.class);

    public static final String ACCOUNT = "account";
    public static final String CLIENT = "client";
    public static final String IS_ACCOUNT_EXISTS = "isAccountExists";
    private AccountService accountService;

    private AdminUtilityService adminUtilityService;

    private ClientAnalyticsService clientAnalyticsService;

    public void setClientAnalyticsService(ClientAnalyticsService clientAnalyticsService)
    {
        this.clientAnalyticsService = clientAnalyticsService;
    }

    public void setAccountService(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public void setAdminUtilityService(AdminUtilityService adminUtilityService)
    {
        this.adminUtilityService = adminUtilityService;
    }

    @GET
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/accounts-by-phone/{phone_numbers}")
    @Profiled(tag = "/accounts-by-phone/{phone_numbers}")
    public String getAccountByPhoneNumber(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader)
    {
        String jsonString = "";

        try
        {
            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);
            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }
            String phoneNumbers = Utils.fetchValueFromPathData(uriInfo, "phone_numbers");
            validate("phone_numbers", phoneNumbers, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_PHONE_NUMBER_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));

            String[] phoneNumberArray = phoneNumbers.split(",");
            Set<String> phoneNumberSet = new HashSet<String>(Arrays.asList(phoneNumberArray));
            WSAccountListResultAdmin wsAccountListResultAdmin = accountService.getAccountListByMobileNumbersForAdmin(phoneNumberSet);

            jsonString = ResponseBuilder.buildOkResponse(wsAccountListResultAdmin, "Accounts list fetch successfully.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while checking account status with phonenumber.", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Error while checking account status with phonenumber.", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;

    }

    @POST
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/add_agent")
    @Profiled(tag = "/add_agent")
    public String addAgent(@Context HttpHeaders httpHeaders, String json) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException, Exception
    {
        String jsonString = "";
        String firstName, lastName;
        List<String> locations = null;
        List<String> localityList = new ArrayList<String>();
        AdminRequest adminRequest = null;
        String mobileNumber = null;
        WSAccount wsAccount = null;
        String deviceId = null;
        String deviceOsVersion = null;
        String devicePlatform = null;
        String cpsId = null;
        boolean isAccountExists = false;
        try
        {
            if (json != null)
            {
                adminRequest = (AdminRequest) Utils.getInstanceFromJson(json, AdminRequest.class);
            }
            mobileNumber = adminRequest.getMobileNumber();
            deviceId = adminRequest.getDeviceId();
            deviceOsVersion = adminRequest.getDeviceOsVersion();
            devicePlatform = adminRequest.getDevicePlatform();
            validate("mobile_number", mobileNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(Constant.MOBILE_NUMBER_LENGTH, Constant.MOBILE_NUMBER_LENGTH), MATCHES(VALID_PHONE_NUMBER_REGEX));
            firstName = adminRequest.getAccount().getFullName();
            lastName = adminRequest.getAccount().getCompanyName();
            String cityId = adminRequest.getAccount().getCityId();
            cpsId = adminRequest.getCpsId();

            String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);
            if (!adminWsAccount.getStatus().equals(Status.SEEDED))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            Map<String, Object> objectMap = adminUtilityService.addMobileNumberAccountClient(mobileNumber, deviceId, deviceOsVersion, devicePlatform, "4567",
                    OAuthGrantType.CLIENT_CREDENTIALS.toString(), firstName, lastName, cityId, Status.SEEDED, false, cpsId);
            wsAccount = (WSAccount) objectMap.get(ACCOUNT);
            isAccountExists = (Boolean) objectMap.get(IS_ACCOUNT_EXISTS);
            if (!isAccountExists)
            {
                jsonString = ResponseBuilder.buildOkResponse(wsAccount, "Admin added Agent " + wsAccount.getId() + " successfully!");
            }
            else
            {
                jsonString = ResponseBuilder.buildOkResponse(wsAccount, "Admin-Agent account " + wsAccount.getId() + " already exists!");
            }
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while seeding account.", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while seeding account.", ae);
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            logger.error("Error while seeding account.", e);
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonString;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/analytics/{phone_number}")
    @Profiled(tag = "/analytics/{phone_number}")
    public String getClientAnalytics(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader)
    {
        String jsonString = "";
        String mobileNumber = null;
        WSAccount account = null;
        try
        {
            String authToken = Utils.getAuthTokenFromHeader(httpHeader);

            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);
            if (!adminWsAccount.getStatus().equals(Status.SEEDED))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }
            mobileNumber = Utils.fetchValueFromPathData(uriInfo, "phone_number");
            account = accountService.getAccountDetailsForAdminByMobileNumber(mobileNumber);

            if (account == null)
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, "No Account found for PhoneNumber " + mobileNumber);
            }
            else if (adminWsAccount.equals(account))
            {
                throw new AccountNotFoundException(AccountExceptionCode.SAME_TO_AND_FROM, true, account.getId());
            }

            List<WSClientAnalyticsResponse> clientAnalyticsList = clientAnalyticsService.fetchTracesByAccountId(account.getId());
            jsonString = ResponseBuilder.buildOkResponse(clientAnalyticsList, "Analytics Details.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching analytics.", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Error while fetching analytics.", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/update_accounts/{phone_number}")
    @Profiled(tag = "/updateAccount/{phone_number}")
    public String updateAccountByPhoneNumber(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader, String json)
    {
        String jsonString = "";
        String mobileNumber = null;
        WSAccount account = null;
        String values[];
        WSAccountUpdate accountUpdate = null;
        AccountFieldType accountFieldType;
        WSRequest wsRequest = null;
        try
        {
            if (uriInfo == null || json == null)
            {
                throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, true);
            }

            wsRequest = (WSRequest) Utils.getInstanceFromJson(json, WSRequest.class);
            json = wsRequest.getAccountUpdate() == null ? null : json;

            validate("account_update", json, NOT_NULL(), NOT_EMPTY());

            accountUpdate = wsRequest.getAccountUpdate();
            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);

            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            mobileNumber = Utils.fetchValueFromPathData(uriInfo, "phone_number");
            accountFieldType = accountUpdate.getNamespace();
            values = accountUpdate.getValues();

            validate("mobile_number", mobileNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_LENGTH), MATCHES(Constant.VALID_PHONE_NUMBER_REGEX));
            validate("value", values[0], NOT_NULL(), NOT_EMPTY());

            account = accountService.getAccountDetailsForAdminByMobileNumber(mobileNumber);

            if (account == null)
            {
                throw new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true, "No Account found for PhoneNumber " + mobileNumber);
            }
            else if (adminWsAccount.equals(account))
            {
                throw new AccountNotFoundException(AccountExceptionCode.SAME_TO_AND_FROM, true, account.getId());
            }
            else if (account.getStatus().equals(Status.ACTIVE))
            {
                throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, true, "Account Active found for number" + mobileNumber);
            }

            adminUtilityService.updateAccountByPhoneNumber(account, accountFieldType, values);

            jsonString = ResponseBuilder.buildOkResponse(null, "Account Updated Succesfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error updating admin account.", jpe);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true));
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error Updating Account", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Error Updating Account", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/property_listings/{property_listing_ids}")
    @Profiled(tag = "/property_listings/{property_listing_ids}")
    public String updatePropertyListings(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader)
    {
        String jsonString = "";
        String propertyListingIds = null;
        try
        {
            if (uriInfo == null)
            {
                throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, false);
            }

            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);

            if (!adminWsAccount.getStatus().equals(Status.SEEDED))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            propertyListingIds = Utils.fetchValueFromPathData(uriInfo, "property_listing_ids");
            validate("property_listing_ids", propertyListingIds, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_IDS_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));

            adminUtilityService.updatePropertyListings(propertyListingIds);

            jsonString = ResponseBuilder.buildOkResponse(null, "PropertyListing Updated Succesfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Admin: Error updating PropertyListing", jpe);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.BAD_REQUEST, true));
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Admin:Error updating PropertyListing", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Admin:Error updating PropertyListing", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/reverse-contacts-by-phone/{phone_number}")
    @Profiled(tag = "/reverse-contacts-by-phone/{phone_number}")
    public String getReverseContactsByPhoneNumber(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader)
    {
        String jsonString = "";
        String phoneNumber = null;
        try
        {

            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);

            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            phoneNumber = Utils.fetchValueFromPathData(uriInfo, "phone_number");

            validate("phone_number", phoneNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_LENGTH), MATCHES(Constant.VALID_PHONE_NUMBER_REGEX));

            WSReverseContacts reverseContacts = adminUtilityService.getAccountDetailsFromNativeContactsByPhoneNumber(phoneNumber);

            jsonString = ResponseBuilder.buildOkResponse(reverseContacts, "Account list found.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Admin:Error while fetching Account Details", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Admin:Error while fetching Account Details", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }
        return jsonString;
    }

    @POST
    @Consumes({ MediaType.TEXT_PLAIN + ";charset=UTF-8", MediaType.APPLICATION_JSON + ";charset=UTF-8" })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/sms")
    @Profiled(tag = "/sms")
    public String sendSMSToPhoneNumbers(@Context HttpHeaders httpHeaders, String jsonRequest) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException, Exception
    {
        String jsonResponse = "";
        AdminRequest adminRequest = null;
        try
        {
            if (jsonRequest != null)
            {
                adminRequest = (AdminRequest) Utils.getInstanceFromJson(jsonRequest, AdminRequest.class);
                jsonRequest = adminRequest.getSmsRequest() == null ? null : jsonRequest;
            }
            validate("sms_request", jsonRequest, NOT_NULL(), NOT_EMPTY());

            WSAdminSMSRequest wsAdminSMSRequest = adminRequest.getSmsRequest();
            // Validate Phone number is in valid format
            String phoneNumber = wsAdminSMSRequest.getPhoneNumber();
            validate("phone_number", phoneNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(Constant.MOBILE_NUMBER_LENGTH, Constant.MOBILE_NUMBER_LENGTH), MATCHES(VALID_PHONE_NUMBER_REGEX));

            // Process text content
            // 1) Check if its not null as well as not empty
            // 2) Replace http sensitive chars & + % # = ^ ~ with their http
            // equivalent encodings
            // 3) check content length is still less than or equal to 160 chars
            // max
            String textContent = wsAdminSMSRequest.getContent();
            validate("content", textContent, NOT_NULL(), NOT_EMPTY());
            textContent = textContent.replace("&", "%26").replace("%", "%25").replace("#", "%23").replace("=", "%3D").replace("~", "%7E").replace("+", "zz").replace("^", "%5E");
            validate("content", textContent, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(0, Constant.MAX_SMS_CONTENT_LENGTH));

            String authToken = Utils.getAuthTokenFromHeader(httpHeaders);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);
            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            adminUtilityService.sendSMSToPhoneNumber(wsAdminSMSRequest);
            jsonResponse = ResponseBuilder.buildOkResponse("", "SMS Send Successfully");
        }
        catch (JsonProcessingException jpe)
        {
            logger.error("Error while Sending SMS", jpe);
            ResponseBuilder.shootFatalException(jpe, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Error while Sending SMS", ae);
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            logger.error("Error while Sending SMS", e);
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        return jsonResponse;
    }

    @PUT
    @Consumes({ MediaType.TEXT_PLAIN + ";charset=UTF-8", MediaType.APPLICATION_JSON + ";charset=UTF-8" })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/update/city_of_groups/{phone_number}/{city_id}")
    @Profiled(tag = "/city_of_groups")
    public String updateCityIdOfGroups(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader) throws JsonMappingException, JsonGenerationException, IOException, Abstract1GroupException, Exception
    {
        String jsonResponse = "";
        boolean groupsUpdated = false;
        try
        {
            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);

            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }

            if (uriInfo == null)
            {
                throw new RequestValidationException(GeneralExceptionCode.BAD_REQUEST, false);
            }

            String phoneNumber = Utils.fetchValueFromPathData(uriInfo, "phone_number");
            validate("phone_number", phoneNumber, NOT_NULL(), NOT_EMPTY(), LENGTH_BETWEEN(MOBILE_NUMBER_LENGTH, MOBILE_NUMBER_LENGTH), MATCHES(Constant.VALID_PHONE_NUMBER_REGEX));
            String cityId = Utils.fetchValueFromPathData(uriInfo, "city_id");
            validate("city_id", cityId, NOT_NULL(), NOT_EMPTY());

            groupsUpdated = adminUtilityService.updateCityIdOfGroups(cityId, phoneNumber);
        }
        catch (RequestValidationException rve)
        {
            logger.error("Exception updating city id of provided mobile number", rve);
            ResponseBuilder.shootFatalException(rve, GeneralExceptionCode.BAD_REQUEST, HTTPResponseType.BAD_REQUEST, "Error parsing request.");
        }
        catch (Abstract1GroupException ae)
        {
            logger.error("Exception updating city id of provided mobile number", ae);
            ResponseBuilder.shootException(ae);
        }
        catch (Exception e)
        {
            logger.error("Exception updating city id of provided mobile number", e);
            ResponseBuilder.shootFatalException(e, GeneralExceptionCode.INTERNAL_ERROR, HTTPResponseType.INTERNAL_SERVER_ERROR, null);
        }
        if (groupsUpdated)
            jsonResponse = "City id for respective phone number updated sucessfully!";
        else
            jsonResponse = "No groups updated for the provided phone number";

        return jsonResponse;

    }

    @GET
    @Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/admin/groups_meta_data/{phone_numbers}")
    @Profiled(tag = "/groups_meta_data/{phone_numbers}")
    public String getGroupsMetaDataByPhoneNumber(@Context UriInfo uriInfo, @Context HttpHeaders httpHeader) throws Abstract1GroupException
    {
        String jsonString = "";

        try
        {
            String authToken = Utils.getAuthTokenFromHeader(httpHeader);
            WSAccount adminWsAccount = accountService.fetchAccountFromAuthToken(authToken);
            if (!adminWsAccount.getStatus().equals(Status.ADMIN))
            {
                throw new RequestValidationException(AccountExceptionCode.ADMIN_ACCOUNT_NOT_SEEDED, true, adminWsAccount.getId());
            }
            String phoneNumbers = Utils.fetchValueFromPathData(uriInfo, "phone_numbers");
            validate("phone_numbers", phoneNumbers, NOT_NULL(), NOT_EMPTY(), MATCHES(Constant.PATH_PARAM_PHONE_NUMBER_REGEX), MAX_TOKEN_COUNT(",", Constant.MAX_PATH_PARAM_IDS));

            String everSync = Utils.fetchValueFromQueryParam(uriInfo, "ever_sync");
            validate("ever_sync", everSync, ADVANCE_ONLY_NOT_NULL(), ONE_OF("true", "false"));

            String[] phoneNumberArray = phoneNumbers.split(",");
            Set<String> phoneNumberSet = new HashSet<String>(Arrays.asList(phoneNumberArray));
            WSAdminGroupMetaDataList wsMetaDataResultAdmin = adminUtilityService.getGroupMetaDataByPhoneNumber(phoneNumberSet, everSync);

            jsonString = ResponseBuilder.buildOkResponse(wsMetaDataResultAdmin, "GroupMetaData list fetch successfully.");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while getting group meta data with phonenumber.", ame);
            ResponseBuilder.shootException(ame);
        }
        catch (Exception e)
        {
            logger.error("Error while getting group meta data with phonenumber.", e);
            ResponseBuilder.shootException(new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true));
        }

        return jsonString;
    }

}
