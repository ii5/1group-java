package one.group.rest.services.socket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import one.group.core.enums.APIVersion;
import one.group.core.enums.PathParamType;
import one.group.core.respone.ResponseBuilder;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.socket.SocketEntity;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.exceptions.movein.General1GroupException;
import one.group.exceptions.movein.UnsupportedAPIVersionException;
import one.group.rest.helpers.v2.GroupsBaseHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Path("{version}")
public class GroupsWebService
{
    private static final Logger logger = LoggerFactory.getLogger(GroupsWebService.class);

    private Map<APIVersion, GroupsBaseHelper> versionHelperMap = new HashMap<APIVersion, GroupsBaseHelper>();

    public Map<APIVersion, GroupsBaseHelper> getVersionHelperMap()
    {
        return versionHelperMap;
    }

    public void setVersionHelperMap(Map<APIVersion, GroupsBaseHelper> versionHelperMap)
    {
        this.versionHelperMap = versionHelperMap;
    }

    @POST
    @Path("/group/save")
    @Produces(MediaType.APPLICATION_JSON)
    public String saveGroup(SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            GroupsBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            helper.saveGroup(socketEntity);
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

    @GET
    @Path("/user/fetch")
    @Produces(MediaType.APPLICATION_JSON)
    public String fetchUser(SocketEntity socketEntity) throws Abstract1GroupException, General1GroupException
    {
        String jsonString = "";
        String version = socketEntity.getPathParam(PathParamType.VERSION);
        try
        {
            GroupsBaseHelper helper = versionHelperMap.get(APIVersion.parse(version));

            if (helper == null)
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.UNRECOGNIZED, true);
            }
            else if (!APIVersion.isVersionSupported(version))
            {
                throw new UnsupportedAPIVersionException(GeneralExceptionCode.MOVED_PERMANENTLY, true);
            }

            List<WSUserCity> responseList = null;// helper.fetchUser(socketEntity);
            jsonString = ResponseBuilder.buildOkResponse(responseList, "User info");
        }
        catch (Abstract1GroupException ame)
        {
            logger.error("Error while fetching group meta data.", ame);
            // ResponseBuilder.shootException(ame);
            throw ame;
        }
        catch (Exception e)
        {
            logger.error("Error while fetching messages of desired chat thread.", e);
            // ResponseBuilder.shootFatalException(e,
            // GeneralExceptionCode.INTERNAL_ERROR,
            // HTTPResponseType.INTERNAL_SERVER_ERROR, null);
            throw new General1GroupException(GeneralExceptionCode.INTERNAL_ERROR, true, e);
        }

        return jsonString;
    }

}
