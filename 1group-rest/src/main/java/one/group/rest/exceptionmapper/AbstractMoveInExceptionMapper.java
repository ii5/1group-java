package one.group.rest.exceptionmapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import one.group.core.respone.ResponseBuilder;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Provider
@Component
public class AbstractMoveInExceptionMapper implements ExceptionMapper<Abstract1GroupException> {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractMoveInExceptionMapper.class);
	
	@Override
	public Response toResponse(Abstract1GroupException exception) {
		Validation.notNull(exception, "Exception passed should not be null.");
		logger.error("error" , exception);
		String json = ResponseBuilder.buildResponseString(exception.isFatal(), exception.getMessage(),
				exception.getCode(), null, null, false);
		return Response.status(exception.getHttpCode()).entity(json).type(MediaType.APPLICATION_JSON).build();
	}
}