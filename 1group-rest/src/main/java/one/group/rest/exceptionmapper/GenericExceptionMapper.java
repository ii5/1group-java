package one.group.rest.exceptionmapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import one.group.core.respone.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Provider
@Component
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	private static final Logger logger = LoggerFactory.getLogger(GenericExceptionMapper.class);
	
	public Response toResponse(Throwable ex) {
		logger.error("error" ,ex);;
		String json = ResponseBuilder.buildResponseString(true, "Something went really wrong! :(", "500", null, null, false);
		return Response.serverError().entity(json).type(MediaType.APPLICATION_JSON).build();
	}

}