package one.group.rest.exceptionmapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import one.group.core.respone.ResponseBuilder;
import one.group.exceptions.codes.GeneralExceptionCode;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;

@Provider
@Component
public class JsonGenerationExceptionMapper implements ExceptionMapper<JsonGenerationException> {
	
	private static final Logger logger = LoggerFactory.getLogger(JsonGenerationExceptionMapper.class);
	
	@Override
	public Response toResponse(JsonGenerationException exception) {
		Validation.notNull(exception, "Exception passed should not be null.");
		logger.error("error" , exception);
		GeneralExceptionCode exceptionCode = GeneralExceptionCode.BAD_REQUEST ;
		String json = ResponseBuilder.buildResponseString(true, exception.getMessage(), exceptionCode.getExceptionName(), null, null, false);
		return Response.status(exceptionCode.getHttpResponseType().getCode()).entity(json).type(MediaType.APPLICATION_JSON).build();
	}
}