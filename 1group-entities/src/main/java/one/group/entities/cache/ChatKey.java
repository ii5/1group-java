package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

public enum ChatKey
{

    CHAT_THREADS_BY_ACTIVITY_OF_ACCOUNT("accounts:[account_id]:chat_threads:by_activity"), CHAT_THREADS_BY_JOINED_OF_ACCOUNT("accounts:[account_id]:chat_threads:by_joined"), CHAT_MESSAGES_OF_CHAT_THREAD(
            "chat_threads:[chat_thread_id]:content"), CHAT_THREAD_ID("chat-thread-[account_id_one]-[account_id_two]"), CHAT_THREAD_PARTICIPANTS("chat_threads:[chat_thread_id]:participants"), CHAT_MESSAGES_OF_CHAT_THREAD_SORTEDSET(
            "chat_threads:[chat_thread_id]:score"), CHAT_THREAD_CURSORS("chat_threads:[chat_thread_id]:cursors:[account_id]"), CHAT_THREADS_OF_PROPERTY_LISTING(
            "property_listing:[property_listing_id]:chat_threads"), SET_ACCOUNT_BLOCKED_BY("chat:[account_id]:blocked_by"), GROUP_ID("chat-thread-[group_source]-[account_id_one]-[account_id_two]");
    private String key;

    ChatKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
