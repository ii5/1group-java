package one.group.entities.cache;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface Cacheable
{

    public String getKey();
}
