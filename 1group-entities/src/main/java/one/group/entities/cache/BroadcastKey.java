/**
 * 
 */
package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

/**
 * @author ashishthorat
 *
 */
public enum BroadcastKey
{
    BROADCAST_KEY("broadcast:[broadcast_id]"), SEARCHED_BROADCAST_OF_ACCOUNT("account:broadcast:[account_id]");
    private String key;

    BroadcastKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
