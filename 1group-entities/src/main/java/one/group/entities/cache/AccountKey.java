package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

/**
 * Account keys
 * 
 * @author nyalfernandes
 * 
 */
public enum AccountKey
{

    SET_ACCOUNT_ALLIANCE("accounts:[account_id]:alliance"),
    HASH_ACCOUNT_ONLINE("accounts:online"),
    ACCOUNT_SMS_NOTIFICATION("accounts:[account_id]:sms_notification"),
    ACCOUNT_INVITE_CODE("accounts:[account_id]:invite_code");

    private String key;

    AccountKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
