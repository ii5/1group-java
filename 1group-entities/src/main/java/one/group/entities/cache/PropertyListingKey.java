package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public enum PropertyListingKey
{

    PROPERTY_LISTING_KEY("property_listing:[property_listing_id]"),
    ;
    
    private String key;

    PropertyListingKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
