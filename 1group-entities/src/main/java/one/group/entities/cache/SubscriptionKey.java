package one.group.entities.cache;

import java.util.HashMap;
import java.util.Map;

import one.group.core.enums.EntityType;
import one.group.utils.Utils;

public enum SubscriptionKey
{
    SUBSCRIPTION_KEY("subscription:[sc_type]:[sc_entity_key]:[account_id]"), ;

    private String key;

    SubscriptionKey(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return this.key;
    }

    public String getFormedKey(Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public Map<String, Object> getParsedKey(String formedKey)
    {
        boolean correctFormat = formedKey.matches(key.replace("[sc_type]", ".*").replace("[sc_entity_key]", ".*").replace("[account_id]", ".*"));
        if (!correctFormat)
        {
            throw new IllegalArgumentException("Wrong key format being passed.");
        }

        Map<String, Object> parsedKey = new HashMap<String, Object>();
        String[] token = formedKey.split(":");
        if (Utils.isNullOrEmpty(token[1]) || Utils.isNullOrEmpty(token[2]))
        {
            throw new IllegalArgumentException("Incorrect formed key being passed.");
        }
        parsedKey.put("sc_type", EntityType.parse(token[1]));
        parsedKey.put("sc_entity_key", token[2]);
        parsedKey.put("account_id", token[3]);

        return parsedKey;
    }
}
