package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

public enum GroupCountKey
{
    GROUP_COUNT("whatsapp:information");

    private String key;

    GroupCountKey(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return this.key;
    }

    @Override
    public String toString()
    {

        return key;
    }

    public String getFormedKey(Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }
}
