package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

public enum ClientKey
{
    CLIENTS_BY_ACCOUNT("accounts:[account_id]:clients");

    private String key;

    ClientKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
