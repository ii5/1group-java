package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

public enum PaginationKey
{
    PAGINATION_ID("pagination:[account_id]:[pagination_key]");

    private String key;

    private PaginationKey(final String key)
    {
        this.key = key;
    }

    public String getFormedKey(final Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }

    public String getKey()
    {
        return this.key;
    }
}
