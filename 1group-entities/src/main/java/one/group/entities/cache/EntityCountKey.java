package one.group.entities.cache;

import java.util.Map;

import one.group.utils.Utils;

public enum EntityCountKey
{
    ENTITY_COUNT_KEY("entity:[entity_type]:count"), ;

    private String key;

    EntityCountKey(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return this.key;
    }

    public String getFormedKey(Map<String, String> values)
    {
        return Utils.populateKey(getKey(), values);
    }
}
