package one.group.entities.sync;

/**
 * Keys for data that should be part of the sync log entry data.
 * 
 * @author nyalfernandes
 *
 */
public enum SyncEntryKey
{
    ASSOCIATED_ENTITY_ID, ASSOCIATED_ENTITY_TYPE, TARGET_ACCOUNT_ID, CLOSED_PROPERTY_LISTING_ID, SYNC_UPDATE, SYNC_UPDATE_DATA, CONTACTS, HEADERS, CLOSED_BROADCAST_ID, REQUEST_HASH;

}
