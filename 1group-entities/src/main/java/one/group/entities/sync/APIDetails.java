package one.group.entities.sync;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * API detail structure that is stored in kafka.
 * 
 * @author nyalfernandes
 * 
 */
public class APIDetails
{
    private static final Logger logger = LoggerFactory.getLogger(APIDetails.class);

    private HTTPRequestMethodType type = HTTPRequestMethodType.GET;

    private String url = "";

    private Date requestTime = new Date();
    
    private Date responseTime;

    private WSRequest request;

    private WSResponse response;

    private Map<String, String> additionalRequestData = new HashMap<String, String>();
    
    public APIDetails()
    {
        type = HTTPRequestMethodType.GET;
        url = "";
        requestTime = new Date();
        request = new WSRequest();
        response = new WSResponse();
    }

    public APIDetails(HTTPRequestMethodType type, String name, Date requestTime)
    {
        this.type = type;
        this.url = name;
        this.requestTime = requestTime;
    }

    public APIDetails(WSRequest request, HTTPRequestMethodType type, String name, Date requestTime)
    {
        this.request = request;
        this.type = type;
        this.url = name;
        this.requestTime = requestTime;
    }

    public APIDetails(HTTPRequestMethodType type, String name, Date requestTime, WSResult result, Object data)
    {
        this(type, name, requestTime);
        response = new WSResponse();
        response.setData(data);
        response.setResult(result);
    }

    public Date getResponseTime()
    {
        return responseTime;
    }
    
    public void setResponseTime(Date responseTime)
    {
        this.responseTime = responseTime;
    }
    
    public Map<String, String> getAdditionalRequestData()
    {
        return additionalRequestData;
    }
    
    public void setAdditionalRequestData(Map<String, String> additionalRequestData)
    {
        this.additionalRequestData = additionalRequestData;
    }
    
    public WSRequest getRequest()
    {
        return request;
    }

    public void setRequest(WSRequest request)
    {
        this.request = request;
    }

    public void setRequestFrom(String jsonRequest)
    {
        if (jsonRequest != null)
        {
            try
            {
                this.request = (WSRequest) Utils.getInstanceFromJson(jsonRequest, WSRequest.class);
            }
            catch (Exception e)
            {
                logger.warn("Exception while saving request in APIDetails.", e);
            }
        }
    }

    public HTTPRequestMethodType getType()
    {
        return type;
    }

    public void setType(HTTPRequestMethodType type)
    {
        this.type = type;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String name)
    {
        this.url = name;
    }

    public Date getRequestTime()
    {
        return requestTime;
    }

    public void setRequestTime(Date requestTime)
    {
        this.requestTime = requestTime;
    }

    public WSResponse getResponse()
    {
        return response;
    }

    public void setResponse(WSResponse response)
    {
        this.response = response;
    }

    public void setResponseFrom(String jsonResponse)
    {
        if (jsonResponse != null)
        {
            try
            {
                this.response = (WSResponse) Utils.getInstanceFromJson(jsonResponse, WSResponse.class);
            }
            catch (Exception e)
            {
                logger.warn("Exception while saving response in APIDetails.", e);
            }
        }
    }
}
