package one.group.entities.sync;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.request.WSAccount;
import one.group.entities.api.request.WSClient;
import one.group.entities.api.response.WSEntityReference;
import one.group.utils.Utils;

/**
 * Model of data that is passed processed in the sync system.
 * 
 * @author nyalfernandes
 * 
 */
public class SyncLogEntry implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private long version = serialVersionUID;

    private WSAccount originatingAccount;

    private WSClient originatingClient;

    private SyncDataType type = SyncDataType.INVALIDATION;

    private SyncActionType action;

    private EntityType associatedEntityType;

    private String associatedEntityId;

    private APIDetails apiDetails = new APIDetails();

    private Map<SyncEntryKey, String> additionalData = new HashMap<SyncEntryKey, String>();

    public SyncLogEntry()
    {
        this.apiDetails = new APIDetails();
        this.version = serialVersionUID;
    }

    public SyncLogEntry(SyncDataType type, SyncActionType action, EntityType associatedEntityType, String associatedEntityId)
    {
        this();
        this.type = type;
        this.action = action;
        this.associatedEntityType = associatedEntityType;
        this.associatedEntityId = associatedEntityId;
    }
    
    public long getVersion()
    {
        return version;
    }
    
    public void setVersion(long version)
    {
        this.version = version;
    }

    public WSAccount getOriginatingAccount()
    {
        return originatingAccount;
    }

    public void setOriginatingAccount(WSAccount originatingAccount)
    {
        this.originatingAccount = originatingAccount;
    }

    public WSClient getOriginatingClient()
    {
        return originatingClient;
    }

    public void setOriginatingClient(WSClient originatingClient)
    {
        this.originatingClient = originatingClient;
    }

    public SyncDataType getType()
    {
        return type;
    }

    public void setType(SyncDataType type)
    {
        this.type = type;
    }

    public SyncActionType getAction()
    {
        return action;
    }

    public void setAction(SyncActionType action)
    {
        this.action = action;
    }

    public EntityType getAssociatedEntityType()
    {
        return associatedEntityType;
    }

    public void setAssociatedEntityType(EntityType associatedEntityType)
    {
        this.associatedEntityType = associatedEntityType;
    }

    public String getAssociatedEntityId()
    {
        return associatedEntityId;
    }

    public void setAssociatedEntityId(String associatedEntityId)
    {
        this.associatedEntityId = associatedEntityId;
    }

    public APIDetails getApiDetails()
    {
        return apiDetails;
    }

    public void setApiDetails(APIDetails apiDetails)
    {
        this.apiDetails = apiDetails;
    }

    public Map<SyncEntryKey, String> getAdditionalData()
    {
        return additionalData;
    }

    public String getAdditionalData(SyncEntryKey key)
    {
        return this.additionalData.get(key);
    }

    public <T> T getAdditionalDataAs(SyncEntryKey key, Class<T> type)
    {
        if (this.getAdditionalData(key) != null)
        {
            try
            {
                return type.cast(Utils.getInstanceFromJson(this.getAdditionalData(key), type));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void setAdditionalData(Map<SyncEntryKey, String> additionalData)
    {
        this.additionalData = additionalData;
    }

    public void setAdditionalData(SyncEntryKey key, String additionalData)
    {
        this.additionalData.put(key, additionalData);
    }

    public void setAdditionalData(SyncEntryKey key, Object o)
    {
        this.additionalData.put(key, Utils.getJsonString(o));
    }

    @Override
    public String toString()
    {
        return "SyncLogEntry [originatingAccount=" + originatingAccount + ", originatingClient=" + originatingClient + ", type=" + type + ", action=" + action + ", associatedEntityType="
                + associatedEntityType + ", associatedEntityId=" + associatedEntityId + ", apiDetails=" + apiDetails + ", additionalData=" + additionalData + "]";
    }

    public static void main(String[] args) throws Exception
    {

        SyncLogEntry entry = new SyncLogEntry();
        entry.setAction(SyncActionType.EDIT_CLOSE);
        entry.setType(SyncDataType.INVALIDATION);
        entry.setAssociatedEntityType(EntityType.PROPERTY_LISTING);
        entry.setAssociatedEntityId("P-ID1234");
        entry.setAdditionalData(SyncEntryKey.SYNC_UPDATE_DATA, Utils.getJsonString(new WSEntityReference(HintType.FETCH, EntityType.PROPERTY_LISTING, "P-ID1234")));
        entry.setOriginatingAccount(new WSAccount());
        entry.setOriginatingClient(new WSClient());
        entry.setApiDetails(new APIDetails());
        System.out.println(Utils.getJsonString(entry));
    }
}
