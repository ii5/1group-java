package one.group.entities.api.response.v2;

import one.group.core.enums.EntityType;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSSubscribeEntity implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int expiresIn;

    private String recordId;

    private String accountId;

    private EntityType type;

    public WSSubscribeEntity(int expiresIn, String recordId, String accountId)
    {
        Validation.notNull(recordId, "Record Id cannot be null.");
        Validation.notNull(accountId, "Account Id cannot be null.");
        Validation.isFalse(expiresIn > 1L, "Set a decent expiry time.");

        this.expiresIn = expiresIn;
        this.recordId = recordId;
        this.accountId = accountId;
    }

    public WSSubscribeEntity()
    {
    }

    public int getExpiresIn()
    {
        return expiresIn;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public EntityType getType()
    {
        return type;
    }

    public void setType(EntityType type)
    {
        this.type = type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        result = prime * result + expiresIn;
        result = prime * result + ((recordId == null) ? 0 : recordId.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSSubscribeEntity other = (WSSubscribeEntity) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (expiresIn != other.expiresIn)
            return false;
        if (recordId == null)
        {
            if (other.recordId != null)
                return false;
        }
        else if (!recordId.equals(other.recordId))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSSubscribeEntity [expiresIn=" + expiresIn + ", recordId=" + recordId + ", accountId=" + accountId + ", type=" + type + "]";
    }

    public static WSSubscribeEntity dummyData()
    {
        WSSubscribeEntity entity = new WSSubscribeEntity(Integer.MAX_VALUE, Utils.randomUUID(), Utils.randomUUID());
        return entity;
        
    }
    
}
