package one.group.entities.api.request;

import java.util.List;

import one.group.entities.api.response.WSChatThreadCursor;

/**
 * 
 * @author sanil
 *
 */
public class WSRequest
{
    private WSAddPropertyListing propertyListing;

    private WSAccountIdsQuery fetch;

    private WSSyncRequest sync;

    private WSRequestOtp requestOtp;

    private WSSignIn signIn;

    private WSAccountRelations accountRelations;

    private WSSendChatMessage chatMessage;

    private WSChatThreadCursor chatThreadCursor;

    private WSContacts contacts;

    private WSAccount account;

    private WSRemoveMedia removeMedia;

    private List<WSClientAnalytics> clientAnalytics;

    private WSAccountUpdate accountUpdate;

    private WSClientData clientData;

    private List<String> invitees;

    private WSLocationMasterRequest locationMaster;

    private WSBroadcast broadcast;

    public WSBroadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSBroadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public WSLocationMasterRequest getLocationMaster()
    {
        return locationMaster;
    }

    public void setLocationMaster(WSLocationMasterRequest locationMaster)
    {
        this.locationMaster = locationMaster;
    }

    public List<String> getInvitees()
    {
        return invitees;
    }

    public void setInvitees(List<String> invitees)
    {
        this.invitees = invitees;
    }

    public WSAccountUpdate getAccountUpdate()
    {
        return accountUpdate;
    }

    public void setAccountUpdate(WSAccountUpdate accountUpdate)
    {
        this.accountUpdate = accountUpdate;
    }

    public WSSignIn getSignIn()
    {
        return signIn;
    }

    public List<WSClientAnalytics> getClientAnalytics()
    {
        return clientAnalytics;
    }

    public void setClientanalytics(List<WSClientAnalytics> clientAnalytics)
    {
        this.clientAnalytics = clientAnalytics;
    }

    public void setSignIn(WSSignIn signIn)
    {
        this.signIn = signIn;
    }

    public WSRequestOtp getRequestOtp()
    {
        return requestOtp;
    }

    public void setRequestOtp(WSRequestOtp requestOtp)
    {
        this.requestOtp = requestOtp;
    }

    public WSSyncRequest getSync()
    {
        return sync;
    }

    public void setSync(WSSyncRequest sync)
    {
        this.sync = sync;
    }

    public WSAccountIdsQuery getFetch()
    {
        return fetch;
    }

    public void setFetch(WSAccountIdsQuery fetch)
    {
        this.fetch = fetch;
    }

    public WSAccount getAccount()
    {
        return account;
    }

    public void setAccount(WSAccount account)
    {
        this.account = account;
    }

    public WSAddPropertyListing getPropertyListing()
    {
        return propertyListing;
    }

    public void setPropertyListing(WSAddPropertyListing propertyListing)
    {
        this.propertyListing = propertyListing;
    }

    public WSAccountRelations getAccountRelations()
    {
        return accountRelations;
    }

    public void setAccountRelations(WSAccountRelations accountRelations)
    {
        this.accountRelations = accountRelations;
    }

    public WSSendChatMessage getChatMessage()
    {
        return chatMessage;
    }

    public void setChatMessage(WSSendChatMessage chatMessage)
    {
        this.chatMessage = chatMessage;
    }

    public WSChatThreadCursor getChatThreadCursor()
    {
        return chatThreadCursor;
    }

    public void setChatThreadCursor(WSChatThreadCursor chatThreadCursor)
    {
        this.chatThreadCursor = chatThreadCursor;
    }

    // public AdminRequest getAdminRequest() {
    // return adminRequest;
    // }
    //
    // public void setAdminRequest(AdminRequest adminRequest) {
    // this.adminRequest = adminRequest;
    // }

    public WSRemoveMedia getRemoveMedia()
    {
        return removeMedia;
    }

    public void setRemoveMedia(WSRemoveMedia removeMedia)
    {
        this.removeMedia = removeMedia;
    }

    public WSContacts getContacts()
    {
        return contacts;
    }

    public void setContacts(WSContacts contacts)
    {
        this.contacts = contacts;
    }

    public WSClientData getClientData()
    {
        return clientData;
    }

    public void setClientData(WSClientData clientData)
    {
        this.clientData = clientData;
    }

    @Override
    public String toString()
    {
        return "WSRequest [propertyListing=" + propertyListing + ", fetch=" + fetch + ", sync=" + sync + ", requestOtp=" + requestOtp + ", signIn=" + signIn + ", accountRelations=" + accountRelations
                + ", chatMessage=" + chatMessage + ", chatThreadCursor=" + chatThreadCursor + ", contacts=" + contacts + ", account=" + account + ", removeMedia=" + removeMedia + ", clientAnalytics="
                + clientAnalytics + ", accountUpdate=" + accountUpdate + ", clientData=" + clientData + ", broadcast=" + broadcast + "]";
    }
}
