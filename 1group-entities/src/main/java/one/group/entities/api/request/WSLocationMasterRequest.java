package one.group.entities.api.request;

import java.util.List;

public class WSLocationMasterRequest implements RequestEntity
{
	private static final long serialVersionUID = 1L;
	private List<WSLocationMasterCity> cities;

	public List<WSLocationMasterCity> getCities() 
	{
		return cities;
	}

	public void setCities(List<WSLocationMasterCity> cities) 
	{
		this.cities = cities;
	}

	
	
}
