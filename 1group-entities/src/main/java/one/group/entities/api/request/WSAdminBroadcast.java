/**
 * 
 */
package one.group.entities.api.request;

/**
 * @author ashishthorat
 *
 */
public class WSAdminBroadcast implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String createdFor;

    private String externalListingTime;

    private String externalSource;

    public String getCreatedFor()
    {
        return createdFor;
    }

    public void setCreatedFor(String createdFor)
    {
        this.createdFor = createdFor;
    }

    public String getExternalListingTime()
    {
        return externalListingTime;
    }

    public void setExternalListingTime(String externalListingTime)
    {
        this.externalListingTime = externalListingTime;
    }

    public String getExternalSource()
    {
        return externalSource;
    }

    public void setExternalSource(String externalSource)
    {
        this.externalSource = externalSource;
    }

    @Override
    public String toString()
    {
        return "WSAdminBroadcast [createdFor=" + createdFor + ", externalListingTime=" + externalListingTime + ", externalSource=" + externalSource + "]";

    }

}
