package one.group.entities.api.request.bpo;


/**
 * 
 * @author nyalfernandes
 *
 */

public class WSGroupMembersMetadataRequest
{
    private String groupId;
    
    private String participantId;

    private Integer readIndex;

    private Integer receivedIndex;

    private String mobileNumber;

    private String receivedMessageId;

    private String readMessageId;

    private Boolean activeMember;

    private Boolean isAdmin;

    private Boolean everSync;

    private Boolean currentSync;

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public Integer getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(Integer readIndex)
    {
        this.readIndex = readIndex;
    }

    public Integer getReceivedIndex()
    {
        return receivedIndex;
    }

    public void setReceivedIndex(Integer receivedIndex)
    {
        this.receivedIndex = receivedIndex;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getReceivedMessageId()
    {
        return receivedMessageId;
    }

    public void setReceivedMessageId(String receivedMessageId)
    {
        this.receivedMessageId = receivedMessageId;
    }

    public String getReadMessageId()
    {
        return readMessageId;
    }

    public void setReadMessageId(String readMessageId)
    {
        this.readMessageId = readMessageId;
    }

    public Boolean getActiveMember()
    {
        return activeMember;
    }

    public void setActiveMember(Boolean activeMember)
    {
        this.activeMember = activeMember;
    }

    public Boolean getIsAdmin()
    {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public Boolean getEverSync()
    {
        return everSync;
    }

    public void setEverSync(Boolean everSync)
    {
        this.everSync = everSync;
    }

    public Boolean getCurrentSync()
    {
        return currentSync;
    }

    public void setCurrentSync(Boolean currentSync)
    {
        this.currentSync = currentSync;
    }
   
}
