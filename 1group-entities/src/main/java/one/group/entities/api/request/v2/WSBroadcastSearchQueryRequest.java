package one.group.entities.api.request.v2;

import java.util.ArrayList;
import java.util.Arrays;

import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.response.v2.WSBroadcastSearchQuery;
import one.group.entities.jpa.Search;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 * 
 */
public class WSBroadcastSearchQueryRequest extends WSBroadcastSearchQuery implements RequestEntity
{
	private static final long serialVersionUID = 1L;
	
    private String accountId;

    public WSBroadcastSearchQueryRequest()
    {

    }

    public WSBroadcastSearchQueryRequest(WSBroadcastSearchQuery searchQuery, String accountId)
    {
        Validation.isTrue(!Utils.isNull(searchQuery), "searchQuery should not be null");

        this.setLocationId(searchQuery.getLocationId());
        this.setMaxArea(searchQuery.getMaxArea());
        this.setMaxPrice(searchQuery.getMaxPrice());
        this.setMinArea(searchQuery.getMinArea());
        this.setMaxPrice(searchQuery.getMaxPrice());
        this.setPropertyType(searchQuery.getPropertyType());
        this.setRooms(searchQuery.getRooms());
        this.setSearchId(searchQuery.getSearchId());
        this.setSearchTime(searchQuery.getSearchTime());

        this.setAccountId(accountId);
        this.setPropertySubType(searchQuery.getPropertySubType());
    }

    public WSBroadcastSearchQueryRequest(Search search)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(search.getId()), "Search id must not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(search.getLocationId()), "Location id is must be valid");
        Validation.isTrue(!Utils.isNull(search.getTransactionType()), "Transaction type should not be null or empty");
        Validation.isTrue(!Utils.isNull(search.getPropertyType()), "Property type should no be null or empty");
        // Validation.isTrue(!Utils.isEmpty(search.getPropertySubType()),
        // "Property sub type should not be null");

        this.setSearchId(search.getId());
        this.setLocationId(search.getLocationId());
        this.setMaxArea(search.getMaxArea());
        this.setMinArea(search.getMinArea());
        this.setMinPrice(String.valueOf(search.getMinPrice()));
        this.setMaxPrice(String.valueOf(search.getMaxPrice()));

        this.setSearchTime(String.valueOf(search.getSearchTime().getTime()));
        this.setAccountId(search.getCreatedBy());

        if (!Utils.isNull(search.getPropertyType()))
        {
            this.setPropertyType(search.getPropertyType().toString());
        }
        if (!Utils.isNull(search.getRooms()))
        {
            this.setRooms(new ArrayList<String>(Arrays.asList(search.getRooms().split(","))));
        }
        if (!Utils.isNull(search.getPropertySubType()))
        {
            this.setPropertySubType(search.getPropertySubType().toString());
        }

    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSBroadcastSearchQueryRequest other = (WSBroadcastSearchQueryRequest) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSBroadcastSearchQueryRequest [accountId=" + accountId + "]";
    }

}
