package one.group.entities.api.response;

import one.group.core.enums.BroadcastType;

public class WSBroadcast implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tags;

    private String id;

    private BroadcastType type;

    public String getTags()
    {
        return tags;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public BroadcastType getType()
    {
        return type;
    }

    public void setType(BroadcastType type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "WSBroadcast [tags=" + tags + ", id=" + id + ", type=" + type + "]";
    }

}
