package one.group.entities.api.response;

import java.util.ArrayList;
import java.util.List;

public class WSGCMResponse<T> implements GCMResponse<T>
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<String> registrationIds = new ArrayList<String>();
    
    private T data;
    
    public void setData(T data)
    {
        this.data = data;
    }
    
    public void setRegistrationIds(List<String> registrationIds)
    {
        this.registrationIds = registrationIds;
    }
    
    public void addRegistrationId(String registrationId)
    {
        this.registrationIds.add(registrationId);
    }
    
    public List<String> getRegistrationIds()
    {
        return registrationIds;
    }

    public T getData()
    {
        return data;
    }

}
