package one.group.entities.api.response;

import java.util.Set;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSPropertyListingSearchResult implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	private Set<WSMatchingPropertyListing> results;
    private String searchId;
    private String expiryTime;

    public WSPropertyListingSearchResult()
    {

    }

    public WSPropertyListingSearchResult(String searchId, Set<WSMatchingPropertyListing> results, String expiryTime)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(searchId), "search id should not be null or empty");
        Validation.isTrue(!Utils.isNull(results), "results shoud not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(expiryTime), "Expiry time must be valid");

        this.results = results;
        this.searchId = searchId;
        this.expiryTime = expiryTime;
    }

    public Set<WSMatchingPropertyListing> getResults()
    {
        return results;
    }

    public void setResults(Set<WSMatchingPropertyListing> results)
    {
        this.results = results;
    }

    public String getSearchId()
    {
        return searchId;
    }

    public void setSearchId(String searchId)
    {
        this.searchId = searchId;
    }

    public String getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    @Override
    public String toString()
    {
        return "WSPropertyListingSearchResult [results=" + results + ", searchId=" + searchId + ", expiryTime=" + expiryTime + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expiryTime == null) ? 0 : expiryTime.hashCode());
        result = prime * result + ((results == null) ? 0 : results.hashCode());
        result = prime * result + ((searchId == null) ? 0 : searchId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSPropertyListingSearchResult other = (WSPropertyListingSearchResult) obj;
        if (expiryTime == null)
        {
            if (other.expiryTime != null)
                return false;
        }
        else if (!expiryTime.equals(other.expiryTime))
            return false;
        if (results == null)
        {
            if (other.results != null)
                return false;
        }
        else if (!results.equals(other.results))
            return false;
        if (searchId == null)
        {
            if (other.searchId != null)
                return false;
        }
        else if (!searchId.equals(other.searchId))
            return false;
        return true;
    }

}
