/**
 * 
 */
package one.group.entities.api.response.v2;

import java.util.List;

import one.group.entities.api.response.ResponseEntity;

/**
 * @author ashishthorat
 *
 */
public class WSAdminGroupMetaDataList implements ResponseEntity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<WSAdminGroupMetaData> groupMetaData;

    public WSAdminGroupMetaDataList()
    {

    }

    public List<WSAdminGroupMetaData> getGroupMetaData()
    {
        return groupMetaData;
    }

    public void setGroupMetaData(List<WSAdminGroupMetaData> groupMetaData)
    {
        this.groupMetaData = groupMetaData;
    }

}
