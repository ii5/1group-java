package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.GroupMembers;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatThreadCursor implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(WSChatThreadCursor.class);

    private Integer readIndex;

    private Integer receivedIndex;

    private String readMessageId;

    private String receivedMessageId;

    public WSChatThreadCursor()
    {
    }

    public WSChatThreadCursor(GroupMembers groupMember)
    {
        this.readIndex = groupMember.getReadIndex();
        this.receivedIndex = groupMember.getReceivedIndex();
        this.receivedMessageId = groupMember.getReceivedMessageId();
        this.readMessageId = groupMember.getReadMessageId();
    }

    public WSChatThreadCursor(final Integer readIndex, final Integer receivedIndex)
    {
        Validation.isTrue(!(readIndex < -2), "Invalid value of readIndex. [" + readIndex + "]");
        // Validation.isTrue(!(readIndex > receivedIndex),
        // "Invalid value of receivedIndex. [" + readIndex + "]");
        Validation.isTrue(!(receivedIndex < -2), "Invalid value of receivedIndex. [" + receivedIndex + "]");

        this.readIndex = readIndex;
        this.receivedIndex = receivedIndex;
    }

    public String getReadMessageId()
    {
        return readMessageId;
    }

    public void setReadMessageId(String readMessageId)
    {
        this.readMessageId = readMessageId;
    }

    public String getReceivedMessageId()
    {
        return receivedMessageId;
    }

    public void setReceivedMessageId(String receivedMessageId)
    {
        this.receivedMessageId = receivedMessageId;
    }

    public Integer getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(Integer readIndex)
    {
        this.readIndex = readIndex;
    }

    public Integer getReceivedIndex()
    {
        return receivedIndex;
    }

    public void setReceivedIndex(Integer receivedIndex)
    {
        this.receivedIndex = receivedIndex;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((readIndex == null) ? 0 : readIndex.hashCode());
        result = prime * result + ((receivedIndex == null) ? 0 : receivedIndex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatThreadCursor other = (WSChatThreadCursor) obj;
        if (readIndex == null)
        {
            if (other.readIndex != null)
                return false;
        }
        else if (!readIndex.equals(other.readIndex))
            return false;
        if (receivedIndex == null)
        {
            if (other.receivedIndex != null)
                return false;
        }
        else if (!receivedIndex.equals(other.receivedIndex))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatThreadCursor [read_index=" + readIndex + ", received_index=" + receivedIndex + "]";
    }

    public static WSChatThreadCursor dummyData()
    {
        WSChatThreadCursor cursor = new WSChatThreadCursor(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 1, true)), Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 1,
                true)));
        cursor.setReadMessageId(Utils.randomUUID());
        cursor.setReceivedMessageId(Utils.randomUUID());
        return cursor;
    }
}
