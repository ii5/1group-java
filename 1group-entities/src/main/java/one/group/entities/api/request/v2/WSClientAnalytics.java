package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;

public class WSClientAnalytics implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String action;

    private String clientEventTime;

    private String context;

    private String entityId;

    private String entityType;

    public WSClientAnalytics()
    {
        this.action = null;
        this.context = null;
        this.entityId = null;
        this.entityType = null;
        this.clientEventTime = null;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getClientEventTime()
    {
        return clientEventTime;
    }

    public void setClientEventTime(String clientEventTime)
    {
        this.clientEventTime = clientEventTime;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((clientEventTime == null) ? 0 : clientEventTime.hashCode());
        result = prime * result + ((context == null) ? 0 : context.hashCode());
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSClientAnalytics other = (WSClientAnalytics) obj;
        if (action == null)
        {
            if (other.action != null)
                return false;
        }
        else if (!action.equals(other.action))
            return false;
        if (clientEventTime == null)
        {
            if (other.clientEventTime != null)
                return false;
        }
        else if (!clientEventTime.equals(other.clientEventTime))
            return false;
        if (context == null)
        {
            if (other.context != null)
                return false;
        }
        else if (!context.equals(other.context))
            return false;
        if (entityId == null)
        {
            if (other.entityId != null)
                return false;
        }
        else if (!entityId.equals(other.entityId))
            return false;
        if (entityType == null)
        {
            if (other.entityType != null)
                return false;
        }
        else if (!entityType.equals(other.entityType))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSAnalytics [" + (action != null ? "action=" + action + ", " : "") + (clientEventTime != null ? "client_event_time=" + clientEventTime + ", " : "")
                + (context != null ? "context=" + context + ", " : "") + (entityId != null ? "entityId=" + entityId + ", " : "") + (entityType != null ? "entityType=" + entityType : "") + "]";
    }

}
