package one.group.entities.api.response.bpo;

import java.util.HashSet;
import java.util.Set;

import one.group.entities.jpa.AuthItem;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSAuthItem
{

    private String name;
    private String type;
    private String description;
    private String ruleName;
    private String data;
    private String createdBy;
    private String createdTime;
    private String updatedBy;
    private String updatedTime;
    private Set<String> children;

    public WSAuthItem(AuthItem item)
    {
        this.name = item.getName();
        this.type = item.getType() + "";
        this.description = item.getDescription();
        this.ruleName = item.getRuleName();
        this.data = item.getData();
        this.createdBy = item.getCreatedBy();
        this.createdTime = (item.getCreatedTime()) == null ? null : item.getCreatedTime().getTime() + "";
        this.updatedBy = item.getUpdatedBy();
        this.updatedTime = (item.getUpdatedTime()) == null ? null : item.getUpdatedTime().getTime() + "";
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getRuleName()
    {
        return ruleName;
    }

    public void setRuleName(String ruleName)
    {
        this.ruleName = ruleName;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Set<String> getChildren()
    {
        return children;
    }

    public void setChildren(Set<String> children)
    {
        this.children = children;
    }

    public void addChildren(String child)
    {
        if (children == null)
        {
            children = new HashSet<String>();
        }

        children.add(child);
    }

}
