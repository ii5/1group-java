package one.group.entities.api.response;

import one.group.core.enums.PhoneNumberType;
import one.group.entities.jpa.PhoneNumber;
import one.group.utils.validation.Validation;

/**
 * API Schema for phone number
 * 
 * @author sanilshet
 * 
 */
public class WSPhoneNumber implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private PhoneNumberType type;

    private String number;

    private String associatedAccountId;

    public WSPhoneNumber()
    {
    };

    public WSPhoneNumber(PhoneNumber phoneNumber)
    {
        this.setNumber(phoneNumber.getNumber());
        this.setType(phoneNumber.getType());
    }
    
    public WSPhoneNumber(PhoneNumber phoneNumber, String associatedAccountId)
    {
        Validation.notNull(phoneNumber, "Phonenumber should not be null.");
        Validation.notNull(associatedAccountId, "associatedAccountId should not be null.");
        this.setNumber(phoneNumber.getNumber());
        this.setType(phoneNumber.getType());
        this.associatedAccountId = associatedAccountId;
    }

    public PhoneNumberType getType()
    {
        return type;
    }

    public String getNumber()
    {
        return number;
    }

    public void setType(PhoneNumberType type)
    {
        this.type = type;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public String getAssociatedAccountId()
    {
        return associatedAccountId;
    }

    public void setAssociatedAccountId(String associatedAccountId)
    {
        this.associatedAccountId = associatedAccountId;
    }

    @Override
    public String toString()
    {
        return "WSPhoneNumber [type=" + type + ", number=" + number + ", associatedAccountId=" + associatedAccountId + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSPhoneNumber other = (WSPhoneNumber) obj;
        if (number == null)
        {
            if (other.number != null)
                return false;
        }
        else if (!number.equals(other.number))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
