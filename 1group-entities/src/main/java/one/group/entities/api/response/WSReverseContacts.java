package one.group.entities.api.response;

import java.util.List;

public class WSReverseContacts implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSAdminReverseContacts> accounts;

    public WSReverseContacts()
    {

    }

    public List<WSAdminReverseContacts> getAccounts()
    {
        return accounts;
    }

    public void setAccounts(List<WSAdminReverseContacts> accounts)
    {
        this.accounts = accounts;
    }

}
