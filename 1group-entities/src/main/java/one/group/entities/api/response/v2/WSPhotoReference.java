package one.group.entities.api.response.v2;

import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Media;
import one.group.utils.validation.Validation;

/**
 * The Photo API schema.
 * 
 * @author miteshchavda
 * 
 */
public class WSPhotoReference implements ResponseEntity, RequestEntity
{
	private static final long serialVersionUID = 1L;
	
	private WSPhoto full;

    private WSPhoto thumbnail;

    public WSPhotoReference()
    {
    };

    public WSPhotoReference(final WSPhoto full, final WSPhoto thumbnail)
    {
        Validation.notNull(full, "Full object should not be empty.");
        Validation.notNull(thumbnail, "Thubnail object should not be empty.");

        this.full = full;
        this.thumbnail = thumbnail;
    }

    public WSPhotoReference(final Media full, final Media thumbnail)
    {

        if (full != null)
        {
        	this.full = new WSPhoto(full.getHeight(), full.getWidth(), full.getUrl(), full.getSize() == null ? null : Long.valueOf(full.getSize().toString()));
        }
        if (thumbnail != null)
        {
            this.thumbnail = new WSPhoto(thumbnail.getHeight(), thumbnail.getWidth(), thumbnail.getUrl(), thumbnail.getSize() == null ? null : Long.valueOf(thumbnail.getSize().toString()));
        }
    }

    public WSPhoto getFull()
    {
        return full;
    }

    public WSPhoto getThumbnail()
    {
        return thumbnail;
    }

    
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((full == null) ? 0 : full.hashCode());
        result = prime * result + ((thumbnail == null) ? 0 : thumbnail.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSPhotoReference other = (WSPhotoReference) obj;
        if (full == null)
        {
            if (other.full != null)
                return false;
        }
        else if (!full.equals(other.full))
            return false;
        if (thumbnail == null)
        {
            if (other.thumbnail != null)
                return false;
        }
        else if (!thumbnail.equals(other.thumbnail))
            return false;
        return true;
    }
    
    

    public void setFull(WSPhoto full) 
    {
		this.full = full;
	}

	public void setThumbnail(WSPhoto thumbnail) 
	{
		this.thumbnail = thumbnail;
	}

	@Override
    public String toString()
    {
        return "WSPhotoReference [full=" + full + ", thumbnail=" + thumbnail + "]";
    }

    public static WSPhotoReference dummyData()
    {
        WSPhoto p1 = WSPhoto.dummyData();
        WSPhoto p2 = WSPhoto.dummyData();
        
        WSPhotoReference ref = new WSPhotoReference(p1, p2);
        
        return ref;
        
    }
}
