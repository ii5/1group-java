package one.group.entities.api.response;

import one.group.core.Constant;
import one.group.entities.api.request.RequestEntity;
import one.group.utils.validation.Validation;

/**
 * The Photo API schema.
 * 
 * @author miteshchavda
 * 
 */
public class WSPhoto implements ResponseEntity, RequestEntity
{
	private static final long serialVersionUID = 1L;
	
	private Integer height = Constant.MIN_MEDIA_HEIGHT;

    private Integer width = Constant.MIN_MEDIA_WIDTH;

    private String location;

    private Long size;

    public WSPhoto()
    {
    };

    public WSPhoto(final int height, final int width, final String location, final long size)
    {
        // Removing validation for min height and width.
//        Validation.isTrue(height >= Constant.MIN_MEDIA_HEIGHT, "Media height should not be less than " + Constant.MIN_MEDIA_HEIGHT + ".");
//        Validation.isTrue(width >= Constant.MIN_MEDIA_WIDTH, "Media height should not be less than " + Constant.MIN_MEDIA_HEIGHT + ".");
        Validation.notNull(location, "The location URL of the media should not be null.");
        Validation.isTrue(size > 0, "The media size should not be empty.");

        this.height = height;
        this.width = width;

        this.location = location;
        this.size = size;
    }

    public int getHeight()
    {
        return height;
    }

    public String getLocation()
    {
        return location;
    }

    public long getSize()
    {
        return size;
    }

    public int getWidth()
    {
        return width;
    }

    
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((height == null) ? 0 : height.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((width == null) ? 0 : width.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSPhoto other = (WSPhoto) obj;
        if (height == null)
        {
            if (other.height != null)
                return false;
        }
        else if (!height.equals(other.height))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (width == null)
        {
            if (other.width != null)
                return false;
        }
        else if (!width.equals(other.width))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSPhoto [height=" + height + ", width=" + width + ", location=" + location + ", size=" + size + "]";
    }

}
