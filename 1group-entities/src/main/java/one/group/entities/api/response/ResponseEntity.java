package one.group.entities.api.response;

import java.io.Serializable;

/**
 * marker interface
 * @author nyalfernandes
 *
 */
public interface ResponseEntity extends Serializable
{
	public static final long serialVersionUID = 1L;
}
