package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;

public class WSSortCriteria implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String field;

    private String direction;

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public String getDirection()
    {
        return direction;
    }

    public void setDirection(String direction)
    {
        this.direction = direction;
    }

    @Override
    public String toString()
    {
        return "WSSortCriteria [field=" + field + ", direction=" + direction + "]";
    }

    public static WSSortCriteria dummyData(String field, String direction)
    {
        WSSortCriteria c = new WSSortCriteria();
        c.setDirection(direction);
        c.setField(field);
        
        return c;
    }
}
