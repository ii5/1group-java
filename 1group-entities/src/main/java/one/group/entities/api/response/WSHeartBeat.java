package one.group.entities.api.response;

import java.util.HashSet;
import java.util.Set;

import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSHeartBeat implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<String> onlineAccountIds = new HashSet<String>();

    public Set<String> getOnlineAccountIds()
    {
        return onlineAccountIds;
    }

    public void setOnlineAccountIds(final Set<String> onlineAccountIds)
    {
        Validation.notNull(onlineAccountIds, "Online account Ids cannot be null.");
        this.onlineAccountIds = onlineAccountIds;
    }
    
    public void addOnlineAccountId(final String onlineAccountId)
    {
        Validation.notNull(onlineAccountId, "Online account Id cannot be null.");
        this.onlineAccountIds.add(onlineAccountId);
    }
}
