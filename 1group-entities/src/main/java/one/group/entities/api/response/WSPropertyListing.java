package one.group.entities.api.response;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.PropertyListing;
import one.group.utils.Utils;

/**
 * 
 * @author sanilshet
 * 
 */
public class WSPropertyListing implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private String shortReference;

    private String id;

    private String description;

    private int area;

    private boolean isHot;

    private String propertyType;

    private String propertySubType;

    private String status;

    private String salePrice;

    private String rentPrice;

    private String commissionType;

    private String measurementUnit;

    private List<String> amenities;

    private String listingTime;

    private Integer locationId;

    private String createdBy;

    private String rooms;

    private String propertyMarket;

    private String city;

    private Set<WSMatchingClients> matchingClients;

    private String externalSource;

    private String landmark;

    private String project;

    private String roomType;

    private Boolean isBookmarked;

    private String oldPropertyListingId;

    private SyncActionType syncActionType;

    private Boolean changedRequiredFields;

    private String customSublocation;

    private String expiryTime;

    private Date lastRenewedTime;

    private WSAccount accountDetails;

    private String locationDetails;

    public WSPropertyListing()
    {
        this.amenities = new ArrayList<String>();
        this.matchingClients = new HashSet<WSMatchingClients>();

    }

    /**
     * Set all property values
     * 
     * @param property
     * @throws ParseException
     */
    public WSPropertyListing(PropertyListing property)
    {

        this.setDescription(property.getDescription());
        this.setId(property.getIdAsString());
        this.setIsHot(property.getIsHot());
        if (property.getStatus().equals(BroadcastStatus.ACTIVE) || property.getStatus().equals(BroadcastStatus.EXPIRED))
        {
            this.setShortReference(property.getShortReference());
        }
        else
        {
            this.setShortReference(null);
        }

        this.setArea(property.getArea() > 0 ? property.getArea() : 0);
        this.setRentPrice(String.valueOf(property.getRentPrice()));
        this.setSalePrice(String.valueOf(property.getSalePrice()));
        // get current date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String currentDate = sdf.format(new Date());
        Date current = null;
        try
        {
            current = sdf.parse(currentDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        // get property listings created time
        if (property.getCreatedTime() != null)
        {
            if (property.getLastRenewedTime() == null)
            {
                this.setExpiryTime("" + (property.getCreatedTime().getTime() + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS));
            }
            else
            {
                this.setExpiryTime("" + (Math.max((property.getLastRenewedTime().getTime()), (property.getCreatedTime().getTime())) + Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS));
            }
            // always check logical status for each property listing , rather
            // than actual physical status. This logical may differ from actual
            // physical status that is present in source of truth
            this.setStatus(property.getStatus().toString().toLowerCase());
            Date getCreatedTime = property.getLastRenewedTime() != null ? property.getLastRenewedTime() : property.getCreatedTime();
            long diff = current.getTime() - getCreatedTime.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if (diff > Constant.TIME_TO_BE_EXPIRE_IN_MILLISECONDS)
            {
                this.setStatus(BroadcastStatus.EXPIRED.toString().toLowerCase());
            }
            if (diff > Constant.TIME_TO_BE_CLOSED_IN_MILLISECONDS || property.getStatus().equals(BroadcastStatus.CLOSED))
            {
                this.setShortReference(null);
                this.setStatus(BroadcastStatus.CLOSED.toString().toLowerCase());
            }
        }

        this.setPropertyType(property.getType().toString().toLowerCase());
        this.setPropertySubType(property.getSubType() != null ? property.getSubType().toString().toLowerCase() : null);
        this.setCommissionType(property.getCommissionType() != null ? property.getCommissionType().toString().toLowerCase() : null);
        this.setRooms(property.getRooms() != null && !Utils.isEmpty(property.getRooms()) ? property.getRooms().toString() : null);
        if (property.getCreatedTime() != null)
        {
            this.setListingTime("" + property.getCreatedTime().getTime());
        }

        this.setPropertyMarket(property.getPropertyMarket().toString());
        this.setCreatedBy(property.getAccount().getIdAsString());
        //if(property.getExternalSource() != null)
        //{
        this.setExternalSource(property.getExternalSource() != null ? property.getExternalSource() : null);
        //}
        //else
        //{
        	//this.setExternalSource(false);
        //}
        
        this.setLocationId(Integer.valueOf(property.getLocation().getId()));
        this.setIsHot(property.getIsHot());
        this.setIsBookmarked(false);
        this.setCustomSublocation(property.getCustomSubLocation());
        this.amenities = new ArrayList<String>();
        this.matchingClients = new HashSet<WSMatchingClients>();
        this.accountDetails = null;
    }

    public String getLocationDetails()
    {
        return locationDetails;
    }

    public void setLocationDetails(String locationDetails)
    {
        this.locationDetails = locationDetails;
    }

    public Boolean getChangedRequiredFields()
    {
        return changedRequiredFields;
    }

    public void setChangedRequiredFields(Boolean changedRequiredFields)
    {
        this.changedRequiredFields = changedRequiredFields;
    }

    public String getOldPropertyListingId()
    {
        return oldPropertyListingId;
    }

    public SyncActionType getSyncActionType()
    {
        return syncActionType;
    }

    public void setOldPropertyListingId(String oldPropertyListingId)
    {
        this.oldPropertyListingId = oldPropertyListingId;
    }

    public void setSyncActionType(SyncActionType syncActionType)
    {
        this.syncActionType = syncActionType;
    }

    /**
     * @return the measurementUnit
     */
    public String getMeasurementUnit()
    {
        return measurementUnit;
    }

    /**
     * @param measurementUnit
     *            the measurementUnit to set
     */
    public void setMeasurementUnit(String measurementUnit)
    {
        this.measurementUnit = measurementUnit;
    }

    /**
     * @return the subType
     */
    public String getPropertySubType()
    {
        return propertySubType;
    }

    /**
     * @return the type
     */
    public String getPropertyType()
    {
        return propertyType;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setPropertyType(String propertyType)
    {
        this.propertyType = propertyType;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @return the rentPrice
     */
    public String getRentPrice()
    {
        return rentPrice;
    }

    /**
     * @param subType
     *            the subType to set
     */
    public void setPropertySubType(String propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * @param rentPrice
     *            the rentPrice to set
     */
    public void setRentPrice(String rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    /**
     * @return the amenities
     */
    public List<String> getAmenities()
    {
        return amenities;
    }

    /**
     * @param amenities
     *            the amenities to set
     */
    public void setAmenities(List<String> amenities)
    {
        this.amenities = amenities;
    }

    // public void addAmenities(WSAmenityReference amenities)
    // {
    // this.amenities.add(amenities);
    // }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the size
     */
    public float getArea()
    {
        return area;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setArea(int area)
    {
        this.area = area;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    /**
     * @return the propertyMarket
     */
    public String getPropertyMarket()
    {
        return propertyMarket;
    }

    /**
     * @param propertyMarket
     *            the propertyMarket to set
     */
    public void setPropertyMarket(String propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    /**
     * @return the shortReference
     */
    public String getShortReference()
    {
        return shortReference;
    }

    /**
     * @param shortReference
     *            the shortReference to set
     */
    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }

    /**
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    /**
     * @return the isHot
     */
    public boolean getIsHot()
    {
        return isHot;
    }

    /**
     * @param isHot
     *            the isHot to set
     */
    public void setIsHot(boolean isHot)
    {
        this.isHot = isHot;
    }

    /**
     * @return the forSalePrice
     */
    public String getSalePrice()
    {
        return salePrice;
    }

    /**
     * @param forSalePrice
     *            the forSalePrice to set
     */
    public void setSalePrice(String salePrice)
    {
        this.salePrice = salePrice;
    }

    /**
     * @return the externalSource
     */
    public String getExternalSource()
    {
        return externalSource;
    }

    /**
     * @param externalSource
     *            the externalSource to set
     */
    public void setExternalSource(String externalSource)
    {
        this.externalSource = externalSource;
    }

    /**
     * @return the landmark
     */
    public String getLandmark()
    {
        return landmark;
    }

    /**
     * @return the project
     */
    public String getProject()
    {
        return project;
    }

    /**
     * @param landmark
     *            the landmark to set
     */
    public void setLandmark(String landmark)
    {
        this.landmark = landmark;
    }

    /**
     * @param project
     *            the project to set
     */
    public void setProject(String project)
    {
        this.project = project;
    }

    /**
     * @return the roomType
     */
    public String getRoomType()
    {
        return roomType;
    }

    /**
     * @param roomType
     *            the roomType to set
     */
    public void setRoomType(String roomType)
    {
        this.roomType = roomType;
    }

    /**
     * @return the isBookmarked
     */
    public Boolean getIsBookmarked()
    {
        return isBookmarked;
    }

    /**
     * @param isBookmarked
     *            the isBookmarked to set
     */
    public void setIsBookmarked(Boolean isBookmarked)
    {
        this.isBookmarked = isBookmarked;
    }

    /**
     * @return the locationId
     */
    public Integer getLocationId()
    {
        return locationId;
    }

    /**
     * @param locationId
     *            the locationId to set
     */
    public void setLocationId(Integer locationId)
    {
        this.locationId = locationId;
    }

    public String getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(String commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public String getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    public String getListingTime()
    {
        return listingTime;
    }

    public void setListingTime(String listingTime)
    {
        this.listingTime = listingTime;
    }

    public Date getLastRenewedTime()
    {
        return lastRenewedTime;
    }

    public void setLastRenewedTime(Date lastRenewedTime)
    {
        this.lastRenewedTime = lastRenewedTime;
    }

    public WSAccount getAccountDetails()
    {
        return accountDetails;
    }

    public void setAccountDetails(WSAccount accountDetails)
    {
        this.accountDetails = accountDetails;
    }

    public Set<WSMatchingClients> getMatchingClients()
    {
        return matchingClients;
    }

    public void setMatchingClients(Set<WSMatchingClients> matchingClients)
    {
        this.matchingClients = matchingClients;
    }

    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSPropertyListing other = (WSPropertyListing) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSPropertyListing [referenceId=" + shortReference + ", id=" + id + ", description=" + description + ", area=" + area + "," + "  isHot=" + isHot + ", amenities=" + amenities + "]";

    }

}