package one.group.entities.api.response;

public class WSMonitor implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private boolean cache;
	
	private boolean database;
	
	private boolean kafka;	
	
	private boolean zookeeper;	
	
	private boolean overallStatus;

	public boolean isZookeeper() 
	{
		return zookeeper;
	}

	public void setZookeeper(boolean zookeeper) 
	{
		this.zookeeper = zookeeper;
	}
	
	public boolean isOverallStatus() 
	{
		return overallStatus;
	}

	public void setOverallStatus(boolean overallStatus) 
	{
		this.overallStatus = overallStatus;
	}

	public boolean isCache() 
	{
		return cache;
	}

	public void setCache(boolean cache)
	{
		this.cache = cache;
	}

	public boolean isDatabase() 
	{
		return database;
	}

	public void setDatabase(boolean database) 
	{
		this.database = database;
	}

	public boolean isKafka() 
	{
		return kafka;
	}

	public void setKafka(boolean kafka)
	{
		this.kafka = kafka;
	}	
	
}
