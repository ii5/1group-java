package one.group.entities.api.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import one.group.entities.jpa.helpers.WSNewsFeedAccountComparator;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSNewsFeed implements ResponseEntity, Comparable<WSNewsFeed>
{
	private static final long serialVersionUID = 1L;
	private String propertyListingId;

    private String accountId;

    private float relevance;

    private Float recalculatedRelevance;

    private String listingTime;

    private String expiryTime;
    @Deprecated
    private WSAccount account;

    private WSPropertyListing propertyListingDetails;

    public WSNewsFeed()
    {

    }

    public WSNewsFeed(String propertyListingId, String accountId, float relevance, String listingTime)
    {
        Validation.isTrue(!Utils.isNull(propertyListingId), "Passed propertyListingId should not be null");
        Validation.isTrue(!Utils.isNull(accountId), "Passed accountId should not be null");
        Validation.isTrue(!Utils.isNull(relevance), "Passed relevence should not be null");
        Validation.notNull(listingTime, "Listing time passed should not be null.");

        this.propertyListingId = propertyListingId;
        this.accountId = accountId;
        this.relevance = relevance;
        this.listingTime = listingTime;
    }

    public WSNewsFeed(String propertyListingId, String accountId, float relevance, String listingTime, String expiryTime)
    {
        Validation.isTrue(!Utils.isNull(propertyListingId), "Passed propertyListingId should not be null");
        Validation.isTrue(!Utils.isNull(accountId), "Passed accountId should not be null");
        Validation.isTrue(!Utils.isNull(relevance), "Passed relevence should not be null");
        Validation.notNull(listingTime, "Listing time passed should not be null.");
        Validation.notNull(expiryTime, "Listing time passed should not be null.");

        this.propertyListingId = propertyListingId;
        this.accountId = accountId;
        this.relevance = relevance;
        this.listingTime = listingTime;
        this.expiryTime = expiryTime;
    }

    public WSNewsFeed(String propertyListingId, String accountId, String listingTime)
    {
        Validation.isTrue(!Utils.isNull(propertyListingId), "Passed propertyListingId should not be null");
        Validation.isTrue(!Utils.isNull(accountId), "Passed accountId should not be null");
        Validation.notNull(listingTime, "Listing time passed should not be null.");

        this.propertyListingId = propertyListingId;
        this.accountId = accountId;
        this.listingTime = listingTime;
    }

    public WSNewsFeed(WSNewsFeed newsFeed)
    {
        Validation.notNull(newsFeed, "The newsFeed passed should not be null.");

        this.account = newsFeed.getAccount();
        this.accountId = newsFeed.getAccountId();
        this.listingTime = newsFeed.getListingTime();
        this.propertyListingDetails = newsFeed.getPropertyListingDetails();
        this.propertyListingId = newsFeed.getPropertyListingId();
        this.relevance = newsFeed.getRelevance();
    }

    public Float getRecalculatedRelevance()
    {
        return recalculatedRelevance;
    }

    public void setRecalculatedRelevance(Float recalculatedRelevance)
    {
        this.recalculatedRelevance = recalculatedRelevance;
    }

    public String getListingTime()
    {
        return listingTime;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public void setListingTime(String listingTime)
    {
        this.listingTime = listingTime;
    }

    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public float getRelevance()
    {
        return relevance;
    }

    public void setRelevance(float relevance)
    {
        this.relevance = relevance;
    }

    @Deprecated
    public WSAccount getAccount()
    {
        return account;
    }

    @Deprecated
    public void setAccount(WSAccount account)
    {
        this.account = account;
    }

    public WSPropertyListing getPropertyListingDetails()
    {
        return propertyListingDetails;
    }

    public void setPropertyListingDetails(WSPropertyListing propertyListingDetails)
    {
        this.propertyListingDetails = propertyListingDetails;
    }

    public String getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        result = prime * result + ((propertyListingId == null) ? 0 : propertyListingId.hashCode());
        long temp;
        temp = Double.doubleToLongBits(relevance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSNewsFeed other = (WSNewsFeed) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (propertyListingId == null)
        {
            if (other.propertyListingId != null)
                return false;
        }
        else if (!propertyListingId.equals(other.propertyListingId))
            return false;
        if (Double.doubleToLongBits(relevance) != Double.doubleToLongBits(other.relevance))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSNewsFeed [propertyListingId=" + propertyListingId + ", accountId=" + accountId + ", relevance=" + relevance + ", recalculatedRelevance=" + recalculatedRelevance + ", listingTime="
                + listingTime + ", expiryTime=" + expiryTime + ", account=" + account + ", propertyListingDetails=" + propertyListingDetails + "]";
    }

    public int compareTo(WSNewsFeed otherNewsFeed)
    {
        int retVal = -1;
        if (this.relevance == otherNewsFeed.relevance)
        {
            retVal = otherNewsFeed.getListingTime().compareTo(this.getListingTime());
        }
        else if (this.relevance > otherNewsFeed.relevance)
        {
            retVal = -1;
        }
        else if (this.relevance < otherNewsFeed.relevance)
        {
            retVal = 1;
        }

        return retVal;
    }

    public static void main(String args[])
    {
        WSNewsFeed nf1 = new WSNewsFeed();
        nf1.setRelevance(1.1f);
        nf1.setAccountId("1");
        nf1.setPropertyListingId("1");

        WSNewsFeed nf2 = new WSNewsFeed();
        nf2.setRelevance(1.2f);
        nf2.setAccountId("");
        nf2.setPropertyListingId("1");

        WSNewsFeed nf3 = new WSNewsFeed();
        nf3.setRelevance(1.1f);
        nf3.setAccountId("1");
        nf3.setPropertyListingId("1");

        WSNewsFeed nf4 = new WSNewsFeed();
        nf4.setRelevance(1.3f);
        nf4.setAccountId("1");
        nf4.setPropertyListingId("1");

        System.out.println(nf1.equals(nf2));

        Set<WSNewsFeed> nfSet = new TreeSet<WSNewsFeed>();
        nfSet.add(nf1);
        nfSet.add(nf2);
        nfSet.add(nf3);
        nfSet.add(nf4);
        System.out.println(nfSet);

        Collections.sort(new ArrayList<WSNewsFeed>(nfSet), new WSNewsFeedAccountComparator());
    }
}
