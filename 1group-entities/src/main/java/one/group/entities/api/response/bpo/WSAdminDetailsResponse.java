package one.group.entities.api.response.bpo;

import one.group.core.enums.AdminStatus;
import one.group.entities.api.response.v2.WSCity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAdminDetailsResponse
{
    private String userName;

    private String password;

    private String rolesId;

    private String email;

    private String lastLogin;

    private String authKey;

    private String passwordHash;

    private String passwordResetToken;

    private AdminStatus status;

    private WSCity city;

    private String managerId;
    
    private int priority;

    public String getUserName()
    {
        return userName;
    }

    public String getPassword()
    {
        return password;
    }

    public String getRolesId()
    {
        return rolesId;
    }

    public String getEmail()
    {
        return email;
    }

    public String getLastLogin()
    {
        return lastLogin;
    }

    public String getAuthKey()
    {
        return authKey;
    }

    public String getPasswordHash()
    {
        return passwordHash;
    }

    public String getPasswordResetToken()
    {
        return passwordResetToken;
    }

    public AdminStatus getStatus()
    {
        return status;
    }

    public WSCity getCity()
    {
        return city;
    }

    public String getManagerId()
    {
        return managerId;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setRolesId(String rolesId)
    {
        this.rolesId = rolesId;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setLastLogin(String lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    public void setAuthKey(String authKey)
    {
        this.authKey = authKey;
    }

    public void setPasswordHash(String passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    public void setPasswordResetToken(String passwordResetToken)
    {
        this.passwordResetToken = passwordResetToken;
    }

    public void setStatus(AdminStatus status)
    {
        this.status = status;
    }

    public void setCity(WSCity city)
    {
        this.city = city;
    }

    public void setManagerId(String managerId)
    {
        this.managerId = managerId;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }
    
    
    public static WSAdminDetailsResponse dummyData(int priority)
    {
        WSAdminDetailsResponse response = new WSAdminDetailsResponse();
        
        response.setAuthKey(Utils.randomUUID());
        response.setCity(WSCity.dummyData("Mumbai"));
        response.setUserName("user-" + Utils.generateAccountReference());
        response.setEmail(response.getUserName() + "@somecompany.com");
        response.setLastLogin(System.currentTimeMillis() + "");
        response.setManagerId(Utils.randomUUID());
        response.setPassword(Utils.randomUUID());
        response.setPasswordHash(response.getPassword());
        response.setPasswordResetToken("");
        response.setPriority(priority);
        response.setStatus(AdminStatus.ACTIVE);
        
        return response;
    }
}
