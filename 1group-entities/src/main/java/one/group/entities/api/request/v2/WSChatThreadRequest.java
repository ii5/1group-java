package one.group.entities.api.request.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.api.response.v2.WSAccount;
import one.group.entities.api.response.v2.WSChatThread;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSChatThreadRequest
{
    private List<String> participants = new ArrayList<String>();

    public List<String> getParticipants()
    {
        return participants;
    }

    public void setParticipants(List<String> participants)
    {
        this.participants = participants;
    }
    
    public void addParticipant(String id)
    {
        this.participants.add(id);
    }
    
    public static WSChatThreadRequest dummyData()
    {
        WSChatThreadRequest request = new WSChatThreadRequest();
        request.addParticipant(Utils.randomUUID());
        request.addParticipant(Utils.randomUUID());
        return request;
    }
    
    
    public static void main(String[] args)
    {
     
        String endpoint = "/chat_threads";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.POST;
        SocketEntity entity = new SocketEntity();
        WSAccount fromAccount = WSAccount.dummyData("Mark Lowry", "Mark and Reality", "+919876598765", false);
        WSAccount toAccount = WSAccount.dummyData("Greogory Gardner", "Mark and Reality", "+919988776655", false);
        WSChatThread responseData = WSChatThread.dummyDataWithType(20, 0, fromAccount, toAccount, 10, 20, System.currentTimeMillis() + "", new MessageType[] {MessageType.PHONE_CALL});
        WSResult result = new WSResult("200", "Chat thread data.", false);
        WSResponse response = new WSResponse();
        
        WSChatThreadRequest chatThreadReques = WSChatThreadRequest.dummyData();
        WSRequest request = new WSRequest();
        request.setChatThread(chatThreadReques);
        
        response.setData(responseData);
        response.setResult(result);
        

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        entity.setWithWSRequest(request);
        
        System.out.println(Utils.getJsonString(entity));
    }
}
