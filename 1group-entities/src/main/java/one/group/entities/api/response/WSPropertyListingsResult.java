package one.group.entities.api.response;

import java.util.HashSet;
import java.util.Set;

import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSPropertyListingsResult
{
    private Set<WSPropertyListing> propertyListingsResult = new HashSet<WSPropertyListing>();
    
    public Set<WSPropertyListing> getPropertyListingsResult()
    {
        return propertyListingsResult;
    }
    
    public void setPropertyListingsResult(Set<WSPropertyListing> propertyListingsResult)
    {
        this.propertyListingsResult = propertyListingsResult;
    }
    
    public void addPropertyListing(WSPropertyListing propertyListing)
    {
        this.propertyListingsResult.add(propertyListing);
    }
    
    public static void main(String[] args)
    {
        WSResponse response = new WSResponse();
        WSResult result = new WSResult();
        result.setCode("200");
        result.setIsFatal(false);
        result.setMessage("Done");
        
        response.setResult(result);
        
        WSAccountsResult data = new WSAccountsResult();
        WSAccount account = new WSAccount();
        account.setFullName("Nyal");
        account.setCompanyName("Ferns");
        account.setId("1");
        account.setPrimaryMobile("+919876543210");
        data.addAccountResult(account);
        
        account = new WSAccount();
        account.setFullName("Mitesh");
        account.setCompanyName("Chadda");
        account.setId("2");
        account.setPrimaryMobile("+919876543210");
        data.addAccountResult(account);
        
        response.setData(data);
        System.out.println(Utils.getJsonString(response));
    }
}
