package one.group.entities.api.response;

import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSMatchingPropertyListing implements ResponseEntity, Comparable<WSMatchingPropertyListing>
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String propertyListingId;

    private String accountId;

    private String listingTime;

    private String match;

    private float relevance;

    private String rentPrice;

    private String salePrice;

    private WSPropertyListing propertyListingDetails;

    public WSMatchingPropertyListing()
    {

    }

    public WSMatchingPropertyListing(PropertyListingScoreObject scoredObject)
    {
        Validation.notNull(scoredObject, "The object passed should not be null.");

        this.propertyListingId = scoredObject.getId();
        this.accountId = scoredObject.getAccountId();
        this.listingTime = "" + scoredObject.getCreationTime().getTime();
        this.rentPrice = "" + scoredObject.getRentPrice();
        this.salePrice = "" + scoredObject.getSalePrice();
    }

    public WSMatchingPropertyListing(String propertyListingId, String accountId, String listingTime, String match, float relevance, String rentPrice, String salePrice)
    {
        Validation.isTrue(!Utils.isNull(propertyListingId), "Passed propertyListingId should not be null");
        Validation.isTrue(!Utils.isNull(accountId), "Passed accountId should not be null");
        Validation.isTrue(!Utils.isNull(listingTime), "Passed listingTime should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(match), "Passed match should not be null");
        Validation.isTrue(!Utils.isNull(relevance), "Passed relevence should not be null");

        this.propertyListingId = propertyListingId;
        this.accountId = accountId;
        this.listingTime = listingTime;
        this.match = match;
        this.relevance = relevance;
        this.rentPrice = rentPrice;
        this.salePrice = salePrice;
    }

    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public String getListingTime()
    {
        return listingTime;
    }

    public String getMatch()
    {
        return match;
    }

    public float getRelevance()
    {
        return relevance;
    }

    public String getRentPrice()
    {
        return rentPrice;
    }

    public void setRentPrice(String rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    public String getSalePrice()
    {
        return salePrice;
    }

    public void setSalePrice(String salePrice)
    {
        this.salePrice = salePrice;
    }

    public void setMatch(String match)
    {
        this.match = match;
    }

    public void setRelevance(float relevance)
    {
        this.relevance = relevance;
    }

    public WSPropertyListing getPropertyListingDetails()
    {
        return propertyListingDetails;
    }

    public void setPropertyListingDetails(WSPropertyListing propertyListingDetails)
    {
        this.propertyListingDetails = propertyListingDetails;
    }

    @Override
    public String toString()
    {
        return "WSMatchingPropertyListing [propertyListingId=" + propertyListingId + ", accountId=" + accountId + ", listingTime=" + listingTime + ", match=" + match + ", relevance=" + relevance
                + ", rentPrice=" + rentPrice + ", salePrice=" + salePrice + ", propertyListingDetails=" + propertyListingDetails + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((propertyListingId == null) ? 0 : propertyListingId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSMatchingPropertyListing other = (WSMatchingPropertyListing) obj;
        if (propertyListingId == null)
        {
            if (other.propertyListingId != null)
                return false;
        }
        else if (!propertyListingId.equals(other.propertyListingId))
            return false;
        return true;
    }

    public int compareTo(WSMatchingPropertyListing o2)
    {
        if (o2 == null)
        {
            return -1;
        }

        if (this.equals(o2))
        {
            return 0;
        }

        return this.getRelevance() < o2.getRelevance() ? 1 : -1;
    }

}
