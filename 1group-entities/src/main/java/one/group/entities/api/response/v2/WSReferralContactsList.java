package one.group.entities.api.response.v2;

import java.util.List;

import one.group.entities.api.response.ResponseEntity;

public class WSReferralContactsList implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
    private boolean syncFinished;
    private List<WSReferralContacts> contactsList;

    public List<WSReferralContacts> getContactsList()
    {
        return contactsList;
    }

    public void setContactsList(List<WSReferralContacts> contactsList)
    {
        this.contactsList = contactsList;
    }

    public boolean isSyncFinished()
    {
        return syncFinished;
    }

    public void setSyncFinished(boolean syncFinished)
    {
        this.syncFinished = syncFinished;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((contactsList == null) ? 0 : contactsList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSReferralContactsList other = (WSReferralContactsList) obj;
        if (contactsList == null)
        {
            if (other.contactsList != null)
                return false;
        }
        else if (!contactsList.equals(other.contactsList))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSReferralContactsList [contactsList=" + contactsList + "]";
    }

}
