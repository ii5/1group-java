package one.group.entities.api.response.v2;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.SyncActionType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.BroadcastTag;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSBroadcast implements ResponseEntity
{

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String id;

    private String messageId;

    private BroadcastStatus status;

    private Set<WSTag> tags = new HashSet<WSTag>();

    private Boolean starred;

    private Integer viewCount;

    private SyncActionType syncActionType;

    private String oldBroadcastId;

    private WSMessageResponse wsMessageResponse;

    private Date updatedTime;

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public WSBroadcast(Broadcast broadcast)
    {
        this.id = broadcast.getId();
        this.messageId = broadcast.getMessageId();
        this.status = broadcast.getStatus();
        this.viewCount = broadcast.getViewCount();
        Set<BroadcastTag> tags = broadcast.getBroadcastTags();
        Set<WSTag> wsTags = new HashSet<WSTag>();
        for (BroadcastTag tag : tags)
        {
            WSLocation wsLocation = new WSLocation(tag.getLocation(), false);
            WSTag wsTag = new WSTag(tag);
            wsTag.setLocation(wsLocation);
            wsTags.add(wsTag);
        }
        this.tags = wsTags;
    }

    public WSBroadcast(Broadcast broadcast, boolean withTag)
    {
        this.id = broadcast.getId();
        this.messageId = broadcast.getMessageId();
        this.status = broadcast.getStatus();
        this.viewCount = broadcast.getViewCount();

        if (withTag)
        {
            Set<BroadcastTag> tags = broadcast.getBroadcastTags();
            Set<WSTag> wsTags = new HashSet<WSTag>();
            for (BroadcastTag tag : tags)
            {
                WSLocation wsLocation = new WSLocation(tag.getLocation(), false);
                WSTag wsTag = new WSTag(tag);
                wsTag.setLocation(wsLocation);
                wsTags.add(wsTag);
            }
            this.tags = wsTags;
        }
    }

    public WSBroadcast()
    {

    }

    public Boolean isStarred()
    {
        return starred;
    }

    public void setStarred(Boolean starred)
    {
        this.starred = starred;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public Set<WSTag> getTags()
    {
        return tags;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public void setTags(Set<WSTag> tags)
    {
        this.tags = tags;
    }

    public void addTag(WSTag tag)
    {
        this.tags.add(tag);
    }

    public BroadcastStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastStatus status)
    {
        this.status = status;
    }

    public Integer getViewCount()
    {
        return viewCount;
    }

    public void setViewCount(Integer viewCount)
    {
        this.viewCount = viewCount;
    }

    public SyncActionType getSyncActionType()
    {
        return syncActionType;
    }

    public void setSyncActionType(SyncActionType syncActionType)
    {
        this.syncActionType = syncActionType;
    }

    public String getOldBroadcastId()
    {
        return oldBroadcastId;
    }

    public void setOldBroadcastId(String oldBroadcastId)
    {
        this.oldBroadcastId = oldBroadcastId;
    }

    public WSMessageResponse getWsMessageResponse()
    {
        return wsMessageResponse;
    }

    public void setWsMessageResponse(WSMessageResponse wsMessageResponse)
    {
        this.wsMessageResponse = wsMessageResponse;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSBroadcast other = (WSBroadcast) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSBroadcast [id=" + id + ", messageId=" + messageId + ", status=" + status + ", tags=" + tags + ", starred=" + starred + ", viewCount=" + viewCount + "]";
    }

    public static WSBroadcast dummyData(String messageId)
    {
        WSBroadcast broadcast = new WSBroadcast();
        // broadcast.setCreatedBy(WSAccount.dummyData(Utils.randomString(Constant.CHARSET_ALPHABETS,
        // 5, true), Utils.randomString(Constant.CHARSET_ALPHANUMERIC, 5, true),
        // "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10,
        // true), new SecureRandom().nextBoolean()));
        broadcast.setId(Utils.randomUUID());
        broadcast.setMessageId(messageId);
        broadcast.setStatus(BroadcastStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % BroadcastStatus.values().length]);
        broadcast.setTags(new HashSet<WSTag>(Arrays.asList(WSTag.dummyData(), WSTag.dummyData(), WSTag.dummyData())));
        // broadcast.setStarred(Arrays.asList(new Boolean[]{true,
        // false}).get(Integer.parseInt(Utils.randomString(new char[]{'0', '1'},
        // 1, false))));
        broadcast.setStarred(true);
        return broadcast;
    }

    public static WSBroadcast dummyDataWithBroadcast(String messageId, String broadcastId)
    {
        WSBroadcast broadcast = new WSBroadcast();
        // broadcast.setCreatedBy(WSAccount.dummyData(Utils.randomString(Constant.CHARSET_ALPHABETS,
        // 5, true), Utils.randomString(Constant.CHARSET_ALPHANUMERIC, 5, true),
        // "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10,
        // true), new SecureRandom().nextBoolean()));
        broadcast.setId(broadcastId);
        broadcast.setMessageId(messageId);
        broadcast.setStatus(BroadcastStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % BroadcastStatus.values().length]);
        broadcast.setTags(new HashSet<WSTag>(Arrays.asList(WSTag.dummyData(), WSTag.dummyData(), WSTag.dummyData())));
        // broadcast.setStarred(Arrays.asList(new Boolean[]{true,
        // false}).get(Integer.parseInt(Utils.randomString(new char[]{'0', '1'},
        // 1, false))));
        broadcast.setStarred(true);
        return broadcast;
    }

}
