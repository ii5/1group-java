/**
 * 
 */
package one.group.entities.api.request;

import java.util.Arrays;

import one.group.core.enums.AccountFieldType;

/**
 * @author ashishthorat
 *
 */
public class WSAccountUpdate implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private AccountFieldType namespace;

    private String[] values;

    private String phoneNumber;

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public AccountFieldType getNamespace()
    {
        return namespace;
    }

    public void setNamespace(AccountFieldType namespace)
    {
        this.namespace = namespace;
    }

    public String[] getValues()
    {
        return values;
    }

    public void setValues(String[] values)
    {
        this.values = values;
    }

    @Override
    public String toString()
    {
        return "WSAccountUpdate [namespace=" + namespace + ", values=" + Arrays.toString(values) + ", phoneNumber=" + phoneNumber + "]";
    }

}
