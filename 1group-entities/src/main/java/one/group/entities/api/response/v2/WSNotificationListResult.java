package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSNotificationListResult implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	private List<WSNotification> notifications = new ArrayList<WSNotification>();

    public List<WSNotification> getNotifications()
    {
        return notifications;
    }

    public void setNotifications(List<WSNotification> notifications)
    {
        this.notifications = notifications;
    }
    
    public void addNotification(WSNotification notification)
    {
    	this.notifications.add(notification);
    }
    
    public static WSNotificationListResult dummyData()
    {
    	WSNotificationListResult result = new WSNotificationListResult();
    	for (int i = 0; i < 10 ;i++)
    	{
    		result.addNotification(WSNotification.dummyData());
    	}
    	
    	return result;
    }
    
    public static void main(String[] args)
    {
    	 String endpoint = "/notifications";
         HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
         SocketEntity entity = new SocketEntity();
         WSNotificationListResult responseData = WSNotificationListResult.dummyData();
         WSResult result = new WSResult("200", "Notifications fetched successfully.", false);
         WSResponse response = new WSResponse();
         response.setData(responseData);
         response.setResult(result);

         entity.setEndpoint(endpoint);
         entity.setRequestType(methodType);
         entity.setWithWSResponse(response);
         System.out.println(Utils.getJsonString(entity));

    }
    

}
