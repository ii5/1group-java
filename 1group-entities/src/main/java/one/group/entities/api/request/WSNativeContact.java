package one.group.entities.api.request;

import java.util.List;

public class WSNativeContact implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String fullName;

    private String companyName;

    private List<String> numbers;

    public WSNativeContact()
    {
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public List<String> getNumbers()
    {
        return numbers;
    }

    public void setNumbers(List<String> numbers)
    {
        this.numbers = numbers;
    }

    @Override
    public String toString()
    {
        return "WSNativeContact [fullName=" + fullName + ", companyName=" + companyName + ", numbers=" + numbers + "]";
    }

}
