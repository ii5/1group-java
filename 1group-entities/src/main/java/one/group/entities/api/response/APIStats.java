package one.group.entities.api.response;

import java.util.Map;

public class APIStats
{
    private long totalTimesCalled;
    private long totalTimeSpentProcessing;
    private long totalClientErrors;
    private long totalServerErrors;
    private long maxTimeSpentProcessing;
    private Map<String, String> additionalData;

    public long getTotalTimesCalled()
    {
        return totalTimesCalled;
    }

    public void setTotalTimesCalled(long totalTimesCalled)
    {
        this.totalTimesCalled = totalTimesCalled;
    }

    public long getTotalTimeSpentProcessing()
    {
        return totalTimeSpentProcessing;
    }

    public void setTotalTimeSpentProcessing(long totalTimeSpentProcessing)
    {
        this.totalTimeSpentProcessing = totalTimeSpentProcessing;
    }

    public long getTotalClientErrors()
    {
        return totalClientErrors;
    }

    public void setTotalClientErrors(long totalClientErrors)
    {
        this.totalClientErrors = totalClientErrors;
    }

    public long getTotalServerErrors()
    {
        return totalServerErrors;
    }

    public void setTotalServerErrors(long totalServerErrors)
    {
        this.totalServerErrors = totalServerErrors;
    }

    public long getMaxTimeSpentProcessing()
    {
        return maxTimeSpentProcessing;
    }

    public void setMaxTimeSpentProcessing(long maxTimeSpentProcessing)
    {
        this.maxTimeSpentProcessing = maxTimeSpentProcessing;
    }

    public Map<String, String> getAdditionalData()
    {
        return additionalData;
    }

    public void setAdditionalData(Map<String, String> additionalData)
    {
        this.additionalData = additionalData;
    }

}
