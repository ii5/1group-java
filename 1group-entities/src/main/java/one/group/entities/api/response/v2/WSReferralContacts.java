package one.group.entities.api.response.v2;

import java.util.List;

import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.api.response.ResponseEntity;

public class WSReferralContacts implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	private boolean syncFinished;
    private Long registeredUserCount;
    private List<WSAccount> registeredContactsList;
    private List<WSNativeContact> contactsList;

    private WSPaginationData pagination;

    public boolean isSyncFinished()
    {
        return syncFinished;
    }

    public void setSyncFinished(boolean syncFinished)
    {
        this.syncFinished = syncFinished;
    }

    public Long getRegisteredUserCount()
    {
        return registeredUserCount;
    }

    public void setRegisteredUserCount(Long registeredUserCount)
    {
        this.registeredUserCount = registeredUserCount;
    }

    public List<WSAccount> getRegisteredContactsList()
    {
        return registeredContactsList;
    }

    public void setRegisteredContactsList(List<WSAccount> registeredContactsList)
    {
        this.registeredContactsList = registeredContactsList;
    }

    public List<WSNativeContact> getContactsList()
    {
        return contactsList;
    }

    public void setContactsList(List<WSNativeContact> contactsList)
    {
        this.contactsList = contactsList;
    }

    public WSPaginationData getPagination()
    {
        return pagination;
    }

    public void setPagination(WSPaginationData pagination)
    {
        this.pagination = pagination;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((contactsList == null) ? 0 : contactsList.hashCode());
        result = prime * result + ((registeredContactsList == null) ? 0 : registeredContactsList.hashCode());
        result = prime * result + registeredUserCount.intValue();
        result = prime * result + (syncFinished ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSReferralContacts other = (WSReferralContacts) obj;
        if (contactsList == null)
        {
            if (other.contactsList != null)
                return false;
        }
        else if (!contactsList.equals(other.contactsList))
            return false;
        if (registeredContactsList == null)
        {
            if (other.registeredContactsList != null)
                return false;
        }
        else if (!registeredContactsList.equals(other.registeredContactsList))
            return false;
        if (registeredUserCount != other.registeredUserCount)
            return false;
        if (syncFinished != other.syncFinished)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSReferralContacts [syncFinished=" + syncFinished + ", registeredUserCount=" + registeredUserCount + ", registeredContactsList=" + registeredContactsList + ", contactsList="
                + contactsList + "]";
    }

}
