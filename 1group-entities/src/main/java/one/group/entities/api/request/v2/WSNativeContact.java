package one.group.entities.api.request.v2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import one.group.core.Constant;
import one.group.entities.api.request.RequestEntity;
import one.group.utils.Utils;

public class WSNativeContact implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    public static final String QUERY_FIND_NUMBERS_BY_ACCOUNT_ID = "SELECT NEW one.group.entities.api.request.v2.WSNativeContact(a.fullName, a.companyName, group_concat(a.phoneNumber)) FROM NativeContacts a where a.account.id = :accountId and a.phoneNumber not in (:phoneNumbers) and REGEXP(a.phoneNumber, '\\\\+91[[:digit:]]{10}$') <> 0 group by fullName ORDER BY a.fullName ASC";

    private String fullName;

    private String companyName;

    private List<String> numbers;

    public WSNativeContact()
    {
    }

    public WSNativeContact(String fullName, String companyName, String numbersStr)
    {
        this.fullName = fullName;
        this.companyName = companyName;
        this.numbers = new ArrayList<String>();
        if (numbersStr != null)
        {
            for (String number : numbersStr.split(","))
            {
                numbers.add(number);
            }
        }

    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public List<String> getNumbers()
    {
        return numbers;
    }

    public void setNumbers(List<String> numbers)
    {
        this.numbers = numbers;
    }

    @Override
    public String toString()
    {
        return "WSNativeContact [fullName=" + fullName + ", companyName=" + companyName + ", numbers=" + numbers + "]";
    }

    public static WSNativeContact dummyData()
    {
        WSNativeContact result = new WSNativeContact();
        result.setCompanyName("Company Name: " + Utils.generateAccountReference());
        result.setFullName("Full Name : " + Utils.generateAccountReference());
        result.setNumbers(Arrays.asList("+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true)));

        return result;

    }

}
