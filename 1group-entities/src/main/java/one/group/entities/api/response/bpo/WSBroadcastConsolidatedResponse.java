package one.group.entities.api.response.bpo;

import java.util.Date;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSBroadcastConsolidatedResponse implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String title;

    private Long size;

    private Long maxSize;

    private Long minSize;

    private PropertyTransactionType transactionType;

    private BroadcastType broadcastType;

    private CommissionType commissionType;

    private String customSublocation;

    private Long maxPrice;

    private Long minPrice;

    private PropertyMarket propertyMarket;

    private PropertyType propertyType;

    private PropertySubType propertySubType;

    private Rooms rooms;

    private String locationId;

    private String cityId;

    private WSLocation location;

    private Long price;

    private String messageId;

    private String broadcastId;

    private String broadcastCreatedBy;

    private String broadcastUpdatedBy;

    private String broadcastCreatedTime;

    private String broadcastUpdatedTime;

    private String tagCreatedBy;

    private String tagUpdatedBy;

    private String tagCreatedTime;

    private String tagUpdatedTime;

    private String locationName;

    private String cityName;

    private BroadcastTagStatus status;

    private String draftedBy;

    private Date draftedTime;

    private String addedBy;

    private Date addedTime;

    private String id;

    private String text;

    private String groupId;

    private String groupName;

    private String groupCityName;

    private String locationDisplayName;

    private String createdBy;

    private Date createdTime;

    private String approvedBy;

    private Date approvedTime;

    private String closedBy;

    private Date closedTime;

    private String discardReason;

    private String shortReference;

    public WSBroadcastConsolidatedResponse(BroadcastTag tag)
    {
        this.title = tag.getBroadcastType() + "-" + tag.getTransactionType() + "-" + tag.getPrice() + "-" + tag.getSize();// TODO
        this.size = tag.getSize();
        this.transactionType = tag.getTransactionType();
        this.broadcastType = tag.getBroadcastType();
        this.maxPrice = tag.getMaximumPrice();
        this.minPrice = tag.getMinimumPrice();
        this.propertyType = tag.getPropertyType();
        this.rooms = tag.getRooms();
        this.price = tag.getPrice();
        this.status = tag.getStatus();
        this.draftedBy = tag.getDraftedBy();
        this.draftedTime = tag.getDraftedTime();
        this.addedBy = tag.getAddedBy();
        this.addedTime = tag.getAddedTime();
        this.status = tag.getStatus();
        this.draftedBy = tag.getDraftedBy();
        this.draftedTime = tag.getDraftedTime();
        this.maxSize = tag.getMaximumSize();
        this.minSize = tag.getMinimumSize();
        if (tag.getPropertySubType() != null)
        {
            this.propertySubType = tag.getPropertySubType();
        }
        this.tagCreatedBy = tag.getCreatedBy();
        this.tagCreatedTime = tag.getCreatedTime() == null ? null : tag.getCreatedTime().getTime() + "";
        this.closedBy = tag.getClosedBy();
        this.closedTime = tag.getClosedTime();
        this.discardReason = tag.getDiscardReason() != null ? tag.getDiscardReason() : null;
    }

    public WSBroadcastConsolidatedResponse()
    {

    }

    public WSBroadcastConsolidatedResponse(BroadcastTagsConsolidated b)
    {
        this.broadcastCreatedBy = b.getBroadcastCreatedBy();
        this.broadcastCreatedTime = (b.getBroadcastCreatedTime() == null) ? null : b.getBroadcastCreatedTime().getTime() + "";
        this.broadcastId = b.getBroadcastId();
        this.broadcastType = b.getBroadcastType();
        this.broadcastUpdatedBy = b.getBroadcastUpdatedBy();
        this.broadcastUpdatedTime = (b.getBroadcastUpdatedTime() == null) ? this.broadcastCreatedTime : b.getBroadcastUpdatedTime().getTime() + "";
        this.cityId = b.getCityId();
        this.cityName = b.getCityName();
        this.commissionType = b.getCommissionType();
        this.customSublocation = b.getCustomSublocation();
        this.locationId = b.getLocationId();
        this.locationName = b.getLocationName();
        this.locationDisplayName = b.getLocationDisplayName();
        this.maxPrice = b.getMaximumPrice();
        this.minPrice = b.getMinimumPrice();
        this.messageId = b.getMessageId();
        this.price = b.getPrice();
        this.propertyType = b.getPropertyType();
        this.rooms = b.getRooms();
        this.size = b.getSize();
        this.tagCreatedBy = b.getTagCreatedBy();
        this.tagCreatedTime = b.getTagCreatedTime().getTime() + "";
        this.tagUpdatedBy = b.getTagUpdatedBy();
        this.tagUpdatedTime = (b.getTagUpdatedTime() == null) ? this.tagCreatedTime : b.getTagUpdatedTime().getTime() + "";
        this.title = b.getTitle();
        this.transactionType = b.getTransactionType();
        this.status = b.getStatus();
        this.draftedBy = b.getDraftedBy();
        this.draftedTime = b.getDraftedTime();
        this.addedBy = b.getAddedBy();
        this.addedTime = b.getAddedTime();
        this.id = b.getId();
        this.maxSize = b.getMaximumSize();
        this.minSize = b.getMinimumSize();
        this.closedBy = b.getClosedBy();
        this.closedTime = b.getClosedTime();

        if (b.getPropertySubType() != null)
        {
            this.propertySubType = b.getPropertySubType();
        }
        this.discardReason = b.getDiscardReason() != null ? b.getDiscardReason() : null;

        this.shortReference = b.getId();
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getApprovedBy()
    {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy)
    {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedTime()
    {
        return approvedTime;
    }

    public void setApprovedTime(Date approvedTime)
    {
        this.approvedTime = approvedTime;
    }

    public String getClosedBy()
    {
        return closedBy;
    }

    public void setClosedBy(String closedBy)
    {
        this.closedBy = closedBy;
    }

    public Date getClosedTime()
    {
        return closedTime;
    }

    public void setClosedTime(Date closedTime)
    {
        this.closedTime = closedTime;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Long getMaxSize()
    {
        return maxSize;
    }

    public void setMaxSize(Long maxSize)
    {
        this.maxSize = maxSize;
    }

    public Long getMinSize()
    {
        return minSize;
    }

    public void setMinSize(Long minSize)
    {
        this.minSize = minSize;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public BroadcastTagStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastTagStatus status)
    {
        this.status = status;
    }

    public String getDraftedBy()
    {
        return draftedBy;
    }

    public void setDraftedBy(String draftedBy)
    {
        this.draftedBy = draftedBy;
    }

    public Date getDraftedTime()
    {
        return draftedTime;
    }

    public void setDraftedTime(Date draftedTime)
    {
        this.draftedTime = draftedTime;
    }

    public Date getAddedTime()
    {
        return addedTime;
    }

    public void setAddedTime(Date addedTime)
    {
        this.addedTime = addedTime;
    }

    public String getAddedBy()
    {
        return addedBy;
    }

    public void setAddedBy(String addedBy)
    {
        this.addedBy = addedBy;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getSize()
    {
        return size;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public PropertyTransactionType getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(PropertyTransactionType transactionType)
    {
        this.transactionType = transactionType;
    }

    public BroadcastType getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(BroadcastType broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public Long getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public Long getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(Long minPrice)
    {
        this.minPrice = minPrice;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public Rooms getRooms()
    {
        return rooms;
    }

    public void setRooms(Rooms rooms)
    {
        this.rooms = rooms;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public WSLocation getLocation()
    {
        return location;
    }

    public void setLocation(WSLocation location)
    {
        this.location = location;
    }

    public Long getPrice()
    {
        return price;
    }

    public void setPrice(Long price)
    {
        this.price = price;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getBroadcastCreatedBy()
    {
        return broadcastCreatedBy;
    }

    public void setBroadcastCreatedBy(String broadcastCreatedBy)
    {
        this.broadcastCreatedBy = broadcastCreatedBy;
    }

    public String getBroadcastUpdatedBy()
    {
        return broadcastUpdatedBy;
    }

    public void setBroadcastUpdatedBy(String broadcastUpdatedBy)
    {
        this.broadcastUpdatedBy = broadcastUpdatedBy;
    }

    public String getBroadcastCreatedTime()
    {
        return broadcastCreatedTime;
    }

    public void setBroadcastCreatedTime(String broadcastCreatedTime)
    {
        this.broadcastCreatedTime = broadcastCreatedTime;
    }

    public String getBroadcastUpdatedTime()
    {
        return broadcastUpdatedTime;
    }

    public void setBroadcastUpdatedTime(String broadcastUpdatedTime)
    {
        this.broadcastUpdatedTime = broadcastUpdatedTime;
    }

    public String getTagCreatedBy()
    {
        return tagCreatedBy;
    }

    public void setTagCreatedBy(String tagCreatedBy)
    {
        this.tagCreatedBy = tagCreatedBy;
    }

    public String getTagUpdatedBy()
    {
        return tagUpdatedBy;
    }

    public void setTagUpdatedBy(String tagUpdatedBy)
    {
        this.tagUpdatedBy = tagUpdatedBy;
    }

    public String getTagCreatedTime()
    {
        return tagCreatedTime;
    }

    public void setTagCreatedTime(String tagCreatedTime)
    {
        this.tagCreatedTime = tagCreatedTime;
    }

    public String getTagUpdatedTime()
    {
        return tagUpdatedTime;
    }

    public void setTagUpdatedTime(String tagUpdatedTime)
    {
        this.tagUpdatedTime = tagUpdatedTime;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupCityName()
    {
        return groupCityName;
    }

    public void setGroupCityName(String groupCityName)
    {
        this.groupCityName = groupCityName;
    }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    public String getDiscardReason()
    {
        return discardReason;
    }

    public void setDiscardReason(String discardReason)
    {
        this.discardReason = discardReason;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((broadcastType == null) ? 0 : broadcastType.hashCode());
        result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
        result = prime * result + ((commissionType == null) ? 0 : commissionType.hashCode());
        result = prime * result + ((customSublocation == null) ? 0 : customSublocation.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + (int) (maxPrice ^ (maxPrice >>> 32));
        result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
        result = prime * result + (int) (minPrice ^ (minPrice >>> 32));
        result = prime * result + (int) (price ^ (price >>> 32));
        result = prime * result + ((propertyMarket == null) ? 0 : propertyMarket.hashCode());
        result = prime * result + ((propertySubType == null) ? 0 : propertySubType.hashCode());
        result = prime * result + ((propertyType == null) ? 0 : propertyType.hashCode());
        result = prime * result + ((rooms == null) ? 0 : rooms.hashCode());
        result = prime * result + (int) (size ^ (size >>> 32));
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSBroadcastConsolidatedResponse other = (WSBroadcastConsolidatedResponse) obj;
        if (broadcastType != other.broadcastType)
            return false;
        if (cityId == null)
        {
            if (other.cityId != null)
                return false;
        }
        else if (!cityId.equals(other.cityId))
            return false;
        if (commissionType != other.commissionType)
            return false;
        if (customSublocation == null)
        {
            if (other.customSublocation != null)
                return false;
        }
        else if (!customSublocation.equals(other.customSublocation))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        if (maxPrice != other.maxPrice)
            return false;
        if (messageId == null)
        {
            if (other.messageId != null)
                return false;
        }
        else if (!messageId.equals(other.messageId))
            return false;
        if (minPrice != other.minPrice)
            return false;
        if (price != other.price)
            return false;
        if (propertyMarket != other.propertyMarket)
            return false;
        if (propertySubType != other.propertySubType)
            return false;
        if (propertyType != other.propertyType)
            return false;
        if (rooms != other.rooms)
            return false;
        if (size != other.size)
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        if (transactionType != other.transactionType)
            return false;
        return true;
    }

}
