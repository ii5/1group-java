package one.group.entities.api.response;

import java.util.Date;

import one.group.entities.jpa.ClientAnalytics;


public class WSClientAnalyticsResponse implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// private int id;
    private String action;

    private Date clientEventTime;

    private String context;

    private String entityId;

    private String entityType;

    public WSClientAnalyticsResponse(ClientAnalytics clientAnalytics)
    {
        // this.id = clientAnalytics.getId();
        this.context = clientAnalytics.getContext();
        this.action = clientAnalytics.getAction();
        this.entityId = clientAnalytics.getEntityId();
        this.entityType = clientAnalytics.getEntityType();
        this.clientEventTime = clientAnalytics.getClientEventTime();
    }

    // public String getId()
    // {
    // return id;
    // }
    //
    // public void setId(String id)
    // {
    // this.id = id;
    // }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public Date getClientEventTime()
    {
        return clientEventTime;
    }

    public void setClientEventTime(Date clientEventTime)
    {
        this.clientEventTime = clientEventTime;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((clientEventTime == null) ? 0 : clientEventTime.hashCode());
        result = prime * result + ((context == null) ? 0 : context.hashCode());
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSClientAnalyticsResponse other = (WSClientAnalyticsResponse) obj;
        if (action == null)
        {
            if (other.action != null)
                return false;
        }
        else if (!action.equals(other.action))
            return false;
        if (clientEventTime == null)
        {
            if (other.clientEventTime != null)
                return false;
        }
        else if (!clientEventTime.equals(other.clientEventTime))
            return false;
        if (context == null)
        {
            if (other.context != null)
                return false;
        }
        else if (!context.equals(other.context))
            return false;
        if (entityId == null)
        {
            if (other.entityId != null)
                return false;
        }
        else if (!entityId.equals(other.entityId))
            return false;
        if (entityType == null)
        {
            if (other.entityType != null)
                return false;
        }
        else if (!entityType.equals(other.entityType))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSClientAnalyticsResponse [action=" + action + ", clientEventTime=" + clientEventTime + ", context=" + context + ", entityId=" + entityId + ", entityType=" + entityType + "]";
    }

}
