package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSSearch implements RequestEntity
{
    private WSBroadcastSearchQueryRequest search;

    public WSSearch()
    {
    }

    public WSSearch(WSBroadcastSearchQueryRequest search)
    {
        Validation.isTrue(!Utils.isNull(search), "Passed search should not be null");

        this.search = search;
    }

    public WSBroadcastSearchQueryRequest getSearch()
    {
        return search;
    }

    public void setSearch(WSBroadcastSearchQueryRequest search)
    {
        this.search = search;
    }

    @Override
    public String toString()
    {
        return "WSSearch [search=" + search + "]";
    }
}
