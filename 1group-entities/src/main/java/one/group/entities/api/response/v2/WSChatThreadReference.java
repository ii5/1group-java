package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSChatThreadReference implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WSChatThread chatThread;

    private Integer chatThreadIndex;

    public WSChatThreadReference()
    {

    }

    public WSChatThreadReference(WSChatThread chatThread, Integer chatThreadIndex)
    {
        Validation.isTrue(!Utils.isNull(chatThread), "Chat thread passed should not be null");
        Validation.isTrue(chatThreadIndex >= -1, "Chat thread index passed should not be less than 0");

        this.chatThread = chatThread;
        this.chatThreadIndex = chatThreadIndex;
    }

    public WSChatThread getChatThread()
    {
        return chatThread;
    }

    public Integer getChatThreadIndex()
    {
        return chatThreadIndex;
    }

    public void setChatThread(WSChatThread chatThread)
    {
        this.chatThread = chatThread;
    }

    public void setChatThreadIndex(Integer chatThreadIndex)
    {
        this.chatThreadIndex = chatThreadIndex;
    }

    @Override
    public String toString()
    {
        return "WSChatThreadReference [chatThread = " + chatThread + ", chatThreadIndex=" + chatThreadIndex + "]";
    }

    public static WSChatThreadReference dummyData(int numberOfMessages, int maxChatMessageIndex, WSAccount createdBy, WSAccount toAccount, Integer unreadMessageCount, Integer myBroadcastCount)
    {
        WSChatThread ct = WSChatThread.dummyData(numberOfMessages, maxChatMessageIndex, createdBy, toAccount, unreadMessageCount, myBroadcastCount, "" + System.currentTimeMillis());
        WSChatThreadReference ref = new WSChatThreadReference(ct, maxChatMessageIndex + Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 1, true)));

        return ref;
    }

    public static WSChatThreadReference dummyData(int numberOfMessages, int maxChatMessageIndex, WSAccount createdBy, WSAccount toAccount, Integer unreadMessageCount, Integer myBroadcastCount,
            MessageType[] messageTypeArray)
    {
        WSChatThread ct = WSChatThread.dummyDataWithType(numberOfMessages, maxChatMessageIndex, createdBy, toAccount, unreadMessageCount, myBroadcastCount, "" + System.currentTimeMillis(),
                messageTypeArray);
        WSChatThreadReference ref = new WSChatThreadReference(ct, maxChatMessageIndex + Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 1, true)));

        return ref;
    }
}
