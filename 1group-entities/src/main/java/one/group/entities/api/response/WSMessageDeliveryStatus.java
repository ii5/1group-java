package one.group.entities.api.response;

public class WSMessageDeliveryStatus implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountId;

    private String readTime;

    private String receivedTime;

    private int status = 0;

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getReadTime()
    {
        return readTime;
    }

    public void setReadTime(String readTime)
    {
        this.readTime = readTime;
    }

    public String getReceivedTime()
    {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime)
    {
        this.receivedTime = receivedTime;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "WSMessageDeliveryStatus [accountId=" + accountId + ", readTime=" + readTime + ", receivedTime=" + receivedTime + ", status=" + status + "]";
    }

}
