package one.group.entities.api.request.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSContacts implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private List<WSNativeContact> nativeContacts = new ArrayList<WSNativeContact>();

    private boolean initialContactsSyncComplete;

    public boolean isInitialContactsSyncComplete()
    {
        return initialContactsSyncComplete;
    }

    public void setInitialContactsSyncComplete(boolean initialContactsSyncComplete)
    {
        this.initialContactsSyncComplete = initialContactsSyncComplete;
    }

    public List<WSNativeContact> getNativeContacts()
    {
        return nativeContacts;
    }

    public void setNativeContacts(List<WSNativeContact> nativeContacts)
    {
        this.nativeContacts = nativeContacts;
    }
    
    public void addNativeContact(WSNativeContact nativeContact)
    {
    	this.nativeContacts.add(nativeContact);
    }
    
    public static WSContacts dummyData()
    {
    	WSContacts result = new WSContacts();
    	
    	for (int i = 0; i < 100; i++)
    	{
    		result.addNativeContact(WSNativeContact.dummyData());
    	}
    	
    	return result;
    }
    
    public static void main(String[] args)
    {
    	 String endpoint = "/contacts/sync";
         HTTPRequestMethodType methodType = HTTPRequestMethodType.POST;
         SocketEntity entity = new SocketEntity();
         
         WSContacts requestData = WSContacts.dummyData();
         WSRequest request = new WSRequest();
         request.setContacts(requestData);
//         WSChatThreadListResult responseData = WSChatThreadListResult.dummyData(10, MessageSourceType.ONEGROUP_DIRECT);
         WSResult result = new WSResult("200", "Constacts synced successfully!.", false);
         WSResponse response = new WSResponse();
//         response.setData(responseData);
         response.setResult(result);

         entity.setEndpoint(endpoint);
         entity.setRequestType(methodType);
         entity.setWithWSResponse(response);
         entity.setWithWSRequest(request);
         System.out.println(Utils.getJsonString(entity));

    }

}
