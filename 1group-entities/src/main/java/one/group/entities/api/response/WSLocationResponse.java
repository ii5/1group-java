package one.group.entities.api.response;

import java.util.Set;

/**
 * 
 * @author
 *
 */
public class WSLocationResponse implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<WSLocation> locations;

    public Set<WSLocation> getLocations()
    {
        return locations;
    }

    public void setLocations(Set<WSLocation> locations)
    {
        this.locations = locations;
    }

}
