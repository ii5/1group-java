package one.group.entities.api.request;

public class WSBroadcastStatusRequest
{
    private String status;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

}
