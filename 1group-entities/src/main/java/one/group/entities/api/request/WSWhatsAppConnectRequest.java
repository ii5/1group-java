package one.group.entities.api.request;

public class WSWhatsAppConnectRequest
{
    private String phoneNumber;

    public WSWhatsAppConnectRequest()
    {

    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSWhatsAppConnectRequest other = (WSWhatsAppConnectRequest) obj;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSWhatsAppConnectRequest [phoneNumber=" + phoneNumber + "]";
    }

}
