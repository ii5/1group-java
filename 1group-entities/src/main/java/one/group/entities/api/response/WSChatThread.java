package one.group.entities.api.response;

import java.util.List;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatThread implements ResponseEntity
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String chatThreadId;

    private Integer maxChatMessageIndex;

    private List<WSChatMessage> chatMessages;

    private List<WSParticipants> participants;

    public WSChatThread()
    {
    }

    public WSChatThread(String chatThreadId, Integer maxChatMessageIndex, List<WSChatMessage> chatMessages, List<WSParticipants> participants)
    {
        this.chatThreadId = chatThreadId;
        this.maxChatMessageIndex = maxChatMessageIndex;
        this.chatMessages = chatMessages;
        this.participants = participants;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public Integer getMaxChatMessageIndex()
    {
        return maxChatMessageIndex;
    }

    public void setMaxChatMessageIndex(Integer maxChatMessageIndex)
    {
        this.maxChatMessageIndex = maxChatMessageIndex;
    }

    public List<WSChatMessage> getChatMessages()
    {
        return chatMessages;
    }

    public void setChatMessages(List<WSChatMessage> chatMessages)
    {
        this.chatMessages = chatMessages;
    }

    public List<WSParticipants> getParticipants()
    {
        return participants;
    }

    public void setParticipants(List<WSParticipants> participants)
    {
        this.participants = participants;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatThread other = (WSChatThread) obj;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatThread [chatThreadId=" + chatThreadId + ", maxChatMessageIndex=" + maxChatMessageIndex + ", chatMessages=" + chatMessages + ", participants=" + participants + "]";
    }

}
