package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.core.enums.OAuthScope;
import one.group.core.enums.OAuthTokenType;
import one.group.core.enums.status.DeviceStatus;
import one.group.core.enums.status.Status;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * API Schema for Client
 * 
 * @author bhumikabhatt
 * 
 */

public class WSClient implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonIgnore
    private String id;
    @JsonIgnore
    private DeviceStatus status;

    private int initialSyncIndex = Constant.MIN_SYNC_INDEX;
    private String pushChannel;
    private String pushKey;
    private String accessToken;
    private String accountId;
    private OAuthScope scope;
    private OAuthTokenType tokenType;
    private String clientType;
    private Status accountStatus;
    private String cityId;
    private String cityName;

    public WSClient()
    {
    };

    public WSClient(Client client, int index, String accessToken, String accountId, OAuthScope scope, OAuthTokenType tokenType)
    {
        this.id = client.getIdAsString();
        this.pushChannel = client.getPushChannel();
        this.status = client.getStatus();
        this.initialSyncIndex = index;
        this.accessToken = accessToken;
        this.accountId = accountId;
        this.scope = scope;
        this.tokenType = tokenType;
        this.clientType = client.getDevicePlatform();
    }

    public WSClient(Client client)
    {
        this.id = client.getIdAsString();
        this.pushChannel = client.getPushChannel();
        this.status = client.getStatus();
    }

    @JsonIgnore
    public String getId()
    {
        return id;
    }

    @JsonProperty
    public void setId(String id)
    {
        this.id = id;
    }

    @JsonIgnore
    public DeviceStatus getStatus()
    {
        return status;
    }

    @JsonProperty
    public void setStatus(DeviceStatus status)
    {
        this.status = status;
    }

    public int getInitialSyncIndex()
    {
        return initialSyncIndex;
    }

    public void setInitialSyncIndex(int initialSyncIndex)
    {
        this.initialSyncIndex = initialSyncIndex;
    }

    public String getPushChannel()
    {
        return pushChannel;
    }

    public void setPushChannel(String pushChannel)
    {
        this.pushChannel = pushChannel;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public OAuthScope getScope()
    {
        return scope;
    }

    public void setScope(OAuthScope scope)
    {
        this.scope = scope;
    }

    public OAuthTokenType getTokenType()
    {
        return tokenType;
    }

    public void setTokenType(OAuthTokenType tokenType)
    {
        this.tokenType = tokenType;
    }

    public String getPushKey()
    {
        return pushKey;
    }

    public void setPushKey(String pushKey)
    {
        this.pushKey = pushKey;
    }

    public String getClientType()
    {
        return clientType;
    }

    public void setClientType(String clientType)
    {
        this.clientType = clientType;
    }

    public Status getAccountStatus()
    {
        return accountStatus;
    }

    public void setAccountStatus(Status accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSClient other = (WSClient) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSClient [id= " + id + ", status=" + status + ", initialSyncIndex=" + initialSyncIndex + ", pushChannel=" + pushChannel + ", pushKey=" + pushKey + ", accessToken=" + accessToken
                + ", accountId=" + accountId + ", scope=" + scope + ", tokenType=" + tokenType + ", clientType=" + clientType + ", accountStatus=" + accountStatus + ", cityId=" + cityId
                + ", cityName=" + cityName + "]";
    }

}
