package one.group.entities.api.response;

import one.group.core.enums.EntityType;

public class WSEntityCount
{
    private EntityType namespace;
    private String count;

    public EntityType getNamespace()
    {
        return namespace;
    }

    public void setNamespace(EntityType namespace)
    {
        this.namespace = namespace;
    }

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

}
