package one.group.entities.api.request;

public class WSAdminPropertyListing implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String createdFor;

    private String externalListingTime;

    private String externalSource;

    public WSAdminPropertyListing()
    {

    }

    public String getCreatedFor()
    {
        return createdFor;
    }

    public String getExternalListingTime()
    {
        return externalListingTime;
    }

    public String getExternalSource()
    {
        return externalSource;
    }

    public void setCreatedFor(String createdFor)
    {
        this.createdFor = createdFor;
    }

    public void setExternalListingTime(String externalListingTime)
    {
        this.externalListingTime = externalListingTime;
    }

    public void setExternalSource(String externalSource)
    {
        this.externalSource = externalSource;
    }

    @Override
    public String toString()
    {
        return "WSAdminPropertyListing [createdFor=" + createdFor + ", externalListingTime=" + externalListingTime + ", externalSource=" + externalSource + "]";
    }
}
