package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Otp;

public class WSOtp implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	private String mobileNumber;

    private String otp;

    public WSOtp()
    {
    }

    public WSOtp(Otp otp)
    {
        this.mobileNumber = otp.getPhoneNumber().getNumber();
        this.otp = otp.getOtp();
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public String getOtp()
    {
        return otp;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public void setOtp(String otp)
    {
        this.otp = otp;
    }

}
