package one.group.entities.api.response;

import one.group.entities.jpa.MarketingList;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSMarketingList implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String phoneNumber;

    private String dataSource;

    private int waGroupCount;

    public WSMarketingList()
    {

    }

    public WSMarketingList(MarketingList marketingList)
    {
        Validation.notNull(marketingList, "Marketing list should not be null");

        this.phoneNumber = marketingList.getPhoneNumber();
        this.dataSource = marketingList.getDataSource();
        this.waGroupCount = marketingList.getWsGroupCount();
    }

    public WSMarketingList(String phoneNumber)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(phoneNumber), "Phone number should not be null or empty");

        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(String dataSource)
    {
        this.dataSource = dataSource;
    }

    public int getWaGroupCount()
    {
        return waGroupCount;
    }

    public void setWaGroupCount(int waGroupCount)
    {
        this.waGroupCount = waGroupCount;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSMarketingList other = (WSMarketingList) obj;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSMarketingList [phoneNumber=" + phoneNumber + ", dataSource=" + dataSource + ", waGroupCount=" + waGroupCount + "]";
    }
}
