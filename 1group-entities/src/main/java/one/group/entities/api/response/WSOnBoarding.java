package one.group.entities.api.response;

/**
 * 
 * @author sanilshet
 *
 */
public class WSOnBoarding
{
    private String totalGroups;
    
    private String totalAccounts;
    
    private String totalPropertyListings;

    private String inviteCode;

    public WSOnBoarding()
    {

    }
    
    public String getTotalAccounts()
    {
        return totalAccounts;
    }
    
    public void setTotalAccounts(String totalAccounts)
    {
        this.totalAccounts = totalAccounts;
    }
    
    public String getTotalPropertyListings()
    {
        return totalPropertyListings;
    }
    
    public void setTotalPropertyListings(String totalPropertyListings)
    {
        this.totalPropertyListings = totalPropertyListings;
    }
    
    public String getInviteCode()
    {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode)
    {
        this.inviteCode = inviteCode;
    }

    public String getTotalGroups()
    {
        return totalGroups;
    }

    public void setTotalGroups(String totalGroups)
    {
        this.totalGroups = totalGroups;
    }

    @Override
    public String toString()
    {
        return "WSOnBoarding [totalGroups=" + totalGroups + ", totalAccounts=" + totalAccounts + ", totalPropertyListings=" + totalPropertyListings + ", inviteCode=" + inviteCode + "]";
    }

  

}
