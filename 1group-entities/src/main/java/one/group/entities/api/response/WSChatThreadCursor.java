package one.group.entities.api.response;

import one.group.entities.api.request.RequestEntity;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatThreadCursor implements ResponseEntity, RequestEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(WSChatThreadCursor.class);

    @APIRequired
    private Integer readIndex;

    @APIRequired
    private Integer receivedIndex;

    public WSChatThreadCursor()
    {
    }

    public WSChatThreadCursor(final Integer readIndex, final Integer receivedIndex)
    {
        Validation.isTrue(!(readIndex < -2), "Invalid value of readIndex. [" + readIndex + "]");
        // Validation.isTrue(!(readIndex > receivedIndex),
        // "Invalid value of receivedIndex. [" + readIndex + "]");
        Validation.isTrue(!(receivedIndex < -2), "Invalid value of receivedIndex. [" + receivedIndex + "]");

        this.readIndex = readIndex;
        this.receivedIndex = receivedIndex;
    }

    public Integer getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(Integer readIndex)
    {
        this.readIndex = readIndex;
    }

    public Integer getReceivedIndex()
    {
        return receivedIndex;
    }

    public void setReceivedIndex(Integer receivedIndex)
    {
        this.receivedIndex = receivedIndex;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((readIndex == null) ? 0 : readIndex.hashCode());
        result = prime * result + ((receivedIndex == null) ? 0 : receivedIndex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatThreadCursor other = (WSChatThreadCursor) obj;
        if (readIndex == null)
        {
            if (other.readIndex != null)
                return false;
        }
        else if (!readIndex.equals(other.readIndex))
            return false;
        if (receivedIndex == null)
        {
            if (other.receivedIndex != null)
                return false;
        }
        else if (!receivedIndex.equals(other.receivedIndex))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatThreadCursor [read_index=" + readIndex + ", received_index=" + receivedIndex + "]";
    }
}
