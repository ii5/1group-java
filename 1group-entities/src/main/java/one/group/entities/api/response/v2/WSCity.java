package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.City;
import one.group.utils.Utils;

public class WSCity implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private List<WSLocation> locations;

    public WSCity()
    {
    };

    public WSCity(City city)
    {
        this.setId(city.getIdAsString());
        this.setName(city.getName());
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public List<WSLocation> getLocations()
    {
        return locations;
    }

    public void setLocations(List<WSLocation> locations)
    {
        this.locations = locations;
    }
    
    public void addLocation(WSLocation location)
    {
        if (location == null)
        {
            return;
        }
        
        if (locations == null)
        {
            locations = new ArrayList<WSLocation>();
        }
        
        locations.add(location);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "WSCity [id=" + id + ", name=" + name + "]";
    }

    
    public static WSCity dummyData(String name)
    {
        WSCity city = new WSCity();
        city.setId(Utils.randomUUID());
        city.setName(name);
        
        return city;
    }
    
    public static WSCity dummyData(String name, int numOfLocations)
    {
        WSCity city = new WSCity();
        city.setId(Utils.randomUUID());
        city.setName(name);
        
        for (int i = 0; i < numOfLocations; i++)
        {
            city.addLocation(WSLocation.dummyData(city));
        }
        
        return city;
    }
}
