package one.group.entities.api.response;

import one.group.core.enums.status.AccountOnlineStatus;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSAccountStatus implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountId;

    private AccountOnlineStatus status;

    private String lastSeenTime;

    public WSAccountStatus() {}
    
    public WSAccountStatus(String accountId, AccountOnlineStatus status, long lastSeenTime)
    {
        this.accountId = accountId;
        this.status = status;
        this.lastSeenTime = String.valueOf(lastSeenTime);
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public AccountOnlineStatus getStatus()
    {
        return status;
    }

    public void setStatus(AccountOnlineStatus status)
    {
        this.status = status;
    }

    public String getLastSeenTime()
    {
        return lastSeenTime;
    }

    public void setLastSeenTime(String lastSeenTime)
    {
        this.lastSeenTime = lastSeenTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccountStatus other = (WSAccountStatus) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        return true;
    }

}
