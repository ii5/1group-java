package one.group.entities.api.request.bpo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSGroupMetaDataRequest
{
    private String id;
    private String groupName;
    private String creatorMobileNo;
    private String createdBy;
    private Date createdTime;
    private String cityId;
    private String locationId;
    private GroupStatus status;
    private String status_s;
    private String mediaId;
    private String assignedTo;
    private Boolean everSync;
    private Date unreadFrom;
    private Integer unreadmsgCount;
    private Integer totalmsgCount;
    private Integer unreadmsgCountQa;
    private Date assignedTime;
    private Date releaseTime;
    private Date statusUpdateTime;
    private Integer invalidExpiredCount;
    private Integer validExpiredCount;
    private Integer duplicateCount;
    private Date latestMessageTimestamp;
    private Date permitSince;
    private GroupSource source;
    private String source_s;
    private String notes;
    private Date updatedTime;
    private String updatedBy;
    private String bpoUpdatedBy;
    private Date bpoUpdatedTime;
    private List<WSGroupMembersMetadataRequest> participants;
    private Boolean skipUserCreation;
    
    public Boolean getSkipUserCreation() 
    {
		return skipUserCreation;
	}
    
    public void setSkipUserCreation(Boolean skipUserCreation) 
    {
		this.skipUserCreation = skipUserCreation;
	}

    private String imageUrl;

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public String getCreatorMobileNo()
    {
        return creatorMobileNo;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public String getCityId()
    {
        return cityId;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public GroupStatus getStatus()
    {
        return status;
    }

    public String getMediaId()
    {
        return mediaId;
    }

    public String getAssignedTo()
    {
        return assignedTo;
    }

    public Boolean isEverSync()
    {
        return everSync;
    }

    public Date getUnreadFrom()
    {
        return unreadFrom;
    }

    public Integer getUnreadmsgCount()
    {
        return unreadmsgCount;
    }

    public Integer getTotalmsgCount()
    {
        return totalmsgCount;
    }

    public Integer getUnreadmsgCountQa()
    {
        return unreadmsgCountQa;
    }

    public Date getAssignedTime()
    {
        return assignedTime;
    }

    public Date getReleaseTime()
    {
        return releaseTime;
    }

    public Date getStatusUpdateTime()
    {
        return statusUpdateTime;
    }

    public Integer getInvalidExpiredCount()
    {
        return invalidExpiredCount;
    }

    public Integer getValidExpiredCount()
    {
        return validExpiredCount;
    }

    public Integer getDuplicateCount()
    {
        return duplicateCount;
    }

    public Date getLatestMessageTimestamp()
    {
        return latestMessageTimestamp;
    }

    public Date getPermitSince()
    {
        return permitSince;
    }

    public GroupSource getSource()
    {
        return source;
    }

    public String getNotes()
    {
        return notes;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public String getSource_s()
    {
        return source_s;
    }

    public String getStatus_s()
    {
        return status_s;
    }

    public Boolean getEverSync()
    {
        return everSync;
    }

    public String getBpoUpdatedBy()
    {
        return bpoUpdatedBy;
    }

    public Date getBpoUpdatedTime()
    {
        return bpoUpdatedTime;
    }

    public List<WSGroupMembersMetadataRequest> getParticipants()
    {
        return participants;
    }

    public void setBpoUpdatedBy(String bpoUpdatedBy)
    {
        this.bpoUpdatedBy = bpoUpdatedBy;
    }

    public void setBpoUpdatedTime(Date bpoUpdatedTime)
    {
        this.bpoUpdatedTime = bpoUpdatedTime;
    }

    public void setSource_s(String source_s)
    {
        this.source = GroupSource.valueOf(source_s);
        this.source_s = source.name();
    }

    public void setStatus_s(String status_s)
    {
        this.status = GroupStatus.valueOf(status_s);
        this.status_s = status.name();
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public void setCreatorMobileNo(String creatorMobileNo)
    {
        this.creatorMobileNo = creatorMobileNo;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public void setStatus(GroupStatus status)
    {
        this.status = status;
        this.status_s = status.name();
    }

    public void setMediaId(String mediaId)
    {
        this.mediaId = mediaId;
    }

    public void setAssignedTo(String assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public void setEverSync(boolean everSync)
    {
        this.everSync = everSync;
    }

    public void setUnreadFrom(Date unreadFrom)
    {
        this.unreadFrom = unreadFrom;
    }

    public void setUnreadmsgCount(Integer unreadmsgCount)
    {
        this.unreadmsgCount = unreadmsgCount;
    }

    public void setTotalmsgCount(Integer totalmsgCount)
    {
        this.totalmsgCount = totalmsgCount;
    }

    public void setUnreadmsgCountQa(Integer unreadmsgCountQa)
    {
        this.unreadmsgCountQa = unreadmsgCountQa;
    }

    public void setAssignedTime(Date assignedTime)
    {
        this.assignedTime = assignedTime;
    }

    public void setReleaseTime(Date releaseTime)
    {
        this.releaseTime = releaseTime;
    }

    public void setStatusUpdateTime(Date statusUpdateTime)
    {
        this.statusUpdateTime = statusUpdateTime;
    }

    public void setInvalidExpiredCount(Integer invalidExpiredCount)
    {
        this.invalidExpiredCount = invalidExpiredCount;
    }

    public void setValidExpiredCount(Integer validExpiredCount)
    {
        this.validExpiredCount = validExpiredCount;
    }

    public void setDuplicateCount(Integer duplicateCount)
    {
        this.duplicateCount = duplicateCount;
    }

    public void setLatestMessageTimestamp(Date latestMessageTimestamp)
    {
        this.latestMessageTimestamp = latestMessageTimestamp;
    }

    public void setPermitSince(Date permitSince)
    {
        this.permitSince = permitSince;
    }

    public void setSource(GroupSource source)
    {
        this.source = source;
        this.source_s = source.name();
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public void setParticipants(List<WSGroupMembersMetadataRequest> participants)
    {
        this.participants = participants;
    }

    public static WSGroupMetaDataRequest dummyData()
    {
        WSGroupMetaDataRequest req = new WSGroupMetaDataRequest();
        req.setId(Utils.randomUUID());
        req.setAssignedTo(Utils.randomUUID());
        req.setLocationId("asvs");

        return req;
    }

    public static void main(String args[]) throws Exception
    {
        WSRequest request = new WSRequest();
        request.setBpoGroupMetaData(dummyData());
        System.out.println(Utils.getJsonString(request));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(Utils.getInstanceFromJson(reader.readLine(), SocketEntity.class));
    }

    public void addParticipants(WSGroupMembersMetadataRequest gmem)
    {
        if (this.participants == null)
        {
            this.participants = new ArrayList<WSGroupMembersMetadataRequest>();
        }
        this.participants.add(gmem);
    }

}
