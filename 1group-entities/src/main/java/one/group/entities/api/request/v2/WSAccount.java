package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;
import one.group.entities.jpa.Account;

public class WSAccount implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String id;

    private String fullName;

    private String companyName;

    private String cityId;

    private WSPhotoReference photo;

    public WSAccount()
    {
    }

    public WSAccount(Account a)
    {
        this.id = a.getId();
        this.fullName = a.getFullName();
        this.companyName = a.getCompanyName();
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setFullName(String firstName)
    {
        this.fullName = firstName;
    }

    public void setCompanyName(String lastName)
    {
        this.companyName = lastName;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccount other = (WSAccount) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSAccount [id=" + id + ", fullName=" + fullName + ", companyName=" + companyName + ", cityId=" + cityId + ", photo=" + photo + "]";
    }

}
