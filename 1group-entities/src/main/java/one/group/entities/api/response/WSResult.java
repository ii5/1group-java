package one.group.entities.api.response;


/**
 * 
 * @author nyalfernandes
 *
 */
public class WSResult implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private String code;
    
    private String message = "";
    
    private boolean isFatal;
    
    public WSResult()
    {
    
    }
    
    
    public WSResult(String code, String message, boolean isFatal)
    {
        this.code = code;
        this.message = message;
        this.isFatal = isFatal;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public boolean getIsFatal()
    {
        return isFatal;
    }

    public void setIsFatal(boolean isFatal)
    {
        this.isFatal = isFatal;
    }


    @Override
    public String toString()
    {
        return "WSResult [code=" + code + ", message=" + message + ", isFatal=" + isFatal + "]";
    }
    
}
