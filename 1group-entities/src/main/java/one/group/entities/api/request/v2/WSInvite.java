package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;

public class WSInvite implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String number;

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

}
