package one.group.entities.api.response;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSNotification implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
    private String title;
    private String body;
    private String id;

    public WSNotification()
    {

    }

    public WSNotification(String title, String body)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(title), "Passed title should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(body), "Passed body should not be null");

        this.title = title;
        this.body = body;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((body == null) ? 0 : body.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSNotification other = (WSNotification) obj;
        if (body == null)
        {
            if (other.body != null)
                return false;
        }
        else if (!body.equals(other.body))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSNotification [title=" + title + ", body=" + body + ", id=" + id + "]";
    }
}