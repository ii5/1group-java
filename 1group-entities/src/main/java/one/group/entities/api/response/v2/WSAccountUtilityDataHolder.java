package one.group.entities.api.response.v2;

import java.util.HashMap;
import java.util.Map;

public class WSAccountUtilityDataHolder
{
    Map<String, WSAccount> idVsAccountMap = new HashMap<String, WSAccount>();
    Map<String, WSAccountRelations> otherAccountVsRelationMap = new HashMap<String, WSAccountRelations>();

    Map<String, Long> accountVsBroadcastCountMap = new HashMap<String, Long>();
    Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap = new HashMap<String, Map<String, WSChatThreadCursor>>();

    Map<String, String> starredBroadcastMap = new HashMap<String, String>();
    Map<String, WSBroadcast> idVsBroadcastMap = new HashMap<String, WSBroadcast>();

    public Map<String, WSAccount> getIdVsAccountMap()
    {
        return idVsAccountMap;
    }

    public void setIdVsAccountMap(Map<String, WSAccount> idVsAccountMap)
    {
        this.idVsAccountMap = idVsAccountMap;
    }

    public Map<String, WSAccountRelations> getOtherAccountVsRelationMap()
    {
        return otherAccountVsRelationMap;
    }

    public void setOtherAccountVsRelationMap(Map<String, WSAccountRelations> otherAccountVsRelationMap)
    {
        this.otherAccountVsRelationMap = otherAccountVsRelationMap;
    }

    public Map<String, Long> getAccountVsBroadcastCountMap()
    {
        return accountVsBroadcastCountMap;
    }

    public void setAccountVsBroadcastCountMap(Map<String, Long> accountVsBroadcastCountMap)
    {
        this.accountVsBroadcastCountMap = accountVsBroadcastCountMap;
    }

    public Map<String, Map<String, WSChatThreadCursor>> getAccountVsCursorMap()
    {
        return accountVsCursorMap;
    }

    public void setAccountVsCursorMap(Map<String, Map<String, WSChatThreadCursor>> accountVsCursorMap)
    {
        this.accountVsCursorMap = accountVsCursorMap;
    }

    public Map<String, String> getStarredBroadcastMap()
    {
        return starredBroadcastMap;
    }

    public void setStarredBroadcastMap(Map<String, String> starredBroadcastMap)
    {
        this.starredBroadcastMap = starredBroadcastMap;
    }

    public Map<String, WSBroadcast> getIdVsBroadcastMap()
    {
        return idVsBroadcastMap;
    }

    public void setIdVsBroadcastMap(Map<String, WSBroadcast> idVsBroadcastMap)
    {
        this.idVsBroadcastMap = idVsBroadcastMap;
    }

}
