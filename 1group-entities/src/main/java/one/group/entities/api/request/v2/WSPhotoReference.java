package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;

public class WSPhotoReference implements RequestEntity
{

    private WSPhoto full;

    private WSPhoto thumbnail;

    public WSPhoto getFull()
    {
        return full;
    }

    public WSPhoto getThumbnail()
    {
        return thumbnail;
    }

    public void setFull(WSPhoto full)
    {
        this.full = full;
    }

    public void setThumbnail(WSPhoto thumbnail)
    {
        this.thumbnail = thumbnail;
    }

}
