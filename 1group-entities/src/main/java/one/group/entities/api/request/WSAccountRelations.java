/**
 * 
 */
package one.group.entities.api.request;

import java.sql.Timestamp;

import one.group.entities.jpa.AccountRelations;


/**
 * @author ashishthorat
 *
 */
public class WSAccountRelations implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String accountId;
    private String contactId;
    private String score;
    private Integer scoredAs;
    private Boolean isContact;
    private Boolean isContactOf;
    private Boolean isBlocked;
    private Boolean isBlockedBy;
    private Timestamp createdOn;

    public WSAccountRelations(String contactId, boolean isBlockedBy)
    {
        this.contactId = contactId;
        this.isBlocked = isBlockedBy;
    }

    public WSAccountRelations()
    {
    }

    public WSAccountRelations(AccountRelations accountRelations)
    {

        this.accountId = accountRelations.getAccount().getId();
        this.contactId = accountRelations.getOtherAccountId();
        this.score = accountRelations.getScore().toString();
        this.isContact = accountRelations.isContact();
        this.isBlocked = accountRelations.isBlocked();
        this.createdOn = accountRelations.getCreatedTime();
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getContactId()
    {
        return contactId;
    }

    public void setContactId(String contactId)
    {
        this.contactId = contactId;
    }

    public String getScore()
    {
        return score;
    }

    public void setScore(String score)
    {
        this.score = score;
    }

    public Boolean getIsContact()
    {
        return isContact;
    }

    public void setIsContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public Boolean getIsBlocked()
    {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Timestamp getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn)
    {
        this.createdOn = createdOn;
    }

    public Integer getScoredAs()
    {
        return scoredAs;
    }

    public void setScoredAs(Integer scoredAs)
    {
        this.scoredAs = scoredAs;
    }

    public Boolean getIsContactOf()
    {
        return isContactOf;
    }

    public void setIsContactOf(Boolean isContactOf)
    {
        this.isContactOf = isContactOf;
    }

    public Boolean getIsBlockedBy()
    {
        return isBlockedBy;
    }

    public void setIsBlockedBy(Boolean isBlockedBy)
    {
        this.isBlockedBy = isBlockedBy;
    }

    @Override
    public String toString()
    {
        return "WSAccountRelations [accountId=" + accountId + ", contactId=" + contactId + ", score=" + score + ", isContact=" + isContact + ", isBlocked=" + isBlocked + ", createdOn=" + createdOn
                + "]";
    }
}
