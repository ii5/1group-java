package one.group.entities.api.request;

import java.util.List;

public class WSAddPropertyListing implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String propertyType;

    private String propertySubType;

    private String area;

    private String commissionType;

    private String description;

    private String rooms;

    private String salePrice;

    private String rentPrice;

    private String locationId;

    private String propertyMarket;

    private String isHot;

    private String status;

    private String action;

    private List<String> amenities;

    private String customSublocation;

    private WSAdminPropertyListing admin;

    public WSAdminPropertyListing getAdmin()
    {
        return admin;
    }

    public void setAdmin(WSAdminPropertyListing admin)
    {
        this.admin = admin;
    }

    public String getPropertyType()
    {
        return propertyType;
    }

    public String getPropertySubType()
    {
        return propertySubType;
    }

    public String getArea()
    {
        return area;
    }

    public String getCommissionType()
    {
        return commissionType;
    }

    public String getDescription()
    {
        return description;
    }

    public String getRooms()
    {
        return rooms;
    }

    public String getSalePrice()
    {
        return salePrice;
    }

    public String getRentPrice()
    {
        return rentPrice;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public String getPropertyMarket()
    {
        return propertyMarket;
    }

    public String getIsHot()
    {
        return isHot;
    }

    public void setPropertyType(String propertyType)
    {
        this.propertyType = propertyType;
    }

    public void setPropertySubType(String propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public void setCommissionType(String commissionType)
    {
        this.commissionType = commissionType;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public void setSalePrice(String salePrice)
    {
        this.salePrice = salePrice;
    }

    public void setRentPrice(String rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public void setPropertyMarket(String propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public void setIsHot(String isHot)
    {
        this.isHot = isHot;
    }

    public List<String> getAmenities()
    {
        return amenities;
    }

    public void setAmenities(List<String> amenities)
    {
        this.amenities = amenities;
    }

    public String getStatus()
    {
        return status;
    }

    public String getAction()
    {
        return action;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    @Override
    public String toString()
    {
        return "WSAddPropertyListing [propertyType=" + propertyType + ", propertySubType=" + propertySubType + ", area=" + area + ", commissionType=" + commissionType + ", description=" + description
                + ", rooms=" + rooms + ", salePrice=" + salePrice + ", rentPrice=" + rentPrice + ", locationId=" + locationId + ", propertyMarket=" + propertyMarket + ", isHot=" + isHot + ", status="
                + status + ", action=" + action + ", amenities=" + amenities + ", admin=" + admin + "]";
    }

}
