package one.group.entities.api.request;

import java.util.ArrayList;
import java.util.List;

public class WSLocationDB implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private List<WSLocationMasterData> locationsMasterData;

    public List<WSLocationMasterData> getLocationsMasterData()
    {
        return locationsMasterData;
    }

    public void setLocationsMasterData(List<WSLocationMasterData> locationsMasterData)
    {
        this.locationsMasterData = locationsMasterData;
    }

    public void addLocationsMasterData(WSLocationMasterData masterData)
    {
        this.locationsMasterData.add(masterData);
    }

    public WSLocationDB()
    {
        this.locationsMasterData = new ArrayList<WSLocationMasterData>();
    }
}
