package one.group.entities.api.response;

import java.util.List;

public class WSAccountListResultAdmin implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSAccount> accounts;

    public WSAccountListResultAdmin()
    {

    }

    public List<WSAccount> getAccounts()
    {
        return accounts;
    }

    public void setAccounts(List<WSAccount> accounts)
    {
        this.accounts = accounts;
    }

    @Override
    public String toString()
    {
        return "WSAccountListResultAdmin [accounts=" + accounts + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accounts == null) ? 0 : accounts.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccountListResultAdmin other = (WSAccountListResultAdmin) obj;
        if (accounts == null)
        {
            if (other.accounts != null)
                return false;
        }
        else if (!accounts.equals(other.accounts))
            return false;
        return true;
    }

}
