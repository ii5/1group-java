package one.group.entities.api.response;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSTag implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String title;

    private String broadcastId;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

}
