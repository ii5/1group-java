package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.socket.Groups;

public class WSGroups implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String chatThreadId;
    private int totalMessageCount;

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public int getTotalMessageCount()
    {
        return totalMessageCount;
    }

    public void setTotalMessageCount(int totalMessageCount)
    {
        this.totalMessageCount = totalMessageCount;
    }

    public WSGroups()
    {

    }

    public WSGroups(Groups groups)
    {
        this.chatThreadId = groups.getId();
        this.totalMessageCount = groups.getTotalmsgCount();
    }
}
