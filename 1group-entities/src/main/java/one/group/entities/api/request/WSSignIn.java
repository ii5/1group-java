package one.group.entities.api.request;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.Client;

public class WSSignIn implements RequestEntity
{
    private String mobileNumber;

    private String otp;

    private String grantType;

    private String clientType;

    private String deviceId;

    private String deviceUserName;

    private String deviceModelName;

    private String devicePlatform;

    private String deviceOsVersion;

    private Account account;

    private Client client;

    private String cpsId;

    private String clientVersion;

    public WSSignIn()
    {
    };

    public WSSignIn(Account account)
    {
        this.setAccount(account);
    }

    public Client getClient()
    {
        return client;
    }

    public void setClient(Client client)
    {
        this.client = client;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp()
    {
        return otp;
    }

    public String getGrantType()
    {
        return grantType;
    }

    public String getClientType()
    {
        return clientType;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public String getDeviceUserName()
    {
        return deviceUserName;
    }

    public String getDeviceModelName()
    {
        return deviceModelName;
    }

    public String getDevicePlatform()
    {
        return devicePlatform;
    }

    public String getDeviceOsVersion()
    {
        return deviceOsVersion;
    }

    public void setOtp(String otp)
    {
        this.otp = otp;
    }

    public void setGrantType(String grantType)
    {
        this.grantType = grantType;
    }

    public void setClientType(String clientType)
    {
        this.clientType = clientType;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public void setDeviceUserName(String deviceUserName)
    {
        this.deviceUserName = deviceUserName;
    }

    public void setDeviceModelName(String deviceModelName)
    {
        this.deviceModelName = deviceModelName;
    }

    public void setDevicePlatform(String devicePlatform)
    {
        this.devicePlatform = devicePlatform;
    }

    public void setDeviceOsVersion(String deviceOsVersion)
    {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getCpsId()
    {
        return cpsId;
    }

    public void setCpsId(String cpsId)
    {
        this.cpsId = cpsId;
    }

    public String getClientVersion()
    {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion)
    {
        this.clientVersion = clientVersion;
    }

    @Override
    public String toString()
    {
        return "WSSignIn [mobileNumber=" + mobileNumber + ", otp=" + otp + ", grantType=" + grantType + ", clientType=" + clientType + ", deviceId=" + deviceId + ", deviceUserName=" + deviceUserName
                + ", deviceModelName=" + deviceModelName + ", devicePlatform=" + devicePlatform + ", deviceOsVersion=" + deviceOsVersion + ", account=" + account + ", client=" + client + ", cpsId="
                + cpsId + ", appVersion=" + clientVersion + "]";
    }

}
