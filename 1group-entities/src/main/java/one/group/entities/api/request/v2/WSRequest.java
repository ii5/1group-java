package one.group.entities.api.request.v2;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.request.bpo.WSAdminUserRequest;
import one.group.entities.api.request.bpo.WSGroupMetaDataRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.api.response.v2.WSChatThreadCursor;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author sanil
 *
 */
public class WSRequest implements RequestEntity
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WSChatThreadCursor chatThreadCursor;

    private WSBroadcastSearchQueryRequest search;

    private WSAccount account;

    private WSAccountRelations accountRelations;

    private WSContacts contacts;

    private List<String> invitees;

    private WSBroadcast broadcast;

    private WSMessage message;

    private WSSyncRequest sync;

    private WSAccountIdsQuery fetch;

    private WSRemoveMedia removeMedia;

    private List<WSClientAnalytics> clientAnalytics;

    private WSClientData clientData;

    private WSGroupMetaDataRequest bpoGroupMetaData;

    private WSRequestOtp requestOtp;

    private WSSignIn signIn;

    private WSChatThreadRequest chatThread;

    private WSAdminUserRequest adminUser;

    private List<WSAdminUserRequest> userCityList;

    private WSUserRequest user;

    private AdminRequest adminRequest;

    private WSInvitation invitation;
    
    private Map<String, Set<WSMessage>> postMessages;

    public WSRequest()
    {

    }

    public WSRequest(WSRequest request)
    {
        this.chatThreadCursor = request.getChatThreadCursor();
        this.sync = request.getSync();
        this.contacts = request.getContacts();
        this.chatThread = request.getChatThread();
        this.adminUser = request.getAdminUser();
        this.userCityList = request.getUserCityList();
        this.bpoGroupMetaData = request.getBpoGroupMetaData();
        this.user = request.getUser();
        this.adminRequest = request.getAdminRequest();
        this.postMessages = request.getPostMessages();
    }

    public Map<String, Set<WSMessage>> getPostMessages() 
    {
		return postMessages;
	}
    
    public void setPostMessages(Map<String, Set<WSMessage>> postMessages) 
    {
		this.postMessages = postMessages;
	}
    public AdminRequest getAdminRequest()
    {
        return adminRequest;
    }

    public void setAdminRequest(AdminRequest adminRequest)
    {
        this.adminRequest = adminRequest;
    }

    public WSUserRequest getUser()
    {
        return user;
    }

    public void setUser(WSUserRequest user)
    {
        this.user = user;
    }

    public List<WSAdminUserRequest> getUserCityList()
    {
        return userCityList;
    }

    public void setUserCityList(List<WSAdminUserRequest> userCity)
    {
        this.userCityList = userCity;
    }

    public WSAdminUserRequest getAdminUser()
    {
        return adminUser;
    }

    public void setAdminUser(WSAdminUserRequest bpoUser)
    {
        this.adminUser = bpoUser;
    }

    public WSChatThreadRequest getChatThread()
    {
        return chatThread;
    }

    public void setChatThread(WSChatThreadRequest request)
    {
        this.chatThread = request;
    }

    public WSSyncRequest getSync()
    {
        return sync;
    }

    public void setSync(WSSyncRequest sync)
    {
        this.sync = sync;
    }

    public WSChatThreadCursor getChatThreadCursor()
    {
        return chatThreadCursor;
    }

    public void setChatThreadCursor(WSChatThreadCursor chatThreadCursor)
    {
        this.chatThreadCursor = chatThreadCursor;
    }

    public WSGroupMetaDataRequest getBpoGroupMetaData()
    {
        return bpoGroupMetaData;
    }

    public void setBpoGroupMetaData(WSGroupMetaDataRequest bpoGroupMetaData)
    {
        this.bpoGroupMetaData = bpoGroupMetaData;
    }

    public WSBroadcastSearchQueryRequest getSearch()
    {
        return search;
    }

    public void setSearch(WSBroadcastSearchQueryRequest search)
    {
        this.search = search;
    }

    public WSAccount getAccount()
    {
        return account;
    }

    public void setAccount(WSAccount account)
    {
        this.account = account;
    }

    public WSAccountRelations getAccountRelations()
    {
        return accountRelations;
    }

    public void setAccountRelations(WSAccountRelations accountRelations)
    {
        this.accountRelations = accountRelations;
    }

    public WSContacts getContacts()
    {
        return contacts;
    }

    public void setContacts(WSContacts contacts)
    {
        this.contacts = contacts;
    }

    public List<String> getInvitees()
    {
        return invitees;
    }

    public void setInvitees(List<String> invitees)
    {
        this.invitees = invitees;
    }

    public WSBroadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSBroadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public WSMessage getMessage()
    {
        return message;
    }

    public void setMessage(WSMessage message)
    {
        this.message = message;
    }

    public List<WSClientAnalytics> getClientAnalytics()
    {
        return clientAnalytics;
    }

    public void setClientAnalytics(List<WSClientAnalytics> clientAnalytics)
    {
        this.clientAnalytics = clientAnalytics;
    }

    public WSClientData getClientData()
    {
        return clientData;
    }

    public void setClientData(WSClientData clientData)
    {
        this.clientData = clientData;
    }

    public static WSRequest dummyData()
    {
        WSRequest request = new WSRequest();
        return request;
    }

    public static WSRequest dummyChatThreadCursor()
    {
        WSRequest request = WSRequest.dummyData();
        request.setChatThreadCursor(WSChatThreadCursor.dummyData());
        return request;
    }

    public static WSRequest dummySync()
    {
        WSRequest request = WSRequest.dummyData();
        request.setSync(WSSyncRequest.dummyData());
        return request;
    }

    public WSAccountIdsQuery getFetch()
    {
        return fetch;
    }

    public void setFetch(WSAccountIdsQuery fetch)
    {
        this.fetch = fetch;
    }

    public WSRemoveMedia getRemoveMedia()
    {
        return removeMedia;
    }

    public void setRemoveMedia(WSRemoveMedia removeMedia)
    {
        this.removeMedia = removeMedia;
    }

    public WSRequestOtp getRequestOtp()
    {
        return requestOtp;
    }

    public void setRequestOtp(WSRequestOtp requestOtp)
    {
        this.requestOtp = requestOtp;
    }

    public WSSignIn getSignIn()
    {
        return signIn;
    }

    public void setSignIn(WSSignIn signIn)
    {
        this.signIn = signIn;
    }

    public WSInvitation getInvitation()
    {
        return invitation;
    }

    public void setInvitation(WSInvitation invitation)
    {
        this.invitation = invitation;
    }

    public static void main(String[] args)
    {
        String endpoint = "/users";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.POST;
        SocketEntity entity = new SocketEntity();

        WSResult result = new WSResult("200", "Sync index updated successfully.", false);
        WSRequest request = WSRequest.dummySync();

        WSUserRequest userReq = new WSUserRequest();
        userReq.setMobileNumber("+919833143042");
        userReq.setLocalStorage("storage");

        request.setUser(userReq);
        WSResponse response = new WSResponse();
        response.setData(WSSyncResult.dummyData(request.getSync().getAckSyncIndex()));
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        // entity.setWithWSResponse(response);
        entity.setWithWSRequest(request);

        System.out.println(Utils.getJsonString(entity));
    }

}
