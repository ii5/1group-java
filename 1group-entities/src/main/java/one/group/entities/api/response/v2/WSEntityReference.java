package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * Model that defines the Hints provided to clients
 * 
 * @author nyalfernandes
 *
 */
public class WSEntityReference implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HintType type;

    private EntityType namespace;

    private String id;

    public WSEntityReference()
    {
    }

    public WSEntityReference(final HintType hintType, final EntityType recordType, final String recordId)
    {
        Validation.notNull(hintType, "The hint type should not be null.");
        Validation.notNull(recordType, "The record type should not be null.");
//        Validation.notNull(recordId, "The record id passed should not be null.");

        this.type = hintType;
        this.namespace = recordType;
        this.id = recordId;
    }

    public HintType getType()
    {
        return type;
    }

    public void setType(HintType type)
    {
        this.type = type;
    }

    public EntityType getNamespace()
    {
        return namespace;
    }

    public String getId()
    {
        return id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSEntityReference other = (WSEntityReference) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (namespace != other.namespace)
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSEntityReference [type=" + type + ", namespace=" + namespace + ", id=" + id + "]";
    }

    
    public static WSEntityReference dummyData()
    {
        HintType hintType = HintType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % HintType.values().length];
        EntityType entityType = EntityType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % EntityType.values().length];
        
        WSEntityReference entityReference = new WSEntityReference(hintType, entityType, Utils.randomUUID());
        
        return entityReference;
        
    }
}
