package one.group.entities.api.response;

import java.util.List;

import one.group.entities.jpa.City;
import one.group.utils.Utils;

public class WSCity implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private List<WSLocation> locations;
    
    private String value;
    
    private String label;

    public WSCity()
    {
    };

    public WSCity(City city)
    {
        this.setId(city.getIdAsString());
        this.setName(city.getName());
        this.value = city.getName();
        this.label = city.getName();
    }

    public String getLabel()
    {
        return label;
    }
    
    public void setLabel(String label)
    {
        this.label = label;
    }
    
    public String getValue()
    {
        return value;
    }
    
    public void setValue(String value)
    {
        this.value = value;
    }
    
    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public List<WSLocation> getLocations()
    {
        return locations;
    }

    public void setLocations(List<WSLocation> locations)
    {
        this.locations = locations;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "WSCity [id=" + id + ", name=" + name + "]";
    }
    
    public void trimForApp()
    {
    	this.setValue(null);
    	this.setLabel(null);
    }

    
    public static WSCity dummyData(String name)
    {
        WSCity city = new WSCity();
        city.setId(Utils.randomUUID());
        city.setName(name);
        
        return city;
    }
}
