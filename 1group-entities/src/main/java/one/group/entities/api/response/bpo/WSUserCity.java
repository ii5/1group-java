package one.group.entities.api.response.bpo;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSUserCity
{
    public static final String QUERY_FETCH_ADMIN_DETAILS = "SELECT NEW one.group.entities.api.response.bpo.WSUserCity(c.admin.id, c.admin.userName, c.city.id, c.city.name, c.priority, c.admin.email) FROM WaUserCity c WHERE c.id IS NOT NULL ";

    public static final String QUERY_FETCH_UNASSIGNED_CITY_ADMIN_DETAILS = "SELECT NEW one.group.entities.api.response.bpo.WSUserCity(a.id, a.userName) FROM Admin a, AuthAssignment aa where aa.user=a.id and aa.authItem.name='BPO Operator' and a.id NOT IN (select distinct wauc.admin.id from WaUserCity wauc) and a.status='ACTIVE'";

    private String adminId;

    private String adminName;

    private String cityId;

    private String cityname;

    private int priority;

    private String email;

    public WSUserCity()
    {
        // TODO Auto-generated constructor stub
    }

    public WSUserCity(String adminId, String adminName, String cityId, String cityname, int priority, String email)
    {
        this.adminId = adminId;
        this.adminName = adminName;
        this.cityId = cityId;
        this.cityname = cityname;
        this.priority = priority;
        this.email = email;
    }

    public WSUserCity(String adminId, String adminName)
    {
        this.adminId = adminId;
        this.adminName = adminName;
    }

    public String getAdminId()
    {
        return adminId;
    }

    public void setAdminId(String adminId)
    {
        this.adminId = adminId;
    }

    public String getAdminName()
    {
        return adminName;
    }

    public void setAdminName(String adminName)
    {
        this.adminName = adminName;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getCityname()
    {
        return cityname;
    }

    public void setCityname(String cityname)
    {
        this.cityname = cityname;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

}
