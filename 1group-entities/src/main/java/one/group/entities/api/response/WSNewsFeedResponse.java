package one.group.entities.api.response;

import java.util.Set;

public class WSNewsFeedResponse implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private Set<WSNewsFeed> newsFeed;

    public Set<WSNewsFeed> getNewsFeed()
    {
        return newsFeed;
    }

    public void setNewsFeed(Set<WSNewsFeed> newsFeed)
    {
        this.newsFeed = newsFeed;
    }

}
