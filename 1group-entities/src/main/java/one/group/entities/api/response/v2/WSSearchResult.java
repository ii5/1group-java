package one.group.entities.api.response.v2;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSSearchResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WSBroadcastSearchQuery query;

    private WSBroadcastListResult broadcasts;

    public WSBroadcastSearchQuery getQuery()
    {
        return query;
    }

    public void setQuery(WSBroadcastSearchQuery query)
    {
        this.query = query;
    }

    public WSBroadcastListResult getBroadcasts()
    {
        return broadcasts;
    }

    public void setBroadcasts(WSBroadcastListResult broadcasts)
    {
        this.broadcasts = broadcasts;
    }

    public static WSSearchResult dummyData()
    {
        WSSearchResult search = new WSSearchResult();

        search.setBroadcasts(WSBroadcastListResult.dummyData());

        return search;
    }

    public static void main(String[] args)
    {
        String endpoint = "/search";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.POST;
        SocketEntity entity = new SocketEntity();
        WSSearchResult responseData = WSSearchResult.dummyData();
        WSResult result = new WSResult("200", "Chat message data.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));
    }
}
