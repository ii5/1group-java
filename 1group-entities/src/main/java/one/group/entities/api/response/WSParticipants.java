/**
 * 
 */
package one.group.entities.api.response;

/**
 * @author miteshchavda
 *
 */
public class WSParticipants implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private String accountId;

    private WSChatThreadCursor cursors;

    public WSParticipants()
    {

    }

    public WSParticipants(String accountId, WSChatThreadCursor cursors)
    {
        this.accountId = accountId;
        this.cursors = cursors;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public WSChatThreadCursor getCursors()
    {
        return cursors;
    }

    public void setCursors(WSChatThreadCursor cursors)
    {
        this.cursors = cursors;
    }

    @Override
    public String toString()
    {
        return "WSParticipants [accountId=" + accountId + ", cursors=" + cursors + "]";
    }

}
