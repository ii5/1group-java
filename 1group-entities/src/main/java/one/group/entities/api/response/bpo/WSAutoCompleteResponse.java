package one.group.entities.api.response.bpo;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSAutoCompleteResponse
{
    private String id;
    private String label;
    private String value;

    public String getId()
    {
        return id;
    }

    public String getLabel()
    {
        return label;
    }

    public String getValue()
    {
        return value;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

}
