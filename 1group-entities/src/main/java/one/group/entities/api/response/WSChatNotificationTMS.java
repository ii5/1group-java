package one.group.entities.api.response;


public class WSChatNotificationTMS implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long lastChatMessageReceivedTms;
    private Long lastReceivedIndexAdvancedTms;
    private Long lastSmsSent;

    public WSChatNotificationTMS()
    {

    }

    public WSChatNotificationTMS(Long lastChatMessageReceivedTms, Long lastReceivedIndexAdvancedTms, Long lastSmsSent)
    {
        // Validation.isTrue(!Utils.isNull(lastChatMessageReceivedTms),
        // "Passed last chat messag received tms should not be null");
        // Validation.isTrue(!Utils.isNull(lastReceivedIndexAdvancedTms),
        // "Passed last received index advanced tms should not be null");
        // Validation.isTrue(!Utils.isNull(lastSmsSent),
        // "Passed last sms sent should not be null");

        this.lastChatMessageReceivedTms = lastChatMessageReceivedTms;
        this.lastReceivedIndexAdvancedTms = lastReceivedIndexAdvancedTms;
        this.lastSmsSent = lastSmsSent;
    }

    public Long getLastChatMessageReceivedTms()
    {
        return lastChatMessageReceivedTms;
    }

    public void setLastChatMessageReceivedTms(Long lastChatMessageReceivedTms)
    {
        this.lastChatMessageReceivedTms = lastChatMessageReceivedTms;
    }

    public Long getLastReceivedIndexAdvancedTms()
    {
        return lastReceivedIndexAdvancedTms;
    }

    public void setLastReceivedIndexAdvancedTms(Long lastReceivedIndexAdvancedTms)
    {
        this.lastReceivedIndexAdvancedTms = lastReceivedIndexAdvancedTms;
    }

    public Long getLastSmsSent()
    {
        return lastSmsSent;
    }

    public void setLastSmsSent(Long lastSmsSent)
    {
        this.lastSmsSent = lastSmsSent;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lastChatMessageReceivedTms == null) ? 0 : lastChatMessageReceivedTms.hashCode());
        result = prime * result + ((lastReceivedIndexAdvancedTms == null) ? 0 : lastReceivedIndexAdvancedTms.hashCode());
        result = prime * result + ((lastSmsSent == null) ? 0 : lastSmsSent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatNotificationTMS other = (WSChatNotificationTMS) obj;
        if (lastChatMessageReceivedTms == null)
        {
            if (other.lastChatMessageReceivedTms != null)
                return false;
        }
        else if (!lastChatMessageReceivedTms.equals(other.lastChatMessageReceivedTms))
            return false;
        if (lastReceivedIndexAdvancedTms == null)
        {
            if (other.lastReceivedIndexAdvancedTms != null)
                return false;
        }
        else if (!lastReceivedIndexAdvancedTms.equals(other.lastReceivedIndexAdvancedTms))
            return false;
        if (lastSmsSent == null)
        {
            if (other.lastSmsSent != null)
                return false;
        }
        else if (!lastSmsSent.equals(other.lastSmsSent))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatNotificationTMS [lastChatMessageReceivedTms=" + lastChatMessageReceivedTms + ", lastReceivedIndexAdvancedTms=" + lastReceivedIndexAdvancedTms + ", lastSmsSent=" + lastSmsSent
                + "]";
    }

}
