package one.group.entities.api.response;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSResponse implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private WSResult result;
    
    private Object data;
    
    public WSResponse()
    {
    
    }
    
    public WSResponse(WSResponse entity)
    {
        this.result = entity.getResult();
        this.data = entity.getData();
    }
    
    public WSResponse(final WSResult result)
    {
        Validation.notNull(result, "Result should not be null.");
        
        this.result = result;
    }

    public WSResult getResult()
    {
        return result;
    }

    public void setResult(WSResult result)
    {
        this.result = result;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "WSResponse [result=" + result + ", data=" + data + "]";
    }
    
    
    public static void main(String[] args)
    {
        WSResponse response = new WSResponse(new WSResult("404", "Resource not found.", true));
        System.out.println(Utils.getJsonString(response));
    }
    
}
