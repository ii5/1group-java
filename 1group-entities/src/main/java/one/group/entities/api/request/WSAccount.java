package one.group.entities.api.request;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import one.group.entities.jpa.Account;


public class WSAccount implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String id;

    private String fullName;

    private String companyName;

    private List<String> locations;

    private WSPhotoReference photo;
    
    public WSAccount() {}
    
    public WSAccount(Account a)
    {
        this.id = a.getId();
        this.fullName = a.getFullName();
        this.companyName = a.getCompanyName();
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public List<String> getLocations()
    {
        return locations;
    }

    public void setFullName(String firstName)
    {
        this.fullName = firstName;
    }

    public void setCompanyName(String lastName)
    {
        this.companyName = lastName;
    }

    public void setLocations(List<String> locations)
    {
        this.locations = locations;
    }

    @Override
    public String toString()
    {
        return "WSAccount [fullName=" + fullName + ", companyName=" + companyName + ", locations=" + locations + ", photo=" + photo + "]";
    }
    
    public static void main(String[] args)
    {
        Calendar c1 = Calendar.getInstance();
        
        Calendar c2 = Calendar.getInstance();
        
        Date now = new Date();
        c1.setTime(now);
        c2.add(Calendar.DAY_OF_MONTH, 45);
        System.out.println("c1: " + c1.getTime());
        System.out.println("c2: " + c2.getTime());
    }
}
