package one.group.entities.api.response;

import java.util.List;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSChatThreadList implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<WSChatThreadReference> chatThreads;

    private Integer maxChatThreadIndex;

    public WSChatThreadList()
    {

    }

    public WSChatThreadList(List<WSChatThreadReference> wsChatThreads, Integer maxChatThreadIndex)
    {
        Validation.isTrue(!Utils.isNull(wsChatThreads), "Chat thread should not be null");
        Validation.isTrue(!Utils.isNull(maxChatThreadIndex), "Max chat thread index should not be null");

        this.chatThreads = wsChatThreads;
        this.maxChatThreadIndex = maxChatThreadIndex;
    }

    public List<WSChatThreadReference> getChatThreads()
    {
        return chatThreads;
    }

    public void setChatThreads(List<WSChatThreadReference> chatThreads)
    {
        this.chatThreads = chatThreads;
    }

    public Integer getMaxChatThreadIndex()
    {
        return maxChatThreadIndex;
    }

    public void setMaxChatThreadIndex(Integer maxChatThreadIndex)
    {
        this.maxChatThreadIndex = maxChatThreadIndex;
    }

}
