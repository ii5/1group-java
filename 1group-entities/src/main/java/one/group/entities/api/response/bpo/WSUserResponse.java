package one.group.entities.api.response.bpo;

import one.group.entities.api.request.v2.WSUserRequest;
import one.group.entities.jpa.User;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSUserResponse extends WSUserRequest
{

    public WSUserResponse()
    {
        
    }
    
    public WSUserResponse (WSUserRequest req)
    {
        this.setCityId(req.getCityId());
        this.setCreatedBy(req.getCreatedBy());
        this.setEmail(req.getEmail());
        this.setFullName(req.getFullName());
        this.setGroupSelectionStatus(req.getGroupSelectionStatus());
        this.setId(req.getId());
        this.setLocalStorage(req.getLocalStorage());
        this.setMobileNumber(req.getMobileNumber());
        this.setShortReference(req.getShortReference());
        this.setSource(req.getSource());
        this.setSyncStatus(req.getSyncStatus());
        this.setType(req.getType());
    }
    
    public WSUserResponse (User req)
    {
        if (req == null)
        {
            return;
        }
//        this.setCityId(req.getC);
        this.setCreatedBy(req.getCreatedBy());
        this.setEmail(req.getEmail());
        this.setFullName(req.getFullName());
        this.setGroupSelectionStatus(req.getGroupSelectionStatus().toString());
        this.setId(req.getId());
        this.setLocalStorage(req.getLocalStorage());
        this.setMobileNumber(req.getWaPhoneNumber());
        this.setShortReference(req.getShortReference());
        this.setSource(req.getSource().toString());
        this.setSyncStatus(req.getSyncStatus().toString());
        this.setType(req.getType().toString());
    }
}
