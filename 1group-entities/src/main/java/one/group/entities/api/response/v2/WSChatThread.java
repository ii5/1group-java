package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatThread implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String chatThreadId;

    private Integer maxChatMessageIndex;

    private Integer unreadMessageCount;

    private Integer myBroadcastCount;

    private String latestMessageTime;

    private List<WSMessageResponse> messages = new ArrayList<WSMessageResponse>();

    private WSPaginationData pagination;

    public WSChatThread()
    {
    }

    public WSChatThread(String chatThreadId, Integer maxChatMessageIndex, List<WSMessageResponse> messages)
    {
        this.chatThreadId = chatThreadId;
        this.maxChatMessageIndex = maxChatMessageIndex;
        this.messages = messages;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public Integer getMaxChatMessageIndex()
    {
        return maxChatMessageIndex;
    }

    public void setMaxChatMessageIndex(Integer maxChatMessageIndex)
    {
        this.maxChatMessageIndex = maxChatMessageIndex;
    }

    public List<WSMessageResponse> getMessages()
    {
        return messages;
    }

    public void setMessages(List<WSMessageResponse> messages)
    {
        this.messages = messages;
    }

    public void addMessages(WSMessageResponse message)
    {
        this.messages.add(message);
    }

    public Integer getMyBroadcastCount()
    {
        return myBroadcastCount;
    }

    public Integer getUnreadMessageCount()
    {
        return unreadMessageCount;
    }

    public void setMyBroadcastCount(Integer myBroadcastCount)
    {
        this.myBroadcastCount = myBroadcastCount;
    }

    public void setUnreadMessageCount(Integer unreadMessageCount)
    {
        this.unreadMessageCount = unreadMessageCount;
    }

    public String getLatestMessageTime()
    {
        return latestMessageTime;
    }

    public void setLatestMessageTime(String latestMessageTime)
    {
        this.latestMessageTime = latestMessageTime;
    }

    public WSPaginationData getPagination()
    {
        return pagination;
    }

    public void setPagination(WSPaginationData pagination)
    {
        this.pagination = pagination;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatThread other = (WSChatThread) obj;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatThread [" + (chatThreadId != null ? "chatThreadId=" + chatThreadId + ", " : "") + (maxChatMessageIndex != null ? "maxChatMessageIndex=" + maxChatMessageIndex + ", " : "")
                + (unreadMessageCount != null ? "unreadMessageCount=" + unreadMessageCount + ", " : "") + (myBroadcastCount != null ? "myBroadcastCount=" + myBroadcastCount + ", " : "")
                + (latestMessageTime != null ? "latestMessageTime=" + latestMessageTime + ", " : "") + (messages != null ? "messages=" + messages : "") + "]";
    }

    public static WSChatThread dummyData(int numberOfMessages, int maxChatMessageIndex, WSAccount createdBy, WSAccount toAccount, Integer unreadMessageCount, Integer myBroadcastCount,
            String latestMessageTime)
    {

        MessageType[] messageTypeArray = { MessageType.BROADCAST, MessageType.TEXT, MessageType.PHOTO, MessageType.PHONE_CALL };
        WSChatThread chatThread = new WSChatThread();

        chatThread.setChatThreadId(Utils.randomUUID());
        chatThread.setMaxChatMessageIndex(maxChatMessageIndex);
        chatThread.setUnreadMessageCount(unreadMessageCount);
        chatThread.setLatestMessageTime(latestMessageTime);

        for (int i = 0; i < numberOfMessages; i++)
        {
            WSMessageResponse message = WSMessageResponse.dummyData(createdBy, toAccount, i, messageTypeArray[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true))
                    % messageTypeArray.length], i + ".) This is message " + 1, WSPhotoReference.dummyData(), chatThread.getChatThreadId(), WSBroadcast.dummyData(null));
            chatThread.addMessages(message);
        }

        return chatThread;
    }

    public static WSChatThread dummyDataWithChatThreadId(int numberOfMessages, int maxChatMessageIndex, WSAccount createdBy, WSAccount toAccount, Integer unreadMessageCount, Integer myBroadcastCount,
            String latestMessageTime, String chatThreadId)
    {
        MessageType[] messageTypeArray = { MessageType.BROADCAST, MessageType.TEXT, MessageType.PHOTO, MessageType.PHONE_CALL };
        WSChatThread chatThread = new WSChatThread();

        chatThread.setChatThreadId(chatThreadId);
        chatThread.setMaxChatMessageIndex(maxChatMessageIndex);
        chatThread.setUnreadMessageCount(unreadMessageCount);
        chatThread.setLatestMessageTime(latestMessageTime);

        for (int i = 0; i < numberOfMessages; i++)
        {
            WSMessageResponse message = WSMessageResponse.dummyData(createdBy, toAccount, i, messageTypeArray[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true))
                    % messageTypeArray.length], i + ".) This is message " + 1, WSPhotoReference.dummyData(), chatThread.getChatThreadId(), WSBroadcast.dummyData(null));
            chatThread.addMessages(message);
        }

        return chatThread;
    }

    public static WSChatThread dummyDataWithType(int numberOfMessages, int maxChatMessageIndex, WSAccount createdBy, WSAccount toAccount, Integer unreadMessageCount, Integer myBroadcastCount,
            String latestMessageTime, MessageType[] messageTypeArray)
    {

        WSChatThread chatThread = new WSChatThread();

        // chatThread.setChatThreadId(Utils.randomUUID());
        // chatThread.setMaxChatMessageIndex(maxChatMessageIndex);
        // chatThread.setUnreadMessageCount(unreadMessageCount);
        // chatThread.setLatestMessageTime(latestMessageTime);

        for (int i = 0; i < numberOfMessages; i++)
        {
            WSMessageResponse message = WSMessageResponse.dummyData(createdBy, toAccount, i, messageTypeArray[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true))
                    % messageTypeArray.length], i + ".) This is message " + 1, WSPhotoReference.dummyData(), chatThread.getChatThreadId(), WSBroadcast.dummyData(null));
            chatThread.addMessages(message);
        }

        return chatThread;
    }

    public static void main(String[] args)
    {
        String endpoint = "/accounts/{account_ids}/broadcasts";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSAccount fromAccount = WSAccount.dummyData("Mark Lowry", "Mark and Reality", "+919876598765", false);
        WSAccount toAccount = WSAccount.dummyData("Greogory Gardner", "Mark and Reality", "+919988776655", false);
        WSChatThread responseData = WSChatThread.dummyDataWithType(10, 200, fromAccount, toAccount, 10, 20, System.currentTimeMillis() + "", new MessageType[] { MessageType.BROADCAST });
        WSResult result = new WSResult("200", "Broadcasts of account.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));
    }
}
