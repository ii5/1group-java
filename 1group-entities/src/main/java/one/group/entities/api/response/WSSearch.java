package one.group.entities.api.response;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSSearch implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String searchId;
    private String expiryTime;

    public WSSearch()
    {
    }

    public WSSearch(String searchId, String expiryTime)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(searchId), "SearchId should not be null or empty");
        Validation.isTrue(!Utils.isNull(expiryTime), "expiryTime must be valid");

        this.searchId = searchId;
        this.expiryTime = expiryTime;
    }

    public String getSearchId()
    {
        return searchId;
    }

    public void setSearchId(String searchId)
    {
        this.searchId = searchId;
    }

    public String getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expiryTime == null) ? 0 : expiryTime.hashCode());
        result = prime * result + ((searchId == null) ? 0 : searchId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSSearch other = (WSSearch) obj;
        if (expiryTime == null)
        {
            if (other.expiryTime != null)
                return false;
        }
        else if (!expiryTime.equals(other.expiryTime))
            return false;
        if (searchId == null)
        {
            if (other.searchId != null)
                return false;
        }
        else if (!searchId.equals(other.searchId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSSearch [searchId=" + searchId + ", expiryTime=" + expiryTime + "]";
    }

}
