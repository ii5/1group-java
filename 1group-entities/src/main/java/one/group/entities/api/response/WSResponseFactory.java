package one.group.entities.api.response;

import one.group.core.enums.EntityType;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.PropertyListing;

public class WSResponseFactory
{
    public ResponseEntity getEntity(EntityType type, Object object)
    {
        if (type == null)
            return null;

        if (type.equals(EntityType.PROPERTY_LISTING))
        {
            return new WSPropertyListing((PropertyListing) object);
        }
        else if (type.equals(EntityType.ACCOUNT))
        {
            return new WSAccount((Account) object);
        }
        else if (type.equals(EntityType.BROADCAST))
        {
            return new WSBroadcast();
        }
        return null;
    }
}
