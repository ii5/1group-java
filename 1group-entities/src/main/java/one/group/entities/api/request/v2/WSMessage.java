package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;

public class WSMessage implements RequestEntity
{
	private static final long serialVersionUID = 1L;
	private String id; 
	
    private String text;

    private String createdById;

    private String toAccountId;

    private String type;

    private String clientSentTime;

    private WSPhotoReference photo;

    private String broadcastId;

    private String broadcastShortReference;

    private String chatThreadId;
    
    private String createdByMobileNumber;

    public WSMessage()
    {

    }
    
    public String getId() 
    {
		return id;
	}
    
    public void setId(String id) 
    {
		this.id = id;
	}
    
	public String getCreatedByMobileNumber() 
	{
		return createdByMobileNumber;
	}

	public void setCreatedByMobileNumber(String createdByMobileNumber) 
	{
		this.createdByMobileNumber = createdByMobileNumber;
	}
	
    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getCreatedById()
    {
        return createdById;
    }

    public void setCreatedById(String createdById)
    {
        this.createdById = createdById;
    }

    public String getToAccountId()
    {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId)
    {
        this.toAccountId = toAccountId;
    }

    public String getClientSentTime()
    {
        return clientSentTime;
    }

    public void setClientSentTime(String clientSentTime)
    {
        this.clientSentTime = clientSentTime;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getBroadcastShortReference()
    {
        return broadcastShortReference;
    }

    public void setBroadcastShortReference(String broadcastShortReference)
    {
        this.broadcastShortReference = broadcastShortReference;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((broadcastId == null) ? 0 : broadcastId.hashCode());
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((clientSentTime == null) ? 0 : clientSentTime.hashCode());
        result = prime * result + ((createdById == null) ? 0 : createdById.hashCode());
        result = prime * result + ((photo == null) ? 0 : photo.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSMessage other = (WSMessage) obj;
        if (broadcastId == null)
        {
            if (other.broadcastId != null)
                return false;
        }
        else if (!broadcastId.equals(other.broadcastId))
            return false;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (clientSentTime == null)
        {
            if (other.clientSentTime != null)
                return false;
        }
        else if (!clientSentTime.equals(other.clientSentTime))
            return false;
        if (createdById == null)
        {
            if (other.createdById != null)
                return false;
        }
        else if (!createdById.equals(other.createdById))
            return false;
        if (photo == null)
        {
            if (other.photo != null)
                return false;
        }
        else if (!photo.equals(other.photo))
            return false;
        if (text == null)
        {
            if (other.text != null)
                return false;
        }
        else if (!text.equals(other.text))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSMessage [text=" + text + ", createdById=" + createdById + ", toAccountId=" + toAccountId + ", type=" + type + ", clientSentTime=" + clientSentTime + ", photo=" + photo
                + ", broadcastId=" + broadcastId + ", broadcastShortReference=" + broadcastShortReference + ", chatThreadId=" + chatThreadId + "]";
    }

}