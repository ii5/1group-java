package one.group.entities.api.request;

import one.group.core.Constant;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncRequest
{
    private Integer ackSyncIndex;
    
    private Integer maxResults;
    
    public WSSyncRequest()
    {
        this.ackSyncIndex = Constant.MINUS_ONE;
        this.maxResults = Constant.DEFAULT_SYNC_RESULT;
    }
    
    public Integer getAckSyncIndex()
    {
        return ackSyncIndex;
    }
    
    public void setAckSyncIndex(Integer ackSyncIndex)
    {
        this.ackSyncIndex = ackSyncIndex;
    }
    
    public Integer getMaxResults()
    {
        return maxResults;
    }
    
    public void setMaxResults(Integer maxResults)
    {
        this.maxResults = maxResults;
    }

    @Override
    public String toString()
    {
        return "WSSyncRequest [ackSyncIndex=" + ackSyncIndex + ", maxResults=" + maxResults + "]";
    }
    
    
}
