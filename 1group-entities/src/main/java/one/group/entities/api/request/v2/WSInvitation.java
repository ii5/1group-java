package one.group.entities.api.request.v2;

import java.util.List;

import one.group.entities.api.request.RequestEntity;

public class WSInvitation implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private List<WSNativeContact> contacts;

    private String mobileNumber;

    private String message;

    public List<WSNativeContact> getContacts()
    {
        return contacts;
    }

    public void setContacts(List<WSNativeContact> contacts)
    {
        this.contacts = contacts;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
