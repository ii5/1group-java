package one.group.entities.api.response.v2;

import java.util.UUID;

import one.group.core.Constant;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;

public class WSPaginationData implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private String id;

    private int currentPage;

    private int nextPage;

    private int previousPage;

    private long pageCount;

    private long totalRecords;

    public WSPaginationData()
    {

    }

    public WSPaginationData(String id, int currentPageNo, long totalRecords, int perPageRecords)
    {
        this.totalRecords = totalRecords;
        this.pageCount = Math.round(Math.ceil((float) totalRecords / perPageRecords));
        this.id = id;
        if (currentPageNo >= pageCount)
        {
            currentPageNo = (int) pageCount - 1;
        }
        if (currentPageNo > 1)
        {
            this.previousPage = currentPageNo - 1;
        }

        this.currentPage = currentPageNo;
        if (currentPageNo < (pageCount - 1))
        {
            this.nextPage = currentPageNo + 1;
        }
        else
        {
            this.nextPage = currentPageNo;
        }
    }

    public static void main(String[] args)
    {
        int recordsPerPage = 5;
        int currentPage = 0;
        int totalRecordsCount = 39;
        String pgId = UUID.randomUUID().toString();
        // WSPaginationData pagination = new WSPaginationData
        WSPaginationData pagination = new WSPaginationData(pgId, currentPage, totalRecordsCount, recordsPerPage);
        System.out.println(Utils.getJsonString(pagination));
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getNextPage()
    {
        return nextPage;
    }

    public void setNextPage(int nextPage)
    {
        this.nextPage = nextPage;
    }

    public int getPreviousPage()
    {
        return previousPage;
    }

    public void setPreviousPage(int previousPage)
    {
        this.previousPage = previousPage;
    }

    public long getPageCount()
    {
        return pageCount;
    }

    public void setPageCount(long pageCount)
    {
        this.pageCount = pageCount;
    }

    public long getTotalRecords()
    {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords)
    {
        this.totalRecords = totalRecords;
    }

    @Override
    public String toString()
    {
        return "WSPaginationData [id=" + id + ", currentPage=" + currentPage + ", nextPage=" + nextPage + ", previousPage=" + previousPage + ", pageCount=" + pageCount + ", totalRecords="
                + totalRecords + "]";
    }

    public static WSPaginationData dummyData()
    {
        WSPaginationData pgd = new WSPaginationData();
        pgd.setCurrentPage(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 1, false)));
        pgd.setId(Utils.randomUUID());
        pgd.setNextPage(pgd.getCurrentPage() + 1);
        pgd.setPageCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 1, false)) + pgd.getNextPage());
        pgd.setPreviousPage(pgd.getCurrentPage() - 1);

        pgd.setTotalRecords(20);
        return pgd;

    }

}
