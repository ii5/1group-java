/**
 * 
 */
package one.group.entities.api.response;

import java.util.List;

/**
 * @author ashishthorat
 *
 */
public class WSCityResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSCity> cities;

    public List<WSCity> getCities()
    {
        return cities;
    }

    public void setCities(List<WSCity> cities)
    {
        this.cities = cities;
    }

}
