package one.group.entities.api.request.bpo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import one.group.core.enums.AdminStatus;
import one.group.entities.api.request.v2.WSRequest;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAdminUserRequest
{
    private String username;
    private String email;
    private String password;
    private String passwordHash;
    private String authKey;
    private Date createdTime;
    private Date updatedTime;
    private AdminStatus status;
    private String id;
    private String cityId;
    private String unassignCityId;
    private Integer priority;
    private Boolean cleanCityAssignment = false;
    private List<String> roles;
    
    
    public WSAdminUserRequest()
    {
        // TODO Auto-generated constructor stub
    }
    
    public List<String> getRoles()
    {
        return roles;
    }
    
    public void setRoles(List<String> roles)
    {
        this.roles = roles;
    }
    
    public Boolean getCleanCityAssignment()
    {
        return cleanCityAssignment;
    }
    
    public void setCleanCityAssignment(Boolean cleanCityAssignment)
    {
        this.cleanCityAssignment = cleanCityAssignment;
    }
    
    public Integer getPriority()
    {
        return priority;
    }
    
    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }
    
    public String getUnassignCityId()
    {
        return unassignCityId;
    }
    
    public void setUnassignCityId(String unassignCityId)
    {
        this.unassignCityId = unassignCityId;
    }

    public String getUsername()
    {
        return username;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPassword()
    {
        return password;
    }

    public String getPasswordHash()
    {
        return passwordHash;
    }

    public String getAuthKey()
    {
        return authKey;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public AdminStatus getStatus()
    {
        return status;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setPasswordHash(String passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    public void setAuthKey(String authKey)
    {
        this.authKey = authKey;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public void setStatus(AdminStatus status)
    {
        this.status = status;
    }
    
    public String getId()
    {
        return id;
    }
    
    public String getCityId()
    {
        return cityId;
    }
    
    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public static WSAdminUserRequest dummyData()
    {
        WSAdminUserRequest req = new WSAdminUserRequest();
        req.setAuthKey(Utils.randomUUID());
        req.setCityId("1");
        req.setCreatedTime(new Date());
        req.setEmail("some@email.com");
        req.setId(Utils.randomUUID());
        req.setPassword("password");
        req.setPriority(2);
        req.setStatus(AdminStatus.ACTIVE);
        req.setUnassignCityId("2");
        req.setUpdatedTime(new Date());
        req.setUsername("user");
        
        return req;
    }
    
    public static void main(String[] args)
    {
        WSRequest req = new WSRequest();
        List<WSAdminUserRequest> adminList = new ArrayList<WSAdminUserRequest>();
        adminList.add(WSAdminUserRequest.dummyData());
        adminList.add(WSAdminUserRequest.dummyData());
        adminList.add(WSAdminUserRequest.dummyData());
        req.setUserCityList(adminList);
        System.out.println(Utils.getJsonString(req));
    }
}
