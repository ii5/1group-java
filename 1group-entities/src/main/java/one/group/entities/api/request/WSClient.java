package one.group.entities.api.request;

import one.group.core.Constant;
import one.group.core.enums.OAuthScope;
import one.group.core.enums.OAuthTokenType;
import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.Client;

/**
 * API Schema for Client
 * 
 * @author bhumikabhatt
 * 
 */

public class WSClient implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String id;
    private DeviceStatus status;
    private int initialSyncIndex = Constant.MIN_SYNC_INDEX;
    private String pushChannel;
    private String pushKey;
    private String accessToken;
    private String accountId;
    private OAuthScope scope;
    private OAuthTokenType tokenType;
    private WSAccount account;
    private String devicePlatform;
    private String appVersion;

    public WSClient()
    {
    };

    public WSClient(one.group.entities.api.response.WSClient client)
    {
        this.id = client.getId();
        this.status = client.getStatus();
        this.initialSyncIndex = client.getInitialSyncIndex();
        this.pushChannel = client.getPushChannel();
        this.pushKey = client.getPushKey();
        this.accessToken = client.getAccessToken();
        this.accountId = client.getAccountId();
        this.scope = client.getScope();
        this.tokenType = client.getTokenType();
    }

    public WSClient(Client client, int index, String accessToken, String accountId, OAuthScope scope, OAuthTokenType tokenType)
    {
        this.id = client.getIdAsString();
        this.pushChannel = client.getPushChannel();
        this.status = client.getStatus();
        this.initialSyncIndex = index;
        this.accessToken = accessToken;
        this.accountId = accountId;
        this.scope = scope;
        this.tokenType = tokenType;
        this.devicePlatform = client.getDevicePlatform();
    }

    public WSClient(Client client)
    {
        this.id = client.getIdAsString();
        this.pushChannel = client.getPushChannel();
        this.status = client.getStatus();
        this.account = new WSAccount(client.getAccount());
        this.devicePlatform = client.getDevicePlatform();
        this.appVersion = client.getAppVersion();
    }

    public WSAccount getAccount()
    {
        return account;
    }

    public void setAccount(WSAccount account)
    {
        this.account = account;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public DeviceStatus getStatus()
    {
        return status;
    }

    public void setStatus(DeviceStatus status)
    {
        this.status = status;
    }

    public int getInitialSyncIndex()
    {
        return initialSyncIndex;
    }

    public void setInitialSyncIndex(int initialSyncIndex)
    {
        this.initialSyncIndex = initialSyncIndex;
    }

    public String getPushChannel()
    {
        return pushChannel;
    }

    public void setPushChannel(String pushChannel)
    {
        this.pushChannel = pushChannel;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public OAuthScope getScope()
    {
        return scope;
    }

    public void setScope(OAuthScope scope)
    {
        this.scope = scope;
    }

    public OAuthTokenType getTokenType()
    {
        return tokenType;
    }

    public void setTokenType(OAuthTokenType tokenType)
    {
        this.tokenType = tokenType;
    }

    public String getPushKey()
    {
        return pushKey;
    }

    public void setPushKey(String pushKey)
    {
        this.pushKey = pushKey;
    }

    public String getClientPlatform()
    {
        return devicePlatform;
    }

    public void setClientPlatform(String clientPlatform)
    {
        this.devicePlatform = clientPlatform;
    }

    public String getAppVersion()
    {
        return appVersion;
    }

    public void setAppVersion(String appVersion)
    {
        this.appVersion = appVersion;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSClient other = (WSClient) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSClient [id=" + id + ", status=" + status + ", initialSyncIndex=" + initialSyncIndex + ", pushChannel=" + pushChannel + ", pushKey=" + pushKey + ", accessToken=" + accessToken
                + ", accountId=" + accountId + ", scope=" + scope + ", tokenType=" + tokenType + "]";
    }
}
