package one.group.entities.api.request;

/**
 * 
 * @author sanilshet
 *
 */
public class WSLocations implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String geopoint;

    private String name;

    private String parentId;

    private String type;

    private String zipCode;

    private String cityId;

    public String getGeopoint()
    {
        return geopoint;
    }

    public String getName()
    {
        return name;
    }

    public String getParentId()
    {
        return parentId;
    }

    public String getType()
    {
        return type;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setGeopoint(String geopoint)
    {
        this.geopoint = geopoint;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

}
