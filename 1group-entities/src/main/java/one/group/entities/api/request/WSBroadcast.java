/**
 * 
 */
package one.group.entities.api.request;

import java.util.List;

/**
 * @author ashishthorat
 *
 */
public class WSBroadcast implements RequestEntity
{
	private static final long serialVersionUID = 1L;
	
    private String broadcastId;

    private String messageId;

    private List<WSTag> tags;

    private String status;

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public List<WSTag> getTags()
    {
        return tags;
    }

    public void setTags(List<WSTag> tags)
    {
        this.tags = tags;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((broadcastId == null) ? 0 : broadcastId.hashCode());
        result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSBroadcast other = (WSBroadcast) obj;
        if (broadcastId == null)
        {
            if (other.broadcastId != null)
                return false;
        }
        else if (!broadcastId.equals(other.broadcastId))
            return false;
        if (messageId == null)
        {
            if (other.messageId != null)
                return false;
        }
        else if (!messageId.equals(other.messageId))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (tags == null)
        {
            if (other.tags != null)
                return false;
        }
        else if (!tags.equals(other.tags))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSBroadcast [broadcastId=" + broadcastId + ", messageId=" + messageId + ", tags=" + tags + ", status=" + status + "]";
    }

}
