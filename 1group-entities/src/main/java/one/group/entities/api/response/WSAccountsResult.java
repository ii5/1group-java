package one.group.entities.api.response;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAccountsResult
{
    private Set<WSAccount> accountsResult = new HashSet<WSAccount>();
    
    public Set<WSAccount> getAccountsResult()
    {
        return accountsResult;
    }
    
    public void setAccountsResult(Set<WSAccount> accountResult)
    {
        this.accountsResult = accountResult;
    }
    
    public void addAccountResult(WSAccount account)
    {
        this.accountsResult.add(account);
    }
}
