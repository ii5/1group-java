package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Search;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 * 
 */
public class WSBroadcastSearchQuery implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String locationId;

    private Integer minArea;

    private Integer maxArea;

    private String minPrice;

    private String maxPrice;

    private String transactionType;

    private String propertyType;

    private List<String> rooms;

    private String searchTime;

    private String searchId;

    private String broadcastType;

    private String cityId;

    private Integer area;

    private String price;

    private String propertySubType;

    public String getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(String propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public WSBroadcastSearchQuery()
    {

    }

    public WSBroadcastSearchQuery(String searchId, Search search)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(searchId), "Search id must not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(search.getLocationId()), "Location id is must be valid");
        Validation.isTrue(!Utils.isNull(search.getTransactionType()), "Transaction type should not be null or empty");
        Validation.isTrue(!Utils.isNull(search.getPropertyType()), "Property type should no be null or empty");
        // Validation.isTrue(!Utils.isEmpty(search.getPropertySubType()),
        // "Property sub type should not be null");

        this.searchId = searchId;
        this.locationId = search.getLocationId();
        if (String.valueOf(search.getMinArea()).equals("" + 0) && String.valueOf(search.getMaxArea()).equals("" + Integer.MAX_VALUE))
        {
            this.maxArea = null;
            this.minArea = null;
        }
        else
        {
            this.maxArea = search.getMaxArea();
            this.minArea = search.getMinArea();
        }

        if (String.valueOf(search.getMinPrice()).equals("0") && String.valueOf(search.getMaxPrice()).equals("" + Long.MAX_VALUE))
        {
            this.minPrice = null;
            this.maxPrice = null;
        }
        else
        {
            this.minPrice = String.valueOf(search.getMinPrice());
            this.maxPrice = String.valueOf(search.getMaxPrice());
        }

        if (!Utils.isNull(search.getPropertyType()))
        {
            this.propertyType = search.getPropertyType().toString();
        }
        if (!Utils.isNull(search.getRooms()))
        {
            // this.rooms = search.getRooms();
        }

        if (!Utils.isNull(search.getPropertySubType()))
        {
            this.propertySubType = search.getPropertySubType().toString();
        }

    }

    public WSBroadcastSearchQuery(Search search)
    {
        Validation.isTrue(!Utils.isNull(search), "Search must not be null or empty");

        this.searchId = search.getId();
        if (!Utils.isNull(search.getLocationId()))
        {
            this.locationId = search.getLocationId();
        }
        if (String.valueOf(search.getMinArea()).equals("" + 0) && String.valueOf(search.getMaxArea()).equals("" + Integer.MAX_VALUE))
        {
            this.maxArea = null;
            this.minArea = null;
        }
        else
        {
            this.maxArea = search.getMaxArea();
            this.minArea = search.getMinArea();
        }

        if (String.valueOf(search.getMinPrice()).equals("0") && String.valueOf(search.getMaxPrice()).equals("" + Long.MAX_VALUE))
        {
            this.minPrice = null;
            this.maxPrice = null;
        }
        else
        {
            this.minPrice = String.valueOf(search.getMinPrice());
            this.maxPrice = String.valueOf(search.getMaxPrice());
        }

        if (!Utils.isNull(search.getPropertyType()))
        {
            this.propertyType = search.getPropertyType().toString();
        }
        if (!Utils.isNull(search.getRooms()))
        {
            this.rooms = new ArrayList<String>(Arrays.asList(search.getRooms().split(",")));
        }
        this.transactionType = search.getTransactionType() == null ? null : search.getTransactionType().name();
        this.propertyType = search.getPropertyType() == null ? null : search.getPropertyType().name();
        this.propertySubType = search.getPropertySubType() == null ? null : search.getPropertySubType().name();
        this.broadcastType = search.getBroadcastType() == null ? null : search.getBroadcastType().name();

        this.cityId = search.getCityId();

    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(String propertyType)
    {
        this.propertyType = propertyType;
    }

    public Integer getMaxArea()
    {
        return maxArea;
    }

    public void setMaxArea(Integer maxArea)
    {
        this.maxArea = maxArea;
    }

    public Integer getMinArea()
    {
        return minArea;
    }

    public void setMinArea(Integer minArea)
    {
        this.minArea = minArea;
    }

    public String getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public List<String> getRooms()
    {
        return rooms;
    }

    public void setRooms(List<String> rooms)
    {
        this.rooms = rooms;
    }

    public String getSearchTime()
    {
        return searchTime;
    }

    public void setSearchTime(String searchTime)
    {
        this.searchTime = searchTime;
    }

    public String getSearchId()
    {
        return searchId;
    }

    public void setSearchId(String searchId)
    {
        this.searchId = searchId;
    }

    public String getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(String minPrice)
    {
        this.minPrice = minPrice;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getType()
    {
        return broadcastType;
    }

    public void setType(String type)
    {
        this.broadcastType = type;
    }

    public String getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(String broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public Integer getArea()
    {
        return area;
    }

    public void setArea(Integer area)
    {
        this.area = area;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "WSBroadcastSearchQuery [locationId=" + locationId + ", minArea=" + minArea + ", maxArea=" + maxArea + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + ", transactionType="
                + transactionType + ", propertyType=" + propertyType + ", rooms=" + rooms + ", searchTime=" + searchTime + ", searchId=" + searchId + ", broadcastType=" + broadcastType + ", cityId="
                + cityId + ", area=" + area + ", price=" + price + "]";
    }

}
