/**
 * 
 */
package one.group.entities.api.response;

import java.util.Date;
import java.util.List;

/**
 * @author ashishthorat
 *
 */
public class WSBroadcastResult
{
    private String broadcastId;

    private String shortReference;

    private List<WSBroadcast> broadcasts;

    private Date createdTime;

    public List<WSBroadcast> getBroadcasts()
    {
        return broadcasts;
    }

    public void setBroadcasts(List<WSBroadcast> broadcasts)
    {
        this.broadcasts = broadcasts;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

}
