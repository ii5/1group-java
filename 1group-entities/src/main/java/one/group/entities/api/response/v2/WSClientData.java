package one.group.entities.api.response.v2;

import one.group.core.enums.ClientUpdateType;

public class WSClientData
{
    private String currentClientVersion;

    private ClientUpdateType clientUpdateType;

    public String getCurrentClientVersion()
    {
        return currentClientVersion;
    }

    public ClientUpdateType getClientUpdateType()
    {
        return clientUpdateType;
    }

    public void setCurrentClientVersion(String currentClientVersion)
    {
        this.currentClientVersion = currentClientVersion;
    }

    public void setClientUpdateType(ClientUpdateType clientUpdateType)
    {
        this.clientUpdateType = clientUpdateType;
    }

    public WSClientData()
    {

    }

    public WSClientData(ClientUpdateType clientUpdateType, String currentClientVersion)
    {
        this.clientUpdateType = clientUpdateType;
        this.currentClientVersion = currentClientVersion;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clientUpdateType == null) ? 0 : clientUpdateType.hashCode());
        result = prime * result + ((currentClientVersion == null) ? 0 : currentClientVersion.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSClientData other = (WSClientData) obj;
        if (clientUpdateType != other.clientUpdateType)
            return false;
        if (currentClientVersion == null)
        {
            if (other.currentClientVersion != null)
                return false;
        }
        else if (!currentClientVersion.equals(other.currentClientVersion))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSClientData [currentClientVersion=" + currentClientVersion + ", clientUpdateType=" + clientUpdateType + "]";
    }

}
