    package one.group.entities.api.response.bpo;

import java.util.HashSet;
import java.util.Set;

import one.group.core.enums.AdminStatus;
import one.group.entities.jpa.User;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSUserMetaData
{
    private String value;
    
    private String label;
    
    private String id;
    
    private String username;
    
    private String email;
    
    private String authKey;
    
    private String password;
    
    private String passwordHash;
    
    private String passwordResetToken;
    
    private String createdAt;
    
    private String updatedAt;
    
    private String mobileNumber;
    
    private String localStorage;
    
    private String waMobileNumber;
    
    private AdminStatus status;
    
    private Set<String> roles = new HashSet<String>();;
    
    public WSUserMetaData() {}
    
    public WSUserMetaData(User user)
    {
        this.id = user.getId();
        this.waMobileNumber = user.getWaPhoneNumber();
    }
    
    public Set<String> getRoles()
    {
        return roles;
    }
    
    public void setRoles(Set<String> roles)
    {
        this.roles = roles;
    }
    
    public void addRole(String role)
    {
        this.roles.add(role);
    }
    
    public AdminStatus getStatus()
    {
        return status;
    }
    
    public void setStatus(AdminStatus status)
    {
        this.status = status;
    }
    
    public String getLocalStorage()
    {
        return localStorage;
    }
    
    public String getMobileNumber()
    {
        return mobileNumber;
    }
    
    public void setLocalStorage(String localStorage)
    {
        this.localStorage = localStorage;
    }
    
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }
    
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getAuthKey()
    {
        return authKey;
    }

    public void setAuthKey(String authKey)
    {
        this.authKey = authKey;
    }

    public String getPasswordHash()
    {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    public String getPasswordResetToken()
    {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken)
    {
        this.passwordResetToken = passwordResetToken;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt()
    {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getUsername()
    {
        return username;
    }
    
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getWaMobileNumber()
    {
        return waMobileNumber;
    }
    
    public void setWaMobileNumber(String waMobileNumber)
    {
        this.waMobileNumber = waMobileNumber;
    }
}
