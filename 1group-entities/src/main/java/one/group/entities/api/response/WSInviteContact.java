package one.group.entities.api.response;

import one.group.core.Constant;
import one.group.core.enums.InviteStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WSInviteContact implements ResponseEntity, Comparable<WSInviteContact>
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String number;
    @JsonIgnore
    private int groupCount;
    private InviteStatus status;
    private String name;

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    @JsonIgnore
    public int getGroupCount()
    {
        return groupCount;
    }

    @JsonIgnore
    public void setGroupCount(int groupCount)
    {
        this.groupCount = groupCount;
    }

    public InviteStatus getStatus()
    {
        return status;
    }

    public void setStatus(InviteStatus status)
    {
        this.status = status;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if (name != null)
        {
            String regStr = Constant.INVITE_NAME_BLACK_LIST_CHARS;
            String temp = name.replaceAll(regStr, "");
            if (temp.length() > 3)
            {
                this.name = name;
            }

        }
    }

    public WSInviteContact()
    {
        this.groupCount = 0;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSInviteContact other = (WSInviteContact) obj;
        if (number == null)
        {
            if (other.number != null)
                return false;
        }
        else if (!number.equals(other.number))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSInviteContact [phoneNumber=" + number + ", groupCount=" + groupCount + ", inviteStatus=" + status + ", name=" + name + "]";
    }

    public int compareTo(WSInviteContact ic)
    {
        if (ic.equals(this))
        {
            return 0;
        }

        if (ic.getName() == null || ic.getName().isEmpty())
        {
            return -1;
        }

        if (this.getName() == null || this.getName().isEmpty())
        {
            return 1;
        }
        int gc1 = ic.getGroupCount();
        int gc2 = this.getGroupCount();
        if (gc1 == gc2)
        {
            /*
             * if (ic.getName().equals(this.getName())) { return
             * ic.getPhoneNumber().compareToIgnoreCase(this.getPhoneNumber()); }
             */
            int isSame = this.getName().compareToIgnoreCase(ic.getName());
            if (isSame == 0)
            {
                return -1;
            }
            return this.getName().compareToIgnoreCase(ic.getName());
        }
        else
        {
            return new Integer(gc1).compareTo(gc2);
        }

    }

}
