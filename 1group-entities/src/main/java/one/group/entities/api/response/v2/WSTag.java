package one.group.entities.api.response.v2;

import java.util.Date;

import one.group.core.Constant;
import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;
import one.group.utils.Utils;

/**
 * 
 * @author miteshchavda
 * 
 */
public class WSTag implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String title;

    private Long size;

    private PropertyTransactionType transactionType;

    private BroadcastType broadcastType;

    private CommissionType commissionType;

    private String customSublocation;

    private Long maxPrice;

    private Long minPrice;

    private PropertyType propertyType;

    private PropertySubType propertySubType;

    private Rooms rooms;

    private WSLocation location;

    private Long price;

    private Long maxSize;

    private Long minSize;

    private String locationId;

    private String cityId;

    private String locationName;

    private String locationDisplayName;

    private String cityName;

    private BroadcastTagStatus status;

    private String draftedBy;

    private Date draftedTime;

    private String addedBy;

    private Date addedTime;

    private String id;

    private PropertyMarket propertyMarket;

    private String createdBy;

    private Date createdTime;

    private String updateBy;

    private Date UpdatedTime;

    public String getUpdateBy()
    {
        return updateBy;
    }

    public void setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;
    }

    public Date getUpdatedTime()
    {
        return UpdatedTime;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        UpdatedTime = updatedTime;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public BroadcastTagStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastTagStatus status)
    {
        this.status = status;
    }

    public String getDraftedBy()
    {
        return draftedBy;
    }

    public void setDraftedBy(String draftedBy)
    {
        this.draftedBy = draftedBy;
    }

    public Date getDraftedTime()
    {
        return draftedTime;
    }

    public void setDraftedTime(Date draftedTime)
    {
        this.draftedTime = draftedTime;
    }

    public String getAddedBy()
    {
        return addedBy;
    }

    public void setAddedBy(String addedBy)
    {
        this.addedBy = addedBy;
    }

    public Date getAddedTime()
    {
        return addedTime;
    }

    public void setAddedTime(Date addedTime)
    {
        this.addedTime = addedTime;
    }

    public Long getMaxSize()
    {
        return maxSize;
    }

    public void setMaxSize(Long maxSize)
    {
        this.maxSize = maxSize;
    }

    public Long getMinSize()
    {
        return minSize;
    }

    public void setMinSize(Long minSize)
    {
        this.minSize = minSize;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public Long getMaxPrice()
    {
        return maxPrice;
    }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    public WSTag(BroadcastTag tag)
    {
        this.size = tag.getSize();
        this.transactionType = tag.getTransactionType();
        this.broadcastType = tag.getBroadcastType();
        this.commissionType = tag.getCommissionType();
        this.customSublocation = tag.getCustomSublocation() == null ? "" : tag.getCustomSublocation();

        this.propertyType = tag.getPropertyType();

        this.rooms = tag.getRooms();

        this.maxPrice = tag.getMaximumPrice() == null ? 0 : tag.getMaximumPrice();
        this.minPrice = tag.getMinimumPrice() == null ? 0 : tag.getMinimumPrice();

        this.maxSize = tag.getMaximumSize() == null ? 0 : tag.getMaximumSize();
        this.minSize = tag.getMinimumSize() == null ? 0 : tag.getMinimumSize();
        this.price = tag.getPrice() == null ? 0 : tag.getPrice();
        this.size = tag.getSize() == null ? 0 : tag.getSize();

        Location location = tag.getLocation();
        this.locationId = location == null ? "" : location.getId();
        this.locationName = location == null ? "" : location.getLocalityName();
        this.locationDisplayName = location == null ? "" : location.getLocationDisplayName();
        this.cityId = tag.getCityId() == null ? "" : tag.getCityId();
        this.cityName = tag.getCity() == null ? "" : tag.getCity().getName();
        this.id = tag.getId();

        if (tag.getPropertySubType() != null)
        {
            this.propertySubType = tag.getPropertySubType();
        }

        this.status = tag.getStatus();

        if (tag.getAddedBy() != null)
        {
            this.addedBy = tag.getAddedBy();
            this.addedTime = tag.getAddedTime();
        }
        if (tag.getDraftedBy() != null)
        {
            this.draftedBy = tag.getDraftedBy();
            this.draftedTime = tag.getDraftedTime();
        }

        if (tag.getPropertyMarket() != null)
        {
            this.propertyMarket = tag.getPropertyMarket();
        }
        this.createdBy = tag.getCreatedBy();
        this.createdTime = tag.getCreatedTime();

        this.updateBy = tag.getUpdatedBy() != null ? tag.getUpdatedBy() : null;
        this.UpdatedTime = tag.getUpdatedTime() != null ? tag.getUpdatedTime() : null;
    }

    public WSTag(BroadcastTag tag, boolean withCity, boolean withLocation)
    {
        this.size = tag.getSize();
        this.transactionType = tag.getTransactionType();
        this.broadcastType = tag.getBroadcastType();
        this.commissionType = tag.getCommissionType();
        this.customSublocation = tag.getCustomSublocation() == null ? "" : tag.getCustomSublocation();

        this.propertyType = tag.getPropertyType();

        this.rooms = tag.getRooms();

        this.maxPrice = tag.getMaximumPrice() == null ? 0 : tag.getMaximumPrice();
        this.minPrice = tag.getMinimumPrice() == null ? 0 : tag.getMinimumPrice();

        this.maxSize = tag.getMaximumSize() == null ? 0 : tag.getMaximumSize();
        this.minSize = tag.getMinimumSize() == null ? 0 : tag.getMinimumSize();
        this.price = tag.getPrice() == null ? 0 : tag.getPrice();
        this.size = tag.getSize() == null ? 0 : tag.getSize();

        if (withLocation)
        {
            Location location = tag.getLocation();
            this.locationId = location == null ? "" : location.getId();
            this.locationName = location == null ? "" : location.getLocalityName();
            this.locationDisplayName = location == null ? "" : location.getLocationDisplayName();
        }

        if (withCity)
        {
            this.cityId = tag.getCityId() == null ? "" : tag.getCityId();
            this.cityName = tag.getCity() == null ? "" : tag.getCity().getName();
        }
        this.id = tag.getId();

        if (tag.getPropertySubType() != null)
        {
            this.propertySubType = tag.getPropertySubType();
        }

        this.status = tag.getStatus();

        if (tag.getAddedBy() != null)
        {
            this.addedBy = tag.getAddedBy();
            this.addedTime = tag.getAddedTime();
        }
        if (tag.getDraftedBy() != null)
        {
            this.draftedBy = tag.getDraftedBy();
            this.draftedTime = tag.getDraftedTime();
        }

        if (tag.getPropertyMarket() != null)
        {
            this.propertyMarket = tag.getPropertyMarket();
        }
        this.createdBy = tag.getCreatedBy();
        this.createdTime = tag.getCreatedTime();

        this.updateBy = tag.getUpdatedBy() != null ? tag.getUpdatedBy() : null;
        this.UpdatedTime = tag.getUpdatedTime() != null ? tag.getUpdatedTime() : null;
    }

    public WSTag(BroadcastTagsConsolidated tag)
    {
        this.title = tag.getBroadcastType() + "-" + tag.getTransactionType() + "-" + tag.getPrice() + "-" + tag.getSize();// TODO
        this.size = tag.getSize();
        this.transactionType = tag.getTransactionType();
        this.broadcastType = tag.getBroadcastType();
        this.commissionType = tag.getCommissionType();
        this.customSublocation = tag.getCustomSublocation() == null ? "" : tag.getCustomSublocation();

        this.propertyType = tag.getPropertyType();

        this.rooms = tag.getRooms();

        this.maxPrice = tag.getMaximumPrice() == null ? 0 : tag.getMaximumPrice();
        this.minPrice = tag.getMinimumPrice() == null ? 0 : tag.getMinimumPrice();

        this.maxSize = tag.getMaximumSize() == null ? 0 : tag.getMaximumSize();
        this.minSize = tag.getMinimumSize() == null ? 0 : tag.getMinimumSize();
        this.price = tag.getPrice() == null ? 0 : tag.getPrice();
        this.size = tag.getSize() == null ? 0 : tag.getSize();

        this.locationId = tag.getLocationId();
        this.cityId = tag.getCityId();

        this.cityName = tag.getCityName();
        this.locationName = tag.getLocationName();
        this.locationDisplayName = tag.getLocationDisplayName();

        this.maxPrice = tag.getMaximumPrice();
        this.minPrice = tag.getMinimumPrice();
        this.id = tag.getId();
        if (tag.getPropertySubType() != null)
        {
            this.propertySubType = tag.getPropertySubType();
        }

        this.status = tag.getStatus();

        if (tag.getAddedBy() != null)
        {
            this.addedBy = tag.getAddedBy();
            this.addedTime = tag.getAddedTime();
        }
        if (tag.getDraftedBy() != null)
        {
            this.draftedBy = tag.getDraftedBy();
            this.draftedTime = tag.getDraftedTime();
        }
        this.propertyMarket = tag.getPropertyMarket();
        this.createdBy = tag.getTagCreatedBy();
        this.createdTime = tag.getTagCreatedTime();

        this.updateBy = tag.getTagUpdatedBy() != null ? tag.getTagUpdatedBy() : null;
        this.UpdatedTime = tag.getTagUpdatedTime() != null ? tag.getTagUpdatedTime() : null;
    }

    public WSTag()
    {

    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getSize()
    {
        return size;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public PropertyTransactionType getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(PropertyTransactionType transactionType)
    {
        this.transactionType = transactionType;
    }

    public BroadcastType getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(BroadcastType broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public Long getMaximumPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public Long getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(Long minPrice)
    {
        this.minPrice = minPrice;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public Rooms getRooms()
    {
        return rooms;
    }

    public void setRooms(Rooms rooms)
    {
        this.rooms = rooms;
    }

    public WSLocation getLocation()
    {
        return location;
    }

    public void setLocation(WSLocation location)
    {
        this.location = location;
    }

    public Long getPrice()
    {
        return price;
    }

    public void setPrice(Long price)
    {
        this.price = price;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((addedBy == null) ? 0 : addedBy.hashCode());
        result = prime * result + ((addedTime == null) ? 0 : addedTime.hashCode());
        result = prime * result + ((broadcastType == null) ? 0 : broadcastType.hashCode());
        result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
        result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
        result = prime * result + ((commissionType == null) ? 0 : commissionType.hashCode());
        result = prime * result + ((customSublocation == null) ? 0 : customSublocation.hashCode());
        result = prime * result + ((draftedBy == null) ? 0 : draftedBy.hashCode());
        result = prime * result + ((draftedTime == null) ? 0 : draftedTime.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
        result = prime * result + ((maxPrice == null) ? 0 : maxPrice.hashCode());
        result = prime * result + ((maxSize == null) ? 0 : maxSize.hashCode());
        result = prime * result + ((minPrice == null) ? 0 : minPrice.hashCode());
        result = prime * result + ((minSize == null) ? 0 : minSize.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((propertySubType == null) ? 0 : propertySubType.hashCode());
        result = prime * result + ((propertyType == null) ? 0 : propertyType.hashCode());
        result = prime * result + ((rooms == null) ? 0 : rooms.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSTag other = (WSTag) obj;
        if (addedBy == null)
        {
            if (other.addedBy != null)
                return false;
        }
        else if (!addedBy.equals(other.addedBy))
            return false;
        if (addedTime == null)
        {
            if (other.addedTime != null)
                return false;
        }
        else if (!addedTime.equals(other.addedTime))
            return false;
        if (broadcastType != other.broadcastType)
            return false;
        if (cityId == null)
        {
            if (other.cityId != null)
                return false;
        }
        else if (!cityId.equals(other.cityId))
            return false;
        if (cityName == null)
        {
            if (other.cityName != null)
                return false;
        }
        else if (!cityName.equals(other.cityName))
            return false;
        if (commissionType != other.commissionType)
            return false;
        if (customSublocation == null)
        {
            if (other.customSublocation != null)
                return false;
        }
        else if (!customSublocation.equals(other.customSublocation))
            return false;
        if (draftedBy == null)
        {
            if (other.draftedBy != null)
                return false;
        }
        else if (!draftedBy.equals(other.draftedBy))
            return false;
        if (draftedTime == null)
        {
            if (other.draftedTime != null)
                return false;
        }
        else if (!draftedTime.equals(other.draftedTime))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        if (locationName == null)
        {
            if (other.locationName != null)
                return false;
        }
        else if (!locationName.equals(other.locationName))
            return false;
        if (maxPrice == null)
        {
            if (other.maxPrice != null)
                return false;
        }
        else if (!maxPrice.equals(other.maxPrice))
            return false;
        if (maxSize == null)
        {
            if (other.maxSize != null)
                return false;
        }
        else if (!maxSize.equals(other.maxSize))
            return false;
        if (minPrice == null)
        {
            if (other.minPrice != null)
                return false;
        }
        else if (!minPrice.equals(other.minPrice))
            return false;
        if (minSize == null)
        {
            if (other.minSize != null)
                return false;
        }
        else if (!minSize.equals(other.minSize))
            return false;
        if (price == null)
        {
            if (other.price != null)
                return false;
        }
        else if (!price.equals(other.price))
            return false;
        if (propertySubType != other.propertySubType)
            return false;
        if (propertyType != other.propertyType)
            return false;
        if (rooms != other.rooms)
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (status != other.status)
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        if (transactionType != other.transactionType)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSTag [title=" + title + ", size=" + size + ", transactionType=" + transactionType + ", broadcastType=" + broadcastType + ", commissionType=" + commissionType + ", customSublocation="
                + customSublocation + ", maxPrice=" + maxPrice + ", minPrice=" + minPrice + ", propertyType=" + propertyType + ", propertySubType=" + propertySubType + ", rooms=" + rooms
                + ", location=" + location + ", price=" + price + ", maxSize=" + maxSize + ", minSize=" + minSize + ", locationId=" + locationId + ", cityId=" + cityId + "]";
    }

    public static WSTag dummyData()
    {
        WSTag tag = new WSTag();
        tag.setSize(Long.parseLong(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        tag.setTransactionType(PropertyTransactionType.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % PropertyTransactionType.values().length]);
        tag.setBroadcastType(BroadcastType.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % BroadcastType.values().length]);
        tag.setCommissionType(CommissionType.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % CommissionType.values().length]);
        tag.setPropertyType(PropertyType.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % PropertyType.values().length]);
        tag.setPropertySubType(PropertySubType.getSubTypesOf(tag.getPropertyType()).get(
                Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % PropertySubType.getSubTypesOf(tag.getPropertyType()).size()));
        tag.setMaxPrice(Long.parseLong(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        tag.setMinPrice(Long.parseLong(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        tag.setRooms(Rooms.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % Rooms.values().length]);
        tag.setPrice(Long.parseLong(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        tag.setLocation(WSLocation.dummyData());
        tag.setTitle(tag.getBroadcastType() + "-" + tag.getTransactionType() + "-" + tag.getPrice() + "-" + tag.getSize());
        return tag;
    }
}
