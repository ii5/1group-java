package one.group.entities.api.response;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatCursor implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountId;
    private String chatThreadId;
    private WSChatThreadCursor cursor;

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public WSChatThreadCursor getCursor()
    {
        return cursor;
    }

    public void setCursor(WSChatThreadCursor cursor)
    {
        this.cursor = cursor;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((cursor == null) ? 0 : cursor.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatCursor other = (WSChatCursor) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (cursor == null)
        {
            if (other.cursor != null)
                return false;
        }
        else if (!cursor.equals(other.cursor))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatCursor [accountId=" + accountId + ", chatThreadId=" + chatThreadId + ", cursor=" + cursor + "]";
    }
}
