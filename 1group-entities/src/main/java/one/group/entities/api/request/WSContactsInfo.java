package one.group.entities.api.request;

import java.util.Set;

public class WSContactsInfo implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private boolean contactsSyncFlagFlipped;

    private Set<String> deltaContacts;

    public WSContactsInfo()
    {

    }

    public boolean isContactsSyncFlagFlipped()
    {
        return contactsSyncFlagFlipped;
    }

    public void setContactsSyncFlagFlipped(boolean contactsSyncFlagFlipped)
    {
        this.contactsSyncFlagFlipped = contactsSyncFlagFlipped;
    }

    public Set<String> getDeltaContacts()
    {
        return deltaContacts;
    }

    public void setDeltaContacts(Set<String> deltaContacts)
    {
        this.deltaContacts = deltaContacts;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (contactsSyncFlagFlipped ? 1231 : 1237);
        result = prime * result + ((deltaContacts == null) ? 0 : deltaContacts.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSContactsInfo other = (WSContactsInfo) obj;
        if (contactsSyncFlagFlipped != other.contactsSyncFlagFlipped)
            return false;
        if (deltaContacts == null)
        {
            if (other.deltaContacts != null)
                return false;
        }
        else if (!deltaContacts.equals(other.deltaContacts))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSContactsInfo [contactsSyncFlagFlipped=" + contactsSyncFlagFlipped + ", deltaContacts=" + deltaContacts + "]";
    }

}
