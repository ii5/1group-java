package one.group.entities.api.response.bpo;

import java.util.Date;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.v2.WSBroadcast;
import one.group.entities.socket.Message;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSMessageResponse implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String waMessageId;

    private String createdBy;

    private String waMobileNo;

    private String waGroupId;

    private String waMessageText;

    private Date waMessageTime;

    private Date recordDate;

    private MessageStatus status;

    private BpoMessageStatus bpoMessageStatus;

    private Date bpoStatusUpdatedTime;

    private String bpoStatusUpdatedBy;

    private boolean isDuplicate;

    private WSBroadcast broadcast;

    private long unreadMessageCount;

    private long totalMessageCount;

    private MessageType messageType;

    public WSMessageResponse()
    {
        // TODO Auto-generated constructor stub
    }

    public WSMessageResponse(Message m)
    {
        this.waMessageId = m.getId();
        this.bpoMessageStatus = m.getBpoMessageStatus();
        this.bpoStatusUpdatedBy = m.getBpoStatusUpdateBy();
        this.bpoStatusUpdatedTime = m.getBpoStatusUpdateTime();
        this.isDuplicate = m.getIsDuplicate();
        this.recordDate = m.getCreatedTime();
        this.status = m.getMessageStatus();
        this.waGroupId = m.getGroupId();
        this.waMessageText = m.getText();
        this.waMessageTime = m.getMessageSentTime();
        this.waMobileNo = m.getSenderPhoneNumber();
        this.createdBy = m.getCreatedBy();
        this.messageType = m.getMessageType();
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getWaMessageId()
    {
        return waMessageId;
    }

    public void setWaMessageId(String waMessageId)
    {
        this.waMessageId = waMessageId;
    }

    public String getWaMobileNo()
    {
        return waMobileNo;
    }

    public void setWaMobileNo(String waMobileNo)
    {
        this.waMobileNo = waMobileNo;
    }

    public String getWaGroupId()
    {
        return waGroupId;
    }

    public void setWaGroupId(String waGroupId)
    {
        this.waGroupId = waGroupId;
    }

    public String getWaMessageText()
    {
        return waMessageText;
    }

    public void setWaMessageText(String waMessageText)
    {
        this.waMessageText = waMessageText;
    }

    public Date getWaMessageTime()
    {
        return waMessageTime;
    }

    public void setWaMessageTime(Date waMessageTime)
    {
        this.waMessageTime = waMessageTime;
    }

    public Date getRecordDate()
    {
        return recordDate;
    }

    public void setRecordDate(Date recordDate)
    {
        this.recordDate = recordDate;
    }

    public MessageStatus getStatus()
    {
        return status;
    }

    public void setStatus(MessageStatus status)
    {
        this.status = status;
    }

    public BpoMessageStatus getBpoMessageStatus()
    {
        return bpoMessageStatus;
    }

    public void setBpoMessageStatus(BpoMessageStatus bpoMessageStatus)
    {
        this.bpoMessageStatus = bpoMessageStatus;
    }

    public Date getBpoStatusUpdatedTime()
    {
        return bpoStatusUpdatedTime;
    }

    public void setBpoStatusUpdatedTime(Date bpoStatusUpdatedTime)
    {
        this.bpoStatusUpdatedTime = bpoStatusUpdatedTime;
    }

    public String getBpoStatusUpdatedBy()
    {
        return bpoStatusUpdatedBy;
    }

    public void setBpoStatusUpdatedBy(String bpoStatusUpdatedBy)
    {
        this.bpoStatusUpdatedBy = bpoStatusUpdatedBy;
    }

    public boolean isDuplicate()
    {
        return isDuplicate;
    }

    public void setDuplicate(boolean isDuplicate)
    {
        this.isDuplicate = isDuplicate;
    }

    public WSBroadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSBroadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public long getUnreadMessageCount()
    {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(long unreadMessageCount)
    {
        this.unreadMessageCount = unreadMessageCount;
    }

    public long getTotalMessageCount()
    {
        return totalMessageCount;
    }

    public void setTotalMessageCount(long totalMessageCount)
    {
        this.totalMessageCount = totalMessageCount;
    }

    public MessageType getMessageType()
    {
        return messageType;
    }

    public void setMessageType(MessageType messageType)
    {
        this.messageType = messageType;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((waMessageId == null) ? 0 : waMessageId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSMessageResponse other = (WSMessageResponse) obj;
        if (waMessageId == null)
        {
            if (other.waMessageId != null)
                return false;
        }
        else if (!waMessageId.equals(other.waMessageId))
            return false;
        return true;
    }

}
