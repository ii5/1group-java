/**
 * 
 */
package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashishthorat
 *
 */
public class WSMessageResponseResult
{
    private List<WSMessageResponse> messages = new ArrayList<WSMessageResponse>();

    public List<WSMessageResponse> getMessages()
    {
        return messages;
    }

    public void setMessages(List<WSMessageResponse> messages)
    {
        this.messages = messages;
    }

}
