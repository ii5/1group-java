package one.group.entities.api.response;

import one.group.entities.jpa.BookMark;

/**
 * 
 * @author sanilshet
 *
 */
public class WSBookMarkPropertyListings implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String propertyListingId;

    private String shortReference;

    private WSPropertyListing propertyListingDetails;

    /**
     * @return the shortReference
     */
    public String getShortReference()
    {
        return shortReference;
    }

    /**
     * @param shortReference
     *            the shortReference to set
     */
    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    /**
     * @return the propertyListingId
     */
    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    /**
     * @param propertyListingId
     *            the propertyListingId to set
     */
    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public WSPropertyListing getPropertyListingDetails()
    {
        return propertyListingDetails;
    }

    public void setPropertyListingDetails(WSPropertyListing propertyListingDetails)
    {
        this.propertyListingDetails = propertyListingDetails;
    }

    public WSBookMarkPropertyListings()
    {
    };

    public WSBookMarkPropertyListings(BookMark bookMark)
    {
        this.setPropertyListingId(bookMark.getTargetEntity());

    }

}
