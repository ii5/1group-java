package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.APIVersion;
import one.group.core.enums.ClientUpdateType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long syncIndex;

    private String locationsDb;

    private APIVersion currentApiVersion;

    private String currentClientVersion;
    
    private String latestClientVersion;

    private ClientUpdateType clientUpdateType = ClientUpdateType.NO_UPDATE;

    private List<WSSyncUpdate> updates;

    public WSSyncResult(long syncIndex)
    {
        this();
        this.syncIndex = syncIndex;
        this.updates = new ArrayList<WSSyncUpdate>();

    }

    public WSSyncResult()
    {
        this.updates = new ArrayList<WSSyncUpdate>();
        this.currentApiVersion = APIVersion.currentVersion();
    }
    
    public String getLatestClientVersion() 
    {
		return latestClientVersion;
	}
    
    public void setLatestClientVersion(String latestClientVersion) 
    {
		this.latestClientVersion = latestClientVersion;
	}

    public APIVersion getCurrentApiVersion()
    {
        return currentApiVersion;
    }

    public void setCurrentApiVersion(APIVersion currentApiVersion)
    {
        this.currentApiVersion = currentApiVersion;
    }

    public String getLocationsDb()
    {
        return locationsDb;
    }

    public void setLocationsDb(String locationsDb)
    {
        this.locationsDb = locationsDb;
    }

    public long getSyncIndex()
    {
        return syncIndex;
    }

    public void setSyncIndex(long syncIndex)
    {
        this.syncIndex = syncIndex;
    }

    public List<WSSyncUpdate> getUpdates()
    {
        return updates;
    }

    public void setUpdates(List<WSSyncUpdate> updates)
    {
        this.updates = updates;
    }

    public void addUpdate(WSSyncUpdate update)
    {
        this.updates.add(update);
    }

    public String getCurrentClientVersion()
    {
        return currentClientVersion;
    }

    public ClientUpdateType getClientUpdateType()
    {
        return clientUpdateType;
    }

    public void setCurrentClientVersion(String currentClientVersion)
    {
        this.currentClientVersion = currentClientVersion;
    }

    public void setClientUpdateType(ClientUpdateType clientUpdateType)
    {
        this.clientUpdateType = clientUpdateType;
    }

    @Override
    public String toString()
    {
        return "WSSyncResult [syncIndex=" + syncIndex + ", locationsDb=" + locationsDb + ", currentApiVersion=" + currentApiVersion + ", currentClientVersion=" + currentClientVersion
                + ", clientUpdateType=" + clientUpdateType + ", updates=" + updates + "]";
    }
    
    public static WSSyncResult dummyData(int index)
    {
        WSSyncResult result = new WSSyncResult();
        result.setClientUpdateType(ClientUpdateType.HARD_UPDATE);
        result.setCurrentApiVersion(APIVersion.VERSION2);
        result.setLocationsDb("http:/location/to/locations.json");
        result.setSyncIndex(Integer.MAX_VALUE);
        index++;
        result.addUpdate(WSSyncUpdate.dummyDataMessage(index++));
        result.addUpdate(WSSyncUpdate.dummyDataBroadcast(index++));
        result.addUpdate(WSSyncUpdate.dummyDataChatCursor(index++));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataMessage(index++));
        result.addUpdate(WSSyncUpdate.dummyDataNotification(index++));
        result.addUpdate(WSSyncUpdate.dummyDataSubscription(index++));
        result.addUpdate(WSSyncUpdate.dummyDataNotification(index++));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataMessage(index++));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
//        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        result.addUpdate(WSSyncUpdate.dummyDataEntity(index++, SyncDataType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % SyncDataType.values().length]));
        
        return result;
    }

}
