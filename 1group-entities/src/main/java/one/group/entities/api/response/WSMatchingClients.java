package one.group.entities.api.response;

import one.group.entities.jpa.PropertySearchRelation;

public class WSMatchingClients implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String searchId;

    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSearchId()
    {
        return searchId;
    }

    public void setSearchId(String searchId)
    {
        this.searchId = searchId;
    }

    public WSMatchingClients(PropertySearchRelation propertySearchRelation)
    {
        this.searchId = propertySearchRelation.getSearchId().getId();
        // this.name =
        // propertySearchRelation.getSearchId().getRequirementName();
    };

}
