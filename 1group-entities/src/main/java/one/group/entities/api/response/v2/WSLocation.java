package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.core.enums.LocationType;
import one.group.core.enums.status.Status;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.LocationAdmin;
import one.group.utils.Utils;

/**
 * 
 * @author sanilshet
 *
 */
public class WSLocation implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private String subLocality;

    private String zipCode;

    private String type;

    private String parentId;

    private String cityId;

    private String latitude;

    private String longitude;

    private Status status;

    private WSCity city;

    private String locationDisplayName;

    public WSLocation()
    {
    };

    public WSLocation(Location locality)
    {
        this.setId(locality.getIdAsString());
        this.setName(locality.getLocalityName());
        this.setLocationDisplayName(locality.getLocationDisplayName());
    }

    public WSLocation(Location locality, Boolean isAdmin)
    {
        this.setId(locality.getIdAsString());
        this.setName(locality.getLocalityName());
        this.setLocationDisplayName(locality.getLocationDisplayName());
        this.setCityId(locality.getCity().getId());
        if (locality.getParentId() != null)
        {
            this.setParentId(locality.getParentId());
        }
        if (locality.getType() != null)
        {
            this.setType(locality.getType().toString());
        }
    }

    public WSLocation(LocationAdmin locality, Boolean isAdmin)
    {
        this.setId(locality.getId() + "");
        this.setName(locality.getLocalityName());
        this.setLocationDisplayName(locality.getLocationDisplayName());
        this.setCityId(locality.getCity().getId());
        if (locality.getParentId() != null)
        {
            this.setParentId(locality.getParentId().toString());
        }
    }

    /**
     * @return the city
     */
    public WSCity getCity()
    {
        return city;
    }

    /**
     * @return the latitude
     */
    public String getLatitude()
    {
        return latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude()
    {
        return longitude;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(WSCity city)
    {
        this.city = city;
        if (city != null)
        {
            this.cityId = city.getId();
        }
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    /**
     * @return the subLocality
     */
    public String getSubLocality()
    {
        return subLocality;
    }

    /**
     * @param subLocality
     *            the subLocality to set
     */
    public void setSubLocality(String subLocality)
    {
        this.subLocality = subLocality;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    @Override
    public String toString()
    {
        return "WSLocality [id=" + id + ", name=" + name + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((subLocality == null) ? 0 : subLocality.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSLocation other = (WSLocation) obj;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (cityId == null)
        {
            if (other.cityId != null)
                return false;
        }
        else if (!cityId.equals(other.cityId))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (latitude == null)
        {
            if (other.latitude != null)
                return false;
        }
        else if (!latitude.equals(other.latitude))
            return false;
        if (longitude == null)
        {
            if (other.longitude != null)
                return false;
        }
        else if (!longitude.equals(other.longitude))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parentId == null)
        {
            if (other.parentId != null)
                return false;
        }
        else if (!parentId.equals(other.parentId))
            return false;
        if (subLocality == null)
        {
            if (other.subLocality != null)
                return false;
        }
        else if (!subLocality.equals(other.subLocality))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        if (zipCode == null)
        {
            if (other.zipCode != null)
                return false;
        }
        else if (!zipCode.equals(other.zipCode))
            return false;
        return true;
    }
    
    public void trimForApp()
    {
    	this.setLocationDisplayName(null);
    }

    public static WSLocation dummyData()
    {
        WSLocation location = new WSLocation();

        location.setCity(WSCity.dummyData("Mumbai"));
        location.setCityId(location.getCity().getId());
        location.setId(Utils.randomUUID());
        location.setLatitude(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true));
        location.setLongitude(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true));
        location.setName("Name:" + location.getId());
        location.setParentId(Utils.randomUUID());
        location.setType(LocationType.LOCALITY.toString());

        return location;
    }

    public static WSLocation dummyData(WSCity city)
    {
        WSLocation location = new WSLocation();

        location.setCity(city);
        location.setCityId(city.getId());
        location.setId(Utils.randomUUID());
        location.setLatitude(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true));
        location.setLongitude(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true));
        location.setName("Name:" + location.getId());
        location.setParentId(Utils.randomUUID());
        location.setType(LocationType.LOCALITY.toString());

        return location;
    }

}
