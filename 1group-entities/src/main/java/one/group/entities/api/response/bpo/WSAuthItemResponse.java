package one.group.entities.api.response.bpo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import one.group.entities.jpa.AuthItem;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAuthItemResponse
{
    private Map<String, WSAuthItem> authItems = new HashMap<String, WSAuthItem>();
    
    private Map<String, Set<String>> authAssignments = new HashMap<String, Set<String>>();
    
    public Map<String, Set<String>> getAuthAssignments()
    {
        return authAssignments;
    }
    
    public void setAuthAssignments(Map<String, Set<String>> authAssignments)
    {
        this.authAssignments = authAssignments;
    }
    
    public void addAuthAssignment(String userId, String authAssignment)
    {
        Set<String> assignments = this.authAssignments.get(userId);
        
        if (assignments == null)
        {
            assignments = new HashSet<String>();
            this.authAssignments.put(userId, assignments);
        }
        
        assignments.add(authAssignment);
    }
    
    public Map<String, WSAuthItem> getAuthItems()
    {
        return authItems;
    }
    
    public void setAuthItems(Map<String, WSAuthItem> authItems)
    {
        this.authItems = authItems;
    }
    
    public void addAuthItem(String itemName, WSAuthItem authItem)
    {
        authItems.put(itemName, authItem);
    }
    
    public void addAuthItem(String itemName, AuthItem authItem, Set<String> children)
    {
        WSAuthItem wsAuthItem = new WSAuthItem(authItem);
        wsAuthItem.setChildren(children);
        
        authItems.put(itemName, wsAuthItem);
    }
    
    public void addChild(String itemName, String child)
    {
        if (authItems.get(itemName) != null)
        {
            authItems.get(itemName).addChildren(child);
        }
    }
}
