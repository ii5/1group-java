package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.request.WSAdminSMSRequest;

/**
 * The Class AdminRequest.
 * 
 * @author shweta.sankhe
 *
 */
public class AdminRequest implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String mobileNumber;

    private String deviceId;

    private String deviceOsVersion;

    private String devicePlatform;

    private String cpsId;

    private WSAccount account;

    private WSAdminSMSRequest smsRequest;

    public WSAdminSMSRequest getSmsRequest()
    {
        return smsRequest;
    }

    public void setSmsRequest(WSAdminSMSRequest smsRequest)
    {
        this.smsRequest = smsRequest;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getDeviceOsVersion()
    {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion)
    {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDevicePlatform()
    {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform)
    {
        this.devicePlatform = devicePlatform;
    }

    public WSAccount getAccount()
    {
        return account;
    }

    public void setAccount(WSAccount account)
    {
        this.account = account;
    }

    public String getCpsId()
    {
        return cpsId;
    }

    public void setCpsId(String cpsId)
    {
        this.cpsId = cpsId;
    }

    @Override
    public String toString()
    {
        return "AdminRequest [mobileNumber=" + mobileNumber + ", deviceId=" + deviceId + ", deviceOsVersion=" + deviceOsVersion + ", devicePlatform=" + devicePlatform + ", cpsId=" + cpsId
                + ", account=" + account + "]";
    }

}
