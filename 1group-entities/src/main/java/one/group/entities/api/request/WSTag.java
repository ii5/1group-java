/**
 * 
 */
package one.group.entities.api.request;

import java.util.List;

import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;

/**
 * @author miteshchavda
 *
 */
public class WSTag implements RequestEntity
{
    private String id;

    private String type;

    private PropertyType propertyType;

    private PropertySubType propertySubType;

    private String area;

    private CommissionType commissionType;

    private String description;

    private Rooms rooms;

    private String salePrice;

    private String rentPrice;

    private String locationId;

    private PropertyMarket propertyMarket;

    private String isHot;

    private BroadcastStatus status;

    private List<String> amenities;

    private String messageId;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Rooms getRooms()
    {
        return rooms;
    }

    public void setRooms(Rooms rooms)
    {
        this.rooms = rooms;
    }

    public String getSalePrice()
    {
        return salePrice;
    }

    public void setSalePrice(String salePrice)
    {
        this.salePrice = salePrice;
    }

    public String getRentPrice()
    {
        return rentPrice;
    }

    public void setRentPrice(String rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public String getIsHot()
    {
        return isHot;
    }

    public void setIsHot(String isHot)
    {
        this.isHot = isHot;
    }

    public BroadcastStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastStatus status)
    {
        this.status = status;
    }

    public List<String> getAmenities()
    {
        return amenities;
    }

    public void setAmenities(List<String> amenities)
    {
        this.amenities = amenities;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amenities == null) ? 0 : amenities.hashCode());
        result = prime * result + ((area == null) ? 0 : area.hashCode());
        result = prime * result + ((commissionType == null) ? 0 : commissionType.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((isHot == null) ? 0 : isHot.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
        result = prime * result + ((propertyMarket == null) ? 0 : propertyMarket.hashCode());
        result = prime * result + ((propertySubType == null) ? 0 : propertySubType.hashCode());
        result = prime * result + ((propertyType == null) ? 0 : propertyType.hashCode());
        result = prime * result + ((rentPrice == null) ? 0 : rentPrice.hashCode());
        result = prime * result + ((rooms == null) ? 0 : rooms.hashCode());
        result = prime * result + ((salePrice == null) ? 0 : salePrice.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSTag other = (WSTag) obj;
        if (amenities == null)
        {
            if (other.amenities != null)
                return false;
        }
        else if (!amenities.equals(other.amenities))
            return false;
        if (area == null)
        {
            if (other.area != null)
                return false;
        }
        else if (!area.equals(other.area))
            return false;
        if (commissionType != other.commissionType)
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (isHot == null)
        {
            if (other.isHot != null)
                return false;
        }
        else if (!isHot.equals(other.isHot))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        if (messageId == null)
        {
            if (other.messageId != null)
                return false;
        }
        else if (!messageId.equals(other.messageId))
            return false;
        if (propertyMarket != other.propertyMarket)
            return false;
        if (propertySubType != other.propertySubType)
            return false;
        if (propertyType != other.propertyType)
            return false;
        if (rentPrice == null)
        {
            if (other.rentPrice != null)
                return false;
        }
        else if (!rentPrice.equals(other.rentPrice))
            return false;
        if (rooms != other.rooms)
            return false;
        if (salePrice == null)
        {
            if (other.salePrice != null)
                return false;
        }
        else if (!salePrice.equals(other.salePrice))
            return false;
        if (status != other.status)
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSTag [id=" + id + ", type=" + type + ", propertyType=" + propertyType + ", propertySubType=" + propertySubType + ", area=" + area + ", commissionType=" + commissionType
                + ", description=" + description + ", rooms=" + rooms + ", salePrice=" + salePrice + ", rentPrice=" + rentPrice + ", locationId=" + locationId + ", propertyMarket=" + propertyMarket
                + ", isHot=" + isHot + ", status=" + status + ", amenities=" + amenities + ", messageId=" + messageId + "]";
    }

}
