package one.group.entities.api.request;

/**
 * Request structure for the request OTP 
 * @author nyalfernandes
 *
 */
public class WSRequestOtp implements RequestEntity
{
    /**
     * Required. Identifies the mobile number for which otp is to be generated and sent via sms. If the mobile_number parameter does not meet the format spcification, a 400 BAD REQUEST exception is returned.
     */
    private String mobileNumber;
    
    public WSRequestOtp()
    {
    }
    
    public WSRequestOtp(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString()
    {
        return "WSRequestOtp [mobileNumber=" + mobileNumber + "]";
    }

}
