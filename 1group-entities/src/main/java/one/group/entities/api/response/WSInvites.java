package one.group.entities.api.response;

public class WSInvites implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WSInvitesList invites;

    public WSInvitesList getInvites()
    {
        return invites;
    }

    public void setInvites(WSInvitesList invites)
    {
        this.invites = invites;
    }

    public WSInvites()
    {
    }
}
