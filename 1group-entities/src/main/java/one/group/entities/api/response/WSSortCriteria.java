package one.group.entities.api.response;

public class WSSortCriteria implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String field;

    private String direction;

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public String getDirection()
    {
        return direction;
    }

    public void setDirection(String direction)
    {
        this.direction = direction;
    }

    @Override
    public String toString()
    {
        return "WSSortCriteria [field=" + field + ", direction=" + direction + "]";
    }

}
