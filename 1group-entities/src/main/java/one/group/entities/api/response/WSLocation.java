package one.group.entities.api.response;

import one.group.core.enums.LocationType;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.LocationAdmin;

/**
 * 
 * @author sanilshet
 *
 */
public class WSLocation implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private String subLocality;

    private String city;

    private String zipCode;
    
    private LocationType type;

    private String parentId;

    private String cityId;
    
    private String geopoint;
    

    public WSLocation()
    {
    };

    public WSLocation(Location locality)
    {
        this.setId(locality.getIdAsString());
        this.setName(locality.getLocalityName());
        this.setParentId(locality.getParentId());
    }

    public WSLocation(Location locality, Boolean isAdmin)
    {
        this.setId(locality.getIdAsString());
        this.setName(locality.getLocalityName());
        this.setCityId(locality.getCity().getId());
        if (locality.getParentId() != null)
        {
            this.setParentId(locality.getParentId());
        }
        if (locality.getType() != null)
        {
        	this.setType(locality.getType());
        }
    }

    public WSLocation(LocationAdmin locality, Boolean isAdmin)
    {
        this.setId(locality.getId() + "");
        this.setName(locality.getLocalityName());
        this.setCityId(locality.getCity().getId());
        if (locality.getParentId() != null)
        {
            this.setParentId(locality.getParentId().toString());
        }
    }
    
    public String getGeopoint()
    {
        return geopoint;
    }
    
    public void setGeopoint(String geopoint)
    {
        this.geopoint = geopoint;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @return the latitude
     */
    public String getLatitude()
    {
        return latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude()
    {
        return longitude;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    private String latitude;

    private String longitude;

    /**
     * @return the subLocality
     */
    public String getSubLocality()
    {
        return subLocality;
    }

    /**
     * @param subLocality
     *            the subLocality to set
     */
    public void setSubLocality(String subLocality)
    {
        this.subLocality = subLocality;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public LocationType getType() 
    {
		return type;
	}

	public void setType(LocationType type) 
	{
		this.type = type;
	}

	@Override
    public String toString()
    {
        return "WSLocality [id=" + id + ", name=" + name + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSLocation other = (WSLocation) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

	

    

}
