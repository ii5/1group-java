package one.group.entities.api.response;

import java.util.List;

public class WSNotificationListResult implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	private List<WSNotification> notifications;

    public List<WSNotification> getNotifications()
    {
        return notifications;
    }

    public void setNotifications(List<WSNotification> notifications)
    {
        this.notifications = notifications;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((notifications == null) ? 0 : notifications.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSNotificationListResult other = (WSNotificationListResult) obj;
        if (notifications == null)
        {
            if (other.notifications != null)
                return false;
        }
        else if (!notifications.equals(other.notifications))
            return false;
        return true;
    }

}
