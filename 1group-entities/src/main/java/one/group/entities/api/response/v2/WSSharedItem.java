package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSharedItem
{
    private String accountId;

    private List<WSMessageResponse> sharedBroadcasts = new ArrayList<WSMessageResponse>();

    private List<WSPhotoReference> sharedMedia = new ArrayList<WSPhotoReference>();

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public List<WSMessageResponse> getSharedBroadcasts()
    {
        return sharedBroadcasts;
    }

    public void setSharedBroadcasts(List<WSMessageResponse> sharedBroadcasts)
    {
        this.sharedBroadcasts = sharedBroadcasts;
    }

    public void addSharedBroadcast(WSMessageResponse broadcast)
    {
        this.sharedBroadcasts.add(broadcast);
    }

    public List<WSPhotoReference> getSharedMedia()
    {
        return sharedMedia;
    }

    public void setSharedMedia(List<WSPhotoReference> sharedMedia)
    {
        this.sharedMedia = sharedMedia;
    }

    public void addSharedMedia(WSPhotoReference media)
    {
        this.sharedMedia.add(media);
    }

    public static WSSharedItem dummyData()
    {
        WSSharedItem response = new WSSharedItem();
        response.setAccountId(Utils.randomUUID());

        for (int i = 0; i < 5; i++)
        {
            response.addSharedBroadcast(
                    WSMessageResponse
                            .dummyData(
                                    WSAccount.dummyData(Utils.generateAccountReference(),
                                            Utils.generateAccountReference(),
                                            "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true),
                                            false),
                                    WSAccount
                                            .dummyData(Utils.generateAccountReference(),
                                                    Utils.generateAccountReference(),
                                                    "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO,
                                                            10, true),
                                                    false),
                                    20, MessageType.BROADCAST, "Hola!", null, Utils.randomUUID(),
                                    WSBroadcast.dummyData(Utils.randomUUID())));
        }

        for (int i = 0; i < 15; i++)
        {
            response.addSharedMedia(WSPhotoReference.dummyData());
        }

        return response;
    }

    public static void main(String[] args)
    {
        String endpoint = "/chat_threads/shared/items";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSSharedItem responseData = WSSharedItem.dummyData();
        WSResult result = new WSResult("200", "Shared items of your account.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));

    }
}
