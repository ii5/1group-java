package one.group.entities.api.response;

public class WSPropertyListingEditResult
{
    private String propertyListingId;

    private String shortReference;

    private String expiryTime;

    public String getExpiryTime()
    {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

}
