package one.group.entities.api.request;

public class WSLocationMasterData implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private String cityName;
    private String cityId;

    public String getCityName()
    {
        return cityName;
    }

    public String getUrl()
    {
        return url;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    private String url;

    public WSLocationMasterData()
    {

    }

    public WSLocationMasterData(WSLocationMasterData data)
    {
        this.cityName = data.getCityName();
        this.cityId = data.getCityId();
        this.url = data.getUrl();
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSLocationMasterData other = (WSLocationMasterData) obj;
        if (cityName == null)
        {
            if (other.cityName != null)
                return false;
        }
        else if (!cityName.equals(other.cityName))
            return false;
        if (url == null)
        {
            if (other.url != null)
                return false;
        }
        else if (!url.equals(other.url))
            return false;
        return true;
    }

}
