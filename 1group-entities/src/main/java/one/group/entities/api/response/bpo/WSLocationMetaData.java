package one.group.entities.api.response.bpo;

import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSLocationMetaData
{
    private String value;
    
    private String label;
    
    private String id;
    
    public String getId()
    {
        return id;
    }
    
    public String getLabel()
    {
        return label;
    }
    
    public String getValue()
    {
        return value;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public void setLabel(String label)
    {
        this.label = label;
    }
    
    public void setValue(String value)
    {
        this.value = value;
    }
    
    
    public static WSLocationMetaData dummyData()
    {
        WSLocationMetaData o = new WSLocationMetaData();
        o.setId(Utils.randomUUID());
        o.setLabel(Utils.generateAccountReference());
        o.setValue(o.getLabel());
        
        return o;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WSLocationMetaData other = (WSLocationMetaData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WSLocationMetaData [value=" + value + ", label=" + label + ", id=" + id + "]";
	}
	
	
    
}
