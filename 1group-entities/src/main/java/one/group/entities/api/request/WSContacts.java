package one.group.entities.api.request;

import java.util.List;

public class WSContacts implements RequestEntity
{
	private static final long serialVersionUID = 1L;
    private List<WSNativeContact> nativeContacts;

    private boolean initialContactsSyncComplete;

    public boolean isInitialContactsSyncComplete()
    {
        return initialContactsSyncComplete;
    }

    public void setInitialContactsSyncComplete(boolean initialContactsSyncComplete)
    {
        this.initialContactsSyncComplete = initialContactsSyncComplete;
    }

    public List<WSNativeContact> getNativeContacts()
    {
        return nativeContacts;
    }

    public void setNativeContacts(List<WSNativeContact> nativeContacts)
    {
        this.nativeContacts = nativeContacts;
    }

}
