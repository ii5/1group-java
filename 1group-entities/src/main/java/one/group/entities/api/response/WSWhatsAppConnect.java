package one.group.entities.api.response;

public class WSWhatsAppConnect
{
    private String totalConnectedGroups;
    private String inviteStatus;
    private boolean everSync;

    public WSWhatsAppConnect()
    {

    }

    public String getTotalConnectedGroups()
    {
        return totalConnectedGroups;
    }

    public void setTotalConnectedGroups(String totalConnectedGroups)
    {
        this.totalConnectedGroups = totalConnectedGroups;
    }

    public String getInviteStatus()
    {
        return inviteStatus;
    }

    public void setInviteStatus(String inviteStatus)
    {
        this.inviteStatus = inviteStatus;
    }

    public boolean isEverSync()
    {
        return everSync;
    }

    public void setEverSync(boolean everSync)
    {
        this.everSync = everSync;
    }

}
