package one.group.entities.api.response;

import java.util.Set;

public class WSInvitesList implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Set<WSInviteContact> recommended;

    Set<WSInviteContact> other;

    public WSInvitesList()
    {

    }

    public Set<WSInviteContact> getRecommended()
    {
        return recommended;
    }

    public void setRecommended(Set<WSInviteContact> recommended)
    {
        this.recommended = recommended;
    }

    public Set<WSInviteContact> getOther()
    {
        return other;
    }

    public void setOther(Set<WSInviteContact> other)
    {
        this.other = other;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((other == null) ? 0 : other.hashCode());
        result = prime * result + ((recommended == null) ? 0 : recommended.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSInvitesList other = (WSInvitesList) obj;
        if (this.other == null)
        {
            if (other.other != null)
                return false;
        }
        else if (!this.other.equals(other.other))
            return false;
        if (recommended == null)
        {
            if (other.recommended != null)
                return false;
        }
        else if (!recommended.equals(other.recommended))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSInvites [recommended=" + recommended + ", other=" + other + "]";
    }
}
