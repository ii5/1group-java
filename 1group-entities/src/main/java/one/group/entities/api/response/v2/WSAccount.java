package one.group.entities.api.response.v2;

import java.security.SecureRandom;

import one.group.core.Constant;
import one.group.core.enums.AccountType;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.status.Status;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSCity;
import one.group.entities.jpa.Account;
import one.group.utils.Utils;

public class WSAccount implements ResponseEntity
{

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String id;

    private String fullName;

    private String companyName;

    private AccountType type;

    private String primaryMobile;

    private String shortReference;

    private WSPhotoReference photo;

    private WSCity city;

    private Status status;

    private Integer score = 0;

    private Boolean isBlocked = false;

    private Boolean isContact = false;

    private Boolean canChat = false;

    private String createdTime;

    private String lastActivityTime;

    private Long broadcastCount = 0l;

    private String registrationTime;

    private WSChatThreadCursor cursors;

    private Integer propertyListingCount;

    private boolean isOnline;

    private Boolean isInitialSyncComplete;

    private SyncStatus syncStatus;

    private GroupSelectionStatus groupSelectionStatus;

    private Integer activeGroupCount;

    private Integer approvedGroupCount;

    // FOR SYNC
    private String localStorage;

    public WSAccount()
    {
        this.canChat = true;
    }

    /**
     * 
     * @param account
     */
    public WSAccount(Account account)
    {
        this.fullName = account.getFullName();
        this.companyName = account.getCompanyName();
        this.id = account.getIdAsString();
        this.type = account.getType();
        this.status = account.getStatus();
        this.primaryMobile = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;
        this.isInitialSyncComplete = account.isInitialContactsSyncComplete();

        if (account.getFullPhoto() != null)
        {
            this.photo = new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto());
        }

        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }
        if (account.getCity() != null)
        {
            this.city = new WSCity(account.getCity());
        }

        this.syncStatus = account.getSyncStatus();

        this.groupSelectionStatus = account.getGroupSelectionStatus();

        this.activeGroupCount = account.getActiveGroupCount();

        this.approvedGroupCount = account.getApprovedGroupCount();

    }

    public WSAccount(Account account, boolean isCity, boolean isPhoto)
    {
        if (account.getFullName() != null)
        {
            this.fullName = account.getFullName();
        }
        if (account.getCompanyName() != null)
        {
            this.companyName = account.getCompanyName();
        }

        this.id = account.getIdAsString();
        this.type = account.getType();
        this.status = account.getStatus();
        this.primaryMobile = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;
        this.syncStatus = account.getSyncStatus();

        this.groupSelectionStatus = account.getGroupSelectionStatus();

        this.activeGroupCount = account.getActiveGroupCount();

        this.approvedGroupCount = account.getApprovedGroupCount();

        if (isCity)
        {
            if (account.getCity() != null)
            {
                WSCity wsCity = new WSCity(account.getCity());
                setCity(wsCity);
            }
        }

        if (isPhoto)
        {
            if (account.getFullPhoto() != null)
            {
                this.photo = new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto());
            }
        }
        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }

        this.createdTime = account.getCreatedTime().getTime() + "";

        if (account.getStatus() != null)
        {
            if ((account.getStatus().equals(Status.ACTIVE)) || (account.getStatus().equals(Status.SEEDED)))
            {
                this.canChat = true;
            }
            else
            {
                this.canChat = false;
            }
        }

        if (account.getRegistrationTime() != null)
        {
            this.registrationTime = account.getRegistrationTime().getTime() + "";
            this.lastActivityTime = account.getRegistrationTime().getTime() + ""; // TODO
        }
    }

    public WSAccount(Account account, Boolean isAdmin)
    {
        if (account.getCreatedTime() != null)
        {
            this.createdTime = "" + account.getCreatedTime().getTime();
        }
        this.status = account.getStatus();
        this.id = account.getId();
        this.syncStatus = account.getSyncStatus();

        this.groupSelectionStatus = account.getGroupSelectionStatus();

        this.activeGroupCount = account.getActiveGroupCount();

        this.approvedGroupCount = account.getApprovedGroupCount();
    }

    public WSAccount(Account account, boolean isPhoto, boolean isCity, boolean isPhoneNumber)
    {
        this.fullName = account.getFullName();
        this.companyName = account.getCompanyName();
        this.id = account.getIdAsString();
        this.type = account.getType();
        this.status = account.getStatus();
        if (isPhoneNumber)
        {
            this.primaryMobile = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;
        }

        this.isInitialSyncComplete = account.isInitialContactsSyncComplete();

        if (isPhoto)
        {
            this.photo = account.getFullPhoto() != null ? new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto()) : null;
        }

        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }

        if (isCity)
        {
            this.city = account.getCity() != null ? new WSCity(account.getCity()) : null;
        }

        this.syncStatus = account.getSyncStatus();

        this.groupSelectionStatus = account.getGroupSelectionStatus();

        this.activeGroupCount = account.getActiveGroupCount();

        this.approvedGroupCount = account.getApprovedGroupCount();

        if (account.getStatus() != null)
        {
            if ((account.getStatus().equals(Status.ACTIVE)) || (account.getStatus().equals(Status.SEEDED)))
            {
                this.canChat = true;
            }
            else
            {
                this.canChat = false;
            }
        }
    }

    public SyncStatus getSyncStatus()
    {
        return syncStatus;
    }

    public void setSyncStatus(SyncStatus syncStatus)
    {
        this.syncStatus = syncStatus;
    }

    public GroupSelectionStatus getGroupSelectionStatus()
    {
        return groupSelectionStatus;
    }

    public void setGroupSelectionStatus(GroupSelectionStatus groupSelectionStatus)
    {
        this.groupSelectionStatus = groupSelectionStatus;
    }

    public Integer getActiveGroupCount()
    {
        return activeGroupCount;
    }

    public void setActiveGroupCount(Integer activeGroupCount)
    {
        this.activeGroupCount = activeGroupCount;
    }

    public Integer getApprovedGroupCount()
    {
        return approvedGroupCount;
    }

    public void setApprovedGroupCount(Integer approvedGroupCount)
    {
        this.approvedGroupCount = approvedGroupCount;
    }

    public boolean isOnline()
    {
        return isOnline;
    }

    public String getLocalStorage()
    {
        return localStorage;
    }

    public void setLocalStorage(String localStorage)
    {
        this.localStorage = localStorage;
    }

    public WSChatThreadCursor getCursors()
    {
        return cursors;
    }

    public void setCursors(WSChatThreadCursor cursors)
    {
        this.cursors = cursors;
    }

    public Long getBroadcastCount()
    {
        return broadcastCount;
    }

    public void setBroadcastCount(Long broadcastCount)
    {
        this.broadcastCount = broadcastCount;
    }

    public Boolean getCanChat()
    {
        return canChat;
    }

    public void setCanChat(Boolean canChat)
    {
        this.canChat = canChat;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public AccountType getType()
    {
        return type;
    }

    public void setType(AccountType type)
    {
        this.type = type;
    }

    public String getPrimaryMobile()
    {
        return primaryMobile;
    }

    public void setPrimaryMobile(String primaryMobile)
    {
        this.primaryMobile = primaryMobile;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public WSCity getCity()
    {
        return city;
    }

    public void setCity(WSCity city)
    {
        this.city = city;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Boolean getIsBlocked()
    {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Boolean getIsContact()
    {
        return isContact;
    }

    public void setIsContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(String lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public String getRegistrationTime()
    {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime)
    {
        this.registrationTime = registrationTime;
    }

    public Integer getPropertyListingCount()
    {
        return propertyListingCount;
    }

    public void setPropertyListingCount(Integer propertyListingCount)
    {
        this.propertyListingCount = propertyListingCount;
    }

    public void setOnline(boolean isOnline)
    {
        this.isOnline = isOnline;
    }

    public boolean getOnline()
    {
        return this.isOnline;
    }

    public Boolean isInitialSyncComplete()
    {
        return isInitialSyncComplete;
    }

    public void setInitialSyncComplete(Boolean isInitialSyncComplete)
    {
        this.isInitialSyncComplete = isInitialSyncComplete;
    }

    @Override
    public String toString()
    {
        return "WSAccount [id=" + id + ", fullName=" + fullName + ", companyName=" + companyName + ", type=" + type + ", primaryMobile=" + primaryMobile + ", shortReference=" + shortReference
                + ", photo=" + photo + ", city=" + city + ", status=" + status + ", score=" + score + ", isBlocked=" + isBlocked + ", isContact=" + isContact + ", canChat=" + canChat
                + ", createdTime=" + createdTime + ", lastActivityTime=" + lastActivityTime + ", broadcastCount=" + broadcastCount + ", registrationTime=" + registrationTime + ", cursors=" + cursors
                + ", propertyListingCount=" + propertyListingCount + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccount other = (WSAccount) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public static WSAccount dummyData(String fullName, String companyName, String mobile, boolean isContact)
    {
        WSAccount account = new WSAccount();
        account.setId(Utils.randomUUID());
        account.setFullName(fullName);
        account.setCompanyName(companyName);
        account.setType(AccountType.AGENT);
        account.setPrimaryMobile(mobile);
        account.setShortReference(Utils.randomString(Constant.CHARSET_ALPHANUMERIC, 6, false));
        account.setPhoto(WSPhotoReference.dummyData());
        account.setCity(WSCity.dummyData("Mumbai"));
        account.setStatus(Status.ACTIVE);
        account.setScore(10);
        account.setIsBlocked(false);
        account.setIsContact(isContact);
        account.setCanChat(true);
        account.setCreatedTime(System.currentTimeMillis() + "");
        account.setRegistrationTime(System.currentTimeMillis() + "");
        account.setLastActivityTime(System.currentTimeMillis() + "");
        account.setBroadcastCount(Long.getLong(Utils.randomString(Constant.CHARSET_NUMERIC, 1, false)));
        account.setCursors(WSChatThreadCursor.dummyData());
        account.setOnline(new SecureRandom().nextBoolean());

        return account;
    }

    public static WSAccount dummyDataWithId(String fullName, String companyName, String mobile, boolean isContact, String id)
    {
        WSAccount account = new WSAccount();
        account.setId(id);
        account.setFullName(fullName);
        account.setCompanyName(companyName);
        account.setType(AccountType.AGENT);
        account.setPrimaryMobile(mobile);
        account.setShortReference(Utils.randomString(Constant.CHARSET_ALPHANUMERIC, 6, false));
        account.setPhoto(WSPhotoReference.dummyData());
        account.setCity(WSCity.dummyData("Mumbai"));
        account.setStatus(Status.ACTIVE);
        account.setScore(10);
        account.setIsBlocked(false);
        account.setIsContact(isContact);
        account.setCanChat(true);
        account.setCreatedTime(System.currentTimeMillis() + "");
        account.setRegistrationTime(System.currentTimeMillis() + "");
        account.setLastActivityTime(System.currentTimeMillis() + "");
        account.setBroadcastCount(Long.getLong(Utils.randomString(Constant.CHARSET_NUMERIC, 1, false)));
        account.setCursors(WSChatThreadCursor.dummyData());
        account.setOnline(new SecureRandom().nextBoolean());

        return account;
    }

    public static void main(String[] args)
    {

    }
}