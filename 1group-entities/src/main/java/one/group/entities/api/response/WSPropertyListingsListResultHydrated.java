package one.group.entities.api.response;

import java.util.List;

public class WSPropertyListingsListResultHydrated
{
    private int maxPropertyListingIndex;

    private List<WSPropertyListing> propertyListings;

    public int getMaxPropertyListingIndex()
    {
        return maxPropertyListingIndex;
    }

    public void setMaxPropertyListingIndex(int maxPropertyListingIndex)
    {
        this.maxPropertyListingIndex = maxPropertyListingIndex;
    }

    public List<WSPropertyListing> getPropertyListings()
    {
        return propertyListings;
    }

    public void setPropertyListings(List<WSPropertyListing> propertyListings)
    {
        this.propertyListings = propertyListings;
    }

    @Override
    public String toString()
    {
        return "WSPropertyListingResultList [maxPropertyListingIndex=" + maxPropertyListingIndex + ", propertyListings=" + propertyListings + "]";
    }
}
