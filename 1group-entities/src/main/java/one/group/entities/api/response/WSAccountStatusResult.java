package one.group.entities.api.response;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAccountStatusResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSAccountStatus> results = new ArrayList<WSAccountStatus>();
    
    public List<WSAccountStatus> getResults()
    {
        return results;
    }
    
    public void setResults(List<WSAccountStatus> results)
    {
        this.results = results;
    }
    
    public void addResult(WSAccountStatus status)
    {
        this.results.add(status);
    }
}
