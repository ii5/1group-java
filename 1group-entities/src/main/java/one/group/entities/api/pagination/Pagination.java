package one.group.entities.api.pagination;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import one.group.core.Constant;
import one.group.core.enums.EntityType;
import one.group.entities.cache.PaginationKey;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class Pagination
{
    private String paginationKey;

    private String refreshPaginationKey;

    private EntityType entityType;

    private HttpHeaders httpHeaders;

    private UriInfo uriInfo;

    private int pageNo;

    private String accountId;

    public Pagination(UriInfo uriInfo, HttpHeaders httpHeaders, EntityType type, String accountId) throws NoSuchAlgorithmException
    {
        Validation.isTrue(!(Utils.isNull(uriInfo)), "Uri Info should not be null or empty");
        Validation.isTrue(!(Utils.isNull(httpHeaders)), "Http headers should not be null or empty");
        Validation.isTrue(!(Utils.isNull(type)), "Type should not be null or empty");
        Validation.isTrue(!(Utils.isNullOrEmpty(accountId)), "Account id should not be null or empty");

        String clientToken = Utils.getAuthTokenFromHeader(httpHeaders);

        String refreshNewKey = Utils.generatePaginationKey(clientToken, type);
        Map<String, String> values = new HashMap<String, String>();
        values.put("account_id", accountId);
        values.put("pagination_key", refreshNewKey);
        this.refreshPaginationKey = PaginationKey.PAGINATION_ID.getFormedKey(values);

        String key = Utils.fetchValueFromQueryParam(uriInfo, "pagination_key") != null ? Utils.fetchValueFromQueryParam(uriInfo, "pagination_key") : refreshPaginationKey;
        values = new HashMap<String, String>();
        values.put("account_id", accountId);
        values.put("pagination_key", key);
        this.paginationKey = PaginationKey.PAGINATION_ID.getFormedKey(values);

        this.pageNo = Utils.fetchValueFromQueryParam(uriInfo, "page_no") == null ? Constant.DEFAULT_PAGINATION_PAGE : Integer.valueOf(Utils.fetchValueFromQueryParam(uriInfo, "page_no"));
    }

    public String getPaginationKey()
    {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey)
    {
        this.paginationKey = paginationKey;
    }

    public String getRefreshPaginationKey()
    {
        return refreshPaginationKey;
    }

    public void setRefreshPaginationKey(String refreshPaginationKey)
    {
        this.refreshPaginationKey = refreshPaginationKey;
    }

    public EntityType getEntityType()
    {
        return entityType;
    }

    public void setEntityType(EntityType entityType)
    {
        this.entityType = entityType;
    }

    public HttpHeaders getHttpHeaders()
    {
        return httpHeaders;
    }

    public void setHttpHeaders(HttpHeaders httpHeaders)
    {
        this.httpHeaders = httpHeaders;
    }

    public UriInfo getUriInfo()
    {
        return uriInfo;
    }

    public void setUriInfo(UriInfo uriInfo)
    {
        this.uriInfo = uriInfo;
    }

    public int getPageNo()
    {
        return pageNo;
    }

    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

}
