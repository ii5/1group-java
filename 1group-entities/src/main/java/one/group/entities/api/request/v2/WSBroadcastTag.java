/**
 * 
 */
package one.group.entities.api.request.v2;

import one.group.entities.api.request.RequestEntity;

/**
 * @author ashishthorat
 *
 */
public class WSBroadcastTag implements RequestEntity
{
	private static final long serialVersionUID = 1L;
	
    private String title;

    private Long size;

    private String transactionType;

    private String broadcastType;

    private String commissionType;

    private String customSublocation;

    private Long maxPrice;

    private Long minPrice;

    private String propertyMarket;

    private String propertyType;

    private String rooms;

    private String price;

    private String locationId;

    private String cityId;

    private Long maxSize;

    private Long minSize;

    private String status;

    private String id;

    private String draftedBy;

    private String draftedTime;

    private String addedBy;

    private String addedTime;

    private String createdBy;

    private String createdTime;

    private String updatedBy;

    private String updatedTime;

    private String propertySubType;
    
    private String discardReason;
    
    private String closedBy;
    
    private String closedTime;
    
    
    
    public String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	public String getClosedTime() {
		return closedTime;
	}

	public void setClosedTime(String closedTime) {
		this.closedTime = closedTime;
	}

	public String getDiscardReason() 
    {
		return discardReason;
	}
    
    public void setDiscardReason(String discardReason)
    {
    	this.discardReason = discardReason;
    }

    public String getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(String propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public String getDraftedBy()
    {
        return draftedBy;
    }

    public void setDraftedBy(String draftedBy)
    {
        this.draftedBy = draftedBy;
    }

    public String getDraftedTime()
    {
        return draftedTime;
    }

    public void setDraftedTime(String draftedTime)
    {
        this.draftedTime = draftedTime;
    }

    public String getAddedBy()
    {
        return addedBy;
    }

    public void setAddedBy(String addedBy)
    {
        this.addedBy = addedBy;
    }

    public String getAddedTime()
    {
        return addedTime;
    }

    public void setAddedTime(String addedTime)
    {
        this.addedTime = addedTime;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getMaxSize()
    {
        return maxSize;
    }

    public void setMaxSize(Long maxSize)
    {
        this.maxSize = maxSize;
    }

    public Long getMinSize()
    {
        return minSize;
    }

    public void setMinSize(Long minSize)
    {
        this.minSize = minSize;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public String getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(String propertyType)
    {
        this.propertyType = propertyType;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getSize()
    {
        return size;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(String broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public String getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(String commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public Long getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public Long getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(Long minPrice)
    {
        this.minPrice = minPrice;
    }

    public String getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(String propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

}
