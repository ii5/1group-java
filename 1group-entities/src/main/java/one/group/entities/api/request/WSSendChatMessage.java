package one.group.entities.api.request;

import one.group.core.enums.MessageType;
import one.group.entities.api.response.WSPhotoReference;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSSendChatMessage
{
    private String fromAccountId;

    private String toAccountId;

    private MessageType type;

    private String text;

    private String propertyListingId;

    private String propertyListingShortReference;

    private WSPhotoReference photo;

    private Long clientSentTime;

    private boolean isMessageRepeated;

    public WSSendChatMessage()
    {

    }

    public WSSendChatMessage(MessageType type, String toAccountId)
    {
        Validation.isTrue(!Utils.isNull(type), "Passed type should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(toAccountId), "Passed toAccountId should not be null or empty");

        this.type = type;
        this.toAccountId = toAccountId;
    }

    public String getFromAccountId()
    {
        return fromAccountId;
    }

    public void setFromAccountId(String fromAccountId)
    {
        this.fromAccountId = fromAccountId;
    }

    public String getToAccountId()
    {
        return toAccountId;
    }

    public MessageType getType()
    {
        return type;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public String getPropertyListingShortReference()
    {
        return propertyListingShortReference;
    }

    public void setPropertyListingShortReference(String propertyListingShortReference)
    {
        this.propertyListingShortReference = propertyListingShortReference;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public Long getClientSentTime()
    {
        return clientSentTime;
    }

    public void setClientSentTime(Long ClientSentTime)
    {
        this.clientSentTime = ClientSentTime;
    }

    public void setToAccountId(String toAccountId)
    {
        this.toAccountId = toAccountId;
    }

    public void setType(MessageType type)
    {
        this.type = type;
    }

    public boolean isMessageRepeated()
    {
        return isMessageRepeated;
    }

    public void setMessageRepeated(boolean isMessageRepeated)
    {
        this.isMessageRepeated = isMessageRepeated;
    }

}
