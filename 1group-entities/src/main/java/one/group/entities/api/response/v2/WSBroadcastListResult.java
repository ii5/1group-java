package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSBroadcastListResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WSPaginationData pagination;

    private List<WSBroadcast> broadcasts = new ArrayList<WSBroadcast>();

    public List<WSBroadcast> getBroadcasts()
    {
        return broadcasts;
    }

    public void setBroadcasts(List<WSBroadcast> broadcasts)
    {
        this.broadcasts = broadcasts;
    }

    public void addBroadcast(WSBroadcast broadcast)
    {
        this.broadcasts.add(broadcast);
    }

    public WSPaginationData getPagination()
    {
        return pagination;
    }

    public void setPagination(WSPaginationData pagination)
    {
        this.pagination = pagination;
    }

    @Override
    public String toString()
    {
        return "WSBroadcastListResult [pagination=" + pagination + ", broadcasts=" + broadcasts + "]";
    }

    public static WSBroadcastListResult dummyData()
    {
        WSBroadcastListResult result = new WSBroadcastListResult();
        result.setPagination(WSPaginationData.dummyData());

        for (int i = 0; i < 10; i++)
        {
            result.addBroadcast(WSBroadcast.dummyData(Utils.randomUUID()));
        }

        return result;
    }

    public static void main(String[] args)
    {
        String endpoint = "/accounts/{account_ids}/broadcasts/starred";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSBroadcastListResult responseData = WSBroadcastListResult.dummyData();
        WSResult result = new WSResult("200", "Broadcast data.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));
    }

}
