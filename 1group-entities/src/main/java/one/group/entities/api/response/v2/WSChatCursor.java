package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class WSChatCursor implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String createdBy;
    private String chatThreadId;
    private WSChatThreadCursor cursor;

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String accountId)
    {
        this.createdBy = accountId;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public WSChatThreadCursor getCursor()
    {
        return cursor;
    }

    public void setCursor(WSChatThreadCursor cursor)
    {
        this.cursor = cursor;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((cursor == null) ? 0 : cursor.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatCursor other = (WSChatCursor) obj;
        if (createdBy == null)
        {
            if (other.createdBy != null)
                return false;
        }
        else if (!createdBy.equals(other.createdBy))
            return false;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (cursor == null)
        {
            if (other.cursor != null)
                return false;
        }
        else if (!cursor.equals(other.cursor))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSChatCursor [accountId=" + createdBy + ", chatThreadId=" + chatThreadId + ", cursor=" + cursor + "]";
    }
    
    public static WSChatCursor dummyData()
    {
        WSChatCursor cursor = new WSChatCursor();
        
        cursor.setChatThreadId(Utils.randomUUID());
        cursor.setCreatedBy(Utils.randomUUID());
        cursor.setCursor(WSChatThreadCursor.dummyData());
        
        return cursor;
    }
}
