package one.group.entities.api.response;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.APIVersion;
import one.group.core.enums.ClientUpdateType;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long syncIndex;

    private String locationsDb;

    private APIVersion currentApiVersion;

    private String currentClientVersion;

    private ClientUpdateType clientUpdateType = ClientUpdateType.NO_UPDATE;

    private List<WSSyncUpdate> updates;

    public WSSyncResult(long syncIndex)
    {
        this();
        this.syncIndex = syncIndex;
        this.updates = new ArrayList<WSSyncUpdate>();

    }

    public WSSyncResult()
    {
        this.updates = new ArrayList<WSSyncUpdate>();
        this.currentApiVersion = APIVersion.currentVersion();
    }

    public APIVersion getCurrentApiVersion()
    {
        return currentApiVersion;
    }

    public void setCurrentApiVersion(APIVersion currentApiVersion)
    {
        this.currentApiVersion = currentApiVersion;
    }

    public String getLocationsDb()
    {
        return locationsDb;
    }

    public void setLocationsDb(String locationsDb)
    {
        this.locationsDb = locationsDb;
    }

    public long getSyncIndex()
    {
        return syncIndex;
    }

    public void setSyncIndex(long syncIndex)
    {
        this.syncIndex = syncIndex;
    }

    public List<WSSyncUpdate> getUpdates()
    {
        return updates;
    }

    public void setUpdates(List<WSSyncUpdate> updates)
    {
        this.updates = updates;
    }

    public void addUpdate(WSSyncUpdate update)
    {
        this.updates.add(update);
    }

    public String getCurrentClientVersion()
    {
        return currentClientVersion;
    }

    public ClientUpdateType getClientUpdateType()
    {
        return clientUpdateType;
    }

    public void setCurrentClientVersion(String currentClientVersion)
    {
        this.currentClientVersion = currentClientVersion;
    }

    public void setClientUpdateType(ClientUpdateType clientUpdateType)
    {
        this.clientUpdateType = clientUpdateType;
    }

    @Override
    public String toString()
    {
        return "WSSyncResult [syncIndex=" + syncIndex + ", locationsDb=" + locationsDb + ", currentApiVersion=" + currentApiVersion + ", currentClientVersion=" + currentClientVersion
                + ", clientUpdateType=" + clientUpdateType + ", updates=" + updates + "]";
    }

}
