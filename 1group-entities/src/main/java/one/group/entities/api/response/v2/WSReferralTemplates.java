package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;

public class WSReferralTemplates implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
    private String id;

    private String template;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTemplate()
    {
        return template;
    }

    public void setTemplate(String template)
    {
        this.template = template;
    }

}
