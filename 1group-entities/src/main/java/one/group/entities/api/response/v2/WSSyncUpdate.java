package one.group.entities.api.response.v2;

import one.group.core.Constant;
import one.group.core.enums.MessageType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncUpdate implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private WSChatCursor chatCursor;
    private WSMessageResponse message;
    private WSEntityReference entity;
    private WSSubscribeEntity subscription;
    private WSNotification notification;
    private WSMessageResponse broadcast;
    private WSAccount accountProfile;
    private WSAccount accountRelation;

    private int index = -1;
    private SyncDataType type;

    public WSSyncUpdate()
    {
    }

    public WSSyncUpdate(int index, SyncDataType type)
    {
        Validation.notNull(type, "The update type should not be null.");
        this.index = index;
        this.type = type;
    }

    public WSSyncUpdate(SyncDataType type)
    {
        Validation.notNull(type, "The update type should not be null.");
        this.type = type;
    }

    public WSSyncUpdate(WSSyncUpdate wsSyncUpdate)
    {
        Validation.notNull(wsSyncUpdate, "The syncUpdate should not be null.");
        this.chatCursor = wsSyncUpdate.getChatCursor();
        this.message = wsSyncUpdate.getMessage();
        this.entity = wsSyncUpdate.getEntity();
        this.subscription = wsSyncUpdate.getSubscription();
        this.notification = wsSyncUpdate.getNotification();
        this.broadcast = wsSyncUpdate.getBroadcast();
        this.index = wsSyncUpdate.getIndex();
        this.type = wsSyncUpdate.getType();
        this.accountProfile = wsSyncUpdate.getAccountProfile();
        this.accountRelation = wsSyncUpdate.getAccountRelation();
    }

    public WSSubscribeEntity getSubscription()
    {
        return subscription;
    }

    public void setSubscription(WSSubscribeEntity subscription)
    {
        this.subscription = subscription;
    }

    public WSChatCursor getChatCursor()
    {
        return chatCursor;
    }

    public void setChatCursor(WSChatCursor chatCursor)
    {
        this.chatCursor = chatCursor;
    }

    public WSMessageResponse getMessage()
    {
        return message;
    }

    public void setMessage(WSMessageResponse message)
    {
        this.message = message;
    }

    public WSEntityReference getEntity()
    {
        return entity;
    }

    public void setEntity(WSEntityReference entity)
    {
        this.entity = entity;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public SyncDataType getType()
    {
        return type;
    }

    public void setType(SyncDataType type)
    {
        this.type = type;
    }

    public WSNotification getNotification()
    {
        return notification;
    }

    public void setNotification(WSNotification notification)
    {
        this.notification = notification;
    }

    public WSMessageResponse getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSMessageResponse broadcast)
    {
        this.broadcast = broadcast;
    }

    public WSAccount getAccountProfile()
    {
        return accountProfile;
    }

    public void setAccountProfile(WSAccount accountProfile)
    {
        this.accountProfile = accountProfile;
    }

    public WSAccount getAccountRelation()
    {
        return accountRelation;
    }

    public void setAccountRelation(WSAccount accountRelation)
    {
        this.accountRelation = accountRelation;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatCursor == null) ? 0 : chatCursor.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((entity == null) ? 0 : entity.hashCode());
        result = prime * result + index;
        result = prime * result + ((subscription == null) ? 0 : subscription.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((notification == null) ? 0 : notification.hashCode());
        result = prime * result + ((broadcast == null) ? 0 : broadcast.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSSyncUpdate other = (WSSyncUpdate) obj;
        if (chatCursor == null)
        {
            if (other.chatCursor != null)
                return false;
        }
        else if (!chatCursor.equals(other.chatCursor))
            return false;
        if (message == null)
        {
            if (other.message != null)
                return false;
        }
        else if (!message.equals(other.message))
            return false;
        if (entity == null)
        {
            if (other.entity != null)
                return false;
        }
        else if (!entity.equals(other.entity))
            return false;
        if (index != other.index)
            return false;
        if (subscription == null)
        {
            if (other.subscription != null)
                return false;
        }
        else if (!subscription.equals(other.subscription))
            return false;
        if (type != other.type)
            return false;
        if (notification == null)
        {
            if (other.notification != null)
                return false;
        }
        else if (!notification.equals(other.notification))
            return false;
        if (broadcast == null)
        {
            if (other.broadcast != null)
                return false;
        }
        else if (!broadcast.equals(other.broadcast))
            return false;

        return true;
    }

    @Override
    public String toString()
    {
        return "WSSyncUpdate [chatCursor=" + chatCursor + ", chatMessage=" + message + ", entity=" + entity + ", subscription=" + subscription + ", notification=" + notification + ", broadcast="
                + broadcast + ", index=" + index + ", type=" + type + ", accountProfile=" + accountProfile + "]";
    }

    public static WSSyncUpdate dummyDataBroadcast(int index)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        WSMessageResponse message = WSMessageResponse.dummyData(
                WSAccount.dummyData(Utils.generateAccountReference(), Utils.generateAccountReference(), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true), false),
                WSAccount.dummyData(Utils.generateAccountReference(), Utils.generateAccountReference(), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true), false), 20,
                MessageType.BROADCAST, "Hola!", null, Utils.randomUUID(), WSBroadcast.dummyData("xx"));
        syncUpdate.setBroadcast(message);
        syncUpdate.setType(SyncDataType.BROADCAST);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate dummyDataChatCursor(int index)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setChatCursor(WSChatCursor.dummyData());
        syncUpdate.setType(SyncDataType.CHAT_CURSOR);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate dummyDataEntity(int index, SyncDataType syncType)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setEntity(WSEntityReference.dummyData());
        syncUpdate.setType(syncType);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate dummyDataMessage(int index)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setMessage(WSMessageResponse.dummyData(
                WSAccount.dummyData(Utils.generateAccountReference(), Utils.generateAccountReference(), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true), false),
                WSAccount.dummyData(Utils.generateAccountReference(), Utils.generateAccountReference(), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true), false), 20,
                MessageType.TEXT, "Hola!", null, Utils.randomUUID(), null));
        syncUpdate.setType(SyncDataType.MESSAGE);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate dummyDataSubscription(int index)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setSubscription(WSSubscribeEntity.dummyData());
        syncUpdate.setType(SyncDataType.SUBSCRIBE);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate dummyDataNotification(int index)
    {
        WSSyncUpdate syncUpdate = new WSSyncUpdate();
        syncUpdate.setNotification(WSNotification.dummyData());
        syncUpdate.setType(SyncDataType.NOTIFICATION);
        setDummyDataIndex(syncUpdate, index);
        return syncUpdate;
    }

    public static WSSyncUpdate setDummyDataIndex(WSSyncUpdate syncUpdate, int index)
    {
        syncUpdate.setIndex(index);
        return syncUpdate;
    }

}
