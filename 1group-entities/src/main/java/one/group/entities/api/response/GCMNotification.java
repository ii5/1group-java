package one.group.entities.api.response;

public class GCMNotification
{
    private String body;
    
    private String jsonBody;

    private String title;

    private String icon;

    private String clickAction;

    public GCMNotification()
    {

    }

    public GCMNotification(String body, String title, String icon, String clickAction)
    {
        this.body = body;
        this.title = title;
        this.icon = icon;
        this.clickAction = clickAction;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getClickAction()
    {
        return clickAction;
    }

    public void setClickAction(String clickAction)
    {
        this.clickAction = clickAction;
    }
    
    public String getJsonBody()
    {
        return jsonBody;
    }
    
    public void setJsonBody(String jsonBody)
    {
        this.jsonBody = jsonBody;
    }

}
