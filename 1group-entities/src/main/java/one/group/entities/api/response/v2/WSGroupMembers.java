package one.group.entities.api.response.v2;

import one.group.entities.api.response.ResponseEntity;
import one.group.entities.jpa.GroupMembers;

public class WSGroupMembers implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String participantId;

    private String groupId;

    private int receivedIndex;

    private int readIndex;

    private String receivedMessageId;

    private String readMessageId;

    public WSGroupMembers()
    {

    }

    public WSGroupMembers(GroupMembers groupMember)
    {
        this.groupId = groupMember.getGroupId();
        this.participantId = groupMember.getParticipantId();
        this.readIndex = groupMember.getReadIndex();
        this.receivedIndex = groupMember.getReceivedIndex();
        this.receivedMessageId = groupMember.getReceivedMessageId();
        this.readMessageId = groupMember.getReadMessageId();

    }

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public int getReceivedIndex()
    {
        return receivedIndex;
    }

    public void setReceivedIndex(int receivedIndex)
    {
        this.receivedIndex = receivedIndex;
    }

    public int getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(int readIndex)
    {
        this.readIndex = readIndex;
    }

    public String getReceivedMessageId()
    {
        return receivedMessageId;
    }

    public void setReceivedMessageId(String receivedMessageId)
    {
        this.receivedMessageId = receivedMessageId;
    }

    public String getReadMessageId()
    {
        return readMessageId;
    }

    public void setReadMessageId(String readMessageId)
    {
        this.readMessageId = readMessageId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((participantId == null) ? 0 : participantId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSGroupMembers other = (WSGroupMembers) obj;
        if (groupId == null)
        {
            if (other.groupId != null)
                return false;
        }
        else if (!groupId.equals(other.groupId))
            return false;
        if (participantId == null)
        {
            if (other.participantId != null)
                return false;
        }
        else if (!participantId.equals(other.participantId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSGroupMembers [participantId=" + participantId + ", groupId=" + groupId + ", receivedIndex=" + receivedIndex + ", readIndex=" + readIndex + ", receivedMessageId=" + receivedMessageId
                + ", readMessageId=" + readMessageId + "]";
    }

}
