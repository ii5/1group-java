package one.group.entities.api.response.v2;

import java.io.IOException;

import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.socket.Message;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * 
 * @author miteshchavda
 * 
 */
public class WSMessageResponse implements ResponseEntity
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private WSAccount createdBy;

    private WSAccount toAccount;

    private String id;

    private Integer index;

    private String createdTime;

    private String text;

    private MessageType type;

    private WSPhotoReference photo = null;

    private String chatThreadId;

    private String clientSentTime;

    private WSBroadcast broadcast = null;

    private MessageStatus status;

    private Integer version;

    private String messageSource;

    @JsonIgnore
    private String createdById;

    @JsonIgnore
    private String toAccountId;

    @JsonIgnore
    private String broadcastId;

    // bpo

    private String groupId;

    private BpoMessageStatus bpoMessageStatus;

    private String bpoStatusUpdateBy;

    private String hash;

    private Boolean isDuplicate = false;

    private Boolean isMessageRepeated = false;

    public WSMessageResponse()
    {
    };

    public WSMessageResponse(Message message) throws JsonMappingException, JsonGenerationException, IOException
    {
        this.id = message.getId();
        this.index = -1;
        if (message.getIndex() != null)
            this.index = message.getIndex();
        this.createdTime = message.getCreatedTime().getTime() + "";

        if (message.getUpdatedTime() != null)
        {
            this.createdTime = message.getUpdatedTime().getTime() + "";
        }
        this.type = message.getMessageType();
        if (message.getMessageType().equals(MessageType.PHOTO))
        {
            this.photo = (WSPhotoReference) Utils.getInstanceFromJson(message.getText(), WSPhotoReference.class);
        }
        else
        {
            this.text = message.getText();
        }
        this.chatThreadId = message.getGroupId();
        if (Utils.isNull(message.getClientSentTime()))
        {
            this.clientSentTime = message.getMessageSentTime().getTime() + "";
        }
        else
        {
            this.clientSentTime = message.getClientSentTime().getTime() + "";
        }
        this.status = message.getMessageStatus();
        this.version = message.getVersion();
        this.createdById = message.getCreatedBy();
        this.toAccountId = message.getToAccountId();
        this.broadcastId = message.getBroadcastId();

        if (message.getMessageSource() != null)
        {
            this.messageSource = message.getMessageSource().toString();
        }
    }

    public WSMessageResponse(final MessageType type, final WSAccount createdBy, final String chatThreadId, final String serverTime)
    {
        Validation.isTrue(!Utils.isNull(type), "Passed type should not be null");
        Validation.isTrue(!Utils.isNull(createdBy), "Passed createdBy should not null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Passed chatThreadId should not null or empty");
        Validation.notNull(serverTime, "Passed serverTime should not be null or empty");

        this.type = type;
        this.createdBy = createdBy;
        this.chatThreadId = chatThreadId;
        this.createdTime = serverTime;
    }

    public WSMessageResponse(final MessageType type, final WSAccount createdBy, final String chatThreadId)
    {
        Validation.isTrue(!Utils.isNull(type), "Passed type should not be null");
        Validation.isTrue(!Utils.isNull(createdBy), "Passed createdBy should not null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Passed chatThreadId should not null or empty");

        this.type = type;
        this.createdBy = createdBy;
        this.chatThreadId = chatThreadId;
    }

    public WSAccount getToAccount()
    {
        return toAccount;
    }

    public void setToAccount(WSAccount toAccount)
    {
        this.toAccount = toAccount;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setType(MessageType type)
    {
        this.type = type;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public String getText()

    {
        return text;
    }

    public MessageType getType()
    {
        return type;
    }

    public void setPhoto(final WSPhotoReference photo)

    {
        this.photo = photo;
    }

    public String getClientSentTime()
    {
        return clientSentTime;
    }

    public void setClientSentTime(String clientSentTime)
    {
        this.clientSentTime = clientSentTime;
    }

    public WSBroadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSBroadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public MessageStatus getStatus()
    {
        return status;
    }

    public void setStatus(MessageStatus status)
    {
        this.status = status;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Integer getIndex()
    {
        return index;
    }

    public void setIndex(Integer index)
    {
        this.index = index;
    }

    public WSAccount getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(WSAccount createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public BpoMessageStatus getBpoMessageStatus()
    {
        return bpoMessageStatus;
    }

    public String getBpoStatusUpdateBy()
    {
        return bpoStatusUpdateBy;
    }

    public String getHash()
    {
        return hash;
    }

    public Boolean getIsDuplicate()
    {
        return isDuplicate;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public void setBpoMessageStatus(BpoMessageStatus bpoMessageStatus)
    {
        this.bpoMessageStatus = bpoMessageStatus;
    }

    public void setBpoStatusUpdateBy(String bpoStatusUpdateBy)
    {
        this.bpoStatusUpdateBy = bpoStatusUpdateBy;
    }

    public void setHash(String hash)
    {
        this.hash = hash;
    }

    public void setIsDuplicate(Boolean isDuplicate)
    {
        this.isDuplicate = isDuplicate;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public Boolean isMessageRepeated()
    {
        return isMessageRepeated;
    }

    public void setMessageRepeated(Boolean isMessageRepeated)
    {
        this.isMessageRepeated = isMessageRepeated;
    }

    public String getCreatedById()
    {
        return createdById;
    }

    public void setCreatedById(String createdById)
    {
        this.createdById = createdById;
    }

    public String getToAccountId()
    {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId)
    {
        this.toAccountId = toAccountId;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getMessageSource()
    {
        return messageSource;
    }

    public void setMessageSource(String messageSource)
    {
        this.messageSource = messageSource;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((broadcastId == null) ? 0 : broadcastId.hashCode());
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((createdById == null) ? 0 : createdById.hashCode());
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((messageSource == null) ? 0 : messageSource.hashCode());
        result = prime * result + ((photo == null) ? 0 : photo.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        result = prime * result + ((toAccountId == null) ? 0 : toAccountId.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((clientSentTime == null) ? 0 : clientSentTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSMessageResponse other = (WSMessageResponse) obj;
        if (broadcastId == null)
        {
            if (other.broadcastId != null)
                return false;
        }
        else if (!broadcastId.equals(other.broadcastId))
            return false;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (createdById == null)
        {
            if (other.createdById != null)
                return false;
        }
        else if (!createdById.equals(other.createdById))
            return false;
        if (groupId == null)
        {
            if (other.groupId != null)
                return false;
        }
        else if (!groupId.equals(other.groupId))
            return false;
        if (messageSource == null)
        {
            if (other.messageSource != null)
                return false;
        }
        else if (!messageSource.equals(other.messageSource))
            return false;
        if (photo == null)
        {
            if (other.photo != null)
                return false;
        }
        else if (!photo.equals(other.photo))
            return false;
        if (text == null)
        {
            if (other.text != null)
                return false;
        }
        else if (!text.equals(other.text))
            return false;
        if (toAccountId == null)
        {
            if (other.toAccountId != null)
                return false;
        }
        else if (!toAccountId.equals(other.toAccountId))
            return false;
        if (type != other.type)
            return false;
        if (clientSentTime == null)
        {
            if (other.clientSentTime != null)
                return false;
        }
        else if (!clientSentTime.equals(other.clientSentTime))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSMessageResponse [createdBy=" + createdBy + ", toAccount=" + toAccount + ", id=" + id + ", index=" + index + ", createdTime=" + createdTime + ", text=" + text + ", type=" + type
                + ", photo=" + photo + ", chatThreadId=" + chatThreadId + ", clientSentTime=" + clientSentTime + ", broadcast=" + broadcast + ", status=" + status + ", version=" + version
                + ", messageSource=" + messageSource + ", createdById=" + createdById + ", toAccountId=" + toAccountId + ", broadcastId=" + broadcastId + ", groupId=" + groupId
                + ", bpoMessageStatus=" + bpoMessageStatus + ", bpoStatusUpdateBy=" + bpoStatusUpdateBy + ", hash=" + hash + ", isDuplicate=" + isDuplicate + ", isMessageRepeated="
                + isMessageRepeated + "]";
    }

    public static WSMessageResponse dummyData(WSAccount createdBy, WSAccount toAccount, int index, MessageType type, String text, WSPhotoReference photo, String chatThreadId, WSBroadcast broadcast)
    {
        WSMessageResponse message = new WSMessageResponse();
        message.setId(Utils.randomUUID());

        message.setStatus(MessageStatus.ACTIVE);

        if (type.equals(MessageType.BROADCAST))
        {
            broadcast.setMessageId(message.getId());
            message.setBroadcast(broadcast);
            message.setText("Text message that formed the broadcast.");
            message.setStatus(MessageStatus.ACTIVE);
        }
        else if (type.equals(MessageType.PHOTO))
        {
            message.setPhoto(photo);
            message.setStatus(MessageStatus.PENDING_REVIEW);
        }
        else
        {
            message.setText(text);
            message.setStatus(MessageStatus.PENDING_REVIEW);
        }

        message.setCreatedBy(createdBy);
        message.setToAccount(toAccount);
        message.setIndex(index);
        message.setClientSentTime(System.currentTimeMillis() + "");
        message.setCreatedTime(System.currentTimeMillis() + "");
        // message.setText(text);
        message.setType(type);
        // message.setPhoto(photo);
        message.setChatThreadId(chatThreadId);
        // message.setBroadcast(broadcast);

        message.setVersion(2);

        return message;
    }

    public static WSMessageResponse dummyDataBpo(WSAccount createdBy, WSAccount toAccount, int index, MessageType type, String text, WSPhotoReference photo, String chatThreadId,
            WSBroadcast broadcast, BpoMessageStatus bpoMessageStatus, String bpoStatusUpdateBy, String hash, Boolean isDuplicate)
    {
        WSMessageResponse message = new WSMessageResponse();
        message.setId(Utils.randomUUID());

        if (type.equals(MessageType.BROADCAST))
        {
            broadcast.setMessageId(message.getId());
            message.setBroadcast(broadcast);
            message.setText("Text message that formed the broadcast.");
        }
        else if (type.equals(MessageType.PHOTO))
        {
            message.setPhoto(photo);
        }
        else
        {
            message.setText(text);
        }

        message.setCreatedBy(createdBy);
        message.setToAccount(toAccount);
        message.setIndex(index);
        message.setClientSentTime(System.currentTimeMillis() + "");
        message.setCreatedTime(System.currentTimeMillis() + "");
        // message.setText(text);
        message.setType(type);
        // message.setPhoto(photo);
        message.setChatThreadId(chatThreadId);
        message.setGroupId(chatThreadId);
        // message.setBroadcast(broadcast);
        message.setStatus(MessageStatus.ACTIVE);
        message.setVersion(2);

        message.setBpoMessageStatus(bpoMessageStatus);
        message.setBpoStatusUpdateBy(bpoStatusUpdateBy);
        message.setIsDuplicate(isDuplicate);
        message.setHash(hash);

        return message;
    }
}
