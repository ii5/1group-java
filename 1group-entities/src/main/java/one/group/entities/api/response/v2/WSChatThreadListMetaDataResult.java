package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSChatThreadListMetaDataResult implements ResponseEntity
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WSChatThreadReference oneGroup;
	private WSChatThreadReference me;
	private WSChatThreadReference starred;
    private List<WSChatThreadReference> direct = new ArrayList<WSChatThreadReference>();

    private Integer maxChatThreadIndex;

    public WSChatThreadListMetaDataResult()
    {

    }

    

    public WSChatThreadReference getOneGroup() {
		return oneGroup;
	}



	public void setOneGroup(WSChatThreadReference oneGroup) {
		this.oneGroup = oneGroup;
	}



	public WSChatThreadReference getMe() {
		return me;
	}



	public void setMe(WSChatThreadReference me) {
		this.me = me;
	}



	public WSChatThreadReference getStarred() {
		return starred;
	}



	public void setStarred(WSChatThreadReference starred) {
		this.starred = starred;
	}



	public List<WSChatThreadReference> getDirect() {
		return direct;
	}


	public void addDirect(WSChatThreadReference direct)
	{
		this.direct.add(direct);
	}

	public void setDirect(List<WSChatThreadReference> direct) {
		this.direct = direct;
	}



	public Integer getMaxChatThreadIndex()
    {
        return maxChatThreadIndex;
    }

    public void setMaxChatThreadIndex(Integer maxChatThreadIndex)
    {
        this.maxChatThreadIndex = maxChatThreadIndex;
    }

    public static WSChatThreadListMetaDataResult dummyData(int chatThreadCount, MessageSourceType messageSource)
    {
        int maxChatMessagesPerChatThread = 10;
        Integer unreadMessageCount = null;
        Integer myBroadcastCount = null;
        WSChatThreadListMetaDataResult chatThreadList = new WSChatThreadListMetaDataResult();

        if (messageSource.equals(MessageSourceType.DIRECT))
        {
            unreadMessageCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }
        else if (messageSource.equals(MessageSourceType.ME))
        {
            myBroadcastCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }

        for (int i = 0; i < chatThreadCount; i++)
        {
            WSAccount participantA = WSAccount.dummyData("Nyal Fernandes", "Ferns Corp", "+919898989898", true);
            WSAccount participantB = WSAccount.dummyData("Raj Seth", "Global Realty", "+919812098120", false);
            List<WSAccount> participants = new ArrayList<WSAccount>();
            participants.add(participantA);
            participants.add(participantB);

            WSChatThreadReference ref = WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount);

//            chatThreadList.addChatThread(ref);
        }

        return chatThreadList;
    }
    
    public static WSChatThreadListMetaDataResult dummyMetaData(int chatThreadCount, MessageSourceType messageSource)
    {
        int maxChatMessagesPerChatThread = 10;
        Integer unreadMessageCount = null;
        Integer myBroadcastCount = null;
        WSChatThreadListMetaDataResult chatThreadList = new WSChatThreadListMetaDataResult();

        WSAccount participantA = WSAccount.dummyData("Nyal Fernandes", "Ferns Corp", "+919898989898", true);
        WSAccount participantB = WSAccount.dummyData("Raj Seth", "Global Realty", "+919812098120", false);
        
        List<WSAccount> participants = new ArrayList<WSAccount>();
        participants.add(participantA);
        participants.add(participantB);
        
        if (messageSource.equals(MessageSourceType.DIRECT))
        {
            unreadMessageCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }
        else if (messageSource.equals(MessageSourceType.ME))
        {
            myBroadcastCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }
        
        chatThreadList.setMe(WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount, new MessageType[]{MessageType.BROADCAST, MessageType.TEXT}));
        
        
        chatThreadList.setOneGroup(WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount, new MessageType[]{MessageType.BROADCAST}));
        
        
        chatThreadList.setStarred(WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount, new MessageType[]{MessageType.BROADCAST}));
        

        for (int i = 0; i < chatThreadCount; i++)
        {
            
            WSChatThreadReference ref = WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount);

            chatThreadList.addDirect(ref);
        }

        return chatThreadList;
    }

    public static void main(String[] args)
    {
        String endpoint = "/meta_data/chat_threads/";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSChatThreadListMetaDataResult responseData = WSChatThreadListMetaDataResult.dummyMetaData(10, MessageSourceType.DIRECT);
        WSResult result = new WSResult("200", "Chat threads data fetched successfully.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));

    }

}
