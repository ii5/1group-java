package one.group.entities.api.response;

import one.group.core.enums.EntityType;
import one.group.core.enums.HintType;
import one.group.core.enums.SyncDataType;
import one.group.entities.api.response.v2.WSMessageResponse;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncUpdate implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WSChatCursor chatCursor;
    private WSMessageResponse chatMessage;
    private WSEntityReference entity;
    private WSSubscribeEntity subscription;
    private WSNewsFeed newsFeed;
    private WSNotification notification;
    private WSBroadcast broadcast;

    private int index = -1;
    private SyncDataType type;

    public WSSyncUpdate()
    {
    }

    public WSSyncUpdate(int index, SyncDataType type)
    {
        Validation.notNull(type, "The update type should not be null.");
        this.index = index;
        this.type = type;
    }

    public WSSyncUpdate(SyncDataType type)
    {
        Validation.notNull(type, "The update type should not be null.");
        this.type = type;
    }

    public WSSubscribeEntity getSubscription()
    {
        return subscription;
    }

    public void setSubscription(WSSubscribeEntity subscription)
    {
        this.subscription = subscription;
    }

    public WSChatCursor getChatCursor()
    {
        return chatCursor;
    }

    public void setChatCursor(WSChatCursor chatCursor)
    {
        this.chatCursor = chatCursor;
    }

    public WSMessageResponse getChatMessage()
    {
        return chatMessage;
    }

    public void setChatMessage(WSMessageResponse chatMessage)
    {
        this.chatMessage = chatMessage;
    }

    public WSEntityReference getEntity()
    {
        return entity;
    }

    public void setEntity(WSEntityReference entity)
    {
        this.entity = entity;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public SyncDataType getType()
    {
        return type;
    }

    public void setType(SyncDataType type)
    {
        this.type = type;
    }

    public WSNewsFeed getNewsFeed()
    {
        return newsFeed;
    }

    public void setNewsFeed(WSNewsFeed newsFeed)
    {
        this.newsFeed = newsFeed;
    }

    public WSNotification getNotification()
    {
        return notification;
    }

    public void setNotification(WSNotification notification)
    {
        this.notification = notification;
    }

    public WSBroadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(WSBroadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatCursor == null) ? 0 : chatCursor.hashCode());
        result = prime * result + ((chatMessage == null) ? 0 : chatMessage.hashCode());
        result = prime * result + ((entity == null) ? 0 : entity.hashCode());
        result = prime * result + index;
        result = prime * result + ((newsFeed == null) ? 0 : newsFeed.hashCode());
        result = prime * result + ((subscription == null) ? 0 : subscription.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((notification == null) ? 0 : notification.hashCode());
        result = prime * result + ((broadcast == null) ? 0 : broadcast.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSSyncUpdate other = (WSSyncUpdate) obj;
        if (chatCursor == null)
        {
            if (other.chatCursor != null)
                return false;
        }
        else if (!chatCursor.equals(other.chatCursor))
            return false;
        if (chatMessage == null)
        {
            if (other.chatMessage != null)
                return false;
        }
        else if (!chatMessage.equals(other.chatMessage))
            return false;
        if (entity == null)
        {
            if (other.entity != null)
                return false;
        }
        else if (!entity.equals(other.entity))
            return false;
        if (index != other.index)
            return false;
        if (newsFeed == null)
        {
            if (other.newsFeed != null)
                return false;
        }
        else if (!newsFeed.equals(other.newsFeed))
            return false;
        if (subscription == null)
        {
            if (other.subscription != null)
                return false;
        }
        else if (!subscription.equals(other.subscription))
            return false;
        if (type != other.type)
            return false;
        if (notification == null)
        {
            if (other.notification != null)
                return false;
        }
        else if (!notification.equals(other.notification))
            return false;
        if (broadcast == null)
        {
            if (other.broadcast != null)
                return false;
        }
        else if (!broadcast.equals(other.broadcast))
            return false;

        return true;
    }

    @Override
    public String toString()
    {
        return "WSSyncUpdate [chatCursor=" + chatCursor + ", chatMessage=" + chatMessage + ", entity=" + entity + ", subscription=" + subscription + ", newsFeed=" + newsFeed + ", notification="
                + notification + ", broadcast=" + broadcast + ", index=" + index + ", type=" + type + "]";
    }

    public static void main(String[] args)
    {
        WSSyncUpdate update = new WSSyncUpdate();
        update.setType(SyncDataType.INVALIDATION);
        update.setIndex(11);
        update.setEntity(new WSEntityReference(HintType.DELETE, EntityType.NEWS_FEED, null));
        System.out.println(Utils.getJsonString(update));
    }

}
