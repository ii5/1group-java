package one.group.entities.api.response;

import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 *
 */
public class WSChatThreadReference implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WSChatThread chatThread;

    private Integer chatThreadIndex;

    public WSChatThreadReference()
    {

    }

    public WSChatThreadReference(WSChatThread chatThread, Integer chatThreadIndex)
    {
        Validation.isTrue(!Utils.isNull(chatThread), "Chat thread passed should not be null");
        Validation.isTrue(chatThreadIndex >= -1, "Chat thread index passed should not be less than 0");

        this.chatThread = chatThread;
        this.chatThreadIndex = chatThreadIndex;
    }

    public WSChatThread getChatThread()
    {
        return chatThread;
    }

    public Integer getChatThreadIndex()
    {
        return chatThreadIndex;
    }

    @Override
    public String toString()
    {
        return "WSChatThreadReference [chatThread = " + chatThread + ", chatThreadIndex=" + chatThreadIndex + "]";
    }
}
