package one.group.entities.api.request;


public class WSInvite implements RequestEntity 
{	
	private static final long serialVersionUID = 1L;
	private String number;
	
	public String getNumber() 
	{
		return number;
	}

	public void setNumber(String number) 
	{
		this.number = number;
	}
		
}
