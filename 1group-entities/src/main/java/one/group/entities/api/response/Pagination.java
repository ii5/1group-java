package one.group.entities.api.response;

import java.util.List;

public class Pagination implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String pageCount;

    private String firstPage;

    private String currentPage;

    private String nextPage;

    private String previousePage;

    private String lastPage;

    private List<WSPaginationSort> sort;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPageCount()
    {
        return pageCount;
    }

    public void setPageCount(String pageCount)
    {
        this.pageCount = pageCount;
    }

    public String getFirstPage()
    {
        return firstPage;
    }

    public void setFirstPage(String firstPage)
    {
        this.firstPage = firstPage;
    }

    public String getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(String currentPage)
    {
        this.currentPage = currentPage;
    }

    public String getNextPage()
    {
        return nextPage;
    }

    public void setNextPage(String nextPage)
    {
        this.nextPage = nextPage;
    }

    public String getPreviousePage()
    {
        return previousePage;
    }

    public void setPreviousePage(String previousePage)
    {
        this.previousePage = previousePage;
    }

    public String getLastPage()
    {
        return lastPage;
    }

    public void setLastPage(String lastPage)
    {
        this.lastPage = lastPage;
    }

    public List<WSPaginationSort> getSort()
    {
        return sort;
    }

    public void setSort(List<WSPaginationSort> sort)
    {
        this.sort = sort;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pagination other = (Pagination) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Pagination [id=" + id + ", pageCount=" + pageCount + ", firstPage=" + firstPage + ", currentPage=" + currentPage + ", nextPage=" + nextPage + ", previousePage=" + previousePage
                + ", lastPage=" + lastPage + ", sort=" + sort + "]";
    }
}
