package one.group.entities.api.response;

import one.group.core.enums.AccountType;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;

public class WSAccount implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String fullName;

    private String companyName;

    private AccountType type;

    private String primaryMobile;

    private String shortReference;

    private WSPhotoReference photo;

    private WSCity city;

    private Status status;

    private Integer score;

    private Boolean isBlocked;

    private Boolean isContact;

    private Boolean canChat;

    private String createdTime;

    private String lastActivityTime;

    private Integer propertyListingCount;

    private String registrationTime;

    public Integer getPropertyListingCount()
    {
        return propertyListingCount;
    }

    public void setPropertyListingCount(Integer propertyListingCount)
    {
        this.propertyListingCount = propertyListingCount;
    }

    public WSAccount()
    {
        this.canChat = true;
    }

    /**
     * 
     * @param account
     */
    public WSAccount(Account account)
    {
        this.fullName = account.getFullName();
        this.companyName = account.getCompanyName();
        this.id = account.getIdAsString();
        this.type = account.getType();
        this.status = account.getStatus();
        this.primaryMobile = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;
        this.city = new WSCity(account.getCity());

        if (account.getFullPhoto() != null)
        {
            this.photo = new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto());
        }

        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }

    }

    public WSAccount(Account account, boolean isLocation, boolean isPhoto)
    {
        this.fullName = account.getFullName();
        this.companyName = account.getCompanyName();
        this.id = account.getIdAsString();
        this.type = account.getType();
        this.status = account.getStatus();
        this.primaryMobile = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;

        if (isLocation)
        {
            this.city = new WSCity(account.getCity());

        }

        if (isPhoto)
        {
            if (account.getFullPhoto() != null)
            {
                this.photo = new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto());
            }
        }

        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }

    }

    public WSAccount(Account account, Boolean isAdmin)
    {
        if (account.getCreatedTime() != null)
        {
            this.createdTime = "" + account.getCreatedTime().getTime();
        }
        this.status = account.getStatus();
        this.id = account.getId();
    }

    public Boolean getCanChat()
    {
        return canChat;
    }

    public void setCanChat(Boolean canChat)
    {
        this.canChat = canChat;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public AccountType getType()
    {
        return type;
    }

    public void setType(AccountType type)
    {
        this.type = type;
    }

    public String getPrimaryMobile()
    {
        return primaryMobile;
    }

    public void setPrimaryMobile(String primaryMobile)
    {
        this.primaryMobile = primaryMobile;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Boolean getIsBlocked()
    {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Boolean getIsContact()
    {
        return isContact;
    }

    public void setIsContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(String lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public String getRegistrationTime()
    {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime)
    {
        this.registrationTime = registrationTime;
    }

    @Override
    public String toString()
    {
        return "WSAccount [id=" + id + ", fullName=" + fullName + ", companyName=" + companyName + ", type=" + type + ", primaryMobile=" + primaryMobile + ", shortReference=" + shortReference
                + ", photo=" + photo + ", city=" + city + ", status=" + status + ", score=" + score + ", isBlocked=" + isBlocked + ", isContact=" + isContact + ", canChat=" + canChat

                + ", createdTime=" + createdTime + ", lastActivityTime=" + lastActivityTime + ", propertyListingCount=" + propertyListingCount + "]";

    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccount other = (WSAccount) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

}