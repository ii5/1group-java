package one.group.entities.api.request.v2;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.request.RequestEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSSyncRequest implements RequestEntity
{
    private Integer ackSyncIndex;

    private Integer maxResults;

    public WSSyncRequest()
    {
        this.ackSyncIndex = Constant.MINUS_ONE;
        this.maxResults = Constant.DEFAULT_SYNC_RESULT;
    }

    public Integer getAckSyncIndex()
    {
        return ackSyncIndex;
    }

    public void setAckSyncIndex(Integer ackSyncIndex)
    {
        this.ackSyncIndex = ackSyncIndex;
    }

    public Integer getMaxResults()
    {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults)
    {
        this.maxResults = maxResults;
    }

    @Override
    public String toString()
    {
        return "WSSyncRequest [ackSyncIndex=" + ackSyncIndex + ", maxResults=" + maxResults + "]";
    }

    public static WSSyncRequest dummyData()
    {
        WSSyncRequest request = new WSSyncRequest();
        request.setAckSyncIndex(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)));
        request.setMaxResults(request.getAckSyncIndex() + Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)));

        return request;
    }

    public static void main(String[] args)
    {
        String endpoint = "/sync";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.POST;
        SocketEntity entity = new SocketEntity();

        WSResult result = new WSResult("200", "Sync data fetched.", false);
        WSResponse response = new WSResponse();
        // response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        entity.setWithWSRequest(WSRequest.dummySync());

        System.out.println(Utils.getJsonString(entity));
    }

}
