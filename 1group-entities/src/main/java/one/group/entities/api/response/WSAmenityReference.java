package one.group.entities.api.response;

import one.group.entities.jpa.Amenity;

/**
 * API schema for Amenity
 *  
 * @author sanilshet
 *
 */
public class WSAmenityReference implements ResponseEntity
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;	

	public WSAmenityReference() {};
	
	public WSAmenityReference(Amenity amenity)
	{
		this.setId(amenity.getIdAsString());
		this.setName(amenity.getName());
	}
	/**
	 * @return the id
	 */
	public String getId() 
	{
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) 
	{
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) 
	{
		this.name = name;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() 
	{
		return "WSAmenityReference [id=" + id + ", name=" + name + "]";
	}
}
