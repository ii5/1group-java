package one.group.entities.api.response;

import java.util.ArrayList;
import java.util.List;

public class WSGCMRequest
{
    private List<String> registrationIds = new ArrayList<String>();

    private GCMData data;

    public List<String> getRegistrationIds()
    {
        return registrationIds;
    }

    public void setRegistrationIds(List<String> registrationIds)
    {
        this.registrationIds = registrationIds;
    }

    public GCMData getData()
    {
        return data;
    }

    public void setData(GCMData data)
    {
        this.data = data;
    }

}
