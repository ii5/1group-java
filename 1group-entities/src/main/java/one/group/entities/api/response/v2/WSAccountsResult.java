package one.group.entities.api.response.v2;

import java.util.HashSet;
import java.util.Set;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageSourceType;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAccountsResult
{
    private Set<WSAccount> accountsResult = new HashSet<WSAccount>();
    
    public Set<WSAccount> getAccountsResult()
    {
        return accountsResult;
    }
    
    public void setAccountsResult(Set<WSAccount> accountResult)
    {
        this.accountsResult = accountResult;
    }
    
    public void addAccountResult(WSAccount account)
    {
        this.accountsResult.add(account);
    }
    
    public static WSAccountsResult dummyData()
    {
    	WSAccountsResult result = new WSAccountsResult();
    	
    	for(int i = 0; i < 10; i++)
    	{
    		result.addAccountResult(WSAccount.dummyData("Full Name : " + Utils.generateAccountReference(), "Company Name : " + Utils.generateAccountReference(), "+91" + Utils.randomString(Constant.CHARSET_NUMERIC, 10, false), true));
    	}
    	
    	return result;
    }
    
    public static void main(String[] args)
    {
    	for (int i = 0; i < 10; i++)
    	{
    		
    	}String endpoint = "/accounts/{account_ids}";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSAccountsResult responseData = WSAccountsResult.dummyData();
        WSResult result = new WSResult("200", "Accounts data fetched successfully.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));
    }
    
}
