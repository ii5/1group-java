package one.group.entities.api.request;

import one.group.core.Constant;

public class WSPhoto implements RequestEntity
{
    private Integer height = Constant.MIN_MEDIA_HEIGHT;

    private Integer width = Constant.MIN_MEDIA_WIDTH;

    private String location;

    private Long size;

    public WSPhoto()
    {
    }

    public Integer getHeight()
    {
        return height;
    }

    public Integer getWidth()
    {
        return width;
    }

    public String getLocation()
    {
        return location;
    }

    public Long getSize()
    {
        return size;
    }

    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void setSize(Long size)
    {
        this.size = size;
    };
}
