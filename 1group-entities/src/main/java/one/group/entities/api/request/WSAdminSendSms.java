package one.group.entities.api.request;

public class WSAdminSendSms
{
    private String mobileNumber;

    private String smsText;

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public String getSmsText()
    {
        return smsText;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public void setSmsText(String smsText)
    {
        this.smsText = smsText;
    }

}
