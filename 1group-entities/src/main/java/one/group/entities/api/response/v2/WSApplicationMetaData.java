package one.group.entities.api.response.v2;

import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

/**
 * 
 * @author nyal fernandes
 *
 */
public class WSApplicationMetaData
{
    private int totalBroadcastCount;
    private int totalAccountCount;
    private int unreadNotificationCount;
    private int totalGroupCount;

    private List<WSReferralTemplates> referralTemplates;

    public int getTotalBroadcastCount()
    {
        return totalBroadcastCount;
    }

    public void setTotalBroadcastCount(int totalBroadcastCount)
    {
        this.totalBroadcastCount = totalBroadcastCount;
    }

    public int getTotalAccountCount()
    {
        return totalAccountCount;
    }

    public void setTotalAccountCount(int totalAccountCount)
    {
        this.totalAccountCount = totalAccountCount;
    }

    public int getUnreadNotificationCount()
    {
        return unreadNotificationCount;
    }

    public void setUnreadNotificationCount(int unreadNotificationCount)
    {
        this.unreadNotificationCount = unreadNotificationCount;
    }

    public List<WSReferralTemplates> getReferralTemplates()
    {
        return referralTemplates;
    }

    public void setReferralTemplates(List<WSReferralTemplates> referralTemplates)
    {
        this.referralTemplates = referralTemplates;
    }

    public int getTotalGroupCount()
    {
        return totalGroupCount;
    }

    public void setTotalGroupCount(int totalGroupCount)
    {
        this.totalGroupCount = totalGroupCount;
    }

    public static WSApplicationMetaData dummyData()
    {
        WSApplicationMetaData applicationMetadata = new WSApplicationMetaData();

        applicationMetadata.setTotalAccountCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        applicationMetadata.setTotalBroadcastCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));
        applicationMetadata.setUnreadNotificationCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 5, true)));

        return applicationMetadata;
    }

    public static void main(String[] args)
    {
        String endpoint = "/meta_data/application";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSApplicationMetaData responseData = WSApplicationMetaData.dummyData();
        WSResult result = new WSResult("200", "Application meta data fetched.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));
    }
}
