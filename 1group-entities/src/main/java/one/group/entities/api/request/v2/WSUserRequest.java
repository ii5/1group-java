package one.group.entities.api.request.v2;


/**
 * 
 * @author nyalfernandes
 *
 */
public class WSUserRequest
{
    private String id;

    private String fullName;

    private String shortReference;

    private String email;

    private String cityId;

    private String type;

    private String source;
    
    private String createdBy;
    
    private String syncStatus;
    
    private String groupSelectionStatus;
    
    private String localStorage;
    
    private String mobileNumber;
    
    public String getMobileNumber()
    {
        return mobileNumber;
    }
    
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getGroupSelectionStatus()
    {
        return groupSelectionStatus;
    }
    
    public String getLocalStorage()
    {
        return localStorage;
    }
    
    public String getSyncStatus()
    {
        return syncStatus;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public void setGroupSelectionStatus(String groupSelectionStatus)
    {
        this.groupSelectionStatus = groupSelectionStatus;
    }
    
    public void setLocalStorage(String localStorage)
    {
        this.localStorage = localStorage;
    }
    
    public void setSyncStatus(String syncStatus)
    {
        this.syncStatus = syncStatus;
    }
    
    
}
