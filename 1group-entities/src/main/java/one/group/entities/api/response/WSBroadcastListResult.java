package one.group.entities.api.response;

import java.util.List;

public class WSBroadcastListResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	WSPaginationData pagination;

    List<WSBroadcast> broadcasts;

    public List<WSBroadcast> getBroadcasts()
    {
        return broadcasts;
    }

    public void setBroadcasts(List<WSBroadcast> broadcasts)
    {
        this.broadcasts = broadcasts;
    }

    public WSPaginationData getPagination()
    {
        return pagination;
    }

    public void setPagination(WSPaginationData pagination)
    {
        this.pagination = pagination;
    }

    @Override
    public String toString()
    {
        return "WSBroadcastListResult [pagination=" + pagination + ", broadcasts=" + broadcasts + "]";
    }

}
