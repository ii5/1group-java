package one.group.entities.api.response;

import java.util.List;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface GCMResponse<T> extends ResponseEntity
{

    public List<String> getRegistrationIds();
    
    public T getData(); 
    
}
