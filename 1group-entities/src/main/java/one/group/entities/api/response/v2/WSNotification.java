package one.group.entities.api.response.v2;

import java.util.HashMap;
import java.util.Map;

import one.group.core.Constant;
import one.group.core.enums.NotificationAction;
import one.group.core.enums.status.NotificationStatus;
import one.group.entities.api.response.ResponseEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSNotification implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
    private String title;
    private String body;
    private String id;
    private NotificationAction action;
    private String timestamp;
    private NotificationStatus status;
    private Map<String, Object> parameters = new HashMap<String, Object>();

    public WSNotification()
    {

    }

    public WSNotification(String id, String title, String body, String timestamp, NotificationStatus status)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(id), "Passed id should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(title), "Passed title should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(body), "Passed body should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(timestamp), "Passed timestamp should not be null");
        Validation.isTrue(!Utils.isNull(status), "Passed status should not be null");

        this.id = id;
        this.title = title;
        this.body = body;
        this.timestamp = timestamp;
        this.status = status;

    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public NotificationStatus getStatus()
    {
        return status;
    }

    public void setStatus(NotificationStatus status)
    {
        this.status = status;
    }

    public NotificationAction getAction()
    {
        return action;
    }

    public void setAction(NotificationAction action)
    {
        this.action = action;
    }

    public Map<String, Object> getParameters()
    {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters)
    {
        this.parameters = parameters;
    }

    public void addParameter(String key, Object o)
    {
        this.parameters.put(key, o);
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSNotification other = (WSNotification) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSNotification [title=" + title + ", body=" + body + ", id=" + id + ", timestamp=" + timestamp + ", status=" + status + "]";
    }

    public static WSNotification dummyData()
    {
        WSNotification notification = new WSNotification();
        notification.setBody("Notification Body");
        notification.setId(Utils.randomUUID());
        notification.setStatus(NotificationStatus.UNREAD);
        notification.setTimestamp(System.currentTimeMillis() + "");
        notification.setTitle("Notification Title");
        notification.setAction(NotificationAction.values()[Integer.valueOf(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % NotificationAction.values().length]);
        notification.addParameter("critria", "somedata");

        return notification;
    }
}