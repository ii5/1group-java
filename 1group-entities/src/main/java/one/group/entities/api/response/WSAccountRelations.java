package one.group.entities.api.response;

import java.sql.Timestamp;

import one.group.entities.jpa.AccountRelations;


public class WSAccountRelations implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountId;
    private String contactId;
    private Integer score;
    private Integer scoredAs;
    private Boolean isContact;
    private Boolean isContactOf;
    private Boolean isBlocked;
    private Boolean isBlockedBy;
    private Timestamp createdOn;

    public WSAccountRelations(String contactId, boolean isContact, boolean isContactOf, boolean isBlocked, boolean isBlockedBy)
    {
        this.contactId = contactId;
        this.isContact = isContact;
        this.isContactOf = isContactOf;
        this.isBlocked = isBlocked;
        this.isBlockedBy = isBlockedBy;
    }

    public WSAccountRelations()
    {
    }

    public WSAccountRelations(AccountRelations accountRelations)
    {

        this.accountId = accountRelations.getAccount().getId();
        this.contactId = accountRelations.getOtherAccountId();
        this.score = accountRelations.getScore();
        this.scoredAs = accountRelations.getScoredAs();
        this.isContact = accountRelations.isContact();
        this.isContactOf = accountRelations.isContactOf();
        this.isBlocked = accountRelations.isBlocked();
        this.isBlockedBy = accountRelations.isBlockedBy();
        this.createdOn = accountRelations.getCreatedTime();
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getContactId()
    {
        return contactId;
    }

    public void setContactId(String contactId)
    {
        this.contactId = contactId;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Boolean getIsContact()
    {
        return isContact;
    }

    public void setIsContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public Boolean getIsBlocked()
    {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Timestamp getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn)
    {
        this.createdOn = createdOn;
    }

    public Integer getScoredAs()
    {
        return scoredAs;
    }

    public void setScoredAs(Integer scoredAs)
    {
        this.scoredAs = scoredAs;
    }

    public Boolean getIsContactOf()
    {
        return isContactOf;
    }

    public void setIsContactOf(Boolean isContactOf)
    {
        this.isContactOf = isContactOf;
    }

    public Boolean getIsBlockedBy()
    {
        return isBlockedBy;
    }

    public void setIsBlockedBy(Boolean isBlockedBy)
    {
        this.isBlockedBy = isBlockedBy;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        result = prime * result + ((contactId == null) ? 0 : contactId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAccountRelations other = (WSAccountRelations) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (contactId == null)
        {
            if (other.contactId != null)
                return false;
        }
        else if (!contactId.equals(other.contactId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WSAccountRelations [accountId=" + accountId + ", contactId=" + contactId + ", score=" + score + ", isContact=" + isContact + ", isBlocked=" + isBlocked + ", createdOn=" + createdOn
                + "]";
    }

}