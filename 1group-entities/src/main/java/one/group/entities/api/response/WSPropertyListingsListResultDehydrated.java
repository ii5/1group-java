package one.group.entities.api.response;

import java.util.List;

public class WSPropertyListingsListResultDehydrated
{
    private int maxPropertyListingIndex;

    private List<String> propertyListings;

    public int getMaxPropertyListingIndex()
    {
        return maxPropertyListingIndex;
    }

    public void setMaxPropertyListingIndex(int maxPropertyListingIndex)
    {
        this.maxPropertyListingIndex = maxPropertyListingIndex;
    }

    public List<String> getPropertyListings()
    {
        return propertyListings;
    }

    public void setPropertyListings(List<String> propertyListings)
    {
        this.propertyListings = propertyListings;
    }

    @Override
    public String toString()
    {
        return "WSPropertyListingResultDehyratedList [maxPropertyListingIndex=" + maxPropertyListingIndex + ", propertyListings=" + propertyListings + "]";
    }

}
