package one.group.entities.api.response;

public class GCMData
{

    private String id;

    private String type;

    private String subType;

    private String senderId;

    private String senderName;

    private String message;

    private String senderPic;

    private String senderLastActiveTime;

    private String senderPrimaryNumber;

    private String senderCity;

    private String chatThreadId;

    public GCMData()
    {

    }

    public GCMData(String id, String type)
    {
        this.id = id;
        this.type = type;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getSubType()
    {
        return subType;
    }

    public void setSubType(String subType)
    {
        this.subType = subType;
    }

    public String getSenderId()
    {
        return senderId;
    }

    public void setSenderId(String senderId)
    {
        this.senderId = senderId;
    }

    public String getSenderName()
    {
        return senderName;
    }

    public void setSenderName(String senderName)
    {
        this.senderName = senderName;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getSenderPic()
    {
        return senderPic;
    }

    public void setSenderPic(String senderPic)
    {
        this.senderPic = senderPic;
    }

    public String getSenderLastActiveTime()
    {
        return senderLastActiveTime;
    }

    public void setSenderLastActiveTime(String senderLastActiveTime)
    {
        this.senderLastActiveTime = senderLastActiveTime;
    }

    public String getSenderPrimaryNumber()
    {
        return senderPrimaryNumber;
    }

    public void setSenderPrimaryNumber(String senderPrimaryNumber)
    {
        this.senderPrimaryNumber = senderPrimaryNumber;
    }

    public String getSenderCity()
    {
        return senderCity;
    }

    public void setSenderCity(String senderCity)
    {
        this.senderCity = senderCity;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

}
