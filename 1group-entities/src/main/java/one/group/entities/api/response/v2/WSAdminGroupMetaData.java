/**
 * 
 */
package one.group.entities.api.response.v2;

/**
 * @author ashishthorat
 *
 */
public class WSAdminGroupMetaData
{
    private String phoneNumber;

    private int approvedGroupCount;

    private int rejectedGroupCount;

    private int newGroupCount;

    private int syncDisabledGroupCount;

    private int totalGroups;

    private int ActiveGroupCount;

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public int getApprovedGroupCount()
    {
        return approvedGroupCount;
    }

    public void setApprovedGroupCount(int approvedGroupCount)
    {
        this.approvedGroupCount = approvedGroupCount;
    }

    public int getRejectedGroupCount()
    {
        return rejectedGroupCount;
    }

    public void setRejectedGroupCount(int rejectedGroupCount)
    {
        this.rejectedGroupCount = rejectedGroupCount;
    }

    public int getNewGroupCount()
    {
        return newGroupCount;
    }

    public void setNewGroupCount(int newGroupCount)
    {
        this.newGroupCount = newGroupCount;
    }

    public int getSyncDisabledGroupCount()
    {
        return syncDisabledGroupCount;
    }

    public void setSyncDisabledGroupCount(int syncDisabledGroupCount)
    {
        this.syncDisabledGroupCount = syncDisabledGroupCount;
    }

    public int getTotalGroups()
    {
        return totalGroups;
    }

    public void setTotalGroups(int totalGroups)
    {
        this.totalGroups = totalGroups;
    }

    public int getActiveGroupCount()
    {
        return ActiveGroupCount;
    }

    public void setActiveGroupCount(int activeGroupCount)
    {
        ActiveGroupCount = activeGroupCount;
    }

}
