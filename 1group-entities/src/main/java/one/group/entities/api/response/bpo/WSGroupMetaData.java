package one.group.entities.api.response.bpo;

import one.group.core.Constant;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.entities.api.response.v2.WSCity;
import one.group.entities.api.response.v2.WSLocation;
import one.group.entities.socket.Groups;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSGroupMetaData
{
    private String id;
    
    private String name;
    
    // same as name for dashboard.
    private String value;
    
    private String label;
    
    private GroupStatus status;

    private Integer count;

    private Integer totalMessages;
    
    private Integer unreadmsgCount;
    
    private Integer readmsgCount;
    
    private Integer unqualifiedmsgCount;
    
    private Integer totalmsgCount;
    
    private String unreadFrom;
    
    private String locationId;
    
    private String cityId;
    
    private String assignedTo;
    
    private String username;
    
    private WSLocation location;
    
    private WSCity city;
    
    private GroupSource source;
    
    private String notes;
    
    private Boolean everSync;
    
    private Boolean currentSync;
    
    private String latestMessageTimestamp;
    
    private String mediaId;

    public WSGroupMetaData()
    {

    }
    
    public WSGroupMetaData(Groups g)
    {
        this.id = g.getId();
        this.name = g.getGroupName();
        this.value = this.name;
        this.label = this.name;
        this.status = g.getStatus();
        this.totalMessages = g.getTotalmsgCount();
        this.unreadmsgCount = g.getUnreadmsgCount();
        this.totalmsgCount = g.getTotalmsgCount();
        this.locationId = g.getLocationId();
        this.cityId = g.getCityId();
        this.source = g.getSource();
        this.notes = g.getNotes();
        this.assignedTo = g.getAssignedTo();
        this.readmsgCount = g.getReadmsgCount();
        this.unqualifiedmsgCount = g.getUnqualifiedmsgCount();
        this.everSync = g.getEverSync();
        this.latestMessageTimestamp = (g.getLatestMessageTimestamp() == null) ? null : g.getLatestMessageTimestamp().getTime()+"";
        this.unreadFrom = g.getUnreadFrom() == null ? null : g.getUnreadFrom().getTime()+"";
        this.mediaId = g.getMediaId();
    }
    
    
    
    public String getUsername() 
    {
		return username;
	}

	public void setUsername(String username) 
	{
		this.username = username;
	}

	public String getMediaId() 
    {
		return mediaId;
	}
    
    public void setMediaId(String mediaId) 
    {
		this.mediaId = mediaId;
	}
    
    public String getLatestMessageTimestamp() 
    {
		return latestMessageTimestamp;
	}
    
    public void setLatestMessageTimestamp(String latestMessageTimestamp) 
    {
		this.latestMessageTimestamp = latestMessageTimestamp;
	}
    
    public Boolean getCurrentSync() 
    {
		return currentSync;
	}
    
    public void setCurrentSync(Boolean currentSync) 
    {
		this.currentSync = currentSync;
	}
    
    public Boolean getEverSync() 
    {
		return everSync;
	}
    
    public void setEverSync(Boolean everSync) 
    {
		this.everSync = everSync;
	}
    
    public Integer getUnqualifiedmsgCount()
    {
        return unqualifiedmsgCount;
    }
    
    public Integer getReadmsgCount()
    {
        return readmsgCount;
    }
    
    public void setUnqualifiedmsgCount(Integer unqualifiedmsgCount)
    {
        this.unqualifiedmsgCount = unqualifiedmsgCount;
    }
    
    public void setReadmsgCount(Integer readmsgCount)
    {
        this.readmsgCount = readmsgCount;
    }
    
    public GroupSource getSource()
    {
        return source;
    }
    
    public String getNotes()
    {
        return notes;
    }
    
    public void setNotes(String notes)
    {
        this.notes = notes;
    }
    
    public void setSource(GroupSource source)
    {
        this.source = source;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public GroupStatus getStatus()
    {
        return status;
    }

    public void setStatus(GroupStatus status)
    {
        this.status = status;
    }

    public Integer getCount()
    {
        return count;
    }

    public void setCount(Integer count)
    {
        this.count = count;
    }

    public Integer getTotalMessages()
    {
        return totalMessages;
    }

    public void setTotalMessages(Integer totalMessages)
    {
        this.totalMessages = totalMessages;
    }

    public Integer getUnreadmsgCount()
    {
        return unreadmsgCount;
    }

    public void setUnreadmsgCount(Integer unreadmsgCount)
    {
        this.unreadmsgCount = unreadmsgCount;
    }

    public Integer getTotalmsgCount()
    {
        return totalmsgCount;
    }

    public void setTotalmsgCount(Integer totalmsgCount)
    {
        this.totalmsgCount = totalmsgCount;
    }

    public String getUnreadFrom()
    {
        return unreadFrom;
    }

    public void setUnreadFrom(String unreadFrom)
    {
        this.unreadFrom = unreadFrom;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public String getAssignedTo()
    {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public WSLocation getLocation()
    {
        return location;
    }

    public void setLocation(WSLocation location)
    {
        this.location = location;
    }

    public WSCity getCity()
    {
        return city;
    }

    public void setCity(WSCity city)
    {
        this.city = city;
    }

   
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("WSGroupMetaData [");
        if (id != null)
        {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (name != null)
        {
            builder.append("name=");
            builder.append(name);
            builder.append(", ");
        }
        if (value != null)
        {
            builder.append("value=");
            builder.append(value);
            builder.append(", ");
        }
        if (label != null)
        {
            builder.append("label=");
            builder.append(label);
            builder.append(", ");
        }
        if (status != null)
        {
            builder.append("status=");
            builder.append(status);
            builder.append(", ");
        }
        builder.append("count=");
        builder.append(count);
        builder.append(", totalMessages=");
        builder.append(totalMessages);
        builder.append(", unreadmsgCount=");
        builder.append(unreadmsgCount);
        builder.append(", totalmsgCount=");
        builder.append(totalmsgCount);
        builder.append(", ");
        if (unreadFrom != null)
        {
            builder.append("unreadFrom=");
            builder.append(unreadFrom);
            builder.append(", ");
        }
        if (locationId != null)
        {
            builder.append("locationId=");
            builder.append(locationId);
            builder.append(", ");
        }
        if (cityId != null)
        {
            builder.append("cityId=");
            builder.append(cityId);
            builder.append(", ");
        }
        if (assignedTo != null)
        {
            builder.append("assignedTo=");
            builder.append(assignedTo);
            builder.append(", ");
        }
        if (location != null)
        {
            builder.append("location=");
            builder.append(location);
            builder.append(", ");
        }
        if (city != null)
        {
            builder.append("city=");
            builder.append(city);
            builder.append(", ");
        }
        if (source != null)
        {
            builder.append("source=");
            builder.append(source);
        }
        builder.append("]");
        return builder.toString();
    }

    public static WSGroupMetaData dummyGroupLabelMetaData()
    {
        WSGroupMetaData o = new WSGroupMetaData();
        o.setValue("Some GROUP " + Utils.generateAccountReference());
        o.setLabel(o.getValue());
        o.setId(Utils.randomUUID());
        
        return o;
    }
    
    public static WSGroupMetaData dummyGroupDetailedMetaData()
    {
        WSGroupMetaData o = new WSGroupMetaData();
        o.setValue("Some GROUP " + Utils.generateAccountReference());
        o.setLabel(o.getValue());
        o.setId(Utils.randomUUID());
        o.setAssignedTo(Utils.randomUUID());
        o.setCity(WSCity.dummyData("CITY-" + Utils.randomString(Constant.CHARSET_UPPERCASE_ALPHABETS, 1, false)));
        o.setCityId(o.getCity().getId());
        o.setCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)));
        o.setLocation(WSLocation.dummyData());
        o.setLocationId(o.getLocation().getId());
        o.setName(o.getValue());
        o.setStatus(GroupStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % GroupStatus.values().length]);
        o.setTotalMessages(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) + o.getCount());
        o.setTotalmsgCount(o.getTotalMessages());
        o.setUnreadFrom(System.currentTimeMillis() + "");
        o.setUnreadmsgCount(Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)));
        o.setSource(GroupSource.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)) % GroupSource.values().length]);
        return o;
    }
    

}
