/**
 * 
 */
package one.group.entities.api.response;

import java.util.List;

/**
 * @author ashishthorat
 *
 */
public class WSCityLocations implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSCity> cityLocations;

    public List<WSCity> getCityLocations()
    {
        return cityLocations;
    }

    public void setCityLocations(List<WSCity> cityLocations)
    {
        this.cityLocations = cityLocations;
    }

}
