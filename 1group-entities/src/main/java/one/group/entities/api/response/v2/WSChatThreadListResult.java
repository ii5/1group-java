package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageSourceType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class WSChatThreadListResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<WSChatThreadReference> chatThreads = new ArrayList<WSChatThreadReference>();

    private Integer maxChatThreadIndex;

    public WSChatThreadListResult()
    {

    }

    public WSChatThreadListResult(List<WSChatThreadReference> wsChatThreads, Integer maxChatThreadIndex)
    {
        Validation.isTrue(!Utils.isNull(wsChatThreads), "Chat thread should not be null");
        Validation.isTrue(!Utils.isNull(maxChatThreadIndex), "Max chat thread index should not be null");

        this.chatThreads = wsChatThreads;
        this.maxChatThreadIndex = maxChatThreadIndex;
    }

    public List<WSChatThreadReference> getChatThreads()
    {
        return chatThreads;
    }

    public void setChatThreads(List<WSChatThreadReference> chatThreads)
    {
        this.chatThreads = chatThreads;
    }

    public void addChatThread(WSChatThreadReference chatThread)
    {
        this.chatThreads.add(chatThread);
    }

    public Integer getMaxChatThreadIndex()
    {
        return maxChatThreadIndex;
    }

    public void setMaxChatThreadIndex(Integer maxChatThreadIndex)
    {
        this.maxChatThreadIndex = maxChatThreadIndex;
    }

    public static WSChatThreadListResult dummyData(int chatThreadCount, MessageSourceType messageSource)
    {
        int maxChatMessagesPerChatThread = 10;
        Integer unreadMessageCount = null;
        Integer myBroadcastCount = null;
        WSChatThreadListResult chatThreadList = new WSChatThreadListResult();

        if (messageSource.equals(MessageSourceType.DIRECT))
        {
            unreadMessageCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }
        else if (messageSource.equals(MessageSourceType.ME))
        {
            myBroadcastCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }

        for (int i = 0; i < chatThreadCount; i++)
        {
            WSAccount participantA = WSAccount.dummyData("Nyal Fernandes", "Ferns Corp", "+919898989898", true);
            WSAccount participantB = WSAccount.dummyData("Raj Seth", "Global Realty", "+919812098120", false);
            List<WSAccount> participants = new ArrayList<WSAccount>();
            participants.add(participantA);
            participants.add(participantB);

            WSChatThreadReference ref = WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount);

            chatThreadList.addChatThread(ref);
        }

        return chatThreadList;
    }
    
    public static WSChatThreadListResult dummyMetaData(int chatThreadCount, MessageSourceType messageSource)
    {
        int maxChatMessagesPerChatThread = 10;
        Integer unreadMessageCount = null;
        Integer myBroadcastCount = null;
        WSChatThreadListResult chatThreadList = new WSChatThreadListResult();

        if (messageSource.equals(MessageSourceType.DIRECT))
        {
            unreadMessageCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }
        else if (messageSource.equals(MessageSourceType.ME))
        {
            myBroadcastCount = Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true));
        }

        for (int i = 0; i < chatThreadCount; i++)
        {
            WSAccount participantA = WSAccount.dummyData("Nyal Fernandes", "Ferns Corp", "+919898989898", true);
            WSAccount participantB = WSAccount.dummyData("Raj Seth", "Global Realty", "+919812098120", false);
            List<WSAccount> participants = new ArrayList<WSAccount>();
            participants.add(participantA);
            participants.add(participantB);

            WSChatThreadReference ref = WSChatThreadReference.dummyData(1, Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 2, true)),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))),
                    participants.get(Integer.parseInt(Utils.randomString(new char[] { '0', '1' }, 1, false))), unreadMessageCount, myBroadcastCount);

            chatThreadList.addChatThread(ref);
        }

        return chatThreadList;
    }

    public static void main(String[] args)
    {
        String endpoint = "/broadcasts/{broadcast_ids}/chat_threads";
        HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
        SocketEntity entity = new SocketEntity();
        WSChatThreadListResult responseData = WSChatThreadListResult.dummyData(10, MessageSourceType.DIRECT);
        WSResult result = new WSResult("200", "Chat threads data fetched successfully.", false);
        WSResponse response = new WSResponse();
        response.setData(responseData);
        response.setResult(result);

        entity.setEndpoint(endpoint);
        entity.setRequestType(methodType);
        entity.setWithWSResponse(response);
        System.out.println(Utils.getJsonString(entity));

    }

}
