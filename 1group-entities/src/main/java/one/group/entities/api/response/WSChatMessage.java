package one.group.entities.api.response;

import one.group.core.enums.MessageType;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author miteshchavda
 * 
 */
public class WSChatMessage implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fromAccountId;

    private Integer index;

    private String propertyListingId = null;

    private String serverTime;

    private String text;

    private MessageType type;

    private WSPhotoReference photo = null;

    private String chatThreadId;

    private String clientSentTime;

    public WSChatMessage()
    {
    };

    public WSChatMessage(final String fromAccountId, final Integer index, final String serverTime, final MessageType type)
    {
        Validation.notNull(!Utils.isNullOrEmpty(fromAccountId), "The originating account ID should not be null.");
        Validation.isTrue(index < 0, "The index position should not be less that 0.");
        Validation.notNull(serverTime, "Invalid server time.");
        Validation.isTrue(!Utils.isNull(type), "The message type passed should not be null.");

        this.fromAccountId = fromAccountId;
        this.index = index;
        this.serverTime = serverTime;
        this.type = type;
        this.photo = new WSPhotoReference();
    }

    public WSChatMessage(final MessageType type, final String fromAccountId, final String chatThreadId, final String serverTime)
    {
        Validation.isTrue(!Utils.isNull(type), "Passed type should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(fromAccountId), "Passed fromAccountId should not null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Passed chatThreadId should not null or empty");
        Validation.notNull(serverTime, "Passed serverTime should not be null or empty");

        this.type = type;
        this.fromAccountId = fromAccountId;
        this.chatThreadId = chatThreadId;
        this.serverTime = serverTime;
    }

    public WSChatMessage(final MessageType type, final String fromAccountId, final String chatThreadId)
    {
        Validation.isTrue(!Utils.isNull(type), "Passed type should not be null");
        Validation.isTrue(!Utils.isNullOrEmpty(fromAccountId), "Passed fromAccountId should not null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "Passed chatThreadId should not null or empty");

        this.type = type;
        this.fromAccountId = fromAccountId;
        this.chatThreadId = chatThreadId;
    }

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public String getServerTime()
    {
        return serverTime;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setFromAccountId(String fromAccountId)
    {
        this.fromAccountId = fromAccountId;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setType(MessageType type)
    {
        this.type = type;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public String getFromAccountId()
    {
        return fromAccountId;
    }

    public String getText()

    {
        return text;
    }

    public MessageType getType()
    {
        return type;
    }

    public void setPhoto(final WSPhotoReference photo)

    {
        this.photo = photo;
    }

    public void setIndex(Integer index)
    {
        this.index = index;
    }

    public Integer getIndex()
    {
        return index;
    }

    public void setPropertyListingId(final String propertyListingId)

    {
        this.propertyListingId = propertyListingId;
    }

    public void setServerTime(final String serverTime)
    {
        this.serverTime = serverTime;
    }

    public String getClientSentTime()
    {
        return clientSentTime;
    }

    public void setClientSentTime(String clientSentTime)
    {
        this.clientSentTime = clientSentTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((fromAccountId == null) ? 0 : fromAccountId.hashCode());
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        result = prime * result + ((photo == null) ? 0 : photo.hashCode());
        result = prime * result + ((propertyListingId == null) ? 0 : propertyListingId.hashCode());
        result = prime * result + ((clientSentTime == null) ? 0 : clientSentTime.hashCode());
        result = prime * result + ((serverTime == null) ? 0 : serverTime.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSChatMessage other = (WSChatMessage) obj;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (fromAccountId == null)
        {
            if (other.fromAccountId != null)
                return false;
        }
        else if (!fromAccountId.equals(other.fromAccountId))
            return false;
        if (index == null)
        {
            if (other.index != null)
                return false;
        }
        else if (!index.equals(other.index))
            return false;
        if (photo == null)
        {
            if (other.photo != null)
                return false;
        }
        else if (!photo.equals(other.photo))
            return false;
        if (propertyListingId == null)
        {
            if (other.propertyListingId != null)
                return false;
        }
        else if (!propertyListingId.equals(other.propertyListingId))
            return false;
        if (clientSentTime == null)
        {
            if (other.clientSentTime != null)
                return false;
        }
        else if (!clientSentTime.equals(other.clientSentTime))
            return false;
        if (serverTime == null)
        {
            if (other.serverTime != null)
                return false;
        }
        else if (!serverTime.equals(other.serverTime))
            return false;
        if (text == null)
        {
            if (other.text != null)
                return false;
        }
        else if (!text.equals(other.text))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return String.format("WSChatMessage [fromAccountId=%s, index=%s, propertyListingId=%s, serverTime=%s, text=%s, type=%s, photo=%s, chatThreadId=%s, clientSentTime=%s]", fromAccountId, index,
                propertyListingId, serverTime, text, type, photo, chatThreadId, clientSentTime);
    }

}