package one.group.entities.api.request.v2;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author nyalfernandes
 *
 */
public class WSAccountIdsQuery
{
    private Set<String> accountIds = new HashSet<String>();
    
    public Set<String> getAccountIds()
    {
        return accountIds;
    }
    
    public void setAccountIds(Set<String> accountIds)
    {
        this.accountIds = accountIds;
    }
    
    public void addAccountId(String accountId)
    {
        this.accountIds.add(accountId);
    }

    @Override
    public String toString()
    {
        if (accountIds == null || accountIds.isEmpty())
        {
            return "";
        }
        
        return "WSAccountIdsQuery [accountIds=" + accountIds + "]";
    }
    
    
}
