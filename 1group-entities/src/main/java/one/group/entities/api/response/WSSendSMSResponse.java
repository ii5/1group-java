package one.group.entities.api.response;

public class WSSendSMSResponse implements ResponseEntity
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String phoneNumber;
    private String message;
    private String status;

    public WSSendSMSResponse(String phoneNumber, String message, String status)
    {
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.status = status;
    }

    public WSSendSMSResponse()
    {
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "WSSendSMSResponse [phoneNumber=" + phoneNumber + ", message=" + message + ", status=" + status + "]";
    }

}