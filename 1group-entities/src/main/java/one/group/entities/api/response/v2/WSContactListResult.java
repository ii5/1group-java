package one.group.entities.api.response.v2;

import java.util.ArrayList;
import java.util.List;

import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.MessageSourceType;
import one.group.entities.api.response.ResponseEntity;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.socket.SocketEntity;
import one.group.utils.Utils;

public class WSContactListResult implements ResponseEntity
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<WSAccount> contacts;

    public List<WSAccount> getContacts()
    {
        return contacts;
    }

    public void setContacts(List<WSAccount> contacts)
    {
        this.contacts = contacts;
    }

    public static WSContactListResult dummyData()
    {
        WSContactListResult result = new WSContactListResult();
        List<WSAccount> list = new ArrayList<WSAccount>();
        list.add(WSAccount.dummyData("Mitesh Chavda", "ii5 technology", "+919898123456", true));
        list.add(WSAccount.dummyData("Nyal Fernandes", "ii5 technology", "+919898223456", true));
        list.add(WSAccount.dummyData("Steve Watson", "Google Technology", "+919898323456", true));
        list.add(WSAccount.dummyData("Tom Smith", "ii5 technology", "+919898423456", true));
        list.add(WSAccount.dummyData("Rutu Raj", "Ahmedabad real estate agency", "+919898523456", true));
        list.add(WSAccount.dummyData("Piere Lee", "Mumbai property agent", "+919898623456", true));
        list.add(WSAccount.dummyData("Ricky Pointing", "Aus property agent", "+919898723456", true));
        list.add(WSAccount.dummyData("Foo Bar", "Foo estate agency", "+919898823456", true));
        list.add(WSAccount.dummyData("Tom harry", "Harry Properties ltd", "+919898923456", true));
        list.add(WSAccount.dummyData("Jerry Smith", "Smit realtor pvt ltd", "+919898103456", true));

        List<WSAccount> contacts = new ArrayList<WSAccount>();
        for (int i = 0; i < list.size(); i++)
        {
            WSAccount account = list.get(i);
            if (i % 2 == 0)
                account.setIsBlocked(false);
            else
                account.setIsBlocked(true);

            contacts.add(account);
        }
        result.setContacts(contacts);
        return result;
    }

    public static void main(String[] args)
    {
    	 String endpoint = "/accounts/{account_ids}/contacts";
         HTTPRequestMethodType methodType = HTTPRequestMethodType.GET;
         SocketEntity entity = new SocketEntity();
         WSContactListResult responseData = WSContactListResult.dummyData();
         WSResult result = new WSResult("200", "Contacts fully.",  false);
         WSResponse response = new WSResponse();
         response.setData(responseData);
         response.setResult(result);

         entity.setEndpoint(endpoint);
         entity.setRequestType(methodType);
         entity.setWithWSResponse(response);
         System.out.println(Utils.getJsonString(entity));

    }
}
