/**
 * 
 */
package one.group.entities.api.response;

import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;

/**
 * @author ashishthorat
 *
 */
public class WSAdminReverseContacts
{
    private String fullname;

    private String phoneNumber;

    private String registrationTime;

    private String id;

    private String shortReference;

    private WSCity city;

    private String companyName;

    private WSPhotoReference photo;

    private Status status;

    private String createdTime;

    private String lastActivityTime;

    private Long broadcastTagCount;

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(String lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

    public Long getBroadcastTagCount()
    {
        return broadcastTagCount;
    }

    public void setBroadcastTagCount(Long broadcastTagCount)
    {
        this.broadcastTagCount = broadcastTagCount;
    }

    public WSPhotoReference getPhoto()
    {
        return photo;
    }

    public void setPhoto(WSPhotoReference photo)
    {
        this.photo = photo;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public WSCity getCity()
    {
        return city;
    }

    public void setCity(WSCity city)
    {
        this.city = city;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)
    {
        this.shortReference = shortReference;
    }

    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getRegistrationTime()
    {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime)
    {
        this.registrationTime = registrationTime;
    }

    public WSAdminReverseContacts(Account account)
    {
        this.fullname = account.getFullName();
        this.companyName = account.getCompanyName();
        this.id = account.getIdAsString();
        this.createdTime = "" + account.getCreatedTime().getTime();
        this.status = account.getStatus();
        this.phoneNumber = account.getPrimaryPhoneNumber() != null ? account.getPrimaryPhoneNumber().getNumber() : null;
        this.city = new WSCity(account.getCity());

        if (account.getFullPhoto() != null)
        {
            this.photo = new WSPhotoReference(account.getFullPhoto(), account.getThumbnailPhoto());
        }

        if (account.getShortReference() != null)
        {
            this.shortReference = account.getShortReference();
        }
        if (account.getRegistrationTime() != null)
        {
            this.registrationTime = "" + account.getRegistrationTime().getTime();
        }

    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WSAdminReverseContacts other = (WSAdminReverseContacts) obj;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        return true;
    }

}
