package one.group.entities.api.response;

import java.util.List;

public class WSPaginationData implements ResponseEntity
{
	private static final long serialVersionUID = 1L;
	
	private String id;

    private int currentPage;

    private int NextPage;

    private int previousPage;

    private long pageCount;

    private long totalRecords;

    private List<WSSortCriteria> sort;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getNextPage()
    {
        return NextPage;
    }

    public void setNextPage(int nextPage)
    {
        NextPage = nextPage;
    }

    public int getPreviousPage()
    {
        return previousPage;
    }

    public void setPreviousPage(int previousPage)
    {
        this.previousPage = previousPage;
    }

    public long getPageCount()
    {
        return pageCount;
    }

    public void setPageCount(long pageCount)
    {
        this.pageCount = pageCount;
    }

    public List<WSSortCriteria> getSort()
    {
        return sort;
    }

    public void setSort(List<WSSortCriteria> sort)
    {
        this.sort = sort;
    }

    public long getTotalRecords()
    {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords)
    {
        this.totalRecords = totalRecords;
    }

    @Override
    public String toString()
    {
        return "WSPaginationData [id=" + id + ", currentPage=" + currentPage + ", NextPage=" + NextPage + ", previousPage=" + previousPage + ", pageCount=" + pageCount + ", totalRecords="
                + totalRecords + ", sort=" + sort + "]";
    }

}
