package one.group.entities.jpa.helpers;

public class AccountObject
{
    private String id;

    public static final String QUERY_FETCH_ALL_ACCOUNT_IDS_BY_STATUS = "SELECT NEW one.group.entities.jpa.helpers.AccountObject(a.id) FROM Account as a where a.status=:status";

    public AccountObject(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

}
