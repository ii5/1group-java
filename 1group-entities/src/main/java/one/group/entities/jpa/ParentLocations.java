package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * @author sanilshet
 *
 */
@Entity
@Table(name = "parent_location_admin")

@NamedQueries
(
        { 	@NamedQuery(name = ParentLocations.QUERY_GET_LOCATIONS_BY_CITIES, query = "FROM ParentLocations p WHERE p.cityId IN :cityList"),
        	@NamedQuery(name = ParentLocations.QUERY_GET_LOCATIONS_BY_CITYID, query = "FROM ParentLocations p WHERE p.cityId = :cityId"),
        	@NamedQuery(name = ParentLocations.QUERY_GET_ALL_LOCATIONS, query = "FROM ParentLocations")
        }
)
     
public class ParentLocations implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_LOCATIONS_BY_CITYID = "findLocationsByCityId";
    
    public static final String QUERY_GET_LOCATIONS_BY_CITIES = "findLocationsByCities";
    
    public static final String QUERY_GET_ALL_LOCATIONS ="findAllLocations";

    @Id
    private String id;

    @Column(name = "locality_id")
    private String localityId;

    @Column(name = "name")
    private String name;

    @Column(name = "city_id")
    private String cityId;

    public String getId()
    {
        return id;
    }

    public String getLocalityId()
    {
        return localityId;
    }

    public String getName()
    {
        return name;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setLocalityId(String localityId)
    {
        this.localityId = localityId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    @Override
    public String toString()
    {
        return "ParentLocations [id=" + id + ", localityId=" + localityId + ", name=" + name + ", cityId=" + cityId + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((localityId == null) ? 0 : localityId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParentLocations other = (ParentLocations) obj;
		if (cityId == null) {
			if (other.cityId != null)
				return false;
		} else if (!cityId.equals(other.cityId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (localityId == null) {
			if (other.localityId != null)
				return false;
		} else if (!localityId.equals(other.localityId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
