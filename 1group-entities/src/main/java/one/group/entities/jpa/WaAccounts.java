package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.WaSyncStatus;

@Entity
@Table(name = "wa_accounts")
public class WaAccounts implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "wa_mobile_no")
    private String waMobileNo;

    @Column(name = "wa_sync_status")
    @Enumerated(EnumType.STRING)
    private WaSyncStatus waSyncStatus;

    @Column(name = "wa_localstorage", length = 4000)
    private String waLocalstorage;

    @Column(name = "wa_group_selection_status")
    private int waGroupSelectionStatus;

    @Column(name = "active_group_count")
    private int activeGroupCount;

    @Column(name = "approved_group_count")
    private int approvedGroupCount;

    @Column(name = "wa_sync_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date waSyncDate;

    public String getWaMobileNo()
    {
        return waMobileNo;
    }

    public void setWaMobileNo(String waMobileNo)
    {
        this.waMobileNo = waMobileNo;
    }

    public WaSyncStatus getWaSyncStatus()
    {
        return waSyncStatus;
    }

    public void setWaSyncStatus(WaSyncStatus waSyncStatus)
    {
        this.waSyncStatus = waSyncStatus;
    }

    public String getWaLocalstorage()
    {
        return waLocalstorage;
    }

    public void setWaLocalstorage(String waLocalstorage)
    {
        this.waLocalstorage = waLocalstorage;
    }

    public int getWaGroupSelectionStatus()
    {
        return waGroupSelectionStatus;
    }

    public void setWaGroupSelectionStatus(int waGroupSelectionStatus)
    {
        this.waGroupSelectionStatus = waGroupSelectionStatus;
    }

    public int getActiveGroupCount()
    {
        return activeGroupCount;
    }

    public void setActiveGroupCount(int activeGroupCount)
    {
        this.activeGroupCount = activeGroupCount;
    }

    public int getApprovedGroupCount()
    {
        return approvedGroupCount;
    }

    public void setApprovedGroupCount(int approvedGroupCount)
    {
        this.approvedGroupCount = approvedGroupCount;
    }

    public Date getWaSyncDate()
    {
        return waSyncDate;
    }

    public void setWaSyncDate(Date waSyncDate)
    {
        this.waSyncDate = waSyncDate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((waMobileNo == null) ? 0 : waMobileNo.hashCode());
        result = prime * result + ((waSyncDate == null) ? 0 : waSyncDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaAccounts other = (WaAccounts) obj;
        if (waMobileNo == null)
        {
            if (other.waMobileNo != null)
                return false;
        }
        else if (!waMobileNo.equals(other.waMobileNo))
            return false;
        if (waSyncDate == null)
        {
            if (other.waSyncDate != null)
                return false;
        }
        else if (!waSyncDate.equals(other.waSyncDate))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaAccounts [" + (waMobileNo != null ? "waMobileNo=" + waMobileNo + ", " : "") + (waSyncStatus != null ? "status=" + waSyncStatus + ", " : "")
                + (waLocalstorage != null ? "waLocalstorage=" + waLocalstorage + ", " : "") + "waGroupSelectionStatus=" + waGroupSelectionStatus + ", activeGroupCount=" + activeGroupCount
                + ", approvedGroupCount=" + approvedGroupCount + ", " + (waSyncDate != null ? "waSyncDate=" + waSyncDate : "") + "]";
    }

}
