package one.group.entities.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
// @EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = AccountRelations.QUERY_FIND_BY_FROMACCOUNT_AND_TOACCOUNT, query = "FROM AccountRelations ar where ar.otherAccountId = :otherAccountId AND ar.account.id = :accountId or ar.otherAccountId = :accountId AND ar.account.id = :otherAccountId"),
        @NamedQuery(name = AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID, query = "FROM AccountRelations a where a.account.id = :accountId and a.otherAccountId=:otherAccountId"),
        @NamedQuery(name = AccountRelations.QUERY_FIND_BY_ACCOUNT_ID, query = "FROM AccountRelations a where a.account.id = :accountId"),
        @NamedQuery(name = AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID_LIST, query = "FROM AccountRelations a WHERE a.account.id = :accountId AND otherAccountId IN :otherAccountIdList"),
        @NamedQuery(name = AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_LIST_OTHER_ACCOUNT_ID, query = "FROM AccountRelations a WHERE a.account.id IN :otherAccountIdList AND otherAccountId = :accountId"),
        @NamedQuery(name = AccountRelations.QUERY_FIND_CONTACTS_OF_ACCOUNT, query = "SELECT otherAccountId FROM AccountRelations a WHERE a.account.id = :accountId and isContact = true"),
        })
@Table(name = "user_relations")
public class AccountRelations implements Serializable
{

    public static final String QUERY_FIND_BY_FROMACCOUNT_AND_TOACCOUNT = "findAccountRelationByFromAccountAndToAccount";
    public static final String QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID = "findAccountRelationByAccountIdAndOtherAccountId";
    public static final String QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID_LIST = "findAccountRelationByAccountIdAndOtherAccountIdList";
    public static final String QUERY_FIND_BY_ACCOUNT_ID_LIST_OTHER_ACCOUNT_ID = "findAccountRelationByAccountIdListAndOtherAccountId";
    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findAccountRelationsByAccountId";
    public static final String QUERY_FIND_CONTACTS_OF_ACCOUNT = "SELECT otherAccountId FROM AccountRelations a WHERE a.account.id = :accountId and isContact = true";
    
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "ID")
    private Account account;

    @Id
    @Column(name = "other_user_id")
    private String otherAccountId;

    /**
     * Score of first account given by second account.
     */

    @Column(name = "score", nullable = true)
    private Integer score = 0;
    /**
     * Is first account a valid contact?
     */

    @Column(name = "is_contact", nullable = true)
    private Boolean isContact = false;
    /**
     * Second account is contact of first account?
     */

    @Column(name = "is_contact_of", nullable = true)
    private Boolean isContactOf = false;
    /**
     * Is first account blocked?
     */

    @Column(name = "is_blocked", nullable = true)
    private Boolean isBlocked = false;
    /**
     * Is second account blocked by first account?
     */

    @Column(name = "is_blocked_by", nullable = true)
    private Boolean isBlockedBy = false;
    /**
     * Relationship created on
     */

    @Column(name = "created_time", nullable = true)
    private Timestamp createdTime;
    /**
     * Second account scored_as
     */

    @Column(name = "updated_time", nullable = true)
    private Timestamp updatedTime;

    @Column(name = "scored_as", nullable = true)
    private Integer scoredAs = 0;

    public AccountRelations()
    {

    }

    public AccountRelations(Account account, String otherAccountId)
    {
        this.account = account;
        this.otherAccountId = otherAccountId;
    }

    public String getOtherAccountId()
    {
        return otherAccountId;
    }

    public void setOtherAccountId(String otherAccountId)
    {
        this.otherAccountId = otherAccountId;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public Integer getScore()
    {
        return score;
    }

    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Boolean isContact()
    {
        return isContact;
    }

    public void setContact(Boolean isContact)
    {
        this.isContact = isContact;
    }

    public Boolean isContactOf()
    {
        return isContactOf;
    }

    public void setContactOf(Boolean isContactOf)
    {
        this.isContactOf = isContactOf;
    }

    public Boolean isBlocked()
    {
        return isBlocked;
    }

    public void setBlocked(Boolean isBlocked)
    {
        this.isBlocked = isBlocked;
    }

    public Boolean isBlockedBy()
    {
        return isBlockedBy;
    }

    public void setBlockedBy(Boolean isBlockedBy)
    {
        this.isBlockedBy = isBlockedBy;
    }

    public Timestamp getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime)
    {
        this.createdTime = createdTime;
    }

    public Integer getScoredAs()
    {
        return scoredAs;
    }

    public void setScoredAs(Integer scoredAs)
    {
        this.scoredAs = scoredAs;
    }

    public Timestamp getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        result = prime * result + otherAccountId.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountRelations other = (AccountRelations) obj;
        if (account.getIdAsString() == null)
        {
            if (other.account.getIdAsString() != null)
                return false;
        }
        else if (!account.getIdAsString().equals(other.account.getIdAsString()))
            return false;
        if (otherAccountId == null)
        {
            if (other.otherAccountId != null)
                return false;
        }
        else if (!otherAccountId.equals(other.otherAccountId))
            return false;

        return true;
    }

    @Override
    public String toString()
    {
        return "AccountRelations [account=" + account + ", otherAccountId=" + otherAccountId + ", score=" + score + ", isContact=" + isContact + ", isContactOf=" + isContactOf + ", isBlocked="
                + isBlocked + ", isBlockedBy=" + isBlockedBy + ", createdTime=" + createdTime + ", scoredAs=" + scoredAs + "]";
    }

}
