/**
 * 
 */
package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "starred_broadcast")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = StarredBroadcast.QUERY_FIND_STARRED_BROADCAST, query = "FROM StarredBroadcast s WHERE s.createdBy =:accountId AND s.broadcast.id =:broadcastId"),
        @NamedQuery(name = StarredBroadcast.QUERY_FIND_STARRED_BROADCAST_BY_ACCOUNT, query = "FROM StarredBroadcast s WHERE s.createdBy =:accountId"),
        @NamedQuery(name = StarredBroadcast.QUERY_FIND_LATEST_STARRED_BROADCAST_BY_ACCOUNT_WITH_LIMIT, query = "FROM StarredBroadcast s WHERE s.createdBy =:accountId ORDER BY createdTime DESC"),
        @NamedQuery(name = StarredBroadcast.QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_ACCOUNT, query = "FROM StarredBroadcast s WHERE s.createdBy =:accountId"),
        @NamedQuery(name = StarredBroadcast.QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_BROADCAST_ID, query = "FROM StarredBroadcast s WHERE s.broadcast.id =:broadcastId"), })
public class StarredBroadcast extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_STARRED_BROADCAST = "findStarredBroadcast";
    public static final String QUERY_FIND_STARRED_BROADCAST_BY_ACCOUNT = "findStarredBroadcastByAccoutId";
    public static final String QUERY_FIND_LATEST_STARRED_BROADCAST_BY_ACCOUNT_WITH_LIMIT = "findLatestStarredBroadcastByAccountIdWithLimit";
    public static final String QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_ACCOUNT = "findAllStarredBroadcastIdByAccountId";
    public static final String QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_BROADCAST_ID = "findAllStarredBroadcastIdByBroadcastId";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "broadcast_id", referencedColumnName = "ID")
    private Broadcast broadcast;

    @Column(name = "broadcast_id", insertable = false, updatable = false)
    private String broadcastId;

    public Broadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(Broadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((broadcast == null) ? 0 : broadcast.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        StarredBroadcast other = (StarredBroadcast) obj;

        if (broadcast == null)
        {
            if (other.broadcast != null)
                return false;
        }
        else if (!broadcast.equals(other.broadcast))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "StarredBroadcast [ broadcast=" + broadcast + "]";
    }

}
