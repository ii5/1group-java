package one.group.entities.jpa.helpers;

import java.util.Date;

public class BroadcastObject
{
    private String id;
    private Date createdTime;
    private Date updatedTime;

    public BroadcastObject()
    {

    }

    public BroadcastObject(String id, Date createdTime, Date updatedTime)
    {
        this.id = id;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

}
