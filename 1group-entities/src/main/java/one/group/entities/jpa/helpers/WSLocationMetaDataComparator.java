package one.group.entities.jpa.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import one.group.core.Constant;
import one.group.core.enums.SortType;
import one.group.entities.api.response.bpo.WSLocationMetaData;
import one.group.utils.Utils;

/**
 * 
 * @author nyalf
 *
 */
public class WSLocationMetaDataComparator implements Comparator<WSLocationMetaData>
{
	private SortType type;
	
	public WSLocationMetaDataComparator(SortType type)
	{
		if (type == null)
		{
			type = SortType.ASCENDING;
		}
		else
		{
			this.type = type;
		}
	}
	
	public int compare(WSLocationMetaData o1, WSLocationMetaData o2) 
	{
		if (o1 == null || o1.getValue() == null)
		{
			if (type.equals(SortType.ASCENDING))
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		
		if (o2 == null || o2.getValue() == null)
		{
			if (type.equals(SortType.ASCENDING))
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		
		if (o1.equals(o2))
		{
			return 0;
		}
		
		String value1 = o1.getValue();
		String value2 = o2.getValue();
		
		if (value1.equals(value2))
		{
			if (type.equals(SortType.ASCENDING))
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		
		if (type.equals(SortType.ASCENDING))
		{
			return value1.compareTo(value2);
		}
		else
		{
			return value2.compareTo(value1);
		}
		
	}
	
	
	public static void main(String[] args)
	{
		List<WSLocationMetaData> locList = new ArrayList<WSLocationMetaData>();
		
		for (int i = 0; i < 100; i++)
		{
			WSLocationMetaData m = new WSLocationMetaData();
			m.setId(i+"");
			m.setLabel(Utils.randomString(Constant.CHARSET_ALPHABETS, 3, true));
			m.setValue(m.getLabel());
			locList.add(m);
		}
		
		Collections.sort(locList, new WSLocationMetaDataComparator(SortType.ASCENDING));
		
		for (WSLocationMetaData m : locList)
		{
			System.out.println(m.getId() + " : " + m.getValue());
		}
	}
}
