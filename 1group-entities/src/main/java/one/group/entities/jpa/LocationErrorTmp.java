package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import one.group.core.enums.LocationTmpStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "location_error_tmp")
@EntityListeners(EntryAuditEntityListener.class)
public class LocationErrorTmp extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    @Column(name = "city_id")
    private City city;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "parent_id")
    private String parentId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private LocationTmpStatus status;

    @Column(name = "action")
    private String action;

    @Column(name = "errors")
    private String errors;

    @Column(name = "suggestions")
    private String suggestions;

    @Column(name = "login_user_id")
    private int loginUserId;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public String getSuggestions()
    {
        return suggestions;
    }

    public void setSuggestions(String suggestions)
    {
        this.suggestions = suggestions;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public LocationTmpStatus getStatus()
    {
        return status;
    }

    public void setStatus(LocationTmpStatus status)
    {
        this.status = status;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getErrors()
    {
        return errors;
    }

    public void setErrors(String errors)
    {
        this.errors = errors;
    }

    public int getLoginUserId()
    {
        return loginUserId;
    }

    public void setLoginUserId(int loginUserId)
    {
        this.loginUserId = loginUserId;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((errors == null) ? 0 : errors.hashCode());
        result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result + loginUserId;
        result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((suggestions == null) ? 0 : suggestions.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocationErrorTmp other = (LocationErrorTmp) obj;
        if (action == null)
        {
            if (other.action != null)
                return false;
        }
        else if (!action.equals(other.action))
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (errors == null)
        {
            if (other.errors != null)
                return false;
        }
        else if (!errors.equals(other.errors))
            return false;
        if (latitude == null)
        {
            if (other.latitude != null)
                return false;
        }
        else if (!latitude.equals(other.latitude))
            return false;
        if (loginUserId != other.loginUserId)
            return false;
        if (longitude == null)
        {
            if (other.longitude != null)
                return false;
        }
        else if (!longitude.equals(other.longitude))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parentId == null)
        {
            if (other.parentId != null)
                return false;
        }
        else if (!parentId.equals(other.parentId))
            return false;
        if (status != other.status)
            return false;
        if (suggestions == null)
        {
            if (other.suggestions != null)
                return false;
        }
        else if (!suggestions.equals(other.suggestions))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "LocationErrorTmp [city=" + city + ", name=" + name + ", type=" + type + ", parentId=" + parentId + ", status=" + status + ", action=" + action + ", errors=" + errors
                + ", suggestions=" + suggestions + ", loginUserId=" + loginUserId + ", latitude=" + latitude + ", longitude=" + longitude + "]";
    }

}
