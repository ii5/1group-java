package one.group.entities.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import one.group.core.enums.AccountType;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 *         TODO : Define parameterized constructor. TODO : Remove destructive
 *         setters.
 */

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@DiscriminatorValue(value = "AGENT")
public class Agent extends Account
{

    private static final long serialVersionUID = 1L;

    /**
     * The {@link Association}s that the agents belong to or has requested to be
     * a part of.
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "agents")
    private Set<Association> associations = new HashSet<Association>();

    /**
     * Contacts of the agent. A contact can either be another {@link Agent}, a
     * {@link Developer} or an anonymous(raw user) {@link Account}.
     * 
     * Look at: {@link AccountType}
     */
    // ? Should all the users contacts be deleted once an agent is? (mind: not
    // deactivated but deleted). for now it will not, CascadeType.ALL should be
    // the value then.
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY)
    private Set<Account> contacts = new HashSet<Account>();

    /**
     * An agent can have multiple devices.
     */
    // ? One Agent can logon through many devices.
    // If a device is shared with two or more users, a separate entry
    // would be created for that purpose. Device sharing across records should
    // not be
    // permitted for auditing purpose.
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
    private Set<Client> clients = new HashSet<Client>();

    /**
     * Power search saved queries of an agent.
     */
    // @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
    // "account")
    // private Set<Search> savedSearches = new HashSet<Search>();

    /**
     * The trust scores assigned to the current agent.
     */
    // @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
    // "toAccount")
    // private Set<TrustScore> trustScores = new HashSet<TrustScore>();

    /**
     * The invitations sent to other agents to request move.in access.
     */
    // ? Should we just log this in another table. Will help log status changes
    // too.
    // @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
    // "toAccount")
    // private Set<Invitation> invitations = new HashSet<Invitation>();

    public Set<Association> getAssociations()
    {
        return associations;
    }

    public void setAssociations(Set<Association> associations)
    {
        this.associations = associations;
    }

    public void addAssociation(Association association)
    {
        Validation.notNull(association, "Association passed should not be null.");
        this.associations.add(association);
    }

    public Set<Account> getContacts()
    {
        return contacts;
    }

    public void setContacts(Set<Account> contacts)
    {
        this.contacts = contacts;
    }

    public void addContact(Account contact)
    {
        if (!contact.equals(this))
        {
            this.contacts.add(contact);
        }
    }

    public Set<Client> getDevices()
    {
        return clients;
    }

    public void setClients(Set<Client> client)
    {
        this.clients = client;
    }

    public void addClient(Client client)
    {
        this.clients.add(client);
    }

}