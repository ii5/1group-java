package one.group.entities.jpa.listeners;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import one.group.core.Constant;
import one.group.entities.jpa.base.BaseEntity;

/**
 * Defines callback methods to add basic audit log information to parent
 * {@link BaseEntity}.
 * 
 * @author nyalfernandes
 * 
 */
public class EntryAuditEntityListener
{

    public String getUser()
    {
        return Constant.ADMIN;
    }

    @PrePersist
    public void prePersist(final BaseEntity base)
    {
        if (base.getCreatedTime() == null)
        {
            base.setCreatedTime(new Date());
        }

        if (base.getCreatedBy() == null)
        {
            base.setCreatedBy(getUser());
        }
    }

    @PreUpdate
    public void preUpdate(final BaseEntity base)
    {
        if (base.getUpdatedTime() == null)
        {
            base.setUpdatedTime(new Date());
        }

        if (base.getUpdatedBy() == null)
        {
            base.setUpdatedBy(getUser());
        }
    }
}
