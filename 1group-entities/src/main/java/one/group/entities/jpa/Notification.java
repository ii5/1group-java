package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.status.NotificationStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "notification")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Notification.QUERY_TOTAL_COUNT_BY_STATUS, query = "select count(*) FROM Notification where createdForUserId=:createdForUserId and status=:status") })
public class Notification extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    public static final String QUERY_TOTAL_COUNT_BY_STATUS = "queryTotalCountNotificationsByStatus";

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "body", nullable = false)
    private String body;

    @Column(name = "created_for_user_id", nullable = false)
    private String createdForUserId;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationStatus status;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getCreatedForUserId()
    {
        return createdForUserId;
    }

    public void setCreatedForUserId(String createdForUserId)
    {
        this.createdForUserId = createdForUserId;
    }

    public NotificationStatus getStatus()
    {
        return status;
    }

    public void setStatus(NotificationStatus status)
    {
        this.status = status;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((body == null) ? 0 : body.hashCode());
        result = prime * result + ((createdForUserId == null) ? 0 : createdForUserId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Notification other = (Notification) obj;
        if (body == null)
        {
            if (other.body != null)
                return false;
        }
        else if (!body.equals(other.body))
            return false;
        if (createdForUserId == null)
        {
            if (other.createdForUserId != null)
                return false;
        }
        else if (!createdForUserId.equals(other.createdForUserId))
            return false;
        if (status != other.status)
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        return true;
    }

}
