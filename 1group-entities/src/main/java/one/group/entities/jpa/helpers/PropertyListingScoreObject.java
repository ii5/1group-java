package one.group.entities.jpa.helpers;

import java.util.Date;

import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class PropertyListingScoreObject
{
    private String id;

    private String accountId;

    private String locationId;

    private Date lastRenewedTime;

    private Date creationTime;

    private Boolean isHot;

    private String description;

    private Integer descriptionLength;

    private long salePrice;

    private long rentPrice;

    private String rooms;

    private String amenities;

    private PropertyType type;

    private PropertySubType subType;

    private int area;

    private PropertyMarket propertyMarket;

    private CommissionType commissionType;

    private Account account;

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_AND_STATUS = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p  WHERE p.location IN :locationList AND p.status = :status " + "ORDER BY p.createdTime DESC";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_AND_STATUS_1 = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.descriptionLength, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p  WHERE p.location IN :locationList AND p.status = :status " + "ORDER BY p.createdTime DESC";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p  WHERE " + "p.location.id IN :locationIdList AND " + "p.status = :status";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS_1 = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.descriptionLength, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + "FROM PropertyListing p  WHERE p.location.id IN :locationIdList AND p.status = :status " + "ORDER BY p.createdTime DESC ";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS_INCLUDING_OWN_PROPERTY_LISTING = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.descriptionLength, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + "FROM PropertyListing p  WHERE (p.location.id IN :locationIdList AND p.status = :status) OR (p.account.id = :accountId) ORDER BY p.createdTime DESC ";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_STATUS = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.descriptionLength, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p WHERE p.status = :status";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_ACCOUNT_ID_LIST_LOCATION_LIST_AND_STATUS = "SELECT NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p WHERE p.account.id IN :accountIdList AND p.location IN :locationList AND p.status = :status";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_ACCOUNT_ID_LIST_LOCATION_ID_LIST_AND_STATUS = "SELECT NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p WHERE p.account.id IN :accountIdList AND p.location.id IN :locationIdList AND p.status = :status";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION = "SELECT "
            + "NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.descriptionLength, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p  WHERE (p.location.id IN (SELECT n.nearByLocation.id from NearByLocality n where n.location.id = :lId)"
            + "OR (p.location.id IN (SELECT m.matchLocation.id from LocationMatch m where m.location.id = :mId and m.isExact = '1') ) OR p.location.id = :pId ) AND p.status =:status";

    public static final String QUERY_FETCH_ACCOUNTS_OF_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS = "SELECT NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account) "
            + " FROM PropertyListing p WHERE p.id IN :propertyListingIdList ";

    public static final String QUERY_FIND_IS_PROPERTY_LISTINGS_EXISTS_FOR_ACCOUNT_ID = "SELECT NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id) "
            + " FROM PropertyListing p WHERE p.account.id = :accountId";

    public static final String QUERY_SEARCH_PROPERTY_LISTINGS_BY_FOR_OWN_ACCOUNT_ID = "SELECT NEW one.group.entities.jpa.helpers.PropertyListingScoreObject(p.id, p.account.id, p.location.id, p.lastRenewedTime, p.createdTime, p.isHot, p.description, p.salePrice, p.rentPrice, p.rooms, p.amenities, p.type, p.subType, p.area, p.propertyMarket, p.commissionType) "
            + " FROM PropertyListing p WHERE p.account.id = :accountId AND p.status = :status";

    public PropertyListingScoreObject()
    {

    }

    public PropertyListingScoreObject(String id, String createdBy, String locationid, Date lastRenewedTime, Date createdTime, boolean isHot, String description, Long salePrice, Long rentPrice,
            String rooms, String amenities, PropertyType type, PropertySubType subType, int area, PropertyMarket propertyMarket, CommissionType commissionType)

    {
        this.id = id;
        this.accountId = createdBy;
        this.locationId = locationid;
        this.lastRenewedTime = lastRenewedTime;
        this.creationTime = createdTime;
        this.isHot = isHot;
        this.description = description;
        this.salePrice = salePrice;
        this.rentPrice = rentPrice;
        this.rooms = rooms;
        this.amenities = amenities;
        this.type = type;
        this.subType = subType;
        this.area = area;
        this.propertyMarket = propertyMarket;
        this.commissionType = commissionType;

    }

    public PropertyListingScoreObject(String id, String createdBy, String locationid, Date lastRenewedTime, Date createdTime, boolean isHot, Integer descriptionLength, Long salePrice, Long rentPrice,
            String rooms, String amenities, PropertyType type, PropertySubType subType, int area, PropertyMarket propertyMarket, CommissionType commissionType)

    {
        this.id = id;
        this.accountId = createdBy;
        this.locationId = locationid;
        this.lastRenewedTime = lastRenewedTime;
        this.creationTime = createdTime;
        this.isHot = isHot;
        // this.description = description;
        this.descriptionLength = descriptionLength;
        this.salePrice = salePrice;
        this.rentPrice = rentPrice;
        this.rooms = rooms;
        this.amenities = amenities;
        this.type = type;
        this.subType = subType;
        this.area = area;
        this.propertyMarket = propertyMarket;
        this.commissionType = commissionType;

    }

    public PropertyListingScoreObject(String id, String createdBy, String locationid, Date lastRenewedTime, Date createdTime, boolean isHot, String description, Integer descriptionLength,
            Long salePrice, Long rentPrice, String rooms, String amenities, PropertyType type, PropertySubType subType, int area, PropertyMarket propertyMarket, CommissionType commissionType)

    {
        this.id = id;
        this.accountId = createdBy;
        this.locationId = locationid;
        this.lastRenewedTime = lastRenewedTime;
        this.creationTime = createdTime;
        this.isHot = isHot;
        this.description = description;
        this.descriptionLength = descriptionLength;
        this.salePrice = salePrice;
        this.rentPrice = rentPrice;
        this.rooms = rooms;
        this.amenities = amenities;
        this.type = type;
        this.subType = subType;
        this.area = area;
        this.propertyMarket = propertyMarket;
        this.commissionType = commissionType;

    }

    public PropertyListingScoreObject(PropertyListing p, Account owningAccount, Location propertyLocation)
    {
        this.setId(p.getId());
        this.setAccountId(owningAccount == null ? p.getAccount().getId() : owningAccount.getId());
        this.setLocationId(propertyLocation == null ? p.getLocation().getId() : propertyLocation.getId());
        this.setLastRenewedTime(p.getLastRenewedTime());
        this.setCreationTime(p.getCreatedTime());
        this.setIsHot(p.getIsHot());
        this.setDescription(p.getDescription());
        this.setDescriptionLength((p.getDescription() == null ? 0 : p.getDescription().length()));
        this.setSalePrice(p.getSalePrice());
        this.setRentPrice(p.getRentPrice());
        this.setRooms(p.getRooms());
        this.setAmenities(p.getAmenities());
        this.setType(p.getType());
        this.setSubType(p.getSubType());
        this.setArea(p.getArea());
        this.setPropertyMarket(p.getPropertyMarket());
        this.setCommissionType(p.getCommissionType());
    }

    public PropertyListingScoreObject(String propertyListingId, Account account)
    {
        this.setId(propertyListingId);
        this.setAccount(account);
    }

    public PropertyListingScoreObject(String propertyListingId)
    {
        this.setId(propertyListingId);
    }

    public Integer getDescriptionLength()
    {
        return descriptionLength;
    }

    public void setDescriptionLength(Integer descriptionLength)
    {
        this.descriptionLength = descriptionLength;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public Date getLastRenewedTime()
    {
        return lastRenewedTime;
    }

    public void setLastRenewedTime(Date lastRenewedTime)
    {
        this.lastRenewedTime = lastRenewedTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }

    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Boolean getIsHot()
    {
        return isHot;
    }

    public void setIsHot(Boolean isHot)
    {
        this.isHot = isHot;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public long getSalePrice()
    {
        return salePrice;
    }

    public void setSalePrice(long salePrice)
    {
        this.salePrice = salePrice;
    }

    public long getRentPrice()
    {
        return rentPrice;
    }

    public void setRentPrice(long rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public int getArea()
    {
        return area;
    }

    public void setArea(int area)
    {
        this.area = area;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType commissionType)
    {
        this.commissionType = commissionType;
    }

    public PropertyType getType()
    {
        return type;
    }

    public void setType(PropertyType type)
    {
        this.type = type;
    }

    public PropertySubType getSubType()
    {
        return subType;
    }

    public void setSubType(PropertySubType subType)
    {
        this.subType = subType;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public String getAmenities()
    {
        return amenities;
    }

    public void setAmenities(String amenities)
    {
        this.amenities = amenities;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PropertyListingScoreObject other = (PropertyListingScoreObject) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public boolean equalLogically(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PropertyListingScoreObject other = (PropertyListingScoreObject) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (amenities == null)
        {
            if (other.amenities != null)
                return false;
        }
        else if (!amenities.equals(other.amenities))
            return false;
        if (area != other.area)
            return false;
        if (commissionType != other.commissionType)
            return false;
        if (description == null)
        {
            if (other.description != null)
                return false;
        }
        else if (!description.equals(other.description))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        if (propertyMarket != other.propertyMarket)
            return false;
        if (rentPrice != other.rentPrice)
            return false;
        if (rooms == null)
        {
            if (other.rooms != null)
                return false;
        }
        else if (!rooms.equals(other.rooms))
            return false;
        if (salePrice != other.salePrice)
            return false;
        if (subType != other.subType)
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "PropertyListingScoreObject [id=" + id + ", accountId=" + accountId + ", locationId=" + locationId + ", lastRenewedTime=" + lastRenewedTime + ", creationTime=" + creationTime
                + ", isHot=" + isHot + ", description=" + description + ", descriptionLength=" + descriptionLength + ", salePrice=" + salePrice + ", rentPrice=" + rentPrice + ", rooms=" + rooms
                + ", amenities=" + amenities + ", type=" + type + ", subType=" + subType + ", area=" + area + ", propertyMarket=" + propertyMarket + ", commissionType=" + commissionType
                + ", account=" + account + "]";
    }

}
