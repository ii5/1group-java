package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.status.NearByLocalityStatus;

/**
 * 
 * @author miteshchavda
 *
 */
@Entity
@Table(name = "nearby_location")
@NamedQueries({ @NamedQuery(name = NearByLocation.QUERY_GET_ACTIVE_LOCALITY_OF_LOCATION, query = "FROM NearByLocation n WHERE n.location.id = :locationId") })
public class NearByLocation implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_ACTIVE_LOCALITY_OF_LOCATION = "findActiveLocalityByLocationId";

    @Id
    @Column
    private String id;

    @ManyToOne
    private Location location;

    @JoinColumn(name = "nearby_location", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location nearByLocation;

    @Enumerated(EnumType.STRING)
    private NearByLocalityStatus status;

    @Column(name = "city_id")
    private String cityId;

    @Column(name = "created_time")
    private Date CreatedTime;

    public NearByLocation()
    {

    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Location getNearByLocation()
    {
        return nearByLocation;
    }

    public void setNearByLocation(Location nearByLocation)
    {
        this.nearByLocation = nearByLocation;
    }

    public NearByLocalityStatus getStatus()
    {
        return status;
    }

    public void setStatus(NearByLocalityStatus status)
    {
        this.status = status;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public Date getCreatedTime()
    {
        return CreatedTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        CreatedTime = createdTime;
    }

}
