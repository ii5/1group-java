package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "account_message_cursors")
@EntityListeners(EntryAuditEntityListener.class)
public class MessageCursors extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "receive_index")
    private long receiveIndex = -1;

    @Column(name = "read_index")
    private long readIndex = -1;

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public long getReceiveIndex()
    {
        return receiveIndex;
    }

    public void setReceiveIndex(long receiveIndex)
    {
        this.receiveIndex = receiveIndex;
    }

    public long getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(long readIndex)
    {
        this.readIndex = readIndex;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        result = prime * result + (int) (readIndex ^ (readIndex >>> 32));
        result = prime * result + (int) (receiveIndex ^ (receiveIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MessageCursors other = (MessageCursors) obj;
        if (account == null)
        {
            if (other.account != null)
                return false;
        }
        else if (!account.equals(other.account))
            return false;
        if (readIndex != other.readIndex)
            return false;
        if (receiveIndex != other.receiveIndex)
            return false;
        return true;
    }

}
