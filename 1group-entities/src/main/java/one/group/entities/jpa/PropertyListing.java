package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.base.BaseEntity;

/**
 * Properties listed by an agent and viewed by others.
 * 
 * @author nyalfernandes
 * 
 *         TODO : Add parameterized Constructors and delete destructive setters.
 *         TODO : Add a proper description. TODO : required minimum parameters
 *         for a property. TODO : Decide if we need currency unit and the action
 *         accordingly.
 * 
 */
@Entity
@Table(name = "property_listing")
@NamedQueries({
        @NamedQuery(name = PropertyListing.QUERY_FIND_PROPERTY_EXISTS, query = "SELECT count(p) > 0 FROM PropertyListing p WHERE p.id =:Id"),
        @NamedQuery(name = PropertyListing.QUERY_TOTAL_COUNT_PROPERTY_LISTING_FOR_ACCOUNT, query = "SELECT count(p.id) FROM PropertyListing p WHERE p.account.id =:accountId AND p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_GET_PROPERTY_LISTINIG_BY_ACCOUNT_ID, query = "FROM PropertyListing p WHERE p.account.id = :accountId AND p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_GET_ALL_PROPERTY_LISTINIG_BY_ACCOUNT_IDS, query = "FROM PropertyListing p WHERE p.status IN :statusList AND p.account.id IN :accountIdList"),
        @NamedQuery(name = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_ASC, query = "FROM PropertyListing p WHERE p.account.id = :accountId AND p.status IN :statusList ORDER BY p.createdTime ASC"),
        @NamedQuery(name = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_DESC, query = "FROM PropertyListing p WHERE p.account.id = :accountId AND p.status IN :statusList ORDER BY p.createdTime DESC"),
        @NamedQuery(name = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_SHORT_REFERENCE, query = "FROM PropertyListing p WHERE p.shortReference = :shortReference"),
        @NamedQuery(name = PropertyListing.QUERY_FIND_IDS_BY_LOCALITY_ID, query = "SELECT p.id FROM PropertyListing p WHERE p.location.id = :locationId"),
        @NamedQuery(name = PropertyListing.QUERY_FIND_IDS_BY_LOCALITY_ID_NEW, query = "SELECT p FROM PropertyListing p WHERE p.location.id = :LOCATION_ID"),
        @NamedQuery(name = PropertyListing.QUERY_PROPERTY_LISTING_BY_STATUS, query = "FROM PropertyListing p WHERE p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_PROPERTY_LISTING_IDS_BY_STATUS, query = "SELECT p.id FROM PropertyListing p WHERE p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_GET_ALL_PROPERTY_LISTING, query = "FROM PropertyListing p"),
        @NamedQuery(name = PropertyListing.QUERY_GET_TOTAL_COUNT_ACTIVE_PROPERTY_LISTING, query = "SELECT count(p.id) FROM PropertyListing p WHERE p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_IDS_AND_STATUS, query = "FROM PropertyListing p WHERE p.id IN :propertyListingIdList AND p.status IN :statusList"),
        @NamedQuery(name = PropertyListing.QUERY_GET_TOTAL_COUNT_ALL_PROPERTY_LISTING, query = "SELECT count(p.id) FROM PropertyListing p"),
        @NamedQuery(name = PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS, query = "FROM PropertyListing p WHERE p.id IN :propertyListingIdList"),
        @NamedQuery(name = PropertyListing.TYPED_QUERY_FIND_IDS_BY_PROPERTY_LISTING_IDS_AND_STATUS, query = "SELECT p.id FROM PropertyListing p WHERE p.id IN :propertyListingIdList AND p.status = :status"),
        @NamedQuery(name = PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS_AND_LAST_RENEWED_TIME, query = "FROM PropertyListing p WHERE p.id IN :propertyListingIdList AND p.lastRenewedTime IS NOT NULL"), })
public class PropertyListing extends BaseEntity
{
    public static final String QUERY_GET_PROPERTY_LISTINIG_BY_ACCOUNT_ID = "findPropertyListingByAccountId";
    public static final String QUERY_GET_ALL_PROPERTY_LISTINIG_BY_ACCOUNT_IDS = "findPropertyListingByAccountIds";
    public static final String QUERY_GET_PROPERTY_LISTING_BY_SHORT_REFERENCE = "findPropertyListingByShortReference";
    public static final String QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_ASC = "findPropertyListingByAccountIdCreatedTimeASC";
    public static final String QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_DESC = "findPropertyListingByAccountIdCreatedTimeDESC";
    public static final String QUERY_FIND_BY_SHORTREFERENCE = "findPropertyListingByShortReference";
    public static final String QUERY_FIND_IDS_BY_LOCALITY_ID = "findPropertyListingIdsByLocalityId";
    public static final String QUERY_FIND_IDS_BY_LOCALITY_ID_NEW = "findPropertyListingIdsByLocalityIdNew";
    public static final String TYPED_QUERY_FIND_IDS_BY_LOCALITY_ID = "SELECT p.id FROM PropertyListing p WHERE p.location.id = :locationId";
    public static final String QUERY_PROPERTY_LISTING_BY_STATUS = "findPropertyListingByStatus";
    public static final String QUERY_PROPERTY_LISTING_IDS_BY_STATUS = "findPropertyListingIdsByStatus";
    public static final String QUERY_FIND_PROPERTY_LISTINGS_BY_IDS_AND_STATUS = "findPropertyListingsByIdsAndStatus";
    public static final String QUERY_FIND_PROPERTY_EXISTS = "isPropertyExists";
    public static final String QUERY_TOTAL_COUNT_PROPERTY_LISTING_FOR_ACCOUNT = "getTotalCountOfPropertyByAccountId";
    public static final String QUERY_GET_ALL_PROPERTY_LISTING = "getAllPropertyListings";
    public static final String QUERY_GET_TOTAL_COUNT_ACTIVE_PROPERTY_LISTING = "getTotalCountOfActivePropertyListings";
    public static final String QUERY_GET_TOTAL_COUNT_ALL_PROPERTY_LISTING = "getTotalCountOfAllPropertyListings";
    public static final String QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS = "findPropertyListingsByPropertyListingIds";
    public static final String TYPED_QUERY_FIND_IDS_BY_PROPERTY_LISTING_IDS_AND_STATUS = "findPropertyListingIdsByPropertyListingIdsAndStatus";
    public static final String QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS_AND_LAST_RENEWED_TIME = "findPropertyListingsByPropertyListingIdsAndLastRenewedTime";

    private static final long serialVersionUID = 1L;

    /**
     * reference id for a property
     */
    @Column(name = "short_reference", length = 24, nullable = true)
    private String shortReference;

    /**
     * The description of a property. Needs a one liner atleast.
     */
    @Column(nullable = true, length = 4000)
    private String description;

    /**
     * 
     */
    @Column(name = "description_length", nullable = false)
    private Integer descriptionLength = 0;

    /**
     * The type of property.
     */
    @Enumerated(EnumType.STRING)
    private PropertyType type;

    /**
     * The subtype of a property.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "sub_type")
    private PropertySubType subType;

    /**
     * The selling price of the property.
     */
    // Capture units (IMPORTANT)
    @Column(name = "sale_price", nullable = false)
    private long salePrice;

    /**
     * The purchasing price of the property.
     */
    @Column(name = "rent_price", nullable = false)
    private long rentPrice;

    /**
     * The size of the property.
     */
    @Column(nullable = true)
    private int area;

    @Column(name = "amenities", nullable = true, length = 1000)
    private String amenities;

    /**
     * The locality to which the Property belongs to.
     */
    @ManyToOne
    private Location location;

    /**
     * Flag to notify if a property is hot or no.
     */
    @Column(nullable = true, name = "is_hot")
    private Boolean isHot;

    /**
     * The {@link Account} who is the owner of the property listing.
     */
    @ManyToOne
    private Account account;

    /**
     * The bhk selector.
     */
    @Column(nullable = true)
    private String rooms;

    /**
     * The status of a property based on various determining factors.
     */
    @Enumerated(EnumType.STRING)
    private BroadcastStatus status;

    /**
     * The commission split stratergy
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "commission_type", nullable = true)
    private CommissionType commissionType;

    @Column(nullable = true, name = "last_renewed_time")
    private Date lastRenewedTime;

    @Column(name = "external_source", nullable = true)
    private String externalSource;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_market", nullable = true)
    private PropertyMarket propertyMarket;

    @Column(name = "custom_sublocation", nullable = true)
    private String customSubLocation;

    public PropertyListing()
    {
        setCreatedTime(new Date());
    }

    public Integer getDescriptionLength()
    {
        return descriptionLength;
    }

    public void setDescriptionLength(Integer descriptionLength)
    {
        this.descriptionLength = descriptionLength;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String referenceId)
    {
        this.shortReference = referenceId;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public PropertyType getType()
    {
        return type;
    }

    public void setType(PropertyType type)
    {
        this.type = type;
    }

    public PropertySubType getSubType()
    {
        return subType;
    }

    public void setSubType(PropertySubType subType)
    {
        this.subType = subType;
    }

    public long getSalePrice()
    {
        return salePrice;
    }

    public void setSalePrice(long salePrice)
    {
        this.salePrice = salePrice;
    }

    public long getRentPrice()
    {
        return rentPrice;
    }

    public void setRentPrice(long rentPrice)
    {
        this.rentPrice = rentPrice;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Boolean getIsHot()
    {
        return isHot;
    }

    public void setIsHot(boolean isHot)
    {
        this.isHot = isHot;
    }

    public BroadcastStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastStatus status)
    {
        this.status = status;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public int getArea()
    {
        return area;
    }

    public void setArea(int area)
    {
        this.area = area;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public String getExternalSource()
    {
        return externalSource;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public String getCustomSubLocation()
    {
        return customSubLocation;
    }

    public void setCommissionType(CommissionType commisionType)
    {
        this.commissionType = commisionType;
    }

    public void setExternalSource(String externalSource)
    {
        this.externalSource = externalSource;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public void setCustomSubLocation(String customSubLocation)
    {
        this.customSubLocation = customSubLocation;
    }

    public Date getLastRenewedTime()
    {
        return lastRenewedTime;
    }

    public void setLastRenewedTime(Date lastRenewedTime)
    {
        this.lastRenewedTime = lastRenewedTime;
    }

    public String getAmenities()
    {
        return amenities;
    }

    public void setAmenities(String amenities)
    {
        this.amenities = amenities;
    }

    @Override
    public String toString()
    {
        return "PropertyListing [shortReference=" + shortReference + ", description=" + description + ", type=" + type + ", subType=" + subType + ", salePrice=" + salePrice + ", rentPrice="
                + rentPrice + ", area=" + area + ", amenities=" + amenities + ", location=" + location + ", isHot=" + isHot + ", account=" + account + ", rooms=" + rooms + ", status=" + status
                + ", commissionType=" + commissionType + ", lastRenewedTime=" + lastRenewedTime + ", externalSource=" + externalSource + ", propertyMarket=" + propertyMarket + ", customSubLocation="
                + customSubLocation + "]";
    }
}