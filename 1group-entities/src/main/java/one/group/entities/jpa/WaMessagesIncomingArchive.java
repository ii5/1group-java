/**
 * 
 */
package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.MessageType;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_messages_incoming_archive")
public class WaMessagesIncomingArchive implements Serializable
{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "wa_message_id")
    private String waMessageId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wa_mobile_no", referencedColumnName = "ID")
    private PhoneNumber waMobileNo;

    @Column(name = "wa_group_id")
    private String waGroupId;

    @Column(name = "wa_message_text", length = 4000)
    private String waMessageText;

    @Column(name = "wa_message_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date waMessageTime;

    @Column(name = "record_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date RecordDate;

    @Column(name = "status")
    private int status;

    @Column(name = "filePath", length = 600)
    private String filePath;

    @Column(name = "message_type_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date messageTypeTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "message_type")
    private MessageType messageType;

    @Column(name = "archive_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date archiveTime;

    public PhoneNumber getWaMobileNo()
    {
        return waMobileNo;
    }

    public void setWaMobileNo(PhoneNumber waMobileNo)
    {
        this.waMobileNo = waMobileNo;
    }

    public String getWaMessageText()
    {
        return waMessageText;
    }

    public void setWaMessageText(String waMessageText)
    {
        this.waMessageText = waMessageText;
    }

    public Date getWaMessageTime()
    {
        return waMessageTime;
    }

    public void setWaMessageTime(Date waMessageTime)
    {
        this.waMessageTime = waMessageTime;
    }

    public Date getRecordDate()
    {
        return RecordDate;
    }

    public void setRecordDate(Date recordDate)
    {
        RecordDate = recordDate;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public Date getMessageTypeTime()
    {
        return messageTypeTime;
    }

    public void setMessageTypeTime(Date messageTypeTime)
    {
        this.messageTypeTime = messageTypeTime;
    }

    public Date getArchiveTime()
    {
        return archiveTime;
    }

    public void setArchiveTime(Date archiveTime)
    {
        this.archiveTime = archiveTime;
    }

    public MessageType getMessageType()
    {
        return messageType;
    }

    public void setMessageType(MessageType messageType)
    {
        this.messageType = messageType;
    }

    public String getWaMessageId()
    {
        return waMessageId;
    }

    public void setWaMessageId(String waMessageId)
    {
        this.waMessageId = waMessageId;
    }

    public String getWaGroupId()
    {
        return waGroupId;
    }

    public void setWaGroupId(String waGroupId)
    {
        this.waGroupId = waGroupId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((RecordDate == null) ? 0 : RecordDate.hashCode());
        result = prime * result + ((archiveTime == null) ? 0 : archiveTime.hashCode());
        result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
        result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
        result = prime * result + ((messageTypeTime == null) ? 0 : messageTypeTime.hashCode());
        result = prime * result + status;
        result = prime * result + ((waGroupId == null) ? 0 : waGroupId.hashCode());
        result = prime * result + ((waMessageId == null) ? 0 : waMessageId.hashCode());
        result = prime * result + ((waMessageText == null) ? 0 : waMessageText.hashCode());
        result = prime * result + ((waMessageTime == null) ? 0 : waMessageTime.hashCode());
        result = prime * result + ((waMobileNo == null) ? 0 : waMobileNo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaMessagesIncomingArchive other = (WaMessagesIncomingArchive) obj;
        if (RecordDate == null)
        {
            if (other.RecordDate != null)
                return false;
        }
        else if (!RecordDate.equals(other.RecordDate))
            return false;
        if (archiveTime == null)
        {
            if (other.archiveTime != null)
                return false;
        }
        else if (!archiveTime.equals(other.archiveTime))
            return false;
        if (filePath == null)
        {
            if (other.filePath != null)
                return false;
        }
        else if (!filePath.equals(other.filePath))
            return false;
        if (messageType != other.messageType)
            return false;
        if (messageTypeTime == null)
        {
            if (other.messageTypeTime != null)
                return false;
        }
        else if (!messageTypeTime.equals(other.messageTypeTime))
            return false;
        if (status != other.status)
            return false;
        if (waGroupId == null)
        {
            if (other.waGroupId != null)
                return false;
        }
        else if (!waGroupId.equals(other.waGroupId))
            return false;
        if (waMessageId == null)
        {
            if (other.waMessageId != null)
                return false;
        }
        else if (!waMessageId.equals(other.waMessageId))
            return false;
        if (waMessageText == null)
        {
            if (other.waMessageText != null)
                return false;
        }
        else if (!waMessageText.equals(other.waMessageText))
            return false;
        if (waMessageTime == null)
        {
            if (other.waMessageTime != null)
                return false;
        }
        else if (!waMessageTime.equals(other.waMessageTime))
            return false;
        if (waMobileNo == null)
        {
            if (other.waMobileNo != null)
                return false;
        }
        else if (!waMobileNo.equals(other.waMobileNo))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaMessagesIncomingArchive [waMessageId=" + waMessageId + ", waMobileNo=" + waMobileNo + ", waGroupId=" + waGroupId + ", waMessageText=" + waMessageText + ", waMessageTime="
                + waMessageTime + ", RecordDate=" + RecordDate + ", status=" + status + ", filePath=" + filePath + ", messageTypeTime=" + messageTypeTime + ", messageType=" + messageType
                + ", archiveTime=" + archiveTime + "]";
    }

}
