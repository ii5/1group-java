package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.Constant;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.Utils;

@Entity
@Table(name = "invitation_log")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = InviteLog.FIND_ALL_INVITED_BY_ACCOUNT, query = "select toPhoneNumber FROM InviteLog m where toPhoneNumber IN(select phoneNumber from NativeContacts nc where nc.account.id = :accountId) group by toPhoneNumber order by sentTime desc") })
public class InviteLog implements Serializable
{

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String FIND_ALL_INVITED_BY_ACCOUNT = "findAllInvitedByAccount";

    public InviteLog()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
    private String id;

    @Column(name = "from_account_id")
    private String fromAccountId;

    @Column(name = "to_phone_number")
    private String toPhoneNumber;

    @Column(name = "sent_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentTime;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFromAccountId()
    {
        return fromAccountId;
    }

    public void setFromAccountId(String fromAccountId)
    {
        this.fromAccountId = fromAccountId;
    }

    public String getToPhoneNumber()
    {
        return toPhoneNumber;
    }

    public void setToPhoneNumber(String toPhoneNumber)
    {
        this.toPhoneNumber = toPhoneNumber;
    }

    public Date getSentTime()
    {
        return sentTime;
    }

    public void setSentTime(Date sentTime)
    {
        this.sentTime = sentTime;
    }

    @Override
    public String toString()
    {
        return "InviteLog [id=" + id + ", fromAccountId=" + fromAccountId + ", toPhoneNumber=" + toPhoneNumber + ", sentTime=" + sentTime + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fromAccountId == null) ? 0 : fromAccountId.hashCode());
        result = prime * result + ((toPhoneNumber == null) ? 0 : toPhoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InviteLog other = (InviteLog) obj;
        if (fromAccountId == null)
        {
            if (other.fromAccountId != null)
                return false;
        }
        else if (!fromAccountId.equals(other.fromAccountId))
            return false;
        if (toPhoneNumber == null)
        {
            if (other.toPhoneNumber != null)
                return false;
        }
        else if (!toPhoneNumber.equals(other.toPhoneNumber))
            return false;
        return true;
    }

}
