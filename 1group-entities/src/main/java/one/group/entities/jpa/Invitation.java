package one.group.entities.jpa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import one.group.core.enums.InvitationType;
import one.group.core.enums.status.ApprovalStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * <code>Invitation</code> represents handshaking requests between agents.
 * 
 * @author nyalfernandes
 * 
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
public class Invitation extends BaseEntity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The agent that requests the invitation.
     */
    @Column(nullable = false)
    private Account fromAccount;

    /**
     * The approval status of the invitation.
     */
    @Enumerated(EnumType.STRING)
    private ApprovalStatus status;

    /**
     * Denotes the type of Invitation. Currently Registration.
     */
    @Enumerated(EnumType.STRING)
    private InvitationType type;

    /**
     * The agent to whom the invitation was sent.
     */
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Account toAccount;

    protected Invitation()
    {
    }

    public Invitation(final Agent FromAccount, final Agent toAgent, final InvitationType type)
    {
        Validation.notNull(FromAccount, "The agent initiating the invitation cannot be null.");
        Validation.notNull(toAgent, "The target agent cannot be null.");
        Validation.notNull(type, "The invitation type cannot be null.");
        Validation.isFalse(FromAccount.equals(toAgent), "Okaaay! Weird request! Why invite yourself?");

        this.fromAccount = FromAccount;
        this.toAccount = toAgent;
        this.type = type;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Invitation other = (Invitation) obj;
        if (fromAccount == null)
        {
            if (other.fromAccount != null)
            {
                return false;
            }
        }
        else if (!fromAccount.equals(other.fromAccount))
        {
            return false;
        }
        if (toAccount == null)
        {
            if (other.toAccount != null)
            {
                return false;
            }
        }
        else if (!toAccount.equals(other.toAccount))
        {
            return false;
        }
        if (type != other.type)
        {
            return false;
        }
        return true;
    }

    public Account getFromAccount()
    {
        return fromAccount;
    }

    public ApprovalStatus getStatus()
    {
        return status;
    }

    public Account getToAccount()
    {
        return toAccount;
    }

    public InvitationType getType()
    {
        return type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((fromAccount == null) ? 0 : fromAccount.hashCode());
        result = prime * result + ((toAccount == null) ? 0 : toAccount.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    public void setFromAccount(final Agent fromAccount)
    {
        this.fromAccount = fromAccount;
    }

    public void setStatus(final ApprovalStatus status)
    {
        this.status = status;
    }

    public void setToAgent(final Account toAccount)
    {
        this.toAccount = toAccount;
    }

    public void setType(final InvitationType type)
    {
        this.type = type;
    }

}
