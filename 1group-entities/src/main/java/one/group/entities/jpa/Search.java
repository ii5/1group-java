package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@Table(name = "search")
@NamedQueries({ @NamedQuery(name = Search.QUERY_FIND_BY_LOCATIONS, query = "FROM Search r WHERE r.locationId IN :locationList"),
        @NamedQuery(name = Search.QUERY_FIND_BY_REQUIREMENT_NAME, query = "FROM Search s where  s.createdBy=:accountId"),
        @NamedQuery(name = Search.QUERY_FIND_BY_ACCOUNT_ID, query = "FROM Search r where r.createdBy = :accountId"),
        @NamedQuery(name = Search.QUERY_FIND_BY_ACCOUNT_IDS, query = "FROM Search r where r.createdBy IN :accountIdList"),
        @NamedQuery(name = Search.QUERY_FIND_REQUIREMENT_SEARCH_BY_ACCOUNT_ID, query = "FROM Search r where r.createdBy = :accountId"),
        @NamedQuery(name = Search.QUERY_FIND_EXPIRED_REQUIREMENT_SEARCHES, query = "FROM Search r where r.searchTime < :expiredTime"),
        @NamedQuery(name = Search.QUERY_FIND_LATEST_SEARCH_BY_ACCOUNT, query = "FROM Search r where r.createdBy = :accountId ORDER BY searchTime DESC ") })
public class Search extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_LOCATIONS = "findSearchByLocations";
    public static final String QUERY_FIND_BY_REQUIREMENT_NAME = "findSearchByRequirementName";
    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findSearchByAccountId";
    public static final String QUERY_FIND_BY_ACCOUNT_IDS = "findSearchByAccountIdList";
    public static final String QUERY_FIND_REQUIREMENT_SEARCH_BY_ACCOUNT_ID = "findRequirementSearchByAccountId";
    public static final String QUERY_FIND_EXPIRED_REQUIREMENT_SEARCHES = "findExpiredRequirementSearches";
    public static final String QUERY_FIND_LATEST_SEARCH_BY_ACCOUNT = "findLatestSearchByAccount";

    @Column(name = "location_id", nullable = true)
    private String locationId;

    @Column(name = "min_price")
    private Long minPrice;

    @Column(name = "max_price")
    private Long maxPrice;

    @Column(name = "min_area")
    private Integer minArea;

    @Column(name = "max_area")
    private Integer maxArea;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_type")
    private PropertyType propertyType;

    @Column(name = "rooms")
    private String rooms;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private PropertyTransactionType transactionType;

    @Column(name = "search_time")
    private Date searchTime;

    @Column(name = "city_id")
    private String cityId;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_sub_type")
    private PropertySubType propertySubType;

    @Enumerated(EnumType.STRING)
    @Column(name = "broadcast_type")
    private BroadcastType broadcastType;

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public Integer getMinArea()
    {
        return minArea;
    }

    public void setMinArea(Integer minArea)
    {
        this.minArea = minArea;
    }

    public Integer getMaxArea()
    {
        return maxArea;
    }

    public void setMaxArea(Integer maxArea)
    {
        this.maxArea = maxArea;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public String getRooms()
    {
        return rooms;
    }

    public void setRooms(String rooms)
    {
        this.rooms = rooms;
    }

    public PropertyTransactionType getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(PropertyTransactionType transactionType)
    {
        this.transactionType = transactionType;
    }

    public Date getSearchTime()
    {
        return searchTime;
    }

    public void setSearchTime(Date searchTime)
    {
        this.searchTime = searchTime;
    }

    public Long getMinPrice()
    {
        return minPrice;
    }

    public void setMinPrice(Long minPrice)
    {
        this.minPrice = minPrice;
    }

    public Long getMaxPrice()
    {
        return maxPrice;
    }

    public void setMaxPrice(Long maxPrice)
    {
        this.maxPrice = maxPrice;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public BroadcastType getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(BroadcastType broadcastType)
    {
        this.broadcastType = broadcastType;
    }

}
