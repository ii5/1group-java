package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.PhoneNumberSource;
import one.group.core.enums.PhoneNumberType;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * Phone numbers associated with {@link Account} or its concrete types and maybe
 * {@link Client}.
 * 
 * 
 * @author nyalfernandes
 * 
 *         TODO : Associate with Contacts locale. TODO : Should primary be a
 *         flag here instead of having two references in the {@link Account}
 *         table?
 * 
 * 
 * 
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = PhoneNumber.QUERY_FIND_BY_NUMBER, query = "FROM PhoneNumber p where p.number = :phoneNumber"),
        @NamedQuery(name = PhoneNumber.QUERY_FIND_COUNT_BY_NUMBER, query = "SELECT count(p) FROM PhoneNumber p WHERE p.number =:phoneNumber") })
@Table(name = "phone_number")
public class PhoneNumber extends BaseEntity
{

	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_BY_NUMBER = "findEntityByNumber";

    public static final String QUERY_FIND_COUNT_BY_NUMBER = "findPhoneNumberCountByNumber";

    @Enumerated(EnumType.STRING)
    private PhoneNumberType type;

    @Column(name = "number", nullable = false, unique = true)
    private String number;

    @Column(name = "source", nullable = false)
    @Enumerated(EnumType.STRING)
    private PhoneNumberSource source;

    public PhoneNumberType getType()
    {
        return type;
    }

    public void setType(PhoneNumberType type)
    {
        this.type = type;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        Validation.validateMobileNumber(number);
        this.number = number;
    }

    public PhoneNumberSource getSource()
    {
        return source;
    }

    public void setSource(PhoneNumberSource source)
    {
        this.source = source;
    }

}