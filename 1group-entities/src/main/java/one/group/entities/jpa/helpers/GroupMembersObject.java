package one.group.entities.jpa.helpers;

public class GroupMembersObject
{
    private String groupId;

    private String participantId;

    private String mobileNumber;
    public static final String FETCH_GROUP_ID_BY_PARTICIPANTS = " SELECT NEW one.group.entities.jpa.helpers.GroupMembersObject(gm1.groupId) "
            + " FROM GroupMembers gm1 left join GroupMembers gm2 WHERE gm1.groupId = gm2.groupId AND gm1.participantId = :participantOne AND gm2.participantId = :participantTwo group by gm1.groupId";

    public static final String FETCH_GROUP_ID_BY_PARTICIPANTS_NEW = " SELECT NEW one.group.entities.jpa.helpers.GroupMembersObject(gm1.groupId) "
            + " FROM GroupMembers gm1 WHERE gm1.source =:source AND gm1.participantId IN (:participantOne, :participantTwo) GROUP BY gm1.groupId HAVING COUNT(gm1.groupId) > 1";

    public static final String FETCH_GROUP_ID_BY_PHONE_NUMBERS_AND_EVER_SYNC = " SELECT NEW one.group.entities.jpa.helpers.GroupMembersObject(gm1.groupId,gm1.participantId,gm1.mobileNumber) "
            + " FROM GroupMembers gm1 WHERE gm1.mobileNumber IN (:mobileNumber) AND everSync IN(:everSync)";

    // SELECT group_id FROM group_members WHERE (participant_id
    // ='8974677281637785') OR (participant_id='1000243652245275') GROUP by
    // group_id having count(group_id)> 1
    public GroupMembersObject()
    {

    }

    public GroupMembersObject(String groupId)
    {
        this.groupId = groupId;
    }

    public GroupMembersObject(String groupId, String participantId, String mobileNumber)
    {
        this.groupId = groupId;
        this.participantId = participantId;
        this.mobileNumber = mobileNumber;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

}
