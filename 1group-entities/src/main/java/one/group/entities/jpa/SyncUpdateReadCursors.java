package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import one.group.utils.validation.Validation;

@Entity
@Table(name = "sync_update_read_cursors")
@NamedQueries
(
        {
            @NamedQuery(name=SyncUpdateReadCursors.QUERY_FIND_BY_CLIENT_ID, query="FROM SyncUpdateReadCursors s where s.clientId = :clientId"),
            @NamedQuery(name=SyncUpdateReadCursors.QUERY_FIND_BY_CLIENT_IDS, query="FROM SyncUpdateReadCursors s where s.clientId IN :clientIdList")
        }
)

@NamedStoredProcedureQueries
(
        {
            @NamedStoredProcedureQuery
            (
                    name=SyncUpdateReadCursors.STORED_PROCEDURE_CREATE_CLIENT, 
                    procedureName="create_client", 
                    parameters=
                    {
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="clientId", type=String.class),
                            @StoredProcedureParameter(mode = ParameterMode.OUT, name="retVal", type=Integer.class)
                    }
            ),
            @NamedStoredProcedureQuery
            (
                    name=SyncUpdateReadCursors.STORED_PROCEDURE_APPEND_UPDATE, 
                    procedureName="append_update", 
                    parameters=
                    {
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="accountId", type=String.class),
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="syncUpdate", type=String.class),
                            @StoredProcedureParameter(mode = ParameterMode.OUT, name="totalUpdates", type=Integer.class)
                    }
            ),
            @NamedStoredProcedureQuery
            (
                    name=SyncUpdateReadCursors.STORED_PROCEDURE_ADVANCE_READ_CURSOR, 
                    procedureName="advance_read_cursor", 
                    parameters=
                    {
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="clientId", type=String.class),
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="accountId", type=String.class),
                            @StoredProcedureParameter(mode = ParameterMode.IN, name="requestedCursorIndex", type=Integer.class),
                            @StoredProcedureParameter(mode = ParameterMode.OUT, name="maxAckedUpdateOfClient", type=Integer.class)
                    }
            )
        }
)
public class SyncUpdateReadCursors implements Serializable
{
    
    private static final long serialVersionUID = 1L;
    public static final String QUERY_FIND_BY_CLIENT_ID = "findReadCursorByClientId";
    public static final String QUERY_FIND_BY_CLIENT_IDS = "findReadCursorByClientIds";
    public static final String STORED_PROCEDURE_CREATE_CLIENT = "storedProcedureCreateClient";
    public static final String STORED_PROCEDURE_APPEND_UPDATE = "storedProcedureAppendUpdate";
    public static final String STORED_PROCEDURE_ADVANCE_READ_CURSOR = "storedProcedureAdvanceReadCursor";
    
    @Id
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "max_acked_update", nullable = false)
    private Integer maxAckedUpdate;

    protected SyncUpdateReadCursors() {}
    
    public SyncUpdateReadCursors(String clientId, int maxAckUpdate)
    {
        Validation.notNull(clientId, "Client Id passed should not be null.");

        this.clientId = clientId;
        this.maxAckedUpdate = maxAckUpdate;
    }

    public String getClientId()
    {
        return clientId;
    }

    public int getMaxAckedUpdate()
    {
        return maxAckedUpdate;
    }
    
    public void setMaxAckedUpdate(int maxAckedUpdate)
    {
        this.maxAckedUpdate = maxAckedUpdate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SyncUpdateReadCursors other = (SyncUpdateReadCursors) obj;
        if (clientId == null)
        {
            if (other.clientId != null)
                return false;
        }
        else if (!clientId.equals(other.clientId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "SyncLogReadCursor [clientId=" + clientId + ", maxAckedUpdate=" + maxAckedUpdate + "]";
    }

}
