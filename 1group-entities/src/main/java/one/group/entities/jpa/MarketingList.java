package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * 
 * @author sanilshet
 *
 */
@Entity
@Table(name = "marketing_list")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = MarketingList.FIND_BY_PHONE_NUMBER, query = "from MarketingList where phoneNumber =:phoneNumber"),
        @NamedQuery(name = MarketingList.FIND_ALL_BY_NATIVE_CONTACTS_BY_ACCOUNT, query = "FROM MarketingList m where phoneNumber IN(select phoneNumber from NativeContacts nc where nc.account.id = :accountId)"),
        @NamedQuery(name = MarketingList.FIND_ALL_WHERE_PHONE_NUMBERS_IN, query = "FROM MarketingList m where phoneNumber IN(:phoneNumbers)"),
        @NamedQuery(name = MarketingList.FIND_ALL_ORDER_BY_DESC_WHATSAPP_GROUP_COUNT_WHERE_PHONE_NUMBERS_IN, query = "FROM MarketingList m where phoneNumber IN(:phoneNumbers) ORDER BY m.wsGroupCount DESC") })
public class MarketingList implements Serializable
{

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String FIND_BY_PHONE_NUMBER = "findByPhoneNumber";
    public static final String FIND_ALL_BY_NATIVE_CONTACTS_BY_ACCOUNT = "findAllByNativeContactsByAccount";
    public static final String FIND_ALL_WHERE_PHONE_NUMBERS_IN = "findAllWherePhoneNumbersIn";
    public static final String FIND_ALL_ORDER_BY_DESC_WHATSAPP_GROUP_COUNT_WHERE_PHONE_NUMBERS_IN = "findAllOrderByWhatsAppGroupCountWherePhoneNumbersIn";

    @Id
    @Column(name = "phone_number", nullable = false, unique = true)
    private String phoneNumber;

    @Column(name = "data_source", nullable = false)
    private String dataSource;

    @Column(name = "wa_group_count")
    private int wsGroupCount;

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(String dataSource)
    {
        this.dataSource = dataSource;
    }

    public int getWsGroupCount()
    {
        return wsGroupCount;
    }

    public void setWsGroupCount(int wsGroupCount)
    {
        this.wsGroupCount = wsGroupCount;
    }

    @Override
    public String toString()
    {
        return "MarketingList [phoneNumber=" + phoneNumber + ", dataSource=" + dataSource + ", wsGroupCount=" + wsGroupCount + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + wsGroupCount;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MarketingList other = (MarketingList) obj;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (wsGroupCount != other.wsGroupCount)
            return false;
        return true;
    }

}
