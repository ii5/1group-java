package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Otp.QUERY_FIND_BY_OTP_AND_NUMBER, query = "FROM Otp p where p.otp = :otp AND p.phoneNumber.number = :phoneNumber"),
        @NamedQuery(name = Otp.QUERY_FIND_ACTIVE_CLIENT_BY_OTP, query = "FROM Otp p WHERE p.otp =:otp"),
        @NamedQuery(name = Otp.QUERY_FIND_ID_BY_NUMBER_CLIENT_OTP, query = "FROM Otp p WHERE p.otp =:otp AND p.phoneNumber.number = :phoneNumber"),
        @NamedQuery(name = Otp.QUERY_DELETE_ID_BY_NUMBER_CLIENT_OTP, query = "DELETE FROM Otp p WHERE p.otp =:otp AND p.phoneNumber.number = :phoneNumber") })
@Table(name = "otp")
public class Otp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_OTP_AND_NUMBER = "findOTPByOtpAndNumber";
    public static final String QUERY_FIND_ACTIVE_CLIENT_BY_OTP = "findOTPForActiveClientByOtp";
    public static final String QUERY_FIND_ID_BY_NUMBER_CLIENT_OTP = "findIdByOtpNumberOtp";
    public static final String QUERY_DELETE_ID_BY_NUMBER_CLIENT_OTP = "deleteByNumberAndOtp";

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PhoneNumber phoneNumber;

    @Column(nullable = false)
    private String otp;

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Otp other = (Otp) obj;
        if (otp == null)
        {
            if (other.otp != null)
            {
                return false;
            }
        }
        else if (!otp.equals(other.otp))
        {
            return false;
        }
        return true;
    }

    // Consider scenario: user login = phoneNumber, one user can have multiple
    // devices.
    // private String deviceId;

    public String getOtp()
    {
        return otp;
    }

    public PhoneNumber getPhoneNumber()
    {
        return phoneNumber;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((otp == null) ? 0 : otp.hashCode());
        return result;
    }

    public void setOtp(final String otp)
    {
        this.otp = otp;
    }

    public void setPhoneNumber(final PhoneNumber phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

}
