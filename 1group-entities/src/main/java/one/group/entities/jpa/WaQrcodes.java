/**
 * 
 */
package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.Constant;
import one.group.utils.Utils;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_qrcodes")
public class WaQrcodes implements Serializable
{

    private static final long serialVersionUID = 1L;

    public WaQrcodes()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
    @Column(name = "current_server_id")
    private String id;

    @Column(name = "current_client_id")
    private String currentClientId;

    @Column(name = "current_client_refresh")
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentClientRefresh;

    @Column(name = "current_client_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentClientTime;

    @Column(name = "current_server_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date currentServerTime;

    @Column(name = "latest_qr_code")
    private String latestQrCode;

    @Column(name = "current_client_status")
    private String currentClientStatus;

    @Column(name = "current_client_mobile")
    private PhoneNumber currentClientMobile;

    @Column(name = "wa_groups_approved_status")
    private int waGroupsApprovedStatus;

    @Column(name = "grouplist", length = 4000)
    private String groupList;

    @Column(name = "current_crm_mobile")
    private String currentCrmMobile;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getCurrentClientId()
    {
        return currentClientId;
    }

    public void setCurrent_client_id(String currentClientId)
    {
        this.currentClientId = currentClientId;
    }

    public String getLatestQrCode()
    {
        return latestQrCode;
    }

    public void setLatestQrCode(String latestQrCode)
    {
        this.latestQrCode = latestQrCode;
    }

    public String getCurrentClientStatus()
    {
        return currentClientStatus;
    }

    public void setCurrentClientStatus(String currentClientStatus)
    {
        this.currentClientStatus = currentClientStatus;
    }

    public int getWaGroupsApprovedStatus()
    {
        return waGroupsApprovedStatus;
    }

    public void setWaGroupsApprovedStatus(int waGroupsApprovedStatus)
    {
        this.waGroupsApprovedStatus = waGroupsApprovedStatus;
    }

    public String getGroupList()
    {
        return groupList;
    }

    public void setGroupList(String groupList)
    {
        this.groupList = groupList;
    }

    public String getCurrentCrmMobile()
    {
        return currentCrmMobile;
    }

    public void setCurrentCrmMobile(String currentCrmMobile)
    {
        this.currentCrmMobile = currentCrmMobile;
    }

    public Date getCurrentClientRefresh()
    {
        return currentClientRefresh;
    }

    public void setCurrentClientRefresh(Date currentClientRefresh)
    {
        this.currentClientRefresh = currentClientRefresh;
    }

    public Date getCurrentClientTime()
    {
        return currentClientTime;
    }

    public void setCurrentClientTime(Date currentClientTime)
    {
        this.currentClientTime = currentClientTime;
    }

    public Date getCurrentServerTime()
    {
        return currentServerTime;
    }

    public void setCurrentServerTime(Date currentServerTime)
    {
        this.currentServerTime = currentServerTime;
    }

    public PhoneNumber getCurrentClientMobile()
    {
        return currentClientMobile;
    }

    public void setCurrentClientMobile(PhoneNumber currentClientMobile)
    {
        this.currentClientMobile = currentClientMobile;
    }

    public void setCurrentClientId(String currentClientId)
    {
        this.currentClientId = currentClientId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((currentClientId == null) ? 0 : currentClientId.hashCode());
        result = prime * result + ((currentClientMobile == null) ? 0 : currentClientMobile.hashCode());
        result = prime * result + ((currentClientRefresh == null) ? 0 : currentClientRefresh.hashCode());
        result = prime * result + ((currentClientStatus == null) ? 0 : currentClientStatus.hashCode());
        result = prime * result + ((currentClientTime == null) ? 0 : currentClientTime.hashCode());
        result = prime * result + ((currentCrmMobile == null) ? 0 : currentCrmMobile.hashCode());
        result = prime * result + ((currentServerTime == null) ? 0 : currentServerTime.hashCode());
        result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((latestQrCode == null) ? 0 : latestQrCode.hashCode());
        result = prime * result + waGroupsApprovedStatus;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaQrcodes other = (WaQrcodes) obj;
        if (currentClientId == null)
        {
            if (other.currentClientId != null)
                return false;
        }
        else if (!currentClientId.equals(other.currentClientId))
            return false;
        if (currentClientMobile == null)
        {
            if (other.currentClientMobile != null)
                return false;
        }
        else if (!currentClientMobile.equals(other.currentClientMobile))
            return false;
        if (currentClientRefresh == null)
        {
            if (other.currentClientRefresh != null)
                return false;
        }
        else if (!currentClientRefresh.equals(other.currentClientRefresh))
            return false;
        if (currentClientStatus == null)
        {
            if (other.currentClientStatus != null)
                return false;
        }
        else if (!currentClientStatus.equals(other.currentClientStatus))
            return false;
        if (currentClientTime == null)
        {
            if (other.currentClientTime != null)
                return false;
        }
        else if (!currentClientTime.equals(other.currentClientTime))
            return false;
        if (currentCrmMobile == null)
        {
            if (other.currentCrmMobile != null)
                return false;
        }
        else if (!currentCrmMobile.equals(other.currentCrmMobile))
            return false;
        if (currentServerTime == null)
        {
            if (other.currentServerTime != null)
                return false;
        }
        else if (!currentServerTime.equals(other.currentServerTime))
            return false;
        if (groupList == null)
        {
            if (other.groupList != null)
                return false;
        }
        else if (!groupList.equals(other.groupList))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (latestQrCode == null)
        {
            if (other.latestQrCode != null)
                return false;
        }
        else if (!latestQrCode.equals(other.latestQrCode))
            return false;
        if (waGroupsApprovedStatus != other.waGroupsApprovedStatus)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaQrcodes [id=" + id + ", currentClientId=" + currentClientId + ", currentClientRefresh=" + currentClientRefresh + ", currentClientTime=" + currentClientTime + ", currentServerTime="
                + currentServerTime + ", latestQrCode=" + latestQrCode + ", currentClientStatus=" + currentClientStatus + ", currentClientMobile=" + currentClientMobile + ", waGroupsApprovedStatus="
                + waGroupsApprovedStatus + ", groupList=" + groupList + ", currentCrmMobile=" + currentCrmMobile + "]";
    }

}
