package one.group.entities.jpa;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import one.group.core.enums.AccountType;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * Account is the parent class for {@link Agent} and {@link Developer} concrete
 * implementations. The <code>Contact</code> contains basic information required
 * to build a contact type in the Move.In context. Contact by itself can be an
 * Entity too which would be a "Raw Entity".
 * 
 * @author nyalfernandes
 * 
 *         TODO : primaryPhonenumber needs to be a part of contact and not the
 *         concrete implementations.
 * 
 *         TODO : Add appropriate constructors and delete destructive setters.
 * 
 */

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = Account.QUERY_FIND_BY_NUMBER, query = "FROM Account a where a.primaryPhoneNumber.number = :number"),
        @NamedQuery(name = Account.QUERY_FIND_BY_NUMBERS, query = "FROM Account a where a.primaryPhoneNumber.number in (:mobileNumbers)"),
        @NamedQuery(name = Account.QUERY_FIND_BY_SHORTREFERENCE, query = "FROM Account a WHERE a.shortReference = :shortreference"),
        @NamedQuery(name = Account.QUERY_TOTAL_COUNT, query = "select count(*) FROM Account where status in (:accountStatus)"),
        @NamedQuery(name = Account.QUERY_ALL_ACCOUNT_BY_STATUS, query = "FROM Account where status=:status"),
        @NamedQuery(name = Account.QUERY_FIND_STATUS_OF_CONTACTS_BY_ACCOUNT, query = "FROM Account a where a.primaryPhoneNumber.id IN (SELECT id from PhoneNumber pn where pn.number IN(select phoneNumber from NativeContacts nc where nc.account.id=:accountId)) "),
        @NamedQuery(name = Account.QUERY_FIND_ACCOUNTS_OF_CONTACTS_BY_ACCOUNT, query = "FROM Account a where a.primaryPhoneNumber.id IN (SELECT id from PhoneNumber pn where pn.number IN(select phoneNumber from NativeContacts nc where nc.account.id=:accountId)) and a.status='ACTIVE'"),
        @NamedQuery(name = Account.QUERY_FIND_PHONENUMBER_OF_CONTACTS_BY_ACCOUNT, query = "select a.primaryPhoneNumber.number FROM Account a where a.primaryPhoneNumber.id IN (SELECT id from PhoneNumber pn where pn.number IN(select phoneNumber from NativeContacts nc where nc.account.id=:accountId)) and a.status='ACTIVE'"),
        // @NamedQuery(name =
        // Account.QUERY_FIND_ACCOUNTS_OF_CONTACTS_BY_ACCOUNT, query =
        // "select a FROM Account a,PhoneNumber pn,NativeContacts nc where a.primaryPhoneNumber.id =pn.id and pn.number =nc.phoneNumber and nc.account.id=:accountId)) and a.status='ACTIVE'"),
        @NamedQuery(name = Account.QUERY_ALL_ACCOUNT_BY_IDS, query = "FROM Account a where a.id in (:accountIds)"),
        @NamedQuery(name = Account.QUERY_FIND_USER_BY_MOBILE_NUMBER_OR_WANUMBER, query = "FROM Account a WHERE a.primaryPhoneNumber.number = :mobileNumber"),
        @NamedQuery(name = Account.QUERY_FIND_BY_ACCOUNT_ID, query = "FROM Account a WHERE a.id = :accountId "), })
// @Table(name = "account")
@DiscriminatorValue(value = "ACCOUNT")
// @MappedSuperclass
public class Account extends User
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_NUMBER = "findByNumber";

    public static final String QUERY_FIND_BY_NUMBERS = "findByNumbers";

    public static final String QUERY_FIND_BY_SHORTREFERENCE = "findByShortReference";

    public static final String QUERY_TOTAL_COUNT = "findTotalAccounts";

    public static final String QUERY_ALL_ACCOUNT_BY_STATUS = "findAllAccountByStatus";

    public static final String QUERY_FIND_STATUS_OF_CONTACTS_BY_ACCOUNT = "findStatusOfNativeContactsByAccount";
    public static final String QUERY_ALL_ACCOUNT_BY_IDS = "findAllAccountByIds";
    public static final String QUERY_FIND_USER_BY_MOBILE_NUMBER_OR_WANUMBER = "findUserByMobileNumberOrWaNumber";

    public static final String QUERY_FIND_ACCOUNTS_OF_CONTACTS_BY_ACCOUNT = "findAccountsOfNativeContactsByAccount";

    public static final String QUERY_FIND_PHONENUMBER_OF_CONTACTS_BY_ACCOUNT = "findPhoneNumberOfNativeContactsByAccount";

    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findByStatusAndAccountId";
    
    public static final String QUERY_FETCH_SELECTIVE_DATA_BY_IDS = "SELECT NEW one.group.entities.jpa.Account(a.id, a.fullName, a.companyName, a.lastLogin, a.shortReference, a.cityId, a.type, a.syncStatus, a.primaryPhoneNumberId, a.fullPhotoId, a.thumbnailPhotoId, a.status, a.registrationTime, a.initialContactsSyncComplete) FROM Account a WHERE a.id IN :accountIdList";

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "primaryphonenumber_id")
    private PhoneNumber primaryPhoneNumber;
    
    @Column(name = "primaryphonenumber_id", insertable = false, updatable = false)
    private String primaryPhoneNumberId;

    @Column(name = "company_name", nullable = true)
    private String companyName;

    @Column(name = "address", length = 10000)
    private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "")
    private Set<PhoneNumber> phoneNumbers = new HashSet<PhoneNumber>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "full_photo_id")
    private Media fullPhoto;
    
    @Column(name = "full_photo_id", insertable = false, updatable = false)
    private String fullPhotoId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "thumbnail_photo_id")
    private Media thumbnailPhoto;
    
    @Column(name = "thumbnail_photo_id", insertable = false, updatable = false)
    private String thumbnailPhotoId;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private Set<AccountRelations> accountRelations = new HashSet<AccountRelations>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private Set<NativeContacts> nativeContacts = new HashSet<NativeContacts>();

    @Column(name = "initial_contacts_sync_complete", nullable = false)
    private boolean initialContactsSyncComplete = false;

    @Column(name = "registration_time", nullable = true)
    private Date registrationTime;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private Set<Broadcast> broadcasts = new HashSet<Broadcast>();

    /**
     * Properties listed by the agent.
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "account")
    private Set<PropertyListing> properties = new HashSet<PropertyListing>();
    
    public Account() {}
    
    public Account(String id, String fullName, String companyName, Date lastLogin, String shortReference, String cityId, AccountType type, SyncStatus syncStatus, String primaryPhoneNumberId, String fullPhotoId, String thumbnailPhotoId, Status status, Date registrationTime, boolean initialContactSyncComplete)
    {
    	setId(id);
    	setFullName(fullName);
    	setCompanyName(companyName);
    	setLastLogin(lastLogin);
    	setShortReference(shortReference);
    	setCityId(cityId);
    	setType(type);
    	setSyncStatus(syncStatus);
    	setPrimaryPhoneNumberId(primaryPhoneNumberId);
    	setFullPhotoId(fullPhotoId);
    	setThumbnailPhotoId(thumbnailPhotoId);
    	setStatus(status);
    	setRegistrationTime(registrationTime);
    	setInitialContactsSyncComplete(initialContactSyncComplete);
    }

    public String getFullPhotoId() 
    {
		return fullPhotoId;
	}
    
    public void setFullPhotoId(String fullPhotoId) 
    {
		this.fullPhotoId = fullPhotoId;
	}
    
    public String getThumbnailPhotoId() 
    {
		return thumbnailPhotoId;
	}
    
    public void setThumbnailPhotoId(String thumbnailPhotoId) 
    {
		this.thumbnailPhotoId = thumbnailPhotoId;
	}
    
    public String getPrimaryPhoneNumberId() 
    {
		return primaryPhoneNumberId;
	}
    
    public void setPrimaryPhoneNumberId(String primaryPhoneNumberId) 
    {
		this.primaryPhoneNumberId = primaryPhoneNumberId;
	}
    
    public Set<AccountRelations> getAccountRelations()
    {
        return accountRelations;
    }

    public void setAccountRelations(Set<AccountRelations> accountRelations)
    {
        this.accountRelations = accountRelations;
    }

    public Set<NativeContacts> getNativeContacts()
    {
        return nativeContacts;
    }

    public void setNativeContacts(Set<NativeContacts> nativeContacts)
    {
        this.nativeContacts = nativeContacts;
    }

    public void addPhoneNumber(PhoneNumber phone)
    {
        Validation.validateMobileNumber(phone.getNumber());
        this.phoneNumbers.add(phone);
    }

    public PhoneNumber getPrimaryPhoneNumber()
    {
        return primaryPhoneNumber;
    }

    public void setPrimaryPhoneNumber(PhoneNumber primaryPhoneNumber)
    {
        this.primaryPhoneNumber = primaryPhoneNumber;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public Set<PhoneNumber> getPhoneNumbers()
    {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers)
    {
        this.phoneNumbers = phoneNumbers;
    }

    public Media getFullPhoto()
    {
        return fullPhoto;
    }

    public void setFullPhoto(Media fullContactPicture)
    {
        this.fullPhoto = fullContactPicture;
    }

    public Media getThumbnailPhoto()
    {
        return thumbnailPhoto;
    }

    public void setThumbnailPhoto(Media thumbnailContactPicture)
    {
        this.thumbnailPhoto = thumbnailContactPicture;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public Set<PropertyListing> getProperties()
    {
        return properties;
    }

    public void setProperties(Set<PropertyListing> properties)
    {
        this.properties = properties;
    }

    public void addProperty(PropertyListing property)
    {
        this.properties.add(property);
    }

    public boolean isInitialContactsSyncComplete()
    {
        return initialContactsSyncComplete;
    }

    public void setInitialContactsSyncComplete(boolean initialContactsSyncComplete)
    {
        this.initialContactsSyncComplete = initialContactsSyncComplete;
    }

    public Date getRegistrationTime()
    {
        return registrationTime;
    }

    public void setRegistrationTime(Date registrationTime)
    {
        this.registrationTime = registrationTime;
    }

    public Set<Broadcast> getBroadcasts()
    {
        return broadcasts;
    }

    public void setBroadcasts(Set<Broadcast> broadcasts)
    {
        this.broadcasts = broadcasts;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((primaryPhoneNumber == null) ? 0 : primaryPhoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (this == obj)
        {
            return true;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Account other = (Account) obj;
        if (primaryPhoneNumber == null)
        {
            if (other.primaryPhoneNumber != null)
            {
                return false;
            }
        }
        else if (!primaryPhoneNumber.equals(other.primaryPhoneNumber))
        {
            return false;
        }
        return true;
    }
}
