package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import one.group.core.enums.MediaDimensionType;
import one.group.core.enums.MediaType;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * The Media linked to a Property. Media can be added by an agent while adding /
 * editing a property
 * 
 * @author miteshchavda
 * 
 *         TODO : Add more description
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Media.QUERY_GET_MEDIA_BY_URL, query = "FROM Media m WHERE m.url in :urlList") })
@Table(name = "media")
public class Media extends BaseEntity
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_MEDIA_BY_URL = "findMediaByUrl";

    /**
     * Name of media which identify media
     */
    // @Column
    @Transient
    private String name;

    /**
     * Type of a media
     */
    @Enumerated(EnumType.STRING)
    private MediaType type;

    /**
     * 
     */
    @Enumerated(EnumType.STRING)
    private MediaDimensionType dimension;

    /**
     * Absolute url of a media
     */
    @Column
    private String url;

    @Column
    private Integer width;

    @Column
    private Integer height;

    @Column
    private Integer size;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public MediaType getType()
    {
        return type;
    }

    public void setType(MediaType type)
    {
        this.type = type;
    }

    public MediaDimensionType getDimension()
    {
        return dimension;
    }

    public void setDimension(MediaDimensionType dimension)
    {
        this.dimension = dimension;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Integer getWidth()
    {
        return width;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public Integer getHeight()
    {
        return height;
    }

    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public Integer getSize()
    {
        return size;
    }

    public void setSize(Integer size)
    {
        this.size = size;
    }

    @Override
    public String toString()
    {
        return "Media [name=" + name + ", type=" + type + ", dimension=" + dimension + ", url=" + url + ", width=" + width + ", height=" + height + ", size=" + size + "]";
    }

}