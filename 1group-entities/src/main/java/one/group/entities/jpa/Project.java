package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import one.group.entities.jpa.listeners.EntryAuditEntityListener;


@Entity
@EntityListeners(EntryAuditEntityListener.class)
public class Project
{

    @Id
    private String id;
    
    @Column(name="name")
    private String name;
    
    @Column(name="is_user_suggested")
    private Boolean isUserSuggested;
    
    
    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the isUserSuggested
     */
    public Boolean getIsUserSuggested()
    {
        return isUserSuggested;
    }

    /**
     * @param isUserSuggested the isUserSuggested to set
     */
    public void setIsUserSuggested(Boolean isUserSuggested)
    {
        this.isUserSuggested = isUserSuggested;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "Project [id=" + id + ", name=" + name + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((isUserSuggested == null) ? 0 : isUserSuggested.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Project other = (Project) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (isUserSuggested == null)
        {
            if (other.isUserSuggested != null)
                return false;
        }
        else if (!isUserSuggested.equals(other.isUserSuggested))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }

}
