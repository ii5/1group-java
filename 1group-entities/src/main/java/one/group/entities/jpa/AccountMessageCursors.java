package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "acount_message_cursors")
@EntityListeners(EntryAuditEntityListener.class)
public class AccountMessageCursors extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "ID")
    private Account account;

    @Column(name = "receive_index")
    private int receiveIndex;

    @Column(name = "read_index")
    private int readIndex;

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public int getReceiveIndex()
    {
        return receiveIndex;
    }

    public void setReceiveIndex(int receiveIndex)
    {
        this.receiveIndex = receiveIndex;
    }

    public int getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(int readIndex)
    {
        this.readIndex = readIndex;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        result = prime * result + readIndex;
        result = prime * result + receiveIndex;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccountMessageCursors other = (AccountMessageCursors) obj;
        if (account == null)
        {
            if (other.account != null)
                return false;
        }
        else if (!account.equals(other.account))
            return false;
        if (readIndex != other.readIndex)
            return false;
        if (receiveIndex != other.receiveIndex)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "AccountMessageCursors [account=" + account + ", receiveIndex=" + receiveIndex + ", readIndex=" + readIndex + "]";
    }

}
