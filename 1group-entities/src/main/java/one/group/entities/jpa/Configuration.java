package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;


@Entity
@Table(name = "configuration")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Configuration.QUERY_GET_KEY_BY_VALUE, query = "FROM Configuration c WHERE c.key =:key") })
public class Configuration extends BaseEntity
{

    public static final String QUERY_GET_KEY_BY_VALUE = "findKeyByValue";

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    public String getKey()
    {
        return key;
    }

    public String getValue()
    {
        return value;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "Configuration [key=" + key + ", value=" + value + "]";
    }
}
