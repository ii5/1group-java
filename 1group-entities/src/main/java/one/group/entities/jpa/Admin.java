package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.AdminStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "admin")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries
(
    {
        @NamedQuery(name=Admin.QUERY_FIND_ADMIN_BY_ROLE_LIST_AND_ADMIN_STATUS_LIST, query="SELECT a FROM Admin a, AuthAssignment aus WHERE aus.authItem.name IN :roleList AND aus.user = a.id AND a.status IN :adminStatusList"),
        @NamedQuery(name=Admin.QUERY_FIND_ADMIN_BY_USERNAME_AND_ADMIN_STATUS_LIST, query="FROM Admin a where a.userName = :username AND a.status IN :adminStatusList"),
    }
)

public class Admin extends BaseEntity
{

    private static final long serialVersionUID = 1L;
    
    public static final String QUERY_FIND_ADMIN_BY_ROLE_LIST_AND_ADMIN_STATUS_LIST = "findAdminUsersByRoleList";
    public static final String QUERY_FIND_ADMIN_BY_USERNAME_AND_ADMIN_STATUS_LIST = "findAdminByUsernameAndStatus";

    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "roles_id")
    private String rolesId;

    @Column(name = "email")
    private String email;

    @Column(name = "last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;

    @Column(name = "auth_key")
    private String authKey;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "password_reset_token")
    private String passwordResetToken;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AdminStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName = "ID")
    private City city;

    @Column(name = "manager_id")
    private String managerId;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getRolesId()
    {
        return rolesId;
    }

    public void setRolesId(String rolesId)
    {
        this.rolesId = rolesId;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getAuthKey()
    {
        return authKey;
    }

    public void setAuthKey(String authKey)
    {
        this.authKey = authKey;
    }

    public String getPasswordHash()
    {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash)
    {
        this.passwordHash = passwordHash;
    }

    public String getPasswordResetToken()
    {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken)
    {
        this.passwordResetToken = passwordResetToken;
    }

    public String getManagerId()
    {
        return managerId;
    }

    public void setManagerId(String managerId)
    {
        this.managerId = managerId;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public AdminStatus getStatus()
    {
        return status;
    }

    public void setStatus(AdminStatus status)
    {
        this.status = status;
    }

    public Date getLastLogin()
    {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((authKey == null) ? 0 : authKey.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((lastLogin == null) ? 0 : lastLogin.hashCode());
        result = prime * result + ((managerId == null) ? 0 : managerId.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((passwordHash == null) ? 0 : passwordHash.hashCode());
        result = prime * result + ((passwordResetToken == null) ? 0 : passwordResetToken.hashCode());
        result = prime * result + ((rolesId == null) ? 0 : rolesId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Admin other = (Admin) obj;
        if (authKey == null)
        {
            if (other.authKey != null)
                return false;
        }
        else if (!authKey.equals(other.authKey))
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (email == null)
        {
            if (other.email != null)
                return false;
        }
        else if (!email.equals(other.email))
            return false;
        if (lastLogin == null)
        {
            if (other.lastLogin != null)
                return false;
        }
        else if (!lastLogin.equals(other.lastLogin))
            return false;
        if (managerId == null)
        {
            if (other.managerId != null)
                return false;
        }
        else if (!managerId.equals(other.managerId))
            return false;
        if (password == null)
        {
            if (other.password != null)
                return false;
        }
        else if (!password.equals(other.password))
            return false;
        if (passwordHash == null)
        {
            if (other.passwordHash != null)
                return false;
        }
        else if (!passwordHash.equals(other.passwordHash))
            return false;
        if (passwordResetToken == null)
        {
            if (other.passwordResetToken != null)
                return false;
        }
        else if (!passwordResetToken.equals(other.passwordResetToken))
            return false;
        if (rolesId == null)
        {
            if (other.rolesId != null)
                return false;
        }
        else if (!rolesId.equals(other.rolesId))
            return false;
        if (status != other.status)
            return false;
        if (userName == null)
        {
            if (other.userName != null)
                return false;
        }
        else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Admin [userName=" + userName + ", password=" + password + ", rolesId=" + rolesId + ", email=" + email + ", lastLogin=" + lastLogin + ", authKey=" + authKey + ", passwordHash="
                + passwordHash + ", passwordResetToken=" + passwordResetToken + ", status=" + status + ", city=" + city + ", managerId=" + managerId + "]";
    }

}
