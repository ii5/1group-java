package one.group.entities.jpa.helpers;

import java.util.Date;

public class ClientObject
{
    private String accountId;

    private Date lastActivityTime;

    public static final String FETCH_ACCOUNTS_LAST_ACTIVITY_TIME = " SELECT NEW one.group.entities.jpa.helpers.ClientObject(c1.account.id, max(lastActivityTime)) "
            + " FROM Client c1 WHERE c1.account.id IN (:accountIds) GROUP BY c1.account.id ";

    public ClientObject(String accountId, Date lastActivityTime)
    {
        this.accountId = accountId;
        this.lastActivityTime = lastActivityTime;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public Date getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivityTime)
    {
        this.lastActivityTime = lastActivityTime;
    }

}
