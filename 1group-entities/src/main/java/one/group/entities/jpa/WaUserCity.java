/**
 * 
 */
package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import one.group.core.Constant;
import one.group.utils.Utils;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_user_city")
public class WaUserCity implements Serializable
{

    public static final String QUERY_FIND_WA_USER_CITY_LIST = "DELETE FROM WaUserCity u WHERE u.admin.id = :adminId and u.city.id IN :cityIdList";
    public static final String QUERY_DELETE_BY_USER_IDS = "DELETE FROM WaUserCity u WHERE u.admin.id IN :adminIdList" ;
    private static final long serialVersionUID = 1L;

    public WaUserCity()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", unique = true, nullable = false)
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "admin_id", referencedColumnName = "ID")
    private Admin admin;

    @JoinColumn(name = "city_id", referencedColumnName = "ID")
    @OneToOne(fetch = FetchType.LAZY)
    private City city;

    @Column(name = "priority")
    private int priority;

    @Column(name = "status")
    private int status;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public Admin getAdmin()
    {
        return admin;
    }

    public void setAdmin(Admin admin)
    {
        this.admin = admin;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((admin == null) ? 0 : admin.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + priority;
        result = prime * result + status;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaUserCity other = (WaUserCity) obj;
        if (admin == null)
        {
            if (other.admin != null)
                return false;
        }
        else if (!admin.equals(other.admin))
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (priority != other.priority)
            return false;
        if (status != other.status)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaUserCity [id=" + id + ", admin=" + admin + ", city=" + city + ", priority=" + priority + ", status=" + status + "]";
    }

}
