package one.group.entities.jpa.listeners;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.base.BaseEntity;


public class PropertyListingEventListener
{
    @PrePersist
    public void setCreatedTime(PropertyListing model)
    {
        if (model.getCreatedTime() == null)
        {
            model.setCreatedTime(new Date());
        }
        else if (model.getCreatedTime() != null)
        {
            model.setCreatedTime(model.getCreatedTime());
        }
        else
        {
            model.setCreatedTime(model.getCreatedTime());
        }
    }

    @PreUpdate
    public void preUpdate(final BaseEntity base)
    {

    }
}
