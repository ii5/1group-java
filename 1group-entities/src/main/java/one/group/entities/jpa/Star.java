package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.StarType;
import one.group.entities.jpa.base.BaseEntity;

@Entity
@Table(name = "star")
@NamedQueries({ @NamedQuery(name = Star.QUERY_FIND_STARRED_ENTRY, query = "FROM Star s WHERE s.referenceId =:referenceId AND s.targetEntity =:targetEntity"),
        @NamedQuery(name = Star.QUERY_FIND_ALL_STARS, query = "FROM Star s WHERE s.referenceId =:referenceId AND s.type = one.group.core.enums.StarType.BROADCAST ORDER BY s.createdTime DESC"), })
public class Star extends BaseEntity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_STARRED_ENTRY = "findStarredEntry";
    public static final String QUERY_FIND_ALL_STARS = "findAllStarred";
    @Column(name = "reference_id")
    private String referenceId;

    @Column(name = "target_entity")
    private String targetEntity;

    @Enumerated(EnumType.STRING)
    private StarType type;

    public Star()
    {
        setCreatedTime(new Date());

    }

    /**
     * @return the referenceId
     */
    public String getReferenceId()
    {
        return referenceId;
    }

    /**
     * @param referenceId
     *            the referenceId to set
     */
    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    /**
     * @return the targetEntity
     */
    public String getTargetEntity()
    {
        return targetEntity;
    }

    /**
     * @return the type
     */
    public StarType getType()
    {
        return type;
    }

    /**
     * @param targetEntity
     *            the targetEntity to set
     */
    public void setTargetEntity(String targetEntity)
    {
        this.targetEntity = targetEntity;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(StarType type)
    {
        this.type = type;
    }
}
