package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "property_search_relation")
@NamedQueries
(
        { 
            @NamedQuery(name = PropertySearchRelation.QUERY_FIND_MATCHING_CLIENTS, query = " FROM PropertySearchRelation p WHERE p.propertyListing.id =:propertyListingId AND p.account.id =:accountId"), 
            @NamedQuery(name = PropertySearchRelation.QUERY_FIND_ALL_MATCHING_CLIENTS, query = " FROM PropertySearchRelation p WHERE p.propertyListing IN :propertyListingIds AND p.account.id =:accountId"),
            @NamedQuery(name = PropertySearchRelation.QUERY_FIND_RELATIONS_OF_ACCOUNT, query = " FROM PropertySearchRelation p WHERE p.account.id = :accountId")
        }
)
public class PropertySearchRelation implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_MATCHING_CLIENTS = "findMatchingClients";
    
    public static final String QUERY_FIND_ALL_MATCHING_CLIENTS = "findAllMatchingClients";
    public static final String QUERY_FIND_RELATIONS_OF_ACCOUNT = "findSearchRelationsOfAnAccount";

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "search_id", referencedColumnName = "ID")
    private Search searchId;

    /**
     * The account id corresponding to the search id.
     */
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "ID")
    private Account account;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_listing_id", referencedColumnName = "ID")
    private PropertyListing propertyListing;

    public Search getSearchId()
    {
        return searchId;
    }

    public Account getAccount()
    {
        return account;
    }

    public PropertyListing getPropertyListing()
    {
        return propertyListing;
    }

    public void setSearchId(Search searchId)
    {
        this.searchId = searchId;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public void setPropertyListing(PropertyListing propertyListing)
    {
        this.propertyListing = propertyListing;
    }

}
