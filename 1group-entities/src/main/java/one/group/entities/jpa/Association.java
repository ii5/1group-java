package one.group.entities.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import one.group.core.enums.status.ApprovalStatus;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * 
 * 
 * @author nyalfernandes
 * 
 *         TODO : Add description. TODO : Change the agent association
 *         collection to a map.
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
public class Association extends BaseEntity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * A name of the association.
     */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * The status of an association based on whether it is active or not.
     */
    @Enumerated(EnumType.STRING)
    private Status status = Status.INACTIVE;

    /**
     * The certificate approval status of an association.
     */
    private ApprovalStatus approvalStatus = ApprovalStatus.REQUESTED;

    /**
     * The agents linked to this association.
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.LAZY)
    private Set<Agent> agents = new HashSet<Agent>();

    /**
     * Phone number linked with the association as a primary contact.
     */
    @OneToOne(cascade = CascadeType.ALL)
    private PhoneNumber phoneNumber;

    // For JPA
    public Association()
    {
    }

    public Association(final String name, final PhoneNumber phoneNumber)
    {
        Validation.notNull(name, "Association name passed should not be null.");
        Validation.notNull(phoneNumber, "Phone number passed cannot be null.");

        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public void addAgent(final Agent agent)
    {
        Validation.notNull(agent, "Agent to be added to an association should not be null.");
        this.agents.add(agent);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Association other = (Association) obj;
        if (name == null)
        {
            if (other.name != null)
            {
                return false;
            }
        }
        else if (!name.equals(other.name))
        {
            return false;
        }
        return true;
    }

    public Set<Agent> getAgents()
    {
        return agents;
    }

    public ApprovalStatus getApprovalStatus()
    {
        return approvalStatus;
    }

    public String getName()
    {
        return name;
    }

    public PhoneNumber getPhoneNumber()
    {
        return phoneNumber;
    }

    public Status getStatus()
    {
        return status;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public void setAgents(final Set<Agent> agents)
    {
        this.agents = agents;
    }

    public void setApprovalStatus(final ApprovalStatus approvalStatus)
    {
        this.approvalStatus = approvalStatus;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setPhoneNumber(final PhoneNumber phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public void setStatus(final Status status)
    {
        this.status = status;
    }

}
