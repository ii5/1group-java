package one.group.entities.jpa.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.Constant;
import one.group.utils.Utils;

/**
 * The parent entity of all others. Contains all common basic audit information,
 * et cetera required in common for other entities.
 * 
 * @author nyalfernandes
 * 
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    // @GenericGenerator(name="moveIn-Generator",
    // strategy="one.group.entities.jpa.helpers.MoveInIDGenerator")
    // @GeneratedValue(generator="moveIn-Generator")
    private String id;

    @Column(name = "created_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "updated_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "updated_by", nullable = true)
    private String updatedBy;

    public BaseEntity()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!this.getClass().isAssignableFrom(obj.getClass()))
        {
            return false;
        }
        BaseEntity other = (BaseEntity) obj;
        if (id == null)
        {
            if (other.id != null)
            {
                return false;
            }
        }
        else if (!id.equals(other.getId()))
        {
            return false;
        }
        return true;
    }

    public String getId()
    {
        return id;
    }

    public String getIdAsString()
    {
        return id.toString();
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString()
    {
        return "BaseEntity [id=" + id + ", createdTime=" + createdTime + ", updatedTime=" + updatedTime + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
    }

}
