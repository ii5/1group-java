package one.group.entities.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
        @NamedQuery(name = NativeContacts.QUERY_FIND_ACCOUNT_RELATIONS_BY_NUMBER, query = "FROM NativeContacts a where a.phoneNumber = :phoneNumber"),
        @NamedQuery(name = NativeContacts.QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBER, query = "FROM NativeContacts a where a.phoneNumber = :phoneNumber AND a.account = :account"),
        @NamedQuery(name = NativeContacts.QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBERS, query = "FROM NativeContacts a where a.phoneNumber in (:phoneNumbers) AND a.account = :account"),
        @NamedQuery(name = NativeContacts.QUERY_FIND_NUMBERS_BY_ACCOUNT, query = "FROM NativeContacts a where a.account = :account"),
        @NamedQuery(name = NativeContacts.QUERY_COUNT_NUMBERS_BY_ACCOUNT_ID, query = "select a.fullName FROM NativeContacts a where a.account.id = :accountId and a.phoneNumber not in (:phoneNumbers) and REGEXP(a.phoneNumber, '\\\\+91[[:digit:]]{10}$') <> 0 group by a.fullName"),
        @NamedQuery(name = NativeContacts.QUERY_FIND_CONTACTS_BY_PHONE_NUMBER_AND_STATUS, query = " SELECT nc.account.id FROM NativeContacts nc where nc.phoneNumber = :phoneNumber AND nc.account.status=:status") })
@Table(name = "native_contacts")
public class NativeContacts implements Serializable
{
    private static final long serialVersionUID = 1L;

    public NativeContacts()
    {
    }

    public static final String QUERY_FIND_ACCOUNT_RELATIONS_BY_NUMBER = "findAccountRelationsByNumber";
    public static final String QUERY_FIND_NUMBERS_BY_ACCOUNT = "findNumberByAccountRelations";
    public static final String QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBER = "findContactsByAccountAndNumber";
    public static final String QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBERS = "findContactsByAccountAndNumbers";
    public static final String QUERY_FIND_NUMBERS_BY_ACCOUNT_ID = "findNumberByAccountId";
    public static final String QUERY_COUNT_NUMBERS_BY_ACCOUNT_ID = "findCountNumberByAccountId";
    public static final String QUERY_FIND_CONTACTS_BY_PHONE_NUMBER_AND_STATUS = "findContactsByPhoneNumberAndStatus";

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by", referencedColumnName = "ID")
    private Account account;

    @Id
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "full_name", nullable = true, length = 4000)
    private String fullName;

    @Column(name = "company_name", nullable = true, length = 4000)
    private String companyName;

    @Column(name = "created_time", nullable = true)
    private Timestamp createdTime;

    public Timestamp getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NativeContacts other = (NativeContacts) obj;
        if (account.getIdAsString() == null)
        {
            if (other.account.getIdAsString() != null)
                return false;
        }
        else if (!account.getIdAsString().equals(other.account.getIdAsString()))
            return false;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;

        return true;
    }
}
