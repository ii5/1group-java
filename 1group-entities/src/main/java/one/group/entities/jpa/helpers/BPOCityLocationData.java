package one.group.entities.jpa.helpers;

/**
 * 
 * @author nyalfernandes
 *
 */

public class BPOCityLocationData
{
    public static final String QUERY_BPO_FETCH_CITY_AND_LOCATIONS_FROM_CITY_ID_LIST = "SELECT NEW one.group.entities.jpa.helpers.BPOCityLocationData(l.city.name, l.id, l.locationDisplayName) FROM Location l WHERE l.city.id IN :cityIdList AND l.status IN :locationStatusList";

    public static final String QUERY_BPO_FETCH_CITY_AND_LOCATIONS_FROM_LOCATION_STATUS = "SELECT NEW one.group.entities.jpa.helpers.BPOCityLocationData(l.city.name, l.id, l.locationDisplayName) FROM Location l WHERE l.status IN :locationStatusList";

    private String cityName;

    private String locationId;

    private String locationDisplayName;

    public BPOCityLocationData(String cityName, String locationId, String locationDisplayName)
    {
        this.cityName = cityName;
        this.locationId = locationId;
        this.locationDisplayName = locationDisplayName;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
        result = prime * result + ((locationDisplayName == null) ? 0 : locationDisplayName.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BPOCityLocationData other = (BPOCityLocationData) obj;
        if (cityName == null)
        {
            if (other.cityName != null)
                return false;
        }
        else if (!cityName.equals(other.cityName))
            return false;
        if (locationDisplayName == null)
        {
            if (other.locationDisplayName != null)
                return false;
        }
        else if (!locationDisplayName.equals(other.locationDisplayName))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        return true;
    }

}
