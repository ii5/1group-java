package one.group.entities.jpa.helpers;

import java.util.Comparator;

import one.group.entities.api.response.WSMatchingPropertyListing;


public class WSMatchingPropertyListingAccountComparator implements Comparator<WSMatchingPropertyListing>
{
    public int compare(WSMatchingPropertyListing o1, WSMatchingPropertyListing o2)
    {
        if (o1 == null)
        {
            return 1;
        }
        
        if (o2 == null)
        {
            return -1;
        }
        
        if (o1.getAccountId() == null || o1.getAccountId().isEmpty())
        {
            return 1;
        }
        
        if (o2.getAccountId() == null || o2.getAccountId().isEmpty())
        {
            return -1;
        }
        long o1Id = Long.valueOf(o1.getAccountId());
        long o2Id = Long.valueOf(o2.getAccountId());
        
        if (o1Id == o2Id)
        {
            return o1.compareTo(o2);
        }
        return  o1Id < o2Id ? 1 : -1;
    }
}
