package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.utils.validation.Validation;

@Entity
@Table(name = "sync_update")
@NamedQueries({ @NamedQuery(name = SyncUpdate.QUERY_FIND_BY_ACCOUNT_ID, query = "FROM SyncUpdate s where s.accountId = :accountId"),
        @NamedQuery(name = SyncUpdate.QUERY_FIND_BY_ACCOUNT_AND_INDEX, query = "FROM SyncUpdate s where s.accountId = :accountId and s.updateIndex = :updateIndex"),
        @NamedQuery(name = SyncUpdate.QUERY_DELETE_UPDATES_LESS_THAN_MIN_SYNC_INDEX, query = "DELETE FROM SyncUpdate s where updateIndex = :updateIndex AND s.accountId = :accountId"),
        @NamedQuery(name = SyncUpdate.QUERY_FIND_BY_ACCOUNT_FROM_AND_TO, query = "FROM SyncUpdate s where s.accountId = :accountId and s.updateIndex BETWEEN :fromUpdateIndex AND :toUpdateIndex") })
public class SyncUpdate implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findSyncLogByAccountId";
    public static final String QUERY_FIND_BY_ACCOUNT_AND_INDEX = "findSyncLogByAccountAndIndex";
    public static final String QUERY_FIND_BY_ACCOUNT_FROM_AND_TO = "findSyncLogByAccountFromAndToIndex";
    public static final String QUERY_DELETE_UPDATES_LESS_THAN_MIN_SYNC_INDEX = "deleteUpdates";

    @Id
    @Column(name = "user_id")
    private String accountId;

    @Id
    @Column(name = "update_index")
    private Integer updateIndex = 0;

    @Column(nullable = false, length = 10000)
    private String update;

    protected SyncUpdate()
    {
    }

    public SyncUpdate(String accountId, int updateIndex, String update)
    {
        Validation.notNull(accountId, "Account id passed should not be null.");
        Validation.notNull(update, "The update json string should not be null.");

        this.accountId = accountId;
        this.updateIndex = updateIndex;
        this.update = update;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public Integer getUpdateIndex()
    {
        return updateIndex;
    }

    public String getUpdate()
    {
        return update;
    }

    public void setUpdate(String update)
    {
        this.update = update;
    }

    public void setUpdateIndex(Integer updateIndex)
    {
        this.updateIndex = updateIndex;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        result = prime * result + ((updateIndex == null) ? 0 : updateIndex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SyncUpdate other = (SyncUpdate) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        if (updateIndex == null)
        {
            if (other.updateIndex != null)
                return false;
        }
        else if (!updateIndex.equals(other.updateIndex))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "SyncLog [accountId=" + accountId + ", updateIndex=" + updateIndex + ", update=" + update + "]";
    }

}
