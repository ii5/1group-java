package one.group.entities.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;

/**
 * City information used widely thought the context of the project.
 * 
 * @author nyalfernandes
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = City.QUERY_GET_ALL_CITIES, query = "FROM City"), @NamedQuery(name = City.QUERY_FIND_ALL_CITIES_BY_ID_LIST, query = "FROM City c WHERE c.id IN :idList"),
        @NamedQuery(name = City.QUERY_FIND_ALL_PUBLISHED_CITY_LIST, query = "FROM City c WHERE c.isPublished IN :isPublishedList"),
        @NamedQuery(name = City.QUERY_FIND_ALL_PUBLISHED_CITY_LIST_BY_IDS , query = "FROM City c WHERE c.id IN :cityIdList AND c.isPublished IN :isPublishedList"),
        })
@NamedEntityGraphs(@NamedEntityGraph(name = City.ENTITY_GRAPH_LOAD_CITY, attributeNodes = @NamedAttributeNode("locations")))
@Table(name = "city")
public class City extends BaseEntity
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_ALL_CITIES = "findAllCities";
    public static final String QUERY_FIND_ALL_CITIES_BY_ID_LIST = "findAllCitiesByIdList";
    public static final String QUERY_FIND_ALL_PUBLISHED_CITY_LIST = "findAllCitiesByIsPublished";
    public static final String QUERY_FIND_ALL_PUBLISHED_CITY_LIST_BY_IDS = "findAllCitiesByIsPublishedAndCityIds";

    public static final String ENTITY_GRAPH_LOAD_CITY = "entityGraphLoadCityIdAndNameWithLocation";

    @Column(nullable = false, unique = true, name = "name")
    private String name;
    
    @Column(nullable = false, unique = true, name = "name", insertable = false, updatable = false)
    private String cityName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private Set<Location> locations = new HashSet<Location>();

    @Column(name = "state_code")
    private String stateCode;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "state_id")
    private String stateId;

    @Column(name = "is_published")
    private boolean isPublished;

    public String getCityName() 
    {
		return cityName;
	}
    
    public void setCityName(String cityName)
    {
    	this.cityName = cityName;
    }
    
    public String getCountryCode()
    {
        return countryCode;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public String getStateId()
    {
        return stateId;
    }

    public Set<Location> getLocations()
    {
        return locations;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setLocations(Set<Location> locations)
    {
        this.locations = locations;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public void setPublished(boolean isPublished)
    {
        this.isPublished = isPublished;
    }

    public void setStateCode(String stateCode)
    {
        this.stateCode = stateCode;
    }

    public void setStateId(String stateId)
    {
        this.stateId = stateId;
    }

	@Override
	public String toString() {
		return "City [name=" + name + ", cityName=" + cityName + ", isPublished=" + isPublished + ", getId()=" + getId()
				+ "]";
	}

    
}
