package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "location_match_admin")
public class LocationMatchAdmin implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    private LocationAdmin location;

    @Id
    @JoinColumn(name = "match_id", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private LocationAdmin matchLocation;

    @Column(name = "is_exact")
    private boolean isExact;

    @JoinColumn(name = "city_id", referencedColumnName = "ID")
    @OneToOne(fetch = FetchType.LAZY)
    private City city;

    public boolean isExact()
    {
        return isExact;
    }

    public void setExact(boolean isExact)
    {
        this.isExact = isExact;
    }

    public LocationMatchAdmin()
    {

    }

    public LocationAdmin getLocation()
    {
        return location;
    }

    public LocationAdmin getMatchLocation()
    {
        return matchLocation;
    }

    public void setLocation(LocationAdmin location)
    {
        this.location = location;
    }

    public void setMatchLocation(LocationAdmin matchLocation)
    {
        this.matchLocation = matchLocation;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + (isExact ? 1231 : 1237);
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((matchLocation == null) ? 0 : matchLocation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocationMatchAdmin other = (LocationMatchAdmin) obj;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (isExact != other.isExact)
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (matchLocation == null)
        {
            if (other.matchLocation != null)
                return false;
        }
        else if (!matchLocation.equals(other.matchLocation))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "LocationMatchAdmin [location=" + location + ", matchLocation=" + matchLocation + ", isExact=" + isExact + ", city=" + city + "]";
    }

}
