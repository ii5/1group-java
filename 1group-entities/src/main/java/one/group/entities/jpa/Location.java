package one.group.entities.jpa;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import one.group.core.enums.LocationType;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Point;

/**
 * The <code>Locality</code> to which the properties belong to. Localities can
 * be added from the back end admin system only.
 * 
 * @author nyalfernandes
 * 
 *         TODO : Add description TODO : Add a parameterised constructor TODO :
 *         Check for additional states.
 * 
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Location.QUERY_GET_LOCALITY_BY_CITY, query = "FROM Location l WHERE l.city in :cityList"),
        @NamedQuery(name = Location.QUERY_GET_ALL_LOCATIONS, query = "FROM Location"),
        @NamedQuery(name = Location.QUERY_FIND_ALL_LOCATIONS_BY_ID_LIST, query = "FROM Location l WHERE l.id IN :idList"),
        @NamedQuery(name = Location.QUERY_GET_LOCALITY_BY_CITY_ID, query = "FROM Location l WHERE l.city.id  = :cityId"),
        @NamedQuery(name = Location.QUERY_GET_LOCALITY_BY_CITY_IDS, query = "FROM Location l WHERE l.city.id in :cityIds"),
        @NamedQuery(name = Location.QUERY_GET_LOCALITY_BY_STATUS, query = "FROM Location l WHERE l.status in :statusList") })
@Table(name = "location")
public class Location implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_LOCALITY_BY_CITY = "findLocalitiesByCity";
    public static final String QUERY_GET_ALL_LOCATIONS = "findLocationsList";
    public static final String CALL_STORE_PROCEDURE = "callStoreProcedure";
    public static final String QUERY_FIND_ALL_LOCATIONS_BY_ID_LIST = "findAllLocationsByIdList";
    public static final String QUERY_GET_LOCALITY_BY_CITY_ID = "findLocalitiesByCityId";
    public static final String QUERY_GET_LOCALITY_BY_CITY_IDS = "findLocalitiesByCityIds";
    public static final String QUERY_GET_LOCALITY_BY_STATUS = "findLocalitiesByStatus";

    @Id
    private String id;

    @Column(nullable = false, name = "name")
    private String localityName;

    @ManyToOne(fetch = FetchType.LAZY)
    private City city;

    @Column(name = "zip_code", nullable = true)
    private String zipCode;

    @Column(name = "parent_id", nullable = true)
    private String parentId;

    @Enumerated(EnumType.STRING)
    private LocationType type;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    private Set<NearByLocation> nearByLocality = new HashSet<NearByLocation>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    private Set<LocationMatch> locationMatches = new HashSet<LocationMatch>();

    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "geopoint")
    private Point geopoint;

    @Column(name = "location_display_name")
    private String locationDisplayName;

    public Location()
    {

    };

    public Location(final String name)
    {
        this.localityName = name;
    }

    public City getCity()
    {
        return city;
    }

    // public Point getGeopoint()
    // {
    // return geopoint;
    // }
    //
    // public void setGeopoint(Point geopoint)
    // {
    // this.geopoint = geopoint;
    // }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    public String getLocalityName()
    {
        return localityName;
    }

    public void setCity(final City city)
    {
        this.city = city;
    }

    public void setLocalityName(final String localityName)
    {
        this.localityName = localityName;
    }

    public String getParentId()
    {
        return parentId;
    }

    public LocationType getType()
    {
        return type;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public void setType(LocationType type)
    {
        this.type = type;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getId()
    {
        return id;
    }

    public Status getStatus()
    {
        return status;
    }

    public Point getGeopoint()
    {
        return geopoint;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getIdAsString()
    {
        return id.toString();
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Location other = (Location) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Location [id=" + id + ", localityName=" + localityName + ", city=" + city + ", zipCode=" + zipCode + ", parentId=" + parentId + ", type=" + type + ", status=" + status + ", geopoint="
                + geopoint + "]";
    }

}
