package one.group.entities.jpa.helpers;

import java.util.Comparator;

import one.group.entities.api.response.WSNewsFeed;

public class WSNewsFeedRelevanceScoreComparator implements Comparator<WSNewsFeed>
{
    public WSNewsFeedRelevanceScoreComparator(boolean considerRecalculatedRelevance)
    {
        this.considerRecalculatedRelevance = considerRecalculatedRelevance;
    }
    
    private boolean considerRecalculatedRelevance = false;

    public int compare(WSNewsFeed o1, WSNewsFeed o2)
    {
        if (o1 == null)
        {
            return -1;
        }

        if (o2 == null)
        {
            return 1;
        }

        Float f1 = null;
        Float f2 = null;
        
        if (considerRecalculatedRelevance)
        {
            f1 = o1.getRecalculatedRelevance();
            f2 = o2.getRecalculatedRelevance();
        }
        else
        {
            f1 = o1.getRelevance();
            f2 = o2.getRelevance();
        }

        if (f1 == null)
        {
            return -1;
        }

        if (f2 == null)
        {
            return 1;
        }

        if (f1.equals(f2))
        {
            return o1.compareTo(o2);
        }

        return f2.compareTo(f1);
    }
}
