package one.group.entities.jpa;

import javax.persistence.Entity;
import javax.persistence.Table;

import one.group.entities.socket.Message;

/**
 * 
 * @author nyalfernandes
 * 
 */
@Entity
@Table(name = "wa_message")
public class Messages extends Message
{
	protected Messages() {};
	
	public Messages (Message m)
	{
		super(m);
	}
}
