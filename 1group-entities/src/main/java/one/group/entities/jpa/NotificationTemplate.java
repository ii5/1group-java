package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "notification_template")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = NotificationTemplate.QUERY_FETCH_ALL_TEMPLATES, query = "FROM NotificationTemplate") })
public class NotificationTemplate extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    public static final String QUERY_FETCH_ALL_TEMPLATES = "fetchAllTemplates";

    @Column(name = "template", nullable = false)
    private String template;

    public String getTemplate()
    {
        return template;
    }

    public void setTemplate(String template)
    {
        this.template = template;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((template == null) ? 0 : template.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        NotificationTemplate other = (NotificationTemplate) obj;
        if (template == null)
        {
            if (other.template != null)
                return false;
        }
        else if (!template.equals(other.template))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "NotificationTemplate [template=" + template + "]";
    }

}
