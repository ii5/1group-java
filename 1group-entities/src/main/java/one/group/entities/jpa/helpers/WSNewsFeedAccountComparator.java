package one.group.entities.jpa.helpers;

import java.util.Comparator;

import one.group.entities.api.response.WSNewsFeed;


public class WSNewsFeedAccountComparator implements Comparator<WSNewsFeed>
{
    public int compare(WSNewsFeed firstNewsFeed, WSNewsFeed secondNewsFeed)
    {
        int retVal = -1;
        if (firstNewsFeed == null || firstNewsFeed.getAccountId() == null || firstNewsFeed.getAccountId().trim().isEmpty())
        {
            retVal = 1;
            return retVal;
        }

        if (secondNewsFeed == null || secondNewsFeed.getAccountId() == null || secondNewsFeed.getAccountId().trim().isEmpty())
        {
            retVal = -1;
            return retVal;
        }

        Long firstNewsFeedAccountId = Long.valueOf(firstNewsFeed.getAccountId());
        Long secondNewsFeedAccountId = Long.valueOf(secondNewsFeed.getAccountId());

        if (firstNewsFeedAccountId.equals(secondNewsFeedAccountId))
        {
            retVal = firstNewsFeed.compareTo(secondNewsFeed);
            return retVal;
        }
        
        retVal = secondNewsFeedAccountId.compareTo(firstNewsFeedAccountId);
        return retVal;
    }
}
