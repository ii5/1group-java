/**
 * 
 */
package one.group.entities.jpa;

import javax.persistence.Entity;
import javax.persistence.Table;

import one.group.entities.socket.Message;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_messages_archive")
//@SQLInsert(sql = "INSERT IGNORE INTO wa_messages_archive (id, sender_phone_number, group_id, text, hash, message_status, message_sent_time, created_by, created_time, bpo_message_status, message_type, bpo_status_update_time, is_duplicate, bpo_status_update_by, snapshot_group_status, updated_by, updated_time, broadcast_id, entity_type, to_account_id, message_source) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
public class WaMessagesArchive extends Message
{
	protected WaMessagesArchive() {};
	
	public WaMessagesArchive (Message m)
	{
		super(m);
	}
   

}
