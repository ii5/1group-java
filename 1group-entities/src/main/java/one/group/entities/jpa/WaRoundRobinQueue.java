/**
 * 
 */
package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.Constant;
import one.group.utils.Utils;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_round_robin_queue")
public class WaRoundRobinQueue implements Serializable
{

    private static final long serialVersionUID = 1L;

    public WaRoundRobinQueue()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wa_mobile_no", referencedColumnName = "ID")
    private PhoneNumber waMobileNo;

    @Column(name = "processed")
    private int processed;

    @Column(name = "last_success_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastSuccessDate;

    @Column(name = "last_failure_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastFailureDate;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public int getProcessed()
    {
        return processed;
    }

    public void setProcessed(int processed)
    {
        this.processed = processed;
    }

    public PhoneNumber getWaMobileNo()
    {
        return waMobileNo;
    }

    public void setWaMobileNo(PhoneNumber waMobileNo)
    {
        this.waMobileNo = waMobileNo;
    }

    public Date getLastSuccessDate()
    {
        return lastSuccessDate;
    }

    public void setLastSuccessDate(Date lastSuccessDate)
    {
        this.lastSuccessDate = lastSuccessDate;
    }

    public Date getLastFailureDate()
    {
        return lastFailureDate;
    }

    public void setLastFailureDate(Date lastFailureDate)
    {
        this.lastFailureDate = lastFailureDate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastFailureDate == null) ? 0 : lastFailureDate.hashCode());
        result = prime * result + ((lastSuccessDate == null) ? 0 : lastSuccessDate.hashCode());
        result = prime * result + processed;
        result = prime * result + ((waMobileNo == null) ? 0 : waMobileNo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaRoundRobinQueue other = (WaRoundRobinQueue) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (lastFailureDate == null)
        {
            if (other.lastFailureDate != null)
                return false;
        }
        else if (!lastFailureDate.equals(other.lastFailureDate))
            return false;
        if (lastSuccessDate == null)
        {
            if (other.lastSuccessDate != null)
                return false;
        }
        else if (!lastSuccessDate.equals(other.lastSuccessDate))
            return false;
        if (processed != other.processed)
            return false;
        if (waMobileNo == null)
        {
            if (other.waMobileNo != null)
                return false;
        }
        else if (!waMobileNo.equals(other.waMobileNo))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaRoundRobinQueue [id=" + id + ", waMobileNo=" + waMobileNo + ", processed=" + processed + ", lastSuccessDate=" + lastSuccessDate + ", lastFailureDate=" + lastFailureDate + "]";
    }

}
