package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.GroupSource;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "group_members")
@NamedQueries({
        @NamedQuery(name = GroupMembers.UPDATE_READ_RECEIVED_INDEX_BY_GROUP_AND_PARTICIPANT, query = "UPDATE GroupMembers SET readIndex=:readIndex, receivedIndex=:receivedIndex, receivedMessageId=:receivedMessageId, readMessageId=:readMessageId WHERE groupId=:groupId AND participantId=:participantId"),
        @NamedQuery(name = GroupMembers.QUERY_ALL_MEMBERS_BY_IDS, query = "FROM GroupMembers g where g.participantId in (:participantIds)"),
        @NamedQuery(name = "findAllParticipantIdAndGroupId", query = "FROM GroupMembers g"),
        @NamedQuery(name = GroupMembers.QUERY_FIND_PARTICIPANTS_BY_GROUP_ID, query = "FROM GroupMembers g where g.groupId = :groupId"),
        @NamedQuery(name = GroupMembers.QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER, query = "FROM GroupMembers g WHERE g.mobileNumber IN :mobileNumberList"),
        @NamedQuery(name = GroupMembers.QUERY_FIND_PARTICIPANTS_BY_GROUP_ID_AND_PARTICIPANT_ID, query = "FROM GroupMembers g where g.groupId = :groupId AND g.participantId = :participantId"),
        @NamedQuery(name = GroupMembers.FETCH_GROUP_ID_BY_PARTICIPANTS_AND_SOURCE, query = " SELECT gm1.groupId FROM GroupMembers gm1 WHERE gm1.source =:source AND gm1.participantId IN (:participantOne, :participantTwo) GROUP BY gm1.groupId HAVING COUNT(gm1.groupId) > 1) "),
        @NamedQuery(name = GroupMembers.FETCH_ALL_GROUP_ID_BY_ACCOUNT_ID, query = " SELECT gm1.groupId FROM GroupMembers gm1 WHERE gm1.participantId = :participantId GROUP BY gm1.groupId "),
        @NamedQuery(name = GroupMembers.FETCH_ALL_PARTICIPANT_IDS_BY_GROUP_ID, query = " SELECT gm1.participantId FROM GroupMembers gm1 WHERE gm1.groupId = :groupId "),
        @NamedQuery(name = GroupMembers.QUERY_FIND_ALL_PARTICIPANTS_BY_GROUP_IDS_AND_PARTICIPANT_ID, query = "FROM GroupMembers g where g.groupId IN :groupIdList AND g.participantId = :participantId"),
        @NamedQuery(name = GroupMembers.FETCH_GROUP_ID_BY_PARTICIPANT_ID_AND_SOURCE, query = "SELECT gm1.groupId FROM GroupMembers gm1 WHERE gm1.source =:source AND gm1.participantId  = :participantId GROUP BY gm1.groupId"),
        @NamedQuery(name = GroupMembers.QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER_AND_EVER_SYNC, query = "FROM GroupMembers g WHERE g.mobileNumber IN :mobileNumberList AND g.everSync=:everSync")

})
@EntityListeners(EntryAuditEntityListener.class)
public class GroupMembers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String UPDATE_READ_RECEIVED_INDEX_BY_GROUP_AND_PARTICIPANT = "updateReadAndReceivedIndexByGroupAndParticipant";
    public static final String QUERY_ALL_MEMBERS_BY_IDS = "fetchAllMemberByIds";
    public static final String QUERY_FIND_PARTICIPANTS_BY_GROUP_ID = "findParticipantsByGroupId";
    public static final String QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER = "findMembersByMobileNumber";
    public static final String QUERY_FIND_PARTICIPANTS_BY_GROUP_ID_AND_PARTICIPANT_ID = "findParticipantsByGroupIdAndParticipantId";
    public static final String FETCH_GROUP_ID_BY_PARTICIPANTS_AND_SOURCE = "fetchGroupIdByParticipantsAndSource";
    public static final String FETCH_ALL_GROUP_ID_BY_ACCOUNT_ID = "FetchAllGroupIdByParticipants";
    public static final String FETCH_ALL_PARTICIPANT_IDS_BY_GROUP_ID = "fetchAllParticipantIdsByGroupId";
    public static final String QUERY_FIND_ALL_PARTICIPANTS_BY_GROUP_IDS_AND_PARTICIPANT_ID = "findAllParticipantsByGroupIdsAndParticipantId";
    public static final String FETCH_GROUP_ID_BY_PARTICIPANT_ID_AND_SOURCE = "fetchGroupIdByParticipantIdAndSource";
    public static final String QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER_AND_EVER_SYNC = "findMembersByMobileNumbersAndEverSync";

    @Column(name = "group_id")
    private String groupId;

    @Column(name = "participant_id")
    private String participantId;

    @Column(name = "read_index")
    private int readIndex;

    @Column(name = "received_index")
    private int receivedIndex;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "received_message_id")
    private String receivedMessageId;

    @Column(name = "read_message_id")
    private String readMessageId;

    @Column(name = "active_member")
    private boolean activeMember;

    @Column(name = "is_admin")
    private boolean isAdmin;

    @Column(name = "ever_sync")
    private boolean everSync;

    @Column(name = "current_sync")
    private boolean currentSync;

    @Enumerated(EnumType.STRING)
    @Column(name = "source")
    private GroupSource source;

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public int getReadIndex()
    {
        return readIndex;
    }

    public void setReadIndex(int readIndex)
    {
        this.readIndex = readIndex;
    }

    public int getReceivedIndex()
    {
        return receivedIndex;
    }

    public void setReceivedIndex(int receivedIndex)
    {
        this.receivedIndex = receivedIndex;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public boolean isActiveMember()
    {
        return activeMember;
    }

    public void setActiveMember(boolean activeMember)
    {
        this.activeMember = activeMember;
    }

    public boolean isAdmin()
    {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public boolean isEverSync()
    {
        return everSync;
    }

    public void setEverSync(boolean everSync)
    {
        this.everSync = everSync;
    }

    public boolean isCurrentSync()
    {
        return currentSync;
    }

    public void setCurrentSync(boolean currentSync)
    {
        this.currentSync = currentSync;
    }

    public String getReceivedMessageId()
    {
        return receivedMessageId;
    }

    public void setReceivedMessageId(String receivedMessageId)
    {
        this.receivedMessageId = receivedMessageId;
    }

    public String getReadMessageId()
    {
        return readMessageId;
    }

    public void setReadMessageId(String readMessageId)
    {
        this.readMessageId = readMessageId;
    }

    public GroupSource getSource()
    {
        return source;
    }

    public void setSource(GroupSource source)
    {
        this.source = source;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((participantId == null) ? 0 : participantId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (super.equals(obj))
            return true;
        if (getClass() != obj.getClass())
            return false;
        GroupMembers other = (GroupMembers) obj;
        if (groupId == null)
        {
            if (other.groupId != null)
                return false;
        }
        else if (!groupId.equals(other.groupId))
            return false;
        if (participantId == null)
        {
            if (other.participantId != null)
                return false;
        }
        else if (!participantId.equals(other.participantId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "GroupMembers [groupId=" + groupId + ", participantId=" + participantId + ", readIndex=" + readIndex + ", receivedIndex=" + receivedIndex + ", mobileNumber=" + mobileNumber
                + ", receivedMessageId=" + receivedMessageId + ", readMessageId=" + readMessageId + ", activeMember=" + activeMember + ", isAdmin=" + isAdmin + ", everSync=" + everSync
                + ", currentSync=" + currentSync + ", source=" + source + "]";
    }

}
