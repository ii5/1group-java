package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import one.group.core.Constant;
import one.group.utils.Utils;

@Entity
@EntityListeners(SmsMarketingLog.class)
@Table(name = "sms_marketing_log")
public class SmsMarketingLog implements Serializable
{
    private static final long serialVersionUID = 1L;

    public SmsMarketingLog()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
    private String id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "message")
    private String message;

    @Column(name = "campaign")
    private String campaign;

    @Column(name = "sent_time")
    private Date sentTime;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getCampaign()
    {
        return campaign;
    }

    public void setCampaign(String campaign)
    {
        this.campaign = campaign;
    }

    public Date getSentTime()
    {
        return sentTime;
    }

    public void setSentTime(Date sentTime)
    {
        this.sentTime = sentTime;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((campaign == null) ? 0 : campaign.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((sentTime == null) ? 0 : sentTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmsMarketingLog other = (SmsMarketingLog) obj;
        if (campaign == null)
        {
            if (other.campaign != null)
                return false;
        }
        else if (!campaign.equals(other.campaign))
            return false;
        if (message == null)
        {
            if (other.message != null)
                return false;
        }
        else if (!message.equals(other.message))
            return false;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (sentTime == null)
        {
            if (other.sentTime != null)
                return false;
        }
        else if (!sentTime.equals(other.sentTime))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "smsMarketingLog [id=" + id + ", phoneNumber=" + phoneNumber + ", message=" + message + ", campaign=" + campaign + ", sentTime=" + sentTime + "]";
    }

}
