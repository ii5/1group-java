package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * 
 * @author bhumikabhatt
 *
 */
@Entity
@Table(name = "client_analytics")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = ClientAnalytics.QUERY_FIND_BY_ACCOUNT_ID, query = "FROM ClientAnalytics a where a.accountId = :accountId") })
public class ClientAnalytics implements Serializable
{

    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findByAccountId";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "client_event_time")
    private Date clientEventTime;

    private String context;

    @Column(name = "entity_id")
    private String entityId;

    @Column(name = "entity_type")
    private String entityType;

    @Column(name = "account_id")
    private String accountId;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "created_time", nullable = false)
    private Date createdTime;

    @Column
    private String action;

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public Date getClientEventTime()
    {
        return clientEventTime;
    }

    public void setClientEventTime(Date clientEventTime)
    {
        this.clientEventTime = clientEventTime;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getClientId()
    {
        return clientId;
    }

    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ClientAnalytics other = (ClientAnalytics) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "ClientAnalytics [" + (action != null ? "action=" + action + ", " : "") + "id=" + id + ", " + (clientEventTime != null ? "client_event_time=" + clientEventTime + ", " : "")
                + (context != null ? "context=" + context + ", " : "") + (entityId != null ? "entityId=" + entityId + ", " : "") + (entityType != null ? "entityType=" + entityType + ", " : "")
                + (accountId != null ? "accountId=" + accountId + ", " : "") + (clientId != null ? "clientId=" + clientId + ", " : "") + (createdTime != null ? "createdTime=" + createdTime : "")
                + "]";
    }

}