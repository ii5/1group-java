package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import one.group.core.enums.BookMarkType;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * 
 * @author sanilshet
 *
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = "findBookMarkedEntry", query = "FROM BookMark b WHERE b.referenceId =:referenceId AND b.targetEntity =:targetEntity"),
        @NamedQuery(name = "findBookMarkedEntryOfProperty", query = "FROM BookMark b WHERE b.referenceId =:referenceId AND b.targetEntity IN :targetEntityList"),
        @NamedQuery(name = "findAllBookMarkes", query = "FROM BookMark b WHERE b.referenceId =:referenceId AND b.type = one.group.core.enums.BookMarkType.PROPERTY_LISTING ORDER BY b.createdTime DESC") })
public class BookMark extends BaseEntity
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BOOKMARKED_ENTRY = "findBookMarkedEntry";

    public static final String QUERY_FIND_ALL_BOOKMARKS = "findAllBookMarkes";
    
    public static final String QUERY_CHECK_BOOKMARKED_PROPERTY_LISTINGS = "findBookMarkedEntryOfProperty";

    @Column(name = "reference_id")
    private String referenceId;

    @Column(name = "target_entity")
    private String targetEntity;

    @Enumerated(EnumType.STRING)
    private BookMarkType type;

    /**
     * @return the referenceId
     */
    public String getReferenceId()
    {
        return referenceId;
    }

    /**
     * @param referenceId
     *            the referenceId to set
     */
    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    /**
     * @return the targetEntity
     */
    public String getTargetEntity()
    {
        return targetEntity;
    }

    /**
     * @return the type
     */
    public BookMarkType getType()
    {
        return type;
    }

    /**
     * @param targetEntity
     *            the targetEntity to set
     */
    public void setTargetEntity(String targetEntity)
    {
        this.targetEntity = targetEntity;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(BookMarkType type)
    {
        this.type = type;
    }
}
