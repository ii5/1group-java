package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.utils.validation.Validation;

@Entity
@Table(name = "sync_update_write_cursors")
@NamedQueries
(
        {
            @NamedQuery(name=SyncUpdateWriteCursors.QUERY_FIND_BY_ACCOUNT_ID, query="FROM SyncUpdateWriteCursors s where s.accountId = :accountId")
        }
)
public class SyncUpdateWriteCursors implements Serializable
{
    
    private static final long serialVersionUID = 1L;
    public static final String QUERY_FIND_BY_ACCOUNT_ID = "findWriteCursorsByAccountId";
    
    @Id
    @Column(name="account_id")
    private String accountId;
    
    @Column(name="total_updates", nullable = false)
    private Integer totalUpdates = 0;
    
    protected SyncUpdateWriteCursors() {}
    
    public SyncUpdateWriteCursors(String accountId, int totalUpdates)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");
        
        this.accountId = accountId;
        this.totalUpdates = totalUpdates;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public int getTotalUpdates()
    {
        return totalUpdates;
    }
    
    public void setTotalUpdates(Integer totalUpdates)
    {
        this.totalUpdates = totalUpdates;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SyncUpdateWriteCursors other = (SyncUpdateWriteCursors) obj;
        if (accountId == null)
        {
            if (other.accountId != null)
                return false;
        }
        else if (!accountId.equals(other.accountId))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "SyncLogWriteCursors [accountId=" + accountId + ", totalUpdates=" + totalUpdates + "]";
    }
    
    
}
