/**
 * 
 */
package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.AccountType;
import one.group.core.enums.GroupSelectionStatus;
import one.group.core.enums.SyncStatus;
import one.group.core.enums.UserSource;
import one.group.entities.jpa.base.BaseEntity;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "ANONYMOUS")
@NamedQueries
(
        { 
            @NamedQuery(name = User.QUERY_FIND_BY_EMAIL_ID, query = "FROM User u where u.email = :emailId"), 
        }
)
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_EMAIL_ID = "findByEmailId";

    @Column(name = "fullname")
    private String fullName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login")
    private Date lastLogin;

    @Column(name = "short_reference")
    private String shortReference;

    @Column(name = "email")
    private String email;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;
    
    @Column(name = "city_id", updatable = false, insertable = false)
    private String cityId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", insertable = false, updatable = false)
    private AccountType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "source")
    private UserSource source;

    @Enumerated(EnumType.STRING)
    @Column(name = "wa_sync_status")
    private SyncStatus syncStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "wa_group_selection_status")
    private GroupSelectionStatus groupSelectionStatus;

    @Column(name = "wa_localstorage")
    private String localStorage;

    @Column(name = "wa_phone_number")
    private String waPhoneNumber;
    
    @Column(name = "active_group_count")
    private Integer activeGroupCount;
    
    @Column(name = "approved_group_count")
    private Integer approvedGroupCount;
    
    public String getCityId() 
    {
		return cityId;
	}
    
    public void setCityId(String cityId) 
    {
		this.cityId = cityId;
	}

    public Integer getActiveGroupCount() 
    {
		return activeGroupCount;
	}

	public void setActiveGroupCount(Integer activeGroupCount) 
	{
		this.activeGroupCount = activeGroupCount;
	}

	public Integer getApprovedGroupCount() 
	{
		return approvedGroupCount;
	}

	public void setApprovedGroupCount(Integer approvedGroupCount) 
	{
		this.approvedGroupCount = approvedGroupCount;
	}

	public String getWaPhoneNumber()
    {
        return waPhoneNumber;
    }

    public void setWaPhoneNumber(String waPhoneNumber)
    {
        this.waPhoneNumber = waPhoneNumber;
    }

    public GroupSelectionStatus getGroupSelectionStatus()
    {
        return groupSelectionStatus;
    }

    public SyncStatus getSyncStatus()
    {
        return syncStatus;
    }

    public String getLocalStorage()
    {
        return localStorage;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public Date getLastLogin()
    {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin)
    {
        this.lastLogin = lastLogin;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getShortReference()
    {
        return shortReference;
    }

    public void setShortReference(String shortReference)

    {
        this.shortReference = shortReference;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public AccountType getType()
    {
        return type;
    }

    public void setType(AccountType type)
    {
        this.type = type;
    }

    public UserSource getSource()
    {
        return source;
    }

    public void setSource(UserSource source)
    {
        this.source = source;
    }

    public void setGroupSelectionStatus(GroupSelectionStatus groupSelectionStatus)
    {
        this.groupSelectionStatus = groupSelectionStatus;
    }

    public void setSyncStatus(SyncStatus syncStatus)
    {
        this.syncStatus = syncStatus;
    }

    public void setLocalStorage(String localStorage)
    {
        this.localStorage = localStorage;
    }

    @Override
    public String toString()
    {
        return "User [" + (fullName != null ? "fullName=" + fullName + ", " : "") + (lastLogin != null ? "lastLogin=" + lastLogin + ", " : "") + (shortReference != null ? "shortReference=" + shortReference + ", " : "") + (email != null ? "email=" + email + ", " : "") + (city != null ? "city=" + city + ", " : "") + (type != null ? "type=" + type + ", " : "") + (source != null ? "source=" + source + ", " : "") + (syncStatus != null ? "syncStatus=" + syncStatus + ", " : "") + (groupSelectionStatus != null ? "groupSelectionStatus=" + groupSelectionStatus + ", " : "") + (localStorage != null ? "localStorage=" + localStorage + ", " : "") + (waPhoneNumber != null ? "waPhoneNumber=" + waPhoneNumber + ", " : "") + (getId() != null ? "getId()=" + getId() + ", " : "") + (getCreatedTime() != null ? "getCreatedTime()=" + getCreatedTime() + ", " : "") + (getUpdatedTime() != null ? "getUpdatedTime()=" + getUpdatedTime() + ", " : "") + (getCreatedBy() != null ? "getCreatedBy()=" + getCreatedBy() + ", " : "") + (getUpdatedBy() != null ? "getUpdatedBy()=" + getUpdatedBy() : "") + "]";
    }

}
