package one.group.entities.jpa.helpers;

public class BroadcastCountObject
{
    private String accountId;
    private long count;

    public static final String FETCH_ACCOUNTS_PROPERTY_LISTING_COUNT_OF_ACCOUNT_BY_STATUS = " SELECT NEW one.group.entities.jpa.helpers.BroadcastCountObject(b.account.id,count(b.account.id)) "
            + " FROM Broadcast b WHERE b.account.id IN(:accountIds) GROUP BY b.account.id ";

    public BroadcastCountObject(String accountId, long count)
    {
        this.accountId = accountId;
        this.count = count;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public long getCount()
    {
        return count;
    }

    public void setCount(long count)
    {
        this.count = count;
    }

}
