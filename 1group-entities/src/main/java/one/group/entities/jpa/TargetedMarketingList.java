package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "targeted_marketing_list")
@NamedQueries({ @NamedQuery(name = TargetedMarketingList.FIND_ALL, query = "from TargetedMarketingList") })
public class TargetedMarketingList implements Serializable
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    public static final String FIND_ALL = "findAll";

    @Id
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "data_source", nullable = false)
    private String dataSource;

    @Column(name = "utm_source", nullable = false)
    private String utmSource;

    @Column(name = "utm_medium", nullable = false)
    private String utmMedium;

    @Column(name = "utm_content", nullable = true)
    private String utmContent;

    @Column(name = "utm_campaign", nullable = false)
    private String utmCampaign;

    @Column(name = "existing_win", nullable = false)
    private String existingWin;

    @Column(name = "message_content", nullable = false)
    private String messageContent;

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(String dataSource)
    {
        this.dataSource = dataSource;
    }

    public String getUtmSource()
    {
        return utmSource;
    }

    public void setUtmSource(String utmSource)
    {
        this.utmSource = utmSource;
    }

    public String getUtmMedium()
    {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium)
    {
        this.utmMedium = utmMedium;
    }

    public String getUtmContent()
    {
        return utmContent;
    }

    public void setUtmContent(String utmContent)
    {
        this.utmContent = utmContent;
    }

    public String getUtmCampaign()
    {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign)
    {
        this.utmCampaign = utmCampaign;
    }

    public String getExistingWin()
    {
        return existingWin;
    }

    public void setExistingWin(String existingWin)
    {
        this.existingWin = existingWin;
    }

    public String getMessageContent()
    {
        return messageContent;
    }

    public void setMessageContent(String messageContent)
    {
        this.messageContent = messageContent;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dataSource == null) ? 0 : dataSource.hashCode());
        result = prime * result + ((existingWin == null) ? 0 : existingWin.hashCode());
        result = prime * result + ((messageContent == null) ? 0 : messageContent.hashCode());
        result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result + ((utmCampaign == null) ? 0 : utmCampaign.hashCode());
        result = prime * result + ((utmContent == null) ? 0 : utmContent.hashCode());
        result = prime * result + ((utmMedium == null) ? 0 : utmMedium.hashCode());
        result = prime * result + ((utmSource == null) ? 0 : utmSource.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TargetedMarketingList other = (TargetedMarketingList) obj;
        if (dataSource == null)
        {
            if (other.dataSource != null)
                return false;
        }
        else if (!dataSource.equals(other.dataSource))
            return false;
        if (existingWin == null)
        {
            if (other.existingWin != null)
                return false;
        }
        else if (!existingWin.equals(other.existingWin))
            return false;
        if (messageContent == null)
        {
            if (other.messageContent != null)
                return false;
        }
        else if (!messageContent.equals(other.messageContent))
            return false;
        if (phoneNumber == null)
        {
            if (other.phoneNumber != null)
                return false;
        }
        else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (utmCampaign == null)
        {
            if (other.utmCampaign != null)
                return false;
        }
        else if (!utmCampaign.equals(other.utmCampaign))
            return false;
        if (utmContent == null)
        {
            if (other.utmContent != null)
                return false;
        }
        else if (!utmContent.equals(other.utmContent))
            return false;
        if (utmMedium == null)
        {
            if (other.utmMedium != null)
                return false;
        }
        else if (!utmMedium.equals(other.utmMedium))
            return false;
        if (utmSource == null)
        {
            if (other.utmSource != null)
                return false;
        }
        else if (!utmSource.equals(other.utmSource))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "TargetedMarketingList [phoneNumber=" + phoneNumber + ", dataSource=" + dataSource + ", utmSource=" + utmSource + ", utmMedium=" + utmMedium + ", utmContent=" + utmContent
                + ", utmCampaign=" + utmCampaign + ", existingWin=" + existingWin + ", messageContent=" + messageContent + "]";
    }

}
