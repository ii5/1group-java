package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@Table(name = "groups")
@NamedQueries({ @NamedQuery(name = Groups.QUERY_FETCH_GROUPS_BY_EVERSYNC_AND_STATUS, query = "FROM Groups g WHERE g.everSync = :everSync AND g.status IN :statusList"),
        @NamedQuery(name = Groups.QUERY_FIND_GROUP_IDS__BY_GROUP_NAME, query = "SELECT g.id FROM Groups g WHERE g.groupName = :groupName"),
// @NamedQuery(name = Groups.QUERY_BPO_FIND_DETAILED_GROUP_METADATA, query =
// ""),
})
public class Groups extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FETCH_GROUPS_BY_EVERSYNC_AND_STATUS = "fetchGroupsByEverSyncAndStatus";
    public static final String QUERY_FIND_GROUP_IDS__BY_GROUP_NAME = "findByGroupName";
    public static final String QUERY_BPO_FIND_DETAILED_GROUP_METADATA = "findDetailedGroupMetaData-BPO";

    public Groups()
    {

    }

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "creator_mobile_no")
    private String creatorMobileNo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName = "ID")
    private City city;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", referencedColumnName = "ID")
    private Location location;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private GroupStatus status;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "media_id", referencedColumnName = "ID")
    private Media media;

    @Column(name = "assigned_to")
    private int assignedTo = 0;

    @Column(name = "ever_sync")
    private boolean everSync;

    @Column(name = "unread_from")
    @Temporal(TemporalType.DATE)
    private Date unreadFrom;

    @Column(name = "unreadmsg_count")
    private int unreadmsgCount;

    @Column(name = "totalmsg_count")
    private int totalmsgCount;

    @Column(name = "unreadmsg_count_qa")
    private int unreadmsgCountQa;

    @Column(name = "assigned_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedTime;

    @Column(name = "release_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseTime;

    @Column(name = "status_update_time")
    private Date statusUpdateTime;

    @Column(name = "invalid_expired_count")
    private int invalidExpiredCount;

    @Column(name = "valid_expired_count")
    private int validExpiredCount;

    @Column(name = "duplicate_count")
    private int duplicateCount;

    @Column(name = "latest_message_timestamp")
    @Temporal(TemporalType.DATE)
    private Date latestMessageTimestamp;

    @Column(name = "permit_since")
    @Temporal(TemporalType.DATE)
    private Date permitSince;

    // @Column(name = "last_updated_time")
    // @Temporal(TemporalType.TIMESTAMP)
    // private Date lastUpdatedTime;

    @Column(name = "source")
    @Enumerated(EnumType.STRING)
    private GroupSource source;

    @Column(name = "notes")
    private String notes;

    @Column(name = "bpo_updated_by")
    private String bpoUpdatedBy;

    @Column(name = "bpo_updated_time")
    @Temporal(TemporalType.DATE)
    private Date bpoUpdatedTime;

    @Column(name = "entity_type")
    private String entityType;

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public GroupStatus getStatus()
    {
        return status;
    }

    public void setStatus(GroupStatus status)
    {
        this.status = status;
    }

    public int getAssignedTo()
    {
        return assignedTo;
    }

    public void setAssignedTo(int assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public Date getUnreadFrom()
    {
        return unreadFrom;
    }

    public void setUnreadFrom(Date unreadFrom)
    {
        this.unreadFrom = unreadFrom;
    }

    public int getUnreadmsgCount()
    {
        return unreadmsgCount;
    }

    public void setUnreadmsgCount(int unreadmsgCount)
    {
        this.unreadmsgCount = unreadmsgCount;
    }

    public int getTotalmsgCount()
    {
        return totalmsgCount;
    }

    public void setTotalmsgCount(int totalmsgCount)
    {
        this.totalmsgCount = totalmsgCount;
    }

    public int getUnreadmsgCountQa()
    {
        return unreadmsgCountQa;
    }

    public void setUnreadmsgCountQa(int unreadmsgCountQa)
    {
        this.unreadmsgCountQa = unreadmsgCountQa;
    }

    public Date getAssignedTime()
    {
        return assignedTime;
    }

    public void setAssignedTime(Date assignedTime)
    {
        this.assignedTime = assignedTime;
    }

    public Date getReleaseTime()
    {
        return releaseTime;
    }

    public void setReleaseTime(Date releaseTime)
    {
        this.releaseTime = releaseTime;
    }

    public Date getStatusUpdateTime()
    {
        return statusUpdateTime;
    }

    public void setStatusUpdateTime(Date statusUpdateTime)
    {
        this.statusUpdateTime = statusUpdateTime;
    }

    public int getInvalidExpiredCount()
    {
        return invalidExpiredCount;
    }

    public void setInvalidExpiredCount(int invalidExpiredCount)
    {
        this.invalidExpiredCount = invalidExpiredCount;
    }

    public int getValidExpiredCount()
    {
        return validExpiredCount;
    }

    public void setValidExpiredCount(int validExpiredCount)
    {
        this.validExpiredCount = validExpiredCount;
    }

    public int getDuplicateCount()
    {
        return duplicateCount;
    }

    public void setDuplicateCount(int duplicateCount)
    {
        this.duplicateCount = duplicateCount;
    }

    public Date getLatestMessageTimestamp()
    {
        return latestMessageTimestamp;
    }

    public void setLatestMessageTimestamp(Date latestMessageTimestamp)
    {
        this.latestMessageTimestamp = latestMessageTimestamp;
    }

    public Date getPermitSince()
    {
        return permitSince;
    }

    public void setPermitSince(Date permitSince)
    {
        this.permitSince = permitSince;
    }

    public GroupSource getSource()
    {
        return source;
    }

    public void setSource(GroupSource source)
    {
        this.source = source;
    }

    public String getNotes()
    {
        return notes;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }

    public String getCreatorMobileNo()
    {
        return creatorMobileNo;
    }

    public void setCreatorMobileNo(String creatorMobileNo)
    {
        this.creatorMobileNo = creatorMobileNo;
    }

    public Media getMedia()
    {
        return media;
    }

    public void setMedia(Media media)
    {
        this.media = media;
    }

    public boolean isEverSync()
    {
        return everSync;
    }

    public void setEverSync(Boolean everSync)
    {
        this.everSync = everSync;
    }

    public String getBpoUpdatedBy()
    {
        return bpoUpdatedBy;
    }

    public void setBpoUpdatedBy(String bpoUpdatedBy)
    {
        this.bpoUpdatedBy = bpoUpdatedBy;
    }

    public Date getBpoUpdatedTime()
    {
        return bpoUpdatedTime;
    }

    public void setBpoUpdatedTime(Date bpoUpdatedTime)
    {
        this.bpoUpdatedTime = bpoUpdatedTime;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public void setEverSync(boolean everSync)
    {
        this.everSync = everSync;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((assignedTime == null) ? 0 : assignedTime.hashCode());
        result = prime * result + assignedTo;
        result = prime * result + ((bpoUpdatedBy == null) ? 0 : bpoUpdatedBy.hashCode());
        result = prime * result + ((bpoUpdatedTime == null) ? 0 : bpoUpdatedTime.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((creatorMobileNo == null) ? 0 : creatorMobileNo.hashCode());
        result = prime * result + duplicateCount;
        result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
        result = prime * result + (everSync ? 1231 : 1237);
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
        result = prime * result + invalidExpiredCount;
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((media == null) ? 0 : media.hashCode());
        result = prime * result + ((notes == null) ? 0 : notes.hashCode());
        result = prime * result + ((permitSince == null) ? 0 : permitSince.hashCode());
        result = prime * result + ((releaseTime == null) ? 0 : releaseTime.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((statusUpdateTime == null) ? 0 : statusUpdateTime.hashCode());
        result = prime * result + totalmsgCount;
        result = prime * result + ((unreadFrom == null) ? 0 : unreadFrom.hashCode());
        result = prime * result + unreadmsgCount;
        result = prime * result + unreadmsgCountQa;
        result = prime * result + validExpiredCount;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Groups other = (Groups) obj;
        if (assignedTime == null)
        {
            if (other.assignedTime != null)
                return false;
        }
        else if (!assignedTime.equals(other.assignedTime))
            return false;
        if (assignedTo != other.assignedTo)
            return false;
        if (bpoUpdatedBy == null)
        {
            if (other.bpoUpdatedBy != null)
                return false;
        }
        else if (!bpoUpdatedBy.equals(other.bpoUpdatedBy))
            return false;
        if (bpoUpdatedTime == null)
        {
            if (other.bpoUpdatedTime != null)
                return false;
        }
        else if (!bpoUpdatedTime.equals(other.bpoUpdatedTime))
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (creatorMobileNo != other.creatorMobileNo)
            return false;
        if (duplicateCount != other.duplicateCount)
            return false;
        if (entityType == null)
        {
            if (other.entityType != null)
                return false;
        }
        else if (!entityType.equals(other.entityType))
            return false;
        if (everSync != other.everSync)
            return false;
        if (groupName == null)
        {
            if (other.groupName != null)
                return false;
        }
        else if (!groupName.equals(other.groupName))
            return false;
        if (invalidExpiredCount != other.invalidExpiredCount)
            return false;
        if (latestMessageTimestamp != other.latestMessageTimestamp)
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (media == null)
        {
            if (other.media != null)
                return false;
        }
        else if (!media.equals(other.media))
            return false;
        if (notes == null)
        {
            if (other.notes != null)
                return false;
        }
        else if (!notes.equals(other.notes))
            return false;
        if (permitSince == null)
        {
            if (other.permitSince != null)
                return false;
        }
        else if (!permitSince.equals(other.permitSince))
            return false;
        if (releaseTime == null)
        {
            if (other.releaseTime != null)
                return false;
        }
        else if (!releaseTime.equals(other.releaseTime))
            return false;
        if (source != other.source)
            return false;
        if (status != other.status)
            return false;
        if (statusUpdateTime == null)
        {
            if (other.statusUpdateTime != null)
                return false;
        }
        else if (!statusUpdateTime.equals(other.statusUpdateTime))
            return false;
        if (totalmsgCount != other.totalmsgCount)
            return false;
        if (unreadFrom == null)
        {
            if (other.unreadFrom != null)
                return false;
        }
        else if (!unreadFrom.equals(other.unreadFrom))
            return false;
        if (unreadmsgCount != other.unreadmsgCount)
            return false;
        if (unreadmsgCountQa != other.unreadmsgCountQa)
            return false;
        if (validExpiredCount != other.validExpiredCount)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Groups [groupName=" + groupName + ", creatorMobileNo=" + creatorMobileNo + ", city=" + city + ", location=" + location + ", status=" + status + ", media=" + media + ", assignedTo="
                + assignedTo + ", everSync=" + everSync + ", unreadFrom=" + unreadFrom + ", unreadmsgCount=" + unreadmsgCount + ", totalmsgCount=" + totalmsgCount + ", unreadmsgCountQa="
                + unreadmsgCountQa + ", assignedTime=" + assignedTime + ", releaseTime=" + releaseTime + ", statusUpdateTime=" + statusUpdateTime + ", invalidExpiredCount=" + invalidExpiredCount
                + ", validExpiredCount=" + validExpiredCount + ", duplicateCount=" + duplicateCount + ", latestMessageTimestamp=" + latestMessageTimestamp + ", permitSince=" + permitSince
                + ", source=" + source + ", notes=" + notes + ", bpoUpdatedBy=" + bpoUpdatedBy + ", bpoUpdatedTime=" + bpoUpdatedTime + ", entityType=" + entityType + "]";
    }

}
