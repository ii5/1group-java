package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Amenities linked to a Property. Amenities can be added from the backend
 * only.
 * 
 * @author nyalfernandes
 * 
 *         TODO : Add more description
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = Amenity.FIND_AMENITY_BY_NAME, query = "from Amenity where name =:name") })
public class Amenity extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    public static final String FIND_AMENITY_BY_NAME = "findAmenityByName";

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String type;

    // @ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
    // private Set<Property> properties = new HashSet<Property>();
    //
    // public Set<Property> getProperties()
    // {
    // return properties;
    // }
    //
    // public void setProperties(Set<Property> properties)
    // {
    // this.properties = properties;
    // }
    //
    // public void addProperty(Property property)
    // {
    // this.properties.add(property);
    // }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (!super.equals(obj))
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Amenity other = (Amenity) obj;
        if (name == null)
        {
            if (other.name != null)
            {
                return false;
            }
        }
        else if (!name.equals(other.name))
        {
            return false;
        }
        if (type == null)
        {
            if (other.type != null)
            {
                return false;
            }
        }
        else if (!type.equals(other.type))
        {
            return false;
        }
        return true;
    }

    public String getName()
    {
        return name;
    }

    @JsonIgnore
    public String getType()
    {
        return type;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setType(final String type)
    {
        this.type = type;
    }
}
