package one.group.entities.jpa.helpers;

import java.util.Comparator;

import one.group.entities.api.response.WSNewsFeed;

/**
 * Sorts proeprty listings based on the listing time.
 * 
 * @author nyalfernandes
 * 
 */
public class WSNewsFeedListingTimeComparator implements Comparator<WSNewsFeed>
{
    public int compare(WSNewsFeed o1, WSNewsFeed o2)
    {
        if (o1 == null)
        {
            return -1;
        }

        if (o2 == null)
        {
            return 1;
        }
        
        if (o1.getPropertyListingId().equals(o2.getPropertyListingId()))
        {
            return 0;
        }
        
        Long lt1 = Long.valueOf(o1.getListingTime());
        Long lt2 = Long.valueOf(o2.getListingTime());
        
        int ltc = lt2.compareTo(lt1);
        
        if (ltc == 0)
        {
            return o2.getPropertyListingId().compareTo(o1.getPropertyListingId());
        }
        
        return ltc;
    }
}
