package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "search_notification")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = SearchNotification.QUERY_FETCH_ALL_NOTIFICATION, query = "FROM SearchNotification"),
        @NamedQuery(name = SearchNotification.QUERY_FETCH_NOTIFICATION_SEARCH_COUNT_BY_ACCOUNT_ID_AND_SENT_FLAG, query = "select count(*) FROM SearchNotification s WHERE s.userId =:userId AND s.sent =:sendFlag"), })
public class SearchNotification extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    public static final String QUERY_FETCH_ALL_NOTIFICATION = "fetchAllSearchNotification";
    public static final String QUERY_FETCH_NOTIFICATION_SEARCH_COUNT_BY_ACCOUNT_ID_AND_SENT_FLAG = "fetchAllSearchNotificationByAccountIDAndSentFlag";

    @Column(name = "user_id", nullable = false, unique = true)
    private String userId;

    @Column(name = "is_sent")
    private boolean sent;

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public boolean isSent()
    {
        return sent;
    }

    public void setSent(boolean sent)
    {
        this.sent = sent;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SearchNotification other = (SearchNotification) obj;
        if (userId == null)
        {
            if (other.userId != null)
                return false;
        }
        else if (!userId.equals(other.userId))
            return false;
        return true;
    }

}
