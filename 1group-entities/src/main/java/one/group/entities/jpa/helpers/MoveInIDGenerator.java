package one.group.entities.jpa.helpers;

import java.io.Serializable;

import one.group.core.Constant;
import one.group.utils.Utils;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class MoveInIDGenerator implements IdentifierGenerator
{

    public Serializable generate(SessionImplementor session, Object object) throws HibernateException
    {
        return Utils.randomString(Constant.CHARSET_NUMERIC, 11, false);
    }

}
