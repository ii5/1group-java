package one.group.entities.jpa;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "broadcast")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = Broadcast.IS_BROADCAST_EXIST, query = "select case when (count(id) > 0) then true else false end from Broadcast where id =:id "),
        @NamedQuery(name = Broadcast.QUERY_TOTAL_COUNT, query = "select count(*) FROM Broadcast"),
        @NamedQuery(name = Broadcast.FIND_ALL_BROADCAST_BY_ACCOUNT_ID, query = " FROM Broadcast b where b.account.id = :accountId"),
        @NamedQuery(name = Broadcast.FIND_BROADCAST_BY_MESSAGE_ID, query = " FROM Broadcast b where b.messageId = :messageId"),
        @NamedQuery(name = Broadcast.QUERY_FIND_BY_MESSAGE_CREATED_BY, query = "FROM Broadcast where account.id=:messageCreatedBy and status =:status"),
        @NamedQuery(name = Broadcast.QUERY_FIND_ALL_BROADCAST_BY_BROADCAST_IDS, query = "FROM Broadcast b where b.id in (:broadcastIds) and status =:status"),
        @NamedQuery(name = Broadcast.REMOVE_RELATION_BETWEEN_MESSAGE_AND_BROADCAST, query = "UPDATE Broadcast b SET b.messageId=null, status=:status  WHERE b.id = :broadcastId AND messageId = :messageId)"),
        @NamedQuery(name = Broadcast.UPDATE_BROADCAST_VIEW_COUNT, query = " UPDATE Broadcast b SET b.viewCount = ( b.viewCount + 1 ) WHERE b.id IN( :broadcastIdList )") })
public class Broadcast extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    public static final String IS_BROADCAST_EXIST = "isBroadcastExist";
    public static final String QUERY_TOTAL_COUNT = "queryTotalCount";

    public static final String FIND_ALL_BROADCAST_BY_ACCOUNT_ID = "findAllBroadcastByAccountId";
    public static final String FIND_BROADCAST_BY_MESSAGE_ID = "findBroadcastByMessageId";

    public static final String QUERY_FIND_BY_MESSAGE_CREATED_BY = "querytoFindByMessageCreatedBy";
    public static final String QUERY_FIND_ALL_BROADCAST_BY_BROADCAST_IDS = "querytoFindByBroadcastIds";
    public static final String REMOVE_RELATION_BETWEEN_MESSAGE_AND_BROADCAST = "queryToRemoveRelationBetweenMessageAndBroadcast";
    public static final String UPDATE_BROADCAST_VIEW_COUNT = "updateBroadcastViewCount";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "message_created_by", referencedColumnName = "ID")
    private Account account;
    
    @Column(name = "message_created_by", insertable=false, updatable=false)
    private String messageCreatedBy;

    @Column(name = "message_id")
    private String messageId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "broadcast")
    private Set<BroadcastTag> broadcastTags;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private BroadcastStatus status;

    @Column(name = "view_count")
    private int viewCount;
    
    public String getMessageCreatedBy()
    {
        return messageCreatedBy;
    }
    
    public void setMessageCreatedBy(String messageCreatedBy)
    {
        this.messageCreatedBy = messageCreatedBy;
    }

    public Set<BroadcastTag> getBroadcastTags()
    {
        return broadcastTags;
    }

    public void setBroadcastTags(Set<BroadcastTag> broadcastTags)
    {
        this.broadcastTags = broadcastTags;
    }

    public Account getAccount()
    {
        return account;
    }

    public void setAccount(Account account)
    {
        this.account = account;
    }

    public BroadcastStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastStatus status)
    {
        this.status = status;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public int getViewCount()
    {
        return viewCount;
    }

    public void setViewCount(int viewCount)
    {
        this.viewCount = viewCount;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        result = prime * result + ((broadcastTags == null) ? 0 : broadcastTags.hashCode());
        // result = prime * result + ((message == null) ? 0 :
        // message.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Broadcast other = (Broadcast) obj;
        if (account == null)
        {
            if (other.account != null)
                return false;
        }
        else if (!account.equals(other.account))
            return false;
        if (broadcastTags == null)
        {
            if (other.broadcastTags != null)
                return false;
        }
        else if (!broadcastTags.equals(other.broadcastTags))
            return false;

        if (status != other.status)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Broadcast [account=" + account + ",  broadcastTags=" + broadcastTags + ", status=" + status + "]";
    }

}
