package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import one.group.core.enums.LocationTmpStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Point;

@Entity
@Table(name = "location_temp")
@EntityListeners(EntryAuditEntityListener.class)
public class LocationTmp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @JoinColumn(name = "city_id", referencedColumnName = "ID")
    @OneToOne(fetch = FetchType.LAZY)
    private City city;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "parent_id")
    private int parentId;

    @Column(name = "zip_code")
    private int zipCode;

    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "geopoint")
    private Point geoPoint;

    @Column(name = "status")
    private LocationTmpStatus status;

    @Column(name = "action")
    private String action;

    @Column(name = "errors")
    private String errors;

    @Column(name = "suggestions")
    private String suggestions;

    @Column(name = "login_user_id")
    private String loginUserId;

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public int getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(int zipCode)
    {
        this.zipCode = zipCode;
    }

    public Point getGeoPoint()
    {
        return geoPoint;
    }

    public void setGeoPoint(Point geoPoint)
    {
        this.geoPoint = geoPoint;
    }

    public LocationTmpStatus getStatus()
    {
        return status;
    }

    public void setStatus(LocationTmpStatus status)
    {
        this.status = status;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getErrors()
    {
        return errors;
    }

    public void setErrors(String errors)
    {
        this.errors = errors;
    }

    public String getSuggestions()
    {
        return suggestions;
    }

    public void setSuggestions(String suggestions)
    {
        this.suggestions = suggestions;
    }

    public String getLoginUserId()
    {
        return loginUserId;
    }

    public void setLoginUserId(String loginUserId)
    {
        this.loginUserId = loginUserId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((errors == null) ? 0 : errors.hashCode());
        result = prime * result + ((geoPoint == null) ? 0 : geoPoint.hashCode());
        result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result + ((loginUserId == null) ? 0 : loginUserId.hashCode());
        result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + parentId;
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((suggestions == null) ? 0 : suggestions.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + zipCode;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocationTmp other = (LocationTmp) obj;
        if (action == null)
        {
            if (other.action != null)
                return false;
        }
        else if (!action.equals(other.action))
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (errors == null)
        {
            if (other.errors != null)
                return false;
        }
        else if (!errors.equals(other.errors))
            return false;
        if (geoPoint == null)
        {
            if (other.geoPoint != null)
                return false;
        }
        else if (!geoPoint.equals(other.geoPoint))
            return false;
        if (latitude == null)
        {
            if (other.latitude != null)
                return false;
        }
        else if (!latitude.equals(other.latitude))
            return false;
        if (loginUserId == null)
        {
            if (other.loginUserId != null)
                return false;
        }
        else if (!loginUserId.equals(other.loginUserId))
            return false;
        if (longitude == null)
        {
            if (other.longitude != null)
                return false;
        }
        else if (!longitude.equals(other.longitude))
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        if (parentId != other.parentId)
            return false;
        if (status != other.status)
            return false;
        if (suggestions == null)
        {
            if (other.suggestions != null)
                return false;
        }
        else if (!suggestions.equals(other.suggestions))
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        if (zipCode != other.zipCode)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "LocationTmp [city=" + city + ", latitude=" + latitude + ", longitude=" + longitude + ", name=" + name + ", type=" + type + ", parentId=" + parentId + ", zipCode=" + zipCode
                + ", geoPoint=" + geoPoint + ", status=" + status + ", action=" + action + ", errors=" + errors + ", suggestions=" + suggestions + ", loginUserId=" + loginUserId + "]";
    }

}
