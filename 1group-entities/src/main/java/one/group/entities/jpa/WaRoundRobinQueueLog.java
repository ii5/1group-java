/**
 * 
 */
package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.Constant;
import one.group.core.enums.MessageQueueStatus;
import one.group.utils.Utils;

/**
 * @author ashishthorat
 *
 */
@Entity
@Table(name = "wa_round_robin_queue_log")
public class WaRoundRobinQueueLog implements Serializable
{

    private static final long serialVersionUID = 1L;

    public WaRoundRobinQueueLog()
    {
        this.id = Utils.randomString(Constant.CHARSET_NUMERIC, 16, true);
    }

    @Id
    private String id;

    @Column(name = "server_id")
    private String serverId;

    @Column(name = "process_id")
    private String processId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wa_mobile_no", referencedColumnName = "ID")
    private PhoneNumber waMobileNo;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private MessageQueueStatus status;

    @Column(name = "record_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date recordDate;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getServerId()
    {
        return serverId;
    }

    public void setServerId(String serverId)
    {
        this.serverId = serverId;
    }

    public String getProcessId()
    {
        return processId;
    }

    public void setProcessId(String processId)
    {
        this.processId = processId;
    }

    public PhoneNumber getWaMobileNo()
    {
        return waMobileNo;
    }

    public void setWaMobileNo(PhoneNumber waMobileNo)
    {
        this.waMobileNo = waMobileNo;
    }

    public MessageQueueStatus getStatus()
    {
        return status;
    }

    public void setStatus(MessageQueueStatus status)
    {
        this.status = status;
    }

    public Date getRecordDate()
    {
        return recordDate;
    }

    public void setRecordDate(Date recordDate)
    {
        this.recordDate = recordDate;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((processId == null) ? 0 : processId.hashCode());
        result = prime * result + ((recordDate == null) ? 0 : recordDate.hashCode());
        result = prime * result + ((serverId == null) ? 0 : serverId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((waMobileNo == null) ? 0 : waMobileNo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        WaRoundRobinQueueLog other = (WaRoundRobinQueueLog) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (processId == null)
        {
            if (other.processId != null)
                return false;
        }
        else if (!processId.equals(other.processId))
            return false;
        if (recordDate == null)
        {
            if (other.recordDate != null)
                return false;
        }
        else if (!recordDate.equals(other.recordDate))
            return false;
        if (serverId == null)
        {
            if (other.serverId != null)
                return false;
        }
        else if (!serverId.equals(other.serverId))
            return false;
        if (status == null)
        {
            if (other.status != null)
                return false;
        }
        else if (!status.equals(other.status))
            return false;
        if (waMobileNo == null)
        {
            if (other.waMobileNo != null)
                return false;
        }
        else if (!waMobileNo.equals(other.waMobileNo))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "WaRoundRobinQueueLog [id=" + id + ", serverId=" + serverId + ", processId=" + processId + ", waMobileNo=" + waMobileNo + ", status=" + status + ", recordDate=" + recordDate + "]";
    }

}
