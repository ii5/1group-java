package one.group.entities.jpa.helpers;

public class BroadcastTagCountObject
{
    private String accountId;
    private long count;

    public static final String FETCH_ACCOUNTS_BROADCAST_TAG_COUNT_OF_ACCOUNT_BY_STATUS = " SELECT NEW one.group.entities.jpa.helpers.BroadcastTagCountObject(bt.broadcast.messageCreatedBy,count(bt.id)) "
            + " FROM BroadcastTag bt WHERE bt.broadcast.messageCreatedBy IN(:accountIds) and bt.status=:broadcastTagStatus and bt.broadcast.status =:broadcastStatus GROUP BY bt.broadcast.messageCreatedBy ";

    public BroadcastTagCountObject(String accountId, long count)
    {
        this.accountId = accountId;
        this.count = count;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public long getCount()
    {
        return count;
    }

    public void setCount(long count)
    {
        this.count = count;
    }

}
