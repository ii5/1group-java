package one.group.entities.jpa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;
import one.group.utils.validation.Validation;

/**
 * TrustScore logs the trust endorsement between Accounts.
 * 
 * @author nyalfernandes
 * 
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
public class TrustScore extends BaseEntity
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The agent who initiated the trust endorsement.
     */
    @Column(nullable = false)
    private Account fromAccount;

    /**
     * A message attached to the endorsement.
     */
    @Column
    private String message;

    /**
     * The receiver of the trust endorsement.
     */
    @ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.EAGER)
    private Account toAccount;

    // For JPA
    protected TrustScore()
    {
    }

    public TrustScore(final Agent fromAgent, final Agent toAgent)
    {
        Validation.notNull(fromAgent, "The agent endorsing cannot be null.");
        Validation.notNull(toAgent, "The agent receiving the endorsement cannot be null.");
        Validation.isFalse(fromAgent.equals(toAgent), "Okaaay! We cannot meet your good expectations! You cannot endorse yourself :)!");
        this.fromAccount = fromAgent;
        this.toAccount = toAgent;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        TrustScore other = (TrustScore) obj;
        if (fromAccount == null)
        {
            if (other.fromAccount != null)
            {
                return false;
            }
        }
        else if (!fromAccount.equals(other.fromAccount))
        {
            return false;
        }
        if (toAccount == null)
        {
            if (other.toAccount != null)
            {
                return false;
            }
        }
        else if (!toAccount.equals(other.toAccount))
        {
            return false;
        }
        return true;
    }

    public Account getFromAccount()
    {
        return fromAccount;
    }

    public String getMessage()
    {
        return message;
    }

    public Account getToAccount()
    {
        return toAccount;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fromAccount == null) ? 0 : fromAccount.hashCode());
        result = prime * result + ((toAccount == null) ? 0 : toAccount.hashCode());
        return result;
    }

    public void setFromAccount(final Agent fromAccount)
    {
        this.fromAccount = fromAccount;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    public void setToAgent(final Account toAccount)
    {
        this.toAccount = toAccount;
    }

}
