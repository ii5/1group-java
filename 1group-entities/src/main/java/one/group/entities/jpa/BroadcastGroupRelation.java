package one.group.entities.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "broadcast_group_relation")
@EntityListeners(EntryAuditEntityListener.class)
public class BroadcastGroupRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Column(name = "broadcast_id", nullable = false)
    private String broadcastId;

    @Column(name = "group_id", nullable = false)
    private String groupId;

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((broadcastId == null) ? 0 : broadcastId.hashCode());
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BroadcastGroupRelation other = (BroadcastGroupRelation) obj;
        if (broadcastId == null)
        {
            if (other.broadcastId != null)
                return false;
        }
        else if (!broadcastId.equals(other.broadcastId))
            return false;
        if (groupId == null)
        {
            if (other.groupId != null)
                return false;
        }
        else if (!groupId.equals(other.groupId))
            return false;
        if (getCreatedBy() == null)
        {
            if (other.getCreatedBy() != null)
                return false;
        }
        else if (!groupId.equals(other.getCreatedBy()))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "BroadcastGroupRelation [broadcastId=" + broadcastId + ", groupId=" + groupId + ", getId()=" + getId() + ", getIdAsString()=" + getIdAsString() + ", getCreatedTime()="
                + getCreatedTime() + ", getUpdatedTime()=" + getUpdatedTime() + ", getCreatedBy()=" + getCreatedBy() + ", getUpdatedBy()=" + getUpdatedBy() + "]";
    }

}
