package one.group.entities.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import one.group.core.enums.status.NearByLocalityStatus;

/**
 * 
 * @author sanilshet
 *
 */
@Entity
@Table(name = "nearby_localities_admin")
public class NearByLocalityAdmin implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private Integer id;

    @ManyToOne
    private Location location;

    @JoinColumn(name = "nearby_location", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Location nearByLocation;

    @Enumerated(EnumType.ORDINAL)
    private NearByLocalityStatus status;

    @Column(name = "city_id")
    private String cityId;

    public NearByLocalityAdmin()
    {

    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Location getNearByLocation()
    {
        return nearByLocation;
    }

    public void setNearByLocation(Location nearByLocation)
    {
        this.nearByLocation = nearByLocation;
    }

    public NearByLocalityStatus getStatus()
    {
        return status;
    }

    public void setStatus(NearByLocalityStatus status)
    {
        this.status = status;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

}
