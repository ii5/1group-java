package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "location_match")
@NamedQueries({ @NamedQuery(name = LocationMatch.QUERY_GET_EXACT_LOCATION_MATCH_OF_LOCATION_LIST, query = "FROM LocationMatch n WHERE n.location.id IN :locationIdList and isExact=:isExact"),
        @NamedQuery(name = LocationMatch.QUERY_GET_LOCATION_MATCH_OF_LOCATION, query = "FROM LocationMatch n WHERE n.location.id = :locationId"), })
public class LocationMatch implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_EXACT_LOCATION_MATCH_OF_LOCATION_LIST = "findExactMatchLocationByLocationIdList";
    public static final String QUERY_GET_LOCATION_MATCH_OF_LOCATION = "findMatchLocationByLocationId";
    public static final String TYPED_QUERY_GET_EXACT_LOCATION_MATCH_ID_LIST_BY_LOCATION_ID_LIST = "SELECT n.matchLocation.id FROM LocationMatch n WHERE n.location.id IN :locationIdList AND isExact=:isExact";
    public static final String TYPED_QUERY_GET_LOCATION_MATCH_ID_BY_LOCATION_ID_LIST = "SELECT n.matchLocation.id FROM LocationMatch n WHERE n.location.id IN :locationIdList";

    @Id
    @ManyToOne
    @JoinColumn(name = "location_id", referencedColumnName = "ID")
    private Location location;

    @Id
    @JoinColumn(name = "match_id", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Location matchLocation;

    @Column(name = "is_exact")
    private boolean isExact;

    // @Column(name = "city_id")
    @Transient
    private String cityId;

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Location getMatchLocation()
    {
        return matchLocation;
    }

    public void setMatchLocation(Location matchLocation)
    {
        this.matchLocation = matchLocation;
    }

    public boolean isExact()
    {
        return isExact;
    }

    public void setExact(boolean isExact)
    {
        this.isExact = isExact;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public LocationMatch()
    {
        setCreatedTime(new Date());
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (isExact ? 1231 : 1237);
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((matchLocation == null) ? 0 : matchLocation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocationMatch other = (LocationMatch) obj;
        if (isExact != other.isExact)
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (matchLocation == null)
        {
            if (other.matchLocation != null)
                return false;
        }
        else if (!matchLocation.equals(other.matchLocation))
            return false;
        return true;
    }

}
