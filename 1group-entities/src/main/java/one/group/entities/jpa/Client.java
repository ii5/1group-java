package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * Agent use different devices to use an application. Device can be linked to an
 * agent after sign-in, registration process. Device can be unlinked to an agent
 * after agent logout from a device
 * 
 * @author miteshchavda
 * 
 *         TODO : Add more description
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID, query = "FROM Client where account.id = :accountId"),
        @NamedQuery(name = Client.QUERY_FIND_ACCOUNT_BY_CLIENT_ID, query = "FROM Client where id = :id"),
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_BY_STATUS, query = "FROM Client where status = :status"),
        @NamedQuery(name = Client.QUERY_FIND_BY_PUSHCHANNEL, query = "FROM Client cl WHERE cl.pushChannel = :pushchannel"),
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_PLATFORM, query = "FROM Client c1 where c1.account.id = :accountId and c1.devicePlatform = :devicePlatform and c1.deviceToken IS NOT NULL"),
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_FROM_SCHEDULER_1, query = "FROM Client c1 WHERE c1.status IN :status AND lastActivityTime < :offsetTime"),
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_TOKEN, query = "FROM Client c1 where c1.account.id = :accountId and c1.deviceToken = :deviceToken"),
        @NamedQuery(name = Client.QUERY_FIND_CLIENT_BY_DEVICE_TOKEN, query = "FROM Client c1 where c1.deviceToken = :deviceToken AND c1.deviceToken IS NOT NULL"),
        @NamedQuery(name = Client.QUERY_FIND_ACTIVE_CLIENT_BY_ACCOUNT_ID, query = "FROM Client where account.id = :accountId AND deviceToken IS NOT NULL"),
        @NamedQuery(name = Client.QUERY_GET_LAST_ACTIVITY_TIME, query = "FROM Client where account.id = :accountId AND lastActivityTime IS NOT NULL ORDER BY lastActivityTime DESC"),
        @NamedQuery(name = Client.QUERY_FIND_ALL_ACTIVE_CLIENT, query = "FROM Client where deviceToken IS NOT NULL"),
        @NamedQuery(name = Client.QUERY_FIND_ACCOUNT_ID_FROM_CLIENT_ID, query = "select c.account.id FROM Client c where c.id =:clientId") })
@Table(name = "client")
public class Client extends BaseEntity
{

    private static final long serialVersionUID = 1L;
    public static final String QUERY_FIND_CLIENT_BY_ACCOUNT_ID = "findClientsByAccount";
    public static final String QUERY_FIND_ACCOUNT_BY_CLIENT_ID = "findAccountByClient";
    public static final String QUERY_FIND_CLIENT_BY_STATUS = "findClientsByStatus";
    public static final String QUERY_FIND_BY_PUSHCHANNEL = "findPushchannelCount";
    public static final String QUERY_FIND_CLIENT_BY_DEVICE_TYPE = "findClientsByAccount";
    public static final String QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_PLATFORM = "findClientsByAccountAndDeviceId";
    public static final String QUERY_FIND_CLIENT_FROM_SCHEDULER_1 = "findClientFromScheduler1";
    public static final String QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_TOKEN = "findClientByAccountIdAndDeviceToken";
    public static final String QUERY_FIND_CLIENT_BY_DEVICE_TOKEN = "findClientByDeviceToken";
    public static final String QUERY_FIND_ACTIVE_CLIENT_BY_ACCOUNT_ID = "findAllActiveClientByAccountId";
    public static final String QUERY_FIND_ALL_ACTIVE_CLIENT = "findAllActiveClient";
    public static final String QUERY_GET_LAST_ACTIVITY_TIME = "findLastActivityTime";
    public static final String QUERY_FIND_ACCOUNT_ID_FROM_CLIENT_ID = "findAccountIdFromClientId";

    @Column(name = "is_online")
    private boolean isOnline;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "ID")
    private Account account;

    @Column(name = "app_name")
    private String appName;

    @Column(name = "app_version")
    private String appVersion;

    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "device_model")
    private String deviceModel;

    @Column(name = "device_version")
    private String deviceVersion;

    @Column(name = "push_channel")
    private String pushChannel;

    @Column(name = "push_badge")
    private String pushBadge;

    @Column(name = "push_alert")
    private String pushAlert;

    @Column(name = "push_sound")
    private String pushSound;

    @Column(name = "development")
    private String development;

    @Column(name = "device_platform")
    private String devicePlatform;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DeviceStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_activity_time")
    private Date lastActivityTime;

    @Column(name = "cps_id")
    private String cpsId;

    public Date getLastActivityTime()
    {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivity)
    {
        this.lastActivityTime = lastActivity;
    }

    public Account getAccount()
    {
        return account;
    }

    public String getAppName()
    {
        return appName;
    }

    public String getAppVersion()
    {
        return appVersion;
    }

    public String getDevelopment()
    {
        return development;
    }

    public String getDeviceModel()
    {
        return deviceModel;
    }

    public String getDeviceToken()
    {
        return deviceToken;
    }

    public String getDeviceVersion()
    {
        return deviceVersion;
    }

    public String getPushAlert()
    {
        return pushAlert;
    }

    public String getPushBadge()
    {
        return pushBadge;
    }

    public String getPushSound()
    {
        return pushSound;
    }

    public DeviceStatus getStatus()
    {
        return status;
    }

    public boolean isOnline()
    {
        return isOnline;
    }

    public void setAccount(final Account account)
    {
        this.account = account;
    }

    public String getPushChannel()
    {
        return pushChannel;
    }

    public void setPushChannel(String pushChannel)
    {
        this.pushChannel = pushChannel;
    }

    public void setAppName(final String appName)
    {
        this.appName = appName;
    }

    public void setAppVersion(final String appVersion)
    {
        this.appVersion = appVersion;
    }

    public void setDevelopment(final String development)
    {
        this.development = development;
    }

    public void setDeviceModel(final String deviceModel)
    {
        this.deviceModel = deviceModel;
    }

    public void setDeviceToken(final String deviceToken)
    {
        this.deviceToken = deviceToken;
    }

    public void setDeviceVersion(final String deviceVersion)
    {
        this.deviceVersion = deviceVersion;
    }

    public void setOnline(final boolean isOnline)
    {
        this.isOnline = isOnline;
    }

    public void setPushAlert(final String pushAlert)
    {
        this.pushAlert = pushAlert;
    }

    public void setPushBadge(final String pushBadge)
    {
        this.pushBadge = pushBadge;
    }

    public void setPushSound(final String pushSound)
    {
        this.pushSound = pushSound;
    }

    public void setStatus(final DeviceStatus status)
    {
        this.status = status;
    }

    public String getDevicePlatform()
    {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform)
    {
        this.devicePlatform = devicePlatform;
    }

    public String getCpsId()
    {
        return cpsId;
    }

    public void setCpsId(String cpsId)
    {
        this.cpsId = cpsId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((cpsId == null) ? 0 : cpsId.hashCode());
        result = prime * result + ((deviceToken == null) ? 0 : deviceToken.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Client other = (Client) obj;
        if (cpsId == null)
        {
            if (other.cpsId != null)
                return false;
        }
        else if (!cpsId.equals(other.cpsId))
            return false;
        if (deviceToken == null)
        {
            if (other.deviceToken != null)
                return false;
        }
        else if (!deviceToken.equals(other.deviceToken))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "Client [isOnline=" + isOnline + ", account=" + account + ", appName=" + appName + ", appVersion=" + appVersion + ", deviceToken=" + deviceToken + ", deviceModel=" + deviceModel
                + ", deviceVersion=" + deviceVersion + ", pushChannel=" + pushChannel + ", pushBadge=" + pushBadge + ", pushAlert=" + pushAlert + ", pushSound=" + pushSound + ", development="
                + development + ", devicePlatform=" + devicePlatform + ", status=" + status + ", lastActivityTime=" + lastActivityTime + ", cpsId=" + cpsId + "]";
    }

}
