package one.group.entities.jpa;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import one.group.core.enums.status.Status;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Point;

@Entity
@Table(name = "location_admin")
@NamedQueries({ @NamedQuery(name = LocationAdmin.QUERY_GET_ALL_ADMIN_LOCATIONS, query = "FROM LocationAdmin"),
        @NamedQuery(name = LocationAdmin.QUERY_GET_ALL_ADMIN_LOCATIONS_BY_CITY, query = "FROM LocationAdmin la where la.city.id IN :cityList") })
public class LocationAdmin implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_GET_ALL_ADMIN_LOCATIONS = "findAdminLocationsList";
    public static final String QUERY_GET_ALL_ADMIN_LOCATIONS_BY_CITY = "findAdminLocationsListByCity";
    public static final String CALL_STORE_PROCEDURE = "callStoreProcedure";

    @Id
    private Integer id;

    @Column(nullable = false, name = "name")
    private String localityName;

    @ManyToOne
    private City city;

    @Column(name = "zip_code", nullable = true)
    private String zipCode;

    @Column(name = "parent_id", nullable = true)
    private Integer parentId;

    private String type;

    @Enumerated(EnumType.ORDINAL)
    private Status status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    private Set<NearByLocation> nearByLocality = new HashSet<NearByLocation>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    private Set<LocationMatch> locationMatches = new HashSet<LocationMatch>();

    @Type(type = "org.hibernate.spatial.GeometryType")
    @Column(name = "geopoint")
    private Point geopoint;

    @Column(name = "location_display_name")
    private String locationDisplayName;

    public LocationAdmin()
    {

    };

    public LocationAdmin(final String name)
    {
        this.localityName = name;
    }

    // public Point getGeopoint()
    // {
    // return geopoint;
    // }
    //
    // public void setGeopoint(Point geopoint)
    // {
    // this.geopoint = geopoint;
    // }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    public String getLocalityName()
    {
        return localityName;
    }

    public void setLocalityName(final String localityName)
    {
        this.localityName = localityName;
    }

    public Integer getParentId()
    {
        return parentId;
    }

    public String getType()
    {
        return type;
    }

    public void setParentId(Integer parentId)
    {
        this.parentId = parentId;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public Integer getId()
    {
        return id;
    }

    public Status getStatus()
    {
        return status;
    }

    public Point getGeopoint()
    {
        return geopoint;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public String getIdAsString()
    {
        return id.toString();
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public Set<NearByLocation> getNearByLocality()
    {
        return nearByLocality;
    }

    public void setNearByLocality(Set<NearByLocation> nearByLocality)
    {
        this.nearByLocality = nearByLocality;
    }

    public Set<LocationMatch> getLocationMatches()
    {
        return locationMatches;
    }

    public void setLocationMatches(Set<LocationMatch> locationMatches)
    {
        this.locationMatches = locationMatches;
    }

    public void setGeopoint(Point geopoint)
    {
        this.geopoint = geopoint;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((geopoint == null) ? 0 : geopoint.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((localityName == null) ? 0 : localityName.hashCode());
        result = prime * result + ((locationDisplayName == null) ? 0 : locationDisplayName.hashCode());
        result = prime * result + ((locationMatches == null) ? 0 : locationMatches.hashCode());
        result = prime * result + ((nearByLocality == null) ? 0 : nearByLocality.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocationAdmin other = (LocationAdmin) obj;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (geopoint == null)
        {
            if (other.geopoint != null)
                return false;
        }
        else if (!geopoint.equals(other.geopoint))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (localityName == null)
        {
            if (other.localityName != null)
                return false;
        }
        else if (!localityName.equals(other.localityName))
            return false;
        if (locationDisplayName == null)
        {
            if (other.locationDisplayName != null)
                return false;
        }
        else if (!locationDisplayName.equals(other.locationDisplayName))
            return false;
        if (locationMatches == null)
        {
            if (other.locationMatches != null)
                return false;
        }
        else if (!locationMatches.equals(other.locationMatches))
            return false;
        if (nearByLocality == null)
        {
            if (other.nearByLocality != null)
                return false;
        }
        else if (!nearByLocality.equals(other.nearByLocality))
            return false;
        if (parentId == null)
        {
            if (other.parentId != null)
                return false;
        }
        else if (!parentId.equals(other.parentId))
            return false;
        if (status != other.status)
            return false;
        if (type == null)
        {
            if (other.type != null)
                return false;
        }
        else if (!type.equals(other.type))
            return false;
        if (zipCode == null)
        {
            if (other.zipCode != null)
                return false;
        }
        else if (!zipCode.equals(other.zipCode))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "LocationAdmin [id=" + id + ", localityName=" + localityName + ", city=" + city + ", zipCode=" + zipCode + ", parentId=" + parentId + ", type=" + type + ", status=" + status
                + ", nearByLocality=" + nearByLocality + ", locationMatches=" + locationMatches + ", geopoint=" + geopoint + ", locationDisplayName=" + locationDisplayName + "]";
    }

}
