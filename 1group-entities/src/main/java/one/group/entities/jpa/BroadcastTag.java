package one.group.entities.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.jpa.base.BaseEntity;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@Table(name = "broadcast_tag")
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({
        @NamedQuery(name = BroadcastTag.QUERY_FIND_TAGS_BY_BROADCAST_ID, query = "FROM BroadcastTag b where b.broadcast.id = :broadcastId AND b.broadcast.status = :status"),
        @NamedQuery(name = BroadcastTag.QUERY_FIND_TAGS_BY_BROADCAST_ID_LIST, query = "FROM BroadcastTag b WHERE b.broadcast.id IN :broadcastIdList"),
        @NamedQuery(name = BroadcastTag.QUERY_FIND_BROADCAST_IDS_BY_LOCATION_AND_CITY_LIST, query = "SELECT b.broadcast.id FROM BroadcastTag b WHERE b.location.id in :locationIdList AND b.cityId in :cityIdList"),
        @NamedQuery(name = BroadcastTag.QUERY_FIND_BROADCAST_TAG_IDS_BY_BROADCAST_ID, query = "SELECT b.id FROM BroadcastTag b WHERE b.broadcast.id  = :broadcastId"),
        @NamedQuery(name = BroadcastTag.QUERY_FIND_COUNT_OF_BROADCAST_BY_CITY_ID, query = "SELECT count(DISTINCT bt.broadcast) FROM BroadcastTag bt WHERE bt.cityId IN :cityId AND status=:status") })
public class BroadcastTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_TAGS_BY_BROADCAST_ID = "findTagsByBroadcastId";
    public static final String QUERY_FIND_TAGS_BY_BROADCAST_ID_LIST = "findTagsByBroadcastIdLIst";
    public static final String QUERY_FIND_BROADCAST_IDS_BY_LOCATION_AND_CITY_LIST = "findBroadcastIdsByLocationAndCityList";
    public static final String QUERY_FIND_BROADCAST_TAG_IDS_BY_BROADCAST_ID = "findBroadcastTagIdsByBroadcastId";
    public static final String QUERY_FIND_COUNT_OF_BROADCAST_BY_CITY_ID = "findBroadcastCountByCityId";

    @Column(name = "broadcast_id", updatable = false, insertable = false)
    private String broadcastId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "broadcast_id", referencedColumnName = "ID")
    private Broadcast broadcast;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "size", nullable = true)
    private Long size;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private PropertyTransactionType transactionType = PropertyTransactionType.UNDEFINED;

    @Enumerated(EnumType.STRING)
    @Column(name = "broadcast_type", nullable = false)
    private BroadcastType broadcastType;

    @Enumerated(EnumType.STRING)
    @Column(name = "commission_type", nullable = true)
    private CommissionType commissionType = CommissionType.UNDEFINED;

    @Column(name = "custom_sublocation", nullable = true)
    private String customSublocation;

    @Column(name = "maximum_price", nullable = true)
    private Long maximumPrice;

    @Column(name = "minimum_price", nullable = true)
    private Long minimumPrice;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_type", nullable = false)
    private PropertyType propertyType = PropertyType.UNDEFINED;

    @Enumerated(EnumType.STRING)
    @Column(name = "rooms", nullable = true)
    private Rooms rooms = Rooms.UNDEFINED;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", referencedColumnName = "ID")
    private Location location;

    @Column(name = "location_id", insertable = false, updatable = false)
    private String locationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName = "ID", insertable = false, updatable = false)
    private City city;

    @Column(name = "city_id", nullable = false)
    private String cityId;

    @Column(name = "price", nullable = true)
    private Long price;

    @Column(name = "maximum_size", nullable = true)
    private Long maximumSize;

    @Column(name = "minimum_size", nullable = true)
    private Long minimumSize;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private BroadcastTagStatus status;

    @Column(name = "drafted_by")
    private String draftedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "drafted_time")
    private Date draftedTime;

    @Column(name = "added_by")
    private String addedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "added_time")
    private Date addedTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_market", nullable = false)
    private PropertyMarket propertyMarket = PropertyMarket.UNDEFINED;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_sub_type", nullable = false)
    private PropertySubType propertySubType = PropertySubType.UNDEFINED;

    @Column(name = "discard_reason", nullable = true)
    private String discardReason;

    @Column(name = "closed_by")
    private String closedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "closed_time")
    private Date closedTime;

    public String getClosedBy()
    {
        return closedBy;
    }

    public void setClosedBy(String closedBy)
    {
        this.closedBy = closedBy;
    }

    public Date getClosedTime()
    {
        return closedTime;
    }

    public void setClosedTime(Date closedTime)
    {
        this.closedTime = closedTime;
    }

    public String getDiscardReason()
    {
        return discardReason;
    }

    public void setDiscardReason(String discardReason)
    {
        this.discardReason = discardReason;
    }

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public String getAddedBy()
    {
        return addedBy;
    }

    public String getDraftedBy()
    {
        return draftedBy;
    }

    public Date getAddedTime()
    {
        return addedTime;
    }

    public Date getDraftedTime()
    {
        return draftedTime;
    }

    public void setAddedBy(String addedBy)
    {
        this.addedBy = addedBy;
    }

    public void setAddedTime(Date addedTime)
    {
        this.addedTime = addedTime;
    }

    public void setDraftedBy(String draftedBy)
    {
        this.draftedBy = draftedBy;
    }

    public void setDraftedTime(Date draftedTime)
    {
        this.draftedTime = draftedTime;
    }

    public BroadcastTagStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastTagStatus status)
    {
        this.status = status;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public Long getMaximumSize()
    {
        return maximumSize;
    }

    public void setMaximumSize(Long maximumSize)
    {
        this.maximumSize = maximumSize;
    }

    public Long getMinimumSize()
    {
        return minimumSize;
    }

    public void setMinimumSize(Long minimumSize)
    {
        this.minimumSize = minimumSize;
    }

    public Broadcast getBroadcast()
    {
        return broadcast;
    }

    public void setBroadcast(Broadcast broadcast)
    {
        this.broadcast = broadcast;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getSize()
    {
        return size;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public PropertyTransactionType getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(PropertyTransactionType transactionType)
    {
        this.transactionType = transactionType;
    }

    public BroadcastType getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(BroadcastType broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType comissionType)
    {
        this.commissionType = comissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public Long getMaximumPrice()
    {
        return maximumPrice;
    }

    public void setMaximumPrice(Long maximumPrice)
    {
        this.maximumPrice = maximumPrice;
    }

    public Long getMinimumPrice()
    {
        return minimumPrice;
    }

    public void setMinimumPrice(Long minimumPrice)
    {
        this.minimumPrice = minimumPrice;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public Rooms getRooms()
    {
        return rooms;
    }

    public void setRooms(Rooms rooms)
    {
        this.rooms = rooms;
    }

    public Location getLocation()
    {
        return location;
    }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public Long getPrice()
    {
        return price;
    }

    public void setPrice(Long price)
    {
        this.price = price;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((addedBy == null) ? 0 : addedBy.hashCode());
        result = prime * result + ((addedTime == null) ? 0 : addedTime.hashCode());
        result = prime * result + ((broadcastType == null) ? 0 : broadcastType.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
        result = prime * result + ((commissionType == null) ? 0 : commissionType.hashCode());
        result = prime * result + ((customSublocation == null) ? 0 : customSublocation.hashCode());
        result = prime * result + ((draftedBy == null) ? 0 : draftedBy.hashCode());
        result = prime * result + ((draftedTime == null) ? 0 : draftedTime.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((locationId == null) ? 0 : locationId.hashCode());
        result = prime * result + ((maximumPrice == null) ? 0 : maximumPrice.hashCode());
        result = prime * result + ((maximumSize == null) ? 0 : maximumSize.hashCode());
        result = prime * result + ((minimumPrice == null) ? 0 : minimumPrice.hashCode());
        result = prime * result + ((minimumSize == null) ? 0 : minimumSize.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((propertyMarket == null) ? 0 : propertyMarket.hashCode());
        result = prime * result + ((propertySubType == null) ? 0 : propertySubType.hashCode());
        result = prime * result + ((propertyType == null) ? 0 : propertyType.hashCode());
        result = prime * result + ((rooms == null) ? 0 : rooms.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
        result = prime * result + ((broadcast.getId() == null) ? 0 : broadcast.getId().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;

        if (getClass() != obj.getClass())
            return false;
        BroadcastTag other = (BroadcastTag) obj;
        if (addedBy == null)
        {
            if (other.addedBy != null)
                return false;
        }
        else if (!addedBy.equals(other.addedBy))
            return false;
        if (addedTime == null)
        {
            if (other.addedTime != null)
                return false;
        }
        else if (!addedTime.equals(other.addedTime))
            return false;
        if (broadcastType != other.broadcastType)
            return false;
        if (city == null)
        {
            if (other.city != null)
                return false;
        }
        else if (!city.equals(other.city))
            return false;
        if (cityId == null)
        {
            if (other.cityId != null)
                return false;
        }
        else if (!cityId.equals(other.cityId))
            return false;
        if (commissionType != other.commissionType)
            return false;
        if (customSublocation == null)
        {
            if (other.customSublocation != null)
                return false;
        }
        else if (!customSublocation.equals(other.customSublocation))
            return false;
        if (draftedBy == null)
        {
            if (other.draftedBy != null)
                return false;
        }
        else if (!draftedBy.equals(other.draftedBy))
            return false;
        if (draftedTime == null)
        {
            if (other.draftedTime != null)
                return false;
        }
        else if (!draftedTime.equals(other.draftedTime))
            return false;
        if (location == null)
        {
            if (other.location != null)
                return false;
        }
        else if (!location.equals(other.location))
            return false;
        if (locationId == null)
        {
            if (other.locationId != null)
                return false;
        }
        else if (!locationId.equals(other.locationId))
            return false;
        if (maximumPrice == null)
        {
            if (other.maximumPrice != null)
                return false;
        }
        else if (!maximumPrice.equals(other.maximumPrice))
            return false;
        if (maximumSize == null)
        {
            if (other.maximumSize != null)
                return false;
        }
        else if (!maximumSize.equals(other.maximumSize))
            return false;
        if (minimumPrice == null)
        {
            if (other.minimumPrice != null)
                return false;
        }
        else if (!minimumPrice.equals(other.minimumPrice))
            return false;
        if (minimumSize == null)
        {
            if (other.minimumSize != null)
                return false;
        }
        else if (!minimumSize.equals(other.minimumSize))
            return false;
        if (price == null)
        {
            if (other.price != null)
                return false;
        }
        else if (!price.equals(other.price))
            return false;
        if (propertyMarket != other.propertyMarket)
            return false;
        if (propertySubType != other.propertySubType)
            return false;
        if (propertyType != other.propertyType)
            return false;
        if (rooms != other.rooms)
            return false;
        if (size == null)
        {
            if (other.size != null)
                return false;
        }
        else if (!size.equals(other.size))
            return false;
        if (status != other.status)
            return false;
        if (title == null)
        {
            if (other.title != null)
                return false;
        }
        else if (!title.equals(other.title))
            return false;
        if (transactionType != other.transactionType)
            return false;
        if (broadcast == null)
        {
            if (other.broadcast != null)
                return false;
        }
        else if (!broadcast.getId().equals(other.broadcast.getId()))
            return false;
        return true;
    }

}
