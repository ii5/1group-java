package one.group.entities.jpa;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import one.group.entities.jpa.listeners.EntryAuditEntityListener;

/**
 * 1. Can a developer be an agent or should a developer be associated with other
 * agents?
 * 
 * 
 */
@Entity
@EntityListeners(EntryAuditEntityListener.class)
@DiscriminatorValue(value = "DEVELOPER")
public class Developer extends Account
{

}
