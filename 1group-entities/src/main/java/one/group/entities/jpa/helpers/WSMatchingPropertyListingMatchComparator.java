package one.group.entities.jpa.helpers;

import java.util.Comparator;

import one.group.entities.api.response.WSMatchingPropertyListing;


public class WSMatchingPropertyListingMatchComparator implements Comparator<WSMatchingPropertyListing>
{
    public int compare(WSMatchingPropertyListing o1, WSMatchingPropertyListing o2)
    {
        if (o1 == null)
        {
            return 1;
        }
        
        if (o2 == null)
        {
            return -1;
        }
        
        if (o1.getMatch() == null || o1.getMatch().isEmpty())
        {
            return 1;
        }
        
        if (o2.getMatch() == null || o2.getMatch().isEmpty())
        {
            return -1;
        }
        
        if (o1.getMatch().equals(o2.getMatch()))
        {
            return o1.compareTo(o2);
        }
        
        return o1.getMatch().compareTo(o2.getMatch());
    }
}
