package one.group.entities.jpa.helpers;

import java.util.UUID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 
 * @author nyalfernandes
 * 
 */
@Converter
public class UUIDConverter implements AttributeConverter<String, String>
{
    public String convertToDatabaseColumn(final String arg)
    {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    public String convertToEntityAttribute(final String arg)
    {
        return arg;
    }
}
