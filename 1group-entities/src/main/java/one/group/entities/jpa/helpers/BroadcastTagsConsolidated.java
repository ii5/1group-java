package one.group.entities.jpa.helpers;

import java.util.Date;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.status.BroadcastStatus;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class BroadcastTagsConsolidated
{
    private String broadcastId;
    private String messageId;
    private BroadcastStatus broadcastStatus;
    private Date broadcastCreatedTime;
    private Date broadcastUpdatedTime;
    private String broadcastCreatedBy;
    private String broadcastUpdatedBy;

    private Date tagCreatedTime;
    private Date tagUpdatedTime;

    private String tagCreatedBy;
    private String tagUpdatedBy;

    private String title;
    private Long size;
    private PropertyTransactionType transactionType;
    private BroadcastType broadcastType;
    private CommissionType commissionType;
    private String customSublocation;
    private Long maximumPrice;
    private Long minimumPrice;
    private PropertyType propertyType;
    private Rooms rooms;
    private String locationId;
    private String locationName;
    private String locationDisplayName;
    private String cityId;
    private String cityName;
    private Long price;
    private BroadcastTagStatus status;
    private String draftedBy;
    private Date draftedTime;
    private String addedBy;
    private Date addedTime;
    private Long maximumSize;
    private Long minimumSize;
    private String id;
    private PropertySubType propertySubType;
    private PropertyMarket propertyMarket;

    private String closedBy;
    private Date closedTime;

    private String discardReason;

    public static final String QUERY_FIND_BROADCAST_IDS_BY_LOCATION_AND_CITY_LIST = "SELECT NEW one.group.entities.jpa.helpers.BroadcastTagsConsolidated(b.id, b.broadcast.id, b.broadcast.messageId, b.broadcast.status, "
            + "b.broadcast.createdTime, b.broadcast.createdBy, b.broadcast.updatedTime, b.broadcast.updatedBy, b.title, b.size, b.transactionType, b.broadcastType, b.commissionType, b.customSublocation, b.maximumPrice, "
            + "b.minimumPrice, b.propertyType, b.rooms, b.location.id, b.location.localityName,b.location.locationDisplayName, b.cityId, b.location.city.name, b.price, b.createdBy, b.updatedBy, b.createdTime, b.updatedTime,"
            + "b.status, b.draftedBy, b.draftedTime, b.addedBy, b.addedTime, b.maximumSize, b.minimumSize,b.propertySubType,b.propertyMarket,b.discardReason) "
            + "FROM BroadcastTag b WHERE b.location.id IN :locationIdList AND b.cityId IN :cityIdList";

    public static final String QUERY_FIND_ALL_BROADCAST_IDS = "SELECT NEW one.group.entities.jpa.helpers.BroadcastTagsConsolidated(b.id, b.broadcast.id, b.broadcast.messageId, b.broadcast.status, "
            + "b.broadcast.createdTime, b.broadcast.createdBy, b.broadcast.updatedTime, b.broadcast.updatedBy, b.title, b.size, b.transactionType, b.broadcastType, b.commissionType, b.customSublocation, b.maximumPrice, "
            + "b.minimumPrice, b.propertyType, b.rooms, b.location.id, b.location.localityName,b.location.locationDisplayName, b.cityId, b.location.city.name, b.price, b.createdBy, b.updatedBy, b.createdTime, b.updatedTime,"
            + "b.status, b.draftedBy, b.draftedTime, b.addedBy, b.addedTime, b.maximumSize, b.minimumSize,b.propertySubType,b.propertyMarket,b.discardReason) " + "FROM BroadcastTag b";

    public BroadcastTagsConsolidated(String id, String broadcastId, String messageId, BroadcastStatus broadcastStatus, Date broadcastCreatedTime, String broadcastCreatedBy, Date broadcastUpdatedTime,
            String broadcastUpdatedBy, String tagTitle, Long size, PropertyTransactionType transactionType, BroadcastType broadcastType, CommissionType commissionType, String customSubLocation,
            Long maximumPrice, Long minimumPrice, PropertyType propertyType, Rooms rooms, String locationId, String locationName, String locationDisplayName, String cityId, String cityName,
            Long price, String tagCreatedBy, String tagUpdatedBy, Date tagCreatedTime, Date tagUpdatedTime, BroadcastTagStatus status, String draftedBy, Date draftedTime, String addedBy,
            Date addedTime, Long maximumSize, Long minimumSize, PropertySubType propertySubType, PropertyMarket propertyMarket, String closedBy, Date closedTime, String discardReason)
    {
        this.id = id;
        this.broadcastId = broadcastId;
        this.messageId = messageId;
        this.broadcastStatus = broadcastStatus;
        this.broadcastCreatedTime = broadcastCreatedTime;
        this.broadcastCreatedBy = broadcastCreatedBy;
        this.broadcastUpdatedTime = broadcastUpdatedTime;
        this.broadcastUpdatedBy = broadcastUpdatedBy;
        this.cityId = cityId;
        this.title = tagTitle;
        this.size = size;
        this.transactionType = transactionType;
        this.broadcastType = broadcastType;
        this.commissionType = commissionType;
        this.customSublocation = customSubLocation;
        this.maximumPrice = maximumPrice;
        this.minimumPrice = minimumPrice;
        this.propertyType = propertyType;
        this.rooms = rooms;
        this.locationId = locationId;
        this.cityId = cityId;
        this.price = price;
        this.tagCreatedBy = tagCreatedBy;
        this.tagCreatedTime = tagCreatedTime;
        this.tagUpdatedBy = tagUpdatedBy;
        this.tagUpdatedTime = tagUpdatedTime;
        this.locationName = locationName;
        this.locationDisplayName = locationDisplayName;
        this.cityName = cityName;
        this.status = status;
        this.draftedBy = draftedBy;
        this.draftedTime = draftedTime;
        this.addedBy = addedBy;
        this.addedTime = addedTime;
        this.maximumSize = maximumSize;
        this.minimumSize = minimumSize;
        if (propertySubType != null)
        {
            this.propertySubType = propertySubType;
        }
        this.propertyMarket = propertyMarket;
        this.closedBy = closedBy;
        this.closedTime = closedTime;
        if (discardReason != null)
        {
            this.discardReason = discardReason;
        }
    }

    public String getClosedBy()
    {
        return closedBy;
    }

    public void setClosedBy(String closedBy)
    {
        this.closedBy = closedBy;
    }

    public Date getClosedTime()
    {
        return closedTime;
    }

    public void setClosedTime(Date closedTime)
    {
        this.closedTime = closedTime;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getMaximumSize()
    {
        return maximumSize;
    }

    public void setMaximumSize(Long maximumSize)
    {
        this.maximumSize = maximumSize;
    }

    public Long getMinimumSize()
    {
        return minimumSize;
    }

    public void setMinimumSize(Long minimumSize)
    {
        this.minimumSize = minimumSize;
    }

    public BroadcastTagStatus getStatus()
    {
        return status;
    }

    public void setStatus(BroadcastTagStatus status)
    {
        this.status = status;
    }

    public String getDraftedBy()
    {
        return draftedBy;
    }

    public void setDraftedBy(String draftedBy)
    {
        this.draftedBy = draftedBy;
    }

    public Date getDraftedTime()
    {
        return draftedTime;
    }

    public void setDraftedTime(Date draftedTime)
    {
        this.draftedTime = draftedTime;
    }

    public String getAddedBy()
    {
        return addedBy;
    }

    public void setAddedBy(String addedBy)
    {
        this.addedBy = addedBy;
    }

    public Date getAddedTime()
    {
        return addedTime;
    }

    public void setAddedTime(Date addedTime)
    {
        this.addedTime = addedTime;
    }

    public String getLocationName()
    {
        return locationName;
    }

    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    public String getCityName()
    {
        return cityName;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public String getTagCreatedBy()
    {
        return tagCreatedBy;
    }

    public Date getTagCreatedTime()
    {
        return tagCreatedTime;
    }

    public String getTagUpdatedBy()
    {
        return tagUpdatedBy;
    }

    public Date getTagUpdatedTime()
    {
        return tagUpdatedTime;
    }

    public void setTagCreatedBy(String tagCreatedBy)
    {
        this.tagCreatedBy = tagCreatedBy;
    }

    public void setTagCreatedTime(Date tagCreatedTime)
    {
        this.tagCreatedTime = tagCreatedTime;
    }

    public void setTagUpdatedBy(String tagUpdatedBy)
    {
        this.tagUpdatedBy = tagUpdatedBy;
    }

    public void setTagUpdatedTime(Date tagUpdatedTime)
    {
        this.tagUpdatedTime = tagUpdatedTime;
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getMessageId()
    {
        return messageId;
    }

    public void setMessageId(String messageId)
    {
        this.messageId = messageId;
    }

    public BroadcastStatus getBroadcastStatus()
    {
        return broadcastStatus;
    }

    public void setBroadcastStatus(BroadcastStatus broadcastStatus)
    {
        this.broadcastStatus = broadcastStatus;
    }

    public Date getBroadcastCreatedTime()
    {
        return broadcastCreatedTime;
    }

    public void setBroadcastCreatedTime(Date broadcastCreatedTime)
    {
        this.broadcastCreatedTime = broadcastCreatedTime;
    }

    public Date getBroadcastUpdatedTime()
    {
        return broadcastUpdatedTime;
    }

    public void setBroadcastUpdatedTime(Date broadcastUpdatedTime)
    {
        this.broadcastUpdatedTime = broadcastUpdatedTime;
    }

    public String getBroadcastCreatedBy()
    {
        return broadcastCreatedBy;
    }

    public void setBroadcastCreatedBy(String broadcastCreatedBy)
    {
        this.broadcastCreatedBy = broadcastCreatedBy;
    }

    public String getBroadcastUpdatedBy()
    {
        return broadcastUpdatedBy;
    }

    public void setBroadcastUpdatedBy(String broadcastUpdatedBy)
    {
        this.broadcastUpdatedBy = broadcastUpdatedBy;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getSize()
    {
        return size;
    }

    public void setSize(Long size)
    {
        this.size = size;
    }

    public PropertyTransactionType getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(PropertyTransactionType transactionType)
    {
        this.transactionType = transactionType;
    }

    public BroadcastType getBroadcastType()
    {
        return broadcastType;
    }

    public void setBroadcastType(BroadcastType broadcastType)
    {
        this.broadcastType = broadcastType;
    }

    public CommissionType getCommissionType()
    {
        return commissionType;
    }

    public void setCommissionType(CommissionType commissionType)
    {
        this.commissionType = commissionType;
    }

    public String getCustomSublocation()
    {
        return customSublocation;
    }

    public void setCustomSublocation(String customSublocation)
    {
        this.customSublocation = customSublocation;
    }

    public Long getMaximumPrice()
    {
        return maximumPrice;
    }

    public void setMaximumPrice(Long maximumPrice)
    {
        this.maximumPrice = maximumPrice;
    }

    public Long getMinimumPrice()
    {
        return minimumPrice;
    }

    public void setMinimumPrice(Long minimumPrice)
    {
        this.minimumPrice = minimumPrice;
    }

    public PropertyType getPropertyType()
    {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType)
    {
        this.propertyType = propertyType;
    }

    public Rooms getRooms()
    {
        return rooms;
    }

    public void setRooms(Rooms rooms)
    {
        this.rooms = rooms;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public String getCityId()
    {
        return cityId;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public Long getPrice()
    {
        return price;
    }

    public void setPrice(Long price)
    {
        this.price = price;
    }

    public PropertySubType getPropertySubType()
    {
        return propertySubType;
    }

    public void setPropertySubType(PropertySubType propertySubType)
    {
        this.propertySubType = propertySubType;
    }

    public String getLocationDisplayName()
    {
        return locationDisplayName;
    }

    public void setLocationDisplayName(String locationDisplayName)
    {
        this.locationDisplayName = locationDisplayName;
    }

    public PropertyMarket getPropertyMarket()
    {
        return propertyMarket;
    }

    public void setPropertyMarket(PropertyMarket propertyMarket)
    {
        this.propertyMarket = propertyMarket;
    }

    public String getDiscardReason()
    {
        return discardReason;
    }

    public void setDiscardReason(String discardReason)
    {
        this.discardReason = discardReason;
    }

}
