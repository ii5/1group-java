package one.group.entities.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import one.group.core.enums.MessageType;
import one.group.entities.jpa.listeners.EntryAuditEntityListener;

@Entity
@EntityListeners(EntryAuditEntityListener.class)
@NamedQueries({ @NamedQuery(name = ChatLog.QUERY_FIND_BY_CHATTHREADID_AND_CHATINDEX, query = "FROM ChatLog cl where cl.chatThreadId = :chatThreadId AND cl.index = :index") })
@Table(name = "chat_log")
public class ChatLog implements Serializable
{
    public static final String QUERY_FIND_BY_CHATTHREADID_AND_CHATINDEX = "findChatLogByChatThreadIdAndToIndex";

    @Id
    @Column(name = "chat_thread_id")
    private String chatThreadId;

    @Id
    @Column(name = "index")
    private Long index;

    @Column(name = "from_account_id")
    private String fromAccountId;

    @Column(name = "to_account_id")
    private String toAccountId;

    @Column(name = "client_sent_time")
    private Date clientSentTime;

    @Column(name = "server_time")
    private Date serverTime;

    @Column(name = "message_type")
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Column(name = "text", length = 4000, nullable = true)
    private String text;

    @Column(name = "property_listing_id", nullable = true)
    private String propertyListingId;

    @Column(name = "photo_full_location", nullable = true)
    private String photoFullLocation;

    public String getChatThreadId()
    {
        return chatThreadId;
    }

    public void setChatThreadId(String chatThreadId)
    {
        this.chatThreadId = chatThreadId;
    }

    public Long getIndex()
    {
        return index;
    }

    public void setIndex(Long index)
    {
        this.index = index;
    }

    public String getFromAccountId()
    {
        return fromAccountId;
    }

    public void setFromAccountId(String fromAccountId)
    {
        this.fromAccountId = fromAccountId;
    }

    public String getToAccountId()
    {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId)
    {
        this.toAccountId = toAccountId;
    }

    public Date getClientSentTime()
    {
        return clientSentTime;
    }

    public void setClientSentTime(Date clientSentTime)
    {
        this.clientSentTime = clientSentTime;
    }

    public Date getServerTime()
    {
        return serverTime;
    }

    public void setServerTime(Date serverTime)
    {
        this.serverTime = serverTime;
    }

    public MessageType getMessageType()
    {
        return messageType;
    }

    public void setMessageType(MessageType messageType)
    {
        this.messageType = messageType;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getPropertyListingId()
    {
        return propertyListingId;
    }

    public void setPropertyListingId(String propertyListingId)
    {
        this.propertyListingId = propertyListingId;
    }

    public String getPhotoFullLocation()
    {
        return photoFullLocation;
    }

    public void setPhotoFullLocation(String photoFullLocation)
    {
        this.photoFullLocation = photoFullLocation;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chatThreadId == null) ? 0 : chatThreadId.hashCode());
        result = prime * result + ((index == null) ? 0 : index.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChatLog other = (ChatLog) obj;
        if (chatThreadId == null)
        {
            if (other.chatThreadId != null)
                return false;
        }
        else if (!chatThreadId.equals(other.chatThreadId))
            return false;
        if (index == null)
        {
            if (other.index != null)
                return false;
        }
        else if (!index.equals(other.index))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "ChatLog [" + (chatThreadId != null ? "chatThreadId=" + chatThreadId + ", " : "") + (index != null ? "index=" + index + ", " : "")
                + (fromAccountId != null ? "fromAccountId=" + fromAccountId + ", " : "") + (toAccountId != null ? "toAccountId=" + toAccountId + ", " : "")
                + (clientSentTime != null ? "clientSentTime=" + clientSentTime + ", " : "") + (serverTime != null ? "serverTime=" + serverTime + ", " : "")
                + (messageType != null ? "messageType=" + messageType + ", " : "") + (text != null ? "text=" + text + ", " : "")
                + (propertyListingId != null ? "propertyListingId=" + propertyListingId + ", " : "") + (photoFullLocation != null ? "photoFullLocation=" + photoFullLocation : "") + "]";
    }

}
