package one.group.entities.socket;

import org.apache.solr.client.solrj.beans.Field;

public class ParticipantStatus extends AbstractSolrEntity
{
    private String participantId;

    private String participantStatus;

    public String getParticipantId()
    {
        return participantId;
    }

    @Field
    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public String getParticipantStatus()
    {
        return participantStatus;
    }

    @Field
    public void setParticipantStatus(String participantStatus)
    {
        this.participantStatus = participantStatus;
    }

}
