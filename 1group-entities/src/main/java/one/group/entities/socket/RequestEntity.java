package one.group.entities.socket;

import java.io.Serializable;

import one.group.entities.api.request.v2.WSRequest;



/**
 * 
 * @author nyalfernandes
 *
 */
public class RequestEntity extends WSRequest implements Serializable
{
	private static final long serialVersionUID = 1L;

	public RequestEntity()
    {
        
    }
    
    public RequestEntity(WSRequest request)
    {
        super(request);
    }
}
