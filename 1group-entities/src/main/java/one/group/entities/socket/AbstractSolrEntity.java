package one.group.entities.socket;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.apache.solr.client.solrj.beans.Field;

//import org.apache.solr.client.solrj.beans.Field;

/**
 * 
 * @author nyalfernandes
 *
 */
@MappedSuperclass
public abstract class AbstractSolrEntity implements ISolrEntity
{
    @Field
    @Id
    private String id;

    @Field
    @Column(name = "entity_type")
    private String entityType;

    public AbstractSolrEntity()
    {
        this.entityType = this.getClass().getSimpleName().toLowerCase();
    }

    public String getEntityType()
    {
        return entityType;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractSolrEntity other = (AbstractSolrEntity) obj;
        if (entityType == null)
        {
            if (other.entityType != null)
                return false;
        }
        else if (!entityType.equals(other.entityType))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "AbstractSolrEntity [id=" + id + ", entityType=" + entityType + "]";
    }
    
    
}
