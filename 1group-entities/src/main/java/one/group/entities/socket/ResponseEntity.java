package one.group.entities.socket;

import java.io.Serializable;

import one.group.entities.api.response.WSResponse;

/**
 * 
 * @author nyalfernandes
 *
 */
public class ResponseEntity extends WSResponse implements Serializable
{
	private static final long serialVersionUID = 1L;

	public ResponseEntity(WSResponse entity)
    {
        super(entity);
    }
    
    public ResponseEntity() {}
}
