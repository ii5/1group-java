package one.group.entities.socket;

import java.security.SecureRandom;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.solr.client.solrj.beans.Field;

import one.group.core.Constant;
import one.group.core.enums.BpoMessageStatus;
import one.group.core.enums.GroupStatus;
import one.group.core.enums.MessageSourceType;
import one.group.core.enums.MessageStatus;
import one.group.core.enums.MessageType;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
@MappedSuperclass
public class Message extends AbstractSolrEntity
{
	@Transient
    private Integer index;
    
    @Column(name = "broadcast_id")
    private String broadcastId;
    
    @Column(name = "created_by")
    private String createdBy;
    
    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;
    
    @Column(name = "group_id")
    private String groupId;
    
    @Column(name = "hash")
    private String hash;
    
    @Column(name = "updated_by")
    private String updatedBy;
    
    @Column(name = "updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;
    
    @Column(name = "message_sent_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date messageSentTime;
    
    @Column(name = "sender_phone_number")
    private String senderPhoneNumber;
    
    @Column(name = "text")
    private String text;
    
    @Transient
    private Date clientSentTime;
    
    @Column(name = "to_account_id")
    private String toAccountId;

    @Transient
    private Integer version;

    @Column(name = "bpo_status_update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bpoStatusUpdateTime;
    
    @Column(name = "bpo_status_update_by")
    private String bpoStatusUpdateBy;

    @Column(name = "snapshot_group_status")
    @Enumerated(EnumType.STRING)
    private GroupStatus snapShotGroupStatus;
    
    @Transient
    private String snapShotGroupStatus_s;

    @Column(name = "message_source")
    @Enumerated(EnumType.STRING)
    private MessageSourceType messageSource;
    
    @Transient
    private String messageSource_s;

    @Column(name = "message_type")
    @Enumerated(EnumType.STRING)
    private MessageType messageType;
    
    @Transient
    private String messageType_s;

    @Column(name = "message_status")
    @Enumerated(EnumType.STRING)
    private MessageStatus messageStatus;
    
    @Transient
    private String messageStatus_s;

    @Column(name = "bpo_message_status")
    @Enumerated(EnumType.STRING)
    private BpoMessageStatus bpoMessageStatus;
    
    @Transient
    private String bpoMessageStatus_s;

    @Column(name = "is_duplicate")
    private boolean isDuplicate;
    
    public Message() {};
    
    public Message(Message m)
    {
    	if (m == null) return;
    	this.setBpoMessageStatus(m.getBpoMessageStatus());
    	this.setBpoMessageStatus_s(m.getBpoMessageStatus_s());
    	this.setBpoStatusUpdateBy(m.getBpoStatusUpdateBy());
    	this.setBpoStatusUpdateTime(m.getBpoStatusUpdateTime());
    	this.setBroadcastId(m.getBroadcastId());
    	this.setClientSentTime(m.getClientSentTime());
    	this.setCreatedBy(m.getCreatedBy());
    	this.setCreatedTime(m.getCreatedTime());
    	this.setGroupId(m.getGroupId());
    	this.setHash(m.getHash());
    	if (this.getHash() == null)
    	{
    		this.setHash(m.getText());
    	}
    	this.setId(m.getId());
//    	this.id = m.getId();
    	this.setIndex(m.getIndex());
    	this.setIsDuplicate(m.getIsDuplicate());
    	this.setMessageSentTime(m.getMessageSentTime());
    	this.setMessageSource(m.getMessageSource());
    	this.setMessageSource_s(m.getMessageSource_s());
    	this.setMessageStatus(m.getMessageStatus());
    	this.setMessageStatus_s(m.getMessageStatus_s());
    	this.setMessageType(m.getMessageType());
    	this.setMessageType_s(m.getMessageType_s());
    	this.setSenderPhoneNumber(m.getSenderPhoneNumber());
    	this.setSnapShotGroupStatus(m.getSnapShotGroupStatus());
    	this.setSnapShotGroupStatus_s(m.getSnapShotGroupStatus_s());
    	this.setText(m.getText());
    	this.setToAccountId(m.getToAccountId());
    	this.setUpdatedBy(m.getUpdatedBy());
    	this.setUpdatedTime(m.getUpdatedTime());
    	this.setVersion(m.getVersion());
    }

    public String getSnapShotGroupStatus_s()
    {
        return snapShotGroupStatus_s;
    }

    @Field
    public void setSnapShotGroupStatus_s(String snapShotGroupStatus_s)
    {
    	if (snapShotGroupStatus_s == null) return;
        this.snapShotGroupStatus_s = snapShotGroupStatus_s;
        this.snapShotGroupStatus = GroupStatus.valueOf(snapShotGroupStatus_s);
    }

    public GroupStatus getSnapShotGroupStatus()
    {
        return snapShotGroupStatus;
    }

    public void setSnapShotGroupStatus(GroupStatus snapShotGroupStatus)
    {
    	if (snapShotGroupStatus == null) return;
        this.snapShotGroupStatus = snapShotGroupStatus;
        this.snapShotGroupStatus_s = snapShotGroupStatus.name();
    }

    public String getBpoStatusUpdateBy()
    {
        return bpoStatusUpdateBy;
    }

    @Field
    public void setBpoStatusUpdateBy(String bpoStatusUpdateBy)
    {
        this.bpoStatusUpdateBy = bpoStatusUpdateBy;
    }

    public Date getBpoStatusUpdateTime()
    {
        return bpoStatusUpdateTime;
    }

    @Field
    public void setBpoStatusUpdateTime(Date bpoStatusUpdateTime)
    {
        this.bpoStatusUpdateTime = bpoStatusUpdateTime;
    }

    @Field
    public void setIsDuplicate(boolean isDuplicate)
    {
        this.isDuplicate = isDuplicate;
    }

    public BpoMessageStatus getBpoMessageStatus()
    {
        return bpoMessageStatus;
    }

    public String getBpoMessageStatus_s()
    {
        return bpoMessageStatus_s;
    }

    public void setBpoMessageStatus(BpoMessageStatus bpoMessageStatus)
    {
    	if (bpoMessageStatus == null) return;
        this.bpoMessageStatus = bpoMessageStatus;
        this.bpoMessageStatus_s = bpoMessageStatus.name();
    }

    @Field
    public void setBpoMessageStatus_s(String bpoMessageStatus_s)
    {
    	if (bpoMessageStatus_s == null) return;
        this.bpoMessageStatus_s = bpoMessageStatus_s;
        this.bpoMessageStatus = BpoMessageStatus.valueOf(bpoMessageStatus_s);
    }

    public MessageStatus getMessageStatus()
    {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus)
    {
    	if (messageStatus == null) return;
        this.messageStatus = messageStatus;
        this.messageStatus_s = messageStatus.name();
    }

    public String getMessageStatus_s()
    {
        return messageStatus_s;
    }

    @Field
    public void setMessageStatus_s(String messageStatus_s)
    {
    	if (messageStatus_s == null) return;
        this.messageStatus_s = messageStatus_s;
        this.messageStatus = MessageStatus.valueOf(messageStatus_s);
    }

    public String getBroadcastId()
    {
        return broadcastId;
    }

    @Field
    public void setBroadcastId(String broadcastId)
    {
        this.broadcastId = broadcastId;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    @Field
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    @Field
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getGroupId()
    {
        return groupId;
    }

    @Field
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getHash()
    {
        return hash;
    }

    @Field
    public void setHash(String hash)
    {
        this.hash = hash;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    @Field
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    @Field
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Date getMessageSentTime()
    {
        return messageSentTime;
    }

    @Field
    public void setMessageSentTime(Date messageSentTime)
    {
        this.messageSentTime = messageSentTime;
    }

    public String getSenderPhoneNumber()
    {
        return senderPhoneNumber;
    }

    @Field
    public void setSenderPhoneNumber(String senderPhoneNumber)
    {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public MessageSourceType getMessageSource()
    {
        return messageSource;
    }

    public void setMessageSource(MessageSourceType messageSource)
    {
    	if (messageSource == null) return;
        this.messageSource = messageSource;
        this.messageSource_s = messageSource.name();
    }

    public String getMessageSource_s()
    {
        return messageSource_s;
    }

    @Field
    public void setMessageSource_s(String messageSource_s)
    {
    	if (messageSource_s == null) return;
        this.messageSource_s = messageSource_s;
        this.messageSource = MessageSourceType.valueOf(messageSource_s);
    }

    public String getText()
    {
        return text;
    }

    @Field
    public void setText(String text)
    {
        this.text = text;
    }

    public MessageType getMessageType()
    {
        return messageType;
    }

    public void setMessageType(MessageType messageType)
    {
    	if (messageType == null) return;
        this.messageType = messageType;
        this.messageType_s = messageType.name();
    }

    public String getMessageType_s()
    {
        return messageType_s;
    }

    @Field
    public void setMessageType_s(String messageType_s)
    {
    	if (messageType_s == null) return;
        this.messageType_s = messageType_s;
        this.messageType = MessageType.valueOf(messageType_s);

    }

    public Integer getIndex()
    {
        return index;
    }

    @Field
    public void setIndex(Integer index)
    {
        this.index = index;
    }

    public Date getClientSentTime()
    {
        return clientSentTime;
    }

    @Field
    public void setClientSentTime(Date clientSentTime)
    {
        this.clientSentTime = clientSentTime;
    }

    public boolean getIsDuplicate()
    {
        return this.isDuplicate;
    }

    public Integer getVersion()
    {
        return version;
    }

    @Field
    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getToAccountId()
    {
        return toAccountId;
    }

    @Field
    public void setToAccountId(String toAccountId)
    {
        this.toAccountId = toAccountId;
    }

    @Override
    public String toString()
    {
        return "Message [index=" + index + ", broadcastId=" + broadcastId + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", groupId=" + groupId + ", hash=" + hash + ", updatedBy="
                + updatedBy + ", updatedTime=" + updatedTime + ", messageSentTime=" + messageSentTime + ", senderPhoneNumber=" + senderPhoneNumber + ", text=" + text + ", clientSentTime="
                + clientSentTime + ", toAccountId=" + toAccountId + ", version=" + version + ", bpoStatusUpdateTime=" + bpoStatusUpdateTime + ", bpoStatusUpdateBy=" + bpoStatusUpdateBy
                + ", snapShotGroupStatus=" + snapShotGroupStatus + ", snapShotGroupStatus_s=" + snapShotGroupStatus_s + ", messageSource=" + messageSource + ", messageSource_s=" + messageSource_s
                + ", messageType=" + messageType + ", messageType_s=" + messageType_s + ", messageStatus=" + messageStatus + ", messageStatus_s=" + messageStatus_s + ", bpoMessageStatus="
                + bpoMessageStatus + ", bpoMessageStatus_s=" + bpoMessageStatus_s + ", isDuplicate=" + isDuplicate + ", getEntityType()=" + getEntityType() + ", getId()=" + getId() + "]";
    }

    public static Message dummyData(String groupId)
    {
        Message m = new Message();

        m.setId(Utils.randomUUID());
        m.setBpoMessageStatus(BpoMessageStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % BpoMessageStatus.values().length]);
        m.setBroadcastId(Utils.randomUUID());
        m.setClientSentTime(new Date());
        m.setCreatedBy(Utils.randomUUID());
        m.setCreatedTime(new Date());
        m.setGroupId(groupId);
        m.setHash(Utils.randomUUID());
        m.setIndex(new SecureRandom().nextInt());
        m.setIsDuplicate(new SecureRandom().nextBoolean());
        m.setMessageSentTime(new Date());
        m.setMessageSource(MessageSourceType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % MessageSourceType.values().length]);
        m.setMessageStatus(MessageStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % MessageStatus.values().length]);
        m.setMessageType(MessageType.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % MessageType.values().length]);
        m.setSenderPhoneNumber("+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, false));
        m.setText("Message text : " + m.getId());
        m.setUpdatedBy(Utils.randomUUID());
        m.setUpdatedTime(new Date());

        return m;
    }

}
