package one.group.entities.socket;

import java.security.SecureRandom;
import java.util.Date;

import one.group.core.Constant;
import one.group.core.enums.GroupStatus;
import one.group.utils.Utils;

import org.apache.solr.client.solrj.beans.Field;

public class Group extends AbstractSolrEntity
{
    private int duplicateCount;
    private String notes;
    private Date latestMessageTimestamp;
    private boolean everSync;
    private int unreadmsgCount;
    private int invalidExpiredCount;
    private String mediaId;
    private String createdMobileNo;
    private Date releaseTime;
    private Date createdTime;
    private int unreadmsgCountQa;
    private String groupName;
    private int totalmsgCount;
    private String createdBy;

    private GroupStatus status;
    private String status_s;

    public int getDuplicateCount()
    {
        return duplicateCount;
    }

    @Field
    public void setDuplicateCount(int duplicateCount)
    {
        this.duplicateCount = duplicateCount;
    }

    public String getNotes()
    {
        return notes;
    }

    @Field
    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public Date getLatestMessageTimestamp()
    {
        return latestMessageTimestamp;
    }

    @Field
    public void setLatestMessageTimestamp(Date latestMessageTimestamp)
    {
        this.latestMessageTimestamp = latestMessageTimestamp;
    }

    public boolean getEverSync()
    {
        return everSync;
    }

    @Field
    public void setEverSync(boolean everSync)
    {
        this.everSync = everSync;
    }

    public int getUnreadmsgCount()
    {
        return unreadmsgCount;
    }

    @Field
    public void setUnreadmsgCount(int unreadmsgCount)
    {
        this.unreadmsgCount = unreadmsgCount;
    }

    public int getInvalidExpiredCount()
    {
        return invalidExpiredCount;
    }

    @Field
    public void setInvalidExpiredCount(int invalidExpiredCount)
    {
        this.invalidExpiredCount = invalidExpiredCount;
    }

    public String getMediaId()
    {
        return mediaId;
    }

    @Field
    public void setMediaId(String mediaId)
    {
        this.mediaId = mediaId;
    }

    public String getCreatedMoibleNo()
    {
        return createdMobileNo;
    }

    @Field
    public void setCreatedMobileNo(String createdMobileNo)
    {
        this.createdMobileNo = createdMobileNo;
    }

    public Date getReleaseTime()
    {
        return releaseTime;
    }

    @Field
    public void setReleaseTime(Date releaseTime)
    {
        this.releaseTime = releaseTime;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    @Field
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public int getUnreadmsgCountQa()
    {
        return unreadmsgCountQa;
    }

    @Field
    public void setUnreadmsgCountQa(int unreadmsgCountQa)
    {
        this.unreadmsgCountQa = unreadmsgCountQa;
    }

    public String getGroupName()
    {
        return groupName;
    }

    @Field
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public int getTotalmsgCount()
    {
        return totalmsgCount;
    }

    @Field
    public void setTotalmsgCount(int totalmsgCount)
    {
        this.totalmsgCount = totalmsgCount;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    @Field
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public GroupStatus getStatus()
    {
        return status;
    }

    @Field
    public void setStatus(GroupStatus status)
    {
        this.status = status;
        this.status_s = status.toString();
    }
    
    public String getStatus_s()
    {
        return status_s;
    }
    
    public void setStatus_s(String status_s)
    {
        this.status_s = status_s;
        this.status = GroupStatus.parse(status_s);
    }

    public static Group dummyData()
    {
        Group g = new Group();
        
        g.setCreatedBy(Utils.randomUUID());
        g.setCreatedMobileNo("+91" + Utils.randomString(Constant.CHARSET_NUMERIC_WITHOUT_ZERO, 10, true));
        g.setCreatedTime(new Date());
        g.setDuplicateCount(new SecureRandom().nextInt());
        g.setEverSync(new SecureRandom().nextBoolean());
        g.setGroupName(Utils.generateAccountReference());
        g.setId(Utils.randomUUID());
        g.setInvalidExpiredCount(new SecureRandom().nextInt());
        g.setLatestMessageTimestamp(new Date());
        g.setMediaId(Utils.randomUUID());
        g.setNotes("Some notes");
        g.setReleaseTime(new Date());
        g.setStatus(GroupStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % GroupStatus.values().length]);
        g.setTotalmsgCount(new SecureRandom().nextInt());
        g.setUnreadmsgCount(new SecureRandom().nextInt());
        g.setUnreadmsgCountQa(new SecureRandom().nextInt());
        
        return g;
    }
}
