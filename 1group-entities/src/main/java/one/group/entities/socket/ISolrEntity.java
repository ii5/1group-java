package one.group.entities.socket;


/**
 * 
 * @author nyalfernandes
 *
 */
public interface ISolrEntity
{   
    public String getId();
}
