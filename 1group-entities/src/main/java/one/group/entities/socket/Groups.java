package one.group.entities.socket;

import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.Constant;
import one.group.core.enums.GroupSource;
import one.group.core.enums.GroupStatus;
import one.group.utils.Utils;

import org.apache.solr.client.solrj.beans.Field;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class Groups extends AbstractSolrEntity
{
    private String groupName;
    private String creatorMobileNo;
    private String createdBy;
    private Date createdTime;
    private String cityId;
    private String locationId;
    private GroupStatus status;
    private String status_s;
    private String mediaId;
    private String assignedTo;
    private Boolean everSync;
    private Date unreadFrom;
    private Integer unreadmsgCount;
    private Integer totalmsgCount;
    private Integer unreadmsgCountQa;
    private Date assignedTime;
    private Date releaseTime;
    private Date statusUpdateTime;
    private Integer invalidExpiredCount;
    private Integer validExpiredCount;
    private Integer duplicateCount;
    private Date latestMessageTimestamp;
    private Date permitSince;
    private GroupSource source;
    private String source_s;
    private String notes;
    private Date updatedTime;
    private String updatedBy;
    private String bpoUpdatedBy;
    private Date bpoUpdatedTime;
    private List<String> participants;
    private List<String> participantsStatus;
    private Integer readmsgCount;
    private Integer unqualifiedmsgCount;

    private Map<String, GroupStatus> participantsStatusMap;
    
    public Integer getUnqualifiedmsgCount()
    {
        return unqualifiedmsgCount;
    }
    
    public Integer getReadmsgCount()
    {
        return readmsgCount;
    }
    
    public void incrementUnqualifiedmshCount()
    {
        if (unqualifiedmsgCount == null)
        {
            unqualifiedmsgCount = 0;
        }
        
        unqualifiedmsgCount++;
    }
    
    public void setUnqualifiedmsgCount(Integer unqualifiedmsgCount)
    {
        this.unqualifiedmsgCount = unqualifiedmsgCount;
    }
    
    public void incrementReadmsgCount()
    {
        if (readmsgCount == null)
        {
            readmsgCount = 0;
        }
        
        readmsgCount++;
    }
    
    public void incrementUnReadmsgCount()
    {
        if (unreadmsgCount == null)
        {
        	unreadmsgCount = 0;
        }
        
        unreadmsgCount++;
    }
    
    public void setReadmsgCount(Integer readmsgCount)
    {
        this.readmsgCount = readmsgCount;
    }

    public Map<String, GroupStatus> getParticipantsStatusMap()
    {
        return participantsStatusMap;
    }

    public void setParticipantsStatusMap(Map<String, GroupStatus> participantsStatusMap)
    {
        this.participantsStatusMap = participantsStatusMap;
    }

    public List<String> getParticipantsStatus()
    {
        return participantsStatus;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public String getCreatorMobileNo()
    {
        return creatorMobileNo;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }

    public String getCityId()
    {
        return cityId;
    }

    public String getLocationId()
    {
        return locationId;
    }

    public GroupStatus getStatus()
    {
        return status;
    }

    public String getMediaId()
    {
        return mediaId;
    }

    public String getAssignedTo()
    {
        return assignedTo;
    }

    public Boolean isEverSync()
    {
        return everSync;
    }

    public Date getUnreadFrom()
    {
        return unreadFrom;
    }

    public Integer getUnreadmsgCount()
    {
        return unreadmsgCount;
    }

    public Integer getTotalmsgCount()
    {
        return totalmsgCount;
    }

    public Integer getUnreadmsgCountQa()
    {
        return unreadmsgCountQa;
    }

    public Date getAssignedTime()
    {
        return assignedTime;
    }

    public Date getReleaseTime()
    {
        return releaseTime;
    }

    public Date getStatusUpdateTime()
    {
        return statusUpdateTime;
    }

    public Integer getInvalidExpiredCount()
    {
        return invalidExpiredCount;
    }

    public Integer getValidExpiredCount()
    {
        return validExpiredCount;
    }

    public Integer getDuplicateCount()
    {
        return duplicateCount;
    }

    public Date getLatestMessageTimestamp()
    {
        return latestMessageTimestamp;
    }

    public Date getPermitSince()
    {
        return permitSince;
    }

    public GroupSource getSource()
    {
        return source;
    }

    public String getNotes()
    {
        return notes;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public String getSource_s()
    {
        return source_s;
    }

    public String getStatus_s()
    {
        return status_s;
    }

    public Boolean getEverSync()
    {
        return everSync;
    }

    public String getBpoUpdatedBy()
    {
        return bpoUpdatedBy;
    }

    public Date getBpoUpdatedTime()
    {
        return bpoUpdatedTime;
    }

    public List<String> getParticipants()
    {
        return participants;
    }

    @Field
    public void setBpoUpdatedBy(String bpoUpdatedBy)
    {
        this.bpoUpdatedBy = bpoUpdatedBy;
    }

    @Field
    public void setBpoUpdatedTime(Date bpoUpdatedTime)
    {
        this.bpoUpdatedTime = bpoUpdatedTime;
    }

    @Field
    public void setSource_s(String source_s)
    {
        this.source = GroupSource.valueOf(source_s);
        this.source_s = source.name();
    }

    @Field
    public void setStatus_s(String status_s)
    {
        this.status = GroupStatus.valueOf(status_s);
        this.status_s = status.name();
    }

    @Field
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    @Field
    public void setCreatorMobileNo(String creatorMobileNo)
    {
        this.creatorMobileNo = creatorMobileNo;
    }

    @Field
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Field
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    @Field
    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    @Field
    public void setLocationId(String locationId)
    {
        this.locationId = locationId;
    }

    public void setStatus(GroupStatus status)
    {
        this.status = status;
        if (status != null)
        {
            this.status_s = status.name();
        }
    }

    @Field
    public void setMediaId(String mediaId)
    {
        this.mediaId = mediaId;
    }

    @Field
    public void setAssignedTo(String assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    @Field
    public void setEverSync(Boolean everSync)
    {
        this.everSync = everSync;
    }

    @Field
    public void setUnreadFrom(Date unreadFrom)
    {
        this.unreadFrom = unreadFrom;
    }

    @Field
    public void setUnreadmsgCount(Integer unreadmsgCount)
    {
        this.unreadmsgCount = unreadmsgCount;
    }

    @Field
    public void setTotalmsgCount(Integer totalmsgCount)
    {
        this.totalmsgCount = totalmsgCount;
    }

    @Field
    public void setUnreadmsgCountQa(Integer unreadmsgCountQa)
    {
        this.unreadmsgCountQa = unreadmsgCountQa;
    }

    @Field
    public void setAssignedTime(Date assignedTime)
    {
        this.assignedTime = assignedTime;
    }

    @Field
    public void setReleaseTime(Date releaseTime)
    {
        this.releaseTime = releaseTime;
    }

    @Field
    public void setStatusUpdateTime(Date statusUpdateTime)
    {
        this.statusUpdateTime = statusUpdateTime;
    }

    @Field
    public void setInvalidExpiredCount(Integer invalidExpiredCount)
    {
        this.invalidExpiredCount = invalidExpiredCount;
    }

    @Field
    public void setValidExpiredCount(Integer validExpiredCount)
    {
        this.validExpiredCount = validExpiredCount;
    }

    @Field
    public void setDuplicateCount(Integer duplicateCount)
    {
        this.duplicateCount = duplicateCount;
    }

    @Field
    public void setLatestMessageTimestamp(Date latestMessageTimestamp)
    {
        this.latestMessageTimestamp = latestMessageTimestamp;
    }

    @Field
    public void setPermitSince(Date permitSince)
    {
        this.permitSince = permitSince;
    }

    public void setSource(GroupSource source)
    {
        this.source = source;
        if (source != null)
        {
            this.source_s = source.name();
        }
    }

    @Field
    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    @Field
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    @Field
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Field
    public void setParticipants(List<String> participants)
    {
        this.participants = participants;
    }

    @Field
    public void setParticipantsStatus(List<String> participantsStatus)
    {
        Map<String, GroupStatus> groupStatusOfParticipants = new HashMap<String, GroupStatus>();
        if (participantsStatus != null)
        {
            for (String status : participantsStatus)
            {
                String[] splitString = status.split(":");
                GroupStatus groupStatus = GroupStatus.convert(splitString[1].toLowerCase());
                groupStatusOfParticipants.put(splitString[0], groupStatus);
            }
        }
        this.participantsStatusMap = groupStatusOfParticipants;
        this.participantsStatus = participantsStatus;
    }

    public static Groups dummyData()
    {
        Groups g = new Groups();

        g.setCreatedBy(Utils.randomUUID());
        g.setCreatedTime(new Date());
        g.setDuplicateCount(new SecureRandom().nextInt());
        g.setEverSync(new SecureRandom().nextBoolean());
        g.setGroupName(Utils.generateAccountReference());
        g.setId(Utils.randomUUID());
        g.setInvalidExpiredCount(new SecureRandom().nextInt());
        g.setLatestMessageTimestamp(new Date());
        g.setMediaId(Utils.randomUUID());
        g.setNotes("Some notes");
        g.setReleaseTime(new Date());
        g.setStatus(GroupStatus.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % GroupStatus.values().length]);
        g.setSource(GroupSource.values()[Integer.parseInt(Utils.randomString(Constant.CHARSET_NUMERIC, 2, true)) % GroupSource.values().length]);
        g.setTotalmsgCount(new SecureRandom().nextInt());
        g.setUnreadmsgCount(new SecureRandom().nextInt());
        g.setUnreadmsgCountQa(new SecureRandom().nextInt());

        return g;
    }

    @Override
    public String toString()
    {
        return "Groups [groupName=" + groupName + ", creatorMobileNo=" + creatorMobileNo + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", cityId=" + cityId + ", locationId="
                + locationId + ", status=" + status + ", status_s=" + status_s + ", mediaId=" + mediaId + ", assignedTo=" + assignedTo + ", everSync=" + everSync + ", unreadFrom=" + unreadFrom
                + ", unreadmsgCount=" + unreadmsgCount + ", totalmsgCount=" + totalmsgCount + ", unreadmsgCountQa=" + unreadmsgCountQa + ", assignedTime=" + assignedTime + ", releaseTime="
                + releaseTime + ", statusUpdateTime=" + statusUpdateTime + ", invalidExpiredCount=" + invalidExpiredCount + ", validExpiredCount=" + validExpiredCount + ", duplicateCount="
                + duplicateCount + ", latestMessageTimestamp=" + latestMessageTimestamp + ", permitSince=" + permitSince + ", source=" + source + ", source_s=" + source_s + ", notes=" + notes
                + ", updatedTime=" + updatedTime + ", updatedBy=" + updatedBy + ", bpoUpdatedBy=" + bpoUpdatedBy + ", bpoUpdatedTime=" + bpoUpdatedTime + ", participants=" + participants
                + ", participantsStatus=" + participantsStatus + ", participantsStatusMap=" + participantsStatusMap + "]";
    }

    public static void main(String[] args) throws Exception
    {
        Groups g = new Groups();
        for (java.lang.reflect.Field f : g.getClass().getDeclaredFields())
        {
            
        }
    }
}
