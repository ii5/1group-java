package one.group.entities.socket;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.HTTPRequestMethodType;
import one.group.core.enums.HttpHeaderType;
import one.group.core.enums.PathParamType;
import one.group.core.enums.QueryParamType;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.StoreKey;
import one.group.entities.api.request.v2.WSBroadcast;
import one.group.entities.api.request.v2.WSRequest;
import one.group.entities.api.response.WSResponse;
import one.group.entities.api.response.WSResult;
import one.group.entities.api.response.v2.WSSyncResult;
import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.utils.Utils;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class SocketEntity implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String endpoint;
    private HTTPRequestMethodType requestType;
    private HashMap<HttpHeaderType, String> headers = new HashMap<HttpHeaderType, String>();
    private HashMap<QueryParamType, String> queryParams = new HashMap<QueryParamType, String>();
    private RequestEntity request;
    private ResponseEntity response;
    private HashMap<SortField, SortType> sort = new HashMap<SortField, SortType>();
    private HashMap<FilterField, Set<String>> filter = new HashMap<FilterField, Set<String>>();
    private String requestHash = null;
    
    private Long clientRequestSentTime;
    private Long serverRequestReceivedTime;
    private Long serverRequestProcessingTime;
    private Long serverResponseTransferTime;
    private Long clientResponseReceivedTime;
    private Long clientResponseProcessingTime;

    // Internal
    private HashMap<StoreKey, Object> store = new HashMap<StoreKey, Object>();
    private HashMap<PathParamType, String> pathParams = new HashMap<PathParamType, String>();
    private String socketId;

    
    
    public Long getClientRequestSentTime() 
    {
		return clientRequestSentTime;
	}

	public void setClientRequestSentTime(Long clientRequestSentTime) 
	{
		this.clientRequestSentTime = clientRequestSentTime;
	}

	public Long getServerRequestReceivedTime() 
	{
		return serverRequestReceivedTime;
	}

	public void setServerRequestReceivedTime(Long serverRequestReceivedTime) 
	{
		this.serverRequestReceivedTime = serverRequestReceivedTime;
	}

	public Long getServerRequestProcessingTime() 
	{
		return serverRequestProcessingTime;
	}

	public void setServerRequestProcessingTime(Long serverRequestProcessingTime) 
	{
		this.serverRequestProcessingTime = serverRequestProcessingTime;
	}

	public Long getServerResponseTransferTime() 
	{
		return serverResponseTransferTime;
	}

	public void setServerResponseTransferTime(Long serverResponseTransferTime) 
	{
		this.serverResponseTransferTime = serverResponseTransferTime;
	}

	public Long getClientResponseReceivedTime() 
	{
		return clientResponseReceivedTime;
	}

	public void setClientResponseReceivedTime(Long clientResponseReceivedTime) 
	{
		this.clientResponseReceivedTime = clientResponseReceivedTime;
	}

	public Long getClientResponseProcessingTime() 
	{
		return clientResponseProcessingTime;
	}

	public void setClientResponseProcessingTime(Long clientResponseProcessingTime) 
	{
		this.clientResponseProcessingTime = clientResponseProcessingTime;
	}

	public HashMap<FilterField, Set<String>> getFilter()
    {
        return filter;
    }

    public void setFilter(HashMap<FilterField, Set<String>> filter)
    {
        this.filter = filter;
    }

    public HashMap<SortField, SortType> getSort()
    {
        return sort;
    }

    public void setSort(HashMap<SortField, SortType> sort)
    {
        this.sort = sort;
    }

    public void addSort(SortField field, SortType type)
    {
        this.sort.put(field, type);
    }

    public void addFilter(FilterField field, String name)
    {
        Set<String> filterNames = filter.get(field);

        if (filterNames == null)
        {
            filterNames = new HashSet<String>();
            filter.put(field, filterNames);
        }

        filterNames.add(name);
    }

    public Set<String> getFilterForField(FilterField field)
    {
        return filter.get(field);
    }

    public SortType getSortForField(SortField field)
    {
        return sort.get(field);
    }

    public String getRequestHash()
    {
        return requestHash;
    }

    public void setRequestHash(String requestHash)
    {
        this.requestHash = requestHash;
    }

    public String getEndpoint()
    {
        return endpoint;
    }

    public void setEndpoint(String endpoint)
    {
        this.endpoint = endpoint;
    }

    public HTTPRequestMethodType getRequestType()
    {
        return requestType;
    }

    public void setRequestType(HTTPRequestMethodType requestType)
    {
        this.requestType = requestType;
    }

    public HashMap<HttpHeaderType, String> getHeaders()
    {
        return headers;
    }

    public String getHeader(HttpHeaderType header)
    {
        return headers.get(header);
    }

    public void setHeaders(HashMap<HttpHeaderType, String> headers)
    {
        this.headers = headers;
    }

    public HashMap<QueryParamType, String> getQueryParams()
    {
        return queryParams;
    }

    public String getQueryParam(QueryParamType paramType)
    {
        return queryParams.get(paramType);
    }

    public void setQueryParams(HashMap<QueryParamType, String> queryParams)
    {
        this.queryParams = queryParams;
    }

    public RequestEntity getRequest()
    {
        return request;
    }

    public void setRequest(RequestEntity request)
    {
        this.request = request;
    }

    public void setWithWSRequest(WSRequest request)
    {
        this.request = new RequestEntity(request);
    }

    public ResponseEntity getResponse()
    {
        return response;
    }

    public void setResponse(ResponseEntity response)
    {
        this.response = response;
    }

    public void setWithWSResponse(WSResponse response)
    {
        this.response = new ResponseEntity(response);
    }

    public HashMap<StoreKey, Object> getStore()
    {
        return store;
    }

    public Object getFromStore(StoreKey key)
    {
        if (store == null)
        {
            return null;
        }
        return this.store.get(key);
    }

    public void addToStore(StoreKey key, Object value)
    {
        this.store.put(key, value);
    }

    public void setStore(HashMap<StoreKey, Object> store)
    {
        this.store = store;
    }

    public HashMap<PathParamType, String> getPathParams()
    {
        return pathParams;
    }

    public String getPathParam(PathParamType pathParam)
    {
        return pathParams.get(pathParam);
    }

    public void setPathParams(HashMap<PathParamType, String> pathParams)
    {
        this.pathParams = pathParams;
    }

    public void addPathParam(PathParamType pathParamType, String value)
    {
        this.pathParams.put(pathParamType, value);
    }

    public String getSocketId()
    {
        return socketId;
    }

    public void setSocketId(String socketId)
    {
        this.socketId = socketId;
    }

    public SocketEntity()
    {
    };

    public SocketEntity(WSSyncUpdate update)
    {
        WSSyncResult result = new WSSyncResult();
        result.setSyncIndex(0);
        result.setCurrentApiVersion(null);
        result.setClientUpdateType(null);
        result.addUpdate(update);

        WSResult wsresult = new WSResult("200", "Standalone sync Found!", false);
        WSResponse response = new WSResponse(wsresult);
        response.setData(result);

        this.setEndpoint("/sync");
        this.setRequestType(HTTPRequestMethodType.POST);
        this.setWithWSResponse(response);

    }

    public SocketEntity(List<WSSyncUpdate> updateList)
    {
        WSSyncResult result = new WSSyncResult();
        result.setSyncIndex(0);
        result.setCurrentApiVersion(null);
        result.setClientUpdateType(null);
        result.setUpdates(updateList);

        WSResult wsresult = new WSResult("200", "Standalone sync Found!", false);
        WSResponse response = new WSResponse(wsresult);
        response.setData(result);

        this.setEndpoint("/sync");
        this.setRequestType(HTTPRequestMethodType.POST);
        this.setWithWSResponse(response);

    }
    
    public void setRelevantResponseData(SocketEntity source)
    {
    	this.setEndpoint(source.getEndpoint());
    	this.setHeaders(source.getHeaders());
    	this.setFilter(source.getFilter());
    	this.setPathParams(source.getPathParams());
    	this.setQueryParams(source.getQueryParams());
    	this.setRequest(source.getRequest());
    	this.setRequestHash(source.getRequestHash());
    	this.setRequestType(source.getRequestType());
    	this.setResponse(source.getResponse());
    	this.setSort(source.getSort());
    	
    	this.setClientRequestSentTime(source.getClientRequestSentTime());
    	this.setServerRequestReceivedTime(source.getServerRequestReceivedTime());
    	this.setServerRequestProcessingTime(source.getServerRequestProcessingTime());
    	this.setServerResponseTransferTime(source.getServerResponseTransferTime());
    	this.setClientResponseReceivedTime(source.getClientResponseReceivedTime());
    	this.setClientResponseProcessingTime(source.getClientResponseProcessingTime());
    }

    public static void main(String[] args)
    {
        HashMap<HttpHeaderType, String> headers = new HashMap<HttpHeaderType, String>();
        HashMap<QueryParamType, String> queryParamMap = new HashMap<QueryParamType, String>();
        RequestEntity request = new RequestEntity();
        ResponseEntity response = new ResponseEntity();

        WSBroadcast broadcast = new WSBroadcast();
        /*
         * broadcast.setArea("1234542");
         * broadcast.setDescription("Good sea side facing property.");
         * broadcast.setLocationId("998291"); broadcast.setRooms("5bhk");
         */
        // request.setBroadcast(broadcast);

        headers.put(HttpHeaderType.AUTHORISATION, "bearer 30e232c7-cde2-4342-83ff-d7b752c4a806");

        queryParamMap.put(QueryParamType.PAGE_NO, "2");
        queryParamMap.put(QueryParamType.OFFSET, "2");

        SocketEntity se = new SocketEntity();
        se.setEndpoint("/accounts/2323432423");
        se.setRequestType(HTTPRequestMethodType.POST);
        se.setRequest(request);
        se.setHeaders(headers);
        se.setQueryParams(queryParamMap);

        System.out.println(Utils.getJsonString(se));
    }

	@Override
	public String toString() {
		return "SocketEntity [" + (endpoint != null ? "endpoint=" + endpoint + ", " : "")
				+ (requestType != null ? "requestType=" + requestType + ", " : "")
				+ (headers != null ? "headers=" + headers + ", " : "")
				+ (queryParams != null ? "queryParams=" + queryParams + ", " : "")
				+ (request != null ? "request=" + request + ", " : "")
				+ (response != null ? "response=" + response + ", " : "") + (sort != null ? "sort=" + sort + ", " : "")
				+ (filter != null ? "filter=" + filter + ", " : "")
				+ (requestHash != null ? "requestHash=" + requestHash + ", " : "")
				+ (clientRequestSentTime != null ? "clientRequestSentTime=" + clientRequestSentTime + ", " : "")
				+ (serverRequestReceivedTime != null ? "serverRequestReceivedTime=" + serverRequestReceivedTime + ", "
						: "")
				+ (serverRequestProcessingTime != null
						? "serverRequestProcessingTime=" + serverRequestProcessingTime + ", " : "")
				+ (serverResponseTransferTime != null
						? "serverResponseTransferTime=" + serverResponseTransferTime + ", " : "")
				+ (clientResponseReceivedTime != null
						? "clientResponseReceivedTime=" + clientResponseReceivedTime + ", " : "")
				+ (clientResponseProcessingTime != null
						? "clientResponseProcessingTime=" + clientResponseProcessingTime + ", " : "")
				+ (store != null ? "store=" + store + ", " : "")
				+ (pathParams != null ? "pathParams=" + pathParams + ", " : "")
				+ (socketId != null ? "socketId=" + socketId : "") + "]";
	}
    
    

}
