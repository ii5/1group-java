package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.SyncUpdateWriteCursors;
import one.group.jpa.dao.SyncUpdateWriteCursorsJpaDAO;
import one.group.utils.validation.Validation;

public class SyncUpdateWriteCursorsJpaDAOImpl extends JPABaseDAOImpl<String, SyncUpdateWriteCursors> implements SyncUpdateWriteCursorsJpaDAO
{
    public SyncUpdateWriteCursors fetchByAccount(String accountId)
    {
        Validation.notNull(accountId, "Account ID should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findByNamedQuery(SyncUpdateWriteCursors.QUERY_FIND_BY_ACCOUNT_ID, parameters);
    }
    
    public void addSyncLogWriteCursors(SyncUpdateWriteCursors writeCursor)
    {
        Validation.notNull(writeCursor, "The Write cursor entity passsed should not be null.");
        persist(writeCursor);
    }

}
