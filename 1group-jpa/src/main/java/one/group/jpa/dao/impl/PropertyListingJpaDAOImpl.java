package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.helpers.PropertyListingCountObject;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.jpa.dao.AmenityJpaDAO;
import one.group.jpa.dao.CityJpaDAO;
import one.group.jpa.dao.PropertyListingJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class PropertyListingJpaDAOImpl extends JPABaseDAOImpl<String, PropertyListing> implements PropertyListingJpaDAO
{

    private AmenityJpaDAO amenityDAO;

    private CityJpaDAO cityJpaDAO;

    public CityJpaDAO getCityJpaDAO()
    {
        return cityJpaDAO;
    }

    public void setCityJpaDAO(CityJpaDAO cityJpaDAO)
    {
        this.cityJpaDAO = cityJpaDAO;
    }

    public AmenityJpaDAO getAmenityDAO()
    {
        return amenityDAO;
    }

    /**
     * Get property listing by property listing id
     * 
     * @param propertyListingId
     */
    public PropertyListing fetchPropertyListing(String propertyListingId)
    {
        return findById(propertyListingId);
    }

    /**
     * save property listing
     * 
     * @param propertyListing
     */
    public PropertyListing savePropertyListing(PropertyListing propertyListing)
    {
        return saveOrUpdate(propertyListing);
    }

    /**
     * Fetch list of property listings by account
     * 
     * @param accountId
     * @param statusList
     * @param offset
     * @param limit
     */
    public List<PropertyListing> fetchAllPropertyListingByAccountId(String accountId, List<BroadcastStatus> statusList)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        String namedQuery = null;
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("statusList", statusList);
        // here we check if status both active and expired , then we are
        // fetching your own property listings,in this case ordering should be
        // from oldest to newest
        if (statusList.size() > 1)
        {
            namedQuery = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_ASC;
        }
        else
        {
            namedQuery = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_DESC;
        }
        return findAllByNamedQuery(namedQuery, parameters);
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIds(List<String> accountIdList, List<Location> locationList, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_ACCOUNT_ID_LIST_LOCATION_LIST_AND_STATUS,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("accountIdList", accountIdList);
        typedQuery.setParameter("locationList", locationList);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    /**
     * Function to get total count of property listings by account
     * 
     * @param accountId
     */
    public long fetchPropertyListingCountByAccountId(String accountId, List<BroadcastStatus> statusList)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("statusList", statusList);

        return getCountByNamedQuery(PropertyListing.QUERY_TOTAL_COUNT_PROPERTY_LISTING_FOR_ACCOUNT, parameters);

    }

    /**
     * Function to fetch property listing by short reference
     * 
     * @param shortReference
     */
    public PropertyListing fetchPropertyListingByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isEmpty(shortReference), "Short reference should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("shortReference", shortReference.toUpperCase());
        return findByNamedQuery(PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_SHORT_REFERENCE, parameters);
    }

    public void renewPropertyListing(PropertyListing propertyListing)
    {

    }

    public PropertyListing fetchByShortReference(String shortReference)
    {
        Validation.isTrue(!Utils.isEmpty(shortReference), "Short reference should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("shortReference", shortReference);

        return findByNamedQuery(PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_SHORT_REFERENCE, parameters);
    }

    public List<String> fetchPropertiesByLocation(String locationId)
    {
        Validation.notNull(locationId, "Location Id passed should not be null.");

        TypedQuery<String> typedQuery = getEntityManager().createQuery(PropertyListing.TYPED_QUERY_FIND_IDS_BY_LOCALITY_ID, String.class);

        typedQuery.setParameter("locationId", locationId);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> getActivePropertyListings()
    {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", BroadcastStatus.ACTIVE);

        // return
        // findAllByNamedQuery(PropertyListing.QUERY_FIND_ACTIVE_PROPERTY_LISTING,
        // parameters);
        return findAllByNamedQuery(PropertyListing.QUERY_PROPERTY_LISTING_BY_STATUS, parameters);

    }

    public List<PropertyListing> getActiveAndExpiredPropertyListings()
    {
        List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
        statusList.add(BroadcastStatus.ACTIVE);
        statusList.add(BroadcastStatus.EXPIRED);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", statusList);

        // return
        // findAllByNamedQuery(PropertyListing.QUERY_FIND_ACTIVE_EXPIRED_PROPERTY_LISTING,
        // parameters);
        return findAllByNamedQuery(PropertyListing.QUERY_PROPERTY_LISTING_BY_STATUS, parameters);
    }

    public List<PropertyListing> searchPropertyListings(String accountId, List<Location> locations, String transactionType, List<PropertyMarket> propertyMarket, List<String> rooms,
            List<Amenity> amenities, int minArea, int maxArea, long minRentPrice, long maxRentPrice, long minSalePrice, long maxSalePrice, List<PropertyType> propertyTypes,
            List<PropertySubType> propertySubTypes)
    {

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<PropertyListing> cq = cb.createQuery(PropertyListing.class);
        Root<PropertyListing> propertyListing = cq.from(PropertyListing.class);
        cq.select(propertyListing);

        List<Predicate> predicates = new ArrayList<Predicate>();

        if (!Utils.isNull(rooms) && rooms.size() > 0)
        {
            predicates.add(cb.and(propertyListing.get("rooms").in(rooms)));
        }

        if (!Utils.isNull(amenities) && amenities.size() > 0)
        {
            predicates.add((cb.and(propertyListing.join("amenities").in(amenities))));
        }

        if (!Utils.isNull(propertyTypes) && propertyTypes.size() > 0)
        {
            predicates.add(propertyListing.get("type").in(propertyTypes));
        }

        if (!Utils.isNull(propertySubTypes) && propertySubTypes.size() > 0)
        {
            predicates.add(propertyListing.get("subType").in(propertySubTypes));
        }

        if (transactionType.equals("sale"))
        {
            predicates.add(cb.and(cb.notEqual(propertyListing.<Integer> get("salePrice"), 0), cb.ge(propertyListing.<Integer> get("salePrice"), minSalePrice),
                    cb.le(propertyListing.<Integer> get("salePrice"), maxSalePrice)));
        }
        else if (transactionType.equals("rent"))
        {
            predicates.add(cb.and(cb.notEqual(propertyListing.<Integer> get("rentPrice"), 0), cb.ge(propertyListing.<Integer> get("rentPrice"), minRentPrice),
                    cb.le(propertyListing.<Integer> get("rentPrice"), maxRentPrice)));
        }
        else if (transactionType.equals("both"))
        {
            predicates.add(cb.or((cb.and(cb.ge(propertyListing.<Integer> get("salePrice"), minSalePrice), cb.le(propertyListing.<Integer> get("salePrice"), maxSalePrice))),
                    (cb.and(cb.ge(propertyListing.<Integer> get("rentPrice"), minRentPrice), cb.le(propertyListing.<Integer> get("rentPrice"), maxRentPrice)))));
        }

        predicates.add(cb.and(cb.ge(propertyListing.<Integer> get("area"), minArea), cb.le(propertyListing.<Integer> get("area"), maxArea)));
        predicates.add(propertyListing.get("location").in(locations));
        predicates.add(propertyListing.get("propertyMarket").in(propertyMarket));
        predicates.add(cb.notEqual(propertyListing.get("account").get("id"), accountId));

        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

        TypedQuery<PropertyListing> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    public List<PropertyListingScoreObject> searchPropertyListing(List<Location> locationList, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_AND_STATUS_1,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("locationList", locationList);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationList, BroadcastStatus status, int maxResults)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS_1,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("locationIdList", locationList);
        typedQuery.setParameter("status", status);
        typedQuery.setMaxResults(maxResults);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> fetchAllPropertyListingByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        String namedQuery = null;
        List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
        statusList.add(BroadcastStatus.ACTIVE);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("statusList", statusList);
        namedQuery = PropertyListing.QUERY_GET_PROPERTY_LISTING_BY_ACCOUNT_CREATEDTIME_DESC;
        return findAllByNamedQuery(namedQuery, parameters);
    }

    public void markOtherPropertyListingsAsUnHot(String accountId, String propertyListingId)
    {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaUpdate<PropertyListing> update = cb.createCriteriaUpdate(PropertyListing.class);
        Root<PropertyListing> root = update.from(PropertyListing.class);
        List<javax.persistence.criteria.Predicate> predicate = new ArrayList<javax.persistence.criteria.Predicate>();
        predicate.add(cb.equal(root.get("account").get("id"), accountId));
        predicate.add(cb.notEqual(root.get("id"), propertyListingId));

        update.set("isHot", Boolean.valueOf(false));

        update.where(cb.and(predicate.toArray(new Predicate[predicate.size()])));

        getEntityManager().createQuery(update).executeUpdate();

    }

    public Long fetchActivePropertyListingCount()
    {
        List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
        statusList.add(BroadcastStatus.ACTIVE);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", statusList);
        return getCountByNamedQuery(PropertyListing.QUERY_GET_TOTAL_COUNT_ACTIVE_PROPERTY_LISTING, parameters);
    }

    public long fetchPropertyListingCountByStatus(BroadcastStatus status)
    {
        List<BroadcastStatus> statusList = new ArrayList<BroadcastStatus>();
        statusList.add(status);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", statusList);
        return getCountByNamedQuery(PropertyListing.QUERY_GET_TOTAL_COUNT_ACTIVE_PROPERTY_LISTING, parameters);
    }

    public List<PropertyListingScoreObject> searchPropertyListingByExactAndNearBy(String locationId, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION, PropertyListingScoreObject.class);
        typedQuery.setParameter("lId", locationId);
        typedQuery.setParameter("mId", locationId);
        typedQuery.setParameter("pId", locationId);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> fetchPropertyListingsByStatus(List<BroadcastStatus> propertyStatus)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", propertyStatus);
        return findAllByNamedQuery(PropertyListing.QUERY_PROPERTY_LISTING_BY_STATUS, parameters);

    }

    public List<String> fetchPropertyListingIdsByStatus(List<BroadcastStatus> propertyStatus)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", propertyStatus);

        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(PropertyListing.QUERY_PROPERTY_LISTING_IDS_BY_STATUS, String.class);
        typedQuery.setParameter("statusList", propertyStatus);

        return typedQuery.getResultList();

    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByStatus(BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_STATUS, PropertyListingScoreObject.class);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIdsAndLocationIds(List<String> accountIdList, List<String> locationIdList, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_ACCOUNT_ID_LIST_LOCATION_ID_LIST_AND_STATUS,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("accountIdList", accountIdList);
        typedQuery.setParameter("locationIdList", locationIdList);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS_1,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("locationIdList", locationIdList);

        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> fetchPropertyListingsByIdsAndStatus(List<String> propertyListingIds, List<BroadcastStatus> propertyStatus)
    {
        Validation.notNull(propertyListingIds, "The property listing ids passed cannot be null.");
        Validation.notNull(propertyStatus, "The property status list passed cannot be null.");

        if (propertyListingIds.isEmpty())
        {
            return new ArrayList<PropertyListing>();
        }

        if (propertyStatus.isEmpty())
        {
            propertyStatus = Arrays.asList(BroadcastStatus.values());
        }

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("propertyListingIdList", propertyListingIds);
        parameters.put("statusList", propertyStatus);

        return findAllByNamedQuery(PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_IDS_AND_STATUS, parameters);
    }

    public Long fetchAllPropertyListingCount()
    {

        Map<String, Object> parameters = new HashMap<String, Object>();

        return getCountByNamedQuery(PropertyListing.QUERY_GET_TOTAL_COUNT_ALL_PROPERTY_LISTING, parameters);

    }

    public List<PropertyListingCountObject> fetchPropertyListingCountOfAccountsByStatus(Set<String> accountIds, BroadcastStatus status)
    {
        TypedQuery<PropertyListingCountObject> typedQuery = getEntityManager().createQuery(PropertyListingCountObject.FETCH_ACCOUNTS_PROPERTY_LISTING_COUNT_OF_ACCOUNT_BY_STATUS,
                PropertyListingCountObject.class);
        typedQuery.setParameter("accountIds", accountIds);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> fetchPropertyListingsByPropertyListingds(List<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("propertyListingIdList", propertyListingIds);

        return findAllByNamedQuery(PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS, parameters);
    }

    public List<String> fetchPropertyListingIdsByPropertyListingIdsAndStatus(List<String> propertyListingIds, BroadcastStatus status)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");
        Validation.notNull(status, "The property status list passed cannot be null.");

        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(PropertyListing.TYPED_QUERY_FIND_IDS_BY_PROPERTY_LISTING_IDS_AND_STATUS, String.class);

        typedQuery.setParameter("propertyListingIdList", propertyListingIds);
        typedQuery.setParameter("status", status);
        return typedQuery.getResultList();

    }

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationList, BroadcastStatus status, int maxResults, String accountId)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(
                PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_LOCATION_ID_LIST_AND_STATUS_INCLUDING_OWN_PROPERTY_LISTING, PropertyListingScoreObject.class);
        typedQuery.setParameter("locationIdList", locationList);
        typedQuery.setParameter("status", status);
        typedQuery.setParameter("accountId", accountId);

        typedQuery.setMaxResults(maxResults);

        return typedQuery.getResultList();
    }

    public List<PropertyListing> fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(Set<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("propertyListingIdList", propertyListingIds);

        return findAllByNamedQuery(PropertyListing.QUERY_FIND_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS_AND_LAST_RENEWED_TIME, parameters);
    }

    public List<PropertyListingScoreObject> fetchAccountsByPropertyListingIds(List<String> propertyListingIds)
    {
        Validation.isTrue(!propertyListingIds.isEmpty(), "propertyListingIds should not be null");

        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_FETCH_ACCOUNTS_OF_PROPERTY_LISTINGS_BY_PROPERTY_LISTING_IDS,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("propertyListingIdList", propertyListingIds);

        return typedQuery.getResultList();
    }

    public List<PropertyListingScoreObject> isPropertyListingsExistOfAccount(String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");

        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_FIND_IS_PROPERTY_LISTINGS_EXISTS_FOR_ACCOUNT_ID,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("accountId", accountId);
        typedQuery.setMaxResults(1);
        return typedQuery.getResultList();

    }

    public List<PropertyListingScoreObject> fetchAllPropertyListingsForAccountId(String accountId, BroadcastStatus status)
    {
        TypedQuery<PropertyListingScoreObject> typedQuery = getEntityManager().createQuery(PropertyListingScoreObject.QUERY_SEARCH_PROPERTY_LISTINGS_BY_FOR_OWN_ACCOUNT_ID,
                PropertyListingScoreObject.class);
        typedQuery.setParameter("accountId", accountId);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();

    }

}