/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.WaQrcodes;
import one.group.jpa.dao.WaQrcodesJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaQrcodesJpaDAOImpl extends JPABaseDAOImpl<String, WaQrcodes> implements WaQrcodesJpaDAO
{

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentServerId(String currentServerId)
    {
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentClientId(String currentClientId)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByStatus(String status)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByCurrentCrmMobile(String currentCrmMobile)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<WaQrcodes> fetchWaQrcodesDAOByWaGroupsApprovedStatus(String waGroupsApprovedStatus)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
