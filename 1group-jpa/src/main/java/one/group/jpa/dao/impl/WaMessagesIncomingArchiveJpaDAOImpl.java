/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.WaMessagesIncomingArchive;
import one.group.jpa.dao.WaMessagesIncomingArchiveJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesIncomingArchiveJpaDAOImpl extends JPABaseDAOImpl<String, WaMessagesIncomingArchive> implements WaMessagesIncomingArchiveJpaDAO
{

    public List<WaMessagesIncomingArchive> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesIncomingArchive(WaMessagesIncomingArchive waMessagesIncomingArchive)
    {
        saveOrUpdate(waMessagesIncomingArchive);
    }

    public WaMessagesIncomingArchive fetchWaMessagesIncomingArchiveById(String waMessageId)
    {
        return findById(waMessageId);
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesIncomingArchive> fetchWaMessagesIncomingArchiveByStatus(String status)
    {
        return null;
    }

}
