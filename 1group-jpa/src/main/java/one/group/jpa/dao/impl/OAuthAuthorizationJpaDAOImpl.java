package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.OauthAuthorization;
import one.group.jpa.dao.OAuthAuthorizationJpaDAO;

public class OAuthAuthorizationJpaDAOImpl extends
		JPABaseDAOImpl<String, OauthAuthorization> implements
		OAuthAuthorizationJpaDAO {

	public OauthAuthorization getOauthAuthorizationByClientId(String clientId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		//Client client = clientJpaDAO.findById(clientId);
//		if(oauthAuthorization!=null){
//			oauthAuthorization.setClient(client);
//		}
		parameters.put("clientId", clientId);
		return findByNamedQuery(
				OauthAuthorization.QUERY_FIND_BY_CLIENT_ID, parameters);
	}

}
