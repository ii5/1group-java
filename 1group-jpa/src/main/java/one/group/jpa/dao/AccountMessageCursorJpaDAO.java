package one.group.jpa.dao;

import one.group.entities.jpa.AccountMessageCursors;

public interface AccountMessageCursorJpaDAO
{
    public void saveAccountMessageCursor(AccountMessageCursors accountMessageCursor);

    public AccountMessageCursors fetchByAccountMessageCursorsId(String accountMessageCursorId);
}
