package one.group.jpa.dao;

import one.group.entities.jpa.Association;


public interface AssociationJpaDAO extends JPABaseDAO<String, Association>
{

}
