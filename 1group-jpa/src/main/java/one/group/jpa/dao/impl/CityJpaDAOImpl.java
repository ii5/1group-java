package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.City;
import one.group.entities.jpa.helpers.BPOCityLocationData;
import one.group.jpa.dao.CityJpaDAO;

public class CityJpaDAOImpl extends JPABaseDAOImpl<String, City> implements CityJpaDAO
{

    public List<City> getAllCities()
    {
        Map<String, String> parameters = new HashMap<String, String>();
        return findAllByNamedQuery(City.QUERY_GET_ALL_CITIES, parameters);
    }

    public List<City> getAllCitiesByCityIds(Collection<String> cityIds)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("idList", cityIds);
        return findAllByNamedQuery(City.QUERY_FIND_ALL_CITIES_BY_ID_LIST, parameters);
    }

    public List<BPOCityLocationData> getAllCityLocations(List<String> cityIds, List<Status> locationStatusList)
    {
        String query = BPOCityLocationData.QUERY_BPO_FETCH_CITY_AND_LOCATIONS_FROM_CITY_ID_LIST;
        TypedQuery<BPOCityLocationData> typedQuery = null;

        if (cityIds == null || cityIds.isEmpty())
        {
            query = BPOCityLocationData.QUERY_BPO_FETCH_CITY_AND_LOCATIONS_FROM_LOCATION_STATUS;
            typedQuery = getEntityManager().createQuery(query, BPOCityLocationData.class);
            // typedQuery.setParameter("cityIdList", cityIds);
        }
        else
        {
            typedQuery = getEntityManager().createQuery(query, BPOCityLocationData.class);
            typedQuery.setParameter("cityIdList", cityIds);

        }

        if (locationStatusList == null || locationStatusList.isEmpty())
        {
            locationStatusList = Arrays.asList(Status.values());
        }

        typedQuery.setParameter("locationStatusList", locationStatusList);
        // typedQuery.setParameter("isPublishedList",
        // Arrays.asList(Boolean.TRUE));

        return typedQuery.getResultList();
    }

    public List<City> getAllCities(List<String> cityIdList, List<Boolean> isPublishedList, Map<SortField,SortType> sort, Map<FilterField,Set<String>> filter, int start, int rows)
    {
    	// FROM City c WHERE c.id IN :cityIdList AND c.isPublished IN :isPublishedList
    	
    	CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<City> cq = cb.createQuery(City.class);

        Root<City> from = cq.from(City.class);

        List<Predicate> predicateList = new ArrayList<Predicate>();
        
        if (isPublishedList == null || isPublishedList.isEmpty())
        {
            isPublishedList = Arrays.asList(Boolean.TRUE, Boolean.FALSE);
        }

        if (cityIdList != null && !cityIdList.isEmpty())
        {
            predicateList.add(from.get("id").in(cityIdList));
        }

        if (isPublishedList != null && !isPublishedList.isEmpty())
        {
        	predicateList.add(from.get("isPublished").in(isPublishedList));
        }
    	
    	// FILTERS

        if (filter != null && !filter.isEmpty())
        {
            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                Set<String> values = entry.getValue();

                predicateList.add(cb.or(from.get(field.dbField()).in(field.castIntoType(values))));
            }
        }

        // SORT

        if (sort != null && !sort.isEmpty())
        {
            for (Entry<SortField, SortType> entry : sort.entrySet())
            {
                SortField field = entry.getKey();
                SortType type = entry.getValue();

                Order order = null;
                if (type.equals(SortType.ASCENDING))
                {
                    order = cb.asc(from.get(field.dbField()));
                } else
                {
                    order = cb.desc(from.get(field.dbField()));
                }

                cq.orderBy(order);
            }
        }

        if (!predicateList.isEmpty())
        {
            cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }

        TypedQuery<City> tq = getEntityManager().createQuery(cq);
        
        tq.setFirstResult(start);
        tq.setMaxResults(rows);

        return tq.getResultList();
        
        
//        String query = City.QUERY_FIND_ALL_PUBLISHED_CITY_LIST_BY_IDS;
//        TypedQuery<City> typedQuery = null;
//
//        if (isPublishedList == null || isPublishedList.isEmpty())
//        {
//            isPublishedList = Arrays.asList(Boolean.TRUE, Boolean.FALSE);
//        }
//
//        if (cityIdList == null || cityIdList.isEmpty())
//        {
//            query = City.QUERY_FIND_ALL_PUBLISHED_CITY_LIST;
//            typedQuery = getEntityManager().createNamedQuery(query, City.class);
//        }
//        else
//        {
//            typedQuery = getEntityManager().createNamedQuery(query, City.class);
//            typedQuery.setParameter("cityIdList", cityIdList);
//        }
//
//        typedQuery.setParameter("isPublishedList", isPublishedList);
//
//        return typedQuery.getResultList();
    }

    public List<City> getAllCities(List<Boolean> isPublishedList)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("isPublishedList", isPublishedList);
        return findAllByNamedQuery(City.QUERY_FIND_ALL_PUBLISHED_CITY_LIST, parameters);
    }
}
