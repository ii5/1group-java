package one.group.jpa.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.helpers.ClientObject;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class ClientJpaDAOImpl extends JPABaseDAOImpl<String, Client> implements ClientJpaDAO
{
    private AccountJpaDAO accountJpaDAO;

    private static final Logger logger = LoggerFactory.getLogger(ClientJpaDAOImpl.class);

    public List<Client> getAllClientsOfAccount(final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Identifier should not be null or empty.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID, parameters);
    }

    public void saveClient(final Client client)
    {
        persist(client);
    }

    public AccountJpaDAO getAccountJpaDAO()
    {
        return accountJpaDAO;
    }

    public void setAccountJpaDAO(AccountJpaDAO accountJpaDAO)
    {
        this.accountJpaDAO = accountJpaDAO;
    }

    public Client getActiveClient(String accountId)
    {
        List<Client> clientList = getAllClientsOfAccount(accountId);
        Client client = null;

        for (Client c : clientList)
        {
            client = c;
            if (c.getStatus().equals(DeviceStatus.UNINSTALLED))
            {
                c.setStatus(DeviceStatus.ACTIVE);
                saveOrUpdate(c);
                break;
            }
        }

        return client;
    }

    public void updateClient(Client clientObj)
    {
        saveOrUpdate(clientObj);
        return;
    }

    public Client fetchByPushChannel(String pushChannel)
    {
        Validation.notNull(pushChannel, "Push Channel should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("pushchannel", pushChannel);

        return findByNamedQuery(Client.QUERY_FIND_BY_PUSHCHANNEL, parameters);
    }

    public List<Client> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Identifier should not be null or empty.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        parameters.put("devicePlatform", devicePlatform);
        return findAllByNamedQuery(Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_PLATFORM, parameters);
    }

    public Client fetchByAccountIdAndDeviceToken(String accountId, String deviceToken)
    {
        Validation.isTrue(!Utils.isNull(accountId), "Passed account is should not be null");
        Validation.isTrue(!Utils.isNull(deviceToken), "Passed device Token is should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        parameters.put("deviceToken", deviceToken);

        return findByNamedQuery(Client.QUERY_FIND_CLIENT_BY_ACCOUNT_ID_AND_DEVICE_TOKEN, parameters);
    }

    public List<Client> fetchByDeviceToken(String deviceToken)
    {
        Validation.isTrue(!Utils.isNull(deviceToken), "Passed device Token is should not be null");

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("deviceToken", deviceToken);

        return findAllByNamedQuery(Client.QUERY_FIND_CLIENT_BY_DEVICE_TOKEN, parameters);
    }

    public List<Client> getAllActiveClientsOfAccount(final String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Identifier should not be null or empty.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(Client.QUERY_FIND_ACTIVE_CLIENT_BY_ACCOUNT_ID, parameters);
    }

    public List<Client> getAllActiveClients()
    {
        Map<String, String> parameters = new HashMap<String, String>();

        return findAllByNamedQuery(Client.QUERY_FIND_ALL_ACTIVE_CLIENT, parameters);
    }

    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("status", deviceStatus);
        parameters.put("offsetTime", beforeTimeDate);
        return findAllByNamedQuery(Client.QUERY_FIND_CLIENT_FROM_SCHEDULER_1, parameters);
    }

    public void deactivateClient(List<Client> clientList)
    {
        for (Client c : clientList)
        {
            c.setStatus(DeviceStatus.INACTIVE);
            persist(c);
        }
    }

    public void purgeClient(List<Client> clientList)
    {
        for (Client c : clientList)
        {
            remove(c);
        }
    }

    public List<Client> getLastActivityTime(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Identifier should not be null or empty.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(Client.QUERY_GET_LAST_ACTIVITY_TIME, parameters, 0, 1);
    }

    public List<ClientObject> getLastActivityOfAccounts(Set<String> accountIds)
    {
        TypedQuery<ClientObject> typedQuery = getEntityManager().createQuery(ClientObject.FETCH_ACCOUNTS_LAST_ACTIVITY_TIME, ClientObject.class);
        typedQuery.setParameter("accountIds", accountIds);

        return typedQuery.getResultList();

    }

    public String getAccountIdFromClientId(String clientId)
    {
        Validation.isTrue(!Utils.isNull(clientId), "Passed client id should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("clientId", clientId);
        return getStringValueByNamedQuery(Client.QUERY_FIND_ACCOUNT_ID_FROM_CLIENT_ID, parameters);

    }

}
