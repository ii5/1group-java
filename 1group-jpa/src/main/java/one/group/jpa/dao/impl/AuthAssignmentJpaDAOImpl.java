package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.FlushModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import one.group.entities.jpa.AuthAssignment;
import one.group.jpa.dao.AuthAssignmentJpaDAO;

public class AuthAssignmentJpaDAOImpl extends JPABaseDAOImpl<String, AuthAssignment> implements AuthAssignmentJpaDAO
{
    public void saveAuthAssignment(AuthAssignment authAssignment)
    {
        persist(authAssignment);

    }

    public List<AuthAssignment> fetchByUserId(String userId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("userId", userId);
        return findAllByNamedQuery(AuthAssignment.QUERY_FIND_BY_USER_ID, parameters);
    }
    
    
    public List<AuthAssignment> fetchAuthAssignmentItemsByUserIdList(Collection<String> userIdList)
    {
        if (userIdList == null || userIdList.isEmpty())
        {
            return new ArrayList<AuthAssignment>();
        }
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userIdList", userIdList);
        return findAllByNamedQuery(AuthAssignment.QUERY_FIND_AUTH_ITEMS_BY_USER_ID, parameters);
    }
    
    
    public List<String> fetchAuthAssignmentItemNamesByUserId(String userId)
    {
        if (userId == null || userId.trim().isEmpty())
        {
            return new ArrayList<String>();
        }
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userId", userId);
        
        TypedQuery<String> query = getEntityManager().createQuery(AuthAssignment.QUERY_FIND_AUTH_ITEM_NAME_BY_USER_ID, String.class);
        query.setParameter("userId", userId);
        
        return query.getResultList();
    }

    public void deleteAssignmentOfId(Collection<String> userId)
    {
        if (userId == null || userId.isEmpty())
        {
            return;
        }
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userId", userId);
        
        Query query = getEntityManager().createQuery("DELETE FROM AuthAssignment a WHERE a.user IN :userId");
        query.setParameter("userId", userId);
        
        query.executeUpdate();
        
    }
    
    public List<String> fetchUserIdsByRoles(Collection<String> roles) 
    {
    	if (roles == null || roles.isEmpty())
    	{
    		return new ArrayList<String>();
    	}
        
        TypedQuery<String> query = getEntityManager().createQuery(AuthAssignment.QUERY_FIND_USER_IDS_BY_ROLES, String.class);
        query.setParameter("roles", roles);
        
        return query.getResultList();
        
    }
}
