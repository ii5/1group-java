package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.TargetedMarketingList;

public interface TargetedMarketingListJpaDAO extends JPABaseDAO<String, TargetedMarketingList>
{
	/**
	 * 
	 * @return
	 */
	public List<TargetedMarketingList> fetchAllTargetedList();
	
	/**
	 * 
	 * @param phoneNumber
	 */
	public void deleteTargetedListByPhoneNumber(String phoneNumber);

	public void deleteAllTargetedList();
}
