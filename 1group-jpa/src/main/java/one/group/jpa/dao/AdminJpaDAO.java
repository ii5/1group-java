/**
 * 
 */
package one.group.jpa.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.AdminStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.jpa.Admin;

/**
 * @author ashishthorat
 *
 */
public interface AdminJpaDAO
{
    public List<Admin> fetchAdminByRoleId(String roleId);

    public List<Admin> fetchAdminByManagerId(String managerId);

    public List<Admin> fetchAdminByStatus(String status);

    public void saveAdmin(Admin admin);

    public Admin fetchAdminById(String adminId);
    
    public List<Admin> fetchAdminUsersByRoleListAndAdminStatusList(List<String> roleList, List<AdminStatus> adminStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);
    
    public List<Admin> fetchAdminUsersByInput(String username, List<AdminStatus> adminStatusList, String email, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);
}
