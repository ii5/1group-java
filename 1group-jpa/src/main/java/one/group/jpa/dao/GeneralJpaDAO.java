package one.group.jpa.dao;

import java.util.List;
import java.util.Map;

import one.group.core.enums.EntityFieldName;

public interface GeneralJpaDAO extends JPABaseDAO<String, Object>
{
    public List<Object> executeAndFetchRecords(String query, Map<EntityFieldName, Object> fieldVsValueMap);
}
