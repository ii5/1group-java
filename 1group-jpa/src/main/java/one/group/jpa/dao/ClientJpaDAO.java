package one.group.jpa.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import one.group.core.enums.status.DeviceStatus;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.helpers.ClientObject;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface ClientJpaDAO extends JPABaseDAO<String, Client>
{

    /**
     * fetch all clients from account.
     * 
     * @param accountId
     * @return
     */
    public List<Client> getAllClientsOfAccount(String accountId);

    public void saveClient(Client client);

    public Client getActiveClient(String accountId);

    public void updateClient(Client clientObj);

    public Client fetchByPushChannel(String pushChannel);

    public List<Client> fetchByAccountAndDevicePlatform(String accountId, String devicePlatform);

    public Client fetchByAccountIdAndDeviceToken(String accountId, String deviceToken);

    public List<Client> fetchByDeviceToken(String deviceId);

    public List<Client> getAllActiveClientsOfAccount(String accountId);

    public List<Client> getAllActiveClients();

    public List<Client> findClientForSchedulers(List<DeviceStatus> deviceStatus, Date beforeTimeDate);

    public void deactivateClient(List<Client> clientList);

    public void purgeClient(List<Client> clientList);

    public List<Client> getLastActivityTime(String accountId);

    public List<ClientObject> getLastActivityOfAccounts(Set<String> accountIds);

    public String getAccountIdFromClientId(String clientId);

}
