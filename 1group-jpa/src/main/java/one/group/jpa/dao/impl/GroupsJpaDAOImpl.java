/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.enums.GroupStatus;
import one.group.entities.jpa.Groups;
import one.group.jpa.dao.GroupsJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class GroupsJpaDAOImpl extends JPABaseDAOImpl<String, Groups> implements GroupsJpaDAO
{

    public List<Groups> fetchGroupsByGroupStatus(GroupStatus status)
    {
        return null;
    }

    public Groups fetchByGroupId(String groupId)
    {
        return findById(groupId);
    }

    public List<Groups> fetchGroupsByCityId(String cityId)
    {
        return null;
    }

    public List<Groups> fetchGroupsByLocationId(String cityId)
    {
        return null;
    }

    public List<Groups> fetchGroupsByCreatorMobileNo(String creatorMobileNo)
    {
        return null;
    }

    public List<Groups> fetchGroupsIdsByGroupName(String groupName)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("groupName", groupName);
        return findAllByNamedQuery(Groups.QUERY_FIND_GROUP_IDS__BY_GROUP_NAME, parameters);

    }

}
