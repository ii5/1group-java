package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.LocationMatch;

/**
 * 
 * @author miteshchavda
 *
 */
public interface LocationMatchJpaDAO extends JPABaseDAO<String, LocationMatch>
{
    /**
     * Fetch location match of location
     * 
     * @param locationId
     * @param exact
     *            TODO
     * @return
     */
    public List<LocationMatch> fetchExactLocationMatchByLocationId(String locationId, boolean isExact);

    /**
     * Fetch location match of location
     * 
     * @param locationId
     * @param exact
     *            TODO
     * @return
     */
    public List<LocationMatch> fetchExactLocationMatchByLocationIdList(List<String> locationIdList, boolean isExact);

    /**
     * 
     * @param locationId
     * @return
     */
    public List<LocationMatch> fetchLocationMatchByLocationId(String locationId);

    /**
     * 
     * @param locationIdList
     * @return
     */
    public List<String> fetchLocationMatchByLocationIdList(List<String> locationIdList);

    /**
     * 
     * @param locationIdList
     * @param isExact
     * @return
     */
    public List<String> fetchExactLocationMatchIdListByLocationIdList(List<String> locationIdList, boolean isExact);

    public void saveLocationMatch(LocationMatch locationMatch);
}
