package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.SearchNotification;

public interface SearchNotificationJpaDAO extends JPABaseDAO<String, SearchNotification>
{
    public List<SearchNotification> fetchAllNotifications();

    public void saveSearchNotification(SearchNotification searchNotification);

    public void removeAllSearchNotification();

    public long getSearchNotificationCountByAccountIdAndSentFlag(String accountId, boolean sendFlag);
}
