/**
 * 
 */
package one.group.jpa.dao;

import one.group.entities.jpa.GroupMembers;

/**
 * @author ashishthorat
 *
 */
public interface GroupJpaDAO
{
    public void saveGroup(GroupMembers group);

}
