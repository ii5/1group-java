package one.group.jpa.dao;

import one.group.entities.jpa.Amenity;


public interface AmenityJpaDAO extends JPABaseDAO<String, Amenity>
{
    /**
     * 
     * @param name
     * @return
     */
    public Amenity findByName(String name);
}
