package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.enums.BookMarkType;
import one.group.entities.jpa.BookMark;
import one.group.jpa.dao.BookMarkJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class BookMarkJpaDAOImpl extends JPABaseDAOImpl<String, BookMark> implements BookMarkJpaDAO
{
    /**
     * 
     */
    public BookMark bookmark(String referenceId, String targetEntityId, BookMarkType type, String action)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Validation.isTrue(!Utils.isEmpty(targetEntityId), "Target entity should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        parameters.put("targetEntity", targetEntityId);
        BookMark bookMark = findByNamedQuery(BookMark.QUERY_FIND_BOOKMARKED_ENTRY, parameters);

        if (bookMark == null && Boolean.valueOf(action))
        {
            BookMark bookmark = new BookMark();
            bookmark.setType(type);
            bookmark.setTargetEntity(targetEntityId);
            bookmark.setReferenceId(referenceId);
            saveOrUpdate(bookmark);
        }
        else if (Boolean.valueOf(action) == false && bookMark != null)
        {
            removeById(bookMark.getIdAsString());
        }
        return bookMark;
    }

    /**
     * 
     */
    public List<BookMark> getAllBookMarksByReference(String referenceId, BookMarkType type)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        List<BookMark> bookMark = findAllByNamedQuery(BookMark.QUERY_FIND_ALL_BOOKMARKS, parameters);
        return bookMark;
    }

    public BookMark getBookMarkByReferenceAndTargetEntity(String referenceId, String targetEntityId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        parameters.put("targetEntity", targetEntityId);
        BookMark bookMark = findByNamedQuery(BookMark.QUERY_FIND_BOOKMARKED_ENTRY, parameters);
        return bookMark;
    }
    
    public List<BookMark> checkIsBookMarkedForPropertyListings(String accountId, List<String> propertyListings)
    {
    	Validation.isTrue(!Utils.isEmpty(accountId), "Reference id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("referenceId", accountId);
        parameters.put("targetEntityList", propertyListings);
        List<BookMark> bookMark = findAllByNamedQuery(BookMark.QUERY_CHECK_BOOKMARKED_PROPERTY_LISTINGS, parameters);
        return bookMark;
    }
}
