package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import one.group.core.enums.status.Status;
import one.group.entities.jpa.Location;
import one.group.jpa.dao.CityJpaDAO;
import one.group.jpa.dao.LocationJpaDAO;
import one.group.utils.validation.Validation;

public class LocationJpaDAOImpl extends JPABaseDAOImpl<String, Location> implements LocationJpaDAO
{

    private CityJpaDAO cityJpaDAO;

    /**
     * @return the cityJpaDAO
     */
    public CityJpaDAO getCityJpaDAO()
    {
        return cityJpaDAO;
    }

    /**
     * @param cityJpaDAO
     *            the cityJpaDAO to set
     */
    public void setCityJpaDAO(CityJpaDAO cityJpaDAO)
    {
        this.cityJpaDAO = cityJpaDAO;
    }

    public Collection<Location> getAllLocations()
    {
        String namedQuery = null;
        namedQuery = Location.QUERY_GET_ALL_LOCATIONS;
        Map<String, Object> parameters = new HashMap<String, Object>();
        return findAllByNamedQuery(namedQuery, parameters);
    }

    public void updateLiveLocationTable(String cityId)
    {
        String cityidstring = "'" + cityId + "'";
        String queryString = "SELECT cast(update_city_wise_locations(" + cityidstring + ") as text)";

        Query query = getEntityManager().createNativeQuery(queryString);
        Object result = query.getSingleResult();

        String nearbylocationsQuery = "SELECT cast(update_nearby_locations_city_wise(" + cityidstring + ") as text)";

        Query q = getEntityManager().createNativeQuery(nearbylocationsQuery);
        Object nearByLocalitirsResult = q.getSingleResult();

        String locationMatchQuery = "SELECT cast(update_location_match_city_wise(" + cityidstring + ") as text)";

        Query locationQ = getEntityManager().createNativeQuery(locationMatchQuery);
        Object locationMatchResult = locationQ.getSingleResult();

    }

    public List<Location> fetchLocalitiesOfCityById(String cityId)
    {
        Validation.notNull(cityId, "cityId should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("cityId", cityId);
        return findAllByNamedQuery(Location.QUERY_GET_LOCALITY_BY_CITY_ID, parameters);
    }

    public List<Location> fetchAllLocations(Collection<String> locationIdList)
    {
        // Validation.notNull(locationIdList,
        // "locationIdList should not be null.");
        String query = Location.QUERY_FIND_ALL_LOCATIONS_BY_ID_LIST;
        Map<String, Object> parameters = new HashMap<String, Object>();

        if (locationIdList == null || locationIdList.isEmpty())
        {
            query = Location.QUERY_GET_ALL_LOCATIONS;
            return findAllByNamedQuery(query, parameters);
        }
        else
        {
            parameters.put("idList", locationIdList);
            return findAllByNamedQuery(query, parameters);
        }

    }

    public List<Location> fetchAllLocationsByCityIds(List<String> cityIds)
    {
        Validation.notNull(cityIds, "cityIds should not be null.");
        if (cityIds.isEmpty())
        {
            return new ArrayList<Location>();
        }
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("cityIds", cityIds);

        return findAllByNamedQuery(Location.QUERY_GET_LOCALITY_BY_CITY_IDS, parameters);
    }

    public List<Location> fetchAllLocationByStatus(List<Status> statusList)
    {
        Validation.notNull(statusList, "statusList should not be null.");
        if (statusList.isEmpty())
        {
            return new ArrayList<Location>();
        }
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("statusList", statusList);

        return findAllByNamedQuery(Location.QUERY_GET_LOCALITY_BY_STATUS, parameters);
    }

}
