package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.PhoneNumber;
import one.group.jpa.dao.PhoneNumberJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 */
public class PhoneNumberJpaDAOImpl extends JPABaseDAOImpl<String, PhoneNumber> implements PhoneNumberJpaDAO
{
    public PhoneNumber fetchPhoneNumber(String mobileNumber)
    {
        Validation.notNull(mobileNumber, "The mobileNumber passed should not be null.");
        Map<String, String> params = new HashMap<String, String>();
        
        params.put("phoneNumber", mobileNumber);
        
        return findByNamedQuery(PhoneNumber.QUERY_FIND_BY_NUMBER, params);
    }
    
    public List<PhoneNumber> fetchPhoneNumbersByIds(Collection<String> phoneNumbers) 
    {
    	return findByIds(phoneNumbers);
    }
}
