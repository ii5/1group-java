/**
 * 
 */
package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.jpa.WaUserCity;

/**
 * @author ashishthorat
 *
 */
public interface WaUserCityJpaDAO
{
    public void saveWaUserCity(WaUserCity waUserCity);

    public WaUserCity fetchByWaUserCityId(String waUserCityId);

    public List<WSUserCity> fetchByPriorityCityAndAdmin(Integer priority, String cityId, String adminId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows);

    public void deleteByUserIdAndCityList(String userId, Collection<String> cityIdList);

    public void saveBulkByUserIdAndCityIdList(String userId, Map<String, Integer> cityIdMap);

    public void cleanAll();

    public void cleanByUserIdList(List<String> userIdList);

    public List<WSUserCity> fetchUnassignedCityAdmin();
}
