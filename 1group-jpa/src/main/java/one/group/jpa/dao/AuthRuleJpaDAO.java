/**
 * 
 */
package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.AuthItem;
import one.group.entities.jpa.AuthRule;

public interface AuthRuleJpaDAO
{
    public List<AuthItem> fetchAuthItemByType(String type);

    public AuthItem fetchAuthItemByRuleName(String ruleName);

    public AuthItem fetchAuthItemByGroupCode(String groupCode);

    public void saveAuthRule(AuthRule authItem);
}
