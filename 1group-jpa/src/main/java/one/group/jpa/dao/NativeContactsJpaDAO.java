package one.group.jpa.dao;

import java.util.List;
import java.util.Set;

import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.NativeContacts;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface NativeContactsJpaDAO extends JPABaseDAO<String, NativeContacts>
{

    public void saveNativeContacts(Account account, String fullName, String companyName, String phoneNumber);

    public Set<Account> fetchAccountsLinkedWithPhonenumber(String phoneNumber);

    public Set<String> fetchPhonenumbersLinkedWithAccount(Account account);

    public NativeContacts fetchContactsWithPhonenumberAndAccount(String phoneNumber, Account account);

    public List<NativeContacts> fetchContactsWithPhonenumbersAndAccount(Set<String> phoneNumbers, Account account);

    public List<NativeContacts> fetchAllNativeContactsByAccountId(String accountId);

    public List<String> fetchNativeContactsByPhoneNumber(String phoneNumber, Status status);

    public List<WSNativeContact> fetchAllNativeContactsByAccountIdFiltered(String accountId, Set<String> registeredPhoneNumbers, int offset, int limit);

    public int fetchAllNativeContactsByAccountIdCount(String accountId, Set<String> registeredPhoneNumbers);

}
