package one.group.jpa.dao;

import java.util.List;
import java.util.Set;

import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.Amenity;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.helpers.PropertyListingCountObject;
import one.group.entities.jpa.helpers.PropertyListingScoreObject;
import one.group.exceptions.movein.PropertyNotFoundException;

public interface PropertyListingJpaDAO extends JPABaseDAO<String, PropertyListing>
{

    /**
     * Fetch property listing by property listing id
     * 
     * @param propertyListingId
     * @return
     */
    public PropertyListing fetchPropertyListing(String propertyListingId);

    /**
     * Save property listing
     * 
     * @param propertyListing
     * @return
     */
    public PropertyListing savePropertyListing(PropertyListing propertyListing);

    /**
     * Fetch property listings by account id
     * 
     * @param accountId
     * @param offset
     * @param limit
     * @return
     */
    List<PropertyListing> fetchAllPropertyListingByAccountId(String accountId, List<BroadcastStatus> statusList);

    /**
     * 
     * @param accountIdList
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIds(List<String> accountIdList, List<Location> locationList, BroadcastStatus status);

    /**
     * Fetch property listings count by account id
     * 
     * @param accountId
     * @return
     */
    public long fetchPropertyListingCountByAccountId(String accountId, List<BroadcastStatus> statusList);

    /**
     * Fetch property listing by short reference
     * 
     * @param shortReference
     * @return
     * @throws PropertyNotFoundException
     */
    public PropertyListing fetchPropertyListingByShortReference(String shortReference);

    /**
     * Renew property listing
     * 
     * @param propertyListing
     * @return
     */
    public void renewPropertyListing(PropertyListing propertyListing);

    /**
     * 
     * @param shortReference
     * @return
     */
    public PropertyListing fetchByShortReference(String shortReference);

    /**
     * 
     * @param locationId
     * @return
     */
    public List<String> fetchPropertiesByLocation(String locationId);

    /**
     * 
     * @return
     */
    public List<PropertyListing> getActivePropertyListings();

    /**
     * 
     * @return
     */
    public List<PropertyListing> getActiveAndExpiredPropertyListings();

    /**
     * Search properties
     * 
     * @param accountId
     * @param locations
     * @param transactionType
     * @param market
     * @param rooms
     * @param amenities
     * @param minArea
     * @param maxArea
     * @param minRentPrice
     * @param maxRentPrice
     * @param minSalePrice
     * @param maxSalePrice
     * @param propertyTypes
     * @param propertySubTypes
     * @return
     */
    public List<PropertyListing> searchPropertyListings(String accountId, List<Location> locations, String transactionType, List<PropertyMarket> market, List<String> rooms, List<Amenity> amenities,
            int minArea, int maxArea, long minRentPrice, long maxRentPrice, long minSalePrice, long maxSalePrice, List<PropertyType> propertyTypes, List<PropertySubType> propertySubTypes);

    /**
     * 
     * @param accountId
     * @return
     */
    public List<PropertyListing> fetchAllPropertyListingByAccountId(String accountId);

    /**
     * 
     * @param accountId
     * @param propertyListingId
     */
    public void markOtherPropertyListingsAsUnHot(String accountId, String propertyListingId);

    /**
     * 
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> searchPropertyListing(List<Location> locationList, BroadcastStatus status);

    /**
     * 
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationList, BroadcastStatus status, int maxResults);

    public Long fetchActivePropertyListingCount();

    public long fetchPropertyListingCountByStatus(BroadcastStatus status);

    public List<PropertyListingScoreObject> searchPropertyListingByExactAndNearBy(String locationId, BroadcastStatus status);

    public List<PropertyListing> fetchPropertyListingsByStatus(List<BroadcastStatus> propertyStatus);

    public List<String> fetchPropertyListingIdsByStatus(List<BroadcastStatus> propertyStatus);

    /**
     * 
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllPropertyListingsByStatus(BroadcastStatus status);

    /**
     * Fetch property listings by id and status.
     * 
     * @param propertyListingIds
     * @param propertyStatus
     * @return
     */
    public List<PropertyListing> fetchPropertyListingsByIdsAndStatus(List<String> propertyListingIds, List<BroadcastStatus> propertyStatus);

    /**
     * Fetch count of all property listing
     * 
     * @return
     */
    public Long fetchAllPropertyListingCount();

    /**
     * Fetch all properties by account ids and location ids and status
     * 
     * @param accountIdList
     * @param locationIdList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> fetchAllPropertyListingsByAccountIdsAndLocationIds(List<String> accountIdList, List<String> locationIdList, BroadcastStatus status);

    /**
     * Search all properties of locations
     * 
     * @param locationList
     * @param status
     * @return
     */
    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationIdList, BroadcastStatus status);

    /**
     * 
     * Fetch property listing count of accounts by status
     * 
     * @param accountIds
     * @param status
     * @return
     */
    public List<PropertyListingCountObject> fetchPropertyListingCountOfAccountsByStatus(Set<String> accountIds, BroadcastStatus status);

    public List<PropertyListing> fetchPropertyListingsByPropertyListingds(List<String> propertyListingIds);

    public List<String> fetchPropertyListingIdsByPropertyListingIdsAndStatus(List<String> propertyListingIds, BroadcastStatus status);

    public List<PropertyListingScoreObject> searchPropertyListingByLocationIds(List<String> locationList, BroadcastStatus status, int maxResults, String accountId);

    public List<PropertyListing> fetchPropertyListingsByPropertyListingIdsAndlastRenewedTime(Set<String> propertyListingIds);

    /**
     * Fetch accounts associated with property listings
     * 
     * @param propertyListingIds
     * @return
     */
    public List<PropertyListingScoreObject> fetchAccountsByPropertyListingIds(List<String> propertyListingIds);

    public List<PropertyListingScoreObject> isPropertyListingsExistOfAccount(String accountId);

    public List<PropertyListingScoreObject> fetchAllPropertyListingsForAccountId(String accountId, BroadcastStatus status);

}