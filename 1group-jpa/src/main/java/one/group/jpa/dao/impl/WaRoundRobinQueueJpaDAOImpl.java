/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.WaRoundRobinQueue;
import one.group.jpa.dao.WaRoundRobinQueueJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaRoundRobinQueueJpaDAOImpl extends JPABaseDAOImpl<String, WaRoundRobinQueue> implements WaRoundRobinQueueJpaDAO
{

}
