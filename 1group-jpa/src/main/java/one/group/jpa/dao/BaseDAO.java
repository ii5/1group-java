package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

/**
 * 
 * @author nyalfernandes
 * 
 * @param <K>
 * @param <E>
 */
public interface BaseDAO<K, E>
{

    /**
     * This method returns all entries of a given entity.
     * 
     * @return
     */
    public Collection<E> findAll();

    /**
     * This method limit number of entries of a given entity
     * 
     */
    public Collection<E> findAllFromTo(E entity, int from, int to);

    /**
     * This method searches for an entity with the passed id.
     * 
     * @param id
     * @return Instance of the Entity found or null otherwise.
     */
    public E findById(K id);
    
    /**
     * 
     * @param ids
     * @return
     */
    public List<E> findByIds(final Collection<String> ids);

    /**
     * This method is used to persist an Entity from a repository.
     * 
     * @param entity
     */
    public void persist(E entity);

    /**
     * This method deletes an entity from a repository.
     * 
     * @param entity
     */
    public void remove(E entity);

    /**
     * Method that removes the given entity with the id specified.
     * 
     * @param id
     */
    public void removeById(K id);

}
