package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.Otp;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.jpa.dao.OtpJpaDAO;
import one.group.jpa.dao.PhoneNumberJpaDAO;

public class OtpJpaDAOImpl extends JPABaseDAOImpl<String, Otp> implements OtpJpaDAO
{
    private PhoneNumberJpaDAO phoneNumberJpaDAO;
    private ClientJpaDAO clientJpaDAO;

    public ClientJpaDAO getClientJpaDAO()
    {
        return clientJpaDAO;
    }

    public void setClientJpaDAO(ClientJpaDAO clientJpaDAO)
    {
        this.clientJpaDAO = clientJpaDAO;
    }

    public PhoneNumberJpaDAO getPhoneNumberJpaDAO()
    {
        return phoneNumberJpaDAO;
    }

    public void setPhoneNumberJpaDAO(final PhoneNumberJpaDAO phoneNumberJpaDAO)
    {
        this.phoneNumberJpaDAO = phoneNumberJpaDAO;
    }

    public Otp fetchOtpFromOtpAndPhoneNumber(String otp, String mobileNumber)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phoneNumber", mobileNumber);
        parameters.put("otp", otp);

        Otp otpObj = findByNamedQuery(Otp.QUERY_FIND_BY_OTP_AND_NUMBER, parameters);

        return otpObj;
    }

    /**
     * Function to delete Otp
     * 
     * @return
     */
    public void deleteOtp(Otp otp)
    {
        remove(otp);
//        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//        CriteriaDelete<Otp> query = builder.createCriteriaDelete(Otp.class);
//        Root<Otp> e = query.from(Otp.class);
//        List<javax.persistence.criteria.Predicate> predicate = new ArrayList<javax.persistence.criteria.Predicate>();
//
//        predicate.add(builder.equal(e.get("otp"), otp.getOtp()));
//        predicate.add(builder.equal(e.get("client").get("id"), otp.getClient().getId()));
//        predicate.add(builder.equal(e.get("phoneNumber").get("id"), otp.getPhoneNumber().getId()));
//
//        query.where(builder.and(predicate.toArray(new Predicate[predicate.size()])));
//
//        getEntityManager().createQuery(query).executeUpdate();

    }
}
