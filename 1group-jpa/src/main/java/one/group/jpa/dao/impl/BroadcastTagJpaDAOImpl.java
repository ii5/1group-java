/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.BroadcastType;
import one.group.core.enums.CommissionType;
import one.group.core.enums.FilterField;
import one.group.core.enums.PropertyMarket;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.BroadcastTag;
import one.group.entities.jpa.City;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.helpers.BroadcastObject;
import one.group.entities.jpa.helpers.BroadcastTagsConsolidated;
import one.group.jpa.dao.BroadcastTagJpaDAO;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 * 
 */
public class BroadcastTagJpaDAOImpl extends JPABaseDAOImpl<String, BroadcastTag> implements BroadcastTagJpaDAO
{

    public List<BroadcastTag> fetchBroadcastTagByBroadcastId(String broadcastId, BroadcastStatus status)
    {
        Validation.notNull(status, "Status should not be null.");
        Validation.notNull(broadcastId, "broadcastId should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("status", status);
        parameters.put("broadcastId", broadcastId);

        return findAllByNamedQuery(BroadcastTag.QUERY_FIND_TAGS_BY_BROADCAST_ID, parameters);
    }

    public List<BroadcastTag> fetchBroadcastTagByBroadcastType(BroadcastType type)
    {
        return null;
    }

    public List<BroadcastTag> fetchBroadcastTagByLocationId(String location_id)
    {
        return null;
    }

    public void saveBroadcastTag(BroadcastTag broadcastTag)
    {
        saveOrUpdate(broadcastTag, false);
    }

    public BroadcastTag fetchBroadcastTagbyBroadcastTagId(String broadcastTagId)
    {
        return findById(broadcastTagId);
    }

    public List<BroadcastTag> fetchBroadcastTagByBroadcastIds(Set<String> broadcastIds)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        if (broadcastIds == null || broadcastIds.isEmpty())
        {
            return Collections.<BroadcastTag> emptyList();
        }

        parameters.put("broadcastIdList", broadcastIds);

        return findAllByNamedQuery(BroadcastTag.QUERY_FIND_TAGS_BY_BROADCAST_ID_LIST, parameters);
    }

    public List<BroadcastTagsConsolidated> fetchBroadcastsByCityAndLocation(List<String> cityIds, List<String> locationIds, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter,
            int start, int rows)
    {
        // "SELECT NEW one.group.entities.jpa.helpers.BroadcastTagsConsolidated(b.broadcast.id, b.broadcast.messageId, b.broadcast.status, "
        // +
        // "b.broadcast.createdTime, b.broadcast.createdBy, b.broadcast.updatedTime, b.broadcast.updatedBy, b.title, b.size, b.transactionType, b.broadcastType, b.commissionType, b.customSublocation, b.maximumPrice, "
        // +
        // "b.minimumPrice, b.propertyType, b.rooms, b.location.id, b.location.localityName, b.cityId, b.location.city.name, b.price, b.createdBy, b.updatedBy, b.createdTime, b.updatedTime) "
        // +
        // "FROM BroadcastTag b WHERE b.location.id IN :locationIdList AND b.cityId IN :cityIdList"

        // public BroadcastTagsConsolidated(String broadcastId, String
        // messageId, BroadcastStatus broadcastStatus, Date
        // broadcastCreatedTime, String broadcastCreatedBy, Date
        // broadcastUpdatedTime, String broadcastUpdatedBy,
        // String tagTitle, Long size, PropertyTransactionType transactionType,
        // BroadcastType broadcastType, CommissionType commissionType, String
        // customSubLocation, Long maximumPrice, Long minimumPrice,
        // PropertyType propertyType, Rooms rooms, String locationId, String
        // locationName, String cityId, String cityName, Long price, String
        // tagCreatedBy, String tagUpdatedBy, Date tagCreatedTime, Date
        // tagUpdatedTime)

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<BroadcastTagsConsolidated> cq = cb.createQuery(BroadcastTagsConsolidated.class);
        Root<BroadcastTag> broadcastTag = cq.from(BroadcastTag.class);
        Join<City, BroadcastTag> joinCity = broadcastTag.join("city", JoinType.LEFT);

        cq.multiselect(broadcastTag.<String> get("id"), broadcastTag.get("broadcast").<String> get("id"), broadcastTag.get("broadcast").<String> get("messageId"), broadcastTag.get("broadcast")
                .<BroadcastStatus> get("status"), broadcastTag.get("broadcast").<Date> get("createdTime"), broadcastTag.get("broadcast").<String> get("createdBy"), broadcastTag.get("broadcast")
                .<Date> get("updatedTime"), broadcastTag.get("broadcast").<String> get("updatedBy"), broadcastTag.<String> get("title"), broadcastTag.<Long> get("size"), broadcastTag
                .<PropertyTransactionType> get("transactionType"), broadcastTag.<BroadcastType> get("broadcastType"), broadcastTag.<CommissionType> get("commissionType"), broadcastTag
                .<String> get("customSublocation"), broadcastTag.<Long> get("maximumPrice"), broadcastTag.<Long> get("minimumPrice"), broadcastTag.<PropertyType> get("propertyType"), broadcastTag
                .<Rooms> get("rooms"), broadcastTag.get("location").<String> get("id"), broadcastTag.get("location").<String> get("localityName"),
                broadcastTag.get("location").<String> get("locationDisplayName"), joinCity.<String> get("id"), joinCity.<String> get("cityName"), broadcastTag.<Long> get("price"), broadcastTag
                        .<String> get("createdBy"), broadcastTag.<String> get("updatedBy"), broadcastTag.<Date> get("createdTime"), broadcastTag.<Date> get("updatedTime"), broadcastTag
                        .<BroadcastTagStatus> get("status"), broadcastTag.<String> get("draftedBy"), broadcastTag.<Date> get("draftedTime"), broadcastTag.<String> get("addedBy"), broadcastTag
                        .<Date> get("addedTime"), broadcastTag.<Long> get("maximumSize"), broadcastTag.<Long> get("minimumSize"), broadcastTag.<PropertySubType> get("propertySubType"), broadcastTag
                        .<PropertyMarket> get("propertyMarket"), broadcastTag.<String> get("closedBy"), broadcastTag.<Date> get("closedTime"), broadcastTag.<String> get("discardReason"));

        List<Predicate> predicates = new ArrayList<Predicate>();

        if (locationIds != null && !locationIds.isEmpty())
        {
            predicates.add(broadcastTag.get("location").<String> get("id").in(locationIds));
        }

        if (cityIds != null && !cityIds.isEmpty())
        {
            predicates.add(broadcastTag.get("cityId").in(cityIds));
        }

        // FILTERS

        if (filter != null && !filter.isEmpty())
        {
            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                Set<String> values = entry.getValue();
                Path<Object> leftClause = null;
                Root root = broadcastTag;

                if (values != null && !values.isEmpty())
                {
                    if (field.equals(FilterField.MESSAGE_CREATED_BY) || field.equals(FilterField.BROADCAST_ID) || field.equals(FilterField.CITY_NAME))
                    {
                        for (String s : field.getDbDomainNames())
                        {
                            if (leftClause == null)
                            {
                                leftClause = root.get(s);
                            }
                            else
                            {
                                leftClause = leftClause.get(s);
                            }
                        }
                        predicates.add(cb.or(leftClause.in(field.castIntoType(values))));
                    }
                    else if (field.equals(FilterField.BRAODCAST_TAG_ADDED_TIME) || field.equals(FilterField.CREATED_TIME) || field.equals(FilterField.BRAODCAST_TAG_CLOSED_TIME)
                            || field.equals(FilterField.DRAFTED_TIME))
                    {
                        Set<?> date = field.castIntoType(values);
                        for (Object object : date)
                        {
                            Date fieldToDate = (Date) object;
                            Calendar fromDate = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
                            // reset hour, minutes, seconds and millis
                            fromDate.setTime(fieldToDate);
                            fromDate.set(Calendar.HOUR_OF_DAY, 0);
                            fromDate.set(Calendar.MINUTE, 0);
                            fromDate.set(Calendar.SECOND, 0);
                            fromDate.set(Calendar.MILLISECOND, 0);
                            fromDate.add(Calendar.DAY_OF_MONTH, 1);
                            Date endDate = fromDate.getTime();

                            predicates.add(cb.and(cb.between(root.get(field.dbField()), fieldToDate, endDate)));
                        }

                    }
                    else if (field.equals(FilterField.SIZE))
                    {
                        Set<?> minMaxSize = field.castIntoType(values);
                        List<String> list = new ArrayList(minMaxSize);
                        if (list.size() == 1)
                            list.add(list.get(0));
                        String minSizeStr = list.get(0);
                        String maxSizeStr = list.get(1);
                        List<Long> minMaxSizeList = new ArrayList<Long>();
                        minMaxSizeList.add(Long.parseLong(minSizeStr));
                        minMaxSizeList.add(Long.parseLong(maxSizeStr));

                        Collections.sort(minMaxSizeList);
                        predicates.add(cb.and(cb.between(root.get(field.dbField()), minMaxSizeList.get(0), minMaxSizeList.get(1))));
                    }
                    else
                    {
                        leftClause = root.get(field.dbField());
                        predicates.add(cb.or(leftClause.in(field.castIntoType(values))));
                    }

                }
            }

        }

        // SORT

        if (sort != null && !sort.isEmpty())
        {
            for (Entry<SortField, SortType> entry : sort.entrySet())
            {
                SortField field = entry.getKey();
                SortType type = entry.getValue();

                Order order = null;
                Path<Object> leftClause = null;
                Root root = broadcastTag;

                if (field.equals(SortField.MESSAGE_CREATED_BY) || field.equals(SortField.BROADCAST_ID) || field.equals(SortField.CITY_NAME))
                {
                    for (String s : field.getDbDomainNames())
                    {
                        if (leftClause == null)
                        {
                            leftClause = root.get(s);
                        }
                        else
                        {
                            leftClause = leftClause.get(s);
                        }
                    }
                }
                else
                {
                    leftClause = root.get(field.dbField());
                }

                if (type.equals(SortType.ASCENDING))
                {
                    order = cb.asc(leftClause);
                }
                else
                {
                    order = cb.desc(leftClause);
                }

                cq.orderBy(order);
            }
        }

        // predicates.add(cb.equal(broadcastTag.get("cityId"),
        // fromCity.get("id")));

        if (!predicates.isEmpty())
        {
            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        TypedQuery<BroadcastTagsConsolidated> q = entityManager.createQuery(cq);

        q.setFirstResult(start);
        q.setMaxResults(rows);

        return q.getResultList();

        // TypedQuery<BroadcastTagsConsolidated> typedQuery = null;
        // if (locationIds == null || cityIds == null || cityIds.isEmpty() ||
        // locationIds.isEmpty())
        // {
        // typedQuery =
        // getEntityManager().createQuery(BroadcastTagsConsolidated.QUERY_FIND_ALL_BROADCAST_IDS,
        // BroadcastTagsConsolidated.class);
        // }
        // else
        // {
        // typedQuery =
        // getEntityManager().createQuery(BroadcastTagsConsolidated.QUERY_FIND_BROADCAST_IDS_BY_LOCATION_AND_CITY_LIST,
        // BroadcastTagsConsolidated.class);
        // typedQuery.setParameter("locationIdList", locationIds);
        // typedQuery.setParameter("cityIdList", cityIds);
        // }
        //
        // typedQuery.setFirstResult(0);
        // typedQuery.setMaxResults(100);
        // return typedQuery.getResultList();
    }

    public List<BroadcastObject> searchBroadcastTypePropertyListing(PropertyType propertType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer minArea, Integer maxArea, Long maxPrice, Long minPrice, List<Rooms> roomList, PropertySubType propertSubType, List<BroadcastStatus> broadcastStatusList,
            List<BroadcastTagStatus> broadcastTagStatusList, int limit, int offset)
    {

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<BroadcastObject> cq = cb.createQuery(BroadcastObject.class);
        Root<BroadcastTag> broadcastTag = cq.from(BroadcastTag.class);
        cq.multiselect(broadcastTag.get("broadcastId"), broadcastTag.<Date> get("createdTime"), broadcastTag.<Date> get("updatedTime"));

        List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(broadcastTag.get("cityId").in(cityId));
        // predicates.add(broadcastTag.get("broadcast").get("status").in(broadcastStatusList));
        predicates.add(broadcastTag.get("status").in(broadcastTagStatusList));
        if (propertType != null)
        {
            predicates.add(broadcastTag.get("propertyType").in(propertType));
        }

        if (propertSubType != null)
        {
            predicates.add(broadcastTag.get("propertySubType").in(propertSubType));
        }

        if (broadcastType != null)
        {
            predicates.add(broadcastTag.get("broadcastType").in(broadcastType));
            if (broadcastType.equals(BroadcastType.PROPERTY_LISTING))
            {
                if (minPrice != null && maxPrice != null)
                {
                    predicates.add(cb.and(cb.ge(broadcastTag.<Integer> get("price"), minPrice), cb.le(broadcastTag.<Integer> get("price"), maxPrice)));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("price"), -1));
                }
                else if (minPrice != null && maxPrice == null)
                {
                    predicates.add(cb.ge(broadcastTag.<Integer> get("price"), minPrice));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("price"), -1));
                }
                else if (minPrice == null && maxPrice != null)
                {
                    predicates.add(cb.le(broadcastTag.<Integer> get("price"), maxPrice));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("price"), -1));
                }

                if (minArea != null && maxArea != null)
                {
                    predicates.add(cb.and(cb.ge(broadcastTag.<Integer> get("size"), minArea), cb.le(broadcastTag.<Integer> get("size"), maxArea)));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("size"), -1));
                }
                else if (minArea != null && maxArea == null)
                {
                    predicates.add(cb.ge(broadcastTag.<Integer> get("size"), minArea));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("size"), -1));
                }
                else if (minArea == null && maxArea != null)
                {
                    predicates.add(cb.le(broadcastTag.<Integer> get("size"), maxArea));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("size"), -1));
                }
            }
            else if (broadcastType.equals(BroadcastType.REQUIREMENT))
            {
                if (minPrice != null && maxPrice != null)
                {
                    predicates.add(cb.and(cb.le(broadcastTag.<Integer> get("minimumPrice"), maxPrice), cb.ge(broadcastTag.<Integer> get("maximumPrice"), minPrice)));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("minimumPrice"), -1));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("maximumPrice"), -1));
                }
                else if (minPrice != null && maxPrice == null)
                {
                    predicates.add(cb.ge(broadcastTag.<Integer> get("minimumPrice"), minPrice));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("minimumPrice"), -1));
                }
                else if (minPrice == null && maxPrice != null)
                {
                    predicates.add(cb.le(broadcastTag.<Integer> get("maximumPrice"), maxPrice));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("maximumPrice"), -1));
                }

                if (minArea != null && maxArea != null)
                {
                    predicates.add(cb.and(cb.le(broadcastTag.<Integer> get("minimumSize"), maxArea), cb.ge(broadcastTag.<Integer> get("maximumSize"), minArea)));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("minimumSize"), -1));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("maximumSize"), -1));
                }
                else if (minArea != null && maxArea == null)
                {
                    predicates.add(cb.ge(broadcastTag.<Integer> get("minimumSize"), minArea));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("minimumSize"), -1));

                }
                else if (minArea == null && maxArea != null)
                {
                    predicates.add(cb.le(broadcastTag.<Integer> get("maximumSize"), maxArea));
                    predicates.add(cb.notEqual(broadcastTag.<Integer> get("maximumSize"), -1));
                }
            }
        }
        else
        {
            if (minPrice != null && maxPrice != null)
            {
                List<Predicate> pricePredicate = new ArrayList<Predicate>();
                pricePredicate.add(cb.and(cb.and(cb.le(broadcastTag.<Integer> get("minimumPrice"), maxPrice), cb.ge(broadcastTag.<Integer> get("maximumPrice"), minPrice),
                        cb.notEqual(broadcastTag.<Integer> get("minimumPrice"), -1), cb.notEqual(broadcastTag.<Integer> get("maximumPrice"), -1))));

                pricePredicate.add(cb.and(cb.and(cb.ge(broadcastTag.<Integer> get("price"), minPrice), cb.le(broadcastTag.<Integer> get("price"), maxPrice),
                        cb.notEqual(broadcastTag.<Integer> get("price"), -1))));

                predicates.add(cb.or(pricePredicate.toArray(new Predicate[pricePredicate.size()])));
            }
            else if (minPrice != null && maxPrice == null)
            {
                List<Predicate> pricePredicate = new ArrayList<Predicate>();
                pricePredicate.add(cb.and(cb.ge(broadcastTag.<Integer> get("minimumPrice"), minPrice), cb.notEqual(broadcastTag.<Integer> get("minimumPrice"), -1)));
                pricePredicate.add(cb.and(cb.ge(broadcastTag.<Integer> get("price"), minPrice), cb.notEqual(broadcastTag.<Integer> get("price"), -1)));
                predicates.add(cb.or(pricePredicate.toArray(new Predicate[pricePredicate.size()])));
            }
            else if (minPrice == null && maxPrice != null)
            {
                List<Predicate> pricePredicate = new ArrayList<Predicate>();
                pricePredicate.add(cb.and(cb.le(broadcastTag.<Integer> get("maximumPrice"), maxPrice), cb.notEqual(broadcastTag.<Integer> get("maximumPrice"), -1)));
                pricePredicate.add(cb.and(cb.le(broadcastTag.<Integer> get("price"), maxPrice), cb.notEqual(broadcastTag.<Integer> get("price"), -1)));
                predicates.add(cb.or(pricePredicate.toArray(new Predicate[pricePredicate.size()])));
            }

            if (minArea != null && maxArea != null)
            {
                List<Predicate> sizePredicate = new ArrayList<Predicate>();
                sizePredicate.add(cb.and(cb.and(cb.le(broadcastTag.<Integer> get("minimumSize"), maxArea), cb.ge(broadcastTag.<Integer> get("maximumSize"), minArea),
                        cb.notEqual(broadcastTag.<Integer> get("minimumSize"), -1), cb.notEqual(broadcastTag.<Integer> get("maximumSize"), -1))));
                sizePredicate.add(cb.and(cb.and(cb.ge(broadcastTag.<Integer> get("size"), minArea), cb.le(broadcastTag.<Integer> get("size"), maxArea),
                        cb.notEqual(broadcastTag.<Integer> get("size"), -1))));
                predicates.add(cb.or(sizePredicate.toArray(new Predicate[sizePredicate.size()])));
            }
            else if (minArea != null && maxArea == null)
            {
                List<Predicate> sizePredicate = new ArrayList<Predicate>();
                sizePredicate.add(cb.and(cb.ge(broadcastTag.<Integer> get("minimumSize"), minArea), cb.notEqual(broadcastTag.<Integer> get("minimumSize"), -1)));
                sizePredicate.add(cb.and(cb.ge(broadcastTag.<Integer> get("size"), minArea), cb.notEqual(broadcastTag.<Integer> get("size"), -1)));
                predicates.add(cb.or(sizePredicate.toArray(new Predicate[sizePredicate.size()])));
            }
            else if (minArea == null && maxArea != null)
            {
                List<Predicate> sizePredicate = new ArrayList<Predicate>();
                sizePredicate.add(cb.and(cb.le(broadcastTag.<Integer> get("maximumSize"), maxArea), cb.notEqual(broadcastTag.<Integer> get("maximumSize"), -1)));
                sizePredicate.add(cb.and(cb.le(broadcastTag.<Integer> get("size"), maxArea), cb.notEqual(broadcastTag.<Integer> get("size"), -1)));
                predicates.add(cb.or(sizePredicate.toArray(new Predicate[sizePredicate.size()])));
            }
        }
        List<Predicate> predicatesRooms = new ArrayList<Predicate>();
        if (roomList != null && !roomList.isEmpty())
        {
            for (Rooms room : roomList)
            {
                predicatesRooms.add(cb.equal(broadcastTag.get("rooms"), room));
            }
        }
        if (transactionType != null)
        {
            predicates.add(broadcastTag.get("transactionType").in(transactionType));
        }

        if (location != null)
        {
            predicates.add(broadcastTag.get("location").in(location));
        }

        if (!predicatesRooms.isEmpty())
        {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])), cb.and(cb.or(predicatesRooms.toArray(new Predicate[predicatesRooms.size()]))));
        }
        else
        {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        }

        cq.orderBy(cb.desc(broadcastTag.<Date> get("updatedTime")));
        TypedQuery<BroadcastObject> q = entityManager.createQuery(cq);

        q.setFirstResult(offset);
        q.setMaxResults(limit);

        return q.getResultList();
    }

    public List<BroadcastObject> searchBroadcastTypeRequirement(PropertyType propertyType, BroadcastType broadcastType, PropertyTransactionType transactionType, Integer cityId, Location location,
            Integer area, Long price)
    {

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<BroadcastObject> cq = cb.createQuery(BroadcastObject.class);
        Root<BroadcastTag> broadcastTag = cq.from(BroadcastTag.class);
        cq.multiselect(broadcastTag.get("broadcast").<String> get("id"), broadcastTag.get("broadcast").<Date> get("createdTime"), broadcastTag.get("broadcast").<Date> get("updatedTime"));

        List<Predicate> predicates = new ArrayList<Predicate>();

        if (propertyType != null)
        {
            predicates.add(broadcastTag.get("propertyType").in(propertyType));
        }

        predicates.add(broadcastTag.get("broadcastType").in(broadcastType));
        predicates.add(broadcastTag.get("cityId").in(cityId));

        if (transactionType != null)
        {
            predicates.add(broadcastTag.get("transactionType").in(transactionType));
        }

        if (price != null)
        {
            predicates.add(cb.and(cb.ge(broadcastTag.<Integer> get("minPrice"), price), cb.le(broadcastTag.<Integer> get("maxPrice"), price)));
        }

        if (area != null)
        {
            predicates.add(cb.and(cb.ge(broadcastTag.<Integer> get("minSize"), area), cb.le(broadcastTag.<Integer> get("maxSize"), area)));
        }

        if (location != null)
        {
            predicates.add(broadcastTag.get("location").in(location));
        }

        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        cq.orderBy(cb.desc(broadcastTag.get("broadcast").<Date> get("updatedTime")));

        TypedQuery<BroadcastObject> q = entityManager.createQuery(cq);
        return q.getResultList();
    }

    public List<String> getBroadcastTagIdsByBroadcastId(String broadcastId)
    {
        Validation.notNull(broadcastId, "broadcastId should not be null.");
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(BroadcastTag.QUERY_FIND_BROADCAST_TAG_IDS_BY_BROADCAST_ID, String.class);
        typedQuery.setParameter("broadcastId", broadcastId);

        return typedQuery.getResultList();
    }

    public void deleteBroadcasTagsByIds(List<String> broadcastTagids)
    {
        if (!broadcastTagids.isEmpty())
        {
            for (String ids : broadcastTagids)
            {
                removeById(ids);
            }
        }
    }

    public List<BroadcastObject> searchBroadcastByCityId(Integer cityId, int offset, int limit)
    {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<BroadcastObject> cq = cb.createQuery(BroadcastObject.class);
        Root<BroadcastTag> broadcastTag = cq.from(BroadcastTag.class);
        cq.multiselect(broadcastTag.get("broadcast").<String> get("id"), broadcastTag.get("broadcast").<Date> get("createdTime"), broadcastTag.get("broadcast").<Date> get("updatedTime"));

        List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(broadcastTag.get("cityId").in(cityId));

        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        cq.orderBy(cb.desc(broadcastTag.get("broadcast").<Date> get("updatedTime")));

        TypedQuery<BroadcastObject> q = entityManager.createQuery(cq);
        q.setFirstResult(offset);
        q.setMaxResults(limit);
        return q.getResultList();
    }

    public void saveBroadcastTags(Collection<BroadcastTag> tagCollection)
    {
        if (tagCollection == null || tagCollection.isEmpty())
        {
            return;
        }

        persistAll(tagCollection);

    }

    public List<BroadcastTag> getBroadcastTagsByIds(Set<String> broadcastTagIds)
    {
        if (broadcastTagIds == null || broadcastTagIds.isEmpty())
        {
            return new ArrayList<BroadcastTag>();
        }
        return findByIds(broadcastTagIds);
    }

    public Long fetchBroadcastCountByCity(String cityId)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("cityId", cityId);
        parameters.put("status", BroadcastTagStatus.ADDED);
        return getCountByNamedQuery(BroadcastTag.QUERY_FIND_COUNT_OF_BROADCAST_BY_CITY_ID, parameters);
    }
}
