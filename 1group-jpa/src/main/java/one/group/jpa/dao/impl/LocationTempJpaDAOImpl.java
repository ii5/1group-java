/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.LocationTmp;
import one.group.jpa.dao.LocationTempJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class LocationTempJpaDAOImpl extends JPABaseDAOImpl<String, LocationTmp> implements LocationTempJpaDAO
{

    public List<LocationTmp> fetchLocationTmpByCityId(String cityId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByParentId(String parentId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByLoginUserID(String loginUserId)
    {
        return null;
    }

    public List<LocationTmp> fetchLocationTmpByStatus(String status)
    {
        return null;
    }

    public LocationTmp fetchLocationTmpByLocationTmpId(String locationTmpId)
    {
        return findById(locationTmpId);
    }

}
