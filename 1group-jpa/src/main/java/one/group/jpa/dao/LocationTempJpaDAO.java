/**
 * 
 */
package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.LocationTmp;

/**
 * @author ashishthorat
 *
 */
public interface LocationTempJpaDAO
{
    public List<LocationTmp> fetchLocationTmpByCityId(String cityId);

    public List<LocationTmp> fetchLocationTmpByParentId(String parentId);

    public List<LocationTmp> fetchLocationTmpByLoginUserID(String loginUserId);

    public List<LocationTmp> fetchLocationTmpByStatus(String status);

    public LocationTmp fetchLocationTmpByLocationTmpId(String locationTmpId);
}
