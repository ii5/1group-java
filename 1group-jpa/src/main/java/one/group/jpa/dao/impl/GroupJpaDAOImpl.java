/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.GroupMembers;
import one.group.jpa.dao.GroupJpaDAO;

public class GroupJpaDAOImpl extends JPABaseDAOImpl<String, GroupMembers> implements GroupJpaDAO
{

    public void saveGroup(GroupMembers groupMembers)
    {
        saveOrUpdate(groupMembers);

    }

}
