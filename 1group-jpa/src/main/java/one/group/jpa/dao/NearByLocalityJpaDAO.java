package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.NearByLocation;

/**
 * 
 * @author miteshchavda
 *
 */
public interface NearByLocalityJpaDAO extends JPABaseDAO<String, NearByLocation>
{
    /**
     * Fetch near by localities of location
     * 
     * @param locationId
     * @return
     */
    public List<NearByLocation> fetchLocalityByLocationId(String locationId);

}
