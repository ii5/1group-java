package one.group.jpa.dao;

import java.util.List;
import java.util.Set;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface AccountRelationsJpaDAO extends JPABaseDAO<String, AccountRelations>
{

    public void updateAccountRelation(AccountRelations accountRelation);

    public void createAccountRelation(AccountRelations accountRelation);

    public List<AccountRelations> fetchAccountRelations(String accountId);
    
    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList, String query);

    public Set<AccountRelations> fetchAccountRelations(Account account);

    public boolean isAccountRelationExist(AccountRelations accountRelation);

    public AccountRelations fetchAccountRelations(String accountId, String otherAccountId);

    public List<AccountRelations> fetchByRequestByAccountAndToAccount(Account requestByAccount, Account otherAccount);

    public AccountRelations fetchByAccountAndOtherAccount(String requestByAccount, String otherAccount);

    public List<AccountRelations> fetchAccountRelationBetweenAccounts(String requestByAccountId, String otherAccountId);

	public List<String> fetchContactsByAccountId(String accountId);
}
