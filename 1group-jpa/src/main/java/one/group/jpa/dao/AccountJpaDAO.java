package one.group.jpa.dao;

import java.util.List;
import java.util.Set;

import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.helpers.AccountObject;

/**
 * 
 * @author nyalfernandes
 * 
 */
public interface AccountJpaDAO extends JPABaseDAO<String, Account>
{
    /**
     * Returns the Account details from the client id.
     * 
     * @param clientId
     * @return
     */
    public Account getAccountFromClientId(String clientId);

    /**
     * 
     * @param mobileNumber
     * @return
     */
    public Account fetchAccountByPhoneNumber(String mobileNumber);

    /**
     * 
     * @return
     */
    public List<Account> getAllAccounts();

    /**
     * 
     * @param accountId
     * @return
     */
    public Account fetchShortReferenceById(String accountId);

    /**
     * 
     * @param shortReference
     * @return
     */
    public Account fetchAccountByShortReference(String shortReference);

    public Set<Account> fetchAccountsByPhoneNumbers(Set<String> mobileNumbers);

    public long fetchCount();

    /**
     * Fetch all accounts by status
     * 
     * @param status
     * @return
     */
    public List<Account> fetchAllAccountsByStatus(Status status);

    public List<Account> fetchAllStatusOfNativeContactsByAccountId(String accountId);

    /**
     * 
     * @param locationIds
     * @return
     */
    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds);

    public List<AccountObject> fetchAllAccountIdsByStatus(Status status);

    /**
     * Fetch all accounts by ids
     * 
     * @param accountIds
     * @return
     */
    public List<Account> fetchAllAccountByIds(Set<String> accountIds);

    /**
     * Fetch account by id
     * 
     * @param userId
     * @return
     */
    public Account fetchAccountByUserId(String userId);

    /**
     * Fetch account by waPhoneNumber
     * 
     * @param userId
     * @return
     */
    public Account fetchAccountBywaPhoneNumber(String waPhoneNumber);

    /**
     * Fetch accounts by waSyncStatus
     * 
     * @param userId
     * @return
     */
    public List<Account> fetchAccountByWaSyncStatus(String waSyncStatus);

    /**
     * Fetch accounts by waGroupSelectionStatus
     * 
     * @param userId
     * @return
     */
    public List<Account> fetchAccountByWaGroupSelectionStatus(String waGroupSelectionStatus);

    /**
     * Fetch ActiveGroupCount by userId
     * 
     * @param userId
     * @return
     */
    public int getActiveGroupCountByUserId(long userId);

    /**
     * Fetch ApprovedGroupCount by userId
     * 
     * @param userId
     * @return
     */
    public int getApprovedGroupCountByUserId(long userId);

    /**
     * 
     * @param mobileNumber
     * @return
     */
    public Account fetchUserByNumber(String mobileNumber);

    public Set<String> fetchAllPhoneNumbersOfNativeContactsByAccountId(String accountId);

    public Account fetchAccountByAccountId(String accountId);
    
    public List<Account> fetchSelectiveDataAccountsByIds(Set<String> accountIds);
}
