package one.group.jpa.dao;

import java.util.List;

import one.group.core.enums.StarType;
import one.group.entities.jpa.Star;

public interface StarJpaDAO extends JPABaseDAO<String, Star>
{
    /**
     * 
     * @param referenceId
     * @param targetEntityId
     * @param type
     * @return
     */
    public Star doStar(String referenceId, String targetEntityId, StarType type);

    /**
     * 
     * @param referenceId
     * @param type
     * @return
     */
    List<Star> getAllStarsByReference(String referenceId, StarType type);

    //
    // /**
    // *
    // * @param referenceId
    // * @param targetEntityId
    // * @return
    // */
    // public Star getStarByReferenceAndTargetEntity(String referenceId, String
    // targetEntityId);
    //
    // /**
    // *
    // * @param accountId
    // * @param propertyListings
    // * @return
    // */
    // public List<Star> checkIsStarredForBroadcasts(String accountId,
    // List<String> broadcastIds);

    public void unStarBroadcast(Star star);

    public Star getStarByReferenceAndTargetEntity(String referenceId, String targetEntityId);
}
