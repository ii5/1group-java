package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.AuthAssignment;

public interface AuthAssignmentJpaDAO
{
    public void saveAuthAssignment(AuthAssignment authAssignment);

    public List<AuthAssignment> fetchByUserId(String userId);
    
    public List<AuthAssignment> fetchAuthAssignmentItemsByUserIdList(Collection<String> userIdList);
    
    public List<String> fetchAuthAssignmentItemNamesByUserId(String userId);

    public void deleteAssignmentOfId(Collection<String> userId);

	public List<String> fetchUserIdsByRoles(Collection<String> roles);
}
