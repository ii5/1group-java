package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import one.group.core.enums.AccountType;
import one.group.core.enums.status.Status;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.Agent;
import one.group.entities.jpa.Client;
import one.group.entities.jpa.helpers.AccountObject;
import one.group.jpa.dao.AccountJpaDAO;
import one.group.jpa.dao.ClientJpaDAO;
import one.group.jpa.dao.PhoneNumberJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class AccountJpaDAOImpl extends JPABaseDAOImpl<String, Account> implements AccountJpaDAO
{

    private ClientJpaDAO clientJpaDAO;

    private PhoneNumberJpaDAO phoneNumberJpaDAO;

    public ClientJpaDAO getClientJpaDAO()
    {
        return clientJpaDAO;
    }

    public void setClientJpaDAO(ClientJpaDAO clientJpaDAO)
    {
        this.clientJpaDAO = clientJpaDAO;
    }

    public PhoneNumberJpaDAO getPhoneNumberJpaDAO()
    {
        return phoneNumberJpaDAO;
    }

    public void setPhoneNumberJpaDAO(PhoneNumberJpaDAO phoneNumberJpaDAO)
    {
        this.phoneNumberJpaDAO = phoneNumberJpaDAO;
    }

    public Account getAccountFromClientId(final String clientId)
    {
        Client c = clientJpaDAO.findById(clientId);
        Account account = (c == null) ? null : c.getAccount();
        return account;
    }

    public Account fetchAccountByPhoneNumber(String mobileNumber)
    {
        Account accObj = null;

        try
        {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("number", mobileNumber);
            accObj = findByNamedQuery(Account.QUERY_FIND_BY_NUMBER, parameters);
        }
        catch (NoResultException nre)
        {
        }
        return accObj;
    }

    public List<Account> getAllAccounts()
    {
        // TODO : use named query
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> root = query.from(Account.class);
        query.select(root);
        List<Account> account = getEntityManager().createQuery(query).getResultList();

        return account;
    }

    public Account createNewAccount(HashMap<String, Object> newAccount)
    {
        Account accountobj = new Agent();

        accountobj.setType((AccountType) newAccount.get("type"));
        accountobj.setStatus((Status) newAccount.get("status"));
        accountobj.setPrimaryPhoneNumber(phoneNumberJpaDAO.findById(newAccount.get("primaryphonenumber_id").toString()));
        // TODO: check in db for null entries
        accountobj.setAddress(newAccount.get("address").toString());
        accountobj.setEmail(newAccount.get("email").toString());
        accountobj.setFullName(newAccount.get("firstname").toString());
        accountobj.setCompanyName(newAccount.get("lastname").toString());
        // accountobj.setEstablishmentName(newAccount.get("establishmentName").toString());

        saveOrUpdate(accountobj);
        return accountobj;
    }

    public Account fetchShortReferenceById(String accountId)
    {
        return findById(accountId);
    }

    public Account fetchAccountByShortReference(String shortReference)
    {
        Validation.notNull(shortReference, "Short reference should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("shortreference", shortReference);

        return findByNamedQuery(Account.QUERY_FIND_BY_SHORTREFERENCE, parameters);
    }

    public Set<Account> fetchAccountsByPhoneNumbers(Set<String> mobileNumbers)
    {

        if (mobileNumbers == null || mobileNumbers.isEmpty())
        {
            return new HashSet<Account>();
        }

        Set<Account> accounts = new HashSet<Account>();
        Map<String, Set<String>> parameters = new HashMap<String, Set<String>>();
        parameters.put("mobileNumbers", mobileNumbers);
        accounts.addAll(findAllByNamedQuery(Account.QUERY_FIND_BY_NUMBERS, parameters));
        return accounts;

    }

    public long fetchCount()
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<Status> statusList = new ArrayList<Status>();
        statusList.add(Status.ACTIVE);
        statusList.add(Status.SEEDED);
        parameters.put("accountStatus", statusList);
        return getCountByNamedQuery(Account.QUERY_TOTAL_COUNT, parameters);

    }

    public List<Account> fetchAllAccountsByStatus(Status status)
    {
        Validation.notNull(status, "Status should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("status", status);

        return findAllByNamedQuery(Account.QUERY_ALL_ACCOUNT_BY_STATUS, parameters);
    }

    public List<Account> fetchAllStatusOfNativeContactsByAccountId(String accountId)
    {
        Validation.notNull(accountId, "AccountId should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(Account.QUERY_FIND_ACCOUNTS_OF_CONTACTS_BY_ACCOUNT, parameters);
    }

    public List<String> fetchAllAccountIdsSubscribedToLocations(List<String> locationIds)
    {
        Validation.notNull(locationIds, "The location Id list passed to fetch its associated accounts should not be null.");

        if (locationIds.isEmpty())
        {
            return new ArrayList<String>();
        }
        return new ArrayList();
    }

    public List<AccountObject> fetchAllAccountIdsByStatus(Status status)
    {
        TypedQuery<AccountObject> typedQuery = getEntityManager().createQuery(AccountObject.QUERY_FETCH_ALL_ACCOUNT_IDS_BY_STATUS, AccountObject.class);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();
    }

    public List<Account> fetchAllAccountByIds(Set<String> accountIds)
    {
        Validation.notNull(accountIds, "AccountId should not be null.");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountIds", accountIds);

        return findAllByNamedQuery(Account.QUERY_ALL_ACCOUNT_BY_IDS, parameters);
    }
    
    
    public List<Account> fetchSelectiveDataAccountsByIds(Set<String> accountIds)
    {
        Validation.notNull(accountIds, "AccountId should not be null.");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountIds", accountIds);

        return findAllByNamedQuery(Account.QUERY_ALL_ACCOUNT_BY_IDS, parameters);
    }

    public Account fetchAccountByUserId(String userId)
    {
        return null;
    }

    public Account fetchAccountBywaPhoneNumber(String waPhoneNumber)
    {
        return null;
    }

    public List<Account> fetchAccountByWaSyncStatus(String waSyncStatus)
    {
        return null;
    }

    public List<Account> fetchAccountByWaGroupSelectionStatus(String waGroupSelectionStatus)
    {
        return null;
    }

    public int getActiveGroupCountByUserId(long userId)
    {
        return 0;
    }

    public int getApprovedGroupCountByUserId(long userId)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    public Account fetchUserByNumber(String mobileNumber)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("mobileNumber", mobileNumber);

        List<Account> accounts = findAllByNamedQuery(Account.QUERY_FIND_USER_BY_MOBILE_NUMBER_OR_WANUMBER, params);

        if (accounts == null || accounts.isEmpty())
        {
            return null;
        }

        if (accounts != null && accounts.size() > 1)
        {
            throw new IllegalStateException("Multiple accounts with same number found.");
        }

        return accounts.get(0);
    }

    public Set<String> fetchAllPhoneNumbersOfNativeContactsByAccountId(String accountId)
    {
        Validation.notNull(accountId, "AccountId should not be null.");
        Set<String> phoneNumbers = new HashSet<String>();
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(Account.QUERY_FIND_PHONENUMBER_OF_CONTACTS_BY_ACCOUNT, String.class);
        typedQuery.setParameter("accountId", accountId);
        List<String> phoneNumberList = typedQuery.getResultList();
        for (String phoneNumber : phoneNumberList)
        {
            phoneNumbers.add(phoneNumber);
        }
        return phoneNumbers;
    }

    public Account fetchAccountByAccountId(String accountId)
    {
        Validation.notNull(accountId, "AccountId should not be null.");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        return findByNamedQuery(Account.QUERY_FIND_BY_ACCOUNT_ID, parameters);
    }

}
