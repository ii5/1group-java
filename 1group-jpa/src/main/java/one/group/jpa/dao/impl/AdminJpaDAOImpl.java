/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.AdminStatus;
import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.AuthAssignment;
import one.group.jpa.dao.AdminJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class AdminJpaDAOImpl extends JPABaseDAOImpl<String, Admin> implements AdminJpaDAO
{

    public List<Admin> fetchAdminByRoleId(String roleId)
    {
        return null;
    }

    public List<Admin> fetchAdminByManagerId(String managerId)
    {
        return null;
    }

    public List<Admin> fetchAdminByStatus(String status)
    {
        return null;
    }

    public void saveAdmin(Admin admin)
    {
        saveOrUpdate(admin);

    }

    public Admin fetchAdminById(String adminId)
    {
        return findById(adminId);

    }

    public List<Admin> fetchAdminUsersByRoleListAndAdminStatusList(List<String> roleList, List<AdminStatus> adminStatusList, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        if (roleList == null || roleList.isEmpty())
        {
            return new ArrayList<Admin>();
        }
        
        // SELECT a FROM Admin a, AuthAssignment aus WHERE aus.authItem.name IN :roleList AND aus.user = a.id AND a.status IN :adminStatusList
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);

        Root<Admin> fromAdmin = cq.from(Admin.class);
        Root<AuthAssignment> fromAssignment = cq.from(AuthAssignment.class);
        
        List<Predicate> predicateList = new ArrayList<Predicate>();
        
        if (roleList != null && !roleList.isEmpty())
        {
        	predicateList.add(fromAssignment.get("authItem").get("name").in(roleList));
        	predicateList.add(cb.equal(fromAdmin.get("id"), fromAssignment.get("user")));
        }
        
        if (adminStatusList != null && !adminStatusList.isEmpty())
        {
        	predicateList.add(fromAdmin.get("status").in(adminStatusList));
        }
        
        
        // SORT

        if (sort != null && !sort.isEmpty())
        {
            SortType type = sort.get(SortField.USERNAME);
            Order order = null;
            
            if (type != null)
            {
            	if (type.equals(SortType.ASCENDING))
            	{
            		order = cb.asc(fromAdmin.get("userName"));
            	}
            	else
            	{
            		order = cb.desc(fromAdmin.get("userName"));
            	}
            	
            	cq.orderBy(order);
            }
        }

        if (!predicateList.isEmpty())
        {
            cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }
        
        cq.select(fromAdmin);
        cq.distinct(true);
        
        TypedQuery<Admin> tq = getEntityManager().createQuery(cq);
        
        tq.setFirstResult(start);
        tq.setMaxResults(rows);

        return tq.getResultList();

//        TypedQuery<Admin> typedQuery = getEntityManager().createNamedQuery(Admin.QUERY_FIND_ADMIN_BY_ROLE_LIST_AND_ADMIN_STATUS_LIST, Admin.class);
//        typedQuery.setParameter("roleList", roleList);
//        typedQuery.setParameter("adminStatusList", adminStatusList);
//
//        return typedQuery.getResultList();

    }

    public List<Admin> fetchAdminUsersByInput(String username, List<AdminStatus> adminStatusList, String email, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

        CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);

        Root<Admin> from = cq.from(Admin.class);

        List<Predicate> predicateList = new ArrayList<Predicate>();

        if (username != null && !username.trim().isEmpty())
        {
            predicateList.add(cb.equal(from.get("userName"), username));
        }

        if (email != null && !email.trim().isEmpty())
        {
            predicateList.add(cb.equal(from.get("email"), email));
        }

        if (adminStatusList != null && !adminStatusList.isEmpty())
        {
            predicateList.add(from.get("status").in(adminStatusList));
        }

        // FILTERS

        if (filter != null && !filter.isEmpty())
        {
            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                Set<String> values = entry.getValue();

                predicateList.add(cb.or(from.get(field.dbField()).in(field.castIntoType(values))));
            }
        }

        // SORT

        if (sort != null && !sort.isEmpty())
        {
            for (Entry<SortField, SortType> entry : sort.entrySet())
            {
                SortField field = entry.getKey();
                SortType type = entry.getValue();

                Order order = null;
                if (type.equals(SortType.ASCENDING))
                {
                    order = cb.asc(from.get(field.dbField()));
                } else
                {
                    order = cb.desc(from.get(field.dbField()));
                }

                cq.orderBy(order);
            }
        }

        if (!predicateList.isEmpty())
        {
            cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }

        TypedQuery<Admin> tq = getEntityManager().createQuery(cq);
        
        tq.setFirstResult(start);
        tq.setMaxResults(rows);

        return tq.getResultList();

        // TypedQuery<Admin> typedQuery =
        // getEntityManager().createNamedQuery(Admin.QUERY_FIND_ADMIN_BY_USERNAME_AND_ADMIN_STATUS_LIST,
        // Admin.class);
        // typedQuery.setParameter("username", username);
        // typedQuery.setParameter("adminStatusList", adminStatusList);
        //
        // List<Admin> adminList = typedQuery.getResultList();
        //
        // if (adminList == null || adminList.isEmpty())
        // {
        // return null;
        // }
        //
        // return adminList.get(0);

    }

}
