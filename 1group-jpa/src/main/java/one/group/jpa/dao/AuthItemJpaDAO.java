/**
 * 
 */
package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.AuthItem;

/**
 * @author ashishthorat
 *
 */
public interface AuthItemJpaDAO
{
    public List<AuthItem> fetchAuthItemByType(String type);

    public AuthItem fetchAuthItemByRuleName(String ruleName);

    public AuthItem fetchAuthItemByGroupCode(String groupCode);

    public void saveAuthItem(AuthItem authItem);
    
    public Collection<AuthItem> fetchAuthItemByNameList(Collection<String> nameList);
}
