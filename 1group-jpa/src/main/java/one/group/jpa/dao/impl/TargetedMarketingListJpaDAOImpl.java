package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.TargetedMarketingList;
import one.group.jpa.dao.TargetedMarketingListJpaDAO;

/**
 * 
 * @author sanilshet
 *
 */
public class TargetedMarketingListJpaDAOImpl extends JPABaseDAOImpl<String, TargetedMarketingList> implements TargetedMarketingListJpaDAO
{

    public List<TargetedMarketingList> fetchAllTargetedList()
    {
        Map<String, String> parameters = new HashMap<String, String>();
        return findAllByNamedQuery(TargetedMarketingList.FIND_ALL, parameters);
    }

    public void deleteTargetedListByPhoneNumber(String phoneNumber)
    {
        removeById(phoneNumber);
    }

    public void deleteAllTargetedList()
    {
        removeAll();
    }

}
