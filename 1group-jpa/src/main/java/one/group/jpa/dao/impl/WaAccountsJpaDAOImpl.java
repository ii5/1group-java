/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.WaAccounts;
import one.group.jpa.dao.WaAccountsJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaAccountsJpaDAOImpl extends JPABaseDAOImpl<String, WaAccounts> implements WaAccountsJpaDAO
{
    public void saveWaAccounts(WaAccounts waAccounts)
    {
        persist(waAccounts);
    }
}
