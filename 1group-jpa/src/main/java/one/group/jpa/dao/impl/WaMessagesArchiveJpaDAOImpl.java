/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.WaMessagesArchive;
import one.group.jpa.dao.WaMessagesArchiveJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesArchiveJpaDAOImpl extends JPABaseDAOImpl<String, WaMessagesArchive> implements WaMessagesArchiveJpaDAO
{

    public List<WaMessagesArchive> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesArchive(WaMessagesArchive waMessagesArchive)
    {
        saveOrUpdate(waMessagesArchive);
    }
    
    public void saveWaMessagesArchive(Collection<WaMessagesArchive> waMessagesArchiveList) 
    {
    	persistAll(waMessagesArchiveList);
    }

    public WaMessagesArchive fetchWaMessagesArchiveByWaMessageId(String waMessageId)
    {
        return findById(waMessageId);
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesArchive> fetchWaMessagesArchiveByStatus(String status)
    {
        return null;
    }

    public boolean isWaMessagesArchiveDuplicate(String waMessageId)
    {
        return false;
    }

}
