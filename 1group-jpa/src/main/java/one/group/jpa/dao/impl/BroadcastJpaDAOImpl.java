/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import one.group.core.enums.BroadcastTagStatus;
import one.group.core.enums.status.BroadcastStatus;
import one.group.entities.jpa.Broadcast;
import one.group.entities.jpa.helpers.BroadcastCountObject;
import one.group.entities.jpa.helpers.BroadcastTagCountObject;
import one.group.exceptions.movein.Abstract1GroupException;
import one.group.jpa.dao.BroadcastJpaDAO;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class BroadcastJpaDAOImpl extends JPABaseDAOImpl<String, Broadcast> implements BroadcastJpaDAO
{

    public Broadcast getBroadcastByBroadcastId(String broadcastId, String accountId, String subscribe, String accountDetails, String locationDetails, String matchingDetails)
            throws Abstract1GroupException
    {
        return null;
    }

    public Broadcast getBroadcastByBroadcastId(String broadcastId)
    {
        return findById(broadcastId);
    }

    public void closeBroadcast(Broadcast broadcast) throws Abstract1GroupException
    {

    }

    public Boolean isBroadcastsExistOfAccount(String accountId)
    {
        return null;
    }

    public List<Broadcast> getBroadcasts(List<String> broadcastIds)
    {
        return null;
    }

    public Broadcast fetchBroadcast(String broadcastId)
    {
        return findById(broadcastId);
    }

    public Broadcast fetchByShortReference(String shortReference)
    {
        return null;
    }

    /**
     * save Broadcast
     * 
     * @param broadcast
     */
    public void saveBroadcast(Broadcast broadcast)
    {
        saveOrUpdate(broadcast, false);
    }

    public List<Broadcast> getBroadcastByBroadcastIds(List<String> broadcastIds, BroadcastStatus status)
    {
        Validation.notNull(broadcastIds, "broadcastIds should not be null.");
        Validation.notNull(status, "status should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("broadcastIds", broadcastIds);
        parameters.put("status", status);
        return findAllByNamedQuery(Broadcast.QUERY_FIND_ALL_BROADCAST_BY_BROADCAST_IDS, parameters);
    }

    public Broadcast getBroadcastByMessageId(String messageId)
    {
        Validation.notNull(messageId, "messageId should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("messageId", messageId);

        return findByNamedQuery(Broadcast.FIND_BROADCAST_BY_MESSAGE_ID, parameters);
    }

    public String fetchBroadcastIdByShortReference(String shortReference)
    {
        return null;
    }

    public String isBroadcastExist(String broadcastId)
    {
        return null;
    }

    public long fetchCount()
    {
        return getCountByNamedQuery(Broadcast.QUERY_TOTAL_COUNT, null);
    }

    public List<BroadcastCountObject> fetchBroadcastCountByAccountIds(List<String> accountIds)
    {
        TypedQuery<BroadcastCountObject> typedQuery = getEntityManager().createQuery(BroadcastCountObject.FETCH_ACCOUNTS_PROPERTY_LISTING_COUNT_OF_ACCOUNT_BY_STATUS, BroadcastCountObject.class);
        typedQuery.setParameter("accountIds", accountIds);
        return typedQuery.getResultList();
    }

    public List<Broadcast> fetchBroadcastByAccountId(String accountId)
    {
        Validation.notNull(accountId, "accountId should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(Broadcast.FIND_ALL_BROADCAST_BY_ACCOUNT_ID, parameters);
    }

    public List<Broadcast> fetchBroadcastByMessageCreatedBy(String messageCreatedBy, BroadcastStatus status)
    {
        Validation.notNull(messageCreatedBy, "messageCreatedBy should not be null.");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("messageCreatedBy", messageCreatedBy);
        parameters.put("status", status);

        return findAllByNamedQuery(Broadcast.QUERY_FIND_BY_MESSAGE_CREATED_BY, parameters);
    }

    public void removeRelationOfBroadcastAndMessageUpdateStatus(String broadcastId, String messageId, BroadcastStatus broadcastStatus)
    {
        Query query = getEntityManager().createNamedQuery(Broadcast.REMOVE_RELATION_BETWEEN_MESSAGE_AND_BROADCAST);
        query.setParameter("broadcastId", broadcastId);
        query.setParameter("messageId", messageId);
        query.setParameter("status", broadcastStatus);
        query.executeUpdate();
    }

    public void updateBroadcastViewCount(List<String> broadcastIdList)
    {
        Query query = getEntityManager().createNamedQuery(Broadcast.UPDATE_BROADCAST_VIEW_COUNT);
        query.setParameter("broadcastIdList", broadcastIdList);
        query.executeUpdate();
    }

    public List<BroadcastTagCountObject> fetchBroadcastTagCountByAccountIds(Set<String> accountIds, BroadcastTagStatus broadcastTagStatus, BroadcastStatus broadcastStatus)
    {
        TypedQuery<BroadcastTagCountObject> typedQuery = getEntityManager().createQuery(BroadcastTagCountObject.FETCH_ACCOUNTS_BROADCAST_TAG_COUNT_OF_ACCOUNT_BY_STATUS, BroadcastTagCountObject.class);
        typedQuery.setParameter("accountIds", accountIds);
        typedQuery.setParameter("broadcastTagStatus", broadcastTagStatus);
        typedQuery.setParameter("broadcastStatus", broadcastStatus);
        return typedQuery.getResultList();
    }
}
