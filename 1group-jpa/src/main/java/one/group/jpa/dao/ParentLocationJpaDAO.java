package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.ParentLocations;

/**
 * 
 * @author sanilshet
 *
 */
public interface ParentLocationJpaDAO extends JPABaseDAO<String, ParentLocations>
{
    /**
     * 
     * @param cityId
     * @return
     */
    public List<ParentLocations> getAllLocationsByCityId(String cityId);

	public List<ParentLocations> getAllParentLocations();

	public Collection<ParentLocations> getAllParentLocationsByCity(List<String> cityIds);
}
