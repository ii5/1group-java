package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.enums.status.NotificationStatus;
import one.group.entities.jpa.Notification;
import one.group.entities.jpa.NotificationTemplate;
import one.group.jpa.dao.NotificationJpaDAO;

public class NotificationJpaDAOImpl extends JPABaseDAOImpl<String, Notification> implements NotificationJpaDAO
{
    public long fetchCountByStatus(String userId, NotificationStatus status)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("createdForUserId", userId);
        parameters.put("status", status);
        return getCountByNamedQuery(Notification.QUERY_TOTAL_COUNT_BY_STATUS, parameters);

    }

    public List<NotificationTemplate> fetchNotificationTemplates()
    {

        return null;
    }
}
