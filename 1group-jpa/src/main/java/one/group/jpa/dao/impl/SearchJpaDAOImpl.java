package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Search;
import one.group.jpa.dao.SearchJpaDAO;
import one.group.utils.validation.Validation;

public class SearchJpaDAOImpl extends JPABaseDAOImpl<String, Search> implements SearchJpaDAO
{

    public void saveSearch(Search search)
    {
        saveOrUpdate(search);
    }

    public List<Search> fetchSearchByLocationList(List<String> locationIdList)
    {
        List<Search> searchList = new ArrayList<Search>();
        try
        {
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("locationList", locationIdList);
            searchList = findAllByNamedQuery(Search.QUERY_FIND_BY_LOCATIONS, parameters);
        }
        catch (NoResultException nre)
        {
        }
        return searchList;
    }

    public Search fetchSearchByRequirementName(String requirementName, String accountId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("requirementName", requirementName);
        parameters.put("accountId", accountId);
        return findByNamedQuery(Search.QUERY_FIND_BY_REQUIREMENT_NAME, parameters);
    }

    public Search fetchSearchByAccountId(String accountId)
    {
        Validation.notNull(accountId, "Account Id should not be null.");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findByNamedQuery(Search.QUERY_FIND_BY_ACCOUNT_ID, parameters);
    }

    public List<Search> fetchSearchByAccountIds(List<String> accountIdList)
    {
        Validation.notNull(accountIdList, "Account Id should not be null.");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountIdList", accountIdList);
        return findAllByNamedQuery(Search.QUERY_FIND_BY_ACCOUNT_IDS, parameters);

    }

    public List<Search> fetchAllSearchBySearchIds(List<String> searchIds)
    {
        return findByIds(searchIds);
    }

    /**
     * @author sanilshet
     * 
     */
    public List<Search> getMatchingSearchIds(String accountId, List<Location> locations, String transactionType, String propertyMarket, String rooms, List<String> amenities, int area, int rentPrice,
            int salePrice, String propertyType, String propertySubType)
    {
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Search> query = builder.createQuery(Search.class);
        Root<Search> search = query.from(Search.class);

        List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();
        List<javax.persistence.criteria.Predicate> predicatesAmenity = new ArrayList<javax.persistence.criteria.Predicate>();
        // predicates.add(builder.notEqual(search.get("account").get("id"),
        // accountId));
        // match for exact and nearby locations
        if (locations.size() > 0)
        {
            List<String> locationsSearchIds = new ArrayList<String>();
            for (Location location : locations)
            {
                locationsSearchIds.add(location.getId());
            }
            predicates.add(search.get("locationId").in(locationsSearchIds));
        }
        // match for amenities
        // if (amenities.size() > 0)
        // {
        // List<String> amenitiesList = (List<String>) search.<List>
        // get("amenitieslist");
        //
        // Boolean equal = Utils.equalLists(amenitiesList, amenities);
        // if (equal)
        // {
        // predicatesAmenity.add(builder.or(builder.isNotNull(search.<String>
        // get("amenities"))));
        // }
        // for (String amenity : amenities)
        // {
        // predicatesAmenity.add(builder.or(builder.like(search.<String>
        // get("amenities"), "%" + amenity + "%")));
        // }
        //
        // predicatesAmenity.add(builder.or(builder.isNull(search.<String>
        // get("amenities"))));
        // predicates.add(builder.or(predicatesAmenity.toArray(new Predicate[]
        // {})));
        //
        // }

        predicates.add(builder.equal(search.get("propertyType"), propertyType));
        predicates.add(builder.isNotNull(search.get("requirementName")));
        if (propertySubType != null)
        {
            predicates.add(builder.or(builder.equal(search.get("propertySubType"), propertySubType), builder.isNull(search.get("propertySubType"))));
        }

        predicates.add(builder.equal(search.get("propertyMarket"), propertyMarket));
        if (rooms != null)
        {
            predicates.add(builder.or(builder.equal(search.get("rooms"), rooms), builder.isNull(search.get("rooms"))));
        }

        if (area > 0)
        {
            predicates.add(builder.and(builder.and(builder.le(search.<Integer> get("minArea"), area), builder.ge(search.<Integer> get("maxArea"), area))));
        }

        if ((rentPrice == -1 || rentPrice > 0) && (salePrice == -1 || salePrice > 0))
        {
            predicates.add(builder.or(builder.equal(search.get("transactionType"), "rent"), builder.equal(search.get("transactionType"), "sale")));

            if (rentPrice > 0)
            {
                // TODO : Use proper ge and le expressions
                // PropertyRentPrice >= searchMinRentPrice && PropertyRentPrice
                // <= searchMaxRentPrice
                // IMPLEMENTED: searchMinRentPrice <= PropertyRentPrice &&
                // searchMaxRentPrice >= PropertyRentPrice
                predicates.add(builder.and(builder.le(search.<Integer> get("minRentPrice"), rentPrice), builder.ge(search.<Integer> get("maxRentPrice"), rentPrice)));
            }

            if (salePrice > 0)
            {
                // TODO : Use proper ge and le expressions
                // PropertySalePrice >= searchMinSalePrice && PropertySalePrice
                // <= searchMaxSalePrice
                // IMPLEMENTED: searchMinSalePrice <= PropertySalePrice &&
                // searchMaxSalePrice >= PropertySalePrice
                predicates.add(builder.and(builder.le(search.<Integer> get("minSalePrice"), salePrice), builder.ge(search.<Integer> get("maxSalePrice"), salePrice)));
            }

        }
        else
        {
            if (rentPrice > 0 || rentPrice == -1)
            {
                predicates.add(builder.equal(search.get("transactionType"), "rent"));
                if (rentPrice > 0)
                {
                    // TODO : Use proper ge and le expressions
                    // PropertyRentPrice >= searchMinRentPrice &&
                    // PropertyRentPrice <= searchMaxRentPrice
                    // IMPLEMENTED: searchMinRentPrice <= PropertyRentPrice &&
                    // searchMaxRentPrice >= PropertyRentPrice
                    predicates.add(builder.and(builder.and(builder.le(search.<Integer> get("minRentPrice"), rentPrice), builder.ge(search.<Integer> get("maxRentPrice"), rentPrice))));
                }
            }
            if (salePrice > 0 || salePrice == -1)
            {
                predicates.add(builder.equal(search.get("transactionType"), "sale"));

                if (salePrice > 0)
                {
                    // TODO : Use proper ge and le expressions
                    // PropertySalePrice >= searchMinSalePrice &&
                    // PropertySalePrice <= searchMaxSalePrice
                    // IMPLEMENTED: searchMinSalePrice <= PropertySalePrice &&
                    // searchMaxSalePrice >= PropertySalePrice
                    predicates.add(builder.and(builder.and(builder.le(search.<Integer> get("minSalePrice"), salePrice), builder.ge(search.<Integer> get("maxSalePrice"), salePrice))));
                }

            }
        }
        /*
         * if (rentPrice > 0 && salePrice > 0) {
         * predicates.add(builder.equal(search.get("transactionType"),
         * "sale,rent"));
         * predicates.add(builder.or(builder.equal(search.get("transactionType"
         * ), "sale,rent"), builder.equal(search.get("transactionType"),
         * "rent,sale"))); }
         */
        query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

        TypedQuery<Search> q = entityManager.createQuery(query);

        List<Search> newMatchingIds = new ArrayList<Search>();

        return newMatchingIds;
    }

    public Search fetchSearchBySearchId(String searchId)
    {
        return findById(searchId);
    }

    public void deleteSearch(Search search)
    {
        remove(search);
    }

    public List<Search> fetchRequirementSearchByAccountId(String accountId)
    {
        Validation.notNull(accountId, "Account Id should not be null.");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        // TODO: add requirement name in function argument
        parameters.put("requirementName", "");
        return findAllByNamedQuery(Search.QUERY_FIND_REQUIREMENT_SEARCH_BY_ACCOUNT_ID, parameters);
    }

    public List<Search> fetchAllExpiredRequirementSearch(Date expiryTime)
    {
        Validation.notNull(expiryTime, "Expiry time should not be null");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("expiredTime", expiryTime);
        parameters.put("requirementName", "");
        return findAllByNamedQuery(Search.QUERY_FIND_EXPIRED_REQUIREMENT_SEARCHES, parameters);
    }

    public Search fetchLatestSearchByAccountId(String accountId) throws NoResultException
    {
        Validation.notNull(accountId, "Account id time should not be null");

        TypedQuery<Search> namedQuery = getEntityManager().createNamedQuery(Search.QUERY_FIND_LATEST_SEARCH_BY_ACCOUNT, Search.class);
        namedQuery.setFirstResult(0);
        namedQuery.setMaxResults(1);
        namedQuery.setParameter("accountId", accountId);
        Search search = namedQuery.getSingleResult();
        return search;
    }

    public List<Search> getMatchingSearches(String locationId, String cityId, long minPrice, long maxPrice, long minSize, long Maxsize, long price, long size, PropertyType propertyType,
            PropertyTransactionType transactionType, Rooms rooms, BroadcastType broadcastType, PropertySubType propertySubType)
    {

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Search> query = builder.createQuery(Search.class);
        Root<Search> search = query.from(Search.class);
        List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();

        if (propertyType != null)
        {
            predicates.add(builder.or(builder.equal(search.get("propertyType"), propertyType), builder.isNull(search.get("propertyType"))));
        }

        if (propertySubType != null)
        {
            predicates.add(builder.or(builder.equal(search.get("propertySubType"), propertySubType), builder.isNull(search.get("propertySubType"))));
        }

        if (transactionType != null)
        {
            predicates.add(builder.or(builder.equal(search.get("transactionType"), transactionType), builder.isNull(search.get("transactionType"))));
        }

        if (rooms != null)
        {

            predicates.add(builder.or(builder.like(search.<String> get("rooms"), "%" + rooms.toString() + "%"), builder.isNull(search.get("rooms"))));
        }

        if (locationId != null)
        {
            predicates.add(builder.or(builder.equal(search.get("locationId"), locationId), builder.isNull(search.get("locationId"))));
        }

        if (cityId != null)
        {
            predicates.add(builder.or(builder.equal(search.get("cityId"), cityId), builder.isNull(search.get("cityId"))));
        }

        if (broadcastType.equals(BroadcastType.PROPERTY_LISTING))
        {
            // TODO : Use proper ge and le expressions
            // price >= minPrice &&
            // price <= maxPrice
            // IMPLEMENTED: minPrice <= price &&
            // maxPrice >= price

            if (price > 0)
            {
                predicates.add(builder.and(builder.and(builder.le(search.<Integer> get("minPrice"), price), builder.ge(search.<Integer> get("maxPrice"), price))));

            }
            if (size > 0)
            {
                predicates.add(builder.and(builder.and(builder.le(search.<Integer> get("minArea"), size), builder.ge(search.<Integer> get("maxArea"), size))));
            }

        }
        else if (broadcastType.equals(BroadcastType.REQUIREMENT))
        {

            if (minSize > 0 && Maxsize > 0)
            {
                predicates.add(builder.and(builder.ge(search.<Integer> get("minArea"), minSize), builder.le(search.<Integer> get("maxArea"), Maxsize)));
            }

            if (minPrice > 0 && maxPrice > 0)
            {
                predicates.add(builder.and(builder.ge(search.<Integer> get("minPrice"), minPrice), builder.le(search.<Integer> get("maxPrice"), maxPrice)));

            }

        }

        query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));

        TypedQuery<Search> q = entityManager.createQuery(query);
        return q.getResultList();

    }
}
