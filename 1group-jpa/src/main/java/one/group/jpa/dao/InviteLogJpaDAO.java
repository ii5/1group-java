package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.InviteLog;

public interface InviteLogJpaDAO
{

    /**
     * 
     * @param inviteLog
     * @return
     */
    public void saveInviteLog(InviteLog inviteLog);

    public List<String> findAllInvitedByAccount(String accountId, long inviteDurationInMillies);

    public InviteLog fetchInviteLogById(String inviteLogId);
}
