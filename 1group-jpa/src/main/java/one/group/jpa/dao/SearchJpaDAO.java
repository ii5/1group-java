package one.group.jpa.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import one.group.core.enums.BroadcastType;
import one.group.core.enums.PropertySubType;
import one.group.core.enums.PropertyTransactionType;
import one.group.core.enums.PropertyType;
import one.group.core.enums.Rooms;
import one.group.entities.jpa.Location;
import one.group.entities.jpa.Search;

public interface SearchJpaDAO
{
    /**
     * Save search
     * 
     * @param search
     */
    public void saveSearch(Search search);

    /**
     * Fetch search by location id.
     * 
     * @param locationId
     * @return
     */
    public List<Search> fetchSearchByLocationList(List<String> locationIdList);

    /**
     * Fetch search by requirement name.
     * 
     * @param clientName
     * @param accountId
     * @return
     */
    public Search fetchSearchByRequirementName(String requirementName, String accountId);

    public List<Search> getMatchingSearchIds(String accountId, List<Location> locations, String transactionType, String propertyMarket, String rooms, List<String> amenities, int area, int rentPrice,
            int salePrice, String propertyType, String propertySubType);

    /**
     * Fetch search by search id
     * 
     * @param searchId
     * @return
     */
    public Search fetchSearchBySearchId(String searchId);

    /**
     * 
     * @param searchIds
     * @return
     */
    public List<Search> fetchAllSearchBySearchIds(List<String> searchIds);

    /**
     * Delete search
     * 
     * @param search
     */
    public void deleteSearch(Search search);

    /**
     * Fetch all requirement searches by account id
     * 
     * @param accountId
     */
    public List<Search> fetchRequirementSearchByAccountId(String accountId);

    public List<Search> fetchSearchByAccountIds(List<String> accountIdList);

    /**
     * Fetch list of expired requirement search
     * 
     * @param expiryTime
     * @return
     */
    public List<Search> fetchAllExpiredRequirementSearch(Date expiryTime);

    /**
     * Fetch search of account
     * 
     * @param accountId
     */
    public Search fetchSearchByAccountId(String accountId);

    public Search fetchLatestSearchByAccountId(String accountId) throws NoResultException;

    public List<Search> getMatchingSearches(String locationId, String cityId, long minPrice, long maxPrice, long minSize, long Maxsize, long price, long size, PropertyType propertyType,
            PropertyTransactionType transactionType, Rooms rooms, BroadcastType broadcastType, PropertySubType propertySubType);
}
