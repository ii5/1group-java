package one.group.jpa.dao;

import one.group.entities.jpa.Otp;

public interface OtpJpaDAO extends JPABaseDAO<String, Otp>
{

    public Otp fetchOtpFromOtpAndPhoneNumber(String otp, String mobileNumber);

    // Otp getOtpForClientStatus(String otp, String mobile_number);

    public void deleteOtp(Otp otp);

}
