/**
 * 
 */
package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.User;

/**
 * @author ashishthorat
 *
 */
public interface UserJpaDAO
{
    /**
     * 
     * @param cityId
     * @return
     */
    public List<User> fetchUserByCityId(String cityId);

    /**
     * Save user
     * 
     * @param messages
     * @return TODO
     * @return
     */
    public User saveUser(User user);

    /**
     * 
     * @param userId
     * @return
     */
    public User fetchUserByUserId(String userId);

    /**
     * 
     * @param userId
     * @return
     */
    public User fetchUserByEmail(String email);

    /**
     * 
     * @param shortRef
     * @return
     */
    public User fetchUserByShortRef(String shortRef);
    
    public void saveAll(Collection<User> userCollection);
}
