package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.ClientAnalytics;

/**
 * 
 * @author bhumikabhatt
 *
 */
public interface ClientAnalyticsJpaDAO extends JPABaseDAO<String, ClientAnalytics>
{
    public void saveclientAnalytics(ClientAnalytics analyticsObj);

    public List<ClientAnalytics> fetchTracesByAccountId(String accountId);
}
