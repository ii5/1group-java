package one.group.jpa.dao;

import one.group.entities.jpa.SyncUpdateWriteCursors;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface SyncUpdateWriteCursorsJpaDAO extends JPABaseDAO<String, SyncUpdateWriteCursors>
{
    public SyncUpdateWriteCursors fetchByAccount(String accountId);    
    
    public void addSyncLogWriteCursors(SyncUpdateWriteCursors writeCursor);
}
