package one.group.jpa.dao;

import one.group.core.enums.status.NotificationStatus;
import one.group.entities.jpa.Notification;

public interface NotificationJpaDAO extends JPABaseDAO<String, Notification>
{

    public long fetchCountByStatus(String userId, NotificationStatus status);

}
