package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.PhoneNumber;

public interface PhoneNumberJpaDAO extends JPABaseDAO<String, PhoneNumber>
{
    public PhoneNumber fetchPhoneNumber(String mobileNumber);

	public List<PhoneNumber> fetchPhoneNumbersByIds(Collection<String> phoneNumbers);
}
