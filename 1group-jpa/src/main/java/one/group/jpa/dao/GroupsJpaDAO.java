/**
 * 
 */
package one.group.jpa.dao;

import java.util.List;

import one.group.core.enums.GroupStatus;
import one.group.entities.jpa.Groups;

/**
 * @author ashishthorat
 *
 */
public interface GroupsJpaDAO
{
    public List<Groups> fetchGroupsByGroupStatus(GroupStatus status);

    public Groups fetchByGroupId(String groupId);

    public List<Groups> fetchGroupsByCityId(String cityId);

    public List<Groups> fetchGroupsByLocationId(String cityId);

    public List<Groups> fetchGroupsByCreatorMobileNo(String creatorMobileNo);

    public List<Groups> fetchGroupsIdsByGroupName(String group);
}
