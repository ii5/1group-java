/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.AccountMessageCursors;
import one.group.jpa.dao.AccountMessageCursorJpaDAO;

public class AccountMessageCursorJpaDAOImpl extends JPABaseDAOImpl<String, AccountMessageCursors> implements AccountMessageCursorJpaDAO
{

    public void saveAccountMessageCursor(AccountMessageCursors accountMessageCursor)
    {
        saveOrUpdate(accountMessageCursor);
    }

    public AccountMessageCursors fetchByAccountMessageCursorsId(String accountMessageCursorId)
    {
        return findById(accountMessageCursorId);
    }

}
