package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.LocationAdmin;
/**
 * 
 * @author sanilshet
 *
 */
public interface LocationAdminJpaDAO extends JPABaseDAO<String, LocationAdmin>
{
    public Collection<LocationAdmin> getAllLocations();
    
    public Collection<LocationAdmin> getAllLocationsByCity(List<String> cityIds);
    
    public List<LocationAdmin> fetchLocalitiesOfCityById(String cityId);
}
