package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.entities.jpa.LocationAdmin;
import one.group.jpa.dao.LocationAdminJpaDAO;

public class LocationAdminJpaDAOImpl extends JPABaseDAOImpl<String, LocationAdmin> implements LocationAdminJpaDAO
{
    public Collection<LocationAdmin> getAllLocations()
    {
        String namedQuery = null;
        namedQuery = LocationAdmin.QUERY_GET_ALL_ADMIN_LOCATIONS;
        Map<String, Object> parameters = new HashMap<String, Object>();
        return findAllByNamedQuery(namedQuery, parameters);
    }

    public List<LocationAdmin> fetchLocalitiesOfCityById(String cityId)
    {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<LocationAdmin> query = builder.createQuery(LocationAdmin.class);
        Root<LocationAdmin> root = query.from(LocationAdmin.class);
        query.select(root);
        if (cityId != null)
        {
            Predicate predicate = builder.equal(root.get("city").get("id"), cityId);
            query.where(predicate);
        }
        List<LocationAdmin> localities = getEntityManager().createQuery(query).getResultList();
        return localities;
    }
	
	public Collection<LocationAdmin> getAllLocationsByCity(List<String> cityIds) 
	{	
		String namedQuery = null;
        namedQuery = LocationAdmin.QUERY_GET_ALL_ADMIN_LOCATIONS_BY_CITY;
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("cityList", cityIds);
        return findAllByNamedQuery(namedQuery, parameters);
	}
}
