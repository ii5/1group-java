/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.Collection;

import one.group.entities.jpa.AuthItemChild;
import one.group.jpa.dao.AuthItemChildJpaDAO;

public class AuthItemChildJpaDAOImpl extends JPABaseDAOImpl<String, AuthItemChild> implements AuthItemChildJpaDAO
{

    public void saveAuthItemChild(AuthItemChild authItemChild)
    {
        saveOrUpdate(authItemChild);
    }
    
    public Collection<AuthItemChild> fetchAll()
    {
        return findAll();
    }
}
