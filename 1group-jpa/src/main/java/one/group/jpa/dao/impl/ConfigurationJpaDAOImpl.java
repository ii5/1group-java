package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.Configuration;
import one.group.jpa.dao.ConfigurationJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class ConfigurationJpaDAOImpl extends JPABaseDAOImpl<String, Configuration> implements ConfigurationJpaDAO
{
    /**
     * @param key
     */
    public Configuration getConfiguationByKey(String key)
    {
        Validation.isTrue(!Utils.isEmpty(key), "Key should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("key", key);
        return findByNamedQuery(Configuration.QUERY_GET_KEY_BY_VALUE, parameters);
    }

    /**
     * @param config
     */
    public Configuration saveConfiguration(Configuration config)
    {
        return saveOrUpdate(config);
    }

    /**
     * @param config
     */
    public void remove(Configuration config)
    {
        remove(config);
    }
}
