package one.group.jpa.dao;

import one.group.entities.jpa.OauthAuthorization;

public interface OAuthAuthorizationJpaDAO extends JPABaseDAO<String, OauthAuthorization>
{
    public OauthAuthorization getOauthAuthorizationByClientId(String clientId);
}
