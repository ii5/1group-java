package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.ParentLocations;
import one.group.jpa.dao.ParentLocationJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class ParentLocationJpaDAOImpl extends JPABaseDAOImpl<String, ParentLocations> implements ParentLocationJpaDAO
{
    /**
     * Function to get top level locations filtered by city
     */
    public List<ParentLocations> getAllLocationsByCityId(String cityId)
    {
        Validation.isTrue(!Utils.isEmpty(cityId), "City id should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("cityId", cityId);
        return findAllByNamedQuery(ParentLocations.QUERY_GET_LOCATIONS_BY_CITYID, parameters);
    }
	
	public List<ParentLocations> getAllParentLocations() 
	{	
        Map<String, String> parameters = new HashMap<String, String>();
        return findAllByNamedQuery(ParentLocations.QUERY_GET_ALL_LOCATIONS, parameters);
	}

	public Collection<ParentLocations> getAllParentLocationsByCity(List<String> cityIds) 
	{	
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("cityList", cityIds);
        return findAllByNamedQuery(ParentLocations.QUERY_GET_LOCATIONS_BY_CITIES, parameters);
	}
}
