/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.WaRoundRobinQueueLog;
import one.group.jpa.dao.WaRoundRobinQueueLogJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaRoundRobinQueueLogJpaDAOImpl extends JPABaseDAOImpl<String, WaRoundRobinQueueLog> implements WaRoundRobinQueueLogJpaDAO
{

}
