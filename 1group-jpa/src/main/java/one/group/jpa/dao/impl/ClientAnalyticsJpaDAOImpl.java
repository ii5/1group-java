package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.ClientAnalytics;
import one.group.jpa.dao.ClientAnalyticsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class ClientAnalyticsJpaDAOImpl extends JPABaseDAOImpl<String, ClientAnalytics> implements ClientAnalyticsJpaDAO
{
    public void saveclientAnalytics(ClientAnalytics clinetAnalyticsObj)
    {
        saveOrUpdate(clinetAnalyticsObj);
        return;
    }

    public List<ClientAnalytics> fetchTracesByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(accountId), "Account Id passed should not be null or empty.");

        List<ClientAnalytics> clientAnalytics = new ArrayList<ClientAnalytics>();
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        clientAnalytics.addAll(findAllByNamedQuery(ClientAnalytics.QUERY_FIND_BY_ACCOUNT_ID, parameters));
        return clientAnalytics;
    }

}
