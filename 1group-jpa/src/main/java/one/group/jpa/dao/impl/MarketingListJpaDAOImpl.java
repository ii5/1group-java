package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.MarketingList;
import one.group.jpa.dao.MarketingListJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author sanilshet
 *
 */
public class MarketingListJpaDAOImpl extends JPABaseDAOImpl<String, MarketingList> implements MarketingListJpaDAO
{
    /**
     * 
     * @param phoneNumber
     */
    public MarketingList checkPhoneNumberExists(String phoneNumber)
    {
        Validation.notNull(phoneNumber, "PhoneNumber should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phoneNumber", phoneNumber);
        return findByNamedQuery(MarketingList.FIND_BY_PHONE_NUMBER, parameters);

    }

    public void saveAll(Collection<MarketingList> marketingListCollection)
    {
        Validation.notNull(marketingListCollection, "marketingList should not be null or empty");
        persistAll(marketingListCollection);
    }

    public void save(MarketingList marketingList)
    {
        Validation.notNull(marketingList, "marketingList should not be null or empty");
        saveOrUpdate(marketingList);
    }

    public Collection<MarketingList> fetchAll()
    {
        return findAll();
    }

    public List<MarketingList> fetchAllMarketingListWhichAreNativeContacts(String accountId)
    {
        Validation.notNull(accountId, "Account id should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(MarketingList.FIND_ALL_BY_NATIVE_CONTACTS_BY_ACCOUNT, parameters);
    }

    public List<MarketingList> fetchMarketingListWherePhoneNumbersIn(List<String> phoneNumbers)
    {
        Validation.notNull(phoneNumbers, "Phone numbers should not be null");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("phoneNumbers", phoneNumbers);

        return findAllByNamedQuery(MarketingList.FIND_ALL_WHERE_PHONE_NUMBERS_IN, parameters);
    }

    public List<MarketingList> fetchPhoneNumbersOrderByWhatsAppGroupCount(List<String> phoneNumbers)
    {
        Validation.notNull(phoneNumbers, "Phone numbers should not be null");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("phoneNumbers", phoneNumbers);

        return findAllByNamedQuery(MarketingList.FIND_ALL_ORDER_BY_DESC_WHATSAPP_GROUP_COUNT_WHERE_PHONE_NUMBERS_IN, parameters);
    }

}
