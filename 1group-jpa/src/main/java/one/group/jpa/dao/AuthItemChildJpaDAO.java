/**
 * 
 */
package one.group.jpa.dao;

import java.util.Collection;

import one.group.entities.jpa.AuthItemChild;

/**
 * @author ashishthorat
 *
 */
public interface AuthItemChildJpaDAO
{

    public void saveAuthItemChild(AuthItemChild authItemChild);
    
    public Collection<AuthItemChild> fetchAll();
}
