package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.Amenity;
import one.group.jpa.dao.AmenityJpaDAO;

public class AmenityJpaDAOImpl extends JPABaseDAOImpl<String, Amenity> implements AmenityJpaDAO
{
    public Amenity findByName(String name)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("name", name);
        return findByNamedQuery(Amenity.FIND_AMENITY_BY_NAME, parameters);
    }

}
