package one.group.jpa.dao;

import java.util.Collection;
import java.util.List;

import one.group.entities.jpa.MarketingList;

/**
 * 
 * @author sanilshet
 *
 */
public interface MarketingListJpaDAO extends JPABaseDAO<String, MarketingList>
{

    /**
     * 
     * @param phoneNumber
     * 
     * @return
     */
    public MarketingList checkPhoneNumberExists(String phoneNumber);

    public void saveAll(Collection<MarketingList> marketingListCollection);

    public void save(MarketingList marketingList);

    public Collection<MarketingList> fetchAll();

    public List<MarketingList> fetchAllMarketingListWhichAreNativeContacts(String accountId);

    public List<MarketingList> fetchMarketingListWherePhoneNumbersIn(List<String> phoneNumbers);

    public List<MarketingList> fetchPhoneNumbersOrderByWhatsAppGroupCount(List<String> phoneNumbers);
}
