package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import one.group.entities.jpa.LocationMatch;
import one.group.jpa.dao.LocationMatchJpaDAO;
import one.group.utils.validation.Validation;

public class LocationMatchJpaDAOImpl extends JPABaseDAOImpl<String, LocationMatch> implements LocationMatchJpaDAO
{

    public List<LocationMatch> fetchExactLocationMatchByLocationId(String locationId, boolean isExact)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        List<String> locationIdList = new ArrayList<String>();
        locationIdList.add(locationId);
        parameters.put("locationIdList", locationIdList);
        parameters.put("isExact", isExact);
        return findAllByNamedQuery(LocationMatch.QUERY_GET_EXACT_LOCATION_MATCH_OF_LOCATION_LIST, parameters);
    }

    public List<LocationMatch> fetchLocationMatchByLocationId(String locationId)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("locationId", locationId);
        return findAllByNamedQuery(LocationMatch.QUERY_GET_LOCATION_MATCH_OF_LOCATION, parameters);
    }

    public List<String> fetchLocationMatchByLocationIdList(List<String> locationIdList)
    {
        TypedQuery<String> typedQuery = getEntityManager().createQuery(LocationMatch.TYPED_QUERY_GET_LOCATION_MATCH_ID_BY_LOCATION_ID_LIST, String.class);

        if (locationIdList.isEmpty())
        {
            return new ArrayList<String>();
        }

        typedQuery.setParameter("locationIdList", locationIdList);

        return typedQuery.getResultList();
    }

    public List<LocationMatch> fetchExactLocationMatchByLocationIdList(List<String> locationIdList, boolean isExact)
    {
        Validation.notNull(locationIdList, "List of locations to be fetched should not be null.");
        if (locationIdList.isEmpty())
        {
            return new ArrayList<LocationMatch>();
        }

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("locationIdList", locationIdList);
        parameters.put("isExact", isExact);
        return findAllByNamedQuery(LocationMatch.QUERY_GET_EXACT_LOCATION_MATCH_OF_LOCATION_LIST, parameters);
    }

    public List<String> fetchExactLocationMatchIdListByLocationIdList(List<String> locationIdList, boolean isExact)
    {
        Validation.notNull(locationIdList, "List of locations to be fetched should not be null.");
        if (locationIdList.isEmpty())
        {
            return new ArrayList<String>();
        }

        TypedQuery<String> typedQuery = getEntityManager().createQuery(LocationMatch.TYPED_QUERY_GET_EXACT_LOCATION_MATCH_ID_LIST_BY_LOCATION_ID_LIST, String.class);

        typedQuery.setParameter("locationIdList", locationIdList);
        typedQuery.setParameter("isExact", isExact);

        return typedQuery.getResultList();

    }

    public void saveLocationMatch(LocationMatch locationMatch)
    {
        saveOrUpdate(locationMatch);
    }
}
