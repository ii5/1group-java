/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.User;
import one.group.jpa.dao.UserJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class UserJpaDAOImpl extends JPABaseDAOImpl<String, User> implements UserJpaDAO
{

    public List<User> fetchUserByCityId(String cityId)
    {
        return null;
    }

    public User saveUser(User user)
    {
        return saveOrUpdate(user);
    }

    public User fetchUserByUserId(String userId)
    {
        return findById(userId);
    }

    public User fetchUserByEmail(String emailId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("emailId", emailId);
        return findByNamedQuery(User.QUERY_FIND_BY_EMAIL_ID, parameters);
    }

    public User fetchUserByShortRef(String shortRef)
    {
        return null;
    }
    
    public void saveAll(Collection<User> userCollection)
    {
        persistAll(userCollection);
    }
}
