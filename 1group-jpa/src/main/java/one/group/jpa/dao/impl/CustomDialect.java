package one.group.jpa.dao.impl;

import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.BooleanType;
import org.hibernate.type.StringType;

public class CustomDialect extends org.hibernate.spatial.dialect.mysql.MySQLSpatialDialect
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CustomDialect()
    {
        super();
        registerFunction("group_concat", new StandardSQLFunction("group_concat", StringType.INSTANCE));
        registerFunction("REGEXP", new SQLFunctionTemplate(BooleanType.INSTANCE, "?1 REGEXP ?2"));
    }

}
