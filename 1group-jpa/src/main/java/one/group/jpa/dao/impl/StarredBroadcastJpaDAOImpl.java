/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.StarredBroadcast;
import one.group.jpa.dao.StarredBroadcastJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * @author ashishthorat
 *
 */
public class StarredBroadcastJpaDAOImpl extends JPABaseDAOImpl<String, StarredBroadcast> implements StarredBroadcastJpaDAO
{

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findAllByNamedQuery(StarredBroadcast.QUERY_FIND_STARRED_BROADCAST_BY_ACCOUNT, parameters);
    }

    public void saveStarredBroadcast(StarredBroadcast starredBroadcast)
    {
        saveOrUpdate(starredBroadcast);
    }

    public StarredBroadcast getStarredBroadcastByAccountIdAndBroadcastId(String accountId, String broadcastId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        Validation.isTrue(!Utils.isEmpty(broadcastId), "Broadcast id should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        parameters.put("broadcastId", broadcastId);

        return findByNamedQuery(StarredBroadcast.QUERY_FIND_STARRED_BROADCAST, parameters);
    }

    public void unStarBroadcast(StarredBroadcast starredBroadcast)
    {
        Validation.isTrue(!Utils.isNull(starredBroadcast), "starredBroadcast Entity should not be null");
        remove(starredBroadcast);
    }

    public List<StarredBroadcast> getStarredBroadcastByAccountId(String accountId, int offset, int limit)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findAllByNamedQuery(StarredBroadcast.QUERY_FIND_LATEST_STARRED_BROADCAST_BY_ACCOUNT_WITH_LIMIT, parameters, offset, limit);
    }

    public List<StarredBroadcast> getStarredBroadcastIdsByAccountId(String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findAllByNamedQuery(StarredBroadcast.QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_ACCOUNT, parameters);
    }

    public boolean isBroadcastStarred(String broadcastId)
    {
        Validation.isTrue(!Utils.isEmpty(broadcastId), "Broadcast Id should not be null");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("broadcastId", broadcastId);
        List<StarredBroadcast> starredBroadcasts = findAllByNamedQuery(StarredBroadcast.QUERY_FIND_ALL_STARRED_BROADCAST_ID_BY_BROADCAST_ID, parameters);
        return starredBroadcasts.size() > 0 ? true : false;
    }
}
