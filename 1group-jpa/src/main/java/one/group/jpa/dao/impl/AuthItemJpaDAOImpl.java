/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.AuthItem;
import one.group.jpa.dao.AuthItemJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class AuthItemJpaDAOImpl extends JPABaseDAOImpl<String, AuthItem> implements AuthItemJpaDAO
{

    public List<AuthItem> fetchAuthItemByType(String type)
    {
        return null;
    }

    public AuthItem fetchAuthItemByRuleName(String ruleName)
    {
        return null;
    }

    public AuthItem fetchAuthItemByGroupCode(String groupCode)
    {
        return null;
    }

    public void saveAuthItem(AuthItem authItem)
    {
        saveOrUpdate(authItem);
    }
    
    public Collection<AuthItem> fetchAuthItemByNameList(Collection<String> nameList)
    {
        if (nameList == null || nameList.isEmpty())
        {
            return new ArrayList<AuthItem>();
        }
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("nameList", nameList);
        
        return findAllByNamedQuery(AuthItem.QUERY_FIND_AUTH_ITEMS_BY_NAME_LIST, params);
    }

}
