package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.NotificationTemplate;

public interface NotificationTemplateJpaDAO
{

    public List<NotificationTemplate> fetchNotificationTemplates();

}
