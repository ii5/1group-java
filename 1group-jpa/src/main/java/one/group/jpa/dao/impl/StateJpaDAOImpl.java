/**
 * 
 */
package one.group.jpa.dao.impl;

import one.group.entities.jpa.State;
import one.group.jpa.dao.StateJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class StateJpaDAOImpl extends JPABaseDAOImpl<String, State> implements StateJpaDAO
{

    public State getStateByCode(String code)
    {
        return null;
    }

    public State getStateByName(String name)
    {
        return null;
    }

    public void saveState(State state)
    {
        saveOrUpdate(state);

    }

    public State fetchStateById(String stateId)
    {
        return findById(stateId);
    }

}
