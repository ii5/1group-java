/**
 * 
 */
package one.group.jpa.dao;

import one.group.entities.jpa.State;

/**
 * @author ashishthorat
 *
 */
public interface StateJpaDAO
{
    public State getStateByCode(String code);

    public State getStateByName(String name);

    public void saveState(State state);

    public State fetchStateById(String stateId);
}
