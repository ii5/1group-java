package one.group.jpa.dao.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.Media;
import one.group.jpa.dao.MediaJpaDAO;
import one.group.utils.validation.Validation;

public class MediaJpaDAOImpl extends JPABaseDAOImpl<String, Media> implements MediaJpaDAO
{

    public Media updateMedia(Media media)
    {
        Validation.notNull(media, "Media should not be null.");
        return saveOrUpdate(media);
    }

    public void removeMedia(String entityType, Map<String, Object> parameters) throws IOException
    {
        List<Media> media = findAllByNamedQuery(Media.QUERY_GET_MEDIA_BY_URL, parameters);
        for (int i = 0; i < media.size(); i++)
        {
            this.removeById(media.get(i).getId());
        }

    }

    public Media fetchByMediaId(String mediaId)
    {
        return findById(mediaId);
    }
    
    public List<Media> fetchMediaByIds(Collection<String> mediaIdCollection)
    {
    	return findByIds(mediaIdCollection);
    }
}
