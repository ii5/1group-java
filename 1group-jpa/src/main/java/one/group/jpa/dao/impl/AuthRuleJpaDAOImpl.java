/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.AuthItem;
import one.group.entities.jpa.AuthRule;
import one.group.jpa.dao.AuthRuleJpaDAO;

public class AuthRuleJpaDAOImpl extends JPABaseDAOImpl<String, AuthRule> implements AuthRuleJpaDAO
{

    public List<AuthItem> fetchAuthItemByType(String type)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public AuthItem fetchAuthItemByRuleName(String ruleName)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public AuthItem fetchAuthItemByGroupCode(String groupCode)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void saveAuthRule(AuthRule authRule)
    {
        saveOrUpdate(authRule);

    }

}
