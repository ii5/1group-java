package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.SyncUpdateReadCursors;
import one.group.jpa.dao.SyncUpdateReadCursorsJpaDAO;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 *
 */
public class SyncUpdateReadCursorsJpaDAOImpl extends JPABaseDAOImpl<String, SyncUpdateReadCursors> implements SyncUpdateReadCursorsJpaDAO
{

    public void delete(SyncUpdateReadCursors readCursor)
    {
        Validation.notNull(readCursor, "Read cursor should not be null.");
        remove(readCursor);
    }

    public void createSyncLogReadCursors(SyncUpdateReadCursors readCursor)
    {
        Validation.notNull(readCursor, "Read cursor should not be null.");
        persist(readCursor);
    }

    public SyncUpdateReadCursors fetchByClient(String clientId)
    {
        Validation.notNull(clientId, "Client id passed should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("clientId", clientId);

        return findByNamedQuery(SyncUpdateReadCursors.QUERY_FIND_BY_CLIENT_ID, parameters);
    }

    public List<SyncUpdateReadCursors> getUpdatesByClientIds(List<String> clients)
    {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("clientIdList", clients);

        return findAllByNamedQuery(SyncUpdateReadCursors.QUERY_FIND_BY_CLIENT_IDS, parameters);
    }

}
