package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.Map;

import one.group.entities.jpa.ChatLog;
import one.group.jpa.dao.ChatLogJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class ChatLogJpaDAOImpl extends JPABaseDAOImpl<String, ChatLog> implements ChatLogJpaDAO
{

    public void saveChatLog(ChatLog chatlog)
    {
        saveOrUpdate(chatlog);
        return;
    }

    public ChatLog fetchChatLog(String chatThreadId, String index)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(chatThreadId), "chatThreadId passed should not be null or empty.");
        Validation.isTrue(!Utils.isNullOrEmpty(index), "index passed should not be null or empty.");

        Map parameters = new HashMap();
        parameters.put("chatThreadId", chatThreadId);
        parameters.put("index", Long.valueOf(index));

        return findByNamedQuery(ChatLog.QUERY_FIND_BY_CHATTHREADID_AND_CHATINDEX, parameters);

    }

}
