package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.SyncUpdateReadCursors;

/**
 * 
 * @author nyalfernandes
 *
 */
public interface SyncUpdateReadCursorsJpaDAO extends JPABaseDAO<String, SyncUpdateReadCursors>
{
    public void delete(SyncUpdateReadCursors readCursor);
    
    public void createSyncLogReadCursors(SyncUpdateReadCursors readCursor);
    
    public SyncUpdateReadCursors fetchByClient(String clientId);

	public List<SyncUpdateReadCursors> getUpdatesByClientIds(List<String> clients);
    
}
