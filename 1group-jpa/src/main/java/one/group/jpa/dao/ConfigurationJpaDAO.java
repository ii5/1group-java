package one.group.jpa.dao;

import one.group.entities.jpa.Configuration;


/**
 * 
 * @author sanilshet
 *
 */
public interface ConfigurationJpaDAO extends JPABaseDAO<String, Configuration>
{
    /**
     * 
     * @param key
     * @return
     */
    public Configuration getConfiguationByKey(String key);

    /**
     * 
     * @param config
     * @return
     */
    public Configuration saveConfiguration(Configuration config);

    /**
     * 
     */
    public void remove(Configuration config);
}
