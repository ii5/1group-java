package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.core.enums.StarType;
import one.group.entities.jpa.Star;
import one.group.jpa.dao.StarJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class StarJpaDAOImpl extends JPABaseDAOImpl<String, Star> implements StarJpaDAO
{
    /**
     * 
     */
    public Star doStar(String referenceId, String targetEntityId, StarType type)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Validation.isTrue(!Utils.isEmpty(targetEntityId), "Target entity should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        parameters.put("targetEntity", targetEntityId);
        Star existingStar = findByNamedQuery(Star.QUERY_FIND_STARRED_ENTRY, parameters);

        if (existingStar == null)
        {
            existingStar = new Star();
            existingStar.setType(type);
            existingStar.setTargetEntity(targetEntityId);
            existingStar.setReferenceId(referenceId);
            existingStar.setCreatedBy(referenceId);
            saveOrUpdate(existingStar);
        }

        return existingStar;
    }

    public List<Star> getAllStarsByReference(String referenceId, StarType type)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        List<Star> existingStars = findAllByNamedQuery(Star.QUERY_FIND_ALL_STARS, parameters);

        return existingStars;
    }

    public void unStarBroadcast(Star star)
    {
        Validation.isTrue(!Utils.isNull(star), "Star Entity should not be null");
        remove(star);
    }

    public Star getStarByReferenceAndTargetEntity(String referenceId, String targetEntityId)
    {
        Validation.isTrue(!Utils.isEmpty(referenceId), "Reference id should not be null");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("referenceId", referenceId);
        parameters.put("targetEntity", targetEntityId);
        return findByNamedQuery(Star.QUERY_FIND_STARRED_ENTRY, parameters);
    }
}
