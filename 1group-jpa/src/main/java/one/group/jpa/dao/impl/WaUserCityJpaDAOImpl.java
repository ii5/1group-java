/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import one.group.core.enums.FilterField;
import one.group.core.enums.SortField;
import one.group.core.enums.SortType;
import one.group.entities.api.response.bpo.WSUserCity;
import one.group.entities.jpa.Admin;
import one.group.entities.jpa.City;
import one.group.entities.jpa.WaUserCity;
import one.group.jpa.dao.AdminJpaDAO;
import one.group.jpa.dao.CityJpaDAO;
import one.group.jpa.dao.WaUserCityJpaDAO;

/**
 * @author ashishthorat
 * 
 */
public class WaUserCityJpaDAOImpl extends JPABaseDAOImpl<String, WaUserCity> implements WaUserCityJpaDAO
{

    private AdminJpaDAO adminJpaDAO;

    private CityJpaDAO cityJpDAO;

    public AdminJpaDAO getAdminJpaDAO()
    {
        return adminJpaDAO;
    }

    public void setAdminJpaDAO(AdminJpaDAO adminJpaDAO)
    {
        this.adminJpaDAO = adminJpaDAO;
    }

    public CityJpaDAO getCityJpDAO()
    {
        return cityJpDAO;
    }

    public void setCityJpDAO(CityJpaDAO cityJpDAO)
    {
        this.cityJpDAO = cityJpDAO;
    }

    public void saveWaUserCity(WaUserCity waUserCity)
    {
        saveOrUpdate(waUserCity);

    }

    public WaUserCity fetchByWaUserCityId(String waUserCityId)
    {
        return findById(waUserCityId);
    }

    public List<WSUserCity> fetchByPriorityCityAndAdmin(Integer priority, String cityId, String adminId, Map<SortField, SortType> sort, Map<FilterField, Set<String>> filter, int start, int rows)
    {

        // "SELECT NEW
        // one.group.entities.api.response.bpo.WSUserCity(c.admin.id,
        // c.admin.userName, c.city.id, c.city.name, c.priority, c.admin.email)
        // FROM WaUserCity c WHERE c.id IS NOT NULL "

        EntityManager entityManager = getEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<WSUserCity> cq = cb.createQuery(WSUserCity.class);
        Root<WaUserCity> waUserCity = cq.from(WaUserCity.class);

        cq.multiselect(waUserCity.get("admin").<String> get("id"), waUserCity.get("admin").<String> get("userName"), waUserCity.get("city").<String> get("id"), waUserCity.get("city").<String> get(
                "name"), waUserCity.get("priority"), waUserCity.get("admin").<String> get("email"));

        List<Predicate> predicates = new ArrayList<Predicate>();

        if (priority != null)
        {
            predicates.add(cb.equal(waUserCity.get("priority"), priority));
        }

        if (cityId != null && !cityId.trim().isEmpty())
        {
            predicates.add(cb.equal(waUserCity.get("city").<String> get("id"), cityId));
        }

        if (adminId != null && !adminId.trim().isEmpty())
        {
            predicates.add(cb.equal(waUserCity.get("admin").<String> get("id"), adminId));
        }

        // FILTERS

        if (filter != null && !filter.isEmpty())
        {
            for (Entry<FilterField, Set<String>> entry : filter.entrySet())
            {
                FilterField field = entry.getKey();
                Set<String> values = entry.getValue();

                Path<Object> leftClause = null;

                for (String s : field.getDbDomainNames())
                {
                    if (leftClause == null)
                    {
                        leftClause = waUserCity.get(s);
                    }
                    else
                    {
                        leftClause = leftClause.get(s);
                    }
                }

                predicates.add(cb.or(leftClause.in(field.castIntoType(values))));
            }
        }

        // SORT

        if (sort != null && !sort.isEmpty())
        {
            for (Entry<SortField, SortType> entry : sort.entrySet())
            {
                SortField field = entry.getKey();
                SortType type = entry.getValue();

                Path<Object> leftClause = null;

                for (String s : field.getDbDomainNames())
                {
                    if (leftClause == null)
                    {
                        leftClause = waUserCity.get(s);
                    }
                    else
                    {
                        leftClause = leftClause.get(s);
                    }
                }
                
                Order order = null;
                if (type.equals(SortType.ASCENDING))
                {
                    order = cb.asc(leftClause);
                }
                else
                {
                    order = cb.desc(leftClause);
                }

                cq.orderBy(order);
            }
        }

        if (!predicates.isEmpty())
        {
            cq.where(predicates.toArray(new Predicate[predicates.size()]));
        }

        TypedQuery<WSUserCity> q = entityManager.createQuery(cq);

        q.setFirstResult(start);
        q.setMaxResults(rows);

        return q.getResultList();

        // String query = WSUserCity.QUERY_FETCH_ADMIN_DETAILS;
        // String adminIdAddition = "c.admin.id = :adminId";
        // String priorityAddition = "priority = :priority";
        // String cityIdAddition = "c.city.id = :cityId";
        // TypedQuery<WSUserCity> typedQuery =
        // getEntityManager().createQuery(query, WSUserCity.class);
        // ;
        //
        // if (priority != null)
        // {
        // query = query + " AND " + priorityAddition;
        // typedQuery = getEntityManager().createQuery(query, WSUserCity.class);
        // typedQuery.setParameter("priority", priority);
        // }
        //
        // if (adminId != null)
        // {
        // query = query + " AND " + adminIdAddition;
        // typedQuery = getEntityManager().createQuery(query, WSUserCity.class);
        // typedQuery.setParameter("adminId", adminId);
        // }
        //
        // if (cityId != null)
        // {
        // query = query + " AND " + cityIdAddition;
        // typedQuery = getEntityManager().createQuery(query, WSUserCity.class);
        // typedQuery.setParameter("cityId", cityId);
        // }
        //
        // if (filter != null && !filter.isEmpty())
        // {
        // for (Entry<FilterField, Set<String>> entry : filter.entrySet())
        // {
        // FilterField field = entry.getKey();
        // Set<String> values = entry.getValue();
        //
        // query = query + " AND " + field.dbField() + " IN :" + field.dbField()
        // + "List";
        //
        // }
        // }
        //
        // return typedQuery.getResultList();
    }

    public void deleteByUserIdAndCityList(String userId, Collection<String> cityIdList)
    {
        Query typedQuery = getEntityManager().createQuery(WaUserCity.QUERY_FIND_WA_USER_CITY_LIST);
        typedQuery.setParameter("adminId", userId);
        typedQuery.setParameter("cityIdList", cityIdList);

        typedQuery.executeUpdate();
    }

    public void saveBulkByUserIdAndCityIdList(String userId, Map<String, Integer> cityIdMap)
    {
        Admin admin = adminJpaDAO.fetchAdminById(userId);
        List<City> cityList = cityJpDAO.getAllCitiesByCityIds(cityIdMap.keySet());

        for (String cityId : cityIdMap.keySet())
        {
            WaUserCity userCity = new WaUserCity();
            City dummyCity = new City();
            dummyCity.setId(cityId);

            userCity.setAdmin(admin);
            int index = cityList.indexOf(dummyCity);
            if (index != -1)
            {
	            userCity.setCity(cityList.get(index));
            }
	        
            userCity.setPriority(cityIdMap.get(cityId));
	        userCity.setStatus(1);
	            
	
	        getEntityManager().merge(userCity);
           
        }

        getEntityManager().flush();
    }

    public void cleanAll()
    {
        getEntityManager().createQuery("DELETE from WaUserCity w").executeUpdate();
        getEntityManager().flush();
    }

    public void cleanByUserIdList(List<String> userIdList)
    {
        Query typedQuery = getEntityManager().createQuery(WaUserCity.QUERY_DELETE_BY_USER_IDS);
        typedQuery.setParameter("adminIdList", userIdList);

        typedQuery.executeUpdate();
    }

    public List<WSUserCity> fetchUnassignedCityAdmin()
    {
        String query = WSUserCity.QUERY_FETCH_UNASSIGNED_CITY_ADMIN_DETAILS;
        TypedQuery<WSUserCity> typedQuery = getEntityManager().createQuery(query, WSUserCity.class);
        return typedQuery.getResultList();
    }
}
