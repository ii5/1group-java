package one.group.jpa.dao.impl;

import java.util.Date;
import java.util.List;

import one.group.entities.jpa.InviteLog;
import one.group.jpa.dao.InviteLogJpaDAO;
import one.group.utils.Utils;

public class InviteLogJpaDAOImpl extends JPABaseDAOImpl<String, InviteLog> implements InviteLogJpaDAO
{

    public void saveInviteLog(InviteLog inviteLog)
    {
        persist(inviteLog);
        // saveOrUpdate(inviteLog);
    }

    public List<String> findAllInvitedByAccount(String accountId, long inviteDurationInMillies)

    {
        Long currentTime = Utils.getSystemTime();
        Date sentTime = new Date(currentTime - inviteDurationInMillies);
        return getEntityManager().createQuery("SELECT toPhoneNumber FROM InviteLog where sentTime > :sentTime and fromAccountId=:fromAccountId GROUP BY toPhoneNumber ")
                .setParameter("sentTime", sentTime).setParameter("fromAccountId", accountId).getResultList();
    }

    public InviteLog fetchInviteLogById(String inviteLogId)
    {

        return findById(inviteLogId);
    }
}
