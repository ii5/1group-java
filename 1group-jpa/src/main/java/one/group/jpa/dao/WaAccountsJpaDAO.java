/**
 * 
 */
package one.group.jpa.dao;

import one.group.entities.jpa.WaAccounts;

/**
 * @author ashishthorat
 *
 */
public interface WaAccountsJpaDAO
{
    public void saveWaAccounts(WaAccounts waAccounts);
}
