/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.LocationErrorTmp;
import one.group.jpa.dao.LocationErrorTempJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class LocationErrorTempJpaDAOImpl extends JPABaseDAOImpl<String, LocationErrorTmp> implements LocationErrorTempJpaDAO
{

    public List<LocationErrorTmp> fetchLocationErrorTmpByCityId(String cityId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrorTmpByParentId(String parentId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrortmpByLoginUserID(String loginUserId)
    {
        return null;
    }

    public List<LocationErrorTmp> fetchLocationErrorTmpByStatus(String status)
    {
        return null;
    }

    public void saveLocationErrorTmp(LocationErrorTmp locationErrorTmp)
    {
        saveOrUpdate(locationErrorTmp);
    }

    public LocationErrorTmp fetchLocationErrorTmpById(String locationErrorTmpId)
    {
        return findById(locationErrorTmpId);
    }

}
