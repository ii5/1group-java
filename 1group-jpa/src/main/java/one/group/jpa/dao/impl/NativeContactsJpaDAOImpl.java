package one.group.jpa.dao.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;

import one.group.core.enums.status.Status;
import one.group.entities.api.request.v2.WSNativeContact;
import one.group.entities.jpa.Account;
import one.group.entities.jpa.NativeContacts;
import one.group.jpa.dao.NativeContactsJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

/**
 * 
 * @author nyalfernandes
 * 
 */
public class NativeContactsJpaDAOImpl extends JPABaseDAOImpl<String, NativeContacts> implements NativeContactsJpaDAO
{

    public void saveNativeContacts(Account account, String fullName, String companyName, String phoneNumber)
    {
        NativeContacts nativeContact = fetchContactsWithPhonenumberAndAccount(phoneNumber, account);
        if (nativeContact == null)
        {
            nativeContact = new NativeContacts();
        }
        nativeContact = merge(nativeContact);
        nativeContact.setAccount(account);
        nativeContact.setPhoneNumber(phoneNumber);
        nativeContact.setCreatedTime(new Timestamp(System.currentTimeMillis()));
        nativeContact.setFullName(fullName);
        nativeContact.setCompanyName(companyName);
    }

    public Set<Account> fetchAccountsLinkedWithPhonenumber(String phoneNumber)
    {
        Set<Account> accounts = new HashSet<Account>();
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("phoneNumber", phoneNumber);
        List<NativeContacts> nativeContacts = findAllByNamedQuery(NativeContacts.QUERY_FIND_ACCOUNT_RELATIONS_BY_NUMBER, parameters);
        for (NativeContacts nativeContact : nativeContacts)
        {
            accounts.add(nativeContact.getAccount());
        }
        return accounts;
    }

    public Set<String> fetchPhonenumbersLinkedWithAccount(Account account)
    {
        Set<String> linkedPhoneNumbers = new HashSet<String>();
        Map<String, Account> parameters = new HashMap<String, Account>();
        parameters.put("account", account);
        List<NativeContacts> nativeContacts = findAllByNamedQuery(NativeContacts.QUERY_FIND_NUMBERS_BY_ACCOUNT, parameters);
        for (NativeContacts nativeContact : nativeContacts)
        {
            linkedPhoneNumbers.add(nativeContact.getPhoneNumber());
        }
        return linkedPhoneNumbers;
    }

    public NativeContacts fetchContactsWithPhonenumberAndAccount(String phoneNumber, Account account)
    {
        NativeContacts nativeContacts = null;
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("phoneNumber", phoneNumber);
        parameters.put("account", account);

        List<NativeContacts> nativeContactsList = findAllByNamedQuery(NativeContacts.QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBER, parameters);
        if (nativeContactsList != null && !nativeContactsList.isEmpty())
        {
            nativeContacts = nativeContactsList.get(0);
        }
        return nativeContacts;
    }

    public List<NativeContacts> fetchContactsWithPhonenumbersAndAccount(Set<String> phoneNumbers, Account account)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("phoneNumbers", phoneNumbers);
        parameters.put("account", account);

        List<NativeContacts> nativeContactsList = findAllByNamedQuery(NativeContacts.QUERY_FIND_CONTACTS_BY_ACCOUNT_AND_NUMBERS, parameters);
        return nativeContactsList;

    }

    public List<NativeContacts> fetchAllNativeContactsByAccountId(String accountId)
    {
        Validation.notNull(accountId, "Account id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(NativeContacts.QUERY_FIND_NUMBERS_BY_ACCOUNT_ID, parameters);

    }

    public List<String> fetchNativeContactsByPhoneNumber(String phoneNumber, Status status)
    {
        Validation.isTrue(!Utils.isNullOrEmpty(phoneNumber), "Phone Number should not be null or empty");
        Validation.isTrue(!Utils.isNullOrEmpty(status.toString()), "Status should not be null or empty");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("phoneNumber", phoneNumber);
        parameters.put("status", status);

        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(NativeContacts.QUERY_FIND_CONTACTS_BY_PHONE_NUMBER_AND_STATUS, String.class);
        typedQuery.setParameter("phoneNumber", phoneNumber);
        typedQuery.setParameter("status", status);

        return typedQuery.getResultList();

    }

    public List<WSNativeContact> fetchAllNativeContactsByAccountIdFiltered(String accountId, Set<String> registeredPhoneNumbers, int offset, int limit)
    {
        Validation.notNull(accountId, "Account id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();

        String query = WSNativeContact.QUERY_FIND_NUMBERS_BY_ACCOUNT_ID;
        TypedQuery<WSNativeContact> typedQuery = getEntityManager().createQuery(query, WSNativeContact.class);
        typedQuery.setParameter("accountId", accountId);
        typedQuery.setParameter("phoneNumbers", registeredPhoneNumbers);
        typedQuery.setFirstResult(offset);
        typedQuery.setMaxResults(limit);
        return typedQuery.getResultList();

        // return
        // findAllByNamedQuery(NativeContacts.QUERY_FIND_NUMBERS_BY_ACCOUNT_ID,
        // parameters, offset, limit);

    }

    public int fetchAllNativeContactsByAccountIdCount(String accountId, Set<String> registeredPhoneNumbers)
    {
        Validation.notNull(accountId, "Account id should not be null");
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(NativeContacts.QUERY_COUNT_NUMBERS_BY_ACCOUNT_ID, String.class);
        typedQuery.setParameter("accountId", accountId);
        typedQuery.setParameter("phoneNumbers", registeredPhoneNumbers);

        return typedQuery.getResultList().size();

    }
}
