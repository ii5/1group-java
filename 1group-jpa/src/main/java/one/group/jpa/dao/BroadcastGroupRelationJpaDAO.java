package one.group.jpa.dao;

import one.group.entities.jpa.BroadcastGroupRelation;

public interface BroadcastGroupRelationJpaDAO
{
    public void save(BroadcastGroupRelation relation);
}
