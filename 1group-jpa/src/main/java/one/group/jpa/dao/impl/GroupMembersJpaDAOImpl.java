/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import one.group.core.enums.GroupSource;
import one.group.entities.jpa.GroupMembers;
import one.group.entities.jpa.helpers.GroupMembersObject;
import one.group.jpa.dao.GroupMembersJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class GroupMembersJpaDAOImpl extends JPABaseDAOImpl<String, GroupMembers> implements GroupMembersJpaDAO
{

    public List<GroupMembers> fetchGroupMembersByAccountId(String accountId)
    {
        return null;
    }

    public GroupMembers fetchGroupMembersById(String groupMemberId)
    {
        return null;
    }

    public List<GroupMembers> fetchGroupMembersByMobileNo(Collection<String> mobilrNoList)
    {
        if (mobilrNoList == null || mobilrNoList.isEmpty())
        {
            return new ArrayList<GroupMembers>();
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("mobileNumberList", mobilrNoList);

        return findAllByNamedQuery(GroupMembers.QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER, params);
    }

    public boolean isAdmin(String groupMembersId)
    {
        return false;
    }

    public void saveGroupMembers(List<GroupMembers> groupMembers)
    {
        saveOrUpdateAll(groupMembers);

    }

    public int updateReadAndReceivedIndex(String groupId, String participantId, int receivedIndex, int readIndex, String receivedMessageId, String readMessageId)
    {
        Query query = getEntityManager().createNamedQuery(GroupMembers.UPDATE_READ_RECEIVED_INDEX_BY_GROUP_AND_PARTICIPANT);
        query.setParameter("readIndex", readIndex);
        query.setParameter("receivedIndex", receivedIndex);
        query.setParameter("receivedMessageId", receivedMessageId);
        query.setParameter("readMessageId", readMessageId);
        query.setParameter("groupId", groupId);
        query.setParameter("participantId", participantId);
        return query.executeUpdate();
    }

    public List<GroupMembers> fetchGroupMemberByParticipantIds(List<String> participantIds)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("participantIds", participantIds);

        return findAllByNamedQuery(GroupMembers.QUERY_ALL_MEMBERS_BY_IDS, parameters);
    }

    public List<GroupMembers> fetchGroupMembersByGroupId(String groupId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("groupId", groupId);

        return findAllByNamedQuery(GroupMembers.QUERY_FIND_PARTICIPANTS_BY_GROUP_ID, parameters);
    }

    public GroupMembers fetchGroupMembersByAccountIdAndGroupId(String accountId, String groupId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("groupId", groupId);
        parameters.put("participantId", accountId);
        return findByNamedQuery(GroupMembers.QUERY_FIND_PARTICIPANTS_BY_GROUP_ID_AND_PARTICIPANT_ID, parameters);
    }

    public String fetchGroupIdFromParticipants(String participantOne, String participantTwo, GroupSource source)
    {
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(GroupMembers.FETCH_GROUP_ID_BY_PARTICIPANTS_AND_SOURCE, String.class);
        typedQuery.setParameter("participantOne", participantOne);
        typedQuery.setParameter("participantTwo", participantTwo);
        typedQuery.setParameter("source", source);
        return typedQuery.getSingleResult();
    }

    public List<String> fetchGroupIdListByAccountId(String accountId)
    {
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(GroupMembers.FETCH_ALL_GROUP_ID_BY_ACCOUNT_ID, String.class);
        typedQuery.setParameter("participantId", accountId);
        return typedQuery.getResultList();
    }

    public List<String> fetchMemberIdListByGroupId(String groupId)
    {
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(GroupMembers.FETCH_ALL_PARTICIPANT_IDS_BY_GROUP_ID, String.class);
        typedQuery.setParameter("groupId", groupId);
        return typedQuery.getResultList();
    }

    public List<GroupMembers> fetchGroupMembersByAccountIdAndGroupId(String accountId, List<String> groupIdList)
    {
        if (groupIdList == null || groupIdList.isEmpty())
        {
            return new ArrayList<GroupMembers>();
        }

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("groupIdList", groupIdList);
        parameters.put("participantId", accountId);
        return findAllByNamedQuery(GroupMembers.QUERY_FIND_ALL_PARTICIPANTS_BY_GROUP_IDS_AND_PARTICIPANT_ID, parameters);
    }

    public String fetchGroupIdFromParticipantIdAndSource(String accountId, GroupSource source)
    {
        TypedQuery<String> typedQuery = getEntityManager().createNamedQuery(GroupMembers.FETCH_GROUP_ID_BY_PARTICIPANT_ID_AND_SOURCE, String.class);
        typedQuery.setParameter("participantId", accountId);
        typedQuery.setParameter("source", source);
        return typedQuery.getSingleResult();
    }

    public List<GroupMembers> fetchGroupMembersByMobileNoAndEverSync(List<String> mobileNumbers, Boolean everSync)
    {
        if (mobileNumbers == null || mobileNumbers.isEmpty())
        {
            return new ArrayList<GroupMembers>();
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("mobileNumberList", mobileNumbers);
        params.put("everSync", everSync);

        return findAllByNamedQuery(GroupMembers.QUERY_FIND_MEMBERS_BY_MOBILE_NUMBER_AND_EVER_SYNC, params);

    }

    public List<GroupMembersObject> fetchGroupMembersObjectByMobileNoAndEverSync(List<String> mobileNumbers, List<Boolean> everSync)
    {
        TypedQuery<GroupMembersObject> typedQuery = getEntityManager().createQuery(GroupMembersObject.FETCH_GROUP_ID_BY_PHONE_NUMBERS_AND_EVER_SYNC, GroupMembersObject.class);
        typedQuery.setParameter("mobileNumber", mobileNumbers);
        typedQuery.setParameter("everSync", everSync);

        return typedQuery.getResultList();
    }
}
