/**
 * 
 */
package one.group.jpa.dao.impl;

import java.util.List;

import one.group.entities.jpa.WaMessagesIncoming;
import one.group.jpa.dao.WaMessagesIncomingJpaDAO;

/**
 * @author ashishthorat
 *
 */
public class WaMessagesIncomingJpaDAOImpl extends JPABaseDAOImpl<String, WaMessagesIncoming> implements WaMessagesIncomingJpaDAO
{

    public List<WaMessagesIncoming> fetchUserByGroupId(String groupId)
    {
        return null;
    }

    public void saveWaMessagesIncoming(WaMessagesIncoming waMessagesIncoming)
    {
        persist(waMessagesIncoming);
    }

    public WaMessagesIncoming fetchWaMessagesIncomingByWaMessagesIncomingId(String waMessageId)
    {
        return findById(waMessageId);
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByWaMobileNo(String waMobileNo)
    {
        return null;
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByMessageType(String messageType)
    {
        return null;
    }

    public List<WaMessagesIncoming> fetchWaMessagesIncomingByStatus(String status)
    {
        return null;
    }

}
