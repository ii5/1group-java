package one.group.jpa.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import one.group.entities.api.response.v2.WSSyncUpdate;
import one.group.entities.jpa.SyncUpdate;
import one.group.entities.jpa.SyncUpdateReadCursors;
import one.group.jpa.dao.SyncUpdateJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class SyncUpdateJpaDAOImpl extends JPABaseDAOImpl<String, SyncUpdate> implements SyncUpdateJpaDAO
{
    public void addAccountUpdate(SyncUpdate syncLog)
    {
        Validation.notNull(syncLog, "Sync Log should not be null.");
        persist(syncLog);
    }

    public List<SyncUpdate> fetchByAccount(String accountId)
    {
        Validation.notNull(accountId, "Account Id passed should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        return findAllByNamedQuery(SyncUpdate.QUERY_FIND_BY_ACCOUNT_ID, parameters);
    }

    public void addAccountUpdates(List<SyncUpdate> syncUpdateList)
    {
        Validation.notNull(syncUpdateList, "syncUpdateList cannot be null.");
        persistAll(syncUpdateList);

    }

    public SyncUpdate fetchByAccountAndUpdateIndex(String accountId, int updateIndex)
    {
        Validation.notNull(accountId, "Account id should not be null.");
        Validation.isTrue(updateIndex >= 0, "Update index should not be less that 0");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("updateIndex", new Integer(updateIndex));
        return findByNamedQuery(SyncUpdate.QUERY_FIND_BY_ACCOUNT_AND_INDEX, parameters);
    }

    public List<SyncUpdate> fetchByAccountFromAndTo(String accountId, int fromUpdateIndex, int toUpdateIndex)
    {
        Validation.notNull(accountId, "Account id should not be null.");
        Validation.isTrue(fromUpdateIndex >= 0, "Update index should not be less that 0");
        Validation.isTrue(toUpdateIndex >= fromUpdateIndex, "I really have no clue what you are trying to do!? Your fromUpdateIndex is greater than the toUpdateIndex! [" + fromUpdateIndex + ", "
                + toUpdateIndex + "]");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("fromUpdateIndex", new Integer(fromUpdateIndex));
        parameters.put("toUpdateIndex", new Integer(toUpdateIndex));
        return findAllByNamedQuery(SyncUpdate.QUERY_FIND_BY_ACCOUNT_FROM_AND_TO, parameters);
    }

    public int createClient(String clientId)
    {
        Validation.notNull(clientId, "Client ID should not be null.");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("clientId", clientId);

        StoredProcedureQuery query = executeStoredProcedure(SyncUpdateReadCursors.STORED_PROCEDURE_CREATE_CLIENT, parameters, null, null);
        return (Integer) query.getOutputParameterValue("retVal");
    }

    public void appendUpdate(String accountId, WSSyncUpdate syncUpdate)
    {
        Validation.notNull(accountId, "Account ID should not be null.");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        parameters.put("syncUpdate", Utils.getJsonString(syncUpdate));

        StoredProcedureQuery query = executeStoredProcedure(SyncUpdateReadCursors.STORED_PROCEDURE_APPEND_UPDATE, parameters, null, null);
        int index = (Integer) query.getOutputParameterValue("totalUpdates");

        syncUpdate.setIndex(index);
    }

    public int advanceReadCursor(String accountId, String clientId, int requestedCursorIndex)
    {
        Validation.notNull(clientId, "Client ID should not be null.");
        Validation.notNull(accountId, "Account ID should not be null.");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("clientId", clientId);
        parameters.put("accountId", accountId);
        parameters.put("requestedCursorIndex", new Integer(requestedCursorIndex));

        StoredProcedureQuery query = executeStoredProcedure(SyncUpdateReadCursors.STORED_PROCEDURE_ADVANCE_READ_CURSOR, parameters, null, null);
        return (Integer) query.getOutputParameterValue("maxAckedUpdateOfClient");
    }

    public void deleteEarlierUpdatesOfAccount(int index, String accountId)
    {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaDelete<SyncUpdate> query = builder.createCriteriaDelete(SyncUpdate.class);
        Root<SyncUpdate> e = query.from(SyncUpdate.class);
        List<javax.persistence.criteria.Predicate> predicate = new ArrayList<javax.persistence.criteria.Predicate>();

        predicate.add(builder.equal(e.get("accountId"), accountId));
        javax.persistence.criteria.Expression<Integer> updateIndex = e.get("updateIndex");

        predicate.add(builder.lt(updateIndex, index));

        query.where(builder.and(predicate.toArray(new javax.persistence.criteria.Predicate[predicate.size()])));

        getEntityManager().createQuery(query).executeUpdate();

    }
}
