package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.SearchNotification;
import one.group.jpa.dao.SearchNotificationJpaDAO;

public class SearchNotificationJpaDAOImpl extends JPABaseDAOImpl<String, SearchNotification> implements SearchNotificationJpaDAO
{

    public List<SearchNotification> fetchAllNotifications()
    {
        Map<String, String> parameters = new HashMap<String, String>();
        return findAllByNamedQuery(SearchNotification.QUERY_FETCH_ALL_NOTIFICATION, parameters);

    }

    public void saveSearchNotification(SearchNotification searchNotification)
    {
        persist(searchNotification);
        // saveOrUpdate(searchNotification);
    }

    public void removeAllSearchNotification()
    {
        removeAll();
    }

    public long getSearchNotificationCountByAccountIdAndSentFlag(String userId, boolean sendFlag)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userId", userId);
        parameters.put("sendFlag", sendFlag);
        return getCountByNamedQuery(SearchNotification.QUERY_FETCH_NOTIFICATION_SEARCH_COUNT_BY_ACCOUNT_ID_AND_SENT_FLAG, parameters);
    }

}
