/**
 * 
 */
package one.group.jpa.dao;

import one.group.entities.jpa.ChatLog;

/**
 * @author ashishthorat
 *
 */
public interface ChatLogJpaDAO extends JPABaseDAO<String, ChatLog>
{
    public void saveChatLog(ChatLog chatlog);

    public ChatLog fetchChatLog(String chatThreadId, String index);
}
