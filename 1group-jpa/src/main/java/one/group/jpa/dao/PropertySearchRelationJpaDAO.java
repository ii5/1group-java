package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;

public interface PropertySearchRelationJpaDAO extends JPABaseDAO<String, PropertySearchRelation>
{
    public List<PropertySearchRelation> getMatchingClients(String propertyListingId, String accountId);
    
    public List<PropertySearchRelation> getPropertySearchRelationsOfAccount(String accountId);

    public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation);
    
    
    public List<PropertySearchRelation> getAllMatchingClientsByIds(List<PropertyListing> propertyListingIds, String accountId);
}
