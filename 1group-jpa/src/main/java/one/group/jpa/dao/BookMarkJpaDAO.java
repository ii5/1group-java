package one.group.jpa.dao;

import java.util.List;

import one.group.core.enums.BookMarkType;
import one.group.entities.jpa.BookMark;

/**
 * 
 * @author sanilshet
 *
 */
public interface BookMarkJpaDAO extends JPABaseDAO<String, BookMark>
{
    /**
     * 
     * @param referenceId
     * @param targetEntityId
     * @param type
     * @return
     */
    BookMark bookmark(String referenceId, String targetEntityId, BookMarkType type, String action);

    /**
     * 
     * @param referenceId
     * @param type
     * @return
     */
    List<BookMark> getAllBookMarksByReference(String referenceId, BookMarkType type);

    /**
     * 
     * @param referenceId
     * @param targetEntityId
     * @return
     */
    public BookMark getBookMarkByReferenceAndTargetEntity(String referenceId, String targetEntityId);
    
    /**
     * 
     * @param accountId
     * @param propertyListings
     * @return
     */
    public List<BookMark> checkIsBookMarkedForPropertyListings(String accountId, List<String> propertyListings);
}
