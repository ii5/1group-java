package one.group.jpa.dao;

import one.group.entities.jpa.Menu;

public interface MenuJpaDAO extends JPABaseDAO<String, Menu>
{
    public void saveMenu(Menu menu);

    public Menu fetchByMenuId(String menuId);
}
