package one.group.jpa.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.TypedQuery;

import one.group.entities.jpa.Account;
import one.group.entities.jpa.AccountRelations;
import one.group.jpa.dao.AccountRelationsJpaDAO;
import one.group.utils.validation.Validation;

public class AccountRelationsJpaDAOImpl extends JPABaseDAOImpl<String, AccountRelations> implements AccountRelationsJpaDAO
{

    public Set<AccountRelations> fetchAccountRelations(Account account)
    {
        return account.getAccountRelations();
    }

    public void updateAccountRelation(AccountRelations accountRelation)
    {
        saveOrUpdate(accountRelation);
    }

    public void createAccountRelation(AccountRelations accountRelation)
    {
        persist(accountRelation);
    }

    public boolean isAccountRelationExist(AccountRelations accountRelation)
    {
        boolean exists = false;
        AccountRelations accountRelations = fetchAccountRelations(accountRelation.getAccount().getIdAsString(), accountRelation.getOtherAccountId());
        if (accountRelations != null)
            exists = true;
        return exists;
    }

    public AccountRelations fetchAccountRelations(String accountId, String otherAccountId)
    {
        AccountRelations accRelObj = null;
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        parameters.put("otherAccountId", otherAccountId);
        accRelObj = findByNamedQuery(AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID, parameters);
        return accRelObj;
    }

    public List<AccountRelations> fetchAccountRelations(String accountId)
    {
        Validation.notNull("accountId", "Account Id should not be null.");
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);

        return findAllByNamedQuery(AccountRelations.QUERY_FIND_BY_ACCOUNT_ID, parameters);
    }

    public List<AccountRelations> fetchByRequestByAccountAndToAccount(Account requestByAccount, Account otherAccount)
    {
        // TODO Auto-generated method stub
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", requestByAccount.getIdAsString());
        parameters.put("otherAccountId", otherAccount.getIdAsString());
        List<AccountRelations> relation = findAllByNamedQuery(AccountRelations.QUERY_FIND_BY_FROMACCOUNT_AND_TOACCOUNT, parameters);
        return relation;
    }

    public AccountRelations fetchByAccountAndOtherAccount(String requestByAccount, String otherAccount)
    {
        // TODO Auto-generated method stub
        Map<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("accountId", requestByAccount);
        parameters.put("otherAccountId", otherAccount);
        AccountRelations relation = findByNamedQuery(AccountRelations.QUERY_FIND_BY_ACCOUNT_ID_OTHER_ACCOUNT_ID, parameters);
        return relation;
    }

    public List<AccountRelations> fetchAccountRelationBetweenAccounts(String requestByAccountId, String otherAccountId)
    {
        Map<String, Object> parameters = new HashMap<String, Object>();

        parameters.put("accountId", requestByAccountId);
        parameters.put("otherAccountId", otherAccountId);
        return findAllByNamedQuery(AccountRelations.QUERY_FIND_BY_FROMACCOUNT_AND_TOACCOUNT, parameters);

    }
    
    public List<AccountRelations> fetchAccountRelations(String accountId, List<String> otherAccountIdList, String query)
    {
        Validation.notNull(accountId, "Account Id should not be null.");
        Validation.notNull(otherAccountIdList, "Other account id list should not be null.");
        Validation.isTrue(!otherAccountIdList.isEmpty(), "Account Id should not be empty.");
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("otherAccountIdList", otherAccountIdList);
        
        return findAllByNamedQuery(query, parameters);
    }

	public List<String> fetchContactsByAccountId(String accountId) 
	{
		if (accountId == null || accountId.isEmpty())
		{
			return Collections.emptyList();
		}
		
		TypedQuery<String> typedQuery = getEntityManager().createQuery(AccountRelations.QUERY_FIND_CONTACTS_OF_ACCOUNT, String.class);
		typedQuery.setParameter("accountId", accountId);
		
		return typedQuery.getResultList();
	}

    
}
