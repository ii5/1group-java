package one.group.jpa.dao.impl;

import one.group.entities.jpa.Menu;
import one.group.jpa.dao.MenuJpaDAO;

public class MenuJpaDAOImpl extends JPABaseDAOImpl<String, Menu> implements MenuJpaDAO
{

    public void saveMenu(Menu menu)
    {
        saveOrUpdate(menu);

    }

    public Menu fetchByMenuId(String menuId)
    {
        return findById(menuId);
    }

}
