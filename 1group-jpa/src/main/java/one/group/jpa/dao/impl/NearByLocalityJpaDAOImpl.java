package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.NearByLocation;
import one.group.jpa.dao.NearByLocalityJpaDAO;

public class NearByLocalityJpaDAOImpl extends JPABaseDAOImpl<String, NearByLocation> implements NearByLocalityJpaDAO
{

    public List<NearByLocation> fetchLocalityByLocationId(String locationId)
    {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("locationId", locationId);
        return findAllByNamedQuery(NearByLocation.QUERY_GET_ACTIVE_LOCALITY_OF_LOCATION, parameters);
    }
}
