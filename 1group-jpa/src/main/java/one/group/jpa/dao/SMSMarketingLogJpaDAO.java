package one.group.jpa.dao;

import java.util.List;

import one.group.entities.jpa.SmsMarketingLog;

/**
 * 
 * @author miteshchavda
 *
 */
public interface SMSMarketingLogJpaDAO
{
    public void saveSMSMarkingLog(SmsMarketingLog smsMarkingLog);

    public List<SmsMarketingLog> fetchAllLog();
}
