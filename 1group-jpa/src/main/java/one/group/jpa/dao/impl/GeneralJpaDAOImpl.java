package one.group.jpa.dao.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import one.group.core.enums.EntityFieldName;
import one.group.jpa.dao.GeneralJpaDAO;

public class GeneralJpaDAOImpl extends JPABaseDAOImpl<String, Object> implements GeneralJpaDAO
{

    public List<Object> executeAndFetchRecords(String query, Map<EntityFieldName, Object> fieldVsValueMap)
    {
        TypedQuery<Object> typedQuery = getEntityManager().createNamedQuery(query, Object.class);

        if (!fieldVsValueMap.isEmpty())
        {
            for (EntityFieldName name : fieldVsValueMap.keySet())
            {
                typedQuery.setParameter(name.toString(), fieldVsValueMap.get(name));
            }
        }

        return typedQuery.getResultList();
    }

}
