package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.NotificationTemplate;
import one.group.jpa.dao.NotificationTemplateJpaDAO;

public class NotificationTemplateJpaDAOImpl extends JPABaseDAOImpl<String, NotificationTemplate> implements NotificationTemplateJpaDAO
{

    public List<NotificationTemplate> fetchNotificationTemplates()
    {
        Map<String, String> params = new HashMap<String, String>();
        return findAllByNamedQuery(NotificationTemplate.QUERY_FETCH_ALL_TEMPLATES, params);
    }

}
