package one.group.jpa.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import one.group.entities.jpa.PropertyListing;
import one.group.entities.jpa.PropertySearchRelation;
import one.group.jpa.dao.PropertySearchRelationJpaDAO;
import one.group.utils.Utils;
import one.group.utils.validation.Validation;

public class PropertySearchRelationJpaDAOImpl extends JPABaseDAOImpl<String, PropertySearchRelation> implements PropertySearchRelationJpaDAO
{

    public List<PropertySearchRelation> getMatchingClients(String propertyListingId, String accountId)
    {
        Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("propertyListingId", propertyListingId);
        return findAllByNamedQuery(PropertySearchRelation.QUERY_FIND_MATCHING_CLIENTS, parameters);
    }
    
    public List<PropertySearchRelation> getPropertySearchRelationsOfAccount(String accountId)
    {
        Validation.notNull(accountId, "Account ID should not be null.");
        
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("accountId", accountId);
        
        return findAllByNamedQuery(PropertySearchRelation.QUERY_FIND_RELATIONS_OF_ACCOUNT, parameters);
    }
    

    public void savePropertySearchRelation(PropertySearchRelation propertySearchRelation)
    {
        this.persist(propertySearchRelation);
    }

	public List<PropertySearchRelation> getAllMatchingClientsByIds(
			List<PropertyListing> propertyListingIds, String accountId) {
		Validation.isTrue(!Utils.isEmpty(accountId), "Account id should not be null");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("accountId", accountId);
        parameters.put("propertyListingIds", propertyListingIds);
        return findAllByNamedQuery(PropertySearchRelation.QUERY_FIND_ALL_MATCHING_CLIENTS, parameters);
	}

}
