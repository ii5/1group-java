package one.group.jpa.dao.impl;

import one.group.entities.jpa.BroadcastGroupRelation;
import one.group.jpa.dao.BroadcastGroupRelationJpaDAO;

public class BroadcastGroupRelationJpaDAOImpl extends JPABaseDAOImpl<String, BroadcastGroupRelationJpaDAO> implements BroadcastGroupRelationJpaDAO
{
    public void save(BroadcastGroupRelation relation)
    {
        save(relation);
    }
}
